module.exports = {
  env: {
    browser: true,
    es6: true,
    amd: true,
    node: true,
    jest: true
  },
  extends: "eslint:recommended",
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
    __DEV__: true,
    FixtureAPI: true,
    "jest/globals": true
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: "module"
  },
  plugins: ["react", "@typescript-eslint", "jest"],
  rules: {
    indent: ["off", 2],
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "single"],
    "no-console": [
      "error",
      {
        allow: ["tron", "disableYellowBox"]
      }
    ],
    "no-empty": [
      "error",
      {
        allowEmptyCatch: true
      }
    ],
    "no-prototype-builtins": "off",
    "no-useless-escape": "off",
    "no-mixed-spaces-and-tabs": "off",
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
    semi: ["error", "never"],
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        vars: "all",
        args: "after-used",
        ignoreRestSiblings: false
      }
    ]
  },
  overrides: [
    {
      files: [
        "App/Components/MealListSlider.js",
        "App/Components/Payment/CreditCardPicker.js",
        "App/Components/Payment/PaymentMethodPicker.js",
        "App/Components/SmartComponents/SelectDeliveryAddressButton.js",
        "App/Components/Styles/PaymentBankPickerStyles.js",
        "App/Containers/AddressTypeScreen.js",
        "App/Containers/MyAddresses/CreateAddress/index.js",
        "App/Containers/MyMealPlanModifyCatalogScreen/MealModifyCardButton.js",
        "App/Containers/MyMealPlanModifyCatalogScreen/index.js",
        "App/Containers/NotificationListScreen.js",
        "App/Containers/Styles/ContactUsStyles.js",
        "App/Containers/Styles/CreditCardListScreenStyle.js",
        "App/Containers/Styles/EmailPhoneVerificationScreenStyle.js",
        "App/Containers/Styles/HomeScreenStyle.js",
        "App/Containers/Styles/MealDetailScreenStyle.js",
        "App/Containers/Styles/TopUpScreenStyle.js",
        "App/Containers/SubscriptionOrderModifyScreen.js",
        "App/Containers/SubscriptionScheduleScreen.js",
        "App/Containers/TagFilterScreen.js",
        "App/Containers/TopUpScreen.js",
        "App/Redux/CreateStore.ts",
        "App/Services/SalesOrderService.js"
      ],
      rules: {
        "@typescript-eslint/no-unused-vars": "off"
      }
    }
  ]
};
