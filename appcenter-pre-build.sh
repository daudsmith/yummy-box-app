#!/usr/bin/env bash

VERSION_CODE=$((VERSION_CODE_SHIFT + APPCENTER_BUILD_ID))
BUILD_REV=${BUILD_SOURCEVERSION:0:8}

if [ -n "$APPCENTER_XCODE_PROJECT" ]; then
    echo "Changing CFBundleVersion: $VERSION_CODE"
    plutil -replace CFBundleVersion -string "$VERSION_CODE" ios/YumboxApp/Info.plist

    echo "Checking build ENVFILE"
    if [ "$ENVFILE" = ".env.prod" ]; then
      echo "Production Build"
      echo "Copying IOS firebase service file"
      cp -rf ios/Firebase/Production/GoogleService-Info.plist ios/GoogleService-Info.plist
    elif [ "$ENVFILE" = ".env.uat" ]; then
      echo "SIT Build"
      echo "Copying IOS firebase service file"
      cp -rf ios/Firebase/Sit/GoogleService-Info.plist ios/GoogleService-Info.plist
    elif [ "$ENVFILE" = ".env.staging" ]; then
      echo "Staging Build"
      echo "Copying IOS firebase service file"
      cp -rf ios/Firebase/Staging/GoogleService-Info.plist ios/GoogleService-Info.plist
    else
      echo "Non Production Build, copying dev firebase service file"
      cp -rf ios/Firebase/Dev/GoogleService-Info.plist ios/YumboxApp/GoogleService-Info.plist
    fi
else
    echo "Changing VERSION_CODE in build.gradle: $VERSION_CODE"
    sed -i'.original' "s/versionCode 1/versionCode $VERSION_CODE/g" android/app/build.gradle
fi

echo "Adding BUILD_REV=$BUILD_REV to env files"
echo "" >> .env.prod
echo "BUILD_REV=$BUILD_REV" >> .env.prod
echo "" >> .env.staging
echo "BUILD_REV=$BUILD_REV" >> .env.staging
echo "" >> .env.test
echo "BUILD_REV=$BUILD_REV" >> .env.test
echo "" >> .env.uat
echo "BUILD_REV=$BUILD_REV" >> .env.uat
