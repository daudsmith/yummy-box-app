import {doLogin} from '../Scenarios/AuthenticationScenarios'
import {goToMealListScreen, goToSelectedMealDetail, addNewItemToCart} from '../Scenarios/MealScenarios'
import {goToSingleOrderCheckout, goToMapPickerScreen, setDestination} from '../Scenarios/CheckoutScenarios'
import {goToSingleOrderPayment, processPaymentExceptWithWalletOrCreditCard} from '../Scenarios/PaymentScenarios'


describe ('Add a meal from meal detail screen and do checkout', () => {
  beforeAll(async () => {
    await doLogin(true)
  })

  it ('should navigate to meal list screen', async () => {
    await goToMealListScreen()
  })

  it ('should navigate to meal detail when user click a meal', async () => {
    await goToSelectedMealDetail(0, 0)
  })

  it ('should switch to next meal when user click next arrow button', async () => {
    await waitFor(element(by.id('nextMealButton_0'))).toBeVisible().withTimeout(1000)
    await element(by.id('nextMealButton_0')).tap()
  })

  it ('should add meal to cart and show cart quantity spinner', async () => {
    await addNewItemToCart(1)
  })

  it ('should show shopping cart button and navigate to single order checkout screen', async () => {
    await goToSingleOrderCheckout('shoppingCartButtonOnMealDetail')
  })

  it ('should show select address button and navigate to map picker screen', async () => {
    await goToMapPickerScreen()
  })

  it ('should select current address and navigate back to single order checkout screen', async () => {
    await setDestination(false, 'Grand Indonesia')
  })

  it ('should select proceed to payment and navigate to single order checkout payment screen', async () => {
    await goToSingleOrderPayment()
  })

  it ('should select cash payment method, process payment, and show sales order modal', async () => {
    await processPaymentExceptWithWalletOrCreditCard('cash')
  })
  afterAll(async () => {
    await element(by.id('closeSalesOrderResultModal')).tap()
  })
})