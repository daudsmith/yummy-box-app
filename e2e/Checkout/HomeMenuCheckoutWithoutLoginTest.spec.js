import {doLogin, byPassLoginToHomeScreen} from '../Scenarios/AuthenticationScenarios'
import {goToMealDetailScreenFromHome, addNewItemToCart} from '../Scenarios/MealScenarios'
import {goToSingleOrderCheckout, goToMapPickerScreen, setDestination, redirectCheckoutToLoginScreen} from '../Scenarios/CheckoutScenarios'
import {goToSingleOrderPayment, processPaymentExceptWithWalletOrCreditCard} from '../Scenarios/PaymentScenarios'

describe ('Add meals from home without logging in first', () => {
  it ('should navigate to home screen without doing login process', async () => {
    await byPassLoginToHomeScreen()
  })

  it ('should navigate to meal detail screen', async () => {
    await goToMealDetailScreenFromHome(0)
  })

  it ('should add meal to cart and show cart quantity spinner', async () => {
    await addNewItemToCart(0)
  })

  it ('should show shopping cart button and navigate to login landing screen', async () => {
    await redirectCheckoutToLoginScreen('MealDetail')
  })

  it ('should do login process', async () => {
    await doLogin(false)
  })

  it ('should navigate to checkout screen after tapping shopping cart button', async () => {
    await goToSingleOrderCheckout('shoppingCartButtonOnHome')
  })

  it ('should show select address button and navigate to map picker screen', async () => {
    await goToMapPickerScreen()
  })

  it ('should select current address and navigate back to single order checkout screen', async () => {
    await setDestination(false, 'Grand Indonesia')
  })

  it ('should select proceed to payment and navigate to single order checkout payment screen', async () => {
    await goToSingleOrderPayment()
  })

  it ('should select cash payment method, process payment, and show sales order modal', async () => {
    await processPaymentExceptWithWalletOrCreditCard('cash')
  })
  afterAll(async () => {
    await element(by.id('closeSalesOrderResultModal')).tap()
  })
})