import {doLogin} from '../Scenarios/AuthenticationScenarios'
import {goToMealListScreen, addNewItemToCart} from '../Scenarios/MealScenarios'
import {goToSingleOrderCheckout, setDestination, goToMapPickerScreen} from '../Scenarios/CheckoutScenarios'
import {goToSingleOrderPayment, processPaymentExceptWithWalletOrCreditCard} from '../Scenarios/PaymentScenarios'

describe('Add some meals from meal list screen and do checkout', () => {
  beforeAll(async () => {
    await doLogin(true)
  })
  it ('should navigate to meal list screen', async () => {
    await goToMealListScreen()
  })
  it ('should show next day meal', async () => {
    await element(by.id('nextDay')).tap()
  })
  it ('should be able to change category', async () => {
    await element(by.id('mealScroll_0')).swipe('left')
  })
  it ('should add meal to cart, and cart quantity spinner is visible', async () => {
    await addNewItemToCart(0, 1)
  })
  it ('should have shopping cart button visible and navigate to single order checkout screen', async () => {
    await goToSingleOrderCheckout('shoppingCartButtonOnMealList')
  })
  it ('should show select address button and navigate to map picker screen', async () => {
    await goToMapPickerScreen()
  })
  it ('should select current address and navigate back to single order checkout screen', async () => {
    await setDestination(true)
  })
  it ('should select proceed to payment and navigate to single order checkout payment screen', async () => {
    await goToSingleOrderPayment()
  })
  it ('should select virtual account payment method, process payment, and show sales order modal', async () => {
    await processPaymentExceptWithWalletOrCreditCard('virtualAccount')
  })
  afterAll(async () => {
    await element(by.id('closeSalesOrderResultModal')).tap()
  })
})