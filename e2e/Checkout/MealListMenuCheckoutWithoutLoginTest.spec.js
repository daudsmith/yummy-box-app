import {doLogin, byPassLoginToHomeScreen} from '../Scenarios/AuthenticationScenarios'
import {goToMealListScreen, addNewItemToCart} from '../Scenarios/MealScenarios'
import {goToSingleOrderCheckout, setDestination, goToMapPickerScreen, redirectCheckoutToLoginScreen} from '../Scenarios/CheckoutScenarios'
import {goToSingleOrderPayment, processPaymentExceptWithWalletOrCreditCard} from '../Scenarios/PaymentScenarios'

describe ('add some meals from meal list screen without loggin in first and do checkout', () => {
  it ('should navigate to home screen without doing login process', async () => {
    await byPassLoginToHomeScreen()
  })
  it ('should navigate to meal list screen', async () => {
    await goToMealListScreen()
  })
  it ('should show next day meal', async () => {
    await element(by.id('nextDay')).tap()
  })
  it ('should be able to change category', async () => {
    await element(by.id('mealScroll_0')).swipe('left')
  })
  it ('should add meal to cart, and cart quantity spinner is visible', async () => {
    await addNewItemToCart(0, 1)
  })
  it ('should show shopping cart button and navigate to login landing screen', async () => {
    await redirectCheckoutToLoginScreen('MealList')
  })
  it ('should do login process', async () => {
    await doLogin(false)
  })
  it ('should navigate to checkout screen after tapping shopping cart button', async () => {
    await goToSingleOrderCheckout('shoppingCartButtonOnHome')
  })

  it ('should show select address button and navigate to map picker screen', async () => {
    await goToMapPickerScreen()
  })

  it ('should select current address and navigate back to single order checkout screen', async () => {
    await setDestination(true)
  })

  it ('should select proceed to payment and navigate to single order checkout payment screen', async () => {
    await goToSingleOrderPayment()
  })

  it ('should select cash payment method, process payment, and show sales order modal', async () => {
    await processPaymentExceptWithWalletOrCreditCard('cash')
  })
  afterAll(async () => {
    await element(by.id('closeSalesOrderResultModal')).tap()
  })

})