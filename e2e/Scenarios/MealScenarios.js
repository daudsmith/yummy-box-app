export const goToMealListScreen = async () => {
  await waitFor(element(by.id('HomeScreen'))).toBeVisible().withTimeout(2000)
  await element(by.id('viewMoreButton')).tap()
  await expect(element(by.id('MealListScreen'))).toBeVisible()
}

export const goToMealDetailScreenFromHome = async (mealIndex = 0) => {
  await waitFor(element(by.id('HomeScreen'))).toBeVisible().withTimeout(2000)
  await element(by.id(`homeMenu_${mealIndex}`)).tap()
  await expect(element(by.id('MealDetailScreen'))).toBeVisible()
}

export const addNewItemToCart = async (mealIndex = 0, categoryIndex = null) => {
  const suffix = categoryIndex === null ? `${mealIndex}` : `${categoryIndex}_${mealIndex}`
  await waitFor(element(by.id(`addToCart_${suffix}`))).toBeVisible().withTimeout(1000)
  await element(by.id(`addToCart_${suffix}`)).tap()
  await expect(element(by.id(`cartQuantitySpinner_${suffix}`))).toBeVisible()
}

export const goToSelectedMealDetail = async (mealIndex = 0, categoryIndex = 0) => {
  await element(by.id(`meal_${categoryIndex}_${mealIndex}`)).tap()
  await expect(element(by.id('MealDetailScreen'))).toBeVisible()
}