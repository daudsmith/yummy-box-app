export const goToSingleOrderCheckout = async (shoppingCartButtonId) => {
  await expect(element(by.id(shoppingCartButtonId))).toBeVisible()
  await element(by.id(shoppingCartButtonId)).tap()
  await expect(element(by.id('SingleOrderCheckoutScreen'))).toBeVisible()
}

export const goToMapPickerScreen = async () => {
  await element(by.id('selectAddressButton')).tap()
  await waitFor(element(by.id('MapPickerScreen'))).toBeVisible().withTimeout(2000)
}

export const setDestination = async (isSelectingCurrentAddress, search = '') => {
  await waitFor(element(by.id('MapPickerScreen'))).toBeVisible().withTimeout(1000)
  if (!isSelectingCurrentAddress) {
    await element(by.id('addressField')).tap()
    await element(by.id('addressField')).typeText(search)
    await element(by.id('addressList_0')).tap()
  }

  await waitFor(element(by.id('setDestinationButton'))).toBeVisible().withTimeout(1000)
  await element(by.id('setDestinationButton')).tap()
  await waitFor(element(by.id('SingleOrderCheckoutScreen'))).toBeNotVisible().withTimeout(1000)
}

export const redirectCheckoutToLoginScreen = async (fromScreen) => {
  await element(by.id(`shoppingCartButtonOn${fromScreen}`)).tap()
  await waitFor(element(by.id('LoginLandingScreen'))).toBeVisible().withTimeout(500)
}