export const goToSingleOrderPayment = async () => {
  await element(by.id('proceedToPaymentButton')).tap()
  await expect(element(by.id('SingleOrderCheckoutPaymentScreen'))).toBeVisible()
}

export const processPaymentExceptWithWalletOrCreditCard = async (method) => {
  switch (method) {
    case 'cash':
      await element(by.id('paymentOptionButton_cash')).tap()
      break
    case 'virtualAccount':
      await element(by.id('paymentOptionButton_virtualAccount')).tap()
  }
  await element(by.id('placeYourOrderButton')).tap()
  await waitFor(element(by.id('salesOrderResultModal'))).toBeVisible().withTimeout(5000)
}