export const doLogin = async (isWaitingForLoginLandingScreen) => {
  if (isWaitingForLoginLandingScreen) {
    await waitFor(element(by.id('LoginLandingScreen'))).toBeVisible().withTimeout(5000)
  }
  await element(by.id('toLoginScreenButton')).tap()
  await expect(element(by.id('LoginScreen'))).toBeVisible()
  await element(by.id('emailField')).typeText('tony@stark.com')
  await element(by.id('passwordField')).typeText('12345')
  await element(by.id('loginButton')).tap()
  await waitFor(element(by.id('HomeScreen'))).toBeVisible().withTimeout(2000)
}

export const byPassLoginToHomeScreen = async () => {
  await waitFor(element(by.id('LoginLandingScreen'))).toBeVisible().withTimeout(5000)
  await element(by.id('browseMenuButton')).tap()
}