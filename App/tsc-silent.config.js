module.exports = {
  suppress: [
    {
      pathRegExp: '/.*.js$',
      codes: [],
    },
    {
      pathRegExp: '/.*.ts$',
      codes: []
    }
  ],
}
