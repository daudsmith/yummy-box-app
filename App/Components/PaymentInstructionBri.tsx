import * as React from 'react'
import { Text, View } from 'react-native'
import Hyperlink from 'react-native-hyperlink'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import PrimaryTabBar from '../Components/PrimaryTabBar'
import LocaleFormatter from '../Services/LocaleFormatter'
import Colors from '../Themes/Colors'
import NumberedList from './NumberedList'
import { ScrollableTabViewContent } from './Common/ScrollableTabViewContent'
import styles from './Styles/PaymentInstructionStyles'
import { instructionProps } from './PaymentInstructionMandiri'

const PaymentInstructionBri: React.FC<instructionProps> = ({accountNumber}) => {
  return (
    <ScrollableTabView
      initialPage={0}
      renderTabBar={() => <PrimaryTabBar
        tabStyle={styles.tabStyle}
        tabButtonStyle={styles.tabButtonStyle}
        tabTitleStyle={styles.tabTitleStyle}
      />}
    >
      <ScrollableTabViewContent tabLabel='ATM' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Text style={styles.text}>Insert the card, select the language and then enter your PIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "Other Menu" and select "Payment"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Select "Other payment" and select "Briva"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter the Virtual Account number <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text> and the amount you want to pay</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Check the transaction data and press "YES"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
      <ScrollableTabViewContent tabLabel='ONLINE' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Hyperlink linkDefault={true}><Text style={styles.text}>Login to <Text style={{color: Colors.grennBlue}}>https://ib.bri.co.id/</Text>, enter your USER ID and PASSWORD</Text></Hyperlink>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "Payment" and select "Briva"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Enter your Virtual Account Number <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text>, the amount that you want to pay, and click Send</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter your password again along with the authentication code from internet banking mToken</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
      <ScrollableTabViewContent tabLabel='BRI Mobile' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Text style={styles.text}>Login to BRI Mobile Banking, enter your USER ID and PIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "Payment" and select "Briva"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Enter your Virtual Account Number <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text>, and the amount that you want to pay</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Input your PIN and click "Send"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
    </ScrollableTabView>
  )
}

export default PaymentInstructionBri
