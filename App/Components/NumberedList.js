import PropTypes from 'prop-types'
import React, {Component} from 'react'
import {Text, View, ViewPropTypes} from 'react-native'
import Colors from '../Themes/Colors'
import {scale, verticalScale} from 'react-native-size-matters'

export default class NumberedList extends Component {
  static propTypes = {
    number: PropTypes.string.isRequired,
    content: PropTypes.object.isRequired,
    bulletStyle: ViewPropTypes.style,
    bulletTextStyle: PropTypes.object,
    contentStyle: ViewPropTypes.style,
  }

  static defaultProps = {
    bulletStyle: {},
    bulletTextStyle: {},
    contentStyle: {},
  }

  constructor(props) {
    super(props)
  }

  render() {
    const {
      number,
      content,
    } = this.props

    return (
      <View style={{flexDirection: 'row', marginBottom: verticalScale(8)}}>
        <View
          style={{
            width: scale(22),
            height: scale(22),
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 11,
            backgroundColor: Colors.bloodOrange,
            marginTop: verticalScale(1),
            ...this.props.bulletStyle,
          }}
        >
          <Text
            style={{
              color: '#fff',
              ...this.props.bulletTextStyle,
            }}
          >
            {number}
          </Text>
        </View>
        <View
          style={{
            flexShrink: 1,
            flexWrap: 'wrap',
            ...this.props.contentStyle,
          }}
        >
          {content}
        </View>
      </View>
    )
  }
}
