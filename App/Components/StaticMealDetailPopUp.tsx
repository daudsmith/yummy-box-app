import * as React from 'react'
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
} from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Modal from 'react-native-modal'
import Moment from 'moment'
import NavigationBar from './NavigationBar'
import IngredientsAndNutritionTab from './IngredientsAndNutritionTab'
import LocaleFormatter  from '../Services/LocaleFormatter'
import Colors from '../Themes/Colors'
import { MealDetailPopUpReduxState } from '../Redux/MealDetailPopUpRedux'
import { Meal } from '../Redux/MealsRedux'
import FastImage from 'react-native-fast-image'

interface StaticMealDetailPopUpInterface {
  mealId?: number,
  date?: string,
  catalogScreenMealDetailPopUp?: Meal
  mealDetailPopUp?: MealDetailPopUpReduxState
  closeModal: () => void
  visible?: boolean
}

const MealDetail = (props?: Meal) => {
  if (props) {
    const {
      name,
      tags,
      catalog_image,
      description,
      ingredients,
      nutrition,
      dinner,
    } = props
    let { medium_thumb } = catalog_image
    if (medium_thumb) {
      medium_thumb = medium_thumb.replace('_medium_thumb', '')
    }

    return (
      <ScrollView contentContainerStyle={Style.mealDetailContent}>
        <Text style={Style.mealDetailTitleText}>{name}</Text>
        <Text style={Style.tagStringText}>{LocaleFormatter.createTagString(tags)}</Text>
        {medium_thumb && (
          <FastImage
            style={Style.mealDetailImage}
            source={{
              uri: medium_thumb,
              priority: FastImage.priority.normal,
            }}
          />
        )}
        <View style={{alignItems: 'flex-start'}}>
          {dinner != '' && (<Text style={Style.mealDetailForDinnerText}>{dinner}</Text>)}
          <Text style={Style.mealDetailDescriptionText}>{description}</Text>
        </View>
        <View style={{width: moderateScale(260)}}>
          <IngredientsAndNutritionTab
            ingredients={ingredients}
            nutritions={nutrition}
          />
        </View>
      </ScrollView>
    )
  }
  return (
    <View />
  )
}

const StaticMealDetailPopUp: React.FC<StaticMealDetailPopUpInterface> = (props) => {
  const { mealDetailPopUp, closeModal, visible, catalogScreenMealDetailPopUp, date } = props
  let mealDetailRendering: Meal
  let loading
  let title = ''
  if (catalogScreenMealDetailPopUp) {
    mealDetailRendering = catalogScreenMealDetailPopUp
    title = Moment(date)
      .format('ddd, DD MMM YYYY')
  } else {
    const { mealDetail } = mealDetailPopUp
    loading = mealDetailPopUp.loading
    if (mealDetail !== null) {
      mealDetailRendering = mealDetail
      title = mealDetail.date !== date
        ? Moment(date)
          .format('ddd, DD MMM YYYY')
        : Moment(mealDetail.date)
          .format('ddd, DD MMM YYYY')
    }
  }

  return (
    <Modal
      backdropColor='white'
      backdropOpacity={0.8}
      isVisible={visible}
      onBackButtonPress={closeModal}
      style={{ margin: 0 }}
    >
      <View style={Style.mealDetailContainer}>
        <View style={{ flex: 1 }}>
          <NavigationBar
            leftSide={() => <View />}
            title={title}
            rightSide={() => {
              return (
                <TouchableOpacity style={Style.closeButton} onPress={closeModal}>
                  <Ionicons name='md-close' size={moderateScale(25)} color={Colors.warmGrey} />
                </TouchableOpacity>
              )
            }}
          />
          {loading
            ? (
              <View style={Style.activityIndicatorContainer}>
                <ActivityIndicator size="large" color={Colors.lightGreyX} />
              </View>
            )
            : (
              MealDetail(mealDetailRendering)
            )
          }
        </View>
      </View>
    </Modal>
  )
}

const Style = StyleSheet.create({
  activityIndicatorContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mealDetailContainer: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1,
    marginVertical: moderateScale(48),
    marginHorizontal: moderateScale(20),
  },
  closeButton: {
    justifyContent: 'center',
    paddingTop: moderateScale(5),
    marginRight: moderateScale(18),
  },
  mealDetailContent: {
    alignItems: 'center',
    paddingTop: moderateScale(15),
    paddingHorizontal: moderateScale(30),
  },
  mealDetailTitleText: {
    fontSize: moderateScale(14),
    fontWeight: '500',
    fontFamily: 'Rubik-Regular',
    color: Colors.brownishGrey,
  },
  tagStringText: {
    fontSize: moderateScale(12),
    fontFamily: 'Rubik-Regular',
    color: Colors.bloodOrange,
    marginTop: moderateScale(5),
  },
  mealDetailImage: {
    height: moderateScale(260),
    width: moderateScale(260),
    borderRadius: 3,
    marginTop: moderateScale(10),
    backgroundColor: Colors.lightGreyX,
  },
  mealDetailDescriptionText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(12),
    fontWeight: '300',
    color: Colors.greyishBrown,
    marginTop: moderateScale(8),
    width: moderateScale(260),
  },
  mealDetailForDinnerText: {
    paddingTop: moderateScale(15),
    width: moderateScale(260),
    color: Colors.primaryGrey,
  },
})

export default StaticMealDetailPopUp
