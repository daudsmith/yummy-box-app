import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import ReactNativeModal from 'react-native-modal'
import { moderateScale } from 'react-native-size-matters'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../Images/SvgIcon/selection.json'
import Colors from '../Themes/Colors'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)
const modalHeight = 40

class OfflineModal extends Component {
  render () {
    return (
      <ReactNativeModal
        animationIn='slideInDown'
        animationOut='slideOutUp'
        isVisible={!this.props.isConnected}
        onBackButtonPress={() => {}}
        backdropColor='rgba(255,255,255,0.8)'
        style={{marginHorizontal: 0, marginVertical: 0}}
      >
        <View style={Styles.modalBox}>
          <View style={Styles.modalContainer}>
            <YumboxIcon name='no-wifi' size={20} color={Colors.brownishGrey} />
            <Text style={Styles.modalText}>You appear to be offline</Text>
          </View>
        </View>
      </ReactNativeModal>
    )
  }
}

const Styles = StyleSheet.create({
  modalBox: {
    flex: 1,
    alignItems: 'center',
  },
  modalContainer: {
    flexDirection: 'row',
    height: moderateScale(modalHeight),
    width: moderateScale(200),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: moderateScale(20),
    elevation: 5,
    borderRadius: modalHeight / 2,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: Colors.pinkishGrey,
    shadowOffset: { height: 5, width: 5 }
  },
  modalText: {
    fontSize: moderateScale(12),
    fontFamily: 'Rubik-Regular',
    marginLeft: moderateScale(10),
    color: Colors.brownishGrey
  },
})

export default OfflineModal
