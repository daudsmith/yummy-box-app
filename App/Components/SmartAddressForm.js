import _ from 'lodash'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AddressForm from './AddressForm'
import CheckoutService from '../Services/CheckoutService'

class SmartAddressForm extends Component {
  static propTypes = {
    date: PropTypes.string,
    type: PropTypes.oneOf(
      [
        'checkout',
        'modify_mmp',
        'modify_subscription_order',
      ]),
    cart:PropTypes.object,
    modifiedOrderDetail:PropTypes.object,
    updateSubscriptionDefaultData:PropTypes.func,
    updateUserCart:PropTypes.func
  }
  static defaultProps = {
    useMultipleAddress: false,
    type: 'checkout',
  }

  constructor(props) {
    super(props)
    const { useMultipleAddress, type, date} = this.props
    this.state = { date, useMultipleAddress, type, newLocation: {}}
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    this.setState({
      useMultipleAddress: newProps.useMultipleAddress,
    })
  }

  getSelectedCartItemsIndex(type) {
    if (type === 'checkout') {
      const { cartItems } = this.props.cart
      const { date } = this.state
      return _.findIndex(cartItems, (cartItem) => {
        return cartItem.date === date
      })
    }
    return -1
  }

  getAddressData(type, selectedCartItemsIndex) {
    let deliveryData
    if (type === 'checkout') {
      const { cartItems } = this.props.cart
      deliveryData = selectedCartItemsIndex !== -1
        ? cartItems[selectedCartItemsIndex].deliveryAddress
        : null
    } else if (type === 'modify_subscription_order') {
      const { modifiedOrderDetail } = this.props
      const { subscription_raw_data } = modifiedOrderDetail
      deliveryData = {
        note: subscription_raw_data.default_delivery_note,
        name: subscription_raw_data.default_recipient_name,
        phone: subscription_raw_data.default_recipient_phone,
        place: {
          address: subscription_raw_data.default_delivery_address,
          longitude: subscription_raw_data.default_delivery_longitude,
          latitude: subscription_raw_data.default_delivery_latitude,
        },
      }
    }

    if (!_.isEmpty(deliveryData)) {
      let phone = deliveryData.phone || deliveryData.phone
      let name = deliveryData.name || deliveryData.name
      let note = deliveryData.note || deliveryData.note

      deliveryData = {
        deliveryAddress: deliveryData.address,
        landmark: deliveryData.landmark,
        location: {
          latitude: parseFloat(deliveryData.latitude),
          longitude: parseFloat(deliveryData.longitude),
        },
        note,
        phone,
        name,
        disableEdit: true,
      }
    }

    return deliveryData
  }

  updateDeliveryDetailToCart(field, value) {
    const { cartItems } = this.props.cart
    const { useMultipleAddress, type } = this.state
    const selectedCartItemsIndex = this.getSelectedCartItemsIndex(type)
    const newCartItems = cartItems.map((cartItem, index) => {
      const deliveryAddress = {
        ...cartItem.deliveryAddress,
        [field]: value,
      }
      if (!useMultipleAddress) {
        return {
          ...cartItem,
          deliveryAddress,
        }
      } else {
        if (selectedCartItemsIndex === index) {
          return {
            ...cartItem,
            deliveryAddress,
          }
        } else {
          return cartItem
        }
      }
    })
    const proceedToPayment = CheckoutService.checkIfCartCanProceedToPayment(newCartItems)
    const cart = {
      ...this.props.cart,
      cartItems: newCartItems,
      proceedToPayment,
    }
    this.props.updateUserCart(cart, )
  }

  updateDefaultDeliveryDetail(field, value) {
    const key = field === 'phone' || field === 'name'
      ? `default_recipient_${field}`
      : `default_delivery_${field}`
    this.props.updateSubscriptionDefaultData(key, value)
  }

  handleDeliveryDetailChange(field, value) {
    const { type } = this.state
    if (type === 'checkout') {
      this.updateDeliveryDetailToCart(field, value)
    } else if (type === 'modify_subscription_order') {
      this.updateDefaultDeliveryDetail(field, value)
    }
  }

  render() {
    const { type } = this.state
    const selectedCartItemsIndex = this.getSelectedCartItemsIndex(type)
    const delivery = this.getAddressData(type, selectedCartItemsIndex)

    return (
      <AddressForm
        {...delivery}
        handleDeliveryDetailChange={(field, value) => this.handleDeliveryDetailChange(field, value)}
        deliveryAddressOptionPicker={this.props.onChangeAddressPress}
      />
    )
  }
}



export default SmartAddressForm
