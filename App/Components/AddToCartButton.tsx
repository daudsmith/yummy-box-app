import React from 'react'
import { Dimensions, StyleSheet, ViewStyle } from 'react-native'
import PrimaryButton from './PrimaryButton'
import Colors from '../Themes/Colors'
import { Meal } from '../Redux/MealsRedux'
import { Moment } from 'moment'
import { scale, verticalScale } from 'react-native-size-matters'
import { itemResponse } from '../Services/V2/ApiCart'

export interface cartCandidateInterface {
  item: itemResponse | Meal,
  date: Moment,
  quantity: number,
}

export interface AddToCartButtonProps {
  testIDSuffix?: string,
  handleAddToCartButtonPress: Function,
}

const { width } = Dimensions.get('window')


const AddToCartButton: React.FC<AddToCartButtonProps> = (props) => {
  const { testIDSuffix } = props

  return (
    <PrimaryButton
      position='center'
      label='ADD'
      onPressMethod={props.handleAddToCartButtonPress}
      buttonStyle={Styles.addToCartButton}
      labelStyle={{ color: Colors.bloodOrange }}
      testID={`addToCart_${testIDSuffix}`}
    />
  )
}

export interface AddToCartButtonStyleInterface {
  addToCartButton: ViewStyle
}

const Styles: AddToCartButtonStyleInterface = StyleSheet.create({
  addToCartButton: {
    height: verticalScale(30),
    width: scale(width * 0.18),
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: Colors.bloodOrange,
  },
})

export default AddToCartButton
