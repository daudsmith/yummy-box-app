import * as React from 'react'

export interface Proptypes {
  showAmount?: boolean
  paymentMethodType?: string
  cardImage?: string
  creditCardNumber?: string | number
  amount?: number
  paymentInfo?: object
}

declare class MMPPaymentInformation extends React.Component<Proptypes, any> {}

export default MMPPaymentInformation
