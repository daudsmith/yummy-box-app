import * as React from 'react'
import { Text, TouchableOpacity, View, Dimensions } from 'react-native'
import ReactNativeModal from 'react-native-modal'
import Styles from './Styles/UpdateVersionModalStyle'

interface UpdateVersionModalInterface {
  open: boolean
  onClose: () => void
  onSubmit: () => void
  title?: string
  content?: string
  closeText?: string
  confirmText?: string
}

const UpdateVersionModal: React.FC<UpdateVersionModalInterface> = ({
  open,
  onClose,
  onSubmit,
  title = 'New Update Available',
  content = 'Update now to get the best Yummy experience :)',
  closeText = 'Remind Me Later',
  confirmText = 'Update Now',
}) => {
  let splitContent = content.split('.')
  let contentTwo: string = ''
  if (splitContent.length > 1) {
    content = `${splitContent[0]}.`
    contentTwo = splitContent[1].trim()
  }

  return (
    <ReactNativeModal
      animationIn='slideInDown'
      animationOut='slideOutUp'
      isVisible={open}
      coverScreen={false}
      backdropColor='rgba(0,0,0,0.6)'
      deviceHeight={Dimensions.get('screen').height}
      style={{ marginHorizontal: 0, marginVertical: 0 }}
    >
      <View style={Styles.modalBox}>
        <View style={Styles.modalContainer}>
          <Text style={Styles.modalTitle}>{title}</Text>
          <Text style={Styles.modalText}>{content}</Text>
          {
            contentTwo !== '' && <Text style={Styles.modalText}>{contentTwo}</Text>
          }
          <View style={Styles.modalButton}>
            <TouchableOpacity onPress={onClose} style={Styles.modalButtonClose}>
              <Text style={Styles.buttonTextClose}>{closeText}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onSubmit} style={Styles.modalButtonOk}>
              <Text style={Styles.buttonTextOk}>{confirmText}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ReactNativeModal>
  )
}

export default UpdateVersionModal
