import React from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {scale} from 'react-native-size-matters'
import styles from './Styles/PrimaryButtonStyles'
import Colors from '../Themes/Colors'

export default class PrimaryButton extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      position: props.position === 'left' ? 'flex-start' : props.position === 'right' ? 'flex-end' : 'center',
      label: props.label,
      labelStyle: props.labelStyle,
      iconName: props.iconName,
      iconColor: props.iconColor,
      iconSize: props.iconSize === undefined ? scale(25) : props.iconSize,
      iconStyle: props.iconStyle,
      iconPosition: props.iconPosition,
      buttonStyle: props.buttonStyle,
      disabled: props.disabled,
      onPressMethod: props.onPressMethod
    }
  }
  handleOnPress () {
    if (typeof this.state.onPressMethod !== 'undefined') {
      this.state.onPressMethod()
    }
  }

  render () {
    let {
      label,
      labelStyle,
      iconName,
      iconColor,
      iconSize,
      iconStyle,
      iconPosition,
      buttonStyle,
      disabled
    } = this.state
    const {
      position
    } = this.props
    const disabledButtonColor = disabled ? {backgroundColor: Colors.pinkishGrey} : {}
    return (
      <TouchableOpacity
        testID={this.props.testID}
        accessibilityLabel={this.props.accessibilityLabel}
        style={[styles.touchableOpacityStyle, disabledButtonColor, buttonStyle]}
        disabled={disabled}
        onPress={() => this.handleOnPress()}
      >
        <View style={[
          styles.iconAndTextContainer,
          {
            alignItems: position === 'left' ? 'flex-start' : position === 'right' ? 'flex-end' : 'center'
          }
        ]}>
          <View style={[styles.iconContainer, iconStyle]}>
            {iconPosition === 'left' &&
              <Icon name={iconName} size={Number(iconSize)} color={iconColor} style={{marginRight: scale(15)}} />
            }
            {iconPosition === 'center' &&
              <Icon name={iconName} size={Number(iconSize)} color={iconColor} />
            }
            {label &&
              <Text style={[styles.text, labelStyle]}>{label}</Text>
            }
            {iconPosition === 'right' &&
              <Icon name={iconName} size={Number(iconSize)} color={iconColor} style={{marginLeft: scale(15)}} />
            }
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
