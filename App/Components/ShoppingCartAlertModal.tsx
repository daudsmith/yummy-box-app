import React from 'react'
import AlertBox from './AlertBox'

interface ShoppingCartAlertModalProps {
  primaryAction: () => void
  secondaryAction: () => void
  dismiss: () => void
  visible: boolean
}

const ShoppingCartAlertModal: React.FC<ShoppingCartAlertModalProps> = (props) => {
  const { primaryAction, secondaryAction, dismiss, visible } = props
  return (
    <AlertBox
      primaryAction={primaryAction}
      primaryActionText='Complete Order'
      primaryActionTestProp='completeOrder'
      secondaryAction={secondaryAction}
      secondaryActionText='Clear Cart'
      secondaryActionTestProp='clearCart'
      dismiss={dismiss}
      title="Proceed to Clear Cart?"
      text="You will need to do a separate checkout for this item or package."
      visible={visible}
      animationIn='fadeIn'
      animationOut='fadeOut'
    />
  )
}

export default ShoppingCartAlertModal
