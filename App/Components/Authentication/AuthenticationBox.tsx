import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'
import { scale, verticalScale } from 'react-native-size-matters'


export interface AuthenticationBoxProps {
  title: string,
  subtext: string,
  boxBottomComponent?: JSX.Element,
  children?: JSX.Element | React.ReactNode,
}

const AuthenticationBox: React.FC<AuthenticationBoxProps> = (props) => {
  const { title, subtext, children, boxBottomComponent } = props
  return (
    <View style={Styles.container}>
      <View style={Styles.box}>
        <Text style={Styles.title}>{title}</Text>
        {subtext !== '' &&
          (
            <Text style={Styles.subText}>{subtext}</Text>
          )
        }
        <View style={{ marginTop: 25 }}>
          {children}
        </View>
      </View>
      <View style={{justifyContent: 'flex-end', flexDirection: 'column'}}>
      {boxBottomComponent}
      </View>
    </View>
  )
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between'
  },
  box: {
    flex: 1,
    backgroundColor: 'white',
    paddingVertical: verticalScale(20),
    paddingHorizontal: scale(20),
    marginTop: verticalScale(5),
    marginHorizontal: scale(20),
    marginBottom: 30,
    borderRadius: 6,
    shadowColor: 'rgba(223, 223, 223, 0.3)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 3,
    shadowOpacity: 1,
    elevation: 2
  },
  title: {
    fontFamily: 'Rubik-Medium',
    fontSize: scale(20),
    color: Colors.greyishBrownTwo
  },
  subText: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(11),
    fontWeight: '300',
    color: Colors.greyishBrown,
    lineHeight: verticalScale(14),
    marginTop: verticalScale(10)
  }
})

export default AuthenticationBox
