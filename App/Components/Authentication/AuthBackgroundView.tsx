import * as React from 'react'
import { Container, Content } from 'native-base'
import AuthenticationBox from './AuthenticationBox'
import KeyboardAwareScrollViewCompat from '../KeyboardAwareScrollViewCompat'
import NavigationBar from '../NavigationBar'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import { Colors } from '../../Themes'

export interface AuthBackgroundViewProps {
  scrollEnabled?: boolean,
  dismiss?: () => void,
  boxTitle: string,
  boxSubtext?: string,
  showedModally?: boolean,
  boxBottomComponent?: JSX.Element,
  boxType?: string,
  children?: JSX.Element | React.ReactNode,
  navigation?: NavigationDrawerProp,
}

const AuthBackgroundView: React.FC<AuthBackgroundViewProps> = (props) => {
  const {
    scrollEnabled = true,
    dismiss,
    showedModally = false,
    boxTitle,
    boxSubtext = '',
    boxBottomComponent,
    navigation,
  } = props

  return (
    <Container style={{backgroundColor: Colors.whiteTwo}}>
      <NavigationBar title="" leftSide="back" leftSideNavigation={() => showedModally ? dismiss() : navigation.goBack()} />
      <Content style={{borderBottomWidth: 0, backgroundColor: 'transparent', marginBottom: 0, paddingBottom: 15}}>
        <KeyboardAwareScrollViewCompat contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled' scrollEnabled={scrollEnabled}>
          <AuthenticationBox title={boxTitle} subtext={boxSubtext} boxBottomComponent={boxBottomComponent}>
            {props.children}
          </AuthenticationBox>
        </KeyboardAwareScrollViewCompat>
      </Content>
    </Container>
  )
}

export default AuthBackgroundView
