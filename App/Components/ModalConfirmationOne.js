import React, { Component } from 'react'
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native'
import Modal from 'react-native-modal'

import {Colors} from '../Themes'

export default class ModalSuccessOne extends Component {
  renderModalText (message, confirmationType, value) {
    if (confirmationType === 'email') {
      return (
        <Text style={Styles.modalConfirmationText}> {message}
          <Text style={{fontFamily: 'Rubik-Medium'}}>{value}</Text>?
        </Text>
      )
    }

    return <Text style={Styles.modalConfirmationText}>{message}</Text>
  }

  render () {
    let {message, confirmationType, onPressYes, onPressNo, showModal, value} = this.props
    return (
      <View>
        <Modal isVisible={showModal} style={Styles.modalConfirmationContainer}>
          <View style={Styles.modalConfirmationBox}>
            {this.renderModalText(message, confirmationType, value)}

            <View style={{flexDirection: 'row', justifyContent: 'space-between', width: 190, marginTop: 15}}>
              <TouchableOpacity
                testID='confirmYes'
                accessibilityLabel='confirmYes'
                onPress={() => onPressYes()}
                style={Styles.button2}>
                <Text style={Styles.button2Text}>Yes</Text>
              </TouchableOpacity>

              <TouchableOpacity
                testID='confirmNo'
                accessibilityLabel='confirmNo'
                onPress={() => onPressNo()}
                style={Styles.button3}>
                <Text style={Styles.button2Text}>No</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

let Styles = StyleSheet.create({
  modalConfirmationContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalConfimationBox: {
    width: 280,
    height: 169,
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 14
  },
  modalConfirmationText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.greyishBrown,
    textAlign: 'center'
  },
  button2: {
    borderRadius: 21,
    width: 90,
    height: 35,
    backgroundColor: Colors.bloodOrange,
    alignItems: 'center',
    justifyContent: 'center'
  },
  button2Text: {
    fontFamily: 'Rubik-Regular',
    color: '#fff',
    fontSize: 16
  },
  button3: {
    borderRadius: 21,
    width: 90,
    height: 35,
    backgroundColor: Colors.lightGreyX,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalConfirmationBox: {
    width: 280,
    height: 169,
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 14
  }
})
