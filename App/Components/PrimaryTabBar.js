import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Animated, StyleSheet, Text, View, ViewPropTypes, TouchableOpacity } from 'react-native'
import Colors from '../Themes/Colors'

export default class PrimaryTabBar extends Component {
  static propTypes = {
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    backgroundColor: PropTypes.string,
    activeTextColor: PropTypes.string,
    inactiveTextColor: PropTypes.string,
    textStyle: PropTypes.object,
    tabStyle: ViewPropTypes.style,
    tabButtonStyle: ViewPropTypes.style,
    tabTitleStyle: PropTypes.object,
    renderTab: PropTypes.func,
    underlineStyle: ViewPropTypes.style
  }

  static defaultProps = {
    activeTextColor: Colors.grennBlue,
    inactiveTextColor: 'black',
    backgroundColor: null,
    tabStyle: {},
    tabButtonStyle: {},
    tabTitleStyle: {},
  }

  renderTab (name, page, isTabActive, onPressHandler, parentProps) {
    const activeTextColor = Colors.grennBlue
    const inactiveTextColor = Colors.greyishBrown
    const textColor = isTabActive ? activeTextColor : inactiveTextColor
    const fontWeight = isTabActive ? 'bold' : 'normal'
    const borderWidth = isTabActive ? 0 : 1
    const borderLeftWidth = isTabActive ? 0 : StyleSheet.hairlineWidth

    const borderActiveBottomWidth = isTabActive ? 2 : 0
    const borderActiveBottomColor = isTabActive ? Colors.grennBlue : Colors.frost

    return <TouchableOpacity
      style={{ flex: 1 }}
      key={name}
      accessible={true}
      accessibilityLabel={name}
      accessibilityTraits='button'
      onPress={() => onPressHandler(page)}
    >
      <View
        style={{
          ...styles.tab,
          borderBottomWidth: borderWidth,
          borderLeftWidth: borderLeftWidth,
          borderRightWidth: borderLeftWidth,
          ...parentProps.tabButtonStyle,
        }}
      >
        <View style={{
          height: '100%',
          borderBottomWidth: borderActiveBottomWidth,
          borderBottomColor: borderActiveBottomColor,
        }}>
          <Text
            style={{
              color: textColor,
              fontWeight,
              ...parentProps.tabTitleStyle,
            }}
          >
            {name}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  }

  render () {
    const containerWidth = this.props.containerWidth
    const numberOfTabs = this.props.tabs.length
    const tabUnderlineStyle = {
      position: 'absolute',
      width: 12,
      height: 1,
      backgroundColor: Colors.grennBlue,
      bottom: 0,
      left: (containerWidth / numberOfTabs) / 2
    }

    const translateX = this.props.scrollValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, containerWidth / numberOfTabs]
    })
    return (
      <View
        style={{
          ...styles.tabs,
          backgroundColor: this.props.backgroundColor ,
          ...this.props.tabStyle
        }}
      >
        {this.props.tabs.map((name, page) => {
          const isTabActive = this.props.activeTab === page
          const renderTab = this.props.renderTab || this.renderTab
          return renderTab(name, page, isTabActive, this.props.goToPage, this.props)
        })}
        <Animated.View
          style={[
            tabUnderlineStyle,
            {
              transform: [
                { translateX }
              ]
            },
            this.props.underlineStyle
          ]}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.frost
  },
  tabs: {
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-around',
  }
})
