import React, {Component} from 'react'
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native'
import {moderateScale} from 'react-native-size-matters'
import PropTypes from 'prop-types'
import Colors from '../Themes/Colors'

class IngredientsAndNutritionTab extends Component {
  static propTypes = {
    ingredients: PropTypes.string.isRequired,
    nutritions: PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)
    this.state = {
      ingredients: this.props.ingredients,
      nutritions: this.props.nutritions,
      activeTabsIndex: 0
    }
  }

  renderContent () {
    const {activeTabsIndex, ingredients, nutritions} = this.state
    return (
      <View style={styles.contentBox}>
        {
          activeTabsIndex === 0 ?
            <Text style={styles.nutritionsText}>{ingredients}</Text>
          :
            nutritions.data.map((nutrition, index) => {
              return (
                <View key={index} style={styles.nutritionsBox}>
                  <View style={{ flex: 3 }}>
                    <Text style={styles.nutritionsText}>{nutrition.name}</Text>
                  </View>

                  <View style={{ flex: 2, alignItems: 'flex-end' }}>
                    <Text style={styles.nutritionsText}>{`${nutrition.amount} ${nutrition.unit}`}</Text>
                  </View>
                </View>
              )
            })
        }
      </View>
    )
  }

  renderHeader () {
    const {activeTabsIndex} = this.state

    const borderBottomAndRightWidthIngredients = activeTabsIndex === 0 ? 0 : 1
    const ingredientsTextColor = activeTabsIndex === 0 ? Colors.green : Colors.brownishGrey
    const ingredientsBorderLineColor = activeTabsIndex === 0 ? Colors.green : 'transparent'

    const borderBottomAndRightWidthNutritions = activeTabsIndex === 1 ? 0 : 1
    const nutritionsTextColor = activeTabsIndex === 1 ? Colors.green : Colors.brownishGrey
    const nutritionsBorderLineColor = activeTabsIndex === 1 ? Colors.green : 'transparent'
    return (
      <View style={styles.ingridientAndNutritionsButtonBox}>
        <TouchableOpacity
          onPress={() => {this.setState({activeTabsIndex: 0})}}
          style={[styles.tabHeader, { borderBottomWidth: borderBottomAndRightWidthIngredients, borderRightWidth: borderBottomAndRightWidthIngredients, borderColor: Colors.pinkishGrey }]}
        >
          <Text style={[styles.tabText, { color: ingredientsTextColor }]}>Ingredients</Text>
          <View style={[styles.tabBorder, { backgroundColor: ingredientsBorderLineColor }]} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {this.setState({activeTabsIndex: 1})}}
          style={[styles.tabHeader, { borderBottomWidth: borderBottomAndRightWidthNutritions, borderLeftWidth: borderBottomAndRightWidthNutritions, borderColor: Colors.pinkishGrey }]}>
          <Text style={[styles.tabText, { color: nutritionsTextColor }]}>Nutrition</Text>
          <View style={[styles.tabBorder, { backgroundColor: nutritionsBorderLineColor }]} />
        </TouchableOpacity>
      </View>
    )
  }

  render () {
    return (
      <View style={styles.ingredientsAndNutritionContainer}>
          <View style={{flex:1}}>
            {this.renderHeader()}
            {this.renderContent()}
          </View>
      </View>

    )
  }
}

const styles= StyleSheet.create({
  ingredientsAndNutritionContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: moderateScale(14),
    marginBottom: moderateScale(50),
    borderWidth: 1,
    borderColor: Colors.lightGreyX,
    borderRadius: moderateScale(5)
  },
  ingridientAndNutritionsButtonBox: {
    flex:1,
    flexDirection: 'row'
  },
  tabHeader: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: moderateScale(12),
    paddingBottom: moderateScale(6)
  },
  tabBorder: {
    top: moderateScale(4),
    height: moderateScale(1.2),
    width: moderateScale(50),
    marginBottom: moderateScale(6)
  },
  tabText: {
    fontSize: moderateScale(16),
    fontFamily: 'Rubik-Regular',
  },
  nutritionsBox: {
    flexDirection: 'row',
    marginBottom: moderateScale(5)
  },
  nutritionsText: {
    fontSize: moderateScale(12),
    lineHeight: moderateScale(18),
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrown
  },
  contentBox: {
    flex:1,
    paddingTop: moderateScale(12),
    paddingBottom: moderateScale(19),
    paddingLeft: moderateScale(20),
    paddingRight: moderateScale(17)
  }
})

export default IngredientsAndNutritionTab
