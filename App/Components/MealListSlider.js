import React, {Component} from 'react'
import {View, ScrollView, Image, Text, ActivityIndicator, TouchableWithoutFeedback, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import {moderateScale} from 'react-native-size-matters'
import Moment from 'moment'
import Colors from '../Themes/Colors'
import LocaleFormatter from '../Services/LocaleFormatter'
import {Images} from '../Themes'

import ApiCatalog from '../Services/ApiCatalog'
import FastImage from 'react-native-fast-image'

class MealListSlider extends Component {
  static propTypes = {
    meals: PropTypes.array.isRequired,
    onPress: PropTypes.func.isRequired,
    lastItemPadding: PropTypes.number.isRequired,
    theme: PropTypes.string.isRequired,
    testId: PropTypes.string,
    date: PropTypes.string,
  }

  static defaultProps = {
    testId: ''
  }

  constructor (props) {
    super(props)
    this.state = {
      availability: [],
    }
  }

  UNSAFE_componentWillMount () {
    let availability = []
    this.props.meals.map((meal, index) => {
      if (!meal.hasOwnProperty('item_id')) {
        availability[index] = {
          fetching: true,
          available: false,
        }
      }
    })

    this.setState({availability})
  }

  componentDidMount () {
    // fetching stock
    let availability = this.state.availability
    this.props.meals.map((meal, index) => {
      if (!meal.hasOwnProperty('item_id')) {
        const date = typeof this.props.date !== 'undefined'
          ? Moment(this.props.date).format('YYYY-MM-DD')
          : (typeof meal.date === 'undefined' ? Moment().add(1, 'day').format('YYYY-MM-DD') : meal.date)
        ApiCatalog.create()
        .getMealAvailability(meal.id, date)
        .then((response) => response.data)
        .then((responseBody) => responseBody.data)
        .then((responseData) => {
          if (responseData) {
            availability[index] = {
              fetching: false,
              available: (responseData.remaining_quantity > 0)
            }
          }
          this.setState({availability})
        })
        .catch(() => {
          availability[index] = {
            fetching: false,
            available: true
          }
          this.setState({availability})
        })
      }
    })
  }

  getMealImage (meal) {
    if (meal.hasOwnProperty('catalog_image')) {
      if (meal.catalog_image.hasOwnProperty('medium_thumb')) {
        if (typeof meal.catalog_image.medium_thumb === 'undefined') {
          return Images.showingSoon
        }
        return meal.catalog_image.medium_thumb.replace('_medium_thumb', '')
      }
      return meal.catalog_image
    } else if (meal.hasOwnProperty('images')) {
      if (meal.images.hasOwnProperty('medium_thumb')) {
        if (typeof meal.images.medium_thumb === 'undefined') {
          return Images.showingSoon
        }
        return meal.images.medium_thumb.replace('_medium_thumb', '')
      }
      return meal.images
    }
  }

  onMealPress (meal, index) {
    if (this.props.theme === 'playlist') {
      const mealId = meal.hasOwnProperty('item_id') ? meal.item_id : meal.id
      this.props.onPress(mealId, meal.date)
    } else if (this.props.theme === 'home') {
      this.props.onPress(index, this.props.meals)
    }
  }

  renderMealItem (meal, index) {
    const imageSize = this.props.theme === 'playlist' ? 102 : 140
    const mealImage = this.getMealImage(meal)
    return (
      <View style={[Styles.mealItemContainer, {width: imageSize}]} key={index}>
        {
          meal.id
          ? (
            <TouchableWithoutFeedback testID={`${this.props.testId}_${index}`} onPress={() => this.onMealPress(meal, index)}>
              <View>
                <FastImage
                  style={[Styles.mealItemImage, {height: imageSize, width: imageSize}]}
                  source={{
                    uri: mealImage,
                    priority: FastImage.priority.normal,
                  }}
                />
                {this.renderMealItemDescription(meal, index)}
              </View>
            </TouchableWithoutFeedback>
          ) : (
            <View>
              <View>
                <Image source={Images.showingSoon} style={[Styles.mealItemImage, {height: imageSize, width: imageSize}]} />
                {this.renderMealItemDescription(meal, index)}
              </View>
            </View>
          )
        }
      </View>
    )
  }

  renderMealItemDescription (meal, index) {
    const fontSizeName = this.props.theme === 'playlist' ? 12 : 15
    const imageSize = this.props.theme === 'playlist' ? 102 : 140
    if (this.props.theme === 'playlist') {
      const price = meal.sale_price === 0 ? meal.base_price : meal.sale_price
      return (
        <View style={{width: imageSize}}>
          <Text style={Styles.mealItemDateText}>{Moment(meal.date).format('ddd, DD MMM YYYY')}</Text>
          {meal.hasOwnProperty('name') &&
            <Text style={{fontFamily: 'Rubik-Medium', fontSize: 12, color: Colors.black, marginTop: 3}}>{meal.name}</Text>
          }
          {meal.hasOwnProperty('sale_price') &&
            <Text style={Styles.mealPriceText}>{LocaleFormatter.numberToCurrency(price)}</Text>
          }
        </View>
      )
    } else if (this.props.theme === 'home') {
      return (
        <View style={{flex: 1, marginTop: 17, width: imageSize}}>
          <Text style={[Styles.mealItemNameText, {fontSize: fontSizeName, fontWeight: 'normal', width: imageSize}]}>{meal.name}</Text>
          {(!meal.hasOwnProperty('item_id') && this.state.availability[index].fetching) ?
            <ActivityIndicator style={Styles.mealPriceText} />
          :
            meal.hasOwnProperty('item_id') ?
              <Text style={Styles.mealPriceText}>
                {LocaleFormatter.numberToCurrency(meal.sale_price)}
              </Text>
            :
              this.state.availability[index].available ?
                <Text style={Styles.mealPriceText}>
                  {LocaleFormatter.numberToCurrency(meal.sale_price)}
                </Text>
              :
              <Text style={Styles.mealPriceText}>
                Sold Out
              </Text>
          }
        </View>
      )
    }
  }

  render () {
    const {meals, lastItemPadding, containerStyle} = this.props

    return (
      <ScrollView testID='MealListSlider' horizontal contentContainerStyle={[{marginLeft: 15}, containerStyle]} showsHorizontalScrollIndicator={false}>
        {meals.map((meal, index) => {
          return this.renderMealItem(meal, index)
        })}
        <View style={{ paddingLeft: 20, backgroundColor: 'transparent', width: moderateScale(lastItemPadding) }} />
      </ScrollView>
    )
  }
}

const Styles = StyleSheet.create({
  mealItemContainer: {
    marginRight: 20
  },
  mealItemImage: {
    height: moderateScale(150),
    width: moderateScale(150),
    borderRadius: 3
  },
  mealItemDateText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 10,
    color: Colors.green,
    marginTop: 7
  },
  mealItemNameText: {
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    width: moderateScale(150),
    color: Colors.greyishBrownTwo,
    flexWrap: 'wrap'
  },
  mealTagText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(10),
    color: Colors.bloodOrange,
    marginTop: moderateScale(10),
    marginBottom: moderateScale(5)
  },
  mealPriceText: {
    fontFamily: 'Rubik-Medium',
    fontSize: moderateScale(10),
    color: Colors.pinkishOrange,
    marginTop: moderateScale(5)
  }
})

export default MealListSlider
