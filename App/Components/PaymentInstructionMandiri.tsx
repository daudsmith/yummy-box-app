import * as React from 'react'
import { Text, View } from 'react-native'
import Hyperlink from 'react-native-hyperlink'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import PrimaryTabBar from '../Components/PrimaryTabBar'
import LocaleFormatter from '../Services/LocaleFormatter'
import Colors from '../Themes/Colors'
import NumberedList from './NumberedList'
import { ScrollableTabViewContent } from './Common/ScrollableTabViewContent'
import styles from './Styles/PaymentInstructionStyles'

export interface instructionProps {
  accountNumber: string
}

const PaymentInstructionMandiri: React.FC<instructionProps> = ({accountNumber}) => {
  const accountText: JSX.Element = (
    <Text style={styles.bold}>
      {LocaleFormatter.formatBankAccountNumber(accountNumber)}
    </Text>
  )

  return (
    <ScrollableTabView
      initialPage={0}
      renderTabBar={() => (
        <PrimaryTabBar
          tabStyle={styles.tabStyle}
          tabButtonStyle={styles.tabButtonStyle}
          tabTitleStyle={styles.tabTitleStyle}
        />
      )}
    >
      <ScrollableTabViewContent tabLabel='ATM' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Text style={styles.text}>Insert your ATM card, select the language</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Enter PIN, then select "ENTER"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Select "PAYMENT", then select "MULTI PAYMENT"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={(
              <Text style={styles.text}>
                Enter company code : "88608" (88608 XENDIT), then press "CORRECT"
              </Text>
            )}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={(
              <Text style={styles.text}>Enter Virtual Account Number {accountText}, then press "CORRECT"</Text>
            )}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={(
              <Text style={styles.text}>Enter the amount to transfer, then press "CORRECT"</Text>
            )}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'7.'}
            content={<Text style={styles.text}>Customer details will be displayed. Choose number 1 according to the amount billed and then press "YES"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'8.'}
            content={<Text style={styles.text}>Payment confirmation will be displayed. Select "YES" to proceed</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'9.'}
            content={<Text style={styles.text}>Keep your receipt as proof of payment</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'10.'}
            content={<Text style={styles.text}>Your transaction is successful</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'11.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
      <ScrollableTabViewContent tabLabel='ONLINE' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Hyperlink linkDefault={true}><Text style={styles.text}>Go to Mandiri Internet Banking website <Text style={{color: Colors.grennBlue}}>https://ib.bankmandiri.co.id/</Text></Text></Hyperlink>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Login with your USER ID and PIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Go to the Home page, then select "Payment"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Select "Multi Payment"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Select "My Account Number"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={<Text style={styles.text}>Select "88608 XENDIT" as service provider</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'7.'}
            content={<Text style={styles.text}>Select "Virtual Account Number"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'8.'}
            content={<Text style={styles.text}>Enter your virtual account number <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text></Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'9.'}
            content={<Text style={styles.text}>Go to confirmation page 1</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'10.'}
            content={<Text style={styles.text}>Click on TOTAL if all details are correct and then click on "CONTINUE"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'11.'}
            content={<Text style={styles.text}>Go to confirmation page 2</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'12.'}
            content={<Text style={styles.text}>Enter Challenge Code from your Internet Banking Token, then click on "Send"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'13.'}
            content={<Text style={styles.text}>You will be directed to the confirmation page once your payment has been completed</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'14.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
    </ScrollableTabView>
  )
}

export default PaymentInstructionMandiri
