import {createIconSetFromIcoMoon} from 'react-native-vector-icons'
import icoMoonConfig from '../Images/SvgIcon/selection.json'

export default createIconSetFromIcoMoon(icoMoonConfig)