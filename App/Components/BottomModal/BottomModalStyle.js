import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'
import { moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  modalStyle: {
    flex: 1,
    width: '100%',
    margin: 0,
    justifyContent: 'flex-end',
  },
  modalContainer: {
    backgroundColor: Colors.white,
    paddingVertical: moderateScale(20),
    paddingHorizontal: moderateScale(16),
    borderTopLeftRadius: moderateScale(4),
    borderTopRightRadius: moderateScale(4),
  },
  title: {
    fontSize: moderateScale(16),
    color: Colors.primaryOrange,
    fontFamily: 'Rubik-Medium',
    marginBottom: moderateScale(16),
  },
  message: {
    fontSize: moderateScale(14),
    color: Colors.primaryDark,
    fontFamily: 'Rubik-Regular',
    marginBottom: moderateScale(16),
  },
  buttonWrapper: {
    color: Colors.white,
    borderRadius: moderateScale(4),
    height: moderateScale(40),
    backgroundColor: Colors.primaryOrange,
    justifyContent: 'center',
  },
  textOnly: {
    color: Colors.white,
    fontSize: moderateScale(16),
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
  },
})
