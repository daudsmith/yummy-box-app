import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import Modal from 'react-native-modal'
import Styles from './BottomModalStyle'

class BottomModal extends Component {
  render() {
    const {
      buttonText,
      dismiss,
      visible,
      title,
      message,
    } = this.props
    return (
      <Modal
        animationIn="slideInUp"
        animationOut="slideOutDown"
        isVisible={visible}
        onBackButtonPress={dismiss}
        onBackdropPress={dismiss}
        style={Styles.modalStyle}
      >
        <View
          style={Styles.modalContainer}
        >
          <View>
            <Text
              style={Styles.title}
            >
              {title}
            </Text>
          </View>
          <View
          >
            <Text
              style={Styles.message}
            >
              {message}
            </Text>
          </View>
          <View
            style={Styles.buttonWrapper}
          >
            <TouchableOpacity
              onPress={dismiss}
            >
              <Text
                style={Styles.textOnly}
              >
                {buttonText}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }
}

BottomModal.defaultProps = {
  visible: false,
}

BottomModal.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  dismiss: PropTypes.func.isRequired,
  visible: PropTypes.bool,
}

export default BottomModal
