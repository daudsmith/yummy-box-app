import React from 'react'
import {Modal, TouchableOpacity, View, Image, Text, Linking} from 'react-native'
import YummyboxIcon from './YummyboxIcon'
import Button from './Common/Button'
import Colors from '../Themes/Colors'
import Images from '../Themes/Images'
import Styles from './Styles/SalesOrderResultModalStyle'

export type SalesOrderModalType = 'thank-you' | 'sorry'
interface SalesOrderResultModalProps {
  modalType: SalesOrderModalType
  visible: boolean
  text?: string
  invoiceUrl?: string
  onPress: () => void
}

const SalesOrderResultModal: React.FC<SalesOrderResultModalProps> = (props) => {
  const {modalType, visible, onPress, invoiceUrl, text} = props
  const modalImage = modalType === 'thank-you' ? Images.ThankYou : Images.Sorry
  const title = modalType === 'thank-you' ? 'Thank You' : 'Sorry'
  const description = modalType === 'thank-you' ? 'for your order.\nBon Appetit!' : 'Something went wrong.\nPlease try again.'
  const textAlert = text === '' || text === undefined ? description : text
  const showButton = modalType === 'thank-you' && invoiceUrl !== null

  const onButtonPress = (invoiceUrl: string) => {
    Linking.canOpenURL(invoiceUrl).then(supported => {
      if (supported) {
        Linking.openURL(invoiceUrl)
      }
    })
  }

  return (
    <Modal
      animationType='slide'
      visible={visible}
      transparent
      onRequestClose={() => {}}
    >
      <View testID='salesOrderResultModal' style={Styles.container}>
        <View style={Styles.modalContainer}>
          <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
            <View style={{flex: 1, alignItems: 'flex-end', marginBottom: 25}}>
              <TouchableOpacity testID="closePay" accessibilityLabel="closePay" onPress={onPress} style={Styles.closeButtonContainer}>
                <YummyboxIcon name='close' size={18} color={Colors.warmGrey} />
              </TouchableOpacity>
            </View>
          </View>
          {showButton && <View style={{height: 13}} />}
          <View style={{justifyContent: 'center', paddingBottom: 54}}>
            <View style={{alignItems: 'center', marginBottom: 29}}>
              <Image source={modalImage} resizeMode='cover' />
              <Text testID="labelThanks" accessibilityLabel="labelThanks" style={Styles.text}>{title}</Text>
              <Text style={[Styles.description, {marginTop: 8, paddingHorizontal: 20}]}>{textAlert}</Text>
            </View>
            {showButton &&
            <Button testID="payNow" accessibilityLabel="payNow" text='Pay Now' onPress={() => onButtonPress(invoiceUrl)} />
            }
          </View>
        </View>
      </View>
    </Modal>
  )
}

export default SalesOrderResultModal
