import * as React from 'react'
import {
  Animated,
  Dimensions,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  View,
  StyleSheet,
  ViewStyle,
} from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { Banner } from '../Redux/PromotionRedux'

const { width } = Dimensions.get('window')

export interface PromotionBannerProps {
  banners: any
  onPress: (bannerId: number) => void
  style?: ViewStyle
}

const PromotionBanner: React.FC<PromotionBannerProps> = (props) => {
  const { banners, onPress, style } = props
  const scrollX = new Animated.Value(0)
  const position = Animated.divide(scrollX, width)

  const renderBanner = (banner: Banner, index: number | string, onPress: (bannerId: number) => void) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => onPress(banner.id)}
        key={index}
        style={{
          borderRadius: 8
        }}
      >
        <View
          style={{
            ...styles.slider,
            borderRadius: 8
          }}
        >
          <Image
            source={{ uri: banner.banner_image }}
            style={{
              ...styles.slider,
              borderRadius: 8
            }}
            resizeMode='cover'
          />
        </View>
      </TouchableWithoutFeedback>
    )
  }

  return (
    <View style={{ backgroundColor: 'transparent' }}>
      <View style={styles.promotionBannerContainer}>
        <FlatList
          style={{...style}}
          data={banners}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item, index }) => renderBanner(item, index, onPress)}
          horizontal={true}
          pagingEnabled={true}
          ItemSeparatorComponent={() => (
            <View
              style={{
                width: scale(8),
                backgroundColor: 'transparent'
              }}
            />
          )}
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }], {
              useNativeDriver: false
            }
          )}
          scrollEventThrottle={16}
        />

        {/* Render Dot */}
        {banners.length > 1 &&
        <View style={styles.dotContainer}>
          {
            banners.map((_, i) => {
              let backgroundColor = position.interpolate({
                inputRange: [
                  i - 0.50000000001,
                  i - 0.5,
                  i,
                  i + 0.5,
                  i + 0.50000000001],
                outputRange: [
                  'transparent',
                  'white',
                  'white',
                  'white',
                  'transparent'],
                extrapolate: 'clamp',
              })
              return (
                <Animated.View
                  key={i}
                  style={{
                    ...styles.dot,
                    backgroundColor: backgroundColor
                  }}
                />
              )
            })}
        </View>
        }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  promotionBannerContainer: {
    flex: 1,
  },
  dotContainer: {
    flexDirection: 'row',
    left: scale(19),
    bottom: verticalScale(10),
  },
  dot: {
    height: verticalScale(6),
    width: scale(6),
    marginHorizontal: scale(5),
    borderRadius: 5,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'white',
  },
  slider: {
    width: scale(311),
    height: verticalScale(140),
  },
})

export default PromotionBanner
