import React from 'react'
import { View, Text, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import PhoneInput from 'react-native-phone-input'
import {Colors} from '../Themes/'
import { scale } from 'react-native-size-matters'

class PhoneNumberTextInput extends React.Component {
  phone
  constructor (props) {
    super(props)
    this.state = {
      borderColor: Colors.lightGreyX
    }
  }
  checkIsValidNumber () {
    return this.phone.isValidNumber()
  }

  onChangePhoneNumber (phone) {
    const isValidNumber = this.checkIsValidNumber()
    this.setState({borderColor: isValidNumber ? Colors.green : Colors.red})
    const countryCode = this.phone.getDialCode()
    this.props.onChange(phone, countryCode, isValidNumber)
  }

  render () {
    const {value} = this.props
    const {borderColor} = this.state

    return (
      <View>
        <View style={Styles.titleContainer}>
          <Text style={Styles.title}>Mobile Number</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', borderBottomColor: this.state.borderColor, borderBottomWidth: 1}}>
          <PhoneInput
            ref={ref => {
              this.props.setRef(ref)
              this.phone = ref
            }}
            value={value}
            initialCountry={'id'}
            onChangePhoneNumber={(phone) => this.onChangePhoneNumber(phone)}
            returnKeyType='next'
            style={{
              backgroundColor: 'white',
              paddingBottom: 6,
              paddingTop: 9,
              paddingHorizontal: 5,
            }}
            textStyle={{
              color: Colors.greyishBrown,
              fontSize: scale(16),
            }}
            textProps={{
              testID: this.props.testID,
              accessibilityLabel: this.props.accessibilityLabel,
            }}
          />
        </View>
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <Icon name='close' size={10} color={borderColor === Colors.red ? Colors.red : 'white'} />
          <Text style={{fontSize: 10, color: borderColor === Colors.red ? Colors.red : 'white', fontFamily: 'Rubik-Regular', marginLeft: 6}}>Invalid phone number</Text>
        </View>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  titleContainer: {
    marginBottom: 13
  },
  title: {
    fontFamily: 'Rubik-Medium',
    color: Colors.warmGrey,
    fontSize: 14
  }
})

export default PhoneNumberTextInput
