import React, {Component} from 'react'
import {View, Text, TouchableWithoutFeedback, StyleSheet} from 'react-native'
import {moderateScale} from 'react-native-size-matters'
import LocaleFormatter from '../Services/LocaleFormatter'
import PropTypes from 'prop-types'
import Colors from '../Themes/Colors'
import FastImage from 'react-native-fast-image'

class MMPDeliveryItem extends Component {
  static propTypes = {
    item: PropTypes.object.isRequired,
    subComponentOnPress: PropTypes.func,
    subComponentText: PropTypes.string,
    hideQuantity: PropTypes.bool,
    hidePrice: PropTypes.bool,
    disableEdit: PropTypes.bool
  }

  render () {
    const {item} = this.props
    const foodData = item.detail.data
    const images = foodData.images.hasOwnProperty('data') ? foodData.images.data[0].thumbs.medium_thumb : foodData.images.medium_thumb
    const disableEdit = this.props.disableEdit

    return (
      <View style={Styles.itemRowContainer}>
        <View style={{marginRight: moderateScale(13)}}>
          <FastImage
            style={{height: moderateScale(78), width: moderateScale(78), borderRadius: 5}}
            source={{
              uri: images,
              priority: FastImage.priority.normal,
            }}
          />
        </View>
        <View style={Styles.itemRowDataContainer}>
          <Text style={Styles.foodNameText}>{foodData.name}</Text>

          <View style={{flexDirection: 'row'}}>
            {!this.props.hidePrice &&
              <Text style={Styles.priceText}>{LocaleFormatter.numberToCurrency(item.unit_price)}</Text>
            }
            {!this.props.hideQuantity &&
              <Text style={Styles.quantityText}>{`x${item.quantity}`}</Text>
            }
          </View>
        </View>
        {
          (typeof this.props.subComponentOnPress !== 'undefined' && disableEdit === false)
          ? (
            <View style={{flex: 1, paddingTop: moderateScale(3), alignItems: 'flex-end'}}>
              <TouchableWithoutFeedback 
                testID='changeProduct'
                accessibilityLabel='changeProduct'
                onPress={() => this.props.subComponentOnPress()}>
                <Text style={Styles.subComponentButtonText}>{this.props.subComponentText}</Text>
              </TouchableWithoutFeedback>
            </View>
          ) : null
        }
        <View/>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  itemRowContainer: {
    flexDirection: 'row',
    marginBottom: moderateScale(15)
  },
  itemRowDataContainer: {
    flex: 3,
    justifyContent: 'space-between',
    paddingTop: moderateScale(3),
    paddingBottom: moderateScale(7)
  },
  foodNameText: {
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(18),
    color: Colors.greyishBrownTwo
  },
  priceText: {
    flex:1,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.warmGreyTwo
  },
  quantityText: {
    flex:1,
    textAlign: 'right',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    color: Colors.greyishBrown
  },
  subComponentContainer: {

  },
  subComponentButtonText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.green,
    fontSize: moderateScale(14)
  }
})

export default MMPDeliveryItem
