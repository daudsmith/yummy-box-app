import React, {Component} from 'react'
import {View, Text, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, Keyboard} from 'react-native'
import RadioButton from 'react-native-radio-button'
import {createIconSetFromIcoMoon} from 'react-native-vector-icons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types'
import { LiteCreditCardInput } from '../../Lib/react-native-credit-card-input-fullpage'
import Colors from '../../Themes/Colors'
import icoMoonConfig from '../../Images/SvgIcon/selection.json'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)
const cardTypes = {
  'visa': 'VISA',
  'master-card': 'MASTER',
  'american-express': 'AMEX'
}
const cardIcons = {
  'VISA': 'visa',
  'MASTER': 'mastercard',
  'AMEX': 'amex',
  'JCB': 'jcb',
  'UNKNOWN': 'cc-normal'
}

class CreditCardPicker extends Component {
  static propTypes = {
    selectedCardId: PropTypes.number,
    cards: PropTypes.array.isRequired,
    transactionType: PropTypes.string
  }
  constructor (props) {
    super(props)
    this.state = {
      showCreditCardForm: this.props.cards.length <= 0,
      transactionType: props.transactionType || 'item',
      type: props.type || 'checkout'
    }
  }

  componentDidMount() {
    const {selectedCardId, cards} = this.props
    if (this.state.showCreditCardForm === false && selectedCardId === null) {
      cards.map((card) => {
        if (card.is_default === 1) {
          this.props.setCreditCardAsPaymentInfo(card, false)
        }
      })
    }
  }

  isCardSelected (card) {
    const {showCreditCardForm} = this.state
    const {selectedCardId} = this.props
    if (showCreditCardForm === true) {
      return false
    }
    if (selectedCardId === null) {
      return card.is_default === 1
    } else {
      return card.id === selectedCardId
    }
  }
  onCreditCardFormChange (form) {
    if (form.valid) {
      const expiries = form.values.expiry.split('/')
      const creditCard = {
        card_number: form.values.number.replace(/ /g, ''),
        card_exp_month: expiries[0],
        card_exp_year: `20${expiries[1]}`,
        card_cvn: form.values.cvc,
        type: cardTypes.hasOwnProperty(form.values.type) ? cardTypes[form.values.type] : 'UNKNOWN',
        is_multiple_use: this.props.saveCreditCard,
        should_authenticate: true,
        token: null,
        saveCreditCard: this.props.saveCreditCard
      }
      Keyboard.dismiss()
      this.props.setCreditCardAsPaymentInfo(creditCard, true)
    } else {
      this.props.setCreditCardAsPaymentInfo(null)
    }
  }
  radioButtonPress (card) {
    this.props.setCreditCardAsPaymentInfo(card, false)
    this.setState({showCreditCardForm: false})
  }
  showCreditCardForm () {
    this.setState({showCreditCardForm: true})
    this.props.setCreditCardAsPaymentInfo(null)
  }
  renderCheckBox () {
    const checkBoxBorderColor = this.props.saveCreditCard ? Colors.primaryOrange : Colors.pinkishGrey
    const iconColor = this.props.saveCreditCard ? Colors.primaryOrange : 'transparent'
    return (
      <View style={[Styles.checkBoxContainer, {borderColor: checkBoxBorderColor}]}>
        <FontAwesome name='check' color={iconColor} size={10}  />
      </View>
    )
  }
  renderCreditCardForm () {
    return (
      <View style={Styles.creditCardFormContainer}>
        <LiteCreditCardInput
          onChange={(form) => {
            this.onCreditCardFormChange(form)
          }}
        />
        {this.state.transactionType !== 'subscription' &&
          <TouchableWithoutFeedback testID="saveCC" accessibilityLabel="saveCC" onPress={() => this.props.toggleSaveCreditCard(!this.props.saveCreditCard)}>
            <View style={Styles.saveCreditCardCheckBoxContainer}>
              <View>
                {this.renderCheckBox()}
              </View>
              <View style={{marginLeft: 8}}>
                <Text style={Styles.saveCreditCardText}>Save Credit Card</Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        }
        {(this.state.type === 'create_subscription' || this.state.type === 'modify_subscription_order' || (this.state.type === 'modify_mmp' && this.state.transactionType === 'subscription')) &&
        <View style={{backgroundColor: Colors.lightGrey2, padding: 16, borderRadius: 3, marginTop: 16}}>
          <Text style={{color: Colors.brownishGrey, fontFamily: 'Rubik-Light', fontSize: 12}}>Your card issuer might notify you about a transaction amounting Rp 10.000. Don’t worry, this amount is only a pre-authorization and will not actually be charged.</Text>
        </View>
        }
      </View>
    )
  }
  renderCreditCardRow (card, index) {
    const isBankSelected = this.isCardSelected(card)
    return (
      <TouchableWithoutFeedback key={index} onPress={() => this.radioButtonPress(card)}>
        <View style={Styles.creditCardRowContainer}>
          <RadioButton
            animation={'bounceIn'}
            isSelected={isBankSelected}
            size={10}
            innerColor={Colors.primaryOrange}
            outerColor={isBankSelected ? Colors.primaryOrange : Colors.pinkishGrey}
            onPress={() => this.radioButtonPress(card)}
          />
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', paddingRight: 20}}>
            <YumboxIcon name={cardIcons[card.type]} size={28} color={Colors.greyishBrown} />
            <Text style={Styles.cardNumberText}>**** **** **** {card.ends_with}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
  renderCreditCardList () {
    const {cards} = this.props
    return (
      <View>
        <Text style={Styles.titleText}>Select Credit Card</Text>
        <View style={Styles.cardListContainer}>
          {cards.map((card, index) => {
            return this.renderCreditCardRow(card, index)
          })}
        </View>
      </View>
    )
  }
  render () {
    const {cards} = this.props
    const {showCreditCardForm} = this.state
    return (
      <View style={{marginTop: 30}}>
        {cards.length > 0 && this.renderCreditCardList()}
        {!showCreditCardForm &&
          <TouchableOpacity onPress={() => this.showCreditCardForm()}>
            <Text style={Styles.addNewCardText}>+ Add New Card</Text>
          </TouchableOpacity>
        }
        {showCreditCardForm &&
          this.renderCreditCardForm()
        }
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  titleText: {
    fontSize: 14,
    fontFamily: 'Rubik-Regular',
    color: Colors.brownishGrey,
    textAlign: 'center'
  },
  cardListContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: Colors.lightGreyX,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Colors.lightGreyX,
    borderStyle: 'solid',
    marginTop: 10,
    marginBottom: 16
  },
  creditCardRowContainer: {
    flexDirection: 'row',
    marginVertical: 19,
    alignItems: 'center',
    paddingLeft: 9
  },
  cardNumberText: {
    marginLeft: 12,
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.brownishGrey
  },
  addNewCardText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.primaryOrange,
    fontSize: 14,
    letterSpacing: 0.1,
    textAlign: 'center',
    marginBottom: 27
  },
  creditCardFormContainer: {
    flex: 1,
    width: '100%',
    marginBottom: 33
  },
  saveCreditCardCheckBoxContainer: {
    flexDirection: 'row',
    marginTop: 12
  },
  saveCreditCardText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    color: Colors.brownishGrey
  },
  checkBoxContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    padding: 2,
    height: 16,
    width: 16,
    borderRadius: 2
  }
})

export default CreditCardPicker
