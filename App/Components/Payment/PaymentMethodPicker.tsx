import _ from 'lodash'
import moment, { Moment } from 'moment'
import React, { Component } from 'react'
import { Platform, Text, View } from 'react-native'

import { InitCustomerCard } from '../../Redux/CustomerCardRedux'
import { PaymentMethodOption, SettingDataInterface } from '../../Redux/SettingRedux'
import LocaleFormatter from '../../Services/LocaleFormatter'
import { Images } from '../../Themes'
import BankTransferInformation from './BankTransferInformation'
import CreditCardPicker from './CreditCardPicker'
import PaymentOptionButton from './PaymentOptionButton'
import Styles from './Styles/PaymentMethodPicker'
import YumCreditsInformationBox from './YumCreditsInformationBox'
import {
  MyMealPlanDetail,
  MyMealPlanDetailSuccess,
} from '../../Redux/MyMealPlanRedux'

type PaymentType =
  | 'checkout'
  | 'create_subscription'
  | 'credit-card'
  | 'modify_mmp'
  | 'modify_delivery'
  | 'modify_subscription_order';

export type PaymentOptionType =
  | 'wallet'
  | 'credit-card'
  | 'virtual-account'
  | 'dana'
  | 'shopeepay'
  | 'gopay'
  | {};

interface PaymentMethodPickerPropsInterface {
  navigateToTopUpScreen: () => void;
  wallet: any;
  customerCard: InitCustomerCard;
  setting: SettingDataInterface;
  type?: PaymentType;
  total?: number;
  cartType?: string;
  cartItems?: any;
  itemCutoff?: Moment | string;
  itemCutDay?: number;
  fetchingWallet?: boolean;
  order?: any;
  confirmIsValid: (confirm: boolean) => void;
  setSelectedPayment?: (selectedPayment: PaymentOptionType) => void;
  setNewCreditCardInfo?: (newCreditCardInfo: object) => void;
  setPaymentInfo?: (paymentInfo) => void;
  onBankTransferPress?: () => void;
  onYumcreditsPress?: () => void;
  onCreditCardPress?: () => void;
  onDanaPress?: () => void;
  onShopeePayPress?: () => void;
  onGopayPress?: () => void;
  modifiedOrderDetail?: {
    subscription_raw_data: {
      default_payment_method: string;
      newCreditCardInfo: any;
      default_payment_info: any;
    };
  };
  modifiedMyMealPlan?: MyMealPlanDetailSuccess;
  paymentInfoUpdater?: any;
  myMealPlanDetail?: any;
  modifyMmpSubscriptionPaymentInfo?: any;
  modifyMmpSubscriptionSelectedPayment?: any;
  setMmpNewCreditCardInfo?: any;
  updateSubscriptionDefaultData?: any;
  setSubscriptionNewCreditCard?: any;
}

interface PaymentMethodPickerStateInterface {
  disabledPayments: any;
  saveCreditCard: any;
  transactionType: any;
  type: any;
  payment_methods: PaymentMethodOption;
}

class PaymentMethodPicker extends Component<
  PaymentMethodPickerPropsInterface,
  PaymentMethodPickerStateInterface
> {
  static defaultProps = {
    type: 'checkout',
    paymentInfoUpdater: null,
    confirmIsValid: null
  };

  constructor(props) {
    super(props)
    const { type } = this.props
    const transactionType = this.getTransactionType(type)
    const disabledPayments = this.mapDisabledPaymentMethod(
      transactionType,
      type
    )

    this.state = {
      disabledPayments,
      saveCreditCard: transactionType === 'subscription',
      transactionType,
      type,
      payment_methods: {
        gopay: false,
        shopeepay: false
      }
    }
  }
  UNSAFE_componentWillMount() {
    const { type } = this.state
    if (type === 'checkout' || type === 'create_subscription') {
      this.setSelectedPayment('wallet')
    }

    if (this.props.setting.payment_methods) {
      this.setState({
        payment_methods: JSON.parse(this.props.setting.payment_methods)
      })
    }
    }
  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      this.props.order && this.props.order.selectedPayment === 'wallet' &&
      newProps.fetchingWallet !== this.props.fetchingWallet
    ) {
      this.setYumcreditsAsPaymentInfo(newProps.wallet)
    }
  }
  getTransactionType(type) {
    switch (type) {
      case 'checkout':
      case 'create_subscription':
        return this.props.cartType
      case 'modify_mmp':
      case 'modify_delivery':
        return this.props.modifiedMyMealPlan.type
      case 'modify_subscription_order':
        return 'subscription'
    }
  }
  disableBankTransfer(disabledPayments, type) {
    if (type === 'modify_delivery') {
      return disabledPayments
    }
    if (type === 'checkout') {
      const haveOrderTomorrow = this.checkIfHaveOrderForTomorrow()
      if (haveOrderTomorrow) {
        const cutOffTime = moment({ hour: 0, minute: 0, second: 0 })
          .utcOffset(7)
          .add(1, 'days')
          .subtract(
            this.props.setting.bank_transfer_payment_option_cutoff,
            'seconds'
          )
        disabledPayments = {
          ...disabledPayments,
          bankTransfer: moment()
            .utcOffset(7)
            .isAfter(cutOffTime, 'minute')
        }
      }

      const haveCutoff = this.checkIfHaveCutoff()
      if (haveCutoff) {
        let { itemCutoff, cartItems, itemCutDay } = this.props
        const now = moment().utcOffset(7)
        cartItems.map(cart => {
          itemCutoff = `${cart.date} ${moment(itemCutoff).format('HH:mm:ss')}`
          itemCutoff = moment(itemCutoff)
            .subtract(itemCutDay, 'days')
            .subtract(1, 'hours')
          if (now.isAfter(moment(itemCutoff))) {
            disabledPayments = { ...disabledPayments, bankTransfer: true }
          }
        })
      }
    }
    return disabledPayments
  }
  mapDisabledPaymentMethod(transactionType, type) {
    let disabledPayments = {
      creditCard: false,
      bankTransfer: false,
      yumCredits: false,
      dana: false,
      shopeepay: false,
      gopay: false
    }
    if (transactionType === 'subscription' || type === 'modify_delivery') {
      disabledPayments = {
        ...disabledPayments,
        bankTransfer: true,
        dana: true,
        shopeepay: true,
        gopay: true
      }
    }

    disabledPayments = this.disableBankTransfer(disabledPayments, type)

    return disabledPayments
  }
  checkIfHaveOrderForTomorrow() {
    const { cartItems, cartType } = this.props
    if (cartType === 'item') {
      const mealIndex = _.findIndex(cartItems, cartItem =>
        moment(cartItem.date)
          .utcOffset(7)
          .isSame(
            moment()
              .utcOffset(7)
              .add(1, 'day'),
            'date'
          )
      )
      return mealIndex > -1
    } else {
      const dateIndex = _.findIndex(cartItems[0].packages[0].dates, date =>
        moment(date)
          .utcOffset(7)
          .isSame(
            moment()
              .utcOffset(7)
              .add(1, 'day'),
            'date'
          )
      )
      return dateIndex > -1
    }
  }
  checkIfHaveCutoff = () => {
    const { itemCutoff, cartType } = this.props
    return cartType === 'item' && itemCutoff != null
  };
  setPaymentInfo(paymentInfo) {
    if (this.props.confirmIsValid !== null) {
      this.props.confirmIsValid(!(paymentInfo === null))
    }

    if (this.state.type === 'modify_mmp') {
      this.props.modifyMmpSubscriptionPaymentInfo(paymentInfo)
    } else if (
      this.state.type === 'checkout' ||
      this.state.type === 'create_subscription'
    ) {
      this.props.setPaymentInfo(paymentInfo)
    } else if (this.state.type === 'modify_delivery') {
      this.props.paymentInfoUpdater(paymentInfo)
    } else {
      this.props.updateSubscriptionDefaultData(
        'default_payment_info',
        paymentInfo
      )
    }
  }
  setNewCreditCardInfo(newCreditCardInfo) {
    const { type } = this.state

    if (type === 'checkout' || type === 'create_subscription') {
      this.props.setNewCreditCardInfo(newCreditCardInfo)
    } else if (type === 'modify_mmp' || type === 'modify_delivery') {
      this.props.setMmpNewCreditCardInfo(newCreditCardInfo)
    } else if (type === 'modify_subscription_order') {
      this.props.setSubscriptionNewCreditCard(newCreditCardInfo)
    }
  }
  setSelectedPayment(selectedPayment: PaymentOptionType) {
    if (
      this.state.type === 'checkout' ||
      this.state.type === 'create_subscription'
    ) {
      this.props.setSelectedPayment(selectedPayment)
    } else if (
      this.state.type === 'modify_mmp' ||
      this.state.type === 'modify_delivery'
    ) {
      this.props.modifyMmpSubscriptionSelectedPayment(selectedPayment)
    } else {
      this.props.updateSubscriptionDefaultData(
        'default_payment_method',
        selectedPayment
      )
    }
  }
  setInitialSelectedCreditCard() {
    const { cards } = this.props.customerCard
    if (cards.length > 0) {
      const defaultCardIndex = _.findIndex(
        cards,
        card => card.is_default === 1
      )
      this.setPaymentInfo({
        ...cards[defaultCardIndex],
        is_multiple_use: true
      })
    } else {
      this.setPaymentInfo(null)
    }
    this.setSelectedPayment('credit-card')
  }
  setCreditCardAsPaymentInfo(card, isCardForm) {
    if (card === null) {
      this.setPaymentInfo(null)
      this.setNewCreditCardInfo(null)
    } else {
      const paymentInfo = isCardForm
        ? { ...card }
        : { ...card, status: 'VERIFIED' }
      if (this.props.confirmIsValid !== null) {
        this.props.confirmIsValid(true)
      }
      if (isCardForm) {
        this.setNewCreditCardInfo(paymentInfo)
      } else {
        this.setPaymentInfo({ ...paymentInfo, is_multiple_use: true })
        this.setNewCreditCardInfo(null)
      }
    }
  }
  setYumcreditsAsPaymentInfo(wallet) {
    const { setting } = this.props
    let total = this.props.total
    if (
      this.state.type === 'modify_mmp' ||
      this.state.type === 'modify_delivery'
    ) {
      const { myMealPlanDetail, modifiedMyMealPlan } = this.props
      const modifiedTotal =
        (modifiedMyMealPlan.data as MyMealPlanDetail).delivery_fee + (modifiedMyMealPlan.data as MyMealPlanDetail).subtotal
      const currentTotal =
        myMealPlanDetail.data.delivery_fee + myMealPlanDetail.data.subtotal
      total = modifiedTotal - currentTotal
    }
    if (this.state.type === 'create_subscription') {
      if (wallet >= setting.subscription_minimum_wallet_balance) {
        this.setPaymentInfo({ info: { current_balance: wallet } })
      } else {
        this.setPaymentInfo(null)
      }
    } else if (wallet >= total) {
      this.setPaymentInfo({ info: { current_balance: wallet } })
    } else {
      this.setPaymentInfo(null)
    }
  }
  onPaymentOptionButtonPress(selectedPayment: PaymentOptionType) {
    let currentSelectedPayment: PaymentOptionType
    switch (this.state.type) {
      case 'checkout':
      case 'create_subscription':
        currentSelectedPayment = this.props.order && this.props.order.selectedPayment
        break
      case 'modify_mmp':
      case 'modify_delivery':
        currentSelectedPayment = this.props.modifiedMyMealPlan
          .payment_info_selectedPayment
        break
      case 'dana':
        currentSelectedPayment = this.props.modifiedOrderDetail
          .subscription_raw_data.default_payment_method
        break
      case 'shopeepay':
        currentSelectedPayment = this.props.modifiedOrderDetail
          .subscription_raw_data.default_payment_method
        break
      case 'gopay':
        currentSelectedPayment = this.props.modifiedOrderDetail
          .subscription_raw_data.default_payment_method
        break
      default:
        break
    }
    if (currentSelectedPayment !== selectedPayment) {
      if (selectedPayment === 'credit-card') {
        this.setInitialSelectedCreditCard()
        this.setSelectedPayment(selectedPayment)
      } else if (selectedPayment === 'wallet') {
        this.setYumcreditsAsPaymentInfo(this.props.wallet)
        this.setSelectedPayment(selectedPayment)
      } else {
        this.setSelectedPayment(selectedPayment)
      }
    }
  }
  toggleSaveCreditCard() {
    const { saveCreditCard } = this.state
    let newCreditCardInfo
    switch (this.state.type) {
      case 'checkout':
      case 'create_subscription':
        newCreditCardInfo = this.props.order.newCreditCardInfo
        break
      case 'modify_mmp':
      case 'modify_delivery':
        newCreditCardInfo = this.props.modifiedMyMealPlan.newCreditCardInfo
        break
      default:
        newCreditCardInfo = this.props.modifiedOrderDetail.subscription_raw_data
          .newCreditCardInfo
        break
    }

    this.setState({ saveCreditCard: !saveCreditCard })
    if (newCreditCardInfo !== null) {
      const newPaymentInfo = {
        ...newCreditCardInfo,
        saveCreditCard: !saveCreditCard,
        is_multiple_use: !saveCreditCard
      }
      this.setNewCreditCardInfo(newPaymentInfo)
    }
  }
  getCreditCardId() {
    const { type } = this.state
    if (type === 'checkout' || type === 'create_subscription') {
      if (this.props.order.paymentInfo === null) {
        return null
      } else {
        return this.props.order.paymentInfo.id
      }
    } else if (type === 'modify_mmp' || type === 'modify_delivery') {
      const { payment_info } = this.props.modifiedMyMealPlan
      if (payment_info === null) {
        return null
      }
      if (
        payment_info.hasOwnProperty('payment_info_json') &&
        payment_info.payment_info_json !== ''
      ) {
        const parsedPaymentInfoJSON = JSON.parse(
          this.props.modifiedMyMealPlan.payment_info.payment_info_json
        )
        return isNaN(parsedPaymentInfoJSON.id)
          ? null
          : parsedPaymentInfoJSON.id
      } else {
        return payment_info.id
      }
    } else {
      const payment_info = this.props.modifiedOrderDetail.subscription_raw_data
        .default_payment_info
      if (payment_info === null) {
        return null
      }

      const parsedPaymentInfoJSON =
        typeof payment_info === 'object'
          ? payment_info
          : JSON.parse(
              this.props.modifiedOrderDetail.subscription_raw_data
                .default_payment_info
            )
      return parsedPaymentInfoJSON.hasOwnProperty('id')
        ? parsedPaymentInfoJSON.id
        : null
    }
  }
  renderCreditCardPaymentMethod() {
    const selectedCardId = this.getCreditCardId()
    return (
      <CreditCardPicker
        cards={this.props.customerCard.cards}
        setCreditCardAsPaymentInfo={(card, isCardForm) =>
          this.setCreditCardAsPaymentInfo(card, isCardForm)
        }
        selectedCardId={selectedCardId}
        saveCreditCard={this.state.saveCreditCard}
        toggleSaveCreditCard={() => this.toggleSaveCreditCard()}
        transactionType={this.state.transactionType}
        type={this.state.type}
      />
    )
  }
  renderBankTransferPaymentMethod() {
    return (
      <View style={{ marginTop: 23 }}>
        <BankTransferInformation />
      </View>
    )
  }
  renderYumCreditsPaymentMethod = () => {
    const { modifiedMyMealPlan, modifiedOrderDetail, cartItems } = this.props
    if (modifiedMyMealPlan || modifiedOrderDetail || cartItems) {
      return (
        <YumCreditsInformationBox
          navigateToTopUpScreen={this.props.navigateToTopUpScreen}
          setYumcreditsAsPaymentInfo={() =>
            this.setYumcreditsAsPaymentInfo(this.props.wallet)
          }
          type={this.state.type}
          total={this.props.total}
          wallet={this.props.wallet}
          fetchingWallet={this.props.fetchingWallet}
          modifiedMyMealPlanDetail={modifiedMyMealPlan}
          modifiedOrderDetail={modifiedOrderDetail}
          cartItems={cartItems}
        />
      )
    }
  };
  renderPaymentOptionButton(selectedPayment: PaymentOptionType) {
    const { disabledPayments, payment_methods } = this.state

    const showDisplayOtherPaymentMethod: PaymentMethodOption = {
      shopeepay: payment_methods?.shopeepay || false,
      gopay: payment_methods?.gopay || false
    }

    const isDanaShown = payment_methods.gopay || payment_methods.shopeepay

    const _androidPaymentOption = (
      <>
          <View style={Styles.paymentMethodBox}>
            <PaymentOptionButton
              disable={disabledPayments.yumCredits}
              selected={selectedPayment === 'wallet'}
              icon="wallet"
              firstLineText="YumCredits"
              secondLineText={LocaleFormatter.numberToCurrency(
                this.props.wallet
              )}
              onPress={() => {
                if (typeof this.props.onYumcreditsPress === 'undefined') {
                  this.onPaymentOptionButtonPress('wallet')
                } else {
                  this.props.onYumcreditsPress()
                }
              }}
              testID="yumCredit"
              accessibilityLabel="yumCredit"
            />
            <PaymentOptionButton
              disable={disabledPayments.creditCard}
              selected={selectedPayment === 'credit-card'}
              icon="payment-cc"
              firstLineText="Credit Card"
              onPress={() => {
                if (typeof this.props.onCreditCardPress === 'undefined') {
                  this.onPaymentOptionButtonPress('credit-card')
                } else {
                  this.props.onCreditCardPress()
                }
              }}
              testID="credit"
              accessibilityLabel="credit"
            />
            <PaymentOptionButton
              disable={disabledPayments.bankTransfer}
              selected={selectedPayment === 'virtual-account'}
              icon="transfer-icon"
              firstLineText="Bank"
              secondLineText="Transfer"
              onPress={() => {
                if (
                  typeof this.props.onBankTransferPress === 'undefined'
                ) {
                  this.setPaymentInfo({})
                  this.onPaymentOptionButtonPress('virtual-account')
                } else {
                  this.props.onBankTransferPress()
                }
              }}
              testID="transfer"
              accessibilityLabel="transfer"
            />
            {!isDanaShown && (
              <PaymentOptionButton
                disable={disabledPayments.dana}
                selected={selectedPayment === 'dana'}
                firstLineText="DANA"
                onPress={() => {
                  if (typeof this.props.onDanaPress === 'undefined') {
                    this.setPaymentInfo({})
                    this.onPaymentOptionButtonPress('dana')
                  } else {
                    this.props.onDanaPress()
                  }
                }}
                testID="dana"
                accessibilityLabel="dana"
                imageActive={Images.danaWhite}
                imageDefault={Images.danaGrey}
              />
            )}
          </View>
          <View style={Styles.paymentMethodBox}>
            {isDanaShown &&
              <PaymentOptionButton
                disable={disabledPayments.dana}
                selected={selectedPayment === 'dana'}
                firstLineText="DANA"
                onPress={() => {
                  if (typeof this.props.onDanaPress === 'undefined') {
                    this.setPaymentInfo({})
                    this.onPaymentOptionButtonPress('dana')
                  } else {
                    this.props.onDanaPress()
                  }
                }}
                testID="dana"
                accessibilityLabel="dana"
                imageActive={Images.danaWhite}
                imageDefault={Images.danaGrey}
              />
            }
            {showDisplayOtherPaymentMethod.shopeepay && (
              <PaymentOptionButton
                disable={disabledPayments.shopeepay}
                selected={selectedPayment === 'shopeepay'}
                firstLineText="ShopeePay"
                onPress={() => {
                  if (typeof this.props.onShopeePayPress === 'undefined') {
                    this.setPaymentInfo({})
                    this.onPaymentOptionButtonPress('shopeepay')
                  } else {
                    this.props.onShopeePayPress()
                  }
                }}
                testID="shopeepay"
                accessibilityLabel="shopeepay"
                imageActive={Images.shopeeWhite}
                imageDefault={Images.shopeeGrey}
              />
            )}
            {
              showDisplayOtherPaymentMethod.gopay && (
                <PaymentOptionButton
                  disable={disabledPayments.gopay}
                  selected={selectedPayment === 'gopay'}
                  firstLineText="GoPay"
                  onPress={() => {
                    if (typeof this.props.onGopayPress === 'undefined') {
                      this.setPaymentInfo({})
                      this.onPaymentOptionButtonPress('gopay')
                    } else {
                      this.props.onGopayPress()
                    }
                  }}
                  testID="gopay"
                  accessibilityLabel="gopay"
                  imageActive={Images.gopayWhite}
                  imageDefault={Images.gopayGrey}
                />
              )
            }
          </View>
        </>
    )

    const _iosPaymentOption = (
      <>
        <View style={Styles.paymentMethodBox}>
          <PaymentOptionButton
            disable={disabledPayments.yumCredits}
            selected={selectedPayment === 'wallet'}
            icon="wallet"
            firstLineText="YumCredits"
            secondLineText={LocaleFormatter.numberToCurrency(
              this.props.wallet
            )}
            onPress={() => {
              if (typeof this.props.onYumcreditsPress === 'undefined') {
                this.onPaymentOptionButtonPress('wallet')
              } else {
                this.props.onYumcreditsPress()
              }
            }}
            testID="yumCredit"
            accessibilityLabel="yumCredit"
          />
          <PaymentOptionButton
            disable={disabledPayments.creditCard}
            selected={selectedPayment === 'credit-card'}
            icon="payment-cc"
            firstLineText="Credit Card"
            onPress={() => {
              if (typeof this.props.onCreditCardPress === 'undefined') {
                this.onPaymentOptionButtonPress('credit-card')
              } else {
                this.props.onCreditCardPress()
              }
            }}
            testID="credit"
            accessibilityLabel="credit"
          />
          <PaymentOptionButton
            disable={disabledPayments.bankTransfer}
            selected={selectedPayment === 'virtual-account'}
            icon="transfer-icon"
            firstLineText="Bank"
            secondLineText="Transfer"
            onPress={() => {
              if (typeof this.props.onBankTransferPress === 'undefined') {
                this.setPaymentInfo({})
                this.onPaymentOptionButtonPress('virtual-account')
              } else {
                this.props.onBankTransferPress()
              }
            }}
            testID="transfer"
            accessibilityLabel="transfer"
          />
          <PaymentOptionButton
            disable={disabledPayments.dana}
            selected={selectedPayment === 'dana'}
            firstLineText="DANA"
            onPress={() => {
              if (typeof this.props.onDanaPress === 'undefined') {
                this.setPaymentInfo({})
                this.onPaymentOptionButtonPress('dana')
              } else {
                this.props.onDanaPress()
              }
            }}
            testID="dana"
            accessibilityLabel="dana"
            imageActive={Images.danaWhite}
            imageDefault={Images.danaGrey}
          />
        </View>
      </>
    )

    return Platform.OS === 'android' ? _androidPaymentOption : _iosPaymentOption

  }
  renderPaymentMethodDetail(selectedPayment: PaymentOptionType) {
    if (selectedPayment === 'credit-card') {
      return this.renderCreditCardPaymentMethod()
    } else if (selectedPayment === 'virtual-account') {
      return this.renderBankTransferPaymentMethod()
    } else if (selectedPayment === 'wallet') {
      return this.renderYumCreditsPaymentMethod()
    }
  }
  getPaymentMethodSwitcher() {
    switch (this.state.type) {
      case 'create_subscription':
        return this.props.order.selectedPayment === null
          ? 'credit-card'
          : this.props.order.selectedPayment
      case 'checkout':
        return this.props.order.selectedPayment
      case 'modify_mmp':
        return this.props.modifiedMyMealPlan
          ? this.props.modifiedMyMealPlan.selectedPayment
          : 'wallet'
      case 'modify_delivery':
        return this.props.modifiedMyMealPlan
          ? this.props.modifiedMyMealPlan.selectedPayment === 'virtual-account'
            ? 'wallet'
            : this.props.modifiedMyMealPlan.selectedPayment
          : 'wallet'
      case 'modify_subscription_order':
        return this.props.modifiedOrderDetail.subscription_raw_data
          .default_payment_method
      default:
        return this.props.order.selectedPayment
    }
  }
  render() {
    const selectedPayment = this.getPaymentMethodSwitcher()
    return (
      <View style={{ marginTop: 38 }}>
        <Text style={Styles.titleText}>Payment Method</Text>
        {this.renderPaymentOptionButton(selectedPayment)}
        {this.renderPaymentMethodDetail(selectedPayment)}
      </View>
    )
  }
}
export default PaymentMethodPicker
