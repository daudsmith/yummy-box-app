import * as React from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  StyleSheet,
  Image,
  ImageSourcePropType,
} from 'react-native'
import Colors from '../../Themes/Colors'
import YummyboxIcon from '../YummyboxIcon'
import {scale, verticalScale} from 'react-native-size-matters'

const { width } = Dimensions.get('window')
const buttonSize = (width - (20 * 2) - (3 * 6)) / 4

export interface PaymentOptionButtonInterface {
  disable?: boolean
  selected: boolean
  icon?: string
  firstLineText: string
  secondLineText?: string
  imageDefault?: ImageSourcePropType
  imageActive?: ImageSourcePropType
  onPress: () => void
  testID?: string
  accessibilityLabel?: string
}

const PaymentOptionButton: React.FC<PaymentOptionButtonInterface> = (props) => {
  const {
    disable,
    selected,
    icon,
    firstLineText,
    secondLineText,
    imageDefault,
    imageActive,
    onPress,
    testID,
    accessibilityLabel,
  } = props
  const buttonStyle = disable
    ? Styles.disableButtonContainer
    : selected
      ? Styles.selectedButtonContainer
      : Styles.unselectedButtonContainer
  const iconColor = disable
    ? 'white'
    : selected
      ? 'white'
      : Colors.pinkishGrey
  const textColor = selected
    ? Colors.primaryOrange
    : Colors.pinkishGrey
  const shadow = !selected && !disable
    ? Styles.buttonShadow
    : {}
  return (
    <View style={Styles.container}>
      <TouchableOpacity
        testID={testID}
        accessibilityLabel={accessibilityLabel}
        disabled={disable}
        style={{
          ...Styles.buttonContainer,
          ...buttonStyle,
          ...shadow,
        }}
        onPress={onPress}
      >
        {icon && (
          <YummyboxIcon
            name={icon}
            size={40}
            color={iconColor}
          />
        )}
        {imageDefault && !selected && (
          <Image
            source={imageDefault}
            resizeMode="contain"
            style={Styles.imageStyle}
          />
        )}
        {selected && imageActive && (
          <Image
            source={imageActive}
            resizeMode="contain"
            style={Styles.imageStyle}
          />
        )}
      </TouchableOpacity>
      <View style={{ marginTop: 6 }}>
        <Text
          style={{
            ...Styles.buttonText,
            color: textColor,
          }}
        >
          {firstLineText}
        </Text>
        {secondLineText && (
          <Text
            style={{
              ...Styles.buttonText,
              color: textColor,
            }}
          >
            {secondLineText}
          </Text>
        )}
      </View>
    </View>
  )
}

const Styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  buttonContainer: {
    borderWidth: 1,
    borderRadius: 3,
    height: buttonSize,
    width: buttonSize,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disableButtonContainer: {
    backgroundColor: Colors.lightGrey2,
    borderColor: Colors.lightGrey2,
  },
  selectedButtonContainer: {
    backgroundColor: Colors.primaryOrange,
    borderColor: Colors.primaryOrange,
  },
  unselectedButtonContainer: {
    backgroundColor: 'white',
    borderColor: 'white',
  },
  buttonText: {
    textAlign: 'center',
    fontSize: scale(12),
    fontFamily: 'Rubik-Regular',
    flexWrap: 'wrap',
    lineHeight: verticalScale(16)
  },
  buttonShadow: {
    shadowColor: Colors.darkBlue,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4,
    elevation: 3,
  },
  imageStyle: {
    width: '80%',
  }
})

export default PaymentOptionButton
