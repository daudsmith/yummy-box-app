import {StyleSheet} from 'react-native'
import Colors from '../../../Themes/Colors'

export default StyleSheet.create({
  titleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: Colors.greyishBrownThree,
    textAlign: 'center'
  },
  paymentButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  paymentMethodBox: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    marginTop: 11
  },
  paymentBankTransferTitle: {
    fontFamily: 'Rubik-Regular',
    textAlign: 'center',
    fontSize: 14,
    color: Colors.brownishGrey
  },
  paymentBankPickerContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderTopColor: Colors.lightGreyX,
    borderBottomColor: Colors.lightGreyX,
    marginTop: 12,
    marginBottom: 20
  },
  paymentBankTransferDescriptionText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    lineHeight: 16,
    color: Colors.brownishGrey,
    marginBottom: 44
  }
})
