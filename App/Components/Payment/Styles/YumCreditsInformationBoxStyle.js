import {StyleSheet} from 'react-native'
import Colors from '../../../Themes/Colors'

const Styles = {
  container: {
    paddingHorizontal: 15,
    paddingTop: 17,
    paddingBottom: 28,
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 3,
    borderColor: Colors.lightGreyX,
    marginTop: 20,
    marginBottom: 20
  },
  balanceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 9
  },
  balanceTitleText: {
    fontFamily: 'Rubik-Light',
    color: Colors.pinkishGrey,
    fontSize: 12,
    marginRight: 6
  },
  balanceValueText: {
    fontFamily: 'Rubik-Medium',
    color: Colors.pinkishGrey,
    fontSize: 12
  },
  paymentSectionContainer: {
    flexDirection: 'row',
    marginBottom: 13
  },
  paymentTitleText: {
    flex: 1,
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    color: Colors.brownishGrey
  },
  bottomContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: Colors.pinkishGrey
  },
  creditsAfterPaymentContainer: {
    flexDirection: 'row',
    marginTop: 13
  },
  creditsAfterPaymentTitleText: {
    flex: 1,
    fontFamily: 'Rubik-Light',
    color: Colors.greyishBrown,
    fontSize: 12
  },
  creditsValueText: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.greyishBrown
  },
  topUpButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.grennBlue,
    textDecorationLine: 'underline'
  }
}

export default Styles