import { TextStyle, ViewStyle } from 'react-native'
interface styleObject {
  container: ViewStyle
  balanceContainer: ViewStyle
  balanceTitleText: TextStyle
  balanceValueText: TextStyle
  paymentSectionContainer: ViewStyle
  paymentTitleText: TextStyle
  bottomContainer: ViewStyle
  creditsAfterPaymentContainer: ViewStyle
  creditsAfterPaymentTitleText: TextStyle
  creditsValueText: TextStyle
  topUpButtonText: TextStyle
}
declare const Styles: styleObject

export default Styles