import _ from 'lodash'
import React from 'react'
import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native'
import PropTypes from 'prop-types'
import LocaleFormatter from '../../Services/LocaleFormatter'
import Colors from '../../Themes/Colors'
import YummyboxIcon from '../YummyboxIcon'
import Styles from './Styles/YumCreditsInformationBoxStyle'

const YumCreditsInformationBox = (props) => {
  let type = 'subscription'
  let grandTotal = props.total
  switch (props.type) {
    case 'checkout':
      type = props.cartType
      break
    case 'modify_mmp':
      type = props.modifiedMyMealPlanDetail.type
      break
    case 'modify_delivery':
      type = 'modify_delivery'
      grandTotal = props.modifiedMyMealPlanDetail.payment_info.total_amount_charged
      break
  }

  return (
    <View style={Styles.container}>
      <View style={Styles.balanceContainer}>
        <YummyboxIcon name='payment-yumCredits' color={Colors.pinkishGrey} size={20} style={{marginRight: 6}} />
        <Text style={Styles.balanceTitleText}>Balance</Text>
        {props.fetchingWallet
        ? <ActivityIndicator />
        : <Text style={Styles.balanceValueText}>{LocaleFormatter.numberToCurrency(props.wallet)}</Text>
        }
      </View>
      {type !== 'subscription' &&
        <View style={Styles.paymentSectionContainer}>
          <Text style={Styles.paymentTitleText}>Payment</Text>
          {props.fetchingWallet
          ? <ActivityIndicator />
          : <Text style={Styles.creditsValueText}>{LocaleFormatter.numberToCurrency(grandTotal)}</Text>
          }
        </View>
      }
      <View style={Styles.bottomContainer}>
        {!props.fetchingWallet && renderBottomContent(type, props)}
        <TouchableOpacity style={{marginTop: 20}} onPress={props.navigateToTopUpScreen}>
          <Text style={Styles.topUpButtonText}>Top Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
  
const getSubscriptionMinimumYumcreditsBalance = (props) => {
  const {subscriptionMinimumWalletBalance, modifiedMyMealPlanDetail, modifiedOrderDetail} = props
  let subscriptionFirstItemPrice = null
  if (props.cartItems !== undefined && typeof props.cartItems[0].first_item !== 'undefined') {
    subscriptionFirstItemPrice = props.cartItems[0].first_item.price
  } else if (!_.isEmpty(modifiedMyMealPlanDetail)) {
    // modify from my meal plans
    subscriptionFirstItemPrice = modifiedMyMealPlanDetail.data.items.data.subtotal
  } else {
    // modify from my order
    subscriptionFirstItemPrice = modifiedOrderDetail.schedules.length !== 0 && modifiedOrderDetail.schedules[0].item.data.subtotal
  }
  const firstItemPriceBelowMinimumWalletBalance = subscriptionFirstItemPrice < subscriptionMinimumWalletBalance
  return firstItemPriceBelowMinimumWalletBalance ? subscriptionMinimumWalletBalance : subscriptionFirstItemPrice
}

const renderInsuficientYumCredits = (props, withValue, grandTotal = 0, wallet = 0) => {
  const difference = wallet - grandTotal
  const differenceColor = difference < -1 ? {color: 'red'} : {}
  return (
    <View style={Styles.creditsAfterPaymentContainer}>
      {withValue ? (
        <Text style={[Styles.creditsAfterPaymentTitleText, {color: 'red'}]}>You don't have enough YumCredits. Please Top Up.</Text>
      ) : (
        <Text style={[Styles.creditsAfterPaymentTitleText]}>{`To use YumCredits for a subscription you need a minimum balance of ${LocaleFormatter.numberToCurrency(getSubscriptionMinimumYumcreditsBalance(props))} balance. Please Top Up.`}</Text>
      )}
      {withValue &&
        <Text style={[Styles.creditsValueText, differenceColor]}>{LocaleFormatter.numberToCurrency(Math.abs(difference))}</Text>
      }
    </View>
  )
}
const renderYumCreditsAfterPayment = (total, wallet) => {
  return (
    <View style={Styles.creditsAfterPaymentContainer}>
      <Text style={Styles.creditsAfterPaymentTitleText}>YumCredits after payment</Text>
      <Text style={Styles.creditsValueText}>{LocaleFormatter.numberToCurrency(wallet - total)}</Text>
    </View>
  )
}

const renderSubscriptionYumCreditsInformation = () => {
  return (
    <View style={Styles.creditsAfterPaymentContainer}>
      <Text style={Styles.creditsAfterPaymentTitleText}>
        We will deduct your YumCredits a day before for each of your scheduled deliveries
      </Text>
    </View>
  )
}

const renderBottomContent = (type, props) => {
  const {total, wallet} = props
  if (type === 'subscription') {
    if (wallet < getSubscriptionMinimumYumcreditsBalance(props)) {
      return renderInsuficientYumCredits(props, false)
    } else {
      return renderSubscriptionYumCreditsInformation()
    }
  } else {
    let grandTotal = total
    if (type === 'modify_delivery') {
      grandTotal = props.modifiedMyMealPlanDetail.payment_info.total_amount_charged
    }
    if (grandTotal <= wallet) {
      return renderYumCreditsAfterPayment(grandTotal, wallet)
    } else {
      return renderInsuficientYumCredits(props, true, grandTotal, wallet)
    }
  }
}

YumCreditsInformationBox.propTypes = {
  navigateToTopUpScreen: PropTypes.func.isRequired,
  total: PropTypes.number,
  type: PropTypes.string.isRequired,
  cartType: PropTypes.string,
  modifiedMyMealPlanDetail: PropTypes.object,
  fetchingWallet: PropTypes.bool,
  wallet: PropTypes.number,
  cartItems: PropTypes.array,
  setYumcreditsAsPaymentInfo: PropTypes.func,
  setting: PropTypes.object,
  order: PropTypes.object,
  modifiedOrderDetail: PropTypes.object,
}

export default YumCreditsInformationBox
