import React, {Component} from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'
import Images from '../../Themes/Images'

const bankLogoNodes = ['topupLogoBRI', 'topupLogoMANDIRI', 'topupLogoBCA', 'topupLogoBNI']

class BankTransferInformation extends Component {
  renderLogoGrid (image, marginStyle) {
    return (
      <View style={{...Styles.grid, ...marginStyle}}>
        <Image source={Images[image]} style={{width: '100%', height: bankLogoNodes.length < 4 ? 25 : 33}} resizeMode='contain' />
      </View>
    )
  }

  renderBanksLogo () {
    if (bankLogoNodes.length < 4) {
      const gridWidth = 100 / bankLogoNodes.length
      return bankLogoNodes.map((_, index) => {
        return (
          <View key={index} style={[Styles.rowBankContainer, {width: `${gridWidth}%`, marginRight: 5}]}>
            {this.renderLogoGrid(bankLogoNodes[index], {})}
          </View>
        )
      })
    } else {
      return bankLogoNodes.map((_, index) => {
        if (index % 2 === 0) {
          return (
            <View key={index} style={Styles.rowBankContainer}>
              {this.renderLogoGrid(bankLogoNodes[index], {marginRight: 7})}
              {this.renderLogoGrid(bankLogoNodes[index + 1], {})}
            </View>
          )
        }
      })
    }
  }

  render () {
    return (
      <View style={Styles.container}>
        {bankLogoNodes.length < 4 ? (
          <View style={{flex: 1, flexDirection: 'row'}}>
            {this.renderBanksLogo()}
          </View>
        ) : (
          this.renderBanksLogo()
        )}
        <Text style={Styles.description}>
          Bank transfer payments can be made via ATM, Internet banking, mobile banking or Klik-pay. Orders will only be processed once we receive confirmation of payment. Once you submit this form, you'll be directed to your bank portal to complete the payment process.
        </Text>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    paddingTop: 17,
    paddingHorizontal: 18,
    paddingBottom: 27,
    marginBottom: 41,
    backgroundColor: Colors.whiteTwo
  },
  description: {
    fontFamily: 'Rubik-Regular',
    fontSize: 9,
    fontWeight: 'normal',
    color: Colors.brownishGrey,
    lineHeight: 13
  },
  rowBankContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 7
  },
  grid: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 20,
    backgroundColor: Colors.lightGrey2
  }
})

export default BankTransferInformation
