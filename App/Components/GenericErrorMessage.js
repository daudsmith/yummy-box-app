import React, {Component} from 'react'
import {View, Text, TouchableWithoutFeedback, StyleSheet} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

class GenericErrorMessage extends Component {
  render () {
    return (
      <View style={{backgroundColor: 'rgba(255, 0, 0, 0.9)', borderRadius: 3}}>
        <View style={Styles.container}>
        <TouchableWithoutFeedback onPress={() => this.props.close()}>
          <View style={{flex: 1}}>
            <FontAwesome name='close' size={18} color='white' />
          </View>
        </TouchableWithoutFeedback>
        <View style={{flex: 8}}>
          <Text style={Styles.boldError}>Error: <Text style={Styles.error}>{this.props.error}</Text></Text>
        </View>
      </View>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 19,
    borderRadius: 3,
    backgroundColor: 'rgba(255, 255, 255, 0.3)'
  },
  boldError: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 14
  },
  error: {
    fontWeight: 'normal',
    color: 'white',
    fontSize: 14
  }
})

export default GenericErrorMessage