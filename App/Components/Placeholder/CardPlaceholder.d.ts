import React from "react";

export interface cardPlaceholderProps {
    style?: object
    width?: string | number
    height?: string | number
}

const CardPlaceholder: React.FC<cardPlaceholderProps>
export default CardPlaceholder
