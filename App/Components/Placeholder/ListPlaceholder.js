import React from 'react'
import { View, StyleSheet, ViewPropTypes } from 'react-native'

import { Line } from '../Animation'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
})

const ListPlaceholder = ({ style }) => (
  <View style={[styles.container, style]}>
    <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
      <View style={{ padding: 10 }}>
        <Line height={60} width={60} />
      </View>
      <View>
        <Line height={12} width="50%" />
        <Line height={12} width="70%" />
      </View>
    </View>
    <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
      <View style={{ padding: 10 }}>
        <Line height={60} width={60} />
      </View>
      <View>
        <Line height={12} width="50%" />
        <Line height={12} width="70%" />
      </View>
    </View>
  </View>
)

ListPlaceholder.propTypes = {
  style: ViewPropTypes.style,
}

export default ListPlaceholder
