import React from 'react'
import { View, StyleSheet, ViewPropTypes, } from 'react-native'
import PropTypes from 'prop-types'

import { Line } from '../Animation'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
  },
})

const CardPlaceholder = ({
  style,
  width,
  height,
}) => {
  return (
    <View style={[styles.container, style]}>
      <Line width={width} height={height} style={{ borderRadius: 8 }} />
    </View>
  )
}

CardPlaceholder.propTypes = {
  style: ViewPropTypes.style,
  width: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  height: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
}

export default CardPlaceholder
