import * as React from 'react'
import { Text, View } from 'react-native'
import Hyperlink from 'react-native-hyperlink'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import PrimaryTabBar from '../Components/PrimaryTabBar'
import LocaleFormatter from '../Services/LocaleFormatter'
import Colors from '../Themes/Colors'
import NumberedList from './NumberedList'
import { ScrollableTabViewContent } from './Common/ScrollableTabViewContent'
import styles from './Styles/PaymentInstructionStyles'
import { instructionProps } from './PaymentInstructionMandiri'

const PaymentInstructionBca: React.FC<instructionProps> = ({accountNumber}) => {

  return (
    <ScrollableTabView
      initialPage={0}
      renderTabBar={() => (
        <PrimaryTabBar
          tabStyle={styles.tabStyle}
          tabButtonStyle={styles.tabButtonStyle}
          tabTitleStyle={styles.tabTitleStyle}
        />
      )}
    >
      <ScrollableTabViewContent tabLabel='ATM' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Text style={styles.text}>Insert the card, select the language and then enter your PIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "Other Menu" and select "Transfer"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Select "Savings" and "BCA Virtual Account"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter the Virtual Account number <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text> and the amount you want to pay</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Check the transaction data and press "YES"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredit balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
      <ScrollableTabViewContent tabLabel='KlikBCA' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Hyperlink linkDefault={true}><Text style={styles.text}>Login to <Text style={{color: Colors.grennBlue}}>https://klikbca.com</Text>, enter your USER ID and PASSWORD</Text></Hyperlink>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "TRANSFER" and select "Transfer to Virtual Account"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Enter the name and number of your Virtual Account <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text></Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter top up amount</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Follow the instructions to complete your transaction</Text>}
          />
        </View>
      </ScrollableTabViewContent>
      <ScrollableTabViewContent tabLabel='BCA Mobile' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Text style={[styles.text]}>Login to BCA Mobile Banking, enter your USER ID and MPIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "M Transfer" and select "Transfer to BCA Virtual Account"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Enter virtual account <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text></Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter top up amount</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Enter your m-BCA PIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredit balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
    </ScrollableTabView>
  )
}

export default PaymentInstructionBca
