import React, { Component } from 'react'
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Modal from 'react-native-modal'

import {Colors} from '../Themes'

export default class ModalSuccessOne extends Component {
  render () {
    let {onPress, message, showModal, testID, accessibilityLabel} = this.props
    return (
      <View>
        <Modal isVisible={showModal} style={Styles.modalConfirmationContainer}>
          <View style={Styles.modalSuccessBox}>
            <FontAwesome
              name={'check'}
              color={Colors.bloodOrange}
              size={36}
              style={{alignSelf: 'center'}}
            />

            <Text style={Styles.modalSuccessh1}>SUCCESS</Text>
            <Text style={Styles.modalSuccessP}>{message}</Text>
            <TouchableOpacity 
              style={Styles.button4} 
              testID={testID} 
              accessibilityLabel={accessibilityLabel} 
              onPress={() => onPress()}
            >
              <Text style={Styles.button4Text}>Done</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    )
  }
}

let Styles = StyleSheet.create({
  modalConfirmationContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalSuccessBox: {
    width: 280,
    height: 238,
    backgroundColor: '#fff',
    borderRadius: 10,
    paddingTop: 36
  },
  modalSuccessh1: {
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    color: Colors.greyishBrownTwo,
    marginTop: 9,
    alignSelf: 'center',
    letterSpacing: 1.6
  },
  modalSuccessP: {
    fontFamily: 'Rubik-Light',
    fontSize: 14,
    color: Colors.greyishBrownTwo,
    marginTop: 4,
    alignSelf: 'center',
    textAlign: 'center'
  },
  button4: {
    backgroundColor: Colors.bloodOrange,
    borderRadius: 21,
    width: 76,
    height: 35,
    marginTop: 27,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button4Text: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: '#fff'
  }
})
