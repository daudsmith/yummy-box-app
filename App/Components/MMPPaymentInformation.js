import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../Images/SvgIcon/selection.json'
import LocaleFormatter from '../Services/LocaleFormatter'
import Colors from '../Themes/Colors'
import { Images } from '../Themes'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)
const cardIcons = {
  'VISA': 'visa',
  'MASTER': 'mastercard',
  'MASTERCARD': 'mastercard',
  'AMEX': 'amex',
  'JCB': 'jcb',
  'UNKNOWN': Images.unknownCard
}

class MMPPaymentInformation extends Component {
  static propTypes = {
    paymentMethodType: PropTypes.string.isRequired,
    cardImage: PropTypes.string,
    creditCardNumber: PropTypes.string,
    amount: PropTypes.number
  }
  getCreditCardImage (paymentMethodType, rawPaymentInfoJson) {
    if (paymentMethodType === 'credit-card') {
      const paymentInfoJSON = JSON.parse(rawPaymentInfoJson)
      if (paymentInfoJSON.hasOwnProperty('type')) {
        const cardBrand = paymentInfoJSON.type
        return cardIcons[cardBrand]
      } else if (paymentInfoJSON.hasOwnProperty('card_brand')) {
        const cardBrand = paymentInfoJSON.card_brand
        return cardIcons[cardBrand]
      } else {
        return ''
      }
    }
    return null
  }
  getCreditCardLast4Number (paymentMethodType, rawPaymentInfoJson) {
    if (paymentMethodType === 'credit-card') {
      const paymentInfoJSON = JSON.parse(rawPaymentInfoJson)
      if (paymentInfoJSON.hasOwnProperty('masked_card_number')) {
        return paymentInfoJSON.masked_card_number.substr(paymentInfoJSON.masked_card_number.length - 4, 4)
      } else if (paymentInfoJSON.hasOwnProperty('ends_with')) {
        return paymentInfoJSON.ends_with
      }
    }
    return ''
  }
  render () {
    const {paymentMethodType, paymentInfo} = this.props
    const cardImage = this.getCreditCardImage(paymentMethodType, paymentInfo.payment_info_json)
    const cardNumber = this.getCreditCardLast4Number(paymentMethodType, paymentInfo.payment_info_json)
    const amount = paymentMethodType === 'item' ? paymentInfo.total_amount_charged : 0
    const showAmount = (paymentMethodType === 'item')
    return (
      <View style={Styles.paymentMethodContainer}>
        <Text style={Styles.paymentMethodText}>Payment Method</Text>
        <View style={{flexDirection: 'row', paddingTop: moderateScale(10)}}>
          <View style={{flex: 1}}>
            {paymentMethodType === 'cod' &&
              <Text style={Styles.paymentMethodTypeText}>Cash</Text>
            }
            {paymentMethodType === 'credit-card' &&
              <YumboxIcon name={cardImage} size={20} />
            }
            {paymentMethodType === 'virtual-account' &&
              <Text style={Styles.paymentMethodTypeText}>Bank Transfer</Text>
            }
            {paymentMethodType === 'wallet' &&
              <Text style={Styles.paymentMethodTypeText}>YumCredits</Text>
            }
            {paymentMethodType === 'dana' &&
              <Text style={Styles.paymentMethodTypeText}>DANA</Text>
            }
          </View>
          {paymentMethodType === 'credit-card' &&
            <Text style={Styles.cardNumberText}>{`**${cardNumber}`}</Text>
          }
          {paymentMethodType === 'wallet' && showAmount &&
            <Text style={Styles.cardNumberText}>{`- ${LocaleFormatter.numberToCurrency(amount)}`}</Text>
          }
        </View>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  paymentMethodContainer: {
    flex: 1,
    marginTop: moderateScale(25),
    marginBottom: moderateScale(40),
    backgroundColor: Colors.lightGrey2,
    borderRadius: 3,
    paddingTop: moderateScale(24),
    paddingHorizontal: moderateScale(18),
    paddingBottom: moderateScale(27)
  },
  paymentMethodTypeText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownThree
  },
  paymentMethodText: {
    fontFamily: 'Rubik-Bold',
    fontWeight: '500',
    color: Colors.greyishBrownTwo
  },
  cardNumberText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.greyishBrownThree
  },
})

export default MMPPaymentInformation
