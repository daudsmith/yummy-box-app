import PropTypes from 'prop-types'
import React from 'react'
import {
  Text,
  TouchableWithoutFeedback,
  View,
  StyleSheet,
} from 'react-native'
import Colors from '../Themes/Colors'
import { scale } from 'react-native-size-matters'

const SelectDeliveryAddressButton = (props) => {
  const { testID, accessibilityLabel, deliveryAddressOptionPicker } = props

  return (
    <View style={{
      flex: 1,
      flexDirection: 'row',
      marginBottom: 16,
    }}>
      <View style={{ flex: 1 }}>
        <Text style={Styles.setDeliveryAddress}>Delivery Address</Text>
      </View>
      <View>
        <TouchableWithoutFeedback
          testID={testID}
          accessibilityLabel={accessibilityLabel}
          onPress={deliveryAddressOptionPicker}
          hitSlop={{
            top: 26,
            left: 26,
            bottom: 26,
            right: 26,
          }}
        >
          <View>
            <Text style={Styles.text}>
              Enter Full Address
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  )
}

SelectDeliveryAddressButton.propTypes = {
  deliveryAddressOptionPicker: PropTypes.func,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
}

const Styles = StyleSheet.create({
  text: {
    fontFamily: 'Rubik-Regular',
    color: Colors.green,
    fontSize: scale(14),
  },
  titleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownThree,
  },
  setDeliveryAddress: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownThree,
    fontSize: scale(14),
  },
})

export default SelectDeliveryAddressButton
