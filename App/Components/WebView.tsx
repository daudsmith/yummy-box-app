import React, { useRef } from 'react'
import { ActivityIndicator, Modal, StyleSheet, TouchableOpacity, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { WebView as RNWebView } from 'react-native-webview'
import { WebViewMessageEvent } from 'react-native-webview/lib/WebViewTypes'

import Colors from '../Themes/Colors'
import { _linkingToOtherApp } from '../util/_linkingApp'

const patchPostMessageFunction = function() {
  window.postMessage = function(data) {
    window['ReactNativeWebView'].postMessage(data)
  }
}
const injectJavascript = '(' + String(patchPostMessageFunction) + ')();'

interface WebViewInterface {
  visible: boolean;
  dismiss: () => void;
  validationSuccess?: (authData: any) => void;
  url: string;
  canGoBack?: boolean;
}

const onMessageHandler = (
  event: WebViewMessageEvent,
  props: WebViewInterface
) => {
  const authData = JSON.parse(event.nativeEvent.data)
  props.dismiss()
  props.validationSuccess(authData)
}

const WebView: React.FC<WebViewInterface> = props => {
  const { visible, dismiss, url, canGoBack } = props

  const [isLinkedPayment, setIsLinkedPayment] = React.useState<boolean>(null)

  React.useEffect(() => {
    const isObjectURL = props.url && typeof props.url === 'object'
    if (isObjectURL) {
      _linkingToOtherApp({
        checkout_url: props.url
      })
      setIsLinkedPayment(true)
    } else {
      setIsLinkedPayment(false)
    }
  }, [setIsLinkedPayment])

  const webViewRef = useRef(null)

  const modalRequestClose = () => {
    if (canGoBack) {
      handleBackButton()
    } else {
      dismiss()
    }
  }

  const handleBackButton = () => {
    webViewRef.current.goBack()
    return true
  }

  if (isLinkedPayment === false) {
    return (
      <Modal
        animationType="slide"
        visible={visible}
        onRequestClose={modalRequestClose}
      >
        <View style={{ flex: 1 }}>
          <View style={Styles.navBar}>
            <TouchableOpacity onPress={dismiss}>
              <Ionicons name="md-close" color="black" size={27} />
            </TouchableOpacity>
          </View>
          <View style={Styles.content}>
            <RNWebView
              ref={webViewRef}
              injectedJavaScript={injectJavascript}
              source={{ uri: url }}
              onMessage={event => onMessageHandler(event, props)}
              startInLoadingState
              renderLoading={() => {
                return (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                  >
                    <ActivityIndicator
                      size="large"
                      color={Colors.bloodOrange}
                    />
                  </View>
                )
              }}
            />
          </View>
        </View>
      </Modal>
    )
  } else {
    return null
  }
}

const Styles = StyleSheet.create({
  navBar: {
    paddingTop: 25,
    paddingLeft: 15,
    paddingBottom: 10,
    backgroundColor: 'white'
  },
  content: {
    flex: 1,
    padding: 10,
    borderTopWidth: 1,
    backgroundColor: '#FEFDFF'
  }
})

export default WebView
