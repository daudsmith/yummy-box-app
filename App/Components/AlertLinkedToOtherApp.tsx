import React from 'react'

import AlertBox from './AlertBox'
import { PaymentOptionType } from './Payment/PaymentMethodPicker'
import SalesOrderResultModal from './SalesOrderResultModal'

const isLinkedApp: PaymentOptionType = 'gopay' || 'shopeepay'

interface AlertContinuePaymentInterface {
  showAlert: boolean;
  dismiss: () => void;
}

interface AlertConfirmLinkedToOtherAppProps
  extends AlertContinuePaymentInterface {
  onNextBtn: () => void;
  paymentMethod: PaymentOptionType;
}

export const AlertConfirmLinkedToOtherApp = (
  props: AlertConfirmLinkedToOtherAppProps
) => {
  const { dismiss, showAlert, onNextBtn, paymentMethod } = props

  if (isLinkedApp) {
    const messageVariantTransform = (): {
      method: string;
      appName: string;
    } => {
      switch (paymentMethod) {
        case 'gopay':
          return {
            method: 'GoPay',
            appName: 'Gojek'
          }
        case 'shopeepay':
          return {
            method: 'ShopeePay',
            appName: 'Shopee'
          }
        default:
          return {
            method: null,
            appName: null
          }
      }
    }
    return (
      <AlertBox
        visible={showAlert}
        primaryAction={dismiss}
        secondaryAction={onNextBtn}
        primaryActionText="Cancel"
        secondaryActionText="Continue"
        title={`Pay using ${messageVariantTransform().method}`}
        text={`You will be redirected to ${
          messageVariantTransform().appName
        } app. Please complete the payment on ${
          messageVariantTransform().appName
        } to place your order.`}
        dismiss={dismiss}
      />
    )
  }
  return null
}

interface AlertPaymentDeepLinkResponse extends AlertContinuePaymentInterface {
  isSuccess: boolean;
}

export const AlertPaymentDeepLink = (props: AlertPaymentDeepLinkResponse) => {
  const { dismiss, showAlert, isSuccess } = props
  if(isSuccess){
    return (
      <SalesOrderResultModal
        visible={showAlert}
        modalType="thank-you"
        invoiceUrl={null}
        onPress={dismiss}
      />
    )
  }
  return (
    <AlertBox
      visible={showAlert}
      primaryAction={dismiss}
      primaryActionText="OK"
      title={`Payment ${isSuccess ? 'Success' : 'Failed'}`}
      text={isSuccess ? '' : 'Oops! Your payment is failed. Please try again.'}
      dismiss={dismiss}
    />
  )
}
