import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet, ViewStyle, TextStyle} from 'react-native'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import Colors from '../Themes/Colors'

export interface BoxBottomProps {
    navigation: NavigationDrawerProp,
    primaryText: string,
    secondText: string,
    to: string,
}

const BoxBottom: React.FC<BoxBottomProps> = (props) => {
    const {navigation, primaryText, secondText, to} = props
    return (
        <View style={Styles.container}>
          <Text style={Styles.primaryText}>{primaryText}</Text>
          <TouchableOpacity onPress={() => navigation.navigate(to)}>
            <Text style={Styles.secondText}>{secondText}</Text>
          </TouchableOpacity>
        </View>
    )
}

export interface BoxBottomStyle {
    container: ViewStyle
    primaryText: TextStyle
    secondText: TextStyle
  }

const Styles: BoxBottomStyle = StyleSheet.create({
    container: {
        justifyContent: 'center', 
        flexDirection: 'row',
    },
    primaryText: {
        fontFamily: 'Rubik-Regular', 
        fontSize: 12, 
        color: Colors.primaryGrey,
    },
    secondText: {
        fontFamily: 'Rubik-Regular', 
        fontSize: 12, 
        color: Colors.primaryOrange,
        marginLeft: 2,
    }
  })

export default BoxBottom