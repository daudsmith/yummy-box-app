import React from 'react'
import { Text, TouchableWithoutFeedback, View, StyleSheet } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Colors from '../Themes/Colors'

const FooterButton = (props) => {
  const backgroundColor = props.disabled ? Styles.backgroundColorDisabled : Styles.backgroundEnabled
  const noIcon = props.noIcon !== undefined
  return (
    <TouchableWithoutFeedback 
      testID={props.testID} 
      accessibilityLabel={props.accessibilityLabel} 
      onPress={() => props.onPress()} 
      disabled={props.disabled}
    >
      {noIcon ?
        <View
          style={[
            Styles.container,
            backgroundColor, {
              justifyContent: 'center'
            }
          ]}>
          <Text style={Styles.text}>{props.text}</Text>
        </View>
      :
        <View style={[Styles.container, backgroundColor]}>
          <Text style={[Styles.text, {flex: 1}]}>{props.text}</Text>
          <FontAwesome name='chevron-right' size={20} color='white' />
        </View>

      }
    </TouchableWithoutFeedback>
  )
}

const Styles = StyleSheet.create({
  container: {
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    flex: 1
  },
  text: {
    fontFamily: 'Rubik-Regular',
    color: 'white',
    fontSize: 16
  },
  backgroundColorDisabled: {
    backgroundColor: Colors.disable
  },
  backgroundEnabled: {
    backgroundColor: Colors.primaryOrange
  }
})

export default FooterButton
