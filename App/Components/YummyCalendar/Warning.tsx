import { Icon } from 'native-base'
import * as React from 'react'
import { Text, View } from 'react-native'
import { Colors } from '../../Themes'

interface WarningInterface {
  message: string
}

const Warning: React.FC<WarningInterface> = (props) => {
  const {
    message,
  } = props
  if (message) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          backgroundColor: Colors.primaryWarning,
          height: 24,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Icon
          name="ios-information-circle"
          style={{
            fontSize: 12,
            color: Colors.white,
            marginRight: 4,
            marginTop: 2,
          }}
        />
        <Text
          style={{
            fontSize: 12,
            fontFamily: 'Rubik-Regular',
            color: Colors.white,
            lineHeight: 16,
          }}
        >
          {message}
        </Text>
      </View>
    )
  }
  return null
}

export default Warning
