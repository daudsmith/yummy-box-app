import moment, { Moment } from 'moment'
import React, { Component } from 'react'
import {
  GestureResponderEvent,
  Platform,
  View,
} from 'react-native'
import { Calendar } from 'react-native-calendars'
import Icon from 'react-native-vector-icons/Entypo'
import BottomModal from '../BottomModal'
import { Colors } from '../../Themes'
import Extra from './Extra'
import Warning from './Warning'

interface CalendarPropsInterface {
  minDate?: string,
  maxDate?: string,
  availableDates?: string[],
  actives?: string[],
  disabled?: string[],
  onSelectDate?: (date: string) => void,
  loading?: boolean,
  disabledByDefault?: boolean,
  disabledWeekEnd?: boolean,
  warningMessage?: string,
  extraTitle?: string,
  extraMessage?: string,
  extraOnClose?: () => void,
  openExtra?: boolean,
  onExtraPress?: (event: GestureResponderEvent) => void,
  offDates?: Moment[],
}

interface CalendarStateInterface {
  currentMonth: string
}

class YummyCalendar extends Component<CalendarPropsInterface, CalendarStateInterface> {
  static defaultProps = {
    minDate: moment()
      .format('YYYY-MM-DD'),
    maxDate: moment()
      .add(3, 'months')
      .format('YYYY-MM-DD'),
    actives: [],
    disabled: [],
    loading: false,
    disabledByDefault: false,
    disabledWeekEnd: false,
    warningMessage: '',
    extraTitle: '',
    extraMessage: '',
    openExtra: false,
    offDates: [],
  }

  calendar

  state = {
    currentMonth: moment(this.props.minDate)
      .format('YYYY-MM'),
  }

  componentDidMount() {
    this.checkInitialMonthDisplay()
  }

  checkInitialMonthDisplay = () => {
    const { minDate } = this.props
    const minDateMonth = moment(minDate)
      .format('YYYY-MM')
    const currentMonth = moment()
      .format('YYYY-MM')
    if (moment(currentMonth)
      .isBefore(moment(minDateMonth))) {
      this.switchDisplayNextMonth()
    }
  }

  getWeekEnds = () => {
    const {
      currentMonth,
    } = this.state
    let weekEnds = []
    let startingDate = moment(currentMonth)
      .startOf('month')
    const endDate = moment(currentMonth)
      .endOf('month')
    let diff = endDate.diff(startingDate, 'days')
    while (diff >= 0) {
      // decrease "days" only if it's a weekday.
      if (startingDate.isoWeekday() === 6
        || startingDate.isoWeekday() === 7
      ) {
        weekEnds.push(startingDate.format('YYYY-MM-DD'))
      }
      diff -= 1
      startingDate = startingDate.add(1, 'days')
    }
    return weekEnds
  }

  setAsMarked = () => {
    const {
      actives,
      disabled,
      disabledWeekEnd,
      availableDates,
      minDate,
      maxDate,
    } = this.props
    let dates = {}
    if (availableDates && availableDates.length) {
      availableDates.map(item => {
        dates = {
          ...dates,
          [item]: {
            disabled: false,
            disableTouchEvent: false,
            selected: false,
            marked: false,
          },
        }
        return item
      })
    }

    if (actives && actives.length) {
      actives.map(item => {
        dates = {
          ...dates,
          [item]: {
            disabled: false,
            disableTouchEvent: false,
            selected: true,
            marked: false,
          },
        }
        return item
      })
    }

    if (disabled && disabled.length) {
      disabled.map(item => {
        if (item) {
          dates = {
            ...dates,
            [item]: {
              disabled: true,
              disableTouchEvent: true,
              selected: false,
              marked: false,
            },
          }
        }
        return item
      })
    }

    if (disabledWeekEnd) {
      const weekEnds = this.getWeekEnds()
      weekEnds.map(item => {
        dates = {
          ...dates,
          [item]: {
            disabled: true,
            disableTouchEvent: true,
            selected: false,
            marked: false,
          },
        }
        return item
      })
    }

    // disable not listed date
    const start = moment(minDate)
    const end = moment(maxDate)
    while (!start.isSame(end)) {
      if (typeof dates[start.format('YYYY-MM-DD')] == 'undefined') {
        dates = {
          ...dates,
          [start.format('YYYY-MM-DD')]: {
            disabled: true,
            disableTouchEvent: true,
            selected: false,
            marked: false,
          },
        }
      }
      start.add(1, 'day')
    }

    return dates
  }

  renderArrow = (direction, currentMonth) => {
    const { minDate, maxDate } = this.props
    const minMonth = moment(minDate)
      .format('YYYY-MM')
    const maxMonth = moment(maxDate)
      .format('YYYY-MM')
    if (direction === 'left' && currentMonth !== minMonth) {
      return (
        <Icon
          name="chevron-left"
          color={Colors.primaryTurquoise}
          size={20}
          style={{
            marginLeft: -10,
          }}
        />
      )
    }

    if (direction === 'right' && currentMonth !== maxMonth) {
      return (
        <Icon
          name="chevron-right"
          color={Colors.primaryTurquoise}
          size={20}
          style={{
            marginRight: -10,
          }}
        />
      )
    }

    return (
      <View
        style={{
          width: 20,
          height: 20,
        }}
      />
    )
  }

  prevHandler = (substractMonth) => {
    const { minDate } = this.props
    const { currentMonth } = this.state
    const minMonth = moment(minDate)
      .format('YYYY-MM')
    if (currentMonth !== minMonth) {
      this.setState({
        currentMonth: moment(currentMonth)
          .subtract(1, 'months')
          .format('YYYY-MM'),
      }, () => {
        substractMonth()
      })
    }
  }

  nextHandler = (addMonth) => {
    const { maxDate } = this.props
    const { currentMonth } = this.state
    const maxMonth = moment(maxDate)
      .format('YYYY-MM')
    if (currentMonth !== maxMonth) {
      this.setState({
        currentMonth: moment(this.state.currentMonth)
          .add(1, 'months')
          .format('YYYY-MM'),
      }, () => {
        addMonth()
      })
    }
  }

  switchDisplayNextMonth = () => {
    this.setState({
      'currentMonth': moment(this.props.minDate)
        .format('YYYY-MM'),
    }, () => {
      this.calendar.addMonth(1)
    })
  }

  setOffDates = () => {
    const { offDates } = this.props

    return offDates.reduce(function(prev, current) {
      return {
        ...prev,
        [current.format('YYYY-MM-DD')]: {
          disabled: true,
          disableTouchEvent: true,
          selected: false,
          marked: false,
        },
      }
    }, [])
  }

  render() {
    const {
      minDate,
      maxDate,
      onSelectDate,
      loading,
      disabledByDefault,
      warningMessage,
      extraMessage,
      extraTitle,
      openExtra,
      extraOnClose,
      onExtraPress,
    } = this.props

    const {
      currentMonth,
    } = this.state

    let markedDates = { ...this.setAsMarked(), ...this.setOffDates() }

    return (
      <View>
        {warningMessage !== '' && (
          <Warning
            message={warningMessage}
          />
        )}

        <Calendar
          ref={ref => this.calendar = ref}
          minDate={minDate}
          maxDate={maxDate}
          onDayPress={date => onSelectDate(date)}
          displayLoadingIndicator={loading}
          markedDates={markedDates}
          disabledByDefault={disabledByDefault}
          onPressArrowLeft={substractMonth => this.prevHandler(substractMonth)}
          onPressArrowRight={addMonth => this.nextHandler(addMonth)}
          renderArrow={direction => this.renderArrow(direction, currentMonth)}
          theme={{
            arrowColor: Colors.primaryGrey,
            monthTextColor: Colors.primaryTurquoise,
            selectedDayTextColor: Colors.white,
            todayTextColor: Colors.primaryDark,
            dayTextColor: Colors.primaryDark,
            textSectionTitleColor: Colors.primaryDark,
            textMonthFontFamily: 'Rubik-Medium',
            textMonthFontWeight: '500',
            textDayFontFamily: 'Rubik-Medium',
            textDayHeaderFontFamily: 'Rubik-Regular',
            selectedDayBackgroundColor: Colors.primaryOrange,
            selectedDotColor: Colors.primaryOrange,
            textDayFontSize: 12,
            'stylesheet.day.basic': {
              base: {
                width: 40,
                height: 40,
                alignItems: 'center',
              },
              selected: {
                backgroundColor: Colors.primaryOrange,
                borderRadius: 20,
              },
              text: {
                marginTop: Platform.OS === 'android'
                  ? 5
                  : 7,
                fontSize: 12,
                fontFamily: 'Rubik-Medium',
                lineHeight: 28,
              },
              disabledText: {
                color: Colors.primaryGrey,
                fontFamily: 'Rubik-Regular',
              },
            },
          }}
          disableMonthChange={true}
        />

        {extraTitle && (
          <Extra
            message={extraTitle}
            onPress={onExtraPress}
          />
        )}

        <BottomModal
          visible={openExtra}
          dismiss={extraOnClose}
          buttonText={'Close'}
          title={extraTitle}
          message={extraMessage}
        />
      </View>
    )
  }
}

export default YummyCalendar
