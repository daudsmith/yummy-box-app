import { Icon } from 'native-base'
import * as React from 'react'
import {
  GestureResponderEvent,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { Colors } from '../../Themes'

interface ExtraInterface {
  message: string
  onPress?: (event: GestureResponderEvent) => void
}

const Extra: React.FC<ExtraInterface> = (props) => {
  const {
    message,
    onPress,
  } = props
  if (message) {
    return (
      <TouchableWithoutFeedback
        onPress={onPress}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: 'transparent',
            height: 30,
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 16,
          }}
        >
          <Text
            style={{
              fontSize: 12,
              fontFamily: 'Rubik-Regular',
              color: Colors.primaryGrey,
            }}
          >
            {message}
          </Text>
          <Icon
            name="ios-information-circle"
            style={{
              fontSize: 14,
              color: Colors.primaryGrey,
              marginLeft: 4,
              marginTop: 1,
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    )
  }
  return null
}

export default Extra
