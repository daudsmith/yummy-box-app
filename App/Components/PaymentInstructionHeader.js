import React from 'react'
import PropTypes from 'prop-types'
import {
  PixelRatio,
  StyleSheet,
  Text,
  View,
} from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import LocaleFormatter from '../Services/LocaleFormatter'
import { Colors } from '../Themes'
import PrimaryButton from './PrimaryButton'

const PaymentInstructionHeader = ({ onCopy, accountNumber }) => (
  <View style={styles.header}>
    <Text style={styles.headerText}>Virtual Account No:</Text>
    <View style={styles.accountContainer}>
      <View style={styles.accountNumberContainer}>
        <Text style={styles.accountNumberText}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text>
      </View>
      <View style={{ marginHorizontal: 8 }}>
        <PrimaryButton
          label='COPY'
          iconName='copy'
          iconColor={Colors.bloodOrange}
          iconSize={11}
          iconPosition='left'
          onPressMethod={onCopy}
          disabled={false}
          labelStyle={styles.buttonText}
          buttonStyle={styles.buttonCopy}
        />
      </View>
    </View>
  </View>
)

PaymentInstructionHeader.propTypes = {
  accountNumber: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
  onCopy: PropTypes.func.isRequired,
}

const accountFontSize = PixelRatio.get() === 2
  ? 14
  : 16

const styles = StyleSheet.create({
  header: {
    height: moderateScale(79),
    padding: moderateScale(8) / PixelRatio.getFontScale(),
  },
  headerText: {
    fontSize: moderateScale(11) / PixelRatio.getFontScale(),
    paddingBottom: moderateScale(6),
    paddingTop: moderateScale(3),
    marginLeft: moderateScale(8) / PixelRatio.getFontScale(),
  },
  accountContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: moderateScale(6),
    paddingHorizontal: moderateScale(8) / PixelRatio.getFontScale(),
  },
  accountNumberContainer: {
    height: moderateScale(32),
    borderRadius: moderateScale(4),
    paddingHorizontal: moderateScale(8) / PixelRatio.getFontScale(),
    backgroundColor: Colors.whiteFive,
    justifyContent: 'center',
  },
  accountNumberText: {
    fontSize: moderateScale(accountFontSize) / PixelRatio.getFontScale(),
    color: Colors.brownishGrey,
    fontWeight: 'bold',
  },
  buttonCopy: {
    backgroundColor: Colors.white,
    height: moderateScale(32),
    width: moderateScale(77),
    borderRadius: moderateScale(4),
    borderWidth: moderateScale(1),
    borderColor: Colors.bloodOrange,
  },
  buttonText: {
    fontSize: moderateScale(12) / PixelRatio.getFontScale(),
    color: Colors.bloodOrange,
  },
})

export default PaymentInstructionHeader
