import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Dimensions,
  Modal,
  ScrollView,
  StyleSheet,
} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import YummyboxIcom from './YummyboxIcon'
import Colors from '../Themes/Colors'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'

const dropdownWidth = scale(120)
const dropdownMaxHeight = verticalScale(Dimensions.get('window').height * 0.25)

class RoundedDropdown extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    useYummyboxIcon: PropTypes.bool,
    iconName: PropTypes.string,
    iconColor: PropTypes.string,
    label: PropTypes.string.isRequired,
    values: PropTypes.array.isRequired,
    showCheck: PropTypes.bool,
    selectedIndex: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]).isRequired,
    disabledIndex: PropTypes.number,
    textDropdown: PropTypes.string.isRequired,
    textList: PropTypes.string.isRequired
  }

  dropdownRef = React.createRef()

  constructor(props) {
    super(props)
    this.state = {
      dropdownOpen: false,
      position: null,
      initialTop: null,
    }
  }

  measureLayout = () => {
    this.dropdownRef.current.measureLayout(1, (left, top, width, height) => {
      this.setState({
        position: {
          left,
          top,
          width,
          height,
        },
        initialTop: top,
      })
    })
  }

  onPress = ({ nativeEvent }) => {
    const top = this.state.initialTop === null
      ? nativeEvent.pageY - nativeEvent.locationY
      : nativeEvent.pageY > this.state.initialTop
        ? this.state.initialTop
        : nativeEvent.pageY - nativeEvent.locationY
    const position = {
      ...this.state.position,
      top,
    }
    this.setState({ position }, () => this.setState({ dropdownOpen: true }))
  }

  onValuePress = (selected) => {
    this.props.onPress(selected)
    this.setState({ dropdownOpen: false })
  }

  render() {
    const { dropdownOpen, position } = this.state
    const { label, values, selectedIndex, showCheck, textDropdown, textList } = this.props
    const useYummyboxIcon = this.props.useYummyboxIcon !== undefined
    return (
      <View ref={this.dropdownRef} onLayout={this.measureLayout}
            style={{ opacity: 1 }}>
        <TouchableWithoutFeedback 
          testID={textDropdown} 
          accessibilityLabel={textDropdown} 
          onPress={(event) => this.onPress(event)}
        >
          <View style={Styles.container}>
            <Text style={Styles.text}>{label}</Text>
            {useYummyboxIcon
              ? (
                <YummyboxIcom
                  name={this.props.iconName}
                  color={this.props.iconColor === undefined
                    ? Colors.primaryOrange
                    : this.props.iconColor}
                  size={scale(14)}
                  style={{ paddingBottom: verticalScale(4) }}
                />
              )
              : (
                <FontAwesome
                  name='sort-down'
                  color={Colors.greyishBrownTwo}
                  size={scale(14)}
                  style={{ paddingBottom: verticalScale(4) }}
                />
              )}
          </View>
        </TouchableWithoutFeedback>
        <Modal
          animationType='fade'
          visible={dropdownOpen}
          onRequestClose={() => this.setState({ dropdownOpen: false })}
          transparent={true}
        >
          <TouchableWithoutFeedback onPress={() => this.setState({ dropdownOpen: false })}>
            <View style={{ flex: 1 }}>
              {position !== null &&
              <View
                style={{
                  ...Styles.resultContainer,
                  top: position.top,
                  flex: 1,
                  maxHeight: dropdownMaxHeight,
                }}
              >
                <ScrollView
                  contentContainerStyle={{
                    flexGrow: 1,
                    paddingHorizontal: scale(13),
                    paddingTop: verticalScale(9),
                  }}
                >
                  {values.map((value, index) => {
                    const disabled = this.props.disabledIndex === undefined
                      ? false
                      : this.props.disabledIndex[value.index]
                    const textColor = value.index === selectedIndex
                      ? { color: Colors.primaryOrange }
                      : disabled
                        ? { color: Colors.disable }
                        : {}
                    return (
                      <TouchableOpacity
                        testID={`${textList}${index}`}
                        accessibilityLabel={`${textList}${index}`}
                        onPress={() => disabled
                          ? {}
                          : this.onValuePress(value)}
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          paddingBottom: verticalScale(17),
                        }}
                        key={value.index}
                      >
                        <Text
                          style={{
                            ...Styles.dropdownText,
                            ...textColor,
                          }}
                        >
                          {value.label}
                        </Text>
                        {value.index === selectedIndex && showCheck && (
                          <FontAwesome name='check' color={Colors.primaryOrange} />
                        )}
                      </TouchableOpacity>
                    )
                  })}
                </ScrollView>
              </View>
              }
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: dropdownWidth,
    height: verticalScale(35),
    paddingHorizontal: scale(12),
    borderRadius: 35 / 2,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: Colors.primaryOrange,
  },
  text: {
    flex: 1,
    fontFamily: 'Rubik-Medium',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo,
  },
  resultContainer: {
    position: 'absolute',
    left: scale(20),
    width: dropdownWidth,
    backgroundColor: 'white',
    shadowColor: 'rgba(178, 178, 178, 0.5)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1,
  },
  dropdownText: {
    flex: 1,
    fontFamily: 'Rubik-Light',
    color: Colors.greyishBrownTwo,
    fontSize: scale(14),
  },
})

export default RoundedDropdown
