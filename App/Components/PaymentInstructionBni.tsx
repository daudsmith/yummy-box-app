import * as React from 'react'
import { Text, View } from 'react-native'
import Hyperlink from 'react-native-hyperlink'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import PrimaryTabBar from '../Components/PrimaryTabBar'
import LocaleFormatter from '../Services/LocaleFormatter'
import Colors from '../Themes/Colors'
import NumberedList from './NumberedList'
import { ScrollableTabViewContent } from './Common/ScrollableTabViewContent'
import { instructionProps } from './PaymentInstructionMandiri'
import styles from './Styles/PaymentInstructionStyles'

const PaymentInstructionBni: React.FC<instructionProps> = ({accountNumber}) => {
  return (
    <ScrollableTabView
      initialPage={0}
      renderTabBar={() => (
        <PrimaryTabBar
          tabStyle={styles.tabStyle}
          tabButtonStyle={styles.tabButtonStyle}
          tabTitleStyle={styles.tabTitleStyle}
        />
      )}
    >
      <ScrollableTabViewContent tabLabel='ATM' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Text style={styles.text}>Insert the card, select the language and then enter your PIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "Other Menu" and select "Transfer"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Select "Savings" and "Account BNI Virtual Account"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter the Virtual Account number <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text> and the amount you want to pay</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Check the transaction data and press "YES"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
      <ScrollableTabViewContent tabLabel='ONLINE' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Hyperlink linkDefault={true}><Text style={styles.text}>Login to <Text style={{color: Colors.grennBlue}}>https://ibank.bni.co.id</Text>, enter your USER ID and PASSWORD</Text></Hyperlink>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "TRANSFER" and select "Add Account Favorites"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>If you use the desktop to add to your account, select "Transactions", then select "Info & Transfer Administration" and then select "Manage Account Destination" then "Add Account Destination"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter the name and number of your Virtual Account <Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text>, and enter the Token Authentication Code</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>If the destination account number added successfully, go back to the menu "TRANSFER"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={<Text style={styles.text}>Select "TRANSFER BETWEEN ACCOUNT BNI", then select the destination account</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'7.'}
            content={<Text style={styles.text}>Select Debit Account and type the nominal, then enter the code authentication token</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'8.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
      <ScrollableTabViewContent tabLabel='BNI Mobile' style={styles.container}>
        <View style={styles.tabContainer}>
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'1.'}
            content={<Text style={[styles.text]}>Login to BNI Mobile Banking, enter your USER ID and MPIN</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'2.'}
            content={<Text style={styles.text}>Select "Transfer" and select "Account Delivery BNI"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'3.'}
            content={<Text style={styles.text}>Select "Input New Account"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'4.'}
            content={<Text style={styles.text}>Enter "Debit Account", "Account Interest (<Text style={{fontWeight: '600'}}>{LocaleFormatter.formatBankAccountNumber(accountNumber)}</Text>)" and "Nominal" then click "Continue"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'5.'}
            content={<Text style={styles.text}>Check your transaction data, enter "Transaction Password" and then click "Continue"</Text>}
          />
          <NumberedList
            bulletStyle={styles.contentBulletStyle}
            bulletTextStyle={styles.contentBulletTextStyle}
            contentStyle={styles.contentStyle}
            number={'6.'}
            content={<Text style={styles.text}>Once the payment transaction is completed, your YumCredits balance will be updated automatically. This may take up to 10 minutes.</Text>}
          />
        </View>
      </ScrollableTabViewContent>
    </ScrollableTabView>
  )
}

export default PaymentInstructionBni
