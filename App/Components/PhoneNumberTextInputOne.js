import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import PhoneInput from 'react-native-phone-input'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { Colors } from '../Themes'
import { scale } from 'react-native-size-matters'

class PhoneNumberTextInputOne extends React.Component {
  checkIsValidNumber () {
    return this.phone.isValidNumber()
  }

  onChangePhoneNumber (phone) {
    const isValidNumber = this.checkIsValidNumber()
    const countryCode = this.phone.getDialCode()
    this.props.onChange(phone, countryCode, isValidNumber)
  }

  render () {
    let {showMessage, onChangePhoneNumber, value} = this.props
    return (
      <View>
        <View style={[{borderBottomWidth: 1, flexDirection: 'row'}, {borderColor: showMessage.iconColor}]}>
          <View style={{width: '95%'}}>
            <PhoneInput
              testID='inputChangeNum'
              accessibilityLabel='inputChangeNum'
              ref={ref => {
                this.props.setRef(ref)
                this.phone = ref
              }}
              value={value}
              initialCountry={'id'}
              onChangePhoneNumber={(phone) => onChangePhoneNumber(phone)}
              returnKeyType='next'
              style={{
                paddingBottom: 6,
                paddingTop: 9,
                paddingHorizontal: 5,
                backgroundColor: '#fff'
              }}
              textStyle={{
                fontFamily: 'Rubik-Regular',
                color: Colors.greyishBrown,
                fontSize: scale(16),
              }}
            />
          </View>
          <FontAwesome
            name={showMessage.iconColor === 'red' ? '' : showMessage.iconName}
            color={showMessage.iconColor}
            size={10}
            style={{alignSelf: 'center', position: 'absolute', right: 0}}
          />
        </View>

        <View style={Styles.messageContainer}>
          <FontAwesome
            name={showMessage.iconColor === 'green' ? '' : showMessage.iconName}
            color={showMessage.iconColor}
            size={10}
          />
          <Text style={[Styles.messageText, {color: showMessage.iconColor}]}>{showMessage.text}</Text>
        </View>

      </View>
    )
  }
}

export default PhoneNumberTextInputOne

let Styles = StyleSheet.create({
  textInputContainer: {
    borderBottomWidth: 0.5,
    flexDirection: 'row'
  },
  messageText: {
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    marginLeft: 6
  },
  messageContainer: {
    flexDirection: 'row',
    marginTop: 6,
    height: 15
  }
})
