import React from 'react'
import { View, TouchableOpacity, Text, Dimensions } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { scale, verticalScale } from 'react-native-size-matters'
import Modal from 'react-native-modal'
import Colors from '../Themes/Colors'
import Styles from './Styles/AlertBoxStyle'

export interface AlertBoxProps {
  primaryAction: () => void,
  secondaryAction?: () => void,
  dismiss: () => void,
  showCloseIcon?: () => void,
  text: string | Function,
  title?: string,
  primaryActionText: string,
  secondaryActionText?: string,
  footerAction?: () => void,
  footerActionText?: string,
  visible: boolean,
  primaryActionTestProp?: string,
  secondaryActionTestProp?: string,
  animationIn?: string | {},
  animationOut?: string | {},
  icon?: string,
  primaryStyle?: 'primary' | 'secondary',
  secondaryStyle?: 'primary' | 'secondary',
}

const AlertBox: React.FC<AlertBoxProps> = ({
  primaryAction,
  secondaryAction,
  dismiss,
  showCloseIcon,
  text,
  title,
  primaryActionText,
  secondaryActionText,
  footerAction,
  footerActionText,
  visible,
  primaryActionTestProp,
  secondaryActionTestProp,
  animationIn,
  animationOut,
  icon,
  primaryStyle,
  secondaryStyle,
}) => {
  const textMarginTop = showCloseIcon || title === undefined ? {} : { marginTop: 5 }
  const iconMarginTop = showCloseIcon ? {} : { marginTop: verticalScale(23) }
  const primaryButtonMarginHorizontal = typeof secondaryAction === 'undefined' ? { marginHorizontal: scale(70) } : {}
  const NewAnimationIn = animationIn === undefined ? 'slideInUp' : animationIn
  const NewAnimationOut = animationOut === undefined ? 'slideOutDown' : animationOut
  const primaryTextStyle = (primaryStyle && primaryStyle === 'primary')
    ? Styles.alertPrimaryButtonText
    : Styles.alertSecondaryButtonText
  const secondaryTextStyle = (secondaryStyle && secondaryStyle === 'secondary')
    ? Styles.alertSecondaryButtonText
    : Styles.alertPrimaryButtonText
  const primaryButtonStyle = (primaryStyle && primaryStyle === 'primary')
    ? Styles.alertPrimaryButton
    : Styles.alertSecondaryButton
  const secondaryButtonStyle = (secondaryStyle && secondaryStyle === 'secondary')
    ? Styles.alertSecondaryButton
    : Styles.alertPrimaryButton

  return (
    <Modal
      animationIn={NewAnimationIn}
      animationOut={NewAnimationOut}
      isVisible={visible}
      coverScreen={false}
      deviceHeight={Dimensions.get('screen').height}
      onBackButtonPress={() => dismiss()}
      onBackdropPress={() => dismiss()}
      style={{ margin: 0 }}
    >
      <View style={Styles.backgroundView}>
        <View style={Styles.box}>
          {showCloseIcon &&
            <TouchableOpacity onPress={() => dismiss()} style={{ alignItems: 'flex-end' }}>
              <Ionicons name='ios-close' size={scale(35)} color={Colors.warmGrey} />
            </TouchableOpacity>
          }
          {icon &&
            <View style={[Styles.iconContainer, iconMarginTop]}>
              <FontAwesome name={icon} color={Colors.bloodOrange} size={scale(36)} />
            </View>
          }
          {title !== undefined &&
            <View>
              <Text style={Styles.title}>{title}</Text>
            </View>
          }
          <View style={textMarginTop}>
            {typeof text === 'string' ? (
              <Text style={(title === undefined ? Styles.alert : Styles.text)}>{text}</Text>
            ) : (
                <View>{text()}</View>
              )}
          </View>
          <View style={[{ width: '100%', alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-around', marginTop: verticalScale(16) }, primaryButtonMarginHorizontal]}>
            <TouchableOpacity
              onPress={() => {
                primaryAction()
                dismiss()
              }}
              style={primaryButtonStyle}
              testID={primaryActionTestProp}
              accessibilityLabel={primaryActionTestProp}
            >
              <Text style={primaryTextStyle}>{primaryActionText}</Text>
            </TouchableOpacity>
            {typeof secondaryAction !== 'undefined' &&
              <TouchableOpacity
                onPress={() => {
                  secondaryAction()
                  dismiss()
                }}
                style={secondaryButtonStyle}
                testID={secondaryActionTestProp}
                accessibilityLabel={secondaryActionTestProp}
              >
                <Text style={secondaryTextStyle}>{secondaryActionText}</Text>
              </TouchableOpacity>
            }
          </View>
          {typeof footerAction !== 'undefined' &&
            <TouchableOpacity
              onPress={() => {
                footerAction()
                dismiss()
              }}
              style={{ alignItems: 'center', marginTop: 18, padding: 10 }}
            >
              <Text style={Styles.alertSecondaryButtonText}>{footerActionText}</Text>
            </TouchableOpacity>
          }
        </View>
      </View>
    </Modal>

  )
}

export default AlertBox
