import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
} from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../Images/SvgIcon/selection.json'
import Colors from '../Themes/Colors'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

class OfflinePage extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          marginHorizontal: moderateScale(30),
        }}
      >
        <YumboxIcon name='no-wifi' size={60} color={Colors.brownishGrey} />
        <Text
          style={[Styles.text, Styles.title]}
        >
          You appear to be offline
        </Text>
        <Text
          style={Styles.text}
        >
          We'll wait for you to find a connection and come back.
        </Text>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  text: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    marginTop: moderateScale(10),
    textAlign: 'center',
    paddingHorizontal: moderateScale(20),
    color: Colors.brownishGrey,
  },
  title: {
    fontSize: moderateScale(18),
    marginTop: moderateScale(20),
  }
})

export default OfflinePage
