import { ViewStyle, TextStyle } from 'react-native'
interface styleObject {
  modalBox: TextStyle
  modalContainer: TextStyle
  modalTitle: TextStyle
  modalText: TextStyle
  modalButton: TextStyle
  modalButtonOk: TextStyle
  modalButtonOk: TextStyle
  modalButtonClose: TextStyle
  buttonTextOk: TextStyle
  buttonTextClose: TextStyle
}
declare const Styles: styleObject

export default Styles