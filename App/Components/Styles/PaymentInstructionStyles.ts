import { PixelRatio, StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabContainer: {
    marginVertical: verticalScale(8),
    paddingHorizontal: scale(10),
  },
  tabStyle: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: verticalScale(20),
  },
  tabButtonStyle: {
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
  },
  tabTitleStyle : {
    fontSize: scale(12/PixelRatio.getFontScale()),
    fontWeight: '400',
  },
  contentBulletStyle: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 0,
    borderRadius: 0,
    backgroundColor: Colors.white,
  },
  contentBulletTextStyle: {
    color: Colors.brownishGrey,
    fontSize: scale(12),
    lineHeight: verticalScale(20),
    fontWeight: 'bold',
  },
  contentStyle: {
    marginTop: 0,
    flexShrink: 1,
    flex: 1,
    width: 0,
    flexWrap: 'nowrap',
    marginLeft: scale(4),
  },
  text: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.brownishGrey,
    lineHeight: verticalScale(20),
  },
  bold: {
    fontWeight: 'bold',
  }
})

export default styles
