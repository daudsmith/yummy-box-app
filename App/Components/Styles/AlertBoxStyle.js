import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  backgroundView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
  },
  box: {
    flex: 1,
    paddingBottom: verticalScale(28),
    backgroundColor: 'white',
    marginHorizontal: scale(30),
    paddingHorizontal: scale(20),
    paddingVertical: verticalScale(16),
    borderRadius: 5,
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1,
  },
  text: {
    fontFamily: 'Rubik-Regular',
    textAlign: 'left',
    fontSize: scale(14),
    color: Colors.primaryDark,
  },
  title: {
    fontSize: scale(16),
    fontFamily: 'Rubik-Medium',
    textAlign: 'left',
    color: Colors.primaryOrange,
    marginBottom: verticalScale(8),
  },
  alertPrimaryButton: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.primaryOrange,
    paddingVertical: verticalScale(12),
    marginHorizontal: scale(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.primaryOrange,
  },
  alertSecondaryButton: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: verticalScale(12),
    marginHorizontal: scale(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.primaryOrange,
  },
  alertPrimaryButtonText: {
    fontFamily: 'Rubik-Regular',
    color: 'white',
    textAlign: 'center',
  },
  alertSecondaryButtonText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.primaryOrange,
    textAlign: 'center',
  },
  iconContainer: {
    alignItems: 'center',
    marginBottom: 2,
  },
  alert: {
    fontFamily: 'Rubik-Regular',
    textAlign: 'center',
    fontSize: scale(14),
    color: Colors.primaryDark,
  },
})
