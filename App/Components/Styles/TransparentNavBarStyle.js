const Styles = {
    container: {
        marginTop: 10,
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },
    button: {
        marginLeft: 20,
        backgroundColor: 'transparent',
    }
}

export default Styles