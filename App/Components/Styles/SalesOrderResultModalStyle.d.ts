import { TextStyle, ViewStyle } from 'react-native'
interface styleObject {
  container: ViewStyle
  modalContainer: ViewStyle,
  text: TextStyle,
  description: TextStyle,
  closeButtonContainer: TextStyle
}
declare const Styles: styleObject

export default Styles