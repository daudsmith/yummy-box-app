
import { PixelRatio } from 'react-native'
import Colors from '../../Themes/Colors'

const Styles = {
    modalBox: {
      padding: 15,
    },
    modalContainer: {
      flexDirection: 'column',
      backgroundColor: 'white',
      elevation: 5,
      borderRadius: 5,
      shadowOpacity: 0.75,
      shadowRadius: 5,
      shadowColor: Colors.pinkishGrey,
      shadowOffset: { height: 5, width: 5 },
      padding: 15,
    },
    modalTitle: {
      fontSize: 17/PixelRatio.getFontScale(),
      fontFamily: 'Rubik-Medium',
      color: Colors.primaryDark,
      marginBottom: 13,
    },
    modalText: {
      fontSize: 13/PixelRatio.getFontScale(),
      fontFamily: 'Rubik-Regular',
      color: Colors.primaryDark,
    },
    modalButton: {
      width: '100%',
      marginTop: 20,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    modalButtonOk: {
      width: '46%',
      height: 48,
      backgroundColor: Colors.bloodOrange,
      borderRadius: 5,
      borderWidth: 1,
      borderColor: Colors.bloodOrange,
      justifyContent: 'center',
    },
    modalButtonClose: {
      width: '46%',
      height: 48,
      backgroundColor: 'white',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: Colors.bloodOrange,
      justifyContent: 'center',
    },
    buttonTextOk: {
      fontSize: 14/PixelRatio.getFontScale(),
      color: 'white',
      textAlign: 'center',
    },
    buttonTextClose: {
      fontSize: 14/PixelRatio.getFontScale(),
      color: Colors.bloodOrange,
      textAlign: 'center',
    },
}

export default Styles
