import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    paddingVertical: 10,
    flex: 1,
    flexDirection: 'column'
  },
  titleText: {
    fontSize: 16
  },
  pickerContainer: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1,
    marginTop: 10
  },
  pickerButton: {
    flex: 1,
    marginHorizontal: 5,
    marginTop: 5,
    borderBottomWidth: 1,
    borderColor: Colors.steel,
    alignItems: 'flex-start',
    borderRadius: Metrics.buttonRadius,
    width: '100%'
  },
  infoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15
  }
})
