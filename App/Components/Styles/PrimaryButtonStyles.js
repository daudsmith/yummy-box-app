import { StyleSheet } from 'react-native'
import {Colors} from '../../Themes/'

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchableOpacityStyle: {
    backgroundColor: Colors.bloodOrange,
    borderRadius: 5,
    height: 40,
    flex: 1,
    flexDirection: 'row'
  },
  iconAndTextContainer: {
    justifyContent: 'center',
    flex: 1
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 18,
    fontFamily: 'Rubik-Regular',
    color: 'white'
  }
})
