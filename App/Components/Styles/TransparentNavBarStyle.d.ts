import { TextStyle, ViewStyle } from 'react-native'
interface styleObject {
    container: ViewStyle
    button: TextStyle
}
declare const Styles: styleObject

export default Styles