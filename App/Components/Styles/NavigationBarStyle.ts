import { StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { Colors } from '../../Themes'
import { scale, verticalScale } from 'react-native-size-matters'

export interface NavigationBarStyleInterface {
  container: ViewStyle,
  yumboxButtonColor: TextStyle,
  buttonColor: TextStyle,
  title: TextStyle,
  badgeContainer: ViewStyle,
}

export default StyleSheet.create<NavigationBarStyleInterface>({
  container: {
    backgroundColor: 'transparent',
    elevation: 0,
  },
  yumboxButtonColor: {
    color: Colors.bloodOrange,
    fontSize: scale(16)
  },
  buttonColor: {
    color: Colors.darkBlue,
    fontSize: scale(30)
  },
  title: {
    color: Colors.darkBlue,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(18),
    fontWeight: 'normal',
  },
  badgeContainer: {
    width: scale(14),
    height: scale(14),
    borderRadius: 50,
    backgroundColor: Colors.bloodOrange,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 0,
    top: verticalScale(8)
  }
})
