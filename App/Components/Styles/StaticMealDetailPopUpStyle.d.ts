import { TextStyle, ImageStyle } from 'react-native'
interface styleObject {
  mealDetailTitleText: TextStyle
  activityIndicatorContainer: TextStyle
  mealDetailContainer: TextStyle
  closeButton: TextStyle
  mealDetailContent: TextStyle
  mealDetailTitleText: TextStyle
  tagStringText: TextStyle
  mealDetailImage : ImageStyle
  mealDetailDescriptionText: TextStyle
}
declare const Styles: styleObject

export default Styles