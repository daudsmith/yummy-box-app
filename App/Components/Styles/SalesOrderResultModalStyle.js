import Colors from '../../Themes/Colors'

const Styles = {
    container: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 30,
      backgroundColor: 'rgba(255, 255, 255, 0.8)',
    },
    modalContainer: {
      flex: 1,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      elevation: 5,
      borderRadius: 5,
      shadowOpacity: 0.75,
      shadowRadius: 5,
      shadowColor: Colors.pinkishGrey,
      shadowOffset: { height: 5, width: 5 }
    },
    text: {
      fontSize: 30,
      fontFamily: 'Rubik-Medium',
      marginTop: 20,
      color: Colors.greyishBrownTwo
    },
    description: {
      fontSize: 16,
      color: Colors.brownishGrey,
      fontFamily: 'Rubik-Regular',
      textAlign: 'center'
    },
    closeButtonContainer: {
      paddingHorizontal: 20,
      paddingVertical: 17,
      alignItems: 'center',
      justifyContent: 'center'
    }
  }

  export default Styles