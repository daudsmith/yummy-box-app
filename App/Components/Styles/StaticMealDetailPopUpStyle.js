
import {moderateScale} from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

const Styles = {
    activityIndicatorContainer: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    mealDetailContainer: {
        flex:1,
        flexDirection: 'row',
        borderRadius: 5,
        backgroundColor: 'white',
        shadowColor: 'rgba(145, 145, 145, 0.5)',
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowRadius: 4,
        shadowOpacity: 1,
        elevation: 1,
        marginVertical: moderateScale(48),
        marginHorizontal: moderateScale(20)
    },
    closeButton: {
        justifyContent: 'center',
        paddingTop: moderateScale(5),
        marginRight: moderateScale(18)
    },
    mealDetailContent: {
        alignItems: 'center',
        paddingTop: moderateScale(15),
        paddingHorizontal: moderateScale(30)
    },
    mealDetailTitleText: {
        fontSize: moderateScale(14),
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        color: Colors.brownishGrey
    },
    tagStringText: {
        fontSize: moderateScale(12),
        fontFamily: 'Rubik-Regular',
        color: Colors.bloodOrange,
        marginTop: moderateScale(5)
    },
    mealDetailImage : {
        height: moderateScale(260),
        width: moderateScale(260),
        borderRadius: 3,
        marginTop: moderateScale(10),
        backgroundColor: Colors.lightGreyX
    },
    mealDetailDescriptionText: {
        fontFamily: 'Rubik-Regular',
        fontSize: moderateScale(12),
        fontWeight: '300',
        color: Colors.greyishBrown,
        marginTop: moderateScale(8)
    }
}

export default Styles
