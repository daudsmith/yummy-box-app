import React from 'react'
import {View, TouchableOpacity} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Colors from '../Themes/Colors'
import Styles from './Styles/TransparentNavBarStyle'

const TransparentNavBar = (props) => {
  const onPress = props.showedModally ? () => props.dismiss() : () => props.navigation.goBack()
  return (
    <View style={Styles.container}>
      <TouchableOpacity style={Styles.button} onPress={() => onPress()}>
        <Ionicons name='ios-arrow-back' color={Colors.bloodOrange} size={27} />
      </TouchableOpacity>
    </View>
  )
}

export default TransparentNavBar