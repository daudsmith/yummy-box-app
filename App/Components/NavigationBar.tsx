import * as React from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  ViewStyle,
} from 'react-native'
import {
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
} from 'native-base'
import { scale, verticalScale } from 'react-native-size-matters'
import YummyboxIcon from '../Components/YummyboxIcon'
import NavigationBarStyle from './Styles/NavigationBarStyle'
import {
  Images,
  Colors,
} from '../Themes'
import Ionicons from 'react-native-vector-icons/Ionicons'


export interface NavigationBarProps {
  leftSide?: string | Function,
  title: string | Function,
  rightSide?: boolean | Function
  dismissModal?: () => void,
  borderOn?: boolean,
  openHamburger?: () => void,
  unread?: number,
  leftSideNavigation?: () => void,
  currentScreen?: string,
  containerCustomStyle?: ViewStyle,
  leftButtonCustomStyle?: ViewStyle,
  statusBarColor?:string,
  navigateFrom?:string,
}

class NavigationBar extends React.PureComponent<NavigationBarProps> {
  static defaultProps = {
    currentScreen: '',
    unread: 0,
  }

  renderBackButton() {
    const { leftSideNavigation, leftButtonCustomStyle } = this.props
    return (
      <Left style={{ flex: 1 }}>
        <TouchableOpacity
          testID="back"
          accessibilityLabel="back"
          onPress={leftSideNavigation}
          style={[Styles.leftButtonStyle, leftButtonCustomStyle]}
          hitSlop={{
            top: verticalScale(26),
            left: scale(26),
            bottom: verticalScale(26),
            right: scale(26),
          }}
        >
          <View>
            <Ionicons name='arrow-back' size={scale(25)} color={Colors.primaryDark} />
          </View>
        </TouchableOpacity>
      </Left>
    )
  }

  renderBackModallyButton() {
    return (
      <Left style={{ flex: 1 }}>
        <Button
          transparent
          onPress={this.props.dismissModal}
          testID="back"
          accessibilityLabel="back"
        >
          <Image
            source={Images.arrowLeft}
            style={{ width: scale(16) }}
            resizeMode='contain'
          />
        </Button>
      </Left>
    )
  }

  renderHamburgerButton = () => {
    let { unread, openHamburger } = this.props
    return (
      <Left style={{ flex: 1 }}>
        <Button
          testID="hamburgerMenu"
          accessibilityLabel="hamburgerMenu"
          transparent
          onPress={openHamburger}
          style={{
            paddingLeft: scale(6),
            paddingTop: verticalScale(10),
          }}
          hitSlop={{
            top: verticalScale(26),
            left: scale(26),
            bottom: verticalScale(26),
            right: scale(26),
          }}
        >
          <YummyboxIcon
            name='hamburger'
            style={NavigationBarStyle.yumboxButtonColor}
          />
          {
            unread !== 0 &&
            <View
              style={{
                ...NavigationBarStyle.badgeContainer,
                width: unread > 99 ? scale(18) : scale(14),
                height: unread > 99 ? scale(18) : scale(14),
              }}
            >
              <Text
                testID="unreadHamburgerMenu"
                accessibilityLabel="unreadHamburgerMenu"
                style={{
                  fontSize: scale(8),
                  color: '#fff',
                  fontFamily: 'Rubik-Regular'
                }}
              >
                {unread}
              </Text>
            </View>
          }
        </Button>
      </Left>
    )
  }

  renderLeft = () => {
    if (this.props.leftSide === 'back') {
      return this.renderBackButton()
    } else if (this.props.leftSide === 'menu') {
      return this.renderHamburgerButton()
    } else if (this.props.leftSide === 'backModally') {
      return this.renderBackModallyButton()
    } else if (typeof this.props.leftSide === 'function') {
      return (
        <Left style={{
          flex: 1,
          justifyContent: 'center',
        }}>
          {this.props.leftSide()}
        </Left>
      )
    } else {
      return <Left/>
    }
  }

  renderTitle = () => {
    if (typeof this.props.title === 'string') {
      const { title } = this.props
      return (
        <Title style={NavigationBarStyle.title}>{title}</Title>
      )
    } else if (typeof this.props.title === 'function') {
      return this.props.title()
    } else {
      return null
    }
  }

  renderRight = () => {
    if (typeof this.props.rightSide === 'function') {
      return (
        <Right style={{ flex: 1 }}>
          {this.props.rightSide()}
        </Right>
      )
    } else {
      return <Right style={{ flex: 1 }}/>
    }
  }

  render() {
    const { containerCustomStyle, statusBarColor, navigateFrom } = this.props
    const border = this.props.borderOn === true
      ? {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: Colors.brownishGrey,
      }
      : { borderBottomWidth: 0 }
    return (
      <Header
        style={[
          NavigationBarStyle.container,
          containerCustomStyle,
          border,
          {
            paddingLeft: 20,
            paddingRight: 20,
          },
        ]}
        translucent={statusBarColor === 'transparent' ? true : false}
        androidStatusBarColor={statusBarColor ? statusBarColor : Colors.darkerDark}
        noShadow
      >
        {this.renderLeft()}
        <Body style={{
          flex: 4,
          marginLeft: navigateFrom === 'home' ? -13 : 0,
          justifyContent: navigateFrom === 'home' ? 'flex-start' : 'center',
          alignItems: navigateFrom === 'home' ? 'flex-start' : 'center',
        }}>
          {this.renderTitle()}
        </Body>
        {this.renderRight()}
      </Header>
    )
  }
}

const Styles = StyleSheet.create({
  leftButtonStyle: {
    paddingLeft: 5,
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
  },
})

export default NavigationBar
