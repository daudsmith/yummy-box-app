import * as React from 'react'
import {Text, View, StyleSheet, TextInput} from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { Colors } from '../Themes'
import { scale } from 'react-native-size-matters'

interface TextInputOneInterface {
  showMessage: showMessageProps
  onChangeText: (text: string) => void
  value: string
  testID: string
  accessibilityLabel: string
}

interface showMessageProps {
  iconColor: string 
  iconName: string
  text: string
}

const TextInputOne: React.FC<TextInputOneInterface> = ({
  showMessage, onChangeText, value, testID, accessibilityLabel
}) => {
  return (
    <View>
      <View style={[Styles.textInputContainer, {borderColor: showMessage.iconColor}]}>
        <TextInput
          testID={testID}
          accessibilityLabel={accessibilityLabel}
          value={value}
          style={{
            width: '95%',
            height: '100%',
            paddingBottom: 1,
            color: Colors.greyishBrown,
            fontSize: scale(16),
          }}
          underlineColorAndroid='transparent'
          autoCapitalize='none'
          onChangeText={onChangeText}
        />
        {showMessage.iconColor === 'green' &&
        (<FontAwesome
          name={showMessage.iconName}
          color={showMessage.iconColor}
          size={10}
          style={{alignSelf: 'center'}}
        />)
        }
      </View>
      <View style={Styles.messageContainer}>
        {showMessage.iconColor === 'red' &&
        (<FontAwesome
          name={showMessage.iconName}
          color={showMessage.iconColor}
          size={10}
        />)
        }
        <Text style={[Styles.messageText, {color: showMessage.iconColor}]}>{showMessage.text}</Text>
      </View>
    </View>
  )
}

let Styles = StyleSheet.create({
  textInputContainer: {
    width: '100%',
    height: 28,
    borderBottomWidth: 0.5,
    flexDirection: 'row'
  },
  messageText: {
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    marginLeft: 6
  },
  messageContainer: {
    flexDirection: 'row',
    marginTop: 6,
    height: 15
  }
})

export default TextInputOne
