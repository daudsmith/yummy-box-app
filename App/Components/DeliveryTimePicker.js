import _ from 'lodash'
import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import PropTypes  from 'prop-types'
import { scale, verticalScale } from 'react-native-size-matters'
import { Colors } from '../Themes'

export default class DeliveryTimePicker extends Component {
  static propTypes = {
    selectedTime: PropTypes.string,
    deliveryTimeSlots: PropTypes.array,
    readOnly: PropTypes.bool,
    handleOnPickerConfirm: PropTypes.func,
    dinner: PropTypes.bool,
    lunch: PropTypes.bool,
  }

  constructor(props) {
    super(props)
    const selectedTime = props.selectedTime
    this.state = {
      selectedTime,
      pickedIndex: 0,
    }
  }

  componentDidMount() {
    if (this.props.selectedTime !== null) {
      const pickedIndex = this.getSelectedValue()
      this.setState({
        pickedIndex,
      })
    }
  }

  getSelectedValue() {
    const { selectedTime, deliveryTimeSlots, readOnly } = this.props
    if (readOnly) {
      return selectedTime
    }
    const selected = _.findIndex(this.state.deliveryTimeSlots, (slot) => {
      return slot.value === selectedTime
    })
    if (deliveryTimeSlots.length < 2) {
      return 0
    }
    return selected
  }

  pressHandler = (time, selectedTime, disableButton) => {
    if (!disableButton && (time !== selectedTime)) {
      this.props.handleOnPickerConfirm(time)
    }
  }

  render() {
    let {selectedTime, dinner, readOnly, deliveryTimeSlots, lunch} = this.props
    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 20,
      }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.setDeliveryTimeText}>Delivery Time</Text>
        </View>
        {!readOnly
          ?
          <View style={styles.wrapper}>
            {deliveryTimeSlots.length < 2 &&
            <View style={{ flex: 1 }}/>
            }
            {deliveryTimeSlots.map((slot, index) => {
              let disableButton = false
              const isDinner = slot.meal_tag === 'dinner'
              const isLunch = slot.meal_tag === 'lunch'
              if (isDinner && !dinner) {
                disableButton = true
                if (selectedTime === slot.value) {
                  selectedTime = ''
                  if (deliveryTimeSlots[index - 1]) {
                    this.pressHandler(deliveryTimeSlots[index - 1].value, selectedTime, false)
                  }
                }
              }
              if (isLunch && !lunch) {
                disableButton = true
                if (selectedTime === slot.value) {
                  selectedTime = ''
                  if (deliveryTimeSlots[index - 1]) {
                    this.pressHandler(deliveryTimeSlots[index - 1].value, selectedTime, false)
                  }
                }
              }
              else {
                if (!selectedTime) {
                  if (lunch && dinner) {
                    if (isLunch) {
                      this.pressHandler(slot.value, selectedTime, disableButton)
                    }
                  }
                  else if (lunch) {
                    this.pressHandler(slot.value, selectedTime, disableButton)
                  }
                  else if (dinner) {
                    this.pressHandler(slot.value, selectedTime, disableButton)
                  }
                
                }
              }

              let testID = 'lunchTime'
              if (isDinner) {
                testID = 'dinnerTime'
              }
              return (
                <TouchableWithoutFeedback
                  key={slot.value}
                  onPress={() => this.pressHandler(slot.value, selectedTime, disableButton)}
                  disabled={disableButton}
                  testID={testID}
                  accessibilityLabel={testID}
                >
                  <View
                    style={[
                      styles.button,
                      selectedTime === slot.value && styles.buttonSelected,
                      disableButton && styles.buttonDisabled,
                    ]}
                  >
                    <Text
                      style={[
                        styles.buttonText,
                        selectedTime === slot.value && styles.buttonSelectedText,
                        disableButton && styles.buttonDisabledText,
                      ]}
                    >
                      {slot.value}
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              )
            })}
          </View>
          : (
            <View>
              <Text style={{
                fontFamily: 'Rubik-Medium',
                fontSize: 14,
                color: Colors.brownishGrey,
              }}>{this.props.selectedTime}</Text>
            </View>
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    borderWidth: 1,
    borderColor: Colors.primaryOrange,
    marginLeft: scale(6),
    paddingHorizontal: scale(8),
    paddingVertical: verticalScale(8),
    alignItems: 'center',
    borderRadius: 3,
  },
  buttonSelected: {
    borderWidth: 0,
    backgroundColor: Colors.primaryOrange,
  },
  buttonDisabled: {
    borderWidth: 1,
    borderColor: Colors.disable,
  },
  buttonText: {
    color: Colors.primaryOrange,
    fontSize: scale(11),
    fontFamily: 'Rubik-Regular',
  },
  buttonSelectedText: {
    color: Colors.ricePaper,
  },
  buttonDisabledText: {
    color: Colors.disable,
  },
  wrapper: {
    flex: 2,
    flexDirection: 'row',
  },
  setDeliveryTimeText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownThree,
    fontSize: scale(14),
  },
})
