import React from 'react'
import ActionSheet from '../Common/ActionSheet'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('ActionSheet', () => {
  test('Var showActionSheet should return true', () => {
    const Platform = jest.requireActual('react-native/Libraries/Utilities/Platform')
    Platform.OS = 'android'
    let actionSheetButtons = [
      {
        text: 'FAQ',
        onPress: () => { return true },
      },
      {
        text: 'Contact',
        onPress: () => { return true },
      },
    ]
    let showActionSheet = false
    const onPress = () => {
      return showActionSheet = true
    }
    const actionSheet = shallow(
      <ActionSheet
        buttons={actionSheetButtons}
        visible={showActionSheet}
        dismiss={onPress}
      />,
    )
    actionSheet.find('TouchableWithoutFeedback')
      .props()['onPress']()
    expect(showActionSheet)
      .toBe(true)
  })

  test('Should render ActionSheet', () => {
    let actionSheetButtons = [
      {
        text: 'FAQ',
        onPress: () => { return true },
      },
      {
        text: 'Contact',
        onPress: () => { return true },
      },
    ]
    let showActionSheet = false
    const dismiss = () => {
      return showActionSheet = true
    }
    const render = renderer
      .create(
        <ActionSheet
          buttons={actionSheetButtons}
          visible={showActionSheet}
          dismiss={dismiss}
        />,
      )
      .toJSON()
    expect(render)
      .toMatchSnapshot()
  })
})
