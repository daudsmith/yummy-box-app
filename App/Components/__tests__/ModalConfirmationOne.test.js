import React from 'react'
import ModalConfirmationOne from '../../Components/ModalConfirmationOne'
import renderer from 'react-test-renderer'

describe('ModalConfirmationOne', () => {
    test('ModalConfirmationOne should match to snapshot', () => {
        const component = renderer.create(
            <ModalConfirmationOne
                showModal={true}
                message={`Are you sure want to update ${'\n'} your email address to ${'\n'}`}
                confirmationType='email'
                value={'email@domain.com'}
                onPressYes={() => {}}
                onPressNo={() => {}}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('ModalConfirmationOne should match to snapshot if props:confirmationType is not set', () => {
        const component = renderer.create(
            <ModalConfirmationOne
                showModal={true}
                message={'Are you sure?'}
                value={'email@domain.com'}
                onPressYes={() => {}}
                onPressNo={() => {}}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})