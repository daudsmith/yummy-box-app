import React from 'react'
import RoundedDropdown from '../RoundedDropdown'
import renderer from 'react-test-renderer'

describe('RoundedDropdown', () => {
  test('Should render RoundedDropdown', () => {
    const fakeData = [
      {
        index: 1,
        label: 'Satu',
      },
      {
        index: 2,
        label: 'Dua',
      },
    ]
    const tree = renderer
      .create(
        <RoundedDropdown
          label={'Test'}
          values={fakeData}
          selectedIndex={1}
          onPress={() => {}}
        />,
      )
      .toJSON()
    expect(tree)
      .toMatchSnapshot()
  })
})
