import React from 'react'
import DrawerButton from '../DrawerButton'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('DrawerButton', () => {
  test('Should render drawer button', () => {
    let text = 'Text'
    const onPress = () => {
      return text = 'Pressed Text'
    }
    const drawerButton = shallow(
      <DrawerButton
        text={text}
        onPress={onPress}
      />,
    )
    drawerButton.find(TouchableOpacity)
      .props()
      .onPress()

    expect(text)
      .toBe('Pressed Text')
  })
})
