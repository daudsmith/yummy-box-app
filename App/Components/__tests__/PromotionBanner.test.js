import React from 'react'
import PromotionBanner from '../../Components/PromotionBanner'
import renderer from 'react-test-renderer'

describe('PromotionBanner', () => {
    test('PromotionBanner should match to snapshot', () =>{
        const tree = renderer
        .create(
            <PromotionBanner
                banners={[
                    {
                        id: 1,
                        src: 'source.jpg'
                    },
                    {
                        id: '2',
                        src: 'source-2.jpg'
                    }
                ]}
                onPress={() => {}}
            />
        )
        .toJSON()
        expect(tree).toMatchSnapshot()
    })
})