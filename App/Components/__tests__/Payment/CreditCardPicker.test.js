import React from 'react'
import CreditCardPicker from '../../Payment/CreditCardPicker'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('CreditCardPicker', () => {
  let transactionType = 'type'
  const selectedCardId = 123
  let cards = [
    { id: 123, name: 'kartu1' },
    { id: 123, name: 'kartu2' },
  ]
  const fakeFunc = () => {
    return transactionType = 'changed'
  }
  test('Should render CreditCardPicker with card list', () => {
    const component = renderer.create(
      <CreditCardPicker transactionType={transactionType} selectedCardId={selectedCardId} cards={cards} toggleSaveCreditCard={fakeFunc} onRadioButtonPress={fakeFunc} />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should render CreditCardPicker with credit card form', () => {
    const component = renderer.create(
      <CreditCardPicker transactionType={transactionType} selectedCardId={selectedCardId} cards={[]} toggleSaveCreditCard={fakeFunc} onRadioButtonPress={fakeFunc} />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })
  test('Should give correct response on function execute', () => {

    const component = shallow(
      <CreditCardPicker transactionType={transactionType} setCreditCardAsPaymentInfo={fakeFunc} selectedCardId={selectedCardId} cards={cards} toggleSaveCreditCard={fakeFunc} onRadioButtonPress={fakeFunc} />,
    )
    component.find(TouchableOpacity).props().onPress()

    component.find('TouchableWithoutFeedback').at(1).props()['onPress']()

    expect(transactionType)
      .toBe('changed')
  })

})
