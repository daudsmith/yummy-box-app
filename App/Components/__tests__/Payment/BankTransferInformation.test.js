import React from 'react'
import BankTransferInformation from '../../../Components/Payment/BankTransferInformation'
import renderer from 'react-test-renderer'

describe('BankTransferInformation', () => {
    test('BankTransferInformation should match to snapshot', () => {
        const component = renderer.create(
            <BankTransferInformation />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})