import React from 'react'
import SectionHeader from '../../Layout/SectionHeader'
import renderer from 'react-test-renderer'
import { TextInput } from 'react-native'

describe('SectionHeader', () => {
  const container = {
    flex: 1,
    backgroundColor: '#fff',
  }


  const captionText = 'caption'
  test('SectionHeader should match to snapshot with style', () => {
    const component = renderer.create(
      <SectionHeader
        style={container}
        captionText={captionText}
        leftIcon={<TextInput value={'Test'} />}
        captionTextStyle={null}
        titleTextStyle={null}
        leftStyle={null}
        centerStyle={null}
        rightStyle={null}
        titleText={null}
        titleRight={null}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('SectionHeader should match to snapshot with captionText', () => {
    const component = renderer.create(
      <SectionHeader
      style={container}
      captionText={captionText}
      leftIcon={<TextInput value={'Test'} />}
      captionTextStyle={null}
      titleTextStyle={null}
      leftStyle={null}
      centerStyle={null}
      rightStyle={null}
      titleText={null}
      titleRight={null}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('SectionHeader should match to snapshot with leftIcon', () => {
    const component = renderer.create(
      <SectionHeader
        style={container}
        captionText={captionText}
        leftIcon={<TextInput value={'Test'} />}
        captionTextStyle={null}
        titleTextStyle={null}
        leftStyle={null}
        centerStyle={null}
        rightStyle={null}
        titleText={null}
        titleRight={null}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })



})
