import React from 'react'
import Section from '../../../Components/Layout/Section'
import renderer from 'react-test-renderer'

describe('Section', () => {
    test('Section should render enabled keyboard', () => {
        const component = renderer.create(
            <Section keyboard={true} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('Section should render given children', () => {
        const component = renderer.create(
            <Section><h1>Anak</h1></Section>
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('Section should render extended styling on props:full equals to TRUE', () => {
        const component = renderer.create(
            <Section full={true} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('Section should render extended styling on props:padder equals to TRUE', () => {
        const component = renderer.create(
            <Section padder={true} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('Section should render header object on props:header is provided', () => {
        const component = renderer.create(
            <Section header={<h1>HEADER</h1>} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('Section should render given props:styling', () => {
        const component = renderer.create(
            <Section style={{ margin: 0 }}/>
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})