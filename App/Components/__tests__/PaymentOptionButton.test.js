import React from 'react'
import PaymentOptionButton from '../Payment/PaymentOptionButton'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('AlertMessage', () => {
    test('Should return pressed true', () => {
        let walletPayment = 'wallet'
        let pressed = false
        const onPress = () => {
            return pressed = true
        }
        const paymentOptionButton = shallow(
            <PaymentOptionButton
                disable={false}
                selected={walletPayment === 'wallet'}
                icon='wallet'
                firstLineText='YumCredits'
                secondLineText='Rp. 20.000'
                onPress={onPress}
                testID='paymentOptionButton_wallet'
            />
        )

        paymentOptionButton.find(TouchableOpacity).props().onPress()

        expect(pressed).toBe(true)
    })

    test('Should render payment option button YumCredits', () => {
        let walletPayment = 'wallet'
        const onPress = () => {
            return true
        }
        renderer.create(
            <PaymentOptionButton
                disable={false}
                selected={walletPayment === 'wallet'}
                icon='wallet'
                firstLineText='YumCredits'
                secondLineText='Rp. 20.000'
                onPress={onPress}
                testID='paymentOptionButton_wallet'
            />
        )
        .toJSON()
        expect(renderer).toMatchSnapshot()
    })

    test('Should render payment option button credit card', () => {
        let ccPayment = 'credit-card'
        const onPress = () => {
            return true
        }
        renderer.create(
            <PaymentOptionButton
                disable={false}
                selected={ccPayment === 'credit-card'}
                icon='payment-cc'
                firstLineText='Credit Card'
                onPress={onPress}
                testID='paymentOptionButton_creditCard'
            />
        )
        .toJSON()
        expect(renderer).toMatchSnapshot()
    })

    test('Should render payment option button virtual-account', () => {
        let vaPayment = 'virtual-account'
        const onPress = () => {
            return true
        }
        renderer.create(
            <PaymentOptionButton
                disable={false}
                selected={vaPayment === 'virtual-account'}
                icon='transfer-icon'
                firstLineText='Bank'
                secondLineText='Transfer'
                onPress={onPress}
                testID='paymentOptionButton_virtualAccount'
            />
        )
        .toJSON()
        expect(renderer).toMatchSnapshot()
    })
})
