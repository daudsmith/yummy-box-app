import React from 'react'
import PrimaryButton from '../PrimaryButton'
import renderer from 'react-test-renderer'

describe('PrimaryButton', () => {
    test('Should render primary button position left with icon position left', () => {
        let isConnected = true
        const onPress = () => {
            return true
        }
        const render =  renderer
        .create(
            <PrimaryButton
                testID='testID' 
                position='left'
                label='Label'
                labelStyle={{color:'red'}}
                iconName='glass'
                iconColor={'gray'}
                iconSize={25}
                iconStyle={{color:'red'}}
                iconPosition='left'
                buttonStyle={{color:'red'}}
                disabled={isConnected}
                onPressMethod={onPress}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })

    test('Should render primary button position right with icon position right', () => {
        let isConnected = true
        const onPress = () => {
            return true
        }
        const render =  renderer
        .create(
            <PrimaryButton
                testID='testID' 
                position='right'
                label='Label'
                labelStyle={{color:'red'}}
                iconName='glass'
                iconColor={'gray'}
                iconSize={25}
                iconStyle={{color:'red'}}
                iconPosition='right'
                buttonStyle={{color:'red'}}
                disabled={isConnected}
                onPressMethod={onPress}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })

    test('Should render primary button position center with icon position center', () => {
        let isConnected = true
        const onPress = () => {
            return true
        }
        const render =  renderer
        .create(
            <PrimaryButton
                testID='testID' 
                position='center'
                label='Label'
                labelStyle={{color:'red'}}
                iconName='glass'
                iconColor={'gray'}
                iconSize={25}
                iconStyle={{color:'red'}}
                iconPosition='center'
                buttonStyle={{color:'red'}}
                disabled={isConnected}
                onPressMethod={onPress}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })

    test('Should not render primary button if isConnected is false', () => {
        let isConnected = false
        const onPress = () => {
            return true
        }
        const render =  renderer
        .create(
            <PrimaryButton
                testID='testID' 
                position='center'
                label='Label'
                labelStyle={{color:'red'}}
                iconName='glass'
                iconColor={'gray'}
                iconSize={25}
                iconStyle={{color:'red'}}
                iconPosition='center'
                buttonStyle={{color:'red'}}
                disabled={isConnected}
                onPressMethod={onPress}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })
})