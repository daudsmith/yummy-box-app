import React from 'react'
import AddressForm from '../AddressForm'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('AddressForm', () => {
  test('Should render drawer button', () => {
    const component = renderer.create(
      <AddressForm
        deliveryAddress="Jalan terus pantang mundur"
        phone="081273981273"
        name="Will Smith"
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response on deliveryAddressOptionPicker execute', () => {
    let pressed = false
    const onPress = () => {
      pressed = true
    }
    const component = shallow(
      <AddressForm
        deliveryAddress="Jalan terus pantang mundur"
        phone="081273981273"
        name="Will Smith"
        deliveryAddressOptionPicker={onPress}
      />,
    )

    component.find(TouchableOpacity)
      .first()
      .props()
      .onPress()

    expect(pressed)
      .toBe(true)
  })
})
