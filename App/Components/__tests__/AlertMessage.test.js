import React from 'react'
import AlertMessage from '../AlertMessage'
import renderer from 'react-test-renderer'

describe('AlertMessage', () => {
    test('Should return alert message if show is true', () => {
        const tree = renderer
        .create(
            <AlertMessage 
                title='Title'
                icon='Alert'
                style={{color:'red'}}
                show={true}
            />
        )
        .toJSON()
        expect(tree).toMatchSnapshot()
    })

    test('Alert message should return null if show is false', () => {
        const tree = renderer
        .create(
            <AlertMessage 
                title='Title'
                icon='Alert'
                style={{color:'red'}}
                show={false}
            />
        )
        .toJSON()
        expect(tree).toMatchSnapshot()
    })
})