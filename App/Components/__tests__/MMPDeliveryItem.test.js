import React from 'react'
import MMPDeliveryItem from '../MMPDeliveryItem'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('MMPDeliveryItem', () => {
  const fakeItem = {
    unit_price: 50000,
    detail: {
      data: {
        images: {
          medium_thumb: 'haha'
        }
      }
    }
  }
  test('Should render MMPDeliveryItem', () => {
    const component = renderer.create(
      <MMPDeliveryItem
        item={fakeItem}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should render MMPDeliveryItem with TouchableWithoutFeedback', () => {
    const onPress = () => { }
    const component = renderer.create(
      <MMPDeliveryItem
        item={fakeItem}
        subComponentOnPress={onPress}
        disableEdit={false}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response on onPress execute', () => {
    let pressed = false
    const onPress = () => {
      pressed = true
    }
    const component = shallow(
      <MMPDeliveryItem
        item={fakeItem}
        subComponentOnPress={onPress}
        disableEdit={false}
      />,
    )
    component.find('TouchableWithoutFeedback').props()['onPress']()
    expect(pressed).toBe(true)
  })
})
