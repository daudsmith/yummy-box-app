import React from 'react'
import LoadingModal from '../../Components/LoadingModal'
import renderer from 'react-test-renderer'

describe('LoadingModal', () => {
    test('LoadingModal should match to snapshot', () => {
        const component = renderer.create(
            <LoadingModal
                visible={true}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})