import React from 'react'
import IngredientsAndNutritionTab from '../IngredientsAndNutritionTab'
import renderer from 'react-test-renderer'

describe('IngredientsAndNutritionTab', () => {

  test('should render IngredientsAndNutritionTab', () => {
    const nutritions = {
      'data': [
        {
          'name': 'Nutrition Name A',
          'amount': 10,
          'unit': 'kCal'
        }
      ]
    }
    const ingredients = 'Fake ingredients text'
    const component = renderer.create(
      <IngredientsAndNutritionTab
        nutritions={nutritions}
        ingredients={ingredients}
      />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})
