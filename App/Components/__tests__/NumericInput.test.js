import React from 'react'
import NumericInput from '../Common/NumericInput'
import renderer from 'react-test-renderer'

describe('NumericInput', () => {
    test('Should render numeric input', () => {
        const render = renderer
        .create(
            <NumericInput 
                digits={2}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })
})