import React from 'react'
import BottomModal from '../../../Components/BottomModal/'
import renderer from 'react-test-renderer'

describe('BottomModal', () => {
    test('BottomModal should match to snapshot', () => {
        const component = renderer.create(
            <BottomModal
                visible={true}
                dismiss={() => {}}
                buttonText={'BottomModal buttonText'}
                title={'BottomModal title'}
                message={'BottomModal message'}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('BottomModal should match to snapshot if no props:visible given', () => {
        const component = renderer.create(
            <BottomModal
                dismiss={() => {}}
                buttonText={'BottomModal buttonText'}
                title={'BottomModal title'}
                message={'BottomModal message'}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})