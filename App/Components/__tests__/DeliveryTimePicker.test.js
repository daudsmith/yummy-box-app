import React from 'react'
import DeliveryTimePicker from '../../Components/DeliveryTimePicker'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('DeliveryTimePicker', () => {
  test('Should render DeliveryTimePicker', () => {
    const component = renderer.create(
      <DeliveryTimePicker
        selectedTime="09:00 - 12:00"
        deliveryTimeSlots={[
          {
            index: 0,
            value: '09:00 - 12:00',
            meal_tag: 'lunch'
          },
          {
            index: 1,
            value: '15:00 - 17:00',
            meal_tag: 'afternoon'
          },
        ]}
      />,
    ).toJSON()

    expect(component)
      .toMatchSnapshot()
  })

  test('Should correct return value on press DeliveryTimePicker', () => {
    let value = '09:00 - 12:00'
    const onPress = (newValue) => {
      value = newValue
    }

    const component = shallow(
      <DeliveryTimePicker
        selectedTime={value}
        deliveryTimeSlots={[
          {
            index: 0,
            value: '09:00 - 12:00',
            meal_tag: 'lunch'
          },
          {
            index: 1,
            value: '15:00 - 17:00',
            meal_tag: 'afternoon'
          },
          {
            index: 2,
            value: '18:00 - 19:00',
            meal_tag: 'dinner'
          },
        ]}
        handleOnPickerConfirm={onPress}
      />,
    )
    component.find('TouchableWithoutFeedback').at(1).props()['onPress']()
    expect(value).toBe('15:00 - 17:00')
  })
})
