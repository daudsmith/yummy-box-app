import React from 'react'
import SubscriptionProfile from '../SubscriptionProfile'
import renderer from 'react-test-renderer'

describe('SubscriptionProfile', () => {
  const theme = 'theme'
  const startOn = 'startOn'
  const repeatEvery = 'repeatEvery'
  const repeatInterval = 'repeatInterval'

  test('Should render SubscriptionProfile with paymentMethod Credit Card', () => {
    const component = renderer.create(
      <SubscriptionProfile theme={theme} startOn={startOn} repeatEvery={repeatEvery} repeatInterval={repeatInterval} paymentMethod='credit-card' />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })
  test('Should render SubscriptionProfile with paymentMethod YumCredits', () => {
    const component = renderer.create(
      <SubscriptionProfile theme={theme} startOn={startOn} repeatEvery={repeatEvery} repeatInterval={repeatInterval} paymentMethod='yumcredit' />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

})
