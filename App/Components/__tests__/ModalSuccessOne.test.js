import React from 'react'
import ModalSuccessOne from '../ModalSuccessOne'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('ModalSuccessOne', () => {
  test('Should render show modal', () => {
    const component = renderer.create(
      <ModalSuccessOne
        message="message"
        showModal={true}
      />,
    )
    let tree = component.toJSON()
      expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response on onPress execute', () => {
    let pressed = false
    const onPress = () => {
      pressed = true
    }
    const component = shallow(
      <ModalSuccessOne
        message="message"
        showModal={true}
        onPress={onPress}
      />,
    )
    component.find(TouchableOpacity).props().onPress()

    expect(pressed).toBe(true)
  })
})
