import React from 'react'
import MMPPaymentInformation from '../MMPPaymentInformation'
import renderer from 'react-test-renderer'

describe('MMPPaymentInformation', () => {
  const fakePaymentInfo = {
    method: 'credit-card',
    status: 'COMPLETE',
    total_amount_charged: 10000,
    total_amount_due: 0,
    payment_info_json: '{"created":"2019-11-20T10:27:37.943Z","business_id":"599bf5f1ccab55b020bb115f","authorized_amount":30000,"external_id":"S191120-00002-1574245655","merchant_id":"xendit_ctv_agg","merchant_reference_code":"5dd51519498a2440aad6c75b","masked_card_number":"400000XXXXXX0002","charge_type":"MULTIPLE_USE_TOKEN","card_brand":"VISA","card_type":"CREDIT","descriptor":"YUMMY CORP.","status":"CAPTURED","bank_reconciliation_id":"5742456574856299903012","eci":"7","capture_amount":30000,"currency":"IDR","id":"5dd51519498a2440aad6c75d","mid_label":"YUMMYCORP_CTV_TEST"}'
  }
  test('MMPPaymentInformation should render if PropTypes:paymentMethodType is credit-card', () => {
    const paymentMethodType = 'credit-card'
    const cardImage = ''
    const creditCardNumber = ''
    const amount = 10000
    const paymentInfo = {...fakePaymentInfo}
    const component = renderer.create(
      <MMPPaymentInformation
        paymentMethodType={paymentMethodType}
        cardImage={cardImage}
        creditCardNumber={creditCardNumber}
        amount={amount}
        paymentInfo={paymentInfo}
      />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
  test('MMPPaymentInformation should render if PropTypes:paymentMethodType is yummycredits', () => {
    const paymentMethodType = 'wallet'
    const cardImage = ''
    const creditCardNumber = ''
    const amount = 10000
    const paymentInfo = {...fakePaymentInfo, payment_info_json: '', method: paymentMethodType}
    const component = renderer.create(
      <MMPPaymentInformation
        paymentMethodType={paymentMethodType}
        cardImage={cardImage}
        creditCardNumber={creditCardNumber}
        amount={amount}
        paymentInfo={paymentInfo}
      />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
  test('MMPPaymentInformation should render if PropTypes:paymentMethodType is virtual-account', () => {
    const paymentMethodType = 'virtual-account'
    const cardImage = ''
    const creditCardNumber = ''
    const amount = 10000
    const paymentInfo = {
      ...fakePaymentInfo,
      payment_info_json: '{"id":"5dd22d23d52c3876479c534f","external_id":"S191118-00002","user_id":"599bf5f1ccab55b020bb115f","status":"PENDING","merchant_name":"Yummy Corp.","merchant_profile_picture_url":"https:\\/\\/xnd-companies.s3.amazonaws.com\\/prod\\/1555929006343_177.png","amount":30000,"payer_email":"fasyagesiana+11@gmail.com","description":"Invoice for S191118-00002","expiry_date":"2019-11-18T08:33:22.911Z","invoice_url":"https:\\/\\/invoice.xendit.co\\/web\\/invoices\\/5dd22d23d52c3876479c534f","available_banks":[{"bank_code":"BRI","collection_type":"POOL","bank_account_number":"262151661235895","transfer_amount":30000,"bank_branch":"Virtual Account","account_holder_name":"YUMMY CORP.","identity_amount":0},{"bank_code":"MANDIRI","collection_type":"POOL","bank_account_number":"889081661342732","transfer_amount":30000,"bank_branch":"Virtual Account","account_holder_name":"YUMMY CORP.","identity_amount":0},{"bank_code":"BNI","collection_type":"POOL","bank_account_number":"8808166101826932","transfer_amount":30000,"bank_branch":"Virtual Account","account_holder_name":"YUMMY CORP.","identity_amount":0},{"bank_code":"BCA","collection_type":"POOL","bank_account_number":"10463228548","transfer_amount":30000,"bank_branch":"Virtual Account","account_holder_name":"YUMMY CORP.","identity_amount":0}],"available_ewallets":[],"should_exclude_credit_card":true,"should_send_email":false,"created":"2019-11-18T05:33:23.122Z","updated":"2019-11-18T05:33:23.122Z","currency":"IDR"}',
      method: paymentMethodType
    }
    const component = renderer.create(
      <MMPPaymentInformation
        paymentMethodType={paymentMethodType}
        cardImage={cardImage}
        creditCardNumber={creditCardNumber}
        amount={amount}
        paymentInfo={paymentInfo}
      />
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})
