import React from 'react'
import MenuSlider from '../Common/MenuSlider'
import renderer from 'react-test-renderer'

describe('MenuSlider', () => {
    test('Should render Menu Slider component', () => {
        let items = [
            {
                id: 1,
                name: 'Meal name'
            },
            {
                id: 2,
                name: 'Meal name 2'
            }
        ]
        const render = renderer
        .create(
            <MenuSlider
                style={{
                    paddingTop: 16,
                    paddingLeft: 20,
                    paddingBottom: 0,
                    paddingRight: 20,
                    top: -8,
                    left: -4,
                }}
                items={items}
                renderItem={jest.fn()}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })
})