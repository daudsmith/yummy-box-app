import React from 'react'
import Moment from 'moment'
import MealListSlider from '../MealListSlider'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('MealListSlider', () => {
  const stringDate = '2019-10-23'
  const date = Moment(stringDate)

  let meals = [
    { id: 1, sale_price: 123, base_price: 123, name: 'anto', item_id: 1, date },
    { id: 2, sale_price: 123, base_price: 123, name: 'anto', item_id: 2, date },
  ]
  const theme = 'playlist'
  const lastItemPadding = 25
  let text = 'text'
  const changed = 'changed'
  const onPress = () => {
    return text = changed
  }

  test('Should render MealListSlider', () => {
    const component = renderer.create(
      <MealListSlider meals={meals} onPress={onPress} lastItemPadding={lastItemPadding} theme={theme} date={stringDate} />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response onPress execute', () => {
    const component = shallow(
      <MealListSlider meals={meals} onPress={onPress} lastItemPadding={lastItemPadding} theme={theme} date={stringDate} />,
    )
    component.find('TouchableWithoutFeedback').at(1).props()['onPress']()
    expect(text).toBe(changed)
  })
})
