import React from 'react'
import AlertBox from '../AlertBox'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('AlertBox', () => {
  test('AlertBox only Primary text', () => {
    let pressed = false
    let dismiss = false
    const onPress = () => {
      pressed = true
    }
    const onDismiss = () => {
      dismiss = true
    }

    const component = shallow(
      <AlertBox
        primaryAction={onPress}
        dismiss={onDismiss}
        text={'Alert Box Opened'}
        primaryActionText={'Ok'}
      />,
    )
    component.find(TouchableOpacity)
      .first()
      .props()
      .onPress()

    expect(pressed)
      .toBe(true)

    expect(dismiss)
      .toBe(true)
  })
  test('AlertBox with Secondary text', () => {
    let pressed = false
    let secondary = false
    let dismiss = false
    const onPress = () => {
      pressed = true
    }
    const onPressSecondary = () => {
      secondary = true
    }
    const onDismiss = () => {
      dismiss = true
    }

    const component = shallow(
      <AlertBox
        primaryAction={onPress}
        secondaryAction={onPressSecondary}
        dismiss={onDismiss}
        text={'Alert Box Opened'}
        primaryActionText={'Ok'}
        secondaryActionText={'Alternative'}
      />,
    )

    component.find(TouchableOpacity)
      .at(0)
      .props()['onPress']()

    expect(pressed)
      .toBe(true)

    expect(dismiss)
      .toBe(true)

    component.find(TouchableOpacity)
      .at(1)
      .props()
      .onPress()

    expect(secondary)
      .toBe(true)

    expect(dismiss)
      .toBe(true)
  })
})
