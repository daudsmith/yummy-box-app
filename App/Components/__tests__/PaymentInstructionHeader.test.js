import React from 'react'
import PaymentInstructionHeader from '../PaymentInstructionHeader'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('PaymentInstructionHeader', () => {
  test('Should render PaymentInstructionHeader', () => {
    const onCopy = () => {}
    const component = renderer.create(
      <PaymentInstructionHeader
        accountNumber="12345"
        onCopy={onCopy}
      />
    ).toJSON()
    expect(component)
      .toMatchSnapshot()
  })

  test('Should correct return value on press copy on PaymentInstructionHeader', () => {
    let value = false
    const onCopy = () => {
      value = true
    }

    const component = shallow(
      <PaymentInstructionHeader
        accountNumber="12345"
        onCopy={onCopy}
      />
    )
    component.find('PrimaryButton').at(0).props()['onPressMethod']()
    expect(value).toBe(true)
  })
})
