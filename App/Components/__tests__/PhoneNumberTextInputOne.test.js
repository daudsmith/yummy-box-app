import React from 'react'
import PhoneNumberTextInputOne from '../PhoneNumberTextInputOne'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('PhoneNumberTextInputOne', () => {
  let phone = '6283812939391'
  let showMessage = {
    iconColor: 'blue',
    iconName: 'music',
    iconText: 'text'
  }

  const setRef = (ref) => {
    return ref
  }
  const onChangePhoneNumber = (phone) => {
    return phone
  }
  test('Should render PhoneNumberTextInputOne', () => {
    const component = renderer.create(
      <PhoneNumberTextInputOne setRef={setRef} onChangePhoneNumber={onChangePhoneNumber('6283877667271')} value={phone} showMessage={showMessage} />,
    )
    let tree = component.toJSON()
      expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response on close execute', () => {

    const component = shallow(
      <PhoneNumberTextInputOne setRef={setRef} onChangePhoneNumber={onChangePhoneNumber} value={phone} showMessage={showMessage} />,
    )
    component.find('PhoneInput').props()['onChangePhoneNumber'](phone)
    expect(phone).toBe(phone)
  })
})
