import React from 'react'
import PhoneNumberTextInput from '../PhoneNumberTextInput'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('PhoneNumberTextInput', () => {
  let phone = '6283812939391'

  const setRef = (ref) => {
    return ref
  }
  const onChange = (phone) => {
    return phone
  }
  test('Should render PhoneNumberTextInput', () => {
    const component = renderer.create(
      <PhoneNumberTextInput setRef={setRef} onChange={onChange('6283877667271')} value={phone} />,
    )
    let tree = component.toJSON()
      expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response on onChangePhoneNumber execute', () => {

    const onChangePhoneNumber = jest.spyOn(PhoneNumberTextInput.prototype, 'onChangePhoneNumber')
    onChangePhoneNumber.mockImplementation(() => phone)

    const component = shallow(
      <PhoneNumberTextInput setRef={setRef} onChange={onChange} value={phone} />,
    )
    component.find('PhoneInput')
      .props()['onChangePhoneNumber'](phone)

    expect(phone).toBe(phone)
  })
})
