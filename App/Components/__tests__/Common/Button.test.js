import React from 'react'
import Button from '../../../Components/Common/Button'
import renderer from 'react-test-renderer'

describe('Button', () => {
    test('Button should match to snapshot if', () => {
        const component = renderer.create(
            <Button
                text={'button text'}
                testID={'1'}
                onPress={() => {}}
                buttonStyle={{ margin: 0 }}
                textStyle={{ margin: 0 }}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('Button should match to snapshot if no props are given', () => {
        const component = renderer.create(
            <Button/>
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})