import React from 'react'
import MealCard from '../../../Components/Common/MealCard'
import { Images } from '../../../Themes'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('MealCard', () => {
    test('MealCard should match to snapshot if given props:image', () => {
        const component = renderer.create(
            <MealCard image={'http://omni.yummycorp.com/static/media/secure_login.49d4cf90.png'} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should render showingSoon image if no props:image given', () => {
        const viewMealCard = shallow(
            <MealCard image={null} />
        )
        expect(viewMealCard.find('Image').props()['source']).toBe(Images.showingSoon)
    })
    test('MealCard should match to snapshot if props:caption given', () => {
        const component = renderer.create(
            <MealCard caption={'card-caption'} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:title given', () => {
        const component = renderer.create(
            <MealCard title={'card-title'} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:description given', () => {
        const component = renderer.create(
            <MealCard description={'card-description'} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:onPress given', () => {
        const component = renderer.create(
            <MealCard onPress={() => {}} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:style given', () => {
        const component = renderer.create(
            <MealCard style={{ margin: 0 }} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:imageContainerStyle given', () => {
        const component = renderer.create(
            <MealCard imageContainerStyle={{ margin: 0 }} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:imageStyle given', () => {
        const component = renderer.create(
            <MealCard imageStyle={{ margin: 0 }} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:contentStyle given', () => {
        const component = renderer.create(
            <MealCard contentStyle={{ margin: 0 }} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:captionStyle given', () => {
        const component = renderer.create(
            <MealCard captionStyle={{ margin: 0 }} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:titleContainerStyle given', () => {
        const component = renderer.create(
            <MealCard titleContainerStyle={{ margin: 0 }} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:titleStyle given', () => {
        const component = renderer.create(
            <MealCard titleStyle={{ margin: 0 }} />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('MealCard should match to snapshot if props:noShadow given', () => {
        const component = renderer.create(
            <MealCard noShadow />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

})
