import React from 'react'
import TextInput from '../../Common/TextInput'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import {TextInput as RNTextInput} from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('Common/TextInput', () => {
  test('should render TextInput', () => {
    let defaultText = 'text'
    let changedText = 'changed'

    const onChangeText = text => {
      return (defaultText = text)
    }
    const onSubmitEditing = () => {
      return (defaultText = changedText)
    }
    const onBlur = () => {
      return (defaultText = changedText)
    }
    const component = shallow(
      <TextInput
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
        onBlur={onBlur}
      />
    )
    component
      .find(RNTextInput)
      .props()
      .onChangeText(changedText)

    expect(defaultText).toBe(changedText)

    component
      .find(RNTextInput)
      .props()
      .onSubmitEditing()

    expect(defaultText).toBe(changedText)

    component.find(RNTextInput).props().onBlur

    expect(defaultText).toBe(changedText)
  })
})
