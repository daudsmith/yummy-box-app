import React from 'react'
import FlexButton from '../../Common/FlexButton'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('Common/FlexButton', () => {

  test('should render FlexButton', () => {
    let text = 'text'
    let changedText = 'changed'
    const onPress = () => {
      return text = changedText
    }
    const flexButton = shallow(
      <FlexButton
        onPress={onPress}
      />
    )

    flexButton.find(TouchableOpacity).props().onPress()

    expect(text).toBe(changedText)
  })

})
