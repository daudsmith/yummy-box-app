import WithSafeAreaView from '../../../Components/Common/WithSafeAreaView'
import LoadingModal from '../../../Components/LoadingModal'
import renderer from 'react-test-renderer'

describe('WithSafeAreaView', () => {
    test('WithSafeAreaView should match to snapshot', () => {
        const component = renderer.create(
            WithSafeAreaView(LoadingModal)
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})
