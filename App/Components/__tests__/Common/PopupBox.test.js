import React from 'react'
import PopupBox from '../../../Components/Common/PopupBox'
import renderer from 'react-test-renderer'

describe('PopupBox', () => {
    test('PopupBox should match to snapshot', () => {
        const component = renderer.create(
            <PopupBox
                title={'PopupBox Title'}
                content={'PopupBox Content'}
                contentButton={{
                    text: 'button text',
                    onPress: () => {}
                }}
                visible={true}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

    test('PopupBox should match to snapshot if props:contentButton is null', () => {
        const component = renderer.create(
            <PopupBox
                title={'PopupBox Title'}
                content={'PopupBox Content'}
                contentButton={null}
                visible={true}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

    test('PopupBox should match to snapshot if props:content type is function', () => {
        const component = renderer.create(
            <PopupBox
                title={'PopupBox Title'}
                content={() => {}}
                contentButton={null}
                visible={true}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})