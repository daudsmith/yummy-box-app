import React from 'react'
import AddButton from '../../../../Components/Common/AddToCart/AddButton'
import renderer from 'react-test-renderer'

describe('AddButton', () => {
    test('Addbuton should match to snapshot if props:label is provided', () => {
        const component = renderer.create(
            <AddButton
                label={'AddButton-Label'}
                onPress={() => {}}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('Addbuton should match to snapshot if no props:label is provided', () => {
        const component = renderer.create(
            <AddButton
                onPress={() => {}}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})