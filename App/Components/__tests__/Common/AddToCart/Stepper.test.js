import React from 'react'
import Stepper from '../../../../Components/Common/AddToCart/Stepper'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('Stepper', () => {
  test('Stepper should match to snapshot if props:quantity is provided', () => {
    const component = renderer.create(
      <Stepper
        quantity={1}
        onIncrement={() => {}}
        onDecrement={() => {}}
      />,
    )
      .toJSON()
    expect(component)
      .toMatchSnapshot()
  })
  test('Stepper should give quantity 2 on increment pressed', () => {
    let quantity = 1
    const onIncrement = () => {
      quantity += 1
    }
    const component = shallow(
      <Stepper
        quantity={1}
        onIncrement={onIncrement}
        onDecrement={() => {}}
      />,
    )
    component.find(TouchableOpacity)
      .at(1)
      .props()
      .onPress()

    expect(quantity)
      .toBe(2)
  })
  test('Stepper should give quantity 2 on decrement pressed', () => {
    let quantity = 1
    const onDecrement = () => {
      quantity -= 1
    }
    const component = shallow(
      <Stepper
        quantity={1}
        onIncrement={() => {}}
        onDecrement={onDecrement}
      />,
    )
    component.find(TouchableOpacity)
      .at(0)
      .props()
      .onPress()

    expect(quantity)
      .toBe(0)
  })
})
