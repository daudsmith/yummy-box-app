import React from 'react'
import moment from 'moment'
import AddToCart from '../../../Common/AddToCart'
import renderer from 'react-test-renderer'

jest.mock('../../../Common/AddToCart/Stepper', () => 'Stepper')
jest.mock('../../../Common/AddToCart/AddButton', () => 'AddButton')

describe('AddToCartIndex', () => {
    test('Should render add to cart index component', () => {
        let item = {
            sku: 'SKU01',
            name: 'Item Name'
        }
        const navigation = {
          state: { params: {} },
          dispatch: jest.fn(),
          goBack: jest.fn(),
          dismiss: jest.fn(),
          navigate: jest.fn(),
          openDrawer: jest.fn(),
          closeDrawer: jest.fn(),
          toggleDrawer: jest.fn(),
          getParam: jest.fn(),
          setParams: jest.fn(),
          addListener: jest.fn(),
          push: jest.fn(),
          replace: jest.fn(),
          pop: jest.fn(),
          popToTop: jest.fn(),
          isFocused: jest.fn()
        }
        const render = renderer
        .create(
            <AddToCart
                quantity={2}
                item={item}
                mealDate={moment('2019-11-22')}
                onIncrement={jest.fn()}
                max={20}
                onDecrement={jest.fn()}
                onMaxItemEvent={jest.fn()}
                navigation={navigation}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })
})
