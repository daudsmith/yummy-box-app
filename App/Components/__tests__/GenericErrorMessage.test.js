import React from 'react'
import GenericErrorMessage from '../GenericErrorMessage'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('GenericErrorMessage', () => {
  test('Should render GenericErrorMessage', () => {
    const component = renderer.create(
      <GenericErrorMessage />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response on close execute', () => {
    let text = 'text'
    let changedText = 'changed'
    const close = () => {
      return text = changedText
    }
    const component = shallow(
      <GenericErrorMessage
        close={close}
      />,
    )
    component.find('TouchableWithoutFeedback')
      .props()['onPress']()
    expect(text)
      .toBe(changedText)
  })
})
