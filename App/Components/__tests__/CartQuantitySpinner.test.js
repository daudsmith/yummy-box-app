import React from 'react'
import CartQuantitySpinner from '../CartQuantitySpinner'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('CartQuantitySpinner', () => {
  test('Should render CartQuantitySpinner', () => {
    let fakeItem = {
      id: 1,
      name: 'Menu 1',
      quantity: 0,
      remaining_daily_quantity: 30
    }
    const component = renderer.create(
      <CartQuantitySpinner
        handleIncreasePress={() => {}}
        handleDecreasePress={() => {}}
        item={fakeItem}
        date="2019-09-09"
        toggleAlertBox={() => {}}
        showErrorModal={false}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should give 1 response on handleIncreasePress press once', () => {
    let fakeItem = {
      id: 1,
      name: 'Menu 1',
      quantity: 0,
      remaining_daily_quantity: 30
    }
    const onPressUp = () => {
      fakeItem.quantity += 1
      fakeItem.remaining_daily_quantity -= 1
    }
    const component = shallow(
      <CartQuantitySpinner
        handleIncreasePress={onPressUp}
        handleDecreasePress={() => {}}
        item={fakeItem}
        date="2019-09-09"
        toggleAlertBox={() => {}}
        showErrorModal={false}
      />,
    )
    component.find('TouchableWithoutFeedback').at(1).props()['onPress']()
  expect(fakeItem.quantity).toBe(1)
  })

  test('Should give max value response on handleIncreasePress exceeding max', () => {
    let fakeItem = {
      id: 1,
      name: 'Menu 1',
      quantity: 0,
      remaining_daily_quantity: 30
    }
    const max = 7
    const onPressUp = () => {
      fakeItem.quantity += 1
      fakeItem.remaining_daily_quantity -= 1
    }
    const component = shallow(
      <CartQuantitySpinner
        handleIncreasePress={onPressUp}
        handleDecreasePress={() => {}}
        item={fakeItem}
        date="2019-09-09"
        max={max}
        toggleAlertBox={() => {}}
        showErrorModal={true}
      />,
    )

    for (let i = 0; i < 7; i++) {
      component.find('TouchableWithoutFeedback').at(1).props()['onPress']()
    }

  expect(fakeItem.quantity).toBe(max)
  })

  test('Should give 0 response on handleDecreasePress press', () => {
    let fakeItem = {
      id: 1,
      name: 'Menu 1',
      quantity: 0,
      remaining_daily_quantity: 30
    }
    const onPressDown = () => {
      fakeItem.quantity -= 1
      if (fakeItem.quantity < 0) {
        fakeItem.quantity = 0
      } else {
        fakeItem.remaining_daily_quantity += 1
      }
    }
    const component = shallow(
      <CartQuantitySpinner
        handleIncreasePress={() => {}}
        handleDecreasePress={onPressDown}
        item={fakeItem}
        date="2019-09-09"
        toggleAlertBox={() => {}}
        showErrorModal={false}
      />,
    )
    component.find('TouchableWithoutFeedback').at(0).props()['onPress']()
  expect(fakeItem.quantity).toBe(0)
  })

  test('Should give 4 response on handleDecreasePress after handleIncreasePress 5 times', () => {
    let fakeItem = {
      id: 1,
      name: 'Menu 1',
      quantity: 0,
      remaining_daily_quantity: 30
    }
    const max = 20
    const onPressUp = () => {
      fakeItem.quantity += 1
      fakeItem.remaining_daily_quantity -= 1
    }
    const onPressDown = () => {
      fakeItem.quantity -= 1
      if (fakeItem.quantity < 0) {
        fakeItem.quantity = 0
      } else {
        fakeItem.remaining_daily_quantity += 1
      }
    }
    const component = shallow(
      <CartQuantitySpinner
        handleIncreasePress={onPressUp}
        handleDecreasePress={onPressDown}
        item={fakeItem}
        date="2019-09-09"
        max={max}
        toggleAlertBox={() => {}}
        showErrorModal={false}
      />,
    )

    for (let i = 0; i < 5; i++) {
      component.find('TouchableWithoutFeedback').at(1).props()['onPress']()
    }

    component.find('TouchableWithoutFeedback').at(0).props()['onPress']()
    expect(fakeItem.quantity).toBe(4)
  })
})
