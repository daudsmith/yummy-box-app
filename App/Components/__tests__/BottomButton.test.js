import React from 'react'
import BottomButton from '../BottomButton'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('BottomButton', () => {

  test('should render BottomButton', () => {
    let text = 'text'
    let changedText = 'changed'

    const onPress = () => {
      return text = changedText
    }

    const bottomButton = shallow(
      <BottomButton
        buttonText={text}
        pressHandler={onPress}
      />,
    )

    bottomButton.find(TouchableOpacity).props().onPress()

    expect(text)
      .toBe('changed')
  })
})
