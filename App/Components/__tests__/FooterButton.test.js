import React from 'react'
import FooterButton from '../FooterButton'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('FooterButton', () => {
    test('Should render enabled footer button', () => {
        let disabled = true
        const onPress = () => {
            return disabled = false
        }
        const footerButton = shallow(
            <FooterButton
                text='Text'
                disabled={disabled}
                noIcon={false}
                onPress={onPress}
            />
        )
        footerButton.find('TouchableWithoutFeedback').props()['onPress']()
        expect(disabled).toBe(false)
    })

    test('Should render disabled footer button', () => {
        let disabled = false
        const onPress = () => {
            return disabled = true
        }
        const footerButton = shallow(
            <FooterButton
                text='Text'
                disabled={disabled}
                noIcon={false}
                onPress={onPress}
            />
        )
        footerButton.find('TouchableWithoutFeedback').props()['onPress']()
        expect(disabled).toBe(true)
    })
})
