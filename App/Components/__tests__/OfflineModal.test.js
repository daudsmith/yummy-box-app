import React from 'react'
import OfflineModal from '../OfflineModal'
import renderer from 'react-test-renderer'

describe('OfflineModal', () => {
    test('OfflineModal should visibile if isConnected is false', () => {
        const render = renderer
        .create(
            <OfflineModal 
                isConnected={false}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })

    test('OfflineModal should not visible if isConnected is true', () => {
        const render = renderer
        .create(
            <OfflineModal 
                isConnected={true}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })
})