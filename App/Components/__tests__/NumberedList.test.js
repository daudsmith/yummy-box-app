import React from 'react'
import NumberedList from '../NumberedList'
import { Text } from 'react-native'
import renderer from 'react-test-renderer'

const styles = {
    contentBulletStyle : {
        flexDirection:'row'
    },
    contentBulletTextStyle : {
        left:10
    },
    contentStyle : {
        left:10
    }
}

describe('NumberedList', () => {
    test('Should render Numbered List component', () => {
        const tree = renderer
        .create(
            <NumberedList
                bulletStyle={styles.contentBulletStyle}
                bulletTextStyle={styles.contentBulletTextStyle}
                contentStyle={styles.contentStyle} 
                number={'1.'}
                content={<Text>Test content</Text>}
            />
        )
        .toJSON()
        expect(tree).toMatchSnapshot()
    })
})