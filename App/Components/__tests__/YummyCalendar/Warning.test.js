import React from 'react'
import Warning from '../..//YummyCalendar/Warning'
import renderer from 'react-test-renderer'

describe('Warning', () => {

  test('Should render Warning if message is has value', () => {
    const message = 'message'
    const component = renderer.create(
      <Warning message={message} />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })


})
