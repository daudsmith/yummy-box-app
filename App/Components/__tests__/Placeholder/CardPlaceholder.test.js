import React from 'react'
import CardPlaceholder from '../../Placeholder/CardPlaceholder'
import renderer from 'react-test-renderer'


describe('CardPlaceholder', () => {
    test('CardPlaceholder with input type number, should match to snapshots', () => {
        const CardPlaceholderprops = {
            height: 3,
            width: 300,
            style: {
                margin: 0
            }
        }
        const component = renderer.create(
            <CardPlaceholder
                {...CardPlaceholderprops}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

    test('CardPlaceholder with input type string, should match to snapshots', () => {
        const CardPlaceholderprops = {
            height: '3',
            width: '300',
            style: {
                margin: 0
            }
        }
        const component = renderer.create(
            <CardPlaceholder
                {...CardPlaceholderprops}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

    test('CardPlaceholder with style props provided, should match to snapshots', () => {
        const CardPlaceholderprops = {
            height: '3',
            width: '300',
            style: {
                margin: 0
            }
        }
        const component = renderer.create(
            <CardPlaceholder
                {...CardPlaceholderprops}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})