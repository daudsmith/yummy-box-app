import React from 'react'
import ListPlaceholder from '../../Placeholder/ListPlaceholder'
import renderer from 'react-test-renderer'

describe('ListPlaceholder', () => {
  const style = {
    flex: 1,
    backgroundColor: '#fff',
  }
  test('Should render ListPlaceholder', () => {
    const component = renderer.create(
      <ListPlaceholder
        style={style}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

})
