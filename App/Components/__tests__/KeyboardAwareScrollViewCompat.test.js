import React from 'react'
import { TextInput } from 'react-native'
import KeyboardAwareScrollViewCompat from '../KeyboardAwareScrollViewCompat'
import renderer from 'react-test-renderer'

describe('NumericInput', () => {
    test('Should render text input in KeyboardAvoidingView', () => {
        const tree = renderer
        .create(
            <KeyboardAwareScrollViewCompat 
                children={<TextInput value={'Test'}/>}
            />
        )
        .toJSON()
        expect(tree).toMatchSnapshot()
    })
})