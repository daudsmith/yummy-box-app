import React from 'react'
import YummyboxIcon from '../YummyboxIcon'
import renderer from 'react-test-renderer'

describe('YummyboxIcon', () => {
    test('Should render yummybox icon', () => {
        const render = renderer
        .create(
            <YummyboxIcon/>
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })
})