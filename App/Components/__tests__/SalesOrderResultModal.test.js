import React from 'react'
import SalesOrderResultModal from '../../Components/SalesOrderResultModal'
import renderer from 'react-test-renderer'

describe('SalesOrderResultModal', () => {
    test('SalesOrderResultModal should match to snapshot if props:visible equals to true', () => {
        const component = renderer.create(
            <SalesOrderResultModal
                visible={true}
                onPress={() => {}}
                modalType={'thank-you'}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

    test('SalesOrderResultModal should match to snapshot if props:modalType is empty', () => {
        const component = renderer.create(
            <SalesOrderResultModal
                visible={true}
                onPress={() => {}}
                modalType={''}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})