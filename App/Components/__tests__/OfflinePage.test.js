import React from 'react'
import OfflinePage from '../OfflinePage'
import renderer from 'react-test-renderer'

describe('AddressForm', () => {
  test('Should render drawer button', () => {
    const component = renderer.create(
      <OfflinePage />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })
})
