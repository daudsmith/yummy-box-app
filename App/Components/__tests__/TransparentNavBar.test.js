import React from 'react'
import TransparentNavBar from '../../Components/TransparentNavBar'
import renderer from 'react-test-renderer'

const mockProps = {
    navigation: { navigate: jest.fn() },
    loginSuccess: jest.fn(),
    isSubmitting: true
}

describe('TransparentNavBar', () => {
    test('TransparentNavBar should match to snapshot if props:showedModally equals to true', () => {
        const component = renderer.create(
            <TransparentNavBar
                showedModally={true}
                onPress={() => {}}
                {...mockProps}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

    test('TransparentNavBar should match to snapshot if props:showedModally equals to false', () => {
        const component = renderer.create(
            <TransparentNavBar
                showedModally={false}
                onPress={() => {}}
                {...mockProps}
            />
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })

})