import React from 'react'
import AuthBackgroundView from '../../../Components/Authentication/AuthBackgroundView'
import Button from '../../../Components/Common/Button'
import renderer from 'react-test-renderer'

describe('AuthBackgroundView', () => {
    test('AuthBackgroundView should match to snapshot', () => {
        const component = renderer.create(
            <AuthBackgroundView
                boxTitle={'AuthBackgroundView boxTitle'}
                boxSubtext={'AuthBackgroundView boxSubtext'}
                boxBottomComponent={<Button/>}
                dismiss={() => {}}
            >
                <Button />
            </AuthBackgroundView>
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('AuthBackgroundView should match to snapshot if no props:boxBottomComponent given', () => {
        const component = renderer.create(
            <AuthBackgroundView
                boxTitle={'AuthBackgroundView boxTitle'}
                boxSubtext={'AuthBackgroundView boxSubtext'}
                dismiss={() => {}}
            >
                <Button />
            </AuthBackgroundView>
        )
        let tree = component.toJSON()
        expect(tree).toMatchSnapshot()
    })
})