import React from 'react'
import AuthenticationBox from '../../Authentication/AuthenticationBox'
import renderer from 'react-test-renderer'
import { TextInput } from 'react-native'

describe('AuthenticationBox', () => {
  const title = 'title'
  const subtext = 'hai'


  const fakeComponent = <TextInput value={'Test'} />

  test('Should render AuthenticationBox', () => {
    const component = renderer.create(
      <AuthenticationBox title={title} subtext={subtext} children={fakeComponent} />
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should render AuthenticationBox with boxBottomComponent', () => {
    const component = renderer.create(
      <AuthenticationBox title={title} subtext={subtext} children={fakeComponent} boxBottomComponent={fakeComponent} />
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should render AuthenticationBox with subtext string', () => {
    const component = renderer.create(
      <AuthenticationBox title={title} subtext='string' children={fakeComponent} boxBottomComponent={fakeComponent} />
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })


})
