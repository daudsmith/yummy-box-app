import React from 'react'
import { TextInput } from 'react-native'
import Row from '../Layout/Row'
import renderer from 'react-test-renderer'

describe('Row', () => {
    test('Should render text input in Row', () => {
        const render = renderer
        .create(
            <Row 
                children={<TextInput value={'Test'}/>}
            />
        )
        .toJSON()
        expect(render).toMatchSnapshot()
    })
})