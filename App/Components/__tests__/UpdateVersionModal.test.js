import React from 'react'
import UpdateVersionModal from '../UpdateVersionModal'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TouchableOpacity } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('UpdateVersionModal', () => {
  let text = 'text'
  const changed = 'changed'
  let open = true
  const onClose = () => {
    return text = changed
  }

  const onSubmit = () => {
    return text = changed
  }
  test('Should render UpdateVersionModal', () => {
    const component = renderer.create(
      <UpdateVersionModal open={open} onClose={onClose} onSubmit={onSubmit} />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })

  test('Should give correct response on close execute', () => {
    const component = shallow(
      <UpdateVersionModal open={open} onClose={onClose} onSubmit={onSubmit} />,
    )
    component.find(TouchableOpacity).at(1)
      .props().onPress()
      expect(text)
      .toBe(changed)
  })

  test('Should give correct response on submit execute', () => {
    const component = shallow(
      <UpdateVersionModal open={open} onClose={onClose} onSubmit={onSubmit} />,
    )
    component.find(TouchableOpacity).at(1)
      .props().onPress()
      expect(text)
      .toBe(changed)
  })
})
