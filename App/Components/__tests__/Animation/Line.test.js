import React from 'react'
import Line from '../../Animation/Line'
import renderer from 'react-test-renderer'

describe('Line', () => {
  const width = '100px'
  const height = '100px'
  const style = {
    backgroundColor: '#eeeeee',
    overflow: 'hidden',
    marginVertical: 4,
  }

  test('Line should match to snapshot', () => {
    const component = renderer.create(
      <Line
        style={style}
        width={width}
        height={height}
      />,
    )
    let tree = component.toJSON()
    expect(tree)
      .toMatchSnapshot()
  })



})
