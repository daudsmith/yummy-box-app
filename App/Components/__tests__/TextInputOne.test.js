import React from 'react'
import TextInputOne from '../TextInputOne'
import renderer from 'react-test-renderer'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { TextInput } from 'react-native'

Enzyme.configure({ adapter: new Adapter() })

describe('TextInputOne', () => {
  test('var text value should return text has been changed', () => {
    let text = null
    const onChangeText = () => {
      return (text = 'text has been changed')
    }
    let showMessage = {
      text: 'Text message',
      iconName: 'close',
      iconColor: 'black'
    }
    const textInputOne = shallow(
      <TextInputOne
        showMessage={showMessage}
        onChangeText={onChangeText}
        value={text}
      />
    )
    textInputOne.find(TextInput).props().onChangeText()
    expect(text).toBe('text has been changed')
  })

  test('Should render text input one with invalid message', () => {
    let text = null
    const onChangeText = () => {
      return (text = 'text has been changed')
    }
    let showMessage = {
      text: 'Invalid message',
      iconName: 'close',
      iconColor: 'red'
    }
    const render = renderer
      .create(
        <TextInputOne
          showMessage={showMessage}
          onChangeText={onChangeText}
          value={text}
        />
      )
      .toJSON()
    expect(render).toMatchSnapshot()
  })

  test('Should render text input one with valid message', () => {
    let text = null
    const onChangeText = () => {
      return (text = 'text has been changed')
    }
    let showMessage = {
      text: 'Valid message',
      iconName: 'close',
      iconColor: 'green'
    }
    const render = renderer
      .create(
        <TextInputOne
          showMessage={showMessage}
          onChangeText={onChangeText}
          value={text}
        />
      )
      .toJSON()
    expect(render).toMatchSnapshot()
  })
})
