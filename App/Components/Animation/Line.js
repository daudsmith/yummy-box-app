import React from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  line: {
    backgroundColor: '#eeeeee',
    overflow: 'hidden',
    marginVertical: 4,
  },
})

const Line = ({ height, width, style }) => (
  <View
    style={[
      styles.line,
      { borderRadius: height / 4, height, width },
      style,
    ]}
  >
  </View>
)

Line.propTypes = {
  height: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  width: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  style: PropTypes.object,
}

export default Line
