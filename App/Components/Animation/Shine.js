import React from 'react'
import {
  Animated,
  StyleSheet,
} from 'react-native'

const START_VALUE = 0
const DURATION = 500

const styles = StyleSheet.create({
  shine: {
    width: 10,
    height: '100%',
    backgroundColor: '#e0e0e0',
    opacity: 0.4,
  },
})

const Shine = ({ width }) => {
  const animation = new Animated.Value(0)

  function startSequence() {
    Animated.sequence([
      Animated.timing(animation, {
        toValue: width,
        duration: DURATION,
        useNativeDriver: true,
      }),
    ]).start(() => {
      startSequence()
    })
  }

  startSequence()

  const position= {
    transform: [
      {
        translateX: animation.interpolate({
          inputRange: [START_VALUE, width],
          outputRange: [0, 160]
        })
      }
    ]
  }

  return (
    <Animated.View style={[styles.shine, position]} />
  )
}

export default Shine
