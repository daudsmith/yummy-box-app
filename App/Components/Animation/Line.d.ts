import React from "react";

interface LineProps {
  width?: number | string;
  height?: number | string;
  style?: object;
}

const Line: React.FC<LineProps>

export default Line
