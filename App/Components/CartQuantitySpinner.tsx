import * as React from 'react'
import Moment from 'moment'
import {
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { Colors } from '../Themes'
import AlertBox from './AlertBox'
import LogEventService from '../Services/LogEventService'
import moment from 'moment'
import { itemResponse } from '../Services/V2/ApiCart'
import { Meal } from '../Redux/MealsRedux'
import { Toast } from 'native-base'

interface CartQuantitySpinnerProps {
  handleIncreasePress: (item: Meal | itemResponse, date: Moment.Moment, quantity: number) => void
  handleDecreasePress: (item: Meal | itemResponse, date: Moment.Moment, quantity: number) => void
  toggleAlertBox: () => void
  item: itemResponse
  date: string
  testID?: string
  testIDPrefix?: string
  max?: number
  showErrorModal: boolean
  exclusive: boolean
  inCartQty?: number
}

interface LogData {
  currency: string
  item_price: number
  item_id: number
  item_name: string
  item_category: string
}

const CartQuantitySpinner: React.FC<CartQuantitySpinnerProps> = (props) => {
  const {
    toggleAlertBox,
    item,
    testID = '',
    testIDPrefix = '',
    showErrorModal = false,
    date,
    max = 20,
    handleIncreasePress,
    handleDecreasePress,
  } = props

  const logEvent = (item: itemResponse) => {
    let logData: LogData = {
      currency: 'IDR',
      item_price: item.sale_price,
      item_id: item.id,
      item_name: item.name,
      item_category: 'item',
    }
    LogEventService.logEvent('add_to_cart', logData, ['default'])
  }

  const cartHandleIncreasePress = () => {
    logEvent(item)
    if ( item.quantity === max) {
      Toast.show({
        text: 'You’ve reached our last stock for this location',
        buttonText: '',
        duration: 1500,
      })
      return false
    } else {
      const quantity = item.quantity + 1
      handleIncreasePress(item, moment(date), quantity)
    }
  }

  const cartHandleDecreasePress = () => {
    const quantity = item.quantity - 1
    handleDecreasePress(item, moment(date), quantity)
  }

  return (
    <View style={styles.container} testID={testID+testIDPrefix}>
      <TouchableWithoutFeedback
        testID={`minQtyCheckout${testIDPrefix}`}
        accessibilityLabel={`minQtyCheckout${testIDPrefix}`}
        onPress={cartHandleDecreasePress}
      >
        <View
          style={[
            styles.button,
            styles.buttonDecrease,
            {
              backgroundColor: '#fff',
              justifyContent: 'center',
              borderColor: Colors.primaryOrange,
              borderWidth: 1,
            },
          ]}
        >
          <Text
            style={{
              ...styles.buttonIcon,
              color: Colors.primaryOrange,
            }}
          >
            -
          </Text>
        </View>
      </TouchableWithoutFeedback>
      <View style={styles.quantityBox}>
        <Text
          testID={testID+testIDPrefix}
          accessibilityLabel={testID+testIDPrefix}
          style={styles.quantityBoxText}
        >
          {item.quantity}
        </Text>
      </View>
      <TouchableWithoutFeedback
        testID={`plusQtyCheckout${testIDPrefix}`}
        accessibilityLabel={`plusQtyCheckout${testIDPrefix}`}
        onPress={cartHandleIncreasePress}
      >
        <View
          style={[
            styles.button,
            styles.buttonIncrease,
          ]}
        >
          <Text style={styles.buttonIcon}>+</Text>
        </View>
      </TouchableWithoutFeedback>
      <AlertBox
        primaryAction={() => {}}
        dismiss={toggleAlertBox}
        primaryActionText='Ok'
        title='Quantity Limit Reached'
        text='You’ve reached our last stock for this location or maximum quantity per order (20 pcs).'
        visible={showErrorModal}
      />
    </View>
  )
}

const styles: any = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderColor: Colors.primaryOrange,
    borderWidth: 0,
    borderRadius: 3,
    alignItems: 'center',
  },
  button: {
    height: 28,
    width: 28,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    backgroundColor: Colors.primaryOrange,
  },
  buttonIcon: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'Rubik-Regular',
  },
  buttonIncrease: {
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
  },
  buttonDecrease: {
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
  },
  quantityBox: {
    padding: 5,
    height: 28,
    width: 30,
    alignItems: 'center',
    borderColor: Colors.primaryOrange,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  quantityBoxText: {
    alignItems: 'center',
    fontFamily: 'Rubik-Regular',
    color: Colors.primaryOrange,
    fontSize: 16,
  },
})

export default CartQuantitySpinner
