import React from "react";
export interface mealCardProps {
    image?: string,
    caption?: string,
    title?: string,
    description?: Element | React.ReactNode,
    onPress?: Function,
    style?: object,
    imageContainerStyle?: object,
    imageStyle?: object,
    contentStyle?: object,
    captionStyle?: object,
    titleContainerStyle?: object,
    titleStyle?: object,
    noShadow?: boolean,
}

const MealCard: React.FC<mealCardProps>
export default MealCard
