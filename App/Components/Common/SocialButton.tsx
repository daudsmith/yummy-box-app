import React from 'react'
import { TouchableOpacity, Text, ActivityIndicator, StyleSheet, ViewStyle, TextStyle } from 'react-native'
import PropTypes from 'prop-types'
import Colors from '../../Themes/Colors'


export interface SocialButtonProps {
  buttonStyle?: ViewStyle,
  textStyle?: TextStyle,
  children?: JSX.Element | React.ReactNode,
  text?: string,
  onPress: () => void,
  disabled?: boolean,
  loading?: boolean,
  loadingColor?: string,
  testID?: string,
  accessibilityLabel?: string,
}

const SocialButton: React.FC<SocialButtonProps> = (props) => {
  const { buttonStyle, textStyle, children, text, onPress, testID, disabled, loading, loadingColor, accessibilityLabel } = props
  let childButton = children
  if (children === undefined) {
    childButton = (
      <Text style={[Styles.text, textStyle]}>{text}</Text>
    )
  }
  if (loading) {
    childButton = (
      <ActivityIndicator
        size='small'
        color={loadingColor}
      />
    )
  }
  return (
      <TouchableOpacity
        testID={testID}
        accessibilityLabel={accessibilityLabel}
        disabled={disabled}
        style={[Styles.button, buttonStyle]}
        onPress={() => onPress()}
      >
        {childButton}
      </TouchableOpacity>
  )
}

SocialButton.propTypes = {
  textStyle: PropTypes.object,
  text: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  loadingColor: PropTypes.string,
  buttonStyle: PropTypes.object,
  children: PropTypes.node
}

SocialButton.defaultProps = {
  onPress: () => { },
  disabled: false,
  loading: false,
  loadingColor: '#fff'
}

const Styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    width: '100%',
    height: 45,
  },
  text: {
    color: Colors.white,
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
  }
})

export default SocialButton
