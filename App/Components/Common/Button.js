import React, {Component} from 'react'
import {TouchableOpacity, Text, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import Colors from '../../Themes/Colors'

class Button extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    buttonStyle: PropTypes.object,
    textStyle: PropTypes.object,
    testID: PropTypes.string,
    accessibilityLabel: PropTypes.string
  }
  static defaultProps = {
    testID: '',
    accessibilityLabel: '',
    text: '',
    onPress: () => {},
    buttonStyle: {},
    textStyle: {}
  }

  render () {
    return (
      <TouchableOpacity style={[Styles.container, this.props.buttonStyle]} testID={this.props.testID} accessibilityLabel={this.props.testID} onPress={() => this.props.onPress()}>
        <Text style={[Styles.text, this.props.textStyle]}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    height: 35,
    backgroundColor: Colors.deepTeal,
    paddingTop: 8,
    paddingHorizontal: 18,
    borderRadius: 17.5
  },
  text: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    textAlign: 'center',
    color: 'white'
  }
})

export default Button