import * as React from 'react'
import { scale } from 'react-native-size-matters'

import Colors from '../../../Themes/Colors'
import PrimaryButton from '../../PrimaryButton'

export interface AddButtonProps {
  onPress: () => void,
  label: string,
  testID?: string,
  accessibilityLabel?: string,
}

const AddButton: React.FC<AddButtonProps> = ({
  onPress,
  label = 'Add',
  testID,
  accessibilityLabel,
}) => (
  <PrimaryButton
    testID={testID}
    accessibilityLabel={accessibilityLabel}
    label={label}
    labelStyle={{ fontSize: scale(14) }}
    onPressMethod={onPress}
    buttonStyle={{ backgroundColor: Colors.pinkishOrange }}
  />
)

export default AddButton
