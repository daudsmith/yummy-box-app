import React from 'react'
import {
  Text,
  View,
} from 'react-native'
import { scale } from 'react-native-size-matters'

import Stepper from './Stepper'
import AddButton from './AddButton'
import Colors from '../../../Themes/Colors'
import { Moment } from 'moment'
import { Meal } from '../../../Redux/MealsRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'

export interface propTypes {
  label?: string
  max?: number
  onMaxItemEvent?: (maximum: number, remaining: number) => void
  item: Meal
  mealDate: Moment
  quantity: number
  onIncrement: (item: Meal, date: Moment, quantity: number) => void
  onDecrement: (item: Meal, date: Moment, quantity: number) => void
  testID?: string
  accessibilityLabel?: string
  navigation: NavigationDrawerProp,
  available: boolean
  availability: number
}

class AddToCart extends React.PureComponent<propTypes> {
  _onIncrement = (
    _item: Meal,
    _mealDate: Moment,
    _max: number,
    _quantity: number
  ) => {
    const {
      onIncrement,
      onMaxItemEvent,
    } = this.props
    if (_quantity < _max && _quantity < this.props.availability) {
      onIncrement(_item, _mealDate, _quantity+1)
    } else if (typeof onMaxItemEvent !== 'undefined') {
      onMaxItemEvent(_max, this.props.availability)
    }
  }

  _onDecrease = (_item: Meal, _mealDate: Moment, _quantity: number) => {
    this.props.onDecrement(_item, _mealDate, _quantity-1)
  }

  render() {
    
    if (!this.props.available) {
      return (
        <View style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: Colors.Grey500,
          borderRadius: 6,
        }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: scale(14),
              color: Colors.warmGreyThree,
            }}>
            Sold Out
          </Text>
        </View>
      )
    }

    if (this.props.quantity > 0) {
      return (
        <Stepper
          quantity={this.props.quantity}
          onIncrement={() => this._onIncrement(this.props.item, this.props.mealDate, this.props.max, this.props.quantity)}
          onDecrement={() => this._onDecrease(this.props.item, this.props.mealDate, this.props.quantity)}
          testID={this.props.testID}
          accessibilityLabel={this.props.accessibilityLabel}
        />
      )
    }

    return (
      <AddButton
        onPress={() => this._onIncrement(this.props.item, this.props.mealDate, this.props.max, this.props.quantity)}
        label={this.props.label}
        testID={`addButton_${this.props.testID}`}
        accessibilityLabel={`addButton_${this.props.accessibilityLabel}`}
      />
    )
  }
}

export default AddToCart
