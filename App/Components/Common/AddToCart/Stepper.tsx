import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  GestureResponderEvent,
} from 'react-native'
import { scale } from 'react-native-size-matters'
import PropTypes from 'prop-types'
import Colors from '../../../Themes/Colors'

export interface StepperProps {
  quantity: number
  onIncrement: (event: GestureResponderEvent) => void
  onDecrement: (event: GestureResponderEvent) => void
  testID?: string,
  accessibilityLabel?: string,
  disabled?: boolean,
}

const Stepper: React.FC<StepperProps> = ({
  quantity,
  onIncrement,
  onDecrement,
  testID,
  accessibilityLabel,
}) => (
  <View style={styles.container}>
    <TouchableOpacity
      onPress={onDecrement}
      style={styles.subtract}
      testID={`minQty_${testID}`}
      accessibilityLabel={`minQty_${accessibilityLabel}`}
    >
      <Text style={styles.buttonIcon}>-</Text>
    </TouchableOpacity>
    <Text style={styles.text}>{quantity}</Text>
    <TouchableOpacity
      onPress={onIncrement}
      style={styles.add}
      testID={`plusQty_${testID}`}
      accessibilityLabel={`plusQty_${accessibilityLabel}`}
    >
      <Text style={styles.buttonIcon}>+</Text>
    </TouchableOpacity>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subtract: {
    width: scale(32),
    height: scale(32),
    paddingLeft: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(50),
    backgroundColor: Colors.warmGreyFour,
  },
  text: {
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    color: Colors.darkBlue,
    fontSize: scale(16),
  },
  add: {
    width: scale(32),
    height: scale(32),
    paddingLeft: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(50),
    backgroundColor: Colors.pinkishOrange,
  },
  buttonIcon: {
    color: Colors.white,
    fontSize: scale(24),
    fontFamily: 'Rubik-Regular',
  },
})

Stepper.propTypes = {
  quantity: PropTypes.number.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired,
}

export default Stepper
