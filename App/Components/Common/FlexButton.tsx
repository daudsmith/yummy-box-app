import React from 'react'
import { View, TouchableOpacity, Text, ActivityIndicator, StyleSheet, ViewStyle, TextStyle } from 'react-native'
import Colors from '../../Themes/Colors'

export interface FlexButtonProps {
  containerStyle?: ViewStyle,
  textStyle?: TextStyle,
  text?: string,
  onPress?: () => void,
  testID?: string,
  accessibilityLabel?: string,
  disableShadow?: boolean,
  disabled?: boolean,
  loading?: boolean,
  loadingColor?: string,
  buttonStyle?: ViewStyle,
  children?: JSX.Element | React.ReactNode
}

const FlexButton: React.FC<FlexButtonProps> = (props) => {
  const { containerStyle, buttonStyle, textStyle, children, text, onPress, testID, disableShadow, disabled, loading, loadingColor, accessibilityLabel } = props
  const shadowStyle = !disableShadow ? Styles.shadowStyle : {}
  let childButton = children
  if (children === undefined) {
    childButton = (
      <Text style={[Styles.text, textStyle]}>{text}</Text>
    )
  }
  if (loading) {
    childButton = (
      <ActivityIndicator
        size='small'
        color={loadingColor}
      />
    )
  }
  return (
    <View style={[Styles.container, containerStyle, shadowStyle]}>
      <TouchableOpacity
        testID={testID}
        accessibilityLabel={accessibilityLabel}
        disabled={disabled}
        style={[Styles.button, buttonStyle]}
        onPress={() => onPress()}
      >
        {childButton}
      </TouchableOpacity>
    </View>
  )
}


FlexButton.defaultProps = {
  onPress: () => { },
  disableShadow: false,
  disabled: false,
  loading: false,
  loadingColor: '#fff'
}

const Styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 11,
    borderRadius: 6,

  },
  shadowStyle: {
    shadowColor: 'rgba(0, 0, 0, 0.13)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1,
  },
  text: {
    color: Colors.bloodOrange,
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    backgroundColor: 'transparent'
  }
})

export default FlexButton
