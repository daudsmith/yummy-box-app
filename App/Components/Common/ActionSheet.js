import React, { Component } from 'react'
import { Platform, TouchableOpacity, TouchableWithoutFeedback, Text, View, StyleSheet, Animated, Easing } from 'react-native'
import Modal from 'react-native-modal'
import Colors from '../../Themes/Colors'

class ActionSheet extends Component {
  constructor(props) {
    super(props)
    this.state = {
      actionSheetStartY: new Animated.Value(0),
      initialY: 0
    }
  }
  UNSAFE_componentWillReceiveProps(newProps) {
    const { visible, buttons } = newProps
    if (this.props.buttons.length !== buttons.length) {
      const initialY = -1 * buttons.length * 50
      this.setState({ actionSheetStartY: new Animated.Value(initialY), initialY }, () => this.toggleActionSheet(visible))
    } else {
      this.toggleActionSheet(visible)
    }
  }
  toggleActionSheet(visible) {
    if (visible) {
      this.show()
    } else {
      this.hide()
    }
  }
  show() {
    Animated.timing(
      this.state.actionSheetStartY, { toValue: 0, duration: 250, easing: Easing.linear, useNativeDriver: false }
    ).start()
  }
  hide() {
    const { initialY } = this.state
    Animated.timing(
      this.state.actionSheetStartY, { toValue: initialY, duration: 250, easing: Easing.linear, useNativeDriver: false }
    ).start()
  }
  render() {
    const { visible, dismiss, buttons } = this.props
    const backdropColor = Platform.OS === 'android' ? 'white' : 'rgb(146, 146, 146)'
    return (
      <Modal
        animationIn='fadeIn'
        animationOut='fadeOut'
        backdropColor={backdropColor}
        backdropOpacity={0.8}
        isVisible={visible}
        onBackButtonPress={() => dismiss()}
        style={{ margin: 0 }}
      >
        <View style={[Styles.container, Platform.OS === 'ios' && Styles.containerIOS]}>
          {Platform.OS === 'android' ?
            <View>
              {buttons.map((button, index) =>
                <ActionSheetButton
                  testID={button.testID}
                  accessibilityLabel={button.accessibilityLabel}
                  key={index}
                  text={button.text}
                  onPress={() => button.onPress()}
                  button={button} dismiss={() => dismiss()}
                  index={index}
                  totalLength={buttons.length}
                />
              )}
              <TouchableWithoutFeedback testID='closeModify' accessibilityLabel='closeModify' onPress={() => dismiss()}>
                <View style={Styles.needHelpButtonAndroid}>
                  <Text style={Styles.needHelpAndroid}>Close</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
            :
            <View style={Styles.containerIOS}>
              <Animated.View style={[Styles.iOSActionSheetContainer, { bottom: this.state.actionSheetStartY }]}>
                {buttons.map((button, index) => <ActionSheetButton testID={button.testID} accessibilityLabel={button.accessibilityLabel} key={index} text={button.text} onPress={() => button.onPress()} dismiss={() => dismiss()} index={index} totalLength={buttons.length} />)}
              </Animated.View>
            </View>
          }
        </View>
      </Modal>
    )
  }
}

const ActionSheetButton = (props) => {
  const { dismiss, text, onPress, index, totalLength, testID, accessibilityLabel } = props
  const iOSBorderStyle = index !== totalLength - 1 && Platform.OS === 'ios' ? { borderBottomWidth: 0.5, borderBottomColor: 'rgb(226, 226, 229)' } : {}
  const buttonStyle = Platform.OS === 'android' ? Styles.androidActionSheetButton : [Styles.iOSActionSheetButton, iOSBorderStyle]
  const textStyle = Platform.OS === 'android' ? Styles.androidActionSheetButtonText : Styles.iOSActionSheetButtonText

  if (Platform.OS === 'ios') {
    return (
      <TouchableOpacity
        testID={testID}
        accessibilityLabel={accessibilityLabel}
        key={index}
        onPress={() => {
          dismiss()
          onPress()
        }}
      >
        <View key={index} style={buttonStyle}>
          <Text style={textStyle}>{text}</Text>
        </View>
      </TouchableOpacity>
    )
  }
  return (
    <TouchableWithoutFeedback
      testID={testID}
      accessibilityLabel={accessibilityLabel}
      onPress={() => {
        dismiss()
        onPress()
      }}
    >
      <View key={index} style={buttonStyle}>
        <Text style={textStyle}>{text}</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  containerIOS: {
    marginHorizontal: 10,
    marginBottom: 10
  },
  iOSActionSheetContainer: {
    backgroundColor: 'white',
    borderRadius: 15
  },
  iOSActionSheetButton: {
    height: 58,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iOSActionSheetButtonText: {
    fontSize: 20,
    color: 'rgb(39, 125, 246)'
  },
  androidActionSheetButton: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderTopColor: Colors.pinkishGrey,
    borderBottomColor: Colors.pinkishGrey
  },
  androidActionSheetButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: Colors.bloodOrange
  },
  needHelpButtonAndroid: {
    backgroundColor: Colors.bloodOrange,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center'
  },
  needHelpAndroid: {
    fontFamily: 'Rubik-Regular',
    color: 'white',
    fontSize: 16
  },
  shadow: {
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: 0,
      height: -3
    },
    shadowRadius: 4,
    shadowOpacity: 1
  }
})

export default ActionSheet
