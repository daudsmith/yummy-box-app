import React from 'react'
import { View, Text, Platform, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import { Dropdown } from '@geuntabuwono/react-native-material-dropdown'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import TextInput from './TextInput'
import Colors from '../../Themes/Colors'

const salutations = [
  { value: 'Mr.' },
  { value: 'Ms.' },
  { value: 'Mrs.' },
]

const TextInputWithSalutation = (props) => {
  const {
    label, salutation, value, onSalutationChange, salutationError,
    onTextFieldChange, setTextFieldRef, onTextFieldSubmitEditing,
    returnKey, textFieldError, testID, accessibilityLabel,
  } = props
  const salutationBorderColor = salutationError
    ? Colors.red
    : salutation !== ''
      ? Colors.greyishBrownTwo
      : Colors.lightGreyX
  return (
    <View>
      <View style={Styles.titleContainer}>
        <Text style={Styles.label}>{label}</Text>
      </View>
      <View style={Styles.fieldContainer}>
        <View style={Styles.salutationContainer}>
          <Dropdown
            labelHeight={0}
            data={salutations}
            inputContainerStyle={{
              borderBottomColor: salutationBorderColor,
              borderBottomWidth: 0.75,
              borderStyle: 'solid',
            }}
            dropdownPosition={0}
            itemTextStyle={{ fontFamily: 'Rubik-Regular' }}
            onChangeText={(text, index) => onSalutationChange(text, index)}
            renderAccessory={() => {
              return (
                <View style={Styles.dropdownIcon}>
                  <FontAwesome name='caret-down' size={14} color={Colors.greyishBrownTwo} />
                </View>
              )
            }}
            testID={`${testID}Salutation`}
            accessibilityLabel={`${testID}Salutation`}
          />
        </View>
        <View style={Styles.textInputContainer}>
          <TextInput
            testID={testID}
            accessibilityLabel={accessibilityLabel}
            value={value}
            onChangeText={(text) => onTextFieldChange(text)}
            setRef={(ref) => setTextFieldRef(ref)}
            onSubmitEditing={() => onTextFieldSubmitEditing()}
            returnKeyType={returnKey}
            useValidator={textFieldError
              ? 'plain'
              : undefined}
          />
        </View>
      </View>
    </View>

  )
}

TextInputWithSalutation.propTypes = {
  label: PropTypes.string,
  salutation: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  salutationError: PropTypes.bool,
  onTextFieldChange: PropTypes.func.isRequired,
  onSalutationChange: PropTypes.func.isRequired,
  setTextFieldRef: PropTypes.func,
  onTextFieldSubmitEditing: PropTypes.func,
  textFieldError: PropTypes.bool,
  testID: PropTypes.string,
  accessibilityLabel: PropTypes.string,
}

TextInputWithSalutation.defaultProps = {
  onTextFieldSubmitEditing: () => { },
  setTextFieldRef: () => { },
  salutationError: false,
  label: '',
}

const Styles = StyleSheet.create({
  label: {
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    color: Colors.warmGrey,
  },
  titleContainer: {
    marginBottom: 14,
  },
  fieldContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  salutationContainer: {
    flex: 1,
    marginRight: 13,
  },
  dropdownIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    right: 5,
  },
  textInputContainer: {
    flex: 3,
    marginTop: Platform.OS === 'android'
      ? 4
      : 7,
    backgroundColor: 'transparent',
    marginBottom: 8,
  },
})

export default TextInputWithSalutation
