import React, {Component} from 'react'
import {View, TextInput, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import Colors from '../../Themes/Colors'

class NumericInput extends Component {
  inputRefs = []
  static propTypes = {
    digits: PropTypes.number
  }
  static defaultProps = {
    digits: 4
  }
  constructor (props) {
    super(props)
    this.state = {
      focusedIndex: 0,
      numberArray: this.generateNumberArray(this.props.digits)
    }
  }

  componentDidMount () {
    this.inputRefs[0].focus()
  }

  generateNumberArray (digits) {
    const numberArray = []
    for (let i = 0; i < digits; i++) {
      numberArray.push('')
    }
    return numberArray
  }

  getConcatenatedNumber () {
    return this.state.numberArray.join().replace(/,/g, '')
  }

  userTypedFromEmptyInput (index, text) {
    const {numberArray} = this.state
    return numberArray[index] === '' && text.length === 1
  }

  userClearedInput (index, text) {
    const {numberArray} = this.state
    return numberArray[index] !== '' && text.length === 0
  }

  moveToNextInput (currentIndex) {
    const nextTarget = `input${currentIndex + 1}`
    this[nextTarget].focus()
    this.setState({focusedIndex: currentIndex + 1})
  }

  moveToPreviousInput (currentIndex) {
    const previousTarget = `input${currentIndex - 1}`
    this[previousTarget].focus()
    this.setState({focusedIndex: currentIndex - 1})
  }

  updateNumberArray (index, text) {
    let {numberArray} = this.state
    numberArray[index] = text
    this.setState({numberArray}, () => this.props.getValue(this.getConcatenatedNumber()))
  }

  moveCursor (index, text) {
    const {digits} = this.props
    if (index === 0) {
      if (this.userTypedFromEmptyInput(index, text)) {
        this.moveToNextInput(index)
      }
    } else if (index === (digits - 1)) {
      if (this.userClearedInput(index, text)) {
        this.moveToPreviousInput(index)
      }
    } else {
      if (this.userTypedFromEmptyInput(index, text)) {
        this.moveToNextInput(index)
      } else if (this.userClearedInput(index, text)) {
        this.moveToPreviousInput(index)
      }
    }
  }

  onInputTyped (index, text) {
    this.moveCursor(index, text)
    this.updateNumberArray(index, text)
  }

  onKeyPress (index, keyValue) {
    const {numberArray} = this.state
    if (numberArray[index] === '' && index !== 0 && keyValue === 'Backspace') {
      this.moveToPreviousInput(index)
      this.updateNumberArray(index - 1, '')
    }
  }

  renderNumericInput (digits) {
    const {numberArray} = this.state
    const inputs = []
    for (let i = 0; i < digits; i++) {
      const borderBottomColor = numberArray[i] === '' ? Colors.lightGrey : Colors.greyishBrown
      inputs.push(
        <View style={Styles.textInputContainer} key={i}>
          <TextInput
            value={numberArray[i]}
            style={[Styles.input, {borderBottomColor}]}
            ref={(ref) => this.inputRefs[i] = ref}
            underlineColorAndroid='transparent'
            keyboardType='numeric'
            maxLength={1}
            onFocus={() => this.setState({focusedIndex: i})}
            onChangeText={(text) => this.onInputTyped(i, text)}
            onKeyPress={(e) => this.onKeyPress(i, e.nativeEvent.key)}
          />
          {i !== (digits - 1) &&
            <View style={Styles.lineContainer}>
              <View style={Styles.line} />
            </View>
          }
        </View>
      )
    }

    return (
      <View style={{flexDirection: 'row'}}>
        {inputs}
      </View>
    )
  }

  render () {
    const {digits} = this.props
    return (
      <View style={Styles.container}>
        {this.renderNumericInput(digits)}
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  textInputContainer: {
    flexDirection: 'row'
  },
  input: {
    fontFamily: 'Rubik-Light',
    fontSize: 24,
    color: Colors.greyishBrown,
    width: 33,
    paddingBottom: 2,
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    textAlign: 'center'
  },
  lineContainer: {
    width: 16,
    justifyContent: 'center',
    alignItems: 'center'
  },
  line: {
    height: 1,
    width: 6,
    backgroundColor: Colors.greyishBrown
  }
})

export default NumericInput
