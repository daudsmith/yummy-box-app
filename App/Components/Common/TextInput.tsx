import React from 'react'
import { TextInput as RNTextInput, View, Text, Platform, StyleSheet, TextStyle } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import YummyboxIcon from '../YummyboxIcon'
import Colors from '../../Themes/Colors'
import PasswordInputText from '@geuntabuwono/react-native-hide-show-password-input'

export interface TextInputProps {
  text?: string,
  icon?: string,
  testID?: string,
  accessibilityLabel?: string,
  setRef?: (ref) => void,
  onChangeText?: (text: string) => void,
  onBlur?: () => void,
  onSubmitEditing?: () => void,
  keyboardType?: string | any,
  returnKeyType?: string | any,
  iconColor?: string,
  secureTextEntry?: boolean,
  value: string,
  validator?: (text: string) => void,
  labelStyle?: TextStyle,
  textInputStyle?: TextStyle,
  autoCapitalize?: string | any,
  useValidator?: string,
  lineColor?: string,
  fromLogin?: boolean,
  placeholder?: string,
  placeholderTextColor?: string,
  multiline?: boolean,
}

const TextInput: React.FC<TextInputProps> = (props) => {

  const {
    text = '',
    labelStyle,
    textInputStyle,
    icon = '',
    testID = '',
    onChangeText = () => { },
    onBlur = () => { },
    onSubmitEditing = () => { },
    keyboardType = 'default',
    returnKeyType = 'default',
    iconColor = Colors.lightGreyX,
    autoCapitalize,
    secureTextEntry = false,
    value = '',
    setRef = () => { },
    useValidator,
    validator = () => { },
    lineColor,
    accessibilityLabel = '',
    fromLogin = false,
    placeholder = '',
    placeholderTextColor = '',
    multiline = false
  } = props

  const checkIfValid = (useValidator, validator, value) => {
    if (useValidator === 'plain') {
      if (value !== '') {
        return Colors.greyishBrownTwo
      }
    } else {
      if (validator(value)) {
        return Colors.green
      }
    }
    return Colors.red
  }

  const renderLabel = (text, labelStyle) => {
    return (
      <View style={[Styles.labelContainer, labelStyle]}>
        <Text style={Styles.labelText}>{text}</Text>
      </View>
    )
  }

  const borderColor = typeof useValidator !== 'undefined' ? checkIfValid(useValidator, validator, value) : value === '' ? Colors.lightGreyX : Colors.greyishBrownTwo
  const color = typeof lineColor === 'undefined' ? borderColor : lineColor
  return (
    <View>
      {text !== '' &&
        renderLabel(text, labelStyle)
      }
      <View style={[Styles.textInputContainer, { borderBottomColor: color }]}>
        <View style={{ flex: 1 }}>
          {
            fromLogin ?
              <View style={{ marginTop: -30 }}>
                <PasswordInputText
                  label=''
                  autoCapitalize={autoCapitalize}
                  getRef={ref => setRef(ref)}
                  returnKeyType={returnKeyType}
                  onChangeText={(text: string) => onChangeText(text)}
                  onSubmitEditing={() => onSubmitEditing()}
                  underlineColorAndroid='transparent'
                  value={value}
                  iconColor={Colors.primaryGrey}
                  testID={testID}
                  accessibilityLabel={accessibilityLabel}
                  style={[Styles.textInputStyle, textInputStyle]}
                  activeLineWidth={0}
                />
              </View>
              :
              <RNTextInput
                placeholder={placeholder}
                placeholderTextColor={placeholderTextColor}
                secureTextEntry={secureTextEntry}
                autoCapitalize={autoCapitalize}
                ref={ref => setRef(ref)}
                keyboardType={keyboardType}
                returnKeyType={returnKeyType}
                onChangeText={(text) => onChangeText(text)}
                onSubmitEditing={() => onSubmitEditing()}
                onBlur={() => onBlur()}
                underlineColorAndroid='transparent'
                style={[Styles.textInputStyle, textInputStyle]}
                testID={testID}
                value={value}
                accessibilityLabel={accessibilityLabel}
                multiline={multiline}
              />
          }
        </View>
        {(color === Colors.red || color === Colors.green) && useValidator !== 'plain' && lineColor === undefined &&
          <View style={{ paddingTop: 5 }}>
            <FontAwesome color={color} size={14} name={color === Colors.red ? 'close' : 'check'} />
          </View>
        }
        {icon !== '' &&
          <YummyboxIcon name={icon} size={20} color={iconColor} />
        }
      </View>

    </View>
  )
}

const Styles = StyleSheet.create({
  labelContainer: {
    marginBottom: 12
  },
  labelText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    color: Colors.primaryDark
  },
  textInputContainer: {
    flexDirection: 'row',
    paddingBottom: Platform.OS === 'ios' ? 8 : 0,
    borderBottomWidth: 0.75
  },
  textInputStyle: {
    fontSize: 14,
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownTwo,
    padding: 0,
    margin: 0
  }
})

export default TextInput
