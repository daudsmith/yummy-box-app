import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import { Platform } from 'react-native'

function withSafeAreaView(WrappedComponent) {
  return class extends React.Component {
    render () {
      if (Platform.OS === 'ios') return <WrappedComponent {...this.props} />
      return (
        <SafeAreaView 
          style={{flex: 1, backgroundColor: 'white'}} 
          forceInset={{top: 'always', bottom: 'always'}}
        >
          <WrappedComponent {...this.props} />
        </SafeAreaView>
      )
    }
  } 
}

export default withSafeAreaView