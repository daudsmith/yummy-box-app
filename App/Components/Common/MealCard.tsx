import * as React from 'react'
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  ViewStyle,
  TextStyle,
  ImageStyle,
} from 'react-native'
import Colors from '../../Themes/Colors'
import { Images } from '../../Themes'
import FastImage from 'react-native-fast-image'

export interface MealCardProps {
  image?: string,
  caption?: string,
  title?: string,
  description?: string | React.ReactNode | Element,
  onPress: () => void,
  style?: ViewStyle,
  imageContainerStyle?: ViewStyle,
  imageStyle?: ImageStyle,
  contentStyle?: ViewStyle,
  captionStyle?: TextStyle,
  titleContainerStyle?: ViewStyle,
  titleStyle?: TextStyle,
  noShadow?: boolean,
  key?: string,
}

class MealCard extends React.PureComponent<MealCardProps> {
  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      image,
      caption,
      title,
      description,
      onPress,
      style,
      imageContainerStyle,
      imageStyle,
      contentStyle,
      captionStyle,
      titleContainerStyle,
      titleStyle,
      noShadow,
      key,
    } = this.props
    let containerStyle: ViewStyle = styles.container
    if (!noShadow) {
      containerStyle = {
        ...containerStyle,
        ...styles.shadow
      }
    }
    containerStyle = {
      ...containerStyle,
      ...style
    }

    return (
      <TouchableWithoutFeedback key={key} onPress={onPress}>
        <View style={containerStyle}>
          <View style={{
            ...styles.imageContainer,
            ...imageContainerStyle,
          }}>
            {image !== null ?
               <FastImage
                  style={[styles.image, imageStyle]}
                  source={{
                    uri: image,
                    priority: FastImage.priority.normal,
                  }}
                  resizeMode={FastImage.resizeMode.cover}
                />
              :
              <Image source={Images.showingSoon} style={[styles.image, imageStyle]} resizeMode="cover" />
            }
          </View>
          <View
            style={{
              ...styles.content,
              ...contentStyle,
            }}
          >
            {caption &&
            <Text
              style={{
                ...styles.caption,
                ...captionStyle,
              }}
            >
              {caption}
            </Text>
            }
            {title &&
            <View
              style={{
                ...styles.titleContainer,
                ...titleContainerStyle,
              }}
            >
              <Text
                numberOfLines={2}
                style={{
                  ...styles.title,
                  ...titleStyle,
                }}
              >
                {title}
              </Text>
            </View>
            }
            {description && description}
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: 160,
    backgroundColor: Colors.white,
  },
  shadow: {
    shadowColor: Colors.darkBlue,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4,
    elevation: 3,
  },
  imageContainer: {
    height: 160,
    width: 160,
    overflow: 'hidden',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  image: {
    flex: 1,
  },
  content: {
    flexDirection: 'column',
    padding: 12,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  titleContainer: {
    height: 40,
  },
  title: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.darkBlue,
    lineHeight: 20,
  },
  caption: {
    flex: 1,
    flexWrap: 'wrap',
    fontFamily: 'Rubik-Regular',
    fontSize: 11,
    color: Colors.warmGreyThree,
  }
})

export default MealCard
