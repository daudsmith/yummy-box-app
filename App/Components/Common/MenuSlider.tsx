import * as React from 'react'
import {
  FlatList,
  View,
  StyleSheet,
  ViewStyle,
  ListRenderItemInfo,
} from 'react-native'
import { Meal } from '../../Redux/MealsRedux'
import { ImmutableArray } from 'seamless-immutable'

export interface MenuSliderProps {
  items: any[] | ImmutableArray<any>,
  renderItem: (item: ListRenderItemInfo<Meal>) => JSX.Element,
  style: ViewStyle,
  maxRender: number,
  initialRender: number,
  windowSize: number,
  removeClippedSubviews: boolean,
}

class MenuSlider extends React.PureComponent<MenuSliderProps> {
  static defaultProps = {
    maxRender: 10,
    initialRender: 10,
    windowSize: 21,
    removeClippedSubviews: false,
  }

  render() {
    const {
      style,
      items,
      renderItem,
      removeClippedSubviews,
      maxRender,
      initialRender,
      windowSize,
    } = this.props
    return (
      <FlatList
        style={{
          ...styles.container,
          ...style,
        }}
        ListFooterComponent={<View style={{ width: 40 }}/>}
        data={(items as Meal[])}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderItem}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        removeClippedSubviews={removeClippedSubviews}
        maxToRenderPerBatch={maxRender}
        initialNumToRender={initialRender}
        windowSize={windowSize}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default MenuSlider
