import React, {Component} from 'react'
import {View, Text, Modal, TouchableOpacity, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import Button from './Button'
import Colors from '../../Themes/Colors'

class PopupBox extends Component {
  static propTypes = {
    title: PropTypes.string,
    visible: PropTypes.bool.isRequired,
    dismiss: PropTypes.func,
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    contentButton: PropTypes.shape({
      text: PropTypes.string,
      onPress: PropTypes.func
    }),
    bottomButtons: PropTypes.arrayOf(PropTypes.shape({
      text: PropTypes.string,
      onPress: PropTypes.func,
      textColor: PropTypes.string
    })),
  }
  static defaultProps = {
    title: '',
    visible: false,
    dismiss: () => {},
    bottomButtons: []
  }
  renderBottomButton () {
    return (
      <View style={Styles.bottomButtonContainer}>
        {this.props.bottomButtons.map((bottomButton, index) => {
          return (
            <TouchableOpacity 
              testID={bottomButton.testProp}
              accessibilityLabel={bottomButton.testProp} 
              key={index} 
              style={Styles.bottomButton} 
              onPress={() => bottomButton.onPress()}>
              <Text style={[Styles.bottomButtonText, {color: typeof bottomButton.textColor === 'string' ? bottomButton.textColor : Colors.pinkishGrey}]}>{bottomButton.text}</Text>
            </TouchableOpacity>
          )
        })}
      </View>
    )
  }
  renderContentButton (contentButton) {
    return (
      <Button text={contentButton.text} onPress={() => contentButton.onPress()} />
    )
  }
  renderContent () {
    const {title, content, contentButton} = this.props
    return (
      <View style={Styles.contentBox}>
        {title !== '' &&
          <View style={Styles.titleContainer}>
            <Text style={Styles.title}>{title}</Text>
          </View>}
        {typeof content === 'string' ?
          <View>
            <Text style={Styles.content}>
              {content}
            </Text>
          </View>
        :
          content()
        }
        {contentButton !== null &&
          <View style={{paddingTop: 20}}>
            {this.renderContentButton(contentButton)}
          </View>
        }
      </View>
    )
  }

  render () {
    const {visible} = this.props
    return (
      <Modal
        visible={visible}
        animationType='fade'
        onRequestClose={() => this.props.dismiss()}
        transparent
      >
        <View style={Styles.modalContainer}>
          <View style={Styles.box}>
            {this.renderContent()}
            {this.renderBottomButton()}
          </View>
        </View>
      </Modal>
    )
  }
}

const Styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgba(236, 236, 236, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  box: {
    flex: 1,
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    shadowColor: 'rgba(220, 220, 220, 0.5)',
    shadowOffset: {
      width: 2,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 1,
    elevation: 5
  },
  contentBox: {
    paddingTop: 20,
    alignItems: 'center',
    paddingBottom: 25,
    paddingHorizontal: 44,
  },
  titleContainer: {
    paddingTop: 29,
    paddingBottom: 6,
    alignItems: 'center'
  },
  title: {
    fontFamily: 'Rubik-Medium',
    color: Colors.greyishBrownThree,
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 1.4,
    marginBottom: 6,
    textAlign: 'center'
  },
  content: {
    fontFamily: 'Rubik-Light',
    color: Colors.charcoalGray,
    textAlign: 'center'
  },
  bottomButtonContainer: {
    flexDirection: 'row'
  },
  bottomButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20
  },
  bottomButtonText: {

  }
})

export default PopupBox
