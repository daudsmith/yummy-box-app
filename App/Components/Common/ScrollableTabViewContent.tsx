import React from 'react'
import { View, ViewProps } from 'react-native'

interface ScrollableTabViewContentProps extends ViewProps {
  tabLabel?: string
  children: React.ReactNode
}
export const ScrollableTabViewContent = (props: ScrollableTabViewContentProps) => {
  const { children, ...rest } = props
  return (<View {...rest}>{children}</View>)
}
