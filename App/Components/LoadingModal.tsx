import React from 'react'
import { ActivityIndicator, Modal, Text, View } from 'react-native'

interface LoadingModalProps {
  visible: boolean
  modalText: string
}

const LoadingModal: React.FC<LoadingModalProps> = ({
  visible,
  modalText
}) => {
  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={visible}
      onRequestClose={() => {}}
    >
      <View style={{backgroundColor: 'black', flex: 1, opacity: .4, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size='large'/>
        <Text style={{fontSize: 32, color: 'white', fontFamily: 'Rubik-Regular'}}>{modalText}</Text>
      </View>
    </Modal>
  )
}

export default LoadingModal
