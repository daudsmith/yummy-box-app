import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Colors from '../Themes/Colors'

const SubscriptionProfile = (props) => {
  const {theme, startOn, repeatEvery, repeatInterval, paymentMethod} = props
  return (
    <View>
      <Text style={Styles.title}>Subscription Profile</Text>
      <Text style={Styles.themeName}>{theme}</Text>
      <View>
        <SubscriptionProfileRow title='Start On' value={startOn} />
        <SubscriptionProfileRow title='Subscribe Every' value={repeatEvery} />
        <SubscriptionProfileRow title='Subscribe For' value={repeatInterval} />
        <SubscriptionProfileRow title='Payment Method' value={paymentMethod === 'credit-card' ? 'Credit Card' : 'YumCredits'} />
      </View>
    </View>
  )
}

const SubscriptionProfileRow = (props) => {
  const {title, value} = props
  return (
    <View style={{flexDirection: 'row', marginBottom: 5}}>
      <Text style={Styles.rowTitle}>{title}</Text>
      <Text style={Styles.rowValue}>{value}</Text>
    </View>
  )
}

const Styles = StyleSheet.create({
  title: {
    fontFamily: 'Rubik-Regular',
    color: Colors.warmGrey,
    fontSize: 18,
    marginBottom: 15
  },
  themeName: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownThree,
    fontSize: 14,
    lineHeight: 18,
    marginBottom: 7
  },
  rowTitle: {
    fontFamily: 'Rubik-Light',
    color: Colors.greyishBrownTwo,
    fontSize: 12,
    flex: 1
  },
  rowValue: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownTwo,
    fontSize: 12
  }
})

export default SubscriptionProfile