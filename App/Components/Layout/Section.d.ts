import React from "react";

export interface sectionProps {
  children?: Element | React.ReactNode
  style?: object
  padder?: boolean
  full?: boolean
  keyboard?: boolean
  header?: Element | React.ReactNode
  footer?: Element | React.ReactNode
}

const Section: React.FC<sectionProps>
export default Section
