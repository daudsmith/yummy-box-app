import React from 'react'
import { KeyboardAvoidingView, View, Platform, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Colors from '../../Themes/Colors'

const Section = ({
  children,
  style,
  full,
  padder,
  keyboard,
  header,
  footer
}) => (
    <KeyboardAvoidingView
      behavior={Platform.select({ ios: 'padding', android: null })}
      enabled={keyboard}
    >
      <View
        style={[
          styles.container,
          full && styles.full,
          padder && styles.padder,
          style,
        ]}
      >
        {header !== 'undefined' &&
          header
        }
        <View style={styles.children}>
          {children}
        </View>
        {footer !== 'undefined' &&
          footer
        }
      </View>
    </KeyboardAvoidingView>
  )

Section.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.element
  ]),
  style: PropTypes.object,
  full: PropTypes.bool,
  padder: PropTypes.bool,
  keyboard: PropTypes.bool,
  header: PropTypes.node,
  footer: PropTypes.node,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 16,
    marginBottom: 16,
    backgroundColor: Colors.white,
  },
  children: {
    flex: 1,
  },
  padder: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 16,
    paddingBottom: 16,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
  },
  full: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
  },
})

export default Section
