import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { scale } from 'react-native-size-matters'
import { Icon } from 'native-base'
import PropTypes from 'prop-types'
import Colors from '../../Themes/Colors'

const SectionFooter = ({
  text,
}) => (
    <View
      style={[
        styles.container,
      ]}
    >
        <View style={styles.container}>
            <Icon
            name="ios-information-circle"
            style={styles.icon}
            />
            <Text style={styles.text}>{text}</Text>
        </View>
    </View>
  )

SectionFooter.propTypes = {
    text: PropTypes.string,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: scale(10), 
    flexDirection: 'row'
  },
  icon: {
    fontSize: 15,
    color: Colors.warmGreyThree,
    marginRight: 5,
  },
  text: {
    fontSize: 10
  }

})

export default SectionFooter
