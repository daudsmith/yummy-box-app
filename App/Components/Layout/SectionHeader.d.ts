import React from "react";
export interface sectionHeaderProps {
    style?: object,
    captionTextStyle?: object,
    titleTextStyle?: object,
    leftStyle?: object,
    centerStyle?: object,
    rightStyle?: object,
    leftIcon?: Element | React.ReactNode | undefined,
    captionText?: string,
    titleText: string,
    titleRight?: Element | React.ReactNode | string | undefined,
}

const SectionHeader: React.FC<sectionHeaderProps>
export default SectionHeader
