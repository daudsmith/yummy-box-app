import React from 'react'
import { View, ViewPropTypes } from 'react-native'
import PropTypes from 'prop-types'

class Row extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    style: ViewPropTypes.style,
  };

  render() {
    const { children, style } = this.props

    return (
      <View style={style}>
        {children}
      </View>
    )
  }
}

export default Row
