import React from 'react'
import { View, Text, StyleSheet, ViewPropTypes } from 'react-native'
import PropTypes from 'prop-types'
import Colors from '../../Themes/Colors'

const SectionHeader = ({
  style,
  captionTextStyle,
  titleTextStyle,
  leftStyle,
  centerStyle,
  rightStyle,
  leftIcon,
  captionText,
  titleText,
  titleRight,
}) => (
    <View
      style={[
        styles.container,
        style,
      ]}
    >
      {leftIcon &&
        <View style={[styles.leftSection, leftStyle]}>
          {leftIcon}
        </View>
      }
      <View style={[styles.centerSection, centerStyle]}>
        <View style={styles.caption}>
          <Text style={{...styles.captionText, ...captionTextStyle}}>{captionText}</Text>
        </View>
        <View style={styles.titleSection}>
          <View style={styles.titleLeft}>
            <Text style={[styles.titleText, titleTextStyle]}>{titleText}</Text>
          </View>
        </View>
      </View>
      {titleRight &&
        <View style={[styles.rightSection, rightStyle]}>
          {titleRight}
        </View>
      }
    </View>
  )

SectionHeader.propTypes = {
  style: ViewPropTypes.style,
  captionTextStyle: PropTypes.object,
  titleTextStyle: PropTypes.object,
  leftStyle: ViewPropTypes.style,
  centerStyle: ViewPropTypes.style,
  rightStyle: ViewPropTypes.style,
  leftIcon: PropTypes.node,
  captionText: PropTypes.string,
  titleText: PropTypes.string,
  titleRight: PropTypes.node,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 16,
    flexDirection: 'row',
    backgroundColor: Colors.white,
  },
  leftSection: {
    padding: 0,
    margin: 0,
  },
  centerSection: {
    flex: 1,
    flexDirection: 'column',
  },
  rightSection: {
    padding: 0,
    margin: 0,
  },
  caption: {
    flex: 1,
    alignItems: 'flex-start',
    marginBottom: 4,
  },
  captionText: {
    fontFamily: 'Rubik-Medium',
    color: Colors.warmGreyThree,
    fontSize: 12,
    textTransform: 'uppercase',
  },
  titleSection: {
    flex: 1,
    flexDirection: 'row',
  },
  titleText: {
    fontFamily: 'Rubik-Medium',
    color: Colors.darkBlue,
    fontSize: 20,
  },
  titleLeft: {
    flex: 1,
  },
  titleRight: {
    padding: 0,
    margin: 0,
  },
})

export default SectionHeader
