import * as React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import icoMoonConfig from '../Images/SvgIcon/selection.json'
import LocaleFormatter from '../Services/LocaleFormatter'
import MealCartService from '../Services/MealCartService'
import { Colors } from '../Themes'
import Analytics from '../Services/LogEventService'
import {
  CartState,
} from '../Redux/V2/CartRedux'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

interface ShoppingCartButtonInterface {
  cart: CartState
  isLoggedIn?: boolean
  testID?: string,
  accessibilityLabel?: string,
  onPressHandle: () => void
}

const ShoppingCartButton: React.FC<ShoppingCartButtonInterface> = (props) => {
  const { cart, onPressHandle, testID, accessibilityLabel } = props
  const { cartItems, totals, type } = cart
  const quantity = MealCartService.getCartQuantity(cartItems, type)

  const handleOnPress = () => {
    const params = {
      order_type: type,
      currency: 'IDR',
      value: totals.total
    }
    Analytics.logEvent('begin_checkout', params, ['default'])
    onPressHandle()
  }

  return (
    <TouchableOpacity
      disabled={props.cart.fetching}
      testID={testID}
      accessibilityLabel={accessibilityLabel}
      activeOpacity={0.9}
      onPress={handleOnPress}
      style={{
        flexDirection: 'row',
        width: '100%',
        height: verticalScale(55),
        backgroundColor: props.cart.fetching
          ? Colors.disable
          : Colors.pinkishOrange
      }}
    >
      {/* CART & BADGE ICON */}
      <View
        style={{
          flex: 1.2,
          backgroundColor: 'transparent',
          borderRightWidth: scale(1.5),
          borderColor: '#fff',
          marginVertical: verticalScale(10),
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <YumboxIcon
          name='cart'
          size={scale(25)}
          color='white'
        />
        <View
          style={{
            position: 'absolute',
            width: scale(16),
            height: scale(16),
            borderRadius: scale(8),
            backgroundColor: '#fff',
            right: scale(12),
            justifyContent: 'center',
            alignItems: 'center',
            bottom: verticalScale(18)
          }}
        >
          <Text
            style={{
              fontSize: scale(9),
              color: Colors.primaryOrange,
              fontFamily: 'Rubik-Regular'
            }}
          >
            {quantity}
          </Text>
        </View>
      </View>

      {/* TEXT AREA */}
      <View
        style={{
          flex: 5,
          backgroundColor: 'transparent',
          marginTop: verticalScale(8),
          marginLeft: scale(10)
        }}
      >
        <Text
          style={{
            color: '#fff',
            fontSize: scale(12),
            fontFamily: 'Rubik-Regular'
          }}
        >
          Checkout
        </Text>
        {type === 'subscription'
          ? (
            <Text
              style={{
                color: '#fff',
                fontSize: scale(16),
                fontFamily: 'Rubik-Medium',
                top: 1
              }}
              numberOfLines={1}
            >
              {`${cartItems[0].subscription.theme_name} subscription`}
            </Text>
          )
          : (
            <Text
              style={{
                color: '#fff',
                fontSize: scale(16),
                fontFamily: 'Rubik-Medium',
                top: 1
              }}
            >
              {LocaleFormatter.numberToCurrency(totals.subtotal)}
            </Text>
          )
        }
      </View>

      {/* ARROW RIGHT AREA */}
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Ionicons
            name='chevron-forward'
            size={scale(25)}
            style={{
              color: 'white',
            }}
          />
      </View>
    </TouchableOpacity>
  )
}

export default ShoppingCartButton
