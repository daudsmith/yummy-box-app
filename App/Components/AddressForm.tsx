import React from 'react'
import {
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  StyleSheet,
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../Images/SvgIcon/selection.json'
import Colors from '../Themes/Colors'
import { moderateScale, scale } from 'react-native-size-matters'
import { Icon } from 'native-base'

interface AddressFormProps {
  note?: string
  date?: string
  readOnly?: boolean
  disableEdit?: boolean
  location?: {
    latitude: number,
    longitude: number
  }
  deliveryAddress: string
  phone: string
  name: string
  landmark?: string
  handleDeliveryChange?: () => void
  deliveryAddressOptionPicker?: () => void
  handleDeliveryDetailChange?: (field: string, value: string) => void
}

let phoneRef: TextInput
let noteRef: TextInput
let nameRef: TextInput

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

const AddressForm: React.FC<AddressFormProps> = (props) => {
  const headerProps = {
    readOnly: props.readOnly,
    deliveryAddressOptionPicker: props.deliveryAddressOptionPicker,
  }
  return (
    <View
      style={{
        flex: 1,
      }}
    >
      {header(headerProps)}
      {body(props)}
    </View>
  )
}

const header = (props) => {
  const {
    readOnly,
    deliveryAddressOptionPicker,
  } = props
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 16,
      }}
    >
      <View style={{ flex: 1 }}>
        <Text style={Styles.headerText}>
          Delivery Address
        </Text>
      </View>
      {!readOnly &&
      <View>
        <TouchableWithoutFeedback
          testID="change"
          accessibilityLabel="change"
          onPress={() => deliveryAddressOptionPicker()}
          hitSlop={{
            top: 26,
            left: 26,
            bottom: 26,
            right: 26,
          }}
        >
          <View>
            <Text style={[
              Styles.headerText,
              { color: Colors.green },
            ]}>Change</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
      }
    </View>
  )
}

const body = (props: AddressFormProps) => {
  const propsNotes = {
    readOnly: props.readOnly,
    disableEdit: props.disableEdit,
    note: props.note,
    handleDeliveryDetailChange: props.handleDeliveryDetailChange,
  }
  const phoneProps = {
    readOnly: props.readOnly,
    disableEdit: props.disableEdit,
    phone: props.phone,
    handleDeliveryDetailChange: props.handleDeliveryDetailChange,
  }
  const nameProps = {
    readOnly: props.readOnly,
    disableEdit: props.disableEdit,
    name: props.name,
    handleDeliveryDetailChange: props.handleDeliveryDetailChange,
  }
  const addressProps = {
    deliveryAddress: props.deliveryAddress,
    landmark: props.landmark,
    readOnly: props.readOnly,
    deliveryAddressOptionPicker: props.deliveryAddressOptionPicker,
  }
  return (
    <View>
      {row(
        'location',
        Colors.bloodOrange,
        () => address(addressProps),
        () => notes(propsNotes),
      )}
      {row(
        'person',
        Colors.bloodOrange,
        () => name(nameProps),
      )}
      {row(
        'call',
        Colors.bloodOrange,
        () => phone(phoneProps),
      )}
    </View>
  )
}

const phone = (props) => {
  const {
    readOnly,
    disableEdit,
    phone,
    handleDeliveryDetailChange,
  } = props
  return (
    <View style={{
      flex: 9,
      flexDirection: 'row',
      alignItems: 'center',
    }}>
      <View style={{ flex: 1 }}>
        {(readOnly || disableEdit) && (
          <Text
            style={Styles.singleLineFormTextInput}
          >
            {phone}
          </Text>
        )}
        {!readOnly && !disableEdit && (
          <TextInput
            value={phone}
            placeholder='Phone Number'
            style={Styles.singleLineFormTextInput}
            underlineColorAndroid='transparent'
            keyboardType={'phone-pad'}
            ref={(ref) => phoneRef = ref}
            onChangeText={(phone) => handleDeliveryDetailChange('phone', phone)}
          />
        )}
      </View>
      {!readOnly && !disableEdit && (
        <TouchableOpacity
          onPress={() => phoneRef.focus()}
          style={{ flex: 1 }}
        >
          <YumboxIcon
            name='edit'
            color={Colors.green}
            style={{ fontSize: 16 }}
          />
        </TouchableOpacity>
      )}
    </View>
  )
}

const notes = (props) => {
  const {
    readOnly,
    disableEdit,
    note,
    handleDeliveryDetailChange,
  } = props

  let showAddNotes = note === ''
  const marginTop = readOnly
    ? (note === ''
      ? 0
      : 10)
    : 8
  const marginBottom = readOnly
    ? note === ''
      ? 0
      : (Platform.OS === 'ios'
        ? 10
        : 0)
    : (Platform.OS === 'ios'
      ? 8
      : 0)
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        marginTop: marginTop,
        marginBottom: marginBottom,
      }}
    >
      <View style={{ flex: 1 }} />
      <View
        style={{
          flex: 9,
        }}
      >
        {(((note && readOnly) === true) || ((note && disableEdit) === true)) && (
          <Text
            style={{
              color: Colors.greyishBrown,
              fontSize: 10,
            }}
          >
            {note}
          </Text>
        )}
        {!showAddNotes && !readOnly && !disableEdit && (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <View style={{ flex: 9 }}>
              <TextInput
                ref={(ref: TextInput) => noteRef = ref}
                style={Styles.addNotesTextInput}
                value={note}
                onChangeText={(note) => handleDeliveryDetailChange('note', note)}
                onBlur={() => showAddNotes = note === ''}
                multiline
                underlineColorAndroid='transparent'
              />
            </View>
            <TouchableOpacity
              onPress={() => noteRef.focus()}
              style={{
                flex: 1,
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}
            >
              <YumboxIcon
                name="edit"
                color={Colors.green}
                style={{ fontSize: 16 }}
              />
            </TouchableOpacity>
          </View>
        )}
        {showAddNotes && !readOnly && !disableEdit && (
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={() => {
              setTimeout(() => noteRef.focus(), 300)
            }}
          >
            <YumboxIcon name='notes' color={Colors.green} size={16} />
            <Text style={Styles.addNotesButton}> Add Notes</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  )
}

const name = (props) => {
  const {
    readOnly,
    disableEdit,
    name,
    handleDeliveryDetailChange,
  } = props
  return (
    <View style={{
      flex: 9,
      flexDirection: 'row',
      alignItems: 'center',
    }}>
      <View style={{ flex: 1 }}>
        {(readOnly || disableEdit) && (
          <Text style={[
            Styles.singleLineFormTextInput,
            {
              padding: 0,
            },
          ]}
          >
            {name}
          </Text>
        )}
        {!readOnly && !disableEdit && (
          <TextInput
            ref={(ref) => nameRef = ref}
            placeholder={'Recipient Name'}
            value={name}
            style={Styles.singleLineFormTextInput}
            onChangeText={(name) => handleDeliveryDetailChange('name', name)}
            underlineColorAndroid='transparent'
            keyboardType='default'
          />
        )}
      </View>
      {!readOnly && !disableEdit && (
        <TouchableOpacity
          onPress={() => { nameRef.focus() }}
          style={{ flex: 1 }}
        >
          <YumboxIcon
            name={'edit'}
            color={Colors.green}
            style={{ fontSize: 16 }}
          />
        </TouchableOpacity>
      )}
    </View>
  )
}

const address = (props) => {
  const {
    deliveryAddress,
    landmark,
    readOnly,
    deliveryAddressOptionPicker,
  } = props
  let addressText = deliveryAddress
  if (landmark) {
    addressText = landmark + ', ' + addressText
  }
  return (
    <View style={{
      flex: 9,
      justifyContent: 'center',
    }}>
      <TouchableOpacity
        disabled={readOnly}
        onPress={() => deliveryAddressOptionPicker()}
      >
        <Text style={Styles.addressText}>{addressText}</Text>
      </TouchableOpacity>
    </View>
  )
}

export const row = (iconName: string, color: string, content: () => void, subContent?: () => void) => {
  return (
    <View style={Styles.rowContainer}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
          }}
        >
          <Icon
            name={'ios-'+iconName}
            style={{
              color: color,
              fontSize: moderateScale(26),
              marginRight: scale(5)
            }}
          />
        </View>
        {content()}
      </View>
      {subContent
        ? subContent()
        : <View />}
    </View>
  )
}

const Styles: any = StyleSheet.create({
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 9,
    borderStyle: 'solid',
    marginBottom: 3,
  },
  rowContainer: {
    flex: 1,
    paddingVertical: 8,
  },
  headerText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownThree,
    fontSize: scale(14),
  },
  addressText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    fontWeight: '300',
    color: Colors.greyishBrown,
    lineHeight: 18,
  },
  addNotesButton: {
    fontFamily: 'Rubik-Regular',
    color: Colors.green,
    fontSize: 10,
  },
  addNotesTextInput: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    fontWeight: '300',
    color: Colors.greyishBrown,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  singleLineFormTextInput: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    fontWeight: '300',
    color: Colors.greyishBrown,
    paddingVertical: Platform.OS === 'android'
      ? 0
      : 8,
    paddingHorizontal: 0,
  },
})

export default AddressForm
