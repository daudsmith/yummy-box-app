import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import DeliveryTimePicker from './DeliveryTimePicker'

const defaultDeliveryTimeSlots = [
  '09:00-12:00'
]

const getSelectedTime = (props, deliveryTimeSlots) => {
  const {
    type,
    cart,
    date,
    modifiedMyMealPlanDetail,
    modifiedOrderDetail,
  } = props
  if (type === 'checkout') {
    const {cartItems} = cart
    const selectedCartItemsIndex = _.findIndex(cartItems, (cartItem) => cartItem.date === date)
    return selectedCartItemsIndex > -1 ? cartItems[selectedCartItemsIndex].deliveryTime : null
  } else if (type === 'modify_subscription_order') {
    const {default_delivery_time} = modifiedOrderDetail.subscription_raw_data
    const defaultDeliveryTimeIndex = _.findIndex(deliveryTimeSlots, (timeSlot) => timeSlot.value === default_delivery_time)
    return defaultDeliveryTimeIndex > -1 ? deliveryTimeSlots[defaultDeliveryTimeIndex].value : null
  } else if (type === 'modify_mmp') {
    const {time} = modifiedMyMealPlanDetail.data
    const defaultDeliveryTimeIndex = _.findIndex(deliveryTimeSlots, (timeSlot) => timeSlot.value === time)
    return defaultDeliveryTimeIndex > -1 ? deliveryTimeSlots[defaultDeliveryTimeIndex].value : null
  } else {
    return 0
  }
}

const getDeliveryTimeSlots = (setting) => {
  if (setting === null) {
    return defaultDeliveryTimeSlots
  }
  if (setting['delivery_times']) {
    const settingDeliveryTimeSlots = setting['delivery_times'].replace(/'/g, '"')
    return JSON.parse(settingDeliveryTimeSlots)
  }
  return defaultDeliveryTimeSlots
}

const getDinnerAvailability = (props) => {
  const {
    cart,
    type,
    modifiedMyMealPlanDetail,
  } = props

  if (type === 'checkout') {
    const { dinner } = cart
    return dinner
  } else if (type === 'modify_mmp') {
    let dinner = true
    const { items } = modifiedMyMealPlanDetail.data
    items.data.map(item => {
      if (item.detail.data.dinner !== '') {
        dinner = false
      }
      return item
    })
    return dinner
  } else {
    return true
  }
}

const getLunchAvailability = (props) => {
  const {
    cart,
    type,
    modifiedMyMealPlanDetail,
  } = props

  if (type === 'checkout') {
    const { lunch } = cart
    return lunch
  } else if (type === 'modify_mmp') {
    let lunch = true
    const { items } = modifiedMyMealPlanDetail.data
    items.data.map(item => {
      if (item.detail.data.lunch !== '') {
        lunch = false
      }
      return item
    })
    return lunch
  } else {
    return true
  }
}

const handleUpdateDeliveryTime = (time, props) => {
  const {
    type,
    useMultipleAddress,
    updateDeliveryTime,
    updateSubscriptionDefaultData,
    modifyMmpPackageDelivery
  } = props
  if (type === 'checkout') {
    updateDeliveryTime(time, useMultipleAddress)
  } else if (type === 'modify_subscription_order') {
    updateSubscriptionDefaultData('default_delivery_time', time)
  } else if (type === 'modify_mmp') {
    modifyMmpPackageDelivery('time', time)
  } else{
    return 0
  }
}

const SmartDeliveryTimePicker = (props) => {
  const {
    date,
    setting,
    type,
  } = props
  let dinner = false
  let lunch = false
  if (type === 'checkout') {
    dinner = getDinnerAvailability(props)
    lunch = getLunchAvailability(props)
  }
  const deliveryTimeSlots = getDeliveryTimeSlots(setting)
  const selectedTime = getSelectedTime(props, deliveryTimeSlots)
  return (
    <DeliveryTimePicker
      date={date}
      selectedTime={selectedTime}
      deliveryTimeSlots={deliveryTimeSlots}
      handleOnPickerConfirm={(selectedTime) => handleUpdateDeliveryTime(selectedTime, props)}
      dinner={dinner}
      lunch={lunch}
      readOnly={type !== 'checkout'}
    />
  )
}

SmartDeliveryTimePicker.propTypes = {
  date: PropTypes.string.isRequired,
  setting: PropTypes.object.isRequired,
  updateDeliveryTime: PropTypes.func,
  updateSubscriptionDefaultData: PropTypes.func,
  modifyMmpPackageDelivery: PropTypes.func,
  cart: PropTypes.object,
  useMultipleAddress: PropTypes.bool,
  type: PropTypes.oneOf(['checkout', 'modify_mmp', 'modify_subscription_order']),
  modifiedOrderDetail: PropTypes.object,
  modifiedMyMealPlanDetail: PropTypes.object,
}

SmartDeliveryTimePicker.defaultProps = {
  useMultipleAddress: false,
  type: 'checkout',
}

export default SmartDeliveryTimePicker
