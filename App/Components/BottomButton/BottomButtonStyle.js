import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'
import { moderateScale } from 'react-native-size-matters'

export default StyleSheet.create({
  buttonContainer: {
    backgroundColor: Colors.white,
    position: 'relative',
    left: 0,
    right: 0,
    bottom: 0,
  },
  buttonWrapper: {
    color: Colors.white,
    marginHorizontal: moderateScale(20),
    marginVertical: moderateScale(8),
    borderRadius: moderateScale(4),
    height: moderateScale(40),
    paddingTop: 0,
    backgroundColor: Colors.primaryOrange,
    justifyContent: 'center',
  },
  disabled: {
    color: Colors.primaryGrey,
    backgroundColor: Colors.disable,
  },
  textOnly: {
    color: Colors.white,
    fontSize: moderateScale(16),
    textAlign: 'center',
    fontFamily: 'Rubik-Regular'
  },
})
