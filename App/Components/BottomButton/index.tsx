import React from 'react'
import Styles from './BottomButtonStyle'
import {
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
  TextStyle,
} from 'react-native'

interface BottomButtonProps {
  buttonText: string
  pressHandler: () => void
  disabled?: boolean
  testID?: string
  accessibilityLabel?: string,
  customStyle?: ViewStyle
  customTextStyle?: TextStyle
}

const BottomButton: React.FC<BottomButtonProps> = ({
  buttonText,
  pressHandler,
  disabled = false,
  testID,
  accessibilityLabel,
  customStyle,
  customTextStyle,
}) => {
  const {
    buttonContainer,
    buttonWrapper,
    textOnly,
  } = Styles
  const disabledStyles = Styles.disabled

  return (
    <View
      style={buttonContainer}
    >
      <View
        style={[
          buttonWrapper,
          customStyle,
          disabled ? disabledStyles : {}
        ]}
      >
        <TouchableOpacity
          testID={testID}
          accessibilityLabel={accessibilityLabel}
          onPress={pressHandler}
          disabled={disabled}
        >
          <Text
            style={[
              textOnly,
              customTextStyle,
              disabled ? disabledStyles : {}
            ]}
          >
            {buttonText}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default BottomButton
