import React from 'react'
import { View, Text } from 'react-native'
import styles from './Styles/AlertMessageStyles'

interface AlertMessageProps {
  title?: string
  icon?: string
  style?: object
  show?: boolean
} 

const AlertMessage: React.FC<AlertMessageProps> = ({
  show = true,
  style,
  title,
}) => {
  let messageComponent = null
  if (show) {
    return (
      <View
        style={[styles.container, style]}
      >
        <View style={styles.contentContainer}>
          <Text allowFontScaling={false} style={styles.message}>{title && title.toUpperCase()}</Text>
        </View>
      </View>
    )
  }
  return messageComponent
}

export default AlertMessage