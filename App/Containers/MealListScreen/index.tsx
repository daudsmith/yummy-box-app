import _ from 'lodash'
import Moment from 'moment'
import {
  Button,
  Icon,
  ScrollableTab,
  Tab,
  Tabs,
  Text,
  View,
  Container,
  Content,
  Footer,
  Toast,
} from 'native-base'
import * as React from 'react'
import {
  ActivityIndicator,
  Dimensions,
  Modal,
  Platform,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native'
import { Calendar } from 'react-native-calendars'
import RNModal from 'react-native-modal'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {
  NavigationActions,
} from 'react-navigation'
import { connect } from 'react-redux'
import NavigationBar from '../../Components/NavigationBar'
import OfflineModal from '../../Components/OfflineModal'
import ShoppingCartAlertModal from '../../Components/ShoppingCartAlertModal'
import ShoppingCartButton from '../../Components/ShoppingCartButton'
import YummyboxIcon from '../../Components/YummyboxIcon'
import AppConfig from '../../Config/AppConfig'
import CartActions, {
  BaseCart,
} from '../../Redux/V2/CartRedux'
import MealsActions, {
  InitMealsState,
  Meal,
  Meals,
  Stock,
} from '../../Redux/MealsRedux'
import AvailabilityDateService from '../../Services/AvailabilityDateService'
import LocaleFormatter from '../../Services/LocaleFormatter'
import {
  checkIfFilterActive,
} from '../../Services/MealService'
import { Colors } from '../../Themes'
import Styles from '../Styles/MealListScreenStyle'
import TagFilterScreen from '../TagFilterScreen'
import MealCardButton from './MealCardButton'
import { AppState } from '../../Redux/CreateStore'
import { SettingDataInterface } from '../../Redux/SettingRedux'
import {
  ImmutableArray,
  ImmutableObject,
} from 'seamless-immutable'
import {
  NavigationDrawerProp,
} from 'react-navigation-drawer/lib/typescript/src/types'
import { cartCandidateInterface } from '../../Components/AddToCartButton'
import FastImage from 'react-native-fast-image'
import SalesOrderService from '../../Services/SalesOrderService'
import {
  itemResponse,
  responseData,
} from '../../Services/V2/ApiCart'
import StockService from '../../Services/StockService'
import { baseLastUsed } from '../../Redux/V2/LastUsedRedux'

export interface MealListScreenProps {
  navigation: NavigationDrawerProp,
  isLoggedIn: boolean,
  cart: BaseCart,
  meals: ImmutableObject<InitMealsState>,
  stock: ImmutableObject<Stock>,
  setting: SettingDataInterface,
  unreadNotification: number,
  isConnected: boolean,
  fetchMeals: (date: string, kitchenCode: string) => void,
  removeFromCart: (item, date) => void,
  fetchFilteredMeals: (date, includeTags, excludeTags) => void,
  resetCart: () => void,
  loading: (status: boolean) => void,
  addToCart: (cart: responseData) => void,
  lastUsed: baseLastUsed,
}

export interface MealListScreenState {
  datePickerVisible: boolean,
  deliveryDates?: Moment.Moment[],
  weekendDates?: Moment.Moment[],
  previousIndex: number,
  nextIndex: number,
  currentIndex: number,
  indexToBeSelected: number,
  initialTab: number,
  loading: boolean,
  categories?: ImmutableArray<Meals> | Meals[],
  cart?: BaseCart,
  emptyCart: boolean,
  fetchingMeals: boolean,
  showTagFilter: boolean,
  offDates?: Moment.Moment[],
  showCartCombinationAlertModal: boolean,
  fromPage: any,
  startedObserveCutOffTime: boolean,
  selectedDate: string | Moment.Moment,
  cartCandidate?: cartCandidateInterface,
  maxItem: number,
}

const { width, height } = Dimensions.get('window')

const ITEM_IMAGE_WIDTH = width * 0.9
const ITEM_IMAGE_HEIGHT = width * 0.9

class MealListScreen extends React.Component<MealListScreenProps, MealListScreenState> {
  deliveryCutOffTimeOut
  willFocusSubscription

  state = {
    datePickerVisible: false,
    deliveryDates: [],
    weekendDates: [],
    previousIndex: null,
    nextIndex: null,
    currentIndex: null,
    indexToBeSelected: null,
    initialTab: 0,
    loading: false,
    categories: [],
    emptyCart: true,
    fetchingMeals: false,
    showTagFilter: false,
    offDates: [],
    showCartCombinationAlertModal: false,
    fromPage: null,
    startedObserveCutOffTime: false,
    selectedDate: '',
    cartCandidate: null,
    maxItem: 0,
  }

  static getDerivedStateFromProps(props: MealListScreenProps, state: MealListScreenState) {
    const { selectedDate, tab } = props.navigation.state.params
    const selectedDateObject = Moment.isMoment(selectedDate)
      ? selectedDate
      : Moment(selectedDate)
    if (selectedDate && (selectedDateObject.format('YYYY-MM-DD') !== state.selectedDate)) {
      const offDates = props.setting.off_dates
      const deliveryDates = AvailabilityDateService.getDeliveryDateList(offDates, props.setting.delivery_cut_off)
      const weekendDates = AvailabilityDateService.getWeekendDateWithinDeliveryDateList(offDates,
        props.setting.delivery_cut_off)
      let currentIndex = !selectedDate
        ? 0
        : _.findIndex(deliveryDates,
          (deliveryDate) =>
            deliveryDate.format('YYYY-MM-DD') === selectedDate
            && selectedDateObject.isAfter(Moment())
        )
      const initialTab = tab === null || tab === undefined
        ? 0
        : tab
      if (currentIndex < 0) {
        currentIndex = 0
      }

      return {
        deliveryDates,
        weekendDates,
        previousIndex: state.currentIndex === null
          ? (currentIndex === 0
            ? null
            : (currentIndex - 1))
          : (state.currentIndex === 0
            ? null
            : state.previousIndex),
        nextIndex: state.currentIndex === null
          ? (currentIndex === deliveryDates.length
            ? null
            : currentIndex + 1)
          : (state.nextIndex === deliveryDates.length
            ? null
            : state.nextIndex),
        currentIndex: state.currentIndex === null
          ? currentIndex
          : state.currentIndex,
        indexToBeSelected: !state.currentIndex === null
          ? currentIndex
          : state.currentIndex,
        initialTab: state.initialTab === undefined
          ? initialTab
          : state.initialTab,
        offDates: state.offDates === undefined
          ? offDates
          : state.offDates,
        selectedDate: state.selectedDate == undefined
          ? selectedDateObject.format('YYYY-MM-DD')
          : state.selectedDate,
        fetchingMeals: props.meals.fetching,
        categories: props.meals.meals,
        cart: props.cart.cartItems,
      }
    }

    if (props.navigation.state.params !== undefined) {
      if (props.navigation.state.params.selectedDate !== undefined) {
        const { deliveryDates } = state
        // cari posisi index tanggal yang sama di array 'deliveryDates' dengan 'this.props.startDate'.
        // index nya disimpan ke 'indexToBeSelected', dipakai untuk marked tanggal di render calender.
        // proses nya di this.markedSelectedDate()
        deliveryDates.map((value: Moment.Moment, key) => {
          if (value.format('YYYY-MM-DD') === props.navigation.state.params.startDate) {
            // UNSAFE_componentWillReceiveProps will run every time user change calendar, so we only update
            // when new date !=== old date
            if (state.indexToBeSelected !== key && state.currentIndex !== key && state.currentIndex !== key) {
              return {
                indexToBeSelected: key,
                currentIndex: key,
                previousIndex: (key - 1),
                fromPage: props.navigation.state.params.fromPage,
              }
            }
          }
        })
      }
    }

    return null
  }

  componentDidMount() {
    const { selectedDate } = this.props.navigation.state.params
    if (Moment()
      .add(1, 'days')
      .isSame(selectedDate, 'day') || selectedDate === null) {
      this.getMealListScreenData(this.state.currentIndex)
    } else {
      this.getMealListScreenDataByDate(selectedDate)
      this.setDateToBeSelected(selectedDate)
    }

    this.willFocusSubscription = this.props.navigation.addListener('willFocus', () => {
      this.getMealListScreenData(this.state.currentIndex)
    })
    this.listenForTimeChange(this.props.setting.delivery_cut_off)

    const {
      sales_order_total_limit,
    } = this.props.setting
    let limit = JSON.parse(sales_order_total_limit)
    if (typeof limit['item'] !== 'undefined'
      && typeof limit['item']['max'] === 'number'
    ) {
      this.setState({
        maxItem: limit['item']['max'],
      })
    }
  }

  componentDidUpdate(
    prevProps: Readonly<MealListScreenProps>, prevState: Readonly<MealListScreenState>): void {
    if (
      (this.props.isConnected && !prevProps.isConnected)
      || (this.state.currentIndex !== prevState.currentIndex)
    ) {
      this.getMealListScreenData(this.state.currentIndex)
    }

    if (this.props.navigation.state.params !== undefined) {
      if (this.props.navigation.state.params.startDate !== undefined) {
        const { deliveryDates } = this.state
        deliveryDates.map((value: Moment.Moment, key) => {
          if (value.format('YYYY-MM-DD') === this.props.navigation.state.params.startDate) {
            if (this.state.indexToBeSelected !== key
              && this.state.currentIndex !== key
            ) {
              const { lastUsed } = this.props
              const { code } = lastUsed
              this.props.fetchMeals(
                this.props.navigation.state.params.startDate,
                code,
              )
            }
          }
        })
      }
    }
  }

  componentWillUnmount() {
    clearTimeout(this.deliveryCutOffTimeOut)
    if (this.willFocusSubscription) {
      this.willFocusSubscription.remove()
    }
  }

  setDatePickerVisibility = (datePickerVisible) => {
    this.setState({ datePickerVisible })
  }

  listenForTimeChange = (deliveryCutOff) => {
    this.deliveryCutOffTimeOut = setTimeout(() => {
      const offDates = this.props.setting.off_dates
      const deliveryDates = AvailabilityDateService.getDeliveryDateList(offDates, deliveryCutOff)
      const currentIndex = this.state.currentIndex > 0
        ? this.state.currentIndex - 1
        : this.state.currentIndex
      this.setState({ deliveryDates }, () => this.getMealListScreenData(currentIndex))
    }, AvailabilityDateService.getDeliveryCutOffCountDown(deliveryCutOff))
  }

  getMealListScreenData = (currentIndex) => {
    if (this.props.isConnected) {
      const { off_dates, delivery_cut_off } = this.props.setting
      const deliveryDates = this.state.deliveryDates.length
        ? this.state.deliveryDates
        : AvailabilityDateService.getDeliveryDateList(off_dates, delivery_cut_off)
      if (deliveryDates.length > 0 && currentIndex >= 0) {
        const { lastUsed } = this.props
        const { code } = lastUsed
        this.props.fetchMeals(
          deliveryDates[currentIndex].format('YYYY-MM-DD'),
          code,
        )
      }
    }
  }

  getMealListScreenDataByDate = (date) => {
    const { lastUsed } = this.props
    const { code } = lastUsed
    this.props.fetchMeals(
      Moment(date)
        .format('YYYY-MM-DD'),
      code,
    )
  }

  setCategoriesState = (categories) => {
    if (categories === null) {
      return false
    }
    if (categories.hasOwnProperty('meals')) {
      this.setState({
        categories: categories.meals,
      })
    } else {
      this.setState({
        categories,
      })
    }
  }

  handleRowItemPress = (item, itemIndex, categoryIndex) => {
    const category = this.state.categories[categoryIndex]
    const navigationAction = NavigationActions.navigate({
      routeName: 'MealDetailScreen',
      params: {
        meals: category.items.data,
        selectedMealIndex: itemIndex,
        item_id: item.id,
        selectedDate: this.state.deliveryDates[this.state.currentIndex],
        transition: Platform.OS,
      },
    })
    this.props.navigation.dispatch(navigationAction)
  }

  applyFilter = (includeTags, excludeTags) => {
    const { deliveryDates, currentIndex } = this.state
    this.setState({ showTagFilter: false })

    this.props.fetchFilteredMeals(deliveryDates[currentIndex].format('YYYY-MM-DD'), includeTags, excludeTags)
    if (!this.props.meals.fetching) {
      this.setCategoriesState(this.props.meals.meals)
    }
  }

  closePicker = () => {
    this.setDatePickerVisibility(false)
  }

  closePickerButton = () => {
    return (
      <Button transparent onPress={() => this.closePicker()}>
        <Icon name='md-close' style={{
          color: Colors.bloodOrange,
          fontSize: scale(28),
        }} />
      </Button>
    )
  }

  setDateToBeSelected = (date) => {
    const convertDateToMoment = Moment(date)
      .utcOffset(7)
    let index = 0
    for (let i = 0; i < this.state.deliveryDates.length; i++) {
      if (this.state.deliveryDates[i].isSame(convertDateToMoment, 'day')) {
        index = i
        break
      }
    }
    this.setState({ currentIndex: index })
  }

  setSelectedDate = (toggleIndex) => {
    const { currentIndex, deliveryDates } = this.state
    let selectedIndex = currentIndex
    if (toggleIndex === 'prev') {
      selectedIndex = Number(currentIndex) - 1
      if (selectedIndex < 0) {
        selectedIndex = 0
      }
    } else if (toggleIndex === 'next') {
      selectedIndex = Number(currentIndex) + 1
      if (selectedIndex >= deliveryDates.length) {
        selectedIndex = deliveryDates.length - 1
      }
    }
    const newNextIndex = selectedIndex === deliveryDates.length - 1
      ? null
      : selectedIndex + 1
    const newPreviousIndex = selectedIndex === 0
      ? null
      : selectedIndex - 1
    this.setSelectedDateState(selectedIndex, newNextIndex, newPreviousIndex)
    this.setDatePickerVisibility(false)
  }

  setSelectedDateState = (selectedIndex, newNextIndex, newPreviousIndex) => {
    this.setState({
      currentIndex: selectedIndex,
      nextIndex: newNextIndex,
      previousIndex: newPreviousIndex,
    })
  }

  calendarPickerButton = () => {
    const {
      previousIndex,
      nextIndex,
      deliveryDates,
      currentIndex
    } = this.state

    if (deliveryDates.length <= 0 || currentIndex === null) {
      return null
    }

    const previousDayColor = previousIndex === null
      ? Colors.pinkishGrey
      : Colors.bloodOrange
    const nextDayColor = nextIndex === null
      ? Colors.pinkishGrey
      : Colors.bloodOrange

    return (
      <View style={Styles.calendarPickerContainer}>
        <TouchableOpacity
          testID='previousDate'
          accessibilityLabel='previousDate'
          onPress={() => this.setSelectedDate('prev')}
          disabled={previousIndex === null}
        >
          <Icon name='ios-arrow-back' style={{ color: previousDayColor }} />
        </TouchableOpacity>
        <TouchableOpacity
          testID='dateSeeAll'
          accessibilityLabel='dateSeeAll'
          onPress={() => this.setDatePickerVisibility(true)}
          style={Styles.calendarPickerButton}
        >
          <Text style={Styles.calendarPickerText}>
            {deliveryDates[currentIndex].format(AppConfig.dateFormat)}
          </Text>
          <FontAwesome
            name='sort-down'
            color={Colors.bloodOrange}
            size={scale(14)}
            style={{
              paddingLeft: scale(5),
              paddingBottom: verticalScale(7),
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          testID='nextDate'
          accessibilityLabel='nextDate'
          onPress={() => this.setSelectedDate('next')}
          disabled={nextIndex === null}
        >
          <Icon
            name='ios-arrow-forward'
            style={{ color: nextDayColor }}
          />
        </TouchableOpacity>
      </View>
    )
  }

  filterButton = () => {
    const { tags } = this.props.meals
    const disable = tags === null
    const filterActive = tags === null
      ? false
      : checkIfFilterActive(tags)
    return (
      <TouchableOpacity
        testID='filterButton'
        accessibilityLabel="filterButton"
        disabled={disable}
        onPress={() => this.setState({ showTagFilter: true })}
        style={{ paddingRight: scale(5) }}
      >
        <YummyboxIcon name='filter' color={Colors.bloodOrange} size={scale(25)} />
        {filterActive &&
          <View style={Styles.badge} />
        }
      </TouchableOpacity>
    )
  }

  renderTagFilterModal = () => {
    const { tags } = this.props.meals

    return (
      <TagFilterScreen
        dismissModal={() => this.setState({ showTagFilter: false })}
        applyFilter={(includeTags, excludeTags) => this.applyFilter(includeTags, excludeTags)}
        tags={tags}
      />
    )
  }

  markedSelectedDate = () => {
    let marked = {}
    const { deliveryDates, currentIndex } = this.state
    if (deliveryDates.length > 0 && currentIndex >= 0) {
      const formattedSelectedDate = deliveryDates[currentIndex].format('YYYY-MM-DD')
      marked[formattedSelectedDate] = { selected: true }
    }
    return marked
  }

  renderDatePickerModal = () => {
    const { datePickerVisible, deliveryDates, weekendDates } = this.state
    const offDates = this.props.setting.off_dates
    if (deliveryDates.length < 1) {
      return null
    }
    const weekendsAndOffDates = AvailabilityDateService
      .disableWeekendAndOffDates(deliveryDates, offDates, weekendDates)
    const selectedDate = this.markedSelectedDate()
    const markedDates = { ...selectedDate, ...weekendsAndOffDates }
    const modalContainerHeight = height * 0.75 >= 480
      ? height * 0.75
      : 480
    return (
      <RNModal
        isVisible={datePickerVisible}
        onBackButtonPress={() => this.setDatePickerVisibility(false)}
        onBackdropPress={() => this.setDatePickerVisibility(false)}
        backdropColor={Platform.OS === 'ios'
          ? 'rgba(255,255,255,0.9)'
          : 'rgba(58, 60, 63, 0.7)'}
        backdropOpacity={1}
        animationIn={'slideInDown'}
        animationOut={'slideOutUp'}
        animationInTiming={300}
        animationOutTiming={300}
        backdropTransitionInTiming={300}
        backdropTransitionOutTiming={300}
        style={{
          marginVertical: 0,
          marginHorizontal: 0,
        }}
      >
        <>
          <View
            style={{
              height: modalContainerHeight,
              backgroundColor: 'white',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowRadius: 4,
              shadowOpacity: 1,
              shadowColor: 'rgba(174,174,174, 0.5)',
            }}
          >
            <NavigationBar
              title='Select Date'
              leftSide={() => this.closePickerButton()}
            />
            <View
              style={{
                flex: 4,
              }}
            >
              <Calendar
                minDate={deliveryDates[0].format('YYYY-MM-DD')}
                maxDate={deliveryDates[13].format('YYYY-MM-DD')}
                markedDates={markedDates}
                hideExtraDays
                onDayPress={(date) => this.setDateToBeSelected(date.dateString)}
                style={{ marginHorizontal: scale(30) }}
                theme={{
                  arrowColor: Colors.green,
                  monthTextColor: Colors.green,
                  selectedDayTextColor: Colors.bloodOrange,
                  dayTextColor: Colors.greyishBrownTwo,
                  textSectionTitleColor: Colors.greyishBrownTwo,
                  textMonthFontFamily: 'Rubik-Regular',
                  textDayFontFamily: 'Rubik-Regular',
                  textDayHeaderFontFamily: 'Rubik-Regular',
                  selectedDayBackgroundColor: 'transparent',
                  selectedDotColor: Colors.bloodOrange,
                }}
              />
            </View>
            <View style={{
              height: 40,
              marginHorizontal: width * 0.4,
              marginBottom: 20,
            }}>
              <TouchableWithoutFeedback
                onPress={() => this.setSelectedDate('')}
              >
                <View
                  style={{
                    height: verticalScale(40),
                    backgroundColor: Colors.bloodOrange,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 5,
                  }}
                >
                  <Text
                    style={{
                      color: 'white',
                      fontFamily: 'Rubik-Regular',
                      fontSize: scale(18),
                    }}
                  >
                    Apply
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <View style={{ flex: 1 }} />
        </>
      </RNModal>
    )
  }

  renderCategoryTabs = (categories) => {
    if (categories === null) {
      const { lastUsed } = this.props
      const { code } = lastUsed
      this.props.fetchMeals(
        this.state.deliveryDates[this.state.currentIndex]
          .format('YYYY-MM-DD'),
        code,
      )
      return null
    }

    return (
      <Tabs
        initialPage={this.state.initialTab}
        renderTabBar={() => (
          <ScrollableTab
            style={{
              borderBottomWidth: 0,
              justifyContent: 'flex-start',
              height: verticalScale(31),
              marginBottom: verticalScale(20),
              backgroundColor: 'white',
            }}
            underlineStyle={{ backgroundColor: 'transparent' }}
          />
        )}
        style={{ backgroundColor: 'white' }}
        tabBarUnderlineStyle={{
          backgroundColor: Colors.grennBlue,
          height: 1,
          width: scale(30),
          marginLeft: scale(23),
        }}
      >
        {categories.map((category, index) => {
          const items = category.items.data
          const haveItems = items.length > 0
          return (
            <Tab
              heading={category.name}
              key={index}
              tabStyle={{
                backgroundColor: 'white',
                marginHorizontal: 0,
                paddingLeft: 5,
                paddingRight: 5,
                height: verticalScale(40),
                flexWrap: 'nowrap',
                width: 'auto',
                justifyContent: 'center',
              }}
              activeTabStyle={{
                backgroundColor: 'white',
                height: verticalScale(40),
                paddingLeft: 5,
                paddingRight: 5,
                width: 'auto',
                justifyContent: 'center',
              }}
              textStyle={{
                color: Colors.warmGreyTwo,
                fontFamily: 'Rubik-Regular',
                fontSize: scale(16),
                paddingHorizontal: 0,
              }}
              activeTextStyle={{
                color: Colors.green,
                fontFamily: 'Rubik-Regular',
                fontSize: scale(16),
              }}
            >
              {!haveItems && (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingHorizontal: scale(20),
                  }}
                >
                  <Text style={{
                    fontFamily: 'Rubik-Regular',
                    textAlign: 'center',
                  }}>
                    Sorry - No products match the filters criteria you applied for the selected date.
                    {'\n'}Please select other filters to apply
                  </Text>
                </View>
              )}
              {haveItems && (
                <FlatList
                  style={{
                    flex: 1,
                    paddingHorizontal: scale(20),
                  }}
                  data={items.map((item, i) => ({
                    ...item,
                    key: i,
                    categoryIndex: index,
                  }))}
                  renderItem={({ item, index }) =>
                    this.renderProductCard(
                      item,
                      index,
                      item.categoryIndex,
                      items.length,
                      category.name,
                      category.exclusive,
                    )
                  }
                />
              )}
            </Tab>
          )
        })}
      </Tabs>
    )
  }

  renderProductTags = (tags) => {
    const tagsLength = tags.length
    let tagsString = ''
    tags.map((tag, index) => {
      if (index === tagsLength - 1) {
        tagsString = tagsString + tag.name
      } else {
        tagsString = tagsString + tag.name + ' \u2022 '
      }
    })

    return (
      <View style={Styles.tagContainer}>
        <Text style={[
          Styles.tagText,
          { fontFamily: 'Rubik-Regular' },
        ]}>{tagsString}</Text>
      </View>
    )
  }

  renderProductCard = (
    meal: itemResponse | Meal,
    itemIndex: number,
    categoryIndex: number,
    itemLength: number,
    category: string,
    exclusive: boolean,
  ) => {
    const { deliveryDates, currentIndex } = this.state
    const tags = (meal as Meal).tags.data
    const haveTags = tags.length > 0
    const lastProductMargin = itemIndex === itemLength - 1
      ? { marginBottom: scale(60) }
      : {}
    const itemImage = (meal as Meal).catalog_image.original
    return (
      <TouchableWithoutFeedback
        testID={`meal_${categoryIndex}_${itemIndex}`}
        onPress={() => this.handleRowItemPress(meal, itemIndex, categoryIndex)}
        key={itemIndex.toString()}
      >
        <View>
          <FastImage
            style={{
              width: ITEM_IMAGE_WIDTH,
              height: ITEM_IMAGE_HEIGHT,
              borderRadius: 3,
            }}
            source={{
              uri: itemImage,
              priority: FastImage.priority.normal,
            }}
          />
          {haveTags && this.renderProductTags(tags)}
          <View style={{ marginTop: verticalScale(5) }}>
            <Text
              style={Styles.mealNameText}
            >
              {meal.name}
            </Text>
          </View>
          <View
            style={[
              Styles.priceContainer,
              lastProductMargin,
            ]}
          >
            <View
              style={{
                flex: 5,
                justifyContent: 'center',
              }}
            >
              <Text
                style={Styles.priceText}
              >
                {LocaleFormatter.numberToCurrency((meal as Meal).sale_price)}
              </Text>
            </View>
            <View
              style={{
                flex: 2,
                alignItems: 'flex-end',
              }}
            >
              <MealCardButton
                meal={meal}
                deliveryDate={deliveryDates[currentIndex]}
                itemIndex={itemIndex}
                categoryIndex={categoryIndex}
                onShowAlert={(cartCandidate: cartCandidateInterface) =>
                  this.setState({
                    showCartCombinationAlertModal: true,
                    cartCandidate,
                  })
                }
                onAddToCart={this.incrementHandler}
                onRemoveFromCart={this.decreaseHandler}
                cart={this.props.cart}
                category={category}
                exclusive={exclusive}
                stock={this.props.stock}
              />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  renderLoadingView = () => {
    return (
      <View style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <ActivityIndicator size='large' />
        <Text
          style={{
            fontFamily: 'Rubik-Regular',
            marginTop: verticalScale(5),
            color: Colors.brownishGrey,
          }}
        >
          Loading...
        </Text>
      </View>
    )
  }

  onPressCompleteOrder = () => {
    const { isLoggedIn } = this.props
    if (isLoggedIn) {
      if (this.props.cart.type === 'subscription') {
        this.props.navigation.navigate('SubscriptionScheduleScreen')
      } else {
        this.props.navigation.navigate('SingleOrderCheckoutScreen')
      }
    } else {
      this.props.navigation.navigate('LoginLandingScreen')
    }
  }

  onPressClearCart = () => {
    this.props.resetCart()
    setTimeout(() => {
      if (this.state.cartCandidate) {
        const {
          cart,
          loading,
          resetCart,
          addToCart,
          lastUsed,
        } = this.props

        const {
          item,
          date,
          quantity,
        } = this.state.cartCandidate

        SalesOrderService.addToCart(
          cart,
          item,
          date,
          quantity,
          lastUsed.code,
          loading,
          resetCart,
          addToCart,
        )

        this.setState({
          cartCandidate: null,
        })
      }
    }, 100)
  }

  _openHamburger = () => {
    const { navigation } = this.props
    navigation.toggleDrawer()
  }

  onPressShoppingCart = () => {
    const { navigation, isLoggedIn, cart } = this.props

    if (isLoggedIn) {
      if (cart.type !== 'subscription') {
        navigation.navigate({
          routeName: 'SingleOrderCheckoutScreen',
          key: 'SingleOrderCheckoutScreen',
        })
      } else {
        navigation.navigate('SubscriptionScheduleScreen')
      }
    } else {
      navigation.navigate('LoginLandingScreen')
    }
  }

  incrementHandler = async (
    item: Meal,
    date: Moment.Moment,
    quantity: number,
  ) => {
    const {
      cart,
      loading,
      resetCart,
      addToCart,
      isLoggedIn,
      navigation,
      stock,
      lastUsed,
    } = this.props

    const { maxItem } = this.state
    const remaining = StockService.getItemStock(item.id, stock.main)
    if ((maxItem > 0 && quantity > maxItem) || quantity > item['remaining_quantity']) {
      SalesOrderService.onMaxItem(maxItem, remaining)
      return
    }

    if (!isLoggedIn) {
      navigation.navigate('Login')
    } else {

      if (SalesOrderService.isCategoryExclusive(cart, item)) {
        this.setState({
          showCartCombinationAlertModal: true,
          cartCandidate: {
            item: item,
            quantity: quantity,
            date: date,
          },
        })
      }
      if (SalesOrderService.isCategoryQtyMax(cart, date, 3)) {
        return Toast.show({
          text: `Max. order qty for ${cart.category} is 3 items per delivery date.`,
          buttonText: '',
          duration: 3000,
        })
      }
      if (!SalesOrderService.isExclusive(cart, item)
        && SalesOrderService.isSameType(cart, 'item')
      ) {
        if (!cart.fetching) {
          const itemAvailability = await SalesOrderService.getItemAvailability(item.id, date, this.props.lastUsed.code, quantity)

          if (!itemAvailability) {
            Toast.show({
              text: 'Someone else has just bought our last stock for this location',
              buttonText: '',
              duration: 1500,
            })
          }
          else {
            SalesOrderService.addToCart(
              cart,
              item,
              date,
              quantity,
              lastUsed.code,
              loading,
              resetCart,
              addToCart,
            )
          }
        }
      } else {
        this.setState({
          showCartCombinationAlertModal: true,
          cartCandidate: {
            item: item,
            quantity: quantity,
            date: date,
          },
        })
      }
    }
    SalesOrderService.logEvent(item, quantity, item.category)
  }

  decreaseHandler = (
    item: Meal,
    date: Moment.Moment,
    quantity: number,
  ) => {
    const {
      cart,
      loading,
      resetCart,
      addToCart,
      lastUsed,
    } = this.props
    if (!cart.fetching) {
      SalesOrderService.decreaseHandler(
        item,
        date,
        quantity,
        loading,
        resetCart,
        addToCart,
        cart,
        null,
        this.props.removeFromCart,
        lastUsed.code,
      )
    }
  }

  render() {
    const {
      cart,
      isLoggedIn,
    } = this.props
    const {
      fetchingMeals,
      categories,
    } = this.state
    return (
      <Container testID='MealListScreen'>
        <NavigationBar
          title={() => this.calendarPickerButton()}
          leftSide='menu'
          rightSide={() => this.filterButton()}
          openHamburger={this._openHamburger}
          unread={this.props.unreadNotification}
        />
        <Content
          contentContainerStyle={{
            flex: 1
          }}
        >
          {
            !fetchingMeals
              ? this.renderCategoryTabs(categories)
              : this.renderLoadingView()
          }
          {this.renderDatePickerModal()}
          <Modal
            animationType='slide'
            transparent={true}
            visible={this.state.showTagFilter}
            onRequestClose={() => this.setState({ showTagFilter: false })}
          >
            {this.renderTagFilterModal()}
          </Modal>
          <ShoppingCartAlertModal
            primaryAction={this.onPressCompleteOrder}
            secondaryAction={this.onPressClearCart}
            visible={this.state.showCartCombinationAlertModal}
            dismiss={() => this.setState({ showCartCombinationAlertModal: false })}
          />
          <OfflineModal
            isConnected={this.props.isConnected}
          />
        </Content>
        {(cart.cartItems.length > 0 && this.props.cart.cartItems[0].items.length > 0) &&
          <Footer
            style={{
              backgroundColor: Colors.pinkishOrange
            }}
          >
            <ShoppingCartButton
              cart={cart}
              isLoggedIn={isLoggedIn}
              onPressHandle={this.onPressShoppingCart}
              testID='shoppingCartButtonOnMealList'
            />
          </Footer>
        }
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    isLoggedIn: state.login.user !== null,
    isConnected: state.network.isConnected,
    cart: state.cart,
    meals: state.meals,
    stock: state.meals.stock,
    setting: state.setting.data,
    unreadNotification: state.customerNotification.unread,
    lastUsed: state.lastUsed,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMeals: (date: string, kitchenCode: string) => dispatch(MealsActions.fetchMeals(date, kitchenCode)),
    fetchFilteredMeals: (date, includeTags, excludeTags) =>
      dispatch(MealsActions.fetchFilteredMeals(date, includeTags, excludeTags)),
    removeFromCart: (item, date) => dispatch(CartActions.removeFromCart(item, date)),
    resetCart: () => dispatch(CartActions.resetCart()),
    addToCart: (cart: responseData) => dispatch(CartActions.addToCart(cart)),
    loading: (status: boolean) => dispatch(CartActions.loading(status)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MealListScreen)
