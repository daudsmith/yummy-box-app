import {
  Text,
  View,
} from 'native-base'
import React from 'react'
import CartQuantitySpinner from '../../Components/CartQuantitySpinner'
import AddToCartButton from '../../Components/AddToCartButton'
import CartService from '../../Services/MealCartService'
import Styles from '../Styles/MealListScreenStyle'
import {
  Meal,
  Stock,
} from '../../Redux/MealsRedux'
import { BaseCart } from '../../Redux/V2/CartRedux'
import { Moment } from 'moment'
import LogEventService from '../../Services/LogEventService'
import StockService from '../../Services/StockService'
import { ImmutableObject } from 'seamless-immutable'
import {
  itemResponse,
} from '../../Services/V2/ApiCart'

export interface MealCardButtonProps {
  meal: itemResponse | Meal,
  cart: BaseCart,
  deliveryDate: Moment,
  itemIndex: number,
  categoryIndex: number,
  onShowAlert: (cartCandidate) => void,
  onAddToCart: (item: Meal, date: Moment, quantity: number) => void,
  onRemoveFromCart: (item: Meal, date: Moment, quantity: number) => void,
  category: string,
  exclusive: boolean,
  stock: ImmutableObject<Stock>
}

export interface MealCardButtonState {
  available: boolean,
  fetching: boolean,
  availability: number,
  showErrorModal: boolean,
}

class MealCardButton extends React.PureComponent<MealCardButtonProps, MealCardButtonState> {
  constructor(props) {
    super(props)
    this.state = {
      available: true,
      fetching: true,
      availability: 0,
      showErrorModal: false,
    }
  }

  isItemInCart() {
    const {
      cart,
      meal,
      deliveryDate,
    } = this.props
    return CartService
      .isItemExistInCart(
        cart.cartItems,
        meal,
        deliveryDate,
      )
  }

  onToggleAlertBox = () => {
    this.setState({
      showErrorModal: !this.state.showErrorModal
    })
  }

  handleAddToCartButtonPress = (meal: Meal | itemResponse, selectedDate: Moment, quantity: number) => {
    const { cart, onAddToCart, onShowAlert } = this.props

    // exclusive for category=exclusive
    if (cart.type != 'item' && cart.cartItems.length > 0) {
      const cartCandidate = {
        item: meal,
        date: selectedDate,
        quantity: quantity
      }
      return onShowAlert(cartCandidate)
    }

    onAddToCart((meal as Meal), selectedDate, quantity)
    this.logEvent(meal)

  }

  logEvent = (item) => {
    let logData = {
      currency: 'IDR',
      item_price: item.sale_price,
      item_id: item.id,
      item_name: item.name,
      item_category: 'item',
    }
    LogEventService.logEvent('add_to_cart', logData, ['default'])
  }

  render() {
    const {
      meal,
      deliveryDate,
      categoryIndex,
      itemIndex,
      onRemoveFromCart,
      exclusive,
      stock,
      cart,
    } = this.props
    const { cartItems } = cart

    let cartItem = CartService.getCartItem(cartItems, (meal as Meal), deliveryDate.format('YYYY-MM-DD'))
    let availability = StockService.getItemStock(meal.id, stock.main)

    let inCartQty = CartService.getItemQuantity(cartItems, meal.id, deliveryDate)
    let available = availability - inCartQty > 0
    const nextCartQty = inCartQty + 1

    return (
      <View>
        {
          !this.isItemInCart()
            ? (
              !available
                ?
                <Text style={[
                  Styles.priceText,
                  { fontSize: 14 },
                ]}>Sold Out</Text>
                :
                <AddToCartButton
                  testIDSuffix={`${categoryIndex}_${itemIndex}`}
                  handleAddToCartButtonPress={() =>
                    this.handleAddToCartButtonPress(meal, deliveryDate, nextCartQty)}
                />
            )
            : (
              <CartQuantitySpinner
                item={cartItem}
                date={deliveryDate.format('YYYY-MM-DD HH:mm:ss')}
                handleIncreasePress={this.handleAddToCartButtonPress}
                handleDecreasePress={onRemoveFromCart}
                max={availability}
                inCartQty={inCartQty}
                toggleAlertBox={this.onToggleAlertBox}
                showErrorModal={this.state.showErrorModal}
                exclusive={exclusive}
              />
            )
        }
      </View>
    )
  }
}

export default MealCardButton
