import _ from 'lodash'
import moment, { Moment } from 'moment'
import {
  Content,
  Container,
  Footer,
} from 'native-base'
import * as React from 'react'
import {
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import { connect } from 'react-redux'
import { AppEventsLogger } from 'react-native-fbsdk'
import AlertBox from '../Components/AlertBox'
import NavigationBar from '../Components/NavigationBar'
import SmartPaymentMethodPicker from '../Components/Payment/PaymentMethodPicker'
import SalesOrderResultModal from '../Components/SalesOrderResultModal'
import SelectDeliveryAddressButton from '../Components/SelectDeliveryAddressButton'
import SmartAddressForm from '../Components/SmartAddressForm'
import SmartDeliveryTimePicker from '../Components/SmartDeliveryTimePicker'
import WebView from '../Components/WebView'
import CartActions, {
  BaseCart,
  DeliveryAddress,
} from '../Redux/V2/CartRedux'
import CustomerCardActions, { InitCustomerCard } from '../Redux/CustomerCardRedux'
import OrderActions, { SalesOrderReduxInterface } from '../Redux/SalesOrderRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
//API
import ApiCustomerOrder from '../Services/ApiCustomerOrder'
import AvailabilityDateService from '../Services/AvailabilityDateService'
import LocaleFormatter from '../Services/LocaleFormatter'
import SalesOrderService from '../Services/SalesOrderService'
import SubscriptionService from '../Services/SubscriptionService'
import XenditService, {
  paymentInfoData,
  xenditCreditTokenResponse,
} from '../Services/XenditService'
import Colors from '../Themes/Colors'
import Styles from './Styles/SubscriptionConfirmationScreenStyle'
import FastImage from 'react-native-fast-image'
import { AppState } from '../Redux/CreateStore'
import { SettingDataInterface } from '../Redux/SettingRedux'
import { LoginState } from '../Redux/LoginRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import ApiCart, { CART_ITEM_STATUS } from '../Services/V2/ApiCart'

export interface SubsConfirmationProps {
  navigation: NavigationDrawerProp,
  login: LoginState,
  cart: BaseCart,
  customerCard: InitCustomerCard,
  orderPaymentInfo: object,
  newCreditCardInfo: paymentInfoData,
  selectedPayment: string,
  order: SalesOrderReduxInterface,
  userId: number,
  setting: SettingDataInterface,
  wallet: number,
  fetchingWallet: boolean,
  updateDeliveryAddress: (address) => void,
  setPaymentInfo: (paymentInfo) => void,
  clearPaymentInfo: () => void,
  createOrder: (order) => void,
  clearOrderSuccess: () => void,
  resetOrder: () => void,
  resetCart: () => void,
  addCard: (card) => void,
  updateDeliveryTime: (time: string) => void,
  addToCart: (cart) => void,
  setNewCreditCardInfo: (newCreditCardInfo) => void,
  setSelectedPayment: (selectedPayment) => void,
  fetchWallet: () => void,
  fetchCards: () => void,
  token: string,
  resetError: () => void,
}

export interface SubsConfirmationStates {
  validForSubscription: boolean,
  loading: boolean,
  showPostCreateOrderModal: boolean,
  createOrderSuccess?: boolean,
  authenticationUrl?: string,
  creditCardToken?: xenditCreditTokenResponse,
  creditCardError?: string,
  showWebView?: boolean,
  subscriptionArtwork: string,
  orderCutOffTime: Moment,
  showCannotCreateSubscriptionWithCutOffTime: boolean,
  disableConfirmSubscriptionButton: boolean,
  defaultAddress?: DeliveryAddress,
  fetchingWallet: boolean,
  isOrderNotAvailable: boolean,
}

class SubscriptionConfirmationScreen extends React.Component<SubsConfirmationProps, SubsConfirmationStates> {
  constructor(props: SubsConfirmationProps) {
    super(props)
    const { orderPaymentInfo, cart } = this.props
    let subscriptionArtwork = null
    if (typeof props.setting.subscription_artwork !== 'undefined') {
      subscriptionArtwork = props.setting.subscription_artwork
    }
    this.state = {
      validForSubscription: SubscriptionService
        .checkAllRequiredInformationValid(cart.cartItems, orderPaymentInfo),
      loading: false,
      showPostCreateOrderModal: false,
      createOrderSuccess: null,
      authenticationUrl: null,
      creditCardToken: null,
      creditCardError: '',
      showWebView: false,
      subscriptionArtwork: subscriptionArtwork,
      orderCutOffTime: this.getCutOffTime(this.props.setting.delivery_cut_off),
      showCannotCreateSubscriptionWithCutOffTime: false,
      disableConfirmSubscriptionButton: false,
      defaultAddress: null,
      fetchingWallet: true,
      isOrderNotAvailable: null,
    }
  }

  componentDidMount() {
    const { token } = this.props.login
    const { cartItems } = this.props.cart
    if (cartItems.length > 0) {
      ApiCustomerOrder.create()
        .getLastOrder(token)
        .then((response) => {
          return response.data.data
        })
        .then((lastOrder) => {
          this.props.fetchWallet()
          if (lastOrder.deliveries.data.length > 0) {
            const defaultAddress = {
              address: lastOrder.deliveries.data[0].address,
              latitude: parseFloat(lastOrder.deliveries.data[0].latitude),
              longitude: parseFloat(lastOrder.deliveries.data[0].longitude),
              address_details: lastOrder.deliveries.data[0].address_details,
              note: lastOrder.deliveries.data[0].note,
              phone: lastOrder.deliveries.data[0].phone,
              name: lastOrder.deliveries.data[0].name,
              landmark: lastOrder.deliveries.data[0].landmark,
            }
            this.setState({
              defaultAddress,
            }, () => {
              this.props.updateDeliveryAddress(defaultAddress)
            })
          }
        })
        .catch((error) => error)
    }

    this.props.fetchCards()
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const {
      cart,
      orderPaymentInfo,
      order,
      newCreditCardInfo,
      fetchingWallet,
    } = newProps
    const validForSubscription = SubscriptionService
      .checkAllRequiredInformationValid(
        cart.cartItems,
        orderPaymentInfo,
        newCreditCardInfo,
      )
    this.setState({
      validForSubscription,
      loading: order.loading,
    }, () => {
      order.createOrderSuccess && this.setState({
        createOrderSuccess: order.createOrderSuccess,
        showPostCreateOrderModal: true,
      })
    })
    if (typeof newProps.setting.subscription_artwork !== 'undefined') {
      this.setState({
        subscriptionArtwork: newProps.setting.subscription_artwork,
      })
    }
    if (this.props.fetchingWallet !== newProps.fetchingWallet) {
      this.setState({ fetchingWallet })
    }
  }

  componentWillUnmount() {
    this.props.clearPaymentInfo()
  }

  componentDidUpdate(prevProps: SubsConfirmationProps) {
    if (
      this.props.order.createOrderSuccess &&
      this.state.defaultAddress &&
      this.state.isOrderNotAvailable !== null &&
      (this.props.cart.cartItems[0].deliveryAddress &&
      prevProps.cart.cartItems[0].deliveryAddress) &&
      this.props.cart.cartItems[0].deliveryAddress.landmark !==
      prevProps.cart.cartItems[0].deliveryAddress.landmark
    ) {
      this._checkOrderAvailabilityInKitchen()
    }
  }

  getCutOffTime(cutOffSeconds) {
    return moment({
      hour: 0,
      minute: 0,
      second: 0,
    })
      .utcOffset(7)
      .add(1, 'days')
      .subtract(cutOffSeconds / 3600, 'hours')
  }

  navigateToHomeScreen() {
    this.setState({ showPostCreateOrderModal: false })
    this.props.resetOrder()
    this.props.resetCart()
    this.props.navigation.navigate('HomeScreen', {
      isFromCheckout: true
    })
  }

  closeModal(createOrderSuccess) {
    if (createOrderSuccess) {
      this.navigateToHomeScreen()
    } else {
      this.props.clearOrderSuccess()
      this.setState({
        showPostCreateOrderModal: false,
        createOrderSuccess: null,
        creditCardError: '',
        disableConfirmSubscriptionButton: false,
      })
    }
    this.setState({
      disableConfirmSubscriptionButton: false,
    })
  }

  todayIsNotFridayOrSaturday() {
    const FRIDAY = 5
    const SATURDAY = 6
    const day = moment()
      .utcOffset(7)
      .isoWeekday()
    return (day !== FRIDAY) && (day !== SATURDAY)
  }

  checkIfSubscriptionStartsTomorrow() {
    const { cartItems } = this.props.cart
    const { start_date } = cartItems[0].subscription
    return start_date === moment(this.state.orderCutOffTime)
      .add(1, 'days')
      .format('YYYY-MM-DD') || start_date === moment(this.state.orderCutOffTime)
      .format('YYYY-MM-DD')
  }

  processCreateOrder = async () => {
    await this._checkOrderAvailabilityInKitchen()
    if(!this.state.isOrderNotAvailable){
      this.setState({ disableConfirmSubscriptionButton: true })
      const { newCreditCardInfo, selectedPayment } = this.props

      const todayIsNotFridayOrSaturday = this.todayIsNotFridayOrSaturday()
      const subscriptionStartsTomorrow = todayIsNotFridayOrSaturday
        ? this.checkIfSubscriptionStartsTomorrow()
        : false
      const nowIsExceedingCutOffTime = subscriptionStartsTomorrow
        ? AvailabilityDateService.checkIfNowIsExceedingCutOffTime(this.state.orderCutOffTime)
        : false

      if (!nowIsExceedingCutOffTime) {
        if (selectedPayment === 'credit-card') {
          if (newCreditCardInfo !== null) {
            this.setState({
              loading: true,
              creditCardError: '',
            })
            XenditService.createToken(
              newCreditCardInfo,
              (authenticationUrl) => this.verification3DSCallback(authenticationUrl),
              () => {
                this.setState({ loading: false }, () => this.setState({
                  showPostCreateOrderModal: true,
                  createOrderSuccess: false,
                  creditCardError: 'We can\'t process your credit card.\nPlease use another card',
                }))
              },
              (creditCardToken) => this.setState({ creditCardToken }),
            )
            return
          }
        }
        this.setState({
          loading: true,
          creditCardError: '',
        })
        this.createOrder()
      } else {
        this.setState({ showCannotCreateSubscriptionWithCutOffTime: true })
      }
    }
  }

  verification3DSCallback(authenticationUrl) {
    this.setState({ authenticationUrl },
      () => this.setState({ loading: false }, () => this.setState({ showWebView: true })))
  }

  creditCard3DSValidationSuccess(authData) {
    const { creditCardToken } = this.state
    const { newCreditCardInfo } = this.props
    const { saveCreditCard } = newCreditCardInfo
    this.setState({ showWebView: false }, () => this.setState({ loading: true }))
    const ends_with = newCreditCardInfo.hasOwnProperty('card_number')
      ? newCreditCardInfo.card_number.substring(12, 16)
      : newCreditCardInfo.ends_with
    const cardType = newCreditCardInfo.type
    if (authData.status !== 'FAILED') {
      const paymentInfo = {
        type: cardType,
        ends_with,
        status: authData.status,
        token: creditCardToken.id,
        authorization: newCreditCardInfo.is_multiple_use
          ? authData.id
          : creditCardToken.authentication_id,
      }
      this.props.setPaymentInfo(paymentInfo)
      if (saveCreditCard) {
        const cardData = {
          account_id: this.props.userId,
          token: creditCardToken.id,
          authorization_id: authData.id,
          type: cardType,
          ends_with,
        }
        this.props.addCard(cardData)
      }
      setTimeout(() => this.createOrder(), 1500)
    }
  }

  createOrder() {
    const { cart, orderPaymentInfo, selectedPayment } = this.props
    AppEventsLogger.logEvent('subscription_order', null, ['default'])
    const isMultipleUse = true
    const order = SalesOrderService.buildSalesOrder(
      cart,
      selectedPayment,
      orderPaymentInfo,
      isMultipleUse,
      this.props.order.orderTotal,
    )
    this.props.createOrder(order)
  }

  renderSubscriptionFirstMealInfo() {
    return (
      <View style={{ marginBottom: 35 }}>
        <Text style={Styles.firstMealText}>
          {'You will only be charged when order is placed'}
        </Text>
        <Text style={Styles.firstMealText}>
          You will be charged
          <Text style={Styles.firstMealBoldText}>{` ${LocaleFormatter.numberToCurrency(
            this.props.cart.cartItems[0].first_item.price)} `}</Text>
          on
          <Text style={Styles.firstMealBoldText}>{` ${moment(
            this.props.cart.cartItems[0].subscription.start_date)
            .format('ddd, DD MMM YYYY')} `}</Text>
          for your first meal
        </Text>
      </View>
    )
  }

  _checkOrderAvailabilityInKitchen = async () => {
    await ApiCart.create()
      .validate(this.props.cart, this.props.token)
      .then(res => {
        const status = res.data.data.subscription.status
        const isOrderNotAvailable = status !== CART_ITEM_STATUS.AVAILABLE
        this.setState({
          isOrderNotAvailable
        })
      })
  };

  _onChangeAddress = () => {
    this.setState({
      isOrderNotAvailable: false,
    })
    this.props.resetError()
    SalesOrderService.getDeliveryAddress(
      this.props.token,
      this.setLoading,
      this.props.navigation,
      true
    )
  }

  renderDeliverySection() {
    let addressIsNotSet = typeof this.props.cart.cartItems[0].deliveryAddress !== 'undefined'
      ? (typeof this.props.cart.cartItems[0].deliveryAddress.name === 'undefined')
      : true
    if (addressIsNotSet && !_.isEmpty(this.state.defaultAddress)) {
      const defaultAddress = {
        name: this.state.defaultAddress.name,
        phone: this.state.defaultAddress.phone,
        note: this.state.defaultAddress.note,
        place: this.state.defaultAddress,
      }
      this.props.updateDeliveryAddress(defaultAddress)
    }

    return (
      <View>
        <View style={Styles.deliveryDetailsContainer}>
          <Text style={Styles.deliveryDetailsTitleText}>Delivery Details</Text>
        </View>
        <SmartDeliveryTimePicker
          type={'checkout'}
          date={this.props.cart.cartItems[0].date}
          useMultipleAddress={false}
          cart={this.props.cart}
          setting={this.props.setting}
          updateDeliveryTime={this.props.updateDeliveryTime}
        />
        {_.isEmpty(this.props.cart.cartItems[0].deliveryAddress)
          ?
          <SelectDeliveryAddressButton
            testID='selectAddressSubscription'
            accessibilityLabel="selectAddressSubscription"
            deliveryAddressOptionPicker={() => SalesOrderService
              .getDeliveryAddress(
                this.props.token,
                this.setLoading,
                this.props.navigation,
                true,
              )}
          />
          :
          <SmartAddressForm
            onChangeAddressPress={this._onChangeAddress}
            date={this.props.cart.cartItems[0].date}
            useMultipleAddress={false}
            navigation={this.props.navigation}
            cart={this.props.cart}
            updateUserCart={this.props.addToCart}
          />
        }
      </View>
    )
  }

  navigationGoBack = () => {
    const parent = this.props.navigation.dangerouslyGetParent()
    if (parent && parent.state && (parent.state.index > 0)) {
      this.props.navigation.goBack()
    } else {
      this.props.navigation.navigate('HomeScreen')
    }
  }

  navigateToTopUpScreen = () => {
    this.props.navigation.navigate('RootTopUpScreen', { navigationParams: 'back' })
  }

  setLoading = async (status: boolean) => {
    this.setState({
      loading: status,
    })
  }

  render() {
    const {
      validForSubscription,
      disableConfirmSubscriptionButton,
      showWebView,
      showPostCreateOrderModal,
      createOrderSuccess,
      creditCardError,
      subscriptionArtwork,
      fetchingWallet,
    } = this.state
    const {
      order,
    } = this.props
    const disableConfirmButton = ((!validForSubscription || disableConfirmSubscriptionButton)
      && fetchingWallet)
    const confirmSubscriptionButtonColor = disableConfirmButton
      ? { backgroundColor: Colors.pinkishGrey }
      : {}
    const cartIsEmpty = this.props.cart.cartItems.length === 0
    const {
      loading,
      loadingText,
    } = order
    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title='Subscription Confirmation'
          leftSideNavigation={this.navigationGoBack}
        />
        <Content
          contentContainerStyle={{ marginHorizontal: 20 }}
          keyboardShouldPersistTaps='handled'
        >
          {subscriptionArtwork !== null
            ? (
              <View style={{ marginTop: 20 }}>
                <FastImage
                  style={{
                    width: '100%',
                    height: 165,
                    borderRadius: 3,
                  }}
                  source={{
                    uri: subscriptionArtwork,
                    priority: FastImage.priority.normal,
                  }}
                  resizeMode={FastImage.resizeMode.cover}
                />
              </View>
            )
            : (
              <View style={Styles.artwork} />
            )}
          {!cartIsEmpty &&
          <View>
            {this.renderDeliverySection()}
            <SmartPaymentMethodPicker
              navigateToTopUpScreen={this.navigateToTopUpScreen}
              type='create_subscription'
              wallet={this.props.wallet}
              total={this.props.cart.totals.grandTotal}
              setting={this.props.setting}
              order={this.props.order}
              customerCard={this.props.customerCard}
              setSelectedPayment={this.props.setSelectedPayment}
              setNewCreditCardInfo={this.props.setNewCreditCardInfo}
              setPaymentInfo={this.props.setPaymentInfo}
              cartType={this.props.cart.type}
              cartItems={this.props.cart.cartItems}
              fetchingWallet={fetchingWallet}
            />
            {this.renderSubscriptionFirstMealInfo()}
          </View>
          }
        </Content>

        <Spinner
          visible={loading || this.state.loading}
          textContent={loadingText}
          textStyle={{
            color: 'white',
            fontFamily: 'Rubik-Regular',
            fontSize: 14,
          }}
        />
        <SalesOrderResultModal
          visible={showPostCreateOrderModal}
          invoiceUrl={this.props.order.invoiceUrl}
          modalType={createOrderSuccess
            ? 'thank-you'
            : 'sorry'} onPress={() => this.closeModal(createOrderSuccess)}
          text={creditCardError}
        />
        {this.state.authenticationUrl !== null &&
        <WebView
          visible={showWebView}
          dismiss={() => this.setState({ showWebView: false })}
          url={this.state.authenticationUrl}
          validationSuccess={(authData) => this.creditCard3DSValidationSuccess(authData)}
        />
        }
        <AlertBox
          primaryAction={() => {}}
          primaryActionText='OK'
          dismiss={() => this.setState({ showCannotCreateSubscriptionWithCutOffTime: false })}
          text={`Sorry, cannot activate a subscription with starting date tomorrow after ${this.state.orderCutOffTime.format(
            'HH:mm')}`}
          visible={this.state.showCannotCreateSubscriptionWithCutOffTime}
        />
        <AlertBox
          primaryAction={this._onChangeAddress}
          secondaryAction={() => {
            this.props.resetCart()
            this.props.navigation.navigate('HomeScreen', {
              isFromCheckout: true
            })
          }}
          primaryActionText='Change Location'
          secondaryActionText="Change Playlist"
          dismiss={() => {}}
          title="Not Available"
          text="Oops! The playlist that you chose is not available in your selected location. Please choose another playlist or another location."
          visible={this.state.isOrderNotAvailable}
        />
        <Footer style={{
          backgroundColor: disableConfirmButton
            ? Colors.pinkishGrey
            : Colors.bloodOrange,
        }}>
          <TouchableWithoutFeedback
            testID='confirmSubscription'
            accessibilityLabel='confirmSubscription'
            disabled={disableConfirmButton}
            onPress={() => this.processCreateOrder()}>
            <View style={[
              Styles.confirmSubscriptionButton,
              confirmSubscriptionButtonColor,
            ]}>
              <Text style={Styles.confirmSubscriptionButtonText}>Confirm Your Subscription</Text>
            </View>
          </TouchableWithoutFeedback>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    login: state.login,
    cart: state.cart,
    customerCard: state.customerCard,
    orderPaymentInfo: state.order.paymentInfo,
    newCreditCardInfo: state.order.newCreditCardInfo,
    selectedPayment: state.order.selectedPayment,
    order: state.order,
    userId: state.login.user.id,
    token: state.login.token,
    setting: state.setting.data,
    wallet: state.customerAccount.wallet,
    fetchingWallet: state.customerAccount.fetchingWallet,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateDeliveryAddress: (address) =>
      dispatch(CartActions.updateAddress(address)),
    setPaymentInfo: (paymentInfo) => dispatch(OrderActions.setPaymentInfo(paymentInfo)),
    clearPaymentInfo: () => dispatch(OrderActions.clearPaymentInfo()),
    createOrder: (order) => dispatch(OrderActions.createOrder(order)),
    clearOrderSuccess: () => dispatch(OrderActions.clearOrderSuccess()),
    resetOrder: () => dispatch(OrderActions.resetOrder()),
    resetCart: () => dispatch(CartActions.resetCart()),
    addCard: (card) => dispatch(CustomerCardActions.addCard(card)),
    updateDeliveryTime: (time: string) =>
      dispatch(CartActions.updateTime(time)),
    addToCart: (cart) => dispatch(CartActions.addToCart(cart)),
    setNewCreditCardInfo: (newCreditCardInfo) => dispatch(OrderActions.setNewCreditCardInfo(newCreditCardInfo)),
    setSelectedPayment: (selectedPayment) => dispatch(OrderActions.setSelectedPayment(selectedPayment)),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
    resetError: () => dispatch(OrderActions.resetError()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubscriptionConfirmationScreen)
