import * as React from 'react'
import { StatusBar, View } from 'react-native'
import { MessageBar, MessageBarManager } from 'react-native-message-bar'
import { connect } from 'react-redux'
import analytics from '@react-native-firebase/analytics'
import crashlytics from '@react-native-firebase/crashlytics'
import messaging from '@react-native-firebase/messaging'
import perf from '@react-native-firebase/perf'
import dynamicLinks from '@react-native-firebase/dynamic-links'
import AppConfig from '../Config/AppConfig'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import AutoRoutingActions from '../Redux/AutoRoutingRedux'
import CustomerAccountActions, { CustomerAccountReduxState } from '../Redux/CustomerAccountRedux'
import LoginActions, { User } from '../Redux/LoginRedux'
import LocaleFormatter from '../Services/LocaleFormatter'
import NavigationService from '../Services/NavigationService'
import { Colors } from '../Themes/index'

// Styles
import styles from './Styles/RootContainerStyles'
import { AppState } from '../Redux/CreateStore'

export interface RootContainerProps {
  user: User,
  token: string,
  customerAccount: CustomerAccountReduxState,
  fetchWallet: () => void,
  updateFcmToken: (fcmToken: string) => void,
  addRoute: (routeName: string, params: any, isOpeningApp: boolean) => void,
}

class RootContainer extends React.Component<RootContainerProps> {
  msgBarRef = React.createRef()
  unsubscribeDeepLink: () => void
  unsubscribeMessage: () => void

  componentDidMount() {
    MessageBarManager.registerMessageBar(this.msgBarRef.current)
    this.customTrace()
    this.listenForDynamicLink()
    this.listenForFCMTokenRefresh()
    this.listenForPushNotification()
    crashlytics().log('Test Message')
  }

  componentWillUnmount() {
    MessageBarManager.unregisterMessageBar()
    this.unsubscribeDeepLink()
    this.unsubscribeMessage()
  }

  componentDidUpdate(prevProps: Readonly<RootContainerProps>): void {
    let uid = null
    let customerGroup = null
    if (this.props.user !== prevProps.user) {
      if (this.props.user) {
        uid = String(this.props.user.id)
        customerGroup = this.props.customerAccount.isClient
          ? 'corporate'
          : 'individual'
      }
      analytics().setUserId(uid)
      analytics().setUserProperty('customer_group', customerGroup)
    }
  }

  customTrace = async () => {
    const trace = await perf().startTrace('custom_trace')

    // Define trace meta details
    trace.putAttribute('user', 'abcd')
    trace.putMetric('credits', 30)

    // Stop the trace
    await trace.stop()
  }

  listenForFCMTokenRefresh = () => {
    messaging().onTokenRefresh((fcmToken: string) => {
      this.props.updateFcmToken(fcmToken)
    })
  }

  listenForPushNotification = async () => {
    const authStatus = await messaging().requestPermission()
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL

    if (enabled) {
      messaging().getInitialNotification().then((payload) => {
        this.processNotificationPayload(payload, true)
      })
    }

    this.unsubscribeMessage = messaging().onMessage((message) => {
      if (message.hasOwnProperty('opened_from_tray')) {
        this.processNotificationPayload(message, false)
      } else {
        this.props.fetchWallet()
        this.showInAppNotification(message.notification.title, message.notification.body)
      }
    })
  }

  listenForDynamicLink = () => {
    dynamicLinks().getInitialLink().then((link) => {
      if (link) {
        this.processLink(LocaleFormatter.deepLinkUrlParser(link.url), true)
      }
    })

    this.unsubscribeDeepLink = () => dynamicLinks().onLink((link) =>
      this.processLink(
        LocaleFormatter.deepLinkUrlParser(link.url),
        false
      ))
  }

  processNotificationPayload = (payload, isOpeningApp) => {
    if (payload === null || typeof payload === 'undefined') {
      return
    }
    if (payload.hasOwnProperty('screen')) {
      const { screen } = payload
      const routeName = NavigationService.routingHelper(screen)
      if (screen === 'playlist') {
        const params = {
          id: payload.id,
        }
        this.props.addRoute(routeName, params, isOpeningApp)
      } else if (screen === 'meal') {
        const params = {
          date: payload.date,
          tab: payload.tab,
        }
        this.props.addRoute(routeName, params, isOpeningApp)
      }
    }
  }

  processLink = (url, isOpeningApp) => {
    const parametersArray = url.replace(AppConfig.FIREBASE_LONG_DOMAIN, '').
      replace('?', '').
      split('&')
    let params = {}
    let routeName = ''
    for (let parameter of parametersArray) {
      const keyAndValue = parameter.split('=')
      if (keyAndValue[0] === 'screen') {
        routeName = NavigationService.routingHelper(keyAndValue[1])
      } else {
        params[keyAndValue[0]] = keyAndValue[1]
      }
    }
    this.props.addRoute(routeName, params, isOpeningApp)
  }

  showInAppNotification = (title, message) => {
    MessageBarManager.showAlert({
      title: title,
      message: message,
      alertType: 'warning',
    })
  }

  render() {
    return (
      <View style={styles.applicationView}>
        <StatusBar
          backgroundColor={Colors.darkerDark}
        />
        <ReduxNavigation />
        <MessageBar ref={this.msgBarRef}/>
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => (
  {
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    updateFcmToken: (fcmToken) => dispatch(
      LoginActions.updateFcmToken(fcmToken)),
    addRoute: (routeName, params, isOpeningApp) => dispatch(
      AutoRoutingActions.addRoute(routeName, params, isOpeningApp)),
  }
)

const mapStateToProps = (state: AppState) => {
  return {
    user: state.login.user,
    token: state.login.token,
    customerAccount: state.customerAccount,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer)
