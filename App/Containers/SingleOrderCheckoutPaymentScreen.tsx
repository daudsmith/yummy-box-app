import _ from 'lodash'
import moment, { Moment } from 'moment'
import {
  Content,
  Container,
  Footer,
} from 'native-base'
import React, { Component } from 'react'
import {
  AppState as RN_AppState,
  AppStateStatus,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import {
  verticalScale,
  scale,
} from 'react-native-size-matters'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { AppEventsLogger } from 'react-native-fbsdk'
import { connect } from 'react-redux'
import AlertBox from '../Components/AlertBox'
import NavigationBar from '../Components/NavigationBar'
import PaymentMethodPicker, { PaymentOptionType } from '../Components/Payment/PaymentMethodPicker'
import SalesOrderResultModal, { SalesOrderModalType } from '../Components/SalesOrderResultModal'
import WebView from '../Components/WebView'
import YummyboxIcon from '../Components/YummyboxIcon'
import CartActions, { BaseCart } from '../Redux/V2/CartRedux'
import CustomerCardActions, { InitCustomerCard } from '../Redux/CustomerCardRedux'
import OrderActions, { SalesOrderReduxInterface } from '../Redux/SalesOrderRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import AvailabilityDateService from '../Services/AvailabilityDateService'
import LocaleFormatter from '../Services/LocaleFormatter'
import SalesOrderService from '../Services/SalesOrderService'
import XenditService, { xenditCreditTokenResponse } from '../Services/XenditService'
import Colors from '../Themes/Colors'
import styles from './Styles/SingleOrderCheckoutPaymentScreenStyle'
import { ScrollView } from 'react-native-gesture-handler'
import { AppState } from '../Redux/CreateStore'
import { User } from '../Redux/LoginRedux'
import { SettingDataInterface } from '../Redux/SettingRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { responseData } from '../Services/V2/ApiCart'
import { AlertConfirmLinkedToOtherApp, AlertPaymentDeepLink } from '../Components/AlertLinkedToOtherApp'
import { LinkingFromOtherAppProps, _linkingFromOtherApp } from '../util/_linkingApp'
import { NavigationActions, StackActions } from 'react-navigation'

export interface CoPaymentProps {
  navigation: NavigationDrawerProp,
  cart: BaseCart,
  customerCard: InitCustomerCard,
  user: User,
  token: string,
  order: SalesOrderReduxInterface,
  isConnected: boolean,
  setting: SettingDataInterface,
  wallet: number,
  fetchingWallet: boolean,
  emptyUserCart: () => void,
  addCard: (card) => void,
  createOrder: (order) => void,
  setPaymentInfo: (paymentInfo) => void,
  clearPaymentInfo: () => void,
  setOrderTotal: (orderTotal) => void,
  resetOrder: () => void,
  clearOrderSuccess: () => void,
  clearCoupon: () => void,
  setNewCreditCardInfo: (newCreditCardInfo) => void,
  setSelectedPayment: (selectedPayment: PaymentOptionType) => void,
  fetchWallet: () => void,
  fetchCards: () => void,
  resetError: () => void,
  loading: (status: boolean) => void,
  addToCart: (cart: responseData) => void,
}

export interface CoPaymentState {
  isValidForOrder: boolean,
  authenticationUrl?: string,
  coupon?: string,
  creditCardToken?: xenditCreditTokenResponse,
  errorMessage?: string,
  showWebView: boolean,
  showLoading: boolean,
  showModal: boolean,
  showFailedToCreateOrderModalWithCutOffTime: boolean,
  modalType?: SalesOrderModalType,
  showAlert: boolean,
  scrollY?: number,
  virtualAccountCutOffTime: Moment,
  orderCutOffTime: Moment,
  disableProceedButton: boolean,
  checkoutUrl?: string,
  showCartAlert: boolean,
  cartAlertText: string
  isLinkedPaymentSuccess: boolean,
  isLinkedPaymentAlert: boolean,
  showAlertConfirmLinkedToOtherApp: boolean,
  appState: AppStateStatus,
}

const isPaymentDeepLinked = (selectedPayment: PaymentOptionType) => {
  return selectedPayment === 'gopay' || selectedPayment === 'shopeepay'
}

class SingleOrderCheckoutPaymentScreen extends Component<CoPaymentProps, CoPaymentState> {
  onFocus
  onBlur
  mounted = false

  constructor(props: CoPaymentProps) {
    super(props)
    const cartLength = this.props.cart.cartItems.length
    const cartType = this.props.cart.type
    const cartItemsContainsMultipleDate = cartLength > 1
    const customerHasDefaultCreditCard = this.props.customerCard.cards.length > 0
    const isValidForOrder = cartItemsContainsMultipleDate || cartType === 'package'
      ? (customerHasDefaultCreditCard)
      : false
    const cutOffInterval = parseInt(JSON.parse(this.props.setting.bank_transfer_payment_option_cutoff)) / 3600
    const virtualAccountCutOffTime = moment({
      hour: 0,
      minute: 0,
      second: 0,
    })
      .utcOffset(7)
      .add(1, 'day')
      .subtract(cutOffInterval, 'hour')

    this.state = {
      isValidForOrder,
      authenticationUrl: null,
      coupon: props.cart.coupon,
      creditCardToken: null,
      errorMessage: '',
      showWebView: false,
      showLoading: false,
      showModal: false,
      showFailedToCreateOrderModalWithCutOffTime: false,
      modalType: 'sorry',
      showAlert: false,
      scrollY: 0,
      virtualAccountCutOffTime,
      orderCutOffTime: this.getCutOffTime(this.props.setting.delivery_cut_off),
      disableProceedButton: false,
      checkoutUrl: null,
      showCartAlert: false,
      cartAlertText: '',
      isLinkedPaymentSuccess: false,
      isLinkedPaymentAlert: false,
      showAlertConfirmLinkedToOtherApp: false,
      appState: RN_AppState.currentState,
    }
  }

  componentDidMount() {
    this.props.fetchWallet()
    this.props.fetchCards()

    this.onFocus = this.props.navigation.addListener('didFocus', () => {
      this.props.fetchWallet()
      this.props.fetchCards()
      this.mounted = true
    })
    this.onBlur = this.props.navigation.addListener('didBlur', () => this.mounted = false)
    this._checkDeepLink().onMounted()
  }

  componentWillUnmount() {
    this.props.clearCoupon()
    this.props.clearPaymentInfo()
    if (this.onFocus) {
      this.onFocus.remove()
    }
    if (this.onBlur) {
      this.onBlur.remove()
    }
    this._checkDeepLink().onUnmounted()
  }

  componentDidUpdate(prevProps) {
    const { order } = this.props
    if (
      (order.selectedPayment !== prevProps.order.selectedPayment)
      || (order.newCreditCardInfo !== prevProps.order.newCreditCardInfo)
      || (order.paymentInfo !== prevProps.order.paymentInfo)
    ) {
      this.setOrderValidity(order)
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (
      props.order.createOrderSuccess
      && !props.order.loading
      && !props.order.checkoutUrl
    ) {
      return {
        showModal: props.order.createOrderSuccess,
        modalType: props.order.createOrderSuccess
          ? 'thank-you'
          : 'sorry',
      }
    }
    if (
      props.order.createOrderSuccess
      && !props.order.loading
      && props.order.checkoutUrl
      && props.order.checkoutUrl !== state.checkoutUrl
    ) {
      return {
        checkoutUrl: props.order.checkoutUrl,
        showWebView: !!props.order.checkoutUrl,
      }
    }

    if (
      !props.order.createOrderSuccess
      && !props.order.loading
      && props.order.error !== ''
    ) {
      // let _errorMessage = `We can\'t process your ${props.order.selectedPayment}.\n Please try again later`
      // if (props.order.selectedPayment === 'credit-card') {
      //   _errorMessage = 'We can\'t process your credit card.\nPlease use another card'
      // }
      let cartAlertText = 'Oops! One or more products that you chose have recently sold out, partially available or not available in your location. Please choose another product or another location'
      if (props.cart.type == 'package') {
        cartAlertText = 'Oops! Product from one or more dates that you chose have recently sold out in your selected location. Please choose another date or another location'
      }
      return {
        showCartAlert: true,
        cartAlertText: cartAlertText,
        disableProceedButton: false,
      }
    }
    return null
  }

  getCutOffTime = (cutOffSeconds: number | string): Moment => {
    return moment({
      hour: 0,
      minute: 0,
      second: 0,
    })
      .utcOffset(7)
      .add(1, 'days')
      .subtract((cutOffSeconds as number) / 3600, 'hours')
  }

  setOrderValidity(order) {
    const { selectedPayment, paymentInfo, newCreditCardInfo } = order
    const isValidForOrder = SalesOrderService.checkAllRequiredInformationValid(selectedPayment, paymentInfo,
      newCreditCardInfo)
    this.setState({ isValidForOrder })
  }

  userIsValidForUsingVirtualAccount() {
    const now = moment()
      .utcOffset(7)
    return now.isBefore(this.state.virtualAccountCutOffTime)
  }

  todayIsNotFridayOrSaturday() {
    const FRIDAY = 5
    const SATURDAY = 6
    const day = moment()
      .utcOffset(7)
      .isoWeekday()
    return (day !== FRIDAY) && (day !== SATURDAY)
  }

  checkIfCartHasItemForTomorrow() {
    const { cartItems, type } = this.props.cart
    if (type === 'item') {
      const nearestMealDateIndex = _.map(cartItems, (cartItem) => {
        return cartItem.date === moment(this.state.orderCutOffTime)
          .add(1, 'days')
          .format('YYYY-MM-DD') || cartItem.date === moment(this.state.orderCutOffTime)
          .format('YYYY-MM-DD')
      })
      return nearestMealDateIndex > -1
    } else if (type === 'package') {
      const firstMealDate = cartItems[0].packages[0].dates[0]
      return firstMealDate === moment(this.state.orderCutOffTime)
        .add(1, 'days')
        .format('YYYY-MM-DD') || firstMealDate === moment(this.state.orderCutOffTime)
        .format('YYYY-MM-DD')
    }
  }

  processPaymentTransaction() {
    this.setState({ disableProceedButton: true })
    const { newCreditCardInfo, selectedPayment } = this.props.order
    const { total } = this.props.cart.totals

    const todayIsNotFridayOrSaturday = this.todayIsNotFridayOrSaturday()
    const cartHasItemForTomorrow = todayIsNotFridayOrSaturday
      ? this.checkIfCartHasItemForTomorrow()
      : false
    const nowIsExceedingCutOffTime = cartHasItemForTomorrow
      ? AvailabilityDateService.checkIfNowIsExceedingCutOffTime(this.state.orderCutOffTime)
      : false

    if (!nowIsExceedingCutOffTime) {
      if (selectedPayment === 'credit-card') {
        if (newCreditCardInfo !== null) {
          this.setState({
            showLoading: true,
            errorMessage: '',
          })
          XenditService.createToken(
            newCreditCardInfo,
            (authenticationUrl) => this.verification3DSCallBack(authenticationUrl),
            () => {
              this.setState({ showLoading: false }, () => this.setState({
                showModal: true,
                errorMessage: 'We can\'t process your credit card.\nPlease use another card',
              }))
            },
            (creditCardToken: xenditCreditTokenResponse) => this.setState({ creditCardToken }),
            total,
          )
          return
        }
      }

      if (selectedPayment === 'virtual-account' && !this.userIsValidForUsingVirtualAccount()) {
        this.setState({ showAlert: true })
        return
      }
      if (isPaymentDeepLinked(selectedPayment)) {
        this.setState({ showAlertConfirmLinkedToOtherApp: true })
      } else {
        this.setState(
          {
            showLoading: true,
            errorMessage: ''
          },
          this.proceedToCreatingOrderData
        )
      }
    } else {
      this.setState({
        showFailedToCreateOrderModalWithCutOffTime: true
      })
    }
  }

  verification3DSCallBack(authenticationUrl) {
    this.setState({
        authenticationUrl
      }, () => this.setState({
        showLoading: false
      }, () => this.setState({ showWebView: true })))
  }

  proceedToCreatingOrderData() {
    const { totals } = this.props.cart
    const { total } = totals
    AppEventsLogger.logEvent('single_order', null, ['default'])
    AppEventsLogger.logEvent('package_order', null, ['default'])
    AppEventsLogger.logPurchase(total, 'IDR')
    this.props.setOrderTotal(SalesOrderService.setOrderTotal(totals))
    setTimeout(
      () =>
        this.setState(
          {
            showLoading: false
          },
          this.createOrderData
        ),
      1500
    )

  }

  applyCoupon() {
    const { coupon } = this.state
    const { cart, loading, emptyUserCart, addToCart, token } = this.props
    if (coupon !== '') {
      SalesOrderService.applyCoupon(
        cart,
        coupon,
        loading,
        emptyUserCart,
        addToCart,
        token
      )
    }
  }

  onCouponChange(coupon) {
    if (coupon === '') {
      this.props.clearCoupon()
    }
    this.setState({
      coupon,
    })
  }

  creditCard3DSValidationSuccess(authData) {
    const { user, order } = this.props
    const { newCreditCardInfo } = order
    const { saveCreditCard } = newCreditCardInfo
    const { creditCardToken } = this.state
    const ends_with = newCreditCardInfo.hasOwnProperty('card_number')
      ? newCreditCardInfo.card_number.substring(12, 16)
      : newCreditCardInfo.ends_with
    const newPaymentInfo = {
      type: newCreditCardInfo.type,
      ends_with,
      status: authData.status,
      token: creditCardToken.id,
      authorization_id: newCreditCardInfo.is_multiple_use
        ? authData.id
        : creditCardToken.authentication_id,
    }
    this.props.setPaymentInfo(newPaymentInfo)
    this.props.setOrderTotal(SalesOrderService.setOrderTotal(this.props.cart.totals))
    if (saveCreditCard) {
      const cardData = {
        account_id: user.id,
        token: creditCardToken.id,
        authorization_id: authData.id,
        type: newCreditCardInfo.type,
        ends_with,
      }
      this.props.addCard(cardData)
    }
    setTimeout(() => this.createOrderData(newCreditCardInfo.is_multiple_use), 1500)
  }

  editOrderHandler = () => {
    this.props.resetError()
    this.setState({
      showCartAlert: false
    }, () => {
      if (this.props.cart.type == 'package') {
        this.props.navigation.navigate('ThemeCalendarScreen')
      } else {
        this.props.navigation.navigate('SingleOrderCheckoutScreen', { isFromPayment: true })
      }
    })
  }

  changeAddressHandler = () => {
    this.props.resetError()
    this.setState({
      showCartAlert: false
    }, () => {
      SalesOrderService.getDeliveryAddress(
        this.props.token,
        this.setShowLoading,
        this.props.navigation,
        false,
        false,
        false,
      )
    })

  }

  setShowLoading = async (status) => {
    this.setState({
      showLoading: status
    })
  }

  createOrderData(isMultipleUse = true) {
    const { cart, order } = this.props
    const { selectedPayment } = order
    const salesOrder = SalesOrderService.buildSalesOrder(
      cart,
      selectedPayment,
      order.paymentInfo,
      isMultipleUse,
      order.orderTotal,
    )
    this.props.createOrder(salesOrder)
  }

  webViewDismiss = () => {
    this.setState({
      showModal: true,
      modalType: 'thank-you',
      showWebView: false,
    })
  }

  _navigateScreenTo() {
    this.setState({ showModal: false })
    this.props.resetOrder()
    this.props.emptyUserCart()
    this.props.navigation.dispatch(StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'HomeScreen'
        })
      ]
    }))
  }

  closeModal(createOrderSuccess) {
    if (!createOrderSuccess) {
      this.props.resetError()
      this.navigationGoBack()
      return this.setState({ showModal: false })
    }

    if (createOrderSuccess) {
      this._navigateScreenTo()
    } else {
      this.setState({
        showModal: false,
        errorMessage: '',
      }, this.props.clearOrderSuccess)
    }
    this.setState({ disableProceedButton: false })
  }

  renderPromotionCodeSection() {
    const { coupon } = this.state
    const { cart } = this.props
    const { promotion } = cart
    const promotionCodeInputBorderColor = promotion && !promotion.valid
      ? 'red'
      : promotion && promotion.valid
        ? Colors.grennBlue
        : Colors.pinkishGrey
    const promotionCodeInputIconColor = promotion && !promotion.valid
      ? 'red'
      : promotion && promotion.valid
        ? Colors.grennBlue
        : 'white'
    const promotionCodeInputIconName = promotion && !promotion.valid
      ? 'close'
      : 'check'
    let promotionCodeMessage = promotion && !promotion.valid
      ? 'Invalid promo code'
      : 'Promo code applied'
    if (promotion && promotion.message) {
      promotionCodeMessage = promotion.message
    }
    return (
      <View>
        <View style={styles.couponSectionContainer}>
          <View style={[
            styles.couponTextInputBox,
            { borderBottomColor: promotionCodeInputBorderColor },
          ]}>
            <TextInput
              testID='inputPromo'
              accessibilityLabel='inputPromo'
              placeholder='Promo or Discount Code'
              underlineColorAndroid='transparent'
              onChangeText={(coupon) => this.onCouponChange(coupon)}
              value={coupon}
              style={styles.couponTextInput}
            />
            <FontAwesome
              name={promotionCodeInputIconName}
              size={14}
              color={promotionCodeInputIconColor}
              style={styles.couponIcon}
            />
          </View>
          <TouchableOpacity testID='apply' accessibilityLabel='apply' onPress={() => this.applyCoupon()} style={styles.couponButton}>
            <Text style={styles.couponButtonText}>Apply</Text>
          </TouchableOpacity>
        </View>
        <Text style={[
          styles.couponMessageText,
          { color: promotionCodeInputIconColor },
        ]}>{promotionCodeMessage}</Text>
      </View>

    )
  }

  renderDetailTransaction() {
    const { cart } = this.props
    const { delivery_fee, discount, subtotal, total } = cart.totals
    if (cart.cartItems.length !== 0) {
      const deliveryFeeColor = delivery_fee === 0
        ? Colors.green
        : null
      const deliveryFeeText = delivery_fee !== 0
        ? LocaleFormatter.numberToCurrency(delivery_fee)
        : 'Free'
      const discountColor = discount !== 0
        ? Colors.green
        : null
      const showDiscount = discount > 0
      return (
        <View style={styles.detailBox}>
          {this.renderDetailTransactionRow('Items in Cart',
            LocaleFormatter.pluralTranslation(SalesOrderService.calculateItemInCart(cart), 'Item', 'Items'), null, null,
            null, null)}
          {this.renderDetailTransactionRow('Subtotal', LocaleFormatter.numberToCurrency(subtotal), null, null,
            null, null)}
          {this.renderDetailTransactionRow('Delivery Fee', deliveryFeeText, null, null, null, deliveryFeeColor)}
          {showDiscount &&
          this.renderDetailTransactionRow('Discount', `- ${LocaleFormatter.numberToCurrency(discount)}`, null,
            null, null, discountColor)
          }
          <View style={styles.detailBoxBorder} />
          {this.renderDetailTransactionRow('Total', LocaleFormatter.numberToCurrency(total), '500', '500',
            Colors.greyishBrownTwo, Colors.greyishBrownTwo)}
          {cart.promotion && cart.promotion.valid &&
          <View
            style={{
              flexDirection: 'row',
              marginBottom: scale(8),
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
              }}
            >
              <Ionicons
                name={'ios-information-circle'}
                size={scale(12)}
                color={Colors.brownishGrey}
              />
            </View>
            <View
              style={{
                flex: 20,
                alignItems: 'flex-end',
                justifyContent: 'flex-start',
              }}
            >
              <Text
                style={[
                  styles.detailText,
                  {
                    color: Colors.brownishGrey,
                    fontWeight: 'normal',
                    fontSize: scale(12),
                  },
                ]}
              >
                Delivery cancellation is not valid for discounted purchase.
              </Text>
            </View>
          </View>
          }
        </View>
      )
    } else {
      return (
        <View>
          <Text>Empty Cart</Text>
        </View>
      )
    }
  }

  renderDetailTransactionRow = (
    label: string,
    value,
    labelFontStyle: 'normal' | 'bold' | '100' | '200' | '300' | '400' | '500' | '600' | '700' | '800' | '900',
    valueFontStyle: 'normal' | 'bold' | '100' | '200' | '300' | '400' | '500' | '600' | '700' | '800' | '900',
    labelFontColor: string,
    valueFontColor: string,
  ) => {
    const textLabelFontStyle = labelFontStyle !== null
      ? labelFontStyle
      : 'normal'
    const textValueFontStyle = valueFontStyle !== null
      ? valueFontStyle
      : 'normal'
    const textLabelFontColor = labelFontColor !== null
      ? labelFontColor
      : Colors.brownishGrey
    const textValueFontColor = valueFontColor !== null
      ? valueFontColor
      : Colors.brownishGrey

    return (
      <View style={{
        flexDirection: 'row',
        marginBottom: verticalScale(8),
      }}>
        <View style={{
          flex: 1,
          alignItems: 'flex-start',
        }}>
          <Text
            style={{
              ...styles.detailText,
              ...styles.detailTextLabel,
              color: textLabelFontColor,
              fontWeight: textLabelFontStyle,
            }}
          >
            {label}
          </Text>
        </View>
        <View style={{
          flex: 1,
          alignItems: 'flex-end',
        }}>
          <Text style={[
            styles.detailText,
            {
              color: textValueFontColor,
              fontWeight: textValueFontStyle,
            },
          ]}>{value}</Text>
        </View>
      </View>
    )
  }

  renderPlaceYourOrderButton() {
    const { loading } = this.props.order
    const { fetchingWallet } = this.props
    const { isValidForOrder, disableProceedButton } = this.state
    const disableButton = (!isValidForOrder || disableProceedButton || fetchingWallet)

    const buttonText = loading
      ? 'Placing Your Order'
      : 'Place Your Order'
    return (
      !this.state.showCartAlert &&
      <Footer style={{
        backgroundColor: disableButton
          ? Colors.disable
          : Colors.primaryOrange,
      }}>
        <TouchableOpacity
          testID='placeOrder'
          accessibilityLabel='placeOrder'
          disabled={disableButton}
          onPress={() => this.processPaymentTransaction()}
          style={{ flex: 1 }}
        >
          <View
            style={{
              ...styles.placeYourOrderButton,
              backgroundColor: disableButton
                ? Colors.disable
                : Colors.primaryOrange,
            }}
          >
            <Text style={styles.placeYourOrderText}>{buttonText}</Text>
            <YummyboxIcon name='basket' color='white' size={scale(30)} />
          </View>
        </TouchableOpacity>
      </Footer>
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  navigateToTopUpScreen = () => {
    this.props.navigation.navigate('TopUpScreen', { navigationParams: 'back' })
  }

  onLinkedAppNextBtn = () => {
    this.setState(
      {
        showLoading: true
      },
      this.proceedToCreatingOrderData
    )
  }

  _checkDeepLink = () => {
    const { selectedPayment } = this.props.order

    const onCheckParam: LinkingFromOtherAppProps = {
      onFailure: () => {
        this.setState({
          showLoading: false,
          isLinkedPaymentSuccess: false,
          isLinkedPaymentAlert: true,
          showWebView: false
        })
      },
      onSuccess: () => {
        this.setState({
          showLoading: false,
          showModal: this.props.order.createOrderSuccess,
          modalType: 'thank-you'
        })
      }
    }

    const _handleAppStateChange = (nextAppState: AppStateStatus) => {
      if (
        this.state.appState.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        this.setState({
          showLoading: false,
          isLinkedPaymentSuccess: false,
          isLinkedPaymentAlert: true,
          showWebView: false
        })
      }
      this.setState({ appState: nextAppState })
    }



    const onMounted = () => {
      _linkingFromOtherApp(onCheckParam).mountData()
       isPaymentDeepLinked(selectedPayment) && RN_AppState.addEventListener('change', _handleAppStateChange)
    }

    const onUnmounted = () => {
      _linkingFromOtherApp(onCheckParam).unmountData()
       isPaymentDeepLinked(selectedPayment) && RN_AppState.removeEventListener('change', _handleAppStateChange)
    }

    return {
      onMounted,
      onUnmounted
    }
  };

  render() {
    const {
      showLoading,
      showModal,
      modalType,
      showWebView,
      virtualAccountCutOffTime,
      errorMessage,
      showAlertConfirmLinkedToOtherApp,
      isLinkedPaymentSuccess,
      isLinkedPaymentAlert
    } = this.state
    const { loading, loadingText, selectedPayment } = this.props.order

    const { createOrderSuccess } = this.props.order

    return (
      <Container testID='SingleOrderCheckoutPaymentScreen' style={{ flex: 1 }}>
        <NavigationBar leftSide='back' title='Payment' leftSideNavigation={this.navigationGoBack} />
        <ScrollView>
          <Content
            contentContainerStyle={{
              marginHorizontal: scale(20),
              padding: 0,
            }}
            keyboardShouldPersistTaps='handled'
            ref='Content'
          >
            {this.renderPromotionCodeSection()}
            {this.renderDetailTransaction()}
            <PaymentMethodPicker
              navigateToTopUpScreen={this.navigateToTopUpScreen}
              type='checkout'
              wallet={this.props.wallet}
              total={this.props.cart.totals.total}
              setting={this.props.setting}
              order={this.props.order}
              customerCard={this.props.customerCard}
              setSelectedPayment={this.props.setSelectedPayment}
              setNewCreditCardInfo={this.props.setNewCreditCardInfo}
              setPaymentInfo={this.props.setPaymentInfo}
              cartType={this.props.cart.type}
              cartItems={this.props.cart.cartItems}
              itemCutoff={this.props.cart.cutoff}
              itemCutDay={this.props.cart.cutDay}
              fetchingWallet={this.props.fetchingWallet}
            />
            <Spinner
              visible={showLoading}
            />
            <Spinner
              visible={loading}
              textContent={loadingText}
              textStyle={{
                color: 'white',
                fontFamily: 'Rubik-Regular',
                fontSize: scale(14),
              }}
            />
            <SalesOrderResultModal
              visible={showModal}
              modalType={modalType}
              invoiceUrl={this.props.order.invoiceUrl}
              onPress={() => this.closeModal(createOrderSuccess)}
              text={errorMessage}
            />

            {this.state.authenticationUrl !== null && (
              <WebView
                visible={showWebView}
                dismiss={() => this.setState({ showWebView: false })}
                url={this.state.authenticationUrl}
                validationSuccess={(authData) => this.creditCard3DSValidationSuccess(authData)}
              />
            )}
            {this.state.checkoutUrl !== null && (
              <WebView
                visible={showWebView}
                url={this.state.checkoutUrl}
                dismiss={this.webViewDismiss}
                canGoBack
              />
            )}
          </Content>
        </ScrollView>
        <AlertBox
          primaryAction={() => this.props.navigation.goBack()}
          primaryActionText='Ok'
          dismiss={() => this.setState({ showAlert: false })}
          text={`Bank Transfer payment method is not available after ${virtualAccountCutOffTime.utcOffset(7)
            .format('HH:mm')}`}
          visible={this.state.showAlert}
        />
        <AlertBox
          primaryAction={() => this.props.navigation.goBack()}
          primaryActionText='OK'
          dismiss={() => this.setState({ showFailedToCreateOrderModalWithCutOffTime: false })}
          text={`Sorry but we cannot place an order for next day delivery after ${this.state.orderCutOffTime.format(
            'HH:mm')}`}
          visible={this.state.showFailedToCreateOrderModalWithCutOffTime}
        />
        <AlertConfirmLinkedToOtherApp
          showAlert={showAlertConfirmLinkedToOtherApp}
          dismiss={() => this.setState({ showAlertConfirmLinkedToOtherApp: false, disableProceedButton: false })}
          paymentMethod={selectedPayment}
          onNextBtn={this.onLinkedAppNextBtn}
        />
        <AlertPaymentDeepLink
          showAlert={isLinkedPaymentAlert}
          isSuccess={isLinkedPaymentSuccess}
          dismiss={() => {
            this._navigateScreenTo()
            this.setState({ isLinkedPaymentAlert: false, disableProceedButton: false })}
          }
        />
        {this.renderPlaceYourOrderButton()}
        <AlertBox
          primaryAction={this.editOrderHandler}
          primaryActionText='Edit My Order'
          secondaryAction={this.changeAddressHandler}
          secondaryActionText='Change Location'
          dismiss={() => this.setState({ showCartAlert: false })}
          title='Product Availability Change'
          text={this.state.cartAlertText}
          visible={this.state.showCartAlert}
        />
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    cart: state.cart,
    customerCard: state.customerCard,
    user: state.login.user,
    token: state.login.token,
    order: state.order,
    isConnected: state.network.isConnected,
    setting: state.setting.data,
    wallet: state.customerAccount.wallet,
    fetchingWallet: state.customerAccount.fetchingWallet,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    emptyUserCart: () => dispatch(CartActions.resetCart()),
    addCard: (card) => dispatch(CustomerCardActions.addCard(card)),
    createOrder: (order) => dispatch(OrderActions.createOrder(order)),
    setPaymentInfo: (paymentInfo) => dispatch(OrderActions.setPaymentInfo(paymentInfo)),
    clearPaymentInfo: () => dispatch(OrderActions.clearPaymentInfo()),
    setOrderTotal: (orderTotal) => dispatch(OrderActions.setOrderTotal(orderTotal)),
    resetOrder: () => dispatch(OrderActions.resetOrder()),
    clearOrderSuccess: () => dispatch(OrderActions.clearOrderSuccess()),
    clearCoupon: () => dispatch(CartActions.clearCoupon()),
    setNewCreditCardInfo: (newCreditCardInfo) => dispatch(OrderActions.setNewCreditCardInfo(newCreditCardInfo)),
    setSelectedPayment: (selectedPayment) => dispatch(OrderActions.setSelectedPayment(selectedPayment)),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
    resetError: () => dispatch(OrderActions.resetError()),
    loading: (status: boolean) => dispatch(CartActions.loading(status)),
    addToCart: (cart: responseData) => dispatch(CartActions.addToCart(cart)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleOrderCheckoutPaymentScreen)
