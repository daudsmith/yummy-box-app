import * as React from 'react'
import { Keyboard, Text, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import {
  SwitchActions,
} from 'react-navigation'
import { connect } from 'react-redux'
import {withNavigation} from 'react-navigation'
import AuthBackgroundView from '../Components/Authentication/AuthBackgroundView'
import FlexButton from '../Components/Common/FlexButton'
import TextInput from '../Components/Common/TextInput'
import LoginActions, { LoginState } from '../Redux/LoginRedux'
import RegistrationActions, {
  CustomerForm,
  initStateRegister,
} from '../Redux/RegisterRedux'
import LocaleFormatter from '../Services/LocaleFormatter'
import { Colors } from '../Themes'
import Styles from './Styles/EmailPhoneVerificationScreenStyle'
import { AppState } from '../Redux/CreateStore'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import BoxBottom from '../Components/BoxBottom'
import SocialLogin from './SocialLogin'

export interface EmailPhoneProps {
  navigation: NavigationDrawerProp,
  registration: initStateRegister,
  login: LoginState,
  isConnected: boolean,
  validateIdentity: (identity) => void,
  validateRegisteredWithFacebook: (customer: CustomerForm) => void,
  loginAttempt: (identity: string, password: string) => void,
}

export interface EmailPhoneState {
  customer: customerInfoInterface,
  emailOrMobileNumber: string,
  showInvalidIdentityMessage: boolean,
  isEmailOrMobileNumber: string,
  showPhoneVerificationModal: boolean,
  validatingIdentity: boolean,
  identityExist: string,
  validatingRegisterWithFacebook: boolean,
  customerRegisteredWithFacebook: string,
  spinnerVisible: boolean,
  errorMessage: string,
  socialLoginType?: string,

}

export interface customerInfoInterface {
  phone: string,
  email: string,
  password: string,
  registrationType: boolean,
}

class EmailPhoneVerificationScreen extends React.Component<EmailPhoneProps, EmailPhoneState> {
  constructor (props) {
    super(props)
    this.state = {
      customer: null,
      emailOrMobileNumber: '',
      showInvalidIdentityMessage: false,
      isEmailOrMobileNumber: '',
      showPhoneVerificationModal: false,
      validatingIdentity: false,
      identityExist: null,
      validatingRegisterWithFacebook: false,
      customerRegisteredWithFacebook: null,
      spinnerVisible: false,
      errorMessage: null
    }
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    const {identityExist, validatingIdentity, validatingRegisterWithFacebook, customerRegisteredWithFacebook} = newProps.registration
    const currentValidating = this.state.validatingIdentity
    const currentValidatingRegisterWithFacebook = this.state.validatingRegisterWithFacebook

    this.setState({validatingIdentity, identityExist, validatingRegisterWithFacebook, customerRegisteredWithFacebook})

    if (currentValidating && !validatingIdentity) {
      if (identityExist) {
        this.setErrorMessage(true, '')
      } else {
        this.setErrorMessage(true, null)
        this.navigateToRegistrationScreen()
      }
    }

    if (currentValidatingRegisterWithFacebook && !validatingRegisterWithFacebook) {
      if (customerRegisteredWithFacebook) {
        const {email, password} = this.state.customer
        this.props.loginAttempt(email, password)
      } else {
        this.toggleSpinner()
        this.setState({socialLoginType: 'facebook', showPhoneVerificationModal: true})
      }
    }

    if (newProps.login.token !== null) {
      this.toggleSpinner()
      this.setState({showPhoneVerificationModal: false})
      this.navigateToHomeScreen()
    }
  }

  setErrorMessage (isIdentityMessage, error) {
    if (error === null) {
      this.setState({errorMessage: error})
    } else {
      if (isIdentityMessage) {
        const {emailOrMobileNumber} = this.state
        const isPhone = LocaleFormatter.isPhoneFormatValid(emailOrMobileNumber)

        if (isPhone) {
          this.setState({errorMessage: 'This phone number is already registered!'})
        } else {
          this.setState({errorMessage: 'This email is already registered!'})
        }
      } else {
        this.setState({errorMessage: error})
      }
    }
  }

  toggleSpinner () {
    this.setState({
      spinnerVisible: !this.state.spinnerVisible
    })
  }

  navigateToHomeScreen () {
    this.props.navigation.dispatch(
      SwitchActions.jumpTo({
        routeName: 'NavigationDrawer'
      })
    )
  }

  navigateToRegistrationScreen = () => {
    const {isEmailOrMobileNumber, emailOrMobileNumber} = this.state
    const email = isEmailOrMobileNumber === 'email' ? emailOrMobileNumber : ''
    const phone = isEmailOrMobileNumber === 'number' ? this.addPhoneNumberCountryCode(emailOrMobileNumber) : '+62'
    const customerInfo = {
      phone: phone,
      email: email,
      registrationType: isEmailOrMobileNumber,
    }

    this.props.navigation.navigate({
      routeName: 'RegistrationScreen',
      params: customerInfo,
    })
  }

  handleSubmitButton () {
    const {emailOrMobileNumber} = this.state
    this.setErrorMessage(false, null)
    Keyboard.dismiss()

    if (emailOrMobileNumber !== '') {
      const isEmail = LocaleFormatter.isEmailFormatValid(emailOrMobileNumber)
      const isPhone = LocaleFormatter.isPhoneFormatValid(emailOrMobileNumber)

      const identity = isEmail ? emailOrMobileNumber : isPhone ? this.addPhoneNumberCountryCode(emailOrMobileNumber) : null
      const isEmailOrMobileNumber = isEmail ? 'email' : isPhone ? 'number' : ''
      this.setState({isEmailOrMobileNumber})

      if (identity !== null) {
        this.props.validateIdentity(identity)
      } else {
        this.setErrorMessage(false, 'Invalid email or phone number format')
      }
    }
  }

  costumerDataFb (result, credentials) {
    return {
      salutation: '',
      first_name: result.first_name,
      last_name: result.last_name,
      email: result.email,
      password: result.id,
      confirmPassword: result.id,
      facebookCredentials: JSON.stringify(credentials),
      isRegisterWithFacebook: true,
      phone: null,
      facebook_id: result.id
    }
  }

  addPhoneNumberCountryCode (phone) {
    const containCountryCode = phone.slice(0, 1) === '+'
    if (containCountryCode) {
      return phone
    }

    return '+62' + phone.slice(1, phone.length)
  }

  renderErrorMessage () {
    const {errorMessage} = this.state
    const color = errorMessage !== null ? {} : {color: 'white'}
    return (
      <View>
        <Text style={[Styles.errorMessage, color]}>{errorMessage}</Text>
      </View>
    )
  }

  renderRegistrationBox () {
    const {emailOrMobileNumber, validatingIdentity} = this.state
    const {navigation} = this.props
    return (
      <View>
        <TextInput
          text='Email or Mobile Number'
          keyboardType='email-address'
          value={emailOrMobileNumber}
          autoCapitalize='none'
          onChangeText={(text) => this.setState({emailOrMobileNumber: text, showInvalidIdentityMessage: false})}
          onSubmitEditing={() => this.handleSubmitButton()}
          testID="emailMobileNumberRegist"
          accessibilityLabel="emailMobileNumberRegist"
        />
        {this.renderErrorMessage()}
        <FlexButton
          testID="submit"
          accessibilityLabel="submit"
          text={validatingIdentity
            ? ''
            : 'Submit'}
          loading={validatingIdentity && true}
          buttonStyle={{backgroundColor: emailOrMobileNumber || validatingIdentity ? Colors.primaryOrange : Colors.primaryGrey, marginTop: 15}}
          textStyle={{color: 'white'}}
          onPress={() => this.handleSubmitButton()}
          disableShadow
          />
        <View style={Styles.altContainer}>
          <Text style={Styles.textAlt}>or register with</Text>
        </View>
        <SocialLogin navigation={navigation}/>
      </View>
    )
  }

  render () {
    const {spinnerVisible} = this.state
    const {navigation} = this.props
    return (
      <AuthBackgroundView
        boxTitle='REGISTER'
        navigation={this.props.navigation}
        boxBottomComponent={(
          <BoxBottom
            navigation={navigation}
            primaryText="Have an account?"
            secondText="Login"
            to="Login"
          />
        )}
      >
        <Spinner visible={spinnerVisible} textStyle={{color: '#FFF'}} />
        {this.renderRegistrationBox()}
      </AuthBackgroundView>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    registration: state.register,
    login: state.login,
    isConnected: state.network.isConnected
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    validateIdentity: (identity) => dispatch(RegistrationActions.validateIdentity(identity)),
    validateRegisteredWithFacebook: (customer) => dispatch(RegistrationActions.validateRegisteredWithFacebook(customer)),
    loginAttempt: (identity, password) => dispatch(LoginActions.loginAttempt(identity, password))
  }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(EmailPhoneVerificationScreen))
