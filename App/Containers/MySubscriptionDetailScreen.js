import { ActionSheet, Root } from 'native-base'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ActivityIndicator, Linking, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import Modal from 'react-native-modal'
import { connect } from 'react-redux'
import AddressForm from '../Components/AddressForm'
import AlertBox from '../Components/AlertBox'
import DeliveryTimePicker from '../Components/DeliveryTimePicker'
import MealListSlider from '../Components/MealListSlider'
import NavigationBar from '../Components/NavigationBar'
import StaticMealDetailPopUp from '../Components/StaticMealDetailPopUp'
import SubscriptionProfile from '../Components/SubscriptionProfile'
import withSafeAreaView from '../Components/Common/WithSafeAreaView'
import CustomerOrderActions from '../Redux/CustomerOrderRedux'
import Colors from '../Themes/Colors'
import ContactUs from './ContactUs'
import MealDetailPopUpActions from '../Redux/MealDetailPopUpRedux'

class MySubscriptionDetailScreen extends Component {
  static propTypes = {
    fetchingOrderDetail: PropTypes.bool.isRequired,
    orderDetail: PropTypes.object,
    stoppingSubscription: PropTypes.bool.isRequired,
    stopSubscriptionSuccess: PropTypes.bool,
    isConnected: PropTypes.bool.isRequired,
    mealDetailPopUp: PropTypes.object.isRequired,
    fetchOrderDetail: PropTypes.func.isRequired,
    clearOrderDetail: PropTypes.func.isRequired,
    setModifiedOrderDetail: PropTypes.func.isRequired,
    stopSubscription: PropTypes.func.isRequired,
    clearSuccessState: PropTypes.func.isRequired,
    fetchMealDetail: PropTypes.func.isRequired,
    resetMealDetailPopUp: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this.state = {
      loading: true,
      showContactUsModal: false,
      showStopSubscriptionModal: false,
      showMealDetailModal: false,
      selectedMealId: null,
      selectedMealDate: null
    }
  }
  componentDidMount () {
    if (this.props.isConnected) {
      const {order_id, type} = this.props.navigation.state.params
      this.props.fetchOrderDetail(order_id, type)
    }
  }
  UNSAFE_componentWillReceiveProps (newProps) {
    this.setState({loading: newProps.fetchingOrderDetail})
    const {stopSubscriptionSuccess} = newProps
    if (stopSubscriptionSuccess) {
      this.props.navigation.goBack()
      this.props.clearSuccessState()
    }
  }
  componentWillUnmount () {
    ActionSheet['actionsheetInstance'] = null
    this.props.clearOrderDetail()
  }
  getMealList (schedules) {
    return schedules.map((schedule) => {
      if (typeof schedule.item.data !== 'undefined') {
        return {
          date: schedule.date,
          ...schedule.item.data.detail.data
        }
      } else {
        return {
          date: schedule.date,
          id: null
        }
      }
    })
  }
  selectMeal = (mealId, date) => {
    this.setState({
      showMealDetailModal: true,
      selectedMealId: mealId,
      selectedMealDate: date
    }, () => {
      this.props.fetchMealDetail(mealId, date)
    })
  }

  closeModal = () => {
    this.setState({
      showMealDetailModal: false,
      selectedMealId: null,
      selectedMealDate: null
    }, this.props.resetMealDetailPopUp)
  }
  showActionSheet () {
    const mainButtons = ['FAQ', 'Contact', 'Close']
    const activeSubscriptionButtons = ['Modify', 'Stop Subscription']
    const buttons = this.props.orderDetail.subscription.status === 'ACTIVE' ? [...activeSubscriptionButtons, ...mainButtons] : mainButtons
    ActionSheet.show({
      options: buttons
    },
      buttonIndex => {
        switch (buttons[buttonIndex]) {
          case 'Modify':
            this.props.setModifiedOrderDetail()
            this.props.navigation.navigate('SubscriptionOrderModifyScreen', this.props.navigation.state.params)
            break
          case 'Stop Subscription':
            this.setState({showStopSubscriptionModal: true})
            break
          case 'FAQ':
            Linking.openURL('http://help.yummybox.id')
            break
          case 'Contact':
            this.setState({showContactUsModal: true})
            break
          default:
            break
        }
    })
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const {orderDetail} = this.props
    const {loading, showContactUsModal, showStopSubscriptionModal} = this.state
    const meals = orderDetail === null
      ? []
      : (orderDetail.schedules)
        && this.getMealList(orderDetail.schedules)

    return (
      <Root>
        <View style={{flex: 1}}>
          <NavigationBar
            leftSide='back'
            title='My Subscription'
            leftSideNavigation={this.navigationGoBack}
          />
          <ScrollView contentContainerStyle={{flexGrow: 1}}>
            {loading ?
              (
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                  <ActivityIndicator color={Colors.warmGrey} size='large' />
                </View>
              ) :
                orderDetail && orderDetail.subscription && orderDetail.subscription.payment !== null ? (
                  <View>
                    <View style={{marginHorizontal: 20, paddingTop: 13}}>
                      <SubscriptionProfile
                        theme={orderDetail.subscription.theme}
                        startOn={orderDetail.subscription.start_date}
                        repeatEvery={orderDetail.subscription.repeat_every}
                        repeatInterval={orderDetail.subscription.repeat_interval}
                        paymentMethod={orderDetail.subscription.payment.method}
                      />
                    </View>
                    <View style={Styles.deliveryDetailContainer}>
                      <Text style={[Styles.title, {marginBottom: 20}]}>Delivery Details</Text>
                      <DeliveryTimePicker readOnly selectedTime={orderDetail.subscription_raw_data.default_delivery_time} />
                      <AddressForm
                        deliveryAddress={orderDetail.subscription_raw_data.default_delivery_address}
                        note={orderDetail.subscription_raw_data.default_delivery_note}
                        phone={orderDetail.subscription_raw_data.default_recipient_phone}
                        name={orderDetail.subscription_raw_data.default_recipient_name}
                        readOnly
                      />
                    </View>
                    <View style={Styles.mealListContainer}>
                      <View style={Styles.textContainer}>
                        <Text style={Styles.title}>Meal List</Text>
                        <Text style={Styles.mealListDescription}>Only show meals until 15 active days. Your next meals will be available soon.</Text>
                      </View>
                      <MealListSlider
                        meals={meals}
                        onPress={(mealId, date) => this.selectMeal(mealId, date)}
                        lastItemPadding={20}
                        containerStyle={{marginLeft: 20}}
                        theme='playlist'
                      />
                    </View>
                    <Root>
                      <TouchableWithoutFeedback testID='helpButton' accessibilityLabel='helpButton' onPress={() => this.showActionSheet()}>
                        <View style={Styles.needHelpButton}>
                          <Text style={Styles.needHelpText}>Need Help?</Text>
                        </View>
                      </TouchableWithoutFeedback>
                    </Root>
                  </View>
                ) : (
                  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text>
                      Detail not Found
                    </Text>
                  </View>
                )
            }
          </ScrollView>
          <Spinner visible={this.props.stoppingSubscription} />
          <Modal
            backdropColor='white'
            backdropOpacity={0.8}
            isVisible={showContactUsModal}
            onBackButtonPress={() => this.setState({showContactUsModal: false})}
            style={{margin: 0}}
          >
            <ContactUs
              dismissModal={() => this.setState({showContactUsModal: false})}
            />
          </Modal>
          <AlertBox
            primaryAction={() => {}}
            primaryActionText='No'
            secondaryAction={() => {
              this.props.stopSubscription(this.props.navigation.state.params.order_id)
            }}
            secondaryActionText='Yes'
            dismiss={() => this.setState({showStopSubscriptionModal: false})}
            text='Are you sure want to stop this subscription?'
            visible={showStopSubscriptionModal}
          />
          {this.props.orderDetail !== null &&
            <StaticMealDetailPopUp
              closeModal={this.closeModal}
              visible={this.state.showMealDetailModal}
              mealId={this.state.selectedMealId}
              date={this.state.selectedMealDate}
              mealDetailPopUp={this.props.mealDetailPopUp}
            />
          }

        </View>
      </Root>
    )
  }
}

const Styles = StyleSheet.create({
  deliveryDetailContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    marginTop: 13,
    borderTopColor: Colors.pinkishGrey,
    paddingHorizontal: 20,
    paddingTop: 19
  },
  title: {
    fontFamily: 'Rubik-Regular',
    fontSize: 18,
    color: Colors.warmGrey,
  },
  textContainer: {
    paddingHorizontal: 20,
    marginBottom: 20
  },
  mealListContainer: {
    paddingTop: 18,
    paddingBottom: 25,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: Colors.pinkishGrey,
    marginTop: 25,
  },
  mealListDescription: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    lineHeight: 18,
    color: Colors.greyishBrownThree,
    marginTop: 9
  },
  needHelpButton: {
    height: 55,
    backgroundColor: Colors.bloodOrange,
    alignItems: 'center',
    justifyContent: 'center'
  },
  needHelpText: {
    fontFamily: 'Rubik-Regular',
    color: 'white',
    fontSize: 16
  }
})

const mapStateToProps = state => {
  return {
    fetchingOrderDetail: state.customerOrder.fetchingOrderDetail,
    orderDetail: state.customerOrder.orderDetail,
    stoppingSubscription: state.customerOrder.stopping,
    stopSubscriptionSuccess: state.customerOrder.stopSubscriptionSuccess,
    isConnected: state.network.isConnected,
    mealDetailPopUp: state.mealDetailPopUp,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOrderDetail: (id, orderType) => dispatch(CustomerOrderActions.fetchOrderDetail(id, orderType)),
    clearOrderDetail: () => dispatch(CustomerOrderActions.clearOrderDetail()),
    setModifiedOrderDetail: () => dispatch(CustomerOrderActions.setModifiedOrderDetail()),
    stopSubscription: (id) => dispatch(CustomerOrderActions.stopSubscription(id)),
    clearSuccessState: () => dispatch(CustomerOrderActions.clearSuccessState()),
    fetchMealDetail: (mealId, date) => dispatch(MealDetailPopUpActions.fetchMealDetail(mealId, date)),
    resetMealDetailPopUp: () => dispatch(MealDetailPopUpActions.resetMealDetailPopUp()),
  }
}

export default withSafeAreaView(connect(mapStateToProps, mapDispatchToProps)(MySubscriptionDetailScreen))
