import _ from 'lodash'
import Moment from 'moment'
import { Container, Content, Footer } from 'native-base'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, Text, TouchableWithoutFeedback, View } from 'react-native'
import { Calendar } from 'react-native-calendars'
import { connect } from 'react-redux'
import AlertBox from '../Components/AlertBox'
import MealListSlider from '../Components/MealListSlider'
import NavigationBar from '../Components/NavigationBar'
import StaticMealDetailPopUp from '../Components/StaticMealDetailPopUp'
import MyMealPlanActions from '../Redux/MyMealPlanRedux'
import ThemeActions from '../Redux/ThemeRedux'
import Colors from '../Themes/Colors'
import Styles from './Styles/MyMealPlanModifyCalendarScreen'
import MealDetailPopUpActions from '../Redux/MealDetailPopUpRedux'

class MyMealPlanModifyCalendarScreen extends Component {
  static propTypes = {
    myMealPlan: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    isConnected: PropTypes.bool.isRequired,
    themeAvailableMeals: PropTypes.array.isRequired,
    mealDetailPopUp: PropTypes.object.isRequired,
    fetchDetailItems: PropTypes.func.isRequired,
    modifyMmpPackageDeliveryMeal: PropTypes.func.isRequired,
    setModifyType: PropTypes.func.isRequired,
    fetchMealDetail: PropTypes.func.isRequired,
    resetMealDetailPopUp: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    const {myMealPlanPackage} = this.props.myMealPlan.modifiedMyMealPlanDetail
    const availableDates = this.props.theme.initialAvailableDateSelectedTheme

    this.state = {
      title: myMealPlanPackage.theme_name,
      theme_id: myMealPlanPackage.theme_id,
      currentSelectedMealDate: this.props.myMealPlan.modifiedMyMealPlanDetail.data.date,
      selectedMealDate: this.props.myMealPlan.modifiedMyMealPlanDetail.data.date,
      currentPackage: myMealPlanPackage,
      availableDates,
      selectedDates: myMealPlanPackage.dates,
      showInvalidDateSelectionModal: false,
      themeMeals: this.props.theme.meals,
      showMealDetailModal: false,
      shownMealDetailDate: null,
      shownMealDetailId: null,
      showConfirmationModal: false,
      themeAvailableMeals: []
    }
  }

  componentDidMount () {
    if (this.props.isConnected) {
      const {theme_id} = this.state
      this.props.fetchDetailItems(theme_id)
    }
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    this.setState({
      themeAvailableMeals: newProps.themeAvailableMeals
    })
  }

  confirmChangeDeliveryDate () {
    const {selectedMealDate, currentSelectedMealDate} = this.state
    if (selectedMealDate !== currentSelectedMealDate) {
      this.props.setModifyType('skip')
      this.props.modifyMmpPackageDeliveryMeal(selectedMealDate, currentSelectedMealDate)
    }
    this.navigationGoBack()
  }

  markAvailableDates (availableDates) {
    let markAvailableDates = {}
    _.map(availableDates, (value) => {
      markAvailableDates[value] = {disabled: false, marked: false, selected: false}
    })

    return markAvailableDates
  }

  markSelectedDates (selectedDates) {
    const {selectedMealDate} = this.state
    let markSelectedDates = {}
    _.map(selectedDates, (value) => {
      if (!Moment(value).isSameOrBefore(Moment().utcOffset(7).format('YYYY-MM-DD'), 'day')) {
        markSelectedDates[value] = {disabled: false, marked: true, dotColor: Colors.bloodOrange}
      }
    })

    if (markSelectedDates.hasOwnProperty(selectedMealDate)) {
      markSelectedDates[selectedMealDate] = {...markSelectedDates[selectedMealDate], selected: true}
    } else {
      markSelectedDates[selectedMealDate] = {disabled: false, selected: true}
    }

    return markSelectedDates
  }

  selectDate (selectedDate) {
    const {selectedDates, currentSelectedMealDate, availableDates} = this.state
    const selectedDateIsNotAvailable = _.findIndex(selectedDates, (date) => {
      return date === selectedDate
    })
    const isSelectedDateOnAvailableDates = _.findIndex(availableDates, (date) => {
      return date === selectedDate
    })

    if (isSelectedDateOnAvailableDates === -1) {
      return
    }

    if (selectedDateIsNotAvailable > -1 && currentSelectedMealDate !== selectedDate) {
      this.setState({showInvalidDateSelectionModal: true})
    } else {
      this.setState({selectedMealDate: selectedDate})
    }
  }

  calendar () {
    const {selectedDates, availableDates} = this.state
    const markedDates = this.markSelectedDates(selectedDates)
    const formattedAvailableDates = this.markAvailableDates(availableDates)
    const markedAndAvailableDates = {...formattedAvailableDates, ...markedDates}
    return (
      <View style={Styles.calendarContainer}>
        <Calendar
          minDate={formattedAvailableDates[0]}
          maxDate={formattedAvailableDates[formattedAvailableDates.length - 1]}
          markedDates={markedAndAvailableDates}
          onDayPress={(date) => this.selectDate(date.dateString)}
          disabledByDefault
          theme={{
            arrowColor: Colors.green,
            monthTextColor: Colors.green,
            selectedDayTextColor: Colors.bloodOrange,
            todayTextColor: Colors.greyishBrownTwo,
            dayTextColor: Colors.greyishBrownTwo,
            textSectionTitleColor: Colors.greyishBrownTwo,
            textMonthFontFamily: 'Rubik-Regular',
            textDayHeaderFontFamily: 'Rubik-Regular',
            selectedDayBackgroundColor: Colors.white,
            selectedDotColor: Colors.bloodOrange
          }}
        />
        <View style={Styles.calendarDescriptionContainer}>
          <Text style={Styles.calendarDescriptionText}>
            Tap a date to add or remove it from My Meal Plan. You can also change your schedule later through My order
          </Text>
        </View>
      </View>
    )
  }

  selectMeal (mealId, date) {
    this.setState({
      showMealDetailModal: true,
      shownMealDetailId: mealId,
      shownMealDetailDate: date
    }, () => {
      this.props.fetchMealDetail(mealId, date)
    })
  }

  closeModal = () => {
    this.setState({
      showMealDetailModal: false
    }, () => {
      this.props.resetMealDetailPopUp()
    })
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const {title, showInvalidDateSelectionModal, themeMeals, showMealDetailModal, showConfirmationModal} = this.state
    const {
      mealDetailPopUp,
    } = this.props
    let nextAvailableDate = ''
    if (this.state.selectedMealDate !== null) {
      nextAvailableDate = Moment(this.state.selectedMealDate).format('ddd, DD MMM YYYY')
    }
    const {myMealPlanDetail} = this.props.myMealPlan
    let nextAvailableMeal = null
    this.state.themeAvailableMeals.map((meal) => {
      if (meal.date === this.state.selectedMealDate) {
        nextAvailableMeal = meal.item.name
      }
    })
    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title={title}
          leftSideNavigation={this.navigationGoBack}
        />
        <Content>
          <ScrollView contentContainerStyle={{flexGrow: 1}}>
            {this.calendar()}
            {themeMeals.length > 0 &&
            <View style={{marginTop: 10}}>
              <MealListSlider meals={themeMeals} onPress={(mealId, date) => this.selectMeal(mealId, date)} lastItemPadding={25} theme={'playlist'} />
            </View>
            }
          </ScrollView>
          <AlertBox
            primaryAction={() => {}}
            dismiss={() => this.setState({showInvalidDateSelectionModal: false})}
            primaryActionText='OK'
            text={'You\'re already have an order on this date, please choose another available date'}
            visible={showInvalidDateSelectionModal}
          />
          <AlertBox
            primaryAction={() => {}}
            primaryActionText='No'
            secondaryAction={() => {
              if (this.state.nextAvailableDate !== null && typeof myMealPlanDetail.data !== 'undefined') {
                this.confirmChangeDeliveryDate()
              }
            }}
            secondaryActionText='Yes'
            dismiss={() => this.setState({showConfirmationModal: false})}
            title={`Move Delivery to ${nextAvailableDate}?`}
            text={() => {
              return (
                <View>
                  <Text style={Styles.alertBoxText}>Your meal will be replaced to <Text style={Styles.alertBoxTextBold}>{nextAvailableMeal}</Text>. Only 1 (one) skip per delivery allowed</Text>
                </View>
              )
            }}
            visible={showConfirmationModal}
          />
          <StaticMealDetailPopUp
            closeModal={this.closeModal}
            visible={showMealDetailModal}
            mealDetailPopUp={mealDetailPopUp}
          />
        </Content>
        <Footer style={{backgroundColor: Colors.bloodOrange}}>
          <TouchableWithoutFeedback onPress={() => this.setState({showConfirmationModal: true})}>
            <View style={Styles.footerButton}>
              <Text style={Styles.footerButtonText}>Confirm</Text>
            </View>
          </TouchableWithoutFeedback>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    myMealPlan: state.myMealPlan,
    theme: state.theme,
    isConnected: state.network.isConnected,
    themeAvailableMeals: state.theme.availableMeals,
    mealDetailPopUp: state.mealDetailPopUp,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDetailItems: (id) => dispatch(ThemeActions.fetchDetailItems(id)),
    modifyMmpPackageDeliveryMeal: (selectedDate, currentDate) => dispatch(MyMealPlanActions.modifyMmpPackageDeliveryMeal(selectedDate, currentDate)),
    setModifyType: (modifyType) => dispatch(MyMealPlanActions.setModifyType(modifyType)),
    fetchMealDetail: (mealId, date) => dispatch(MealDetailPopUpActions.fetchMealDetail(mealId, date)),
    resetMealDetailPopUp: () => dispatch(MealDetailPopUpActions.resetMealDetailPopUp()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyMealPlanModifyCalendarScreen)
