import React from 'react'
import 'moment/locale/en-au'
import { Image, View } from 'react-native'
import { connect } from 'react-redux'
import AppConfig from '../Config/AppConfig'
import SettingActions, { SettingDataInterface } from '../Redux/SettingRedux'
import { Images } from '../Themes'
import styles from './Styles/LaunchScreenStyles'
import { AppState } from '../Redux/CreateStore'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { User } from '../Redux/LoginRedux'
import _ from 'lodash'
import { baseLastUsed } from '../Redux/V2/LastUsedRedux'

export interface LaunchScreenProps {
  navigation: NavigationDrawerProp,
  isConnected: boolean,
  setting: SettingDataInterface,
  user: User,
  lastUsed: baseLastUsed,
  skipWelcomeScreen: () => void,
  fetchSetting: (key: string[]) => void,
}

class LaunchScreen extends React.Component<LaunchScreenProps> {
  componentDidMount() {
    const { isConnected } = this.props
    if (isConnected) {
      this.fetchBulkSetting()
    }
  }

  componentDidUpdate(prevProps: Readonly<LaunchScreenProps>): void {
    const { setting, isConnected } = this.props

    if (setting.finishFetch && !prevProps.setting.finishFetch) {
      this.navigateScreen()
    }
    if (isConnected && !prevProps.isConnected) {
      this.fetchBulkSetting()
    }
  }

  fetchBulkSetting = () => {
    this.props.fetchSetting([
      'off_dates',
      'delivery_cut_off',
      'app_link_faq',
      'app_link_privacy_policy',
      'app_link_terms_and_conditions',
      'cs_email_address',
      'cs_phone_number',
      'sales_order_total_limit',
      'subscription_minimum_wallet_balance',
      'bank_transfer_payment_option_cutoff',
      'subscription_repeat_interval',
      'delivery_times',
      'virtual_account_banks',
      'subscription_artwork',
      'homescreen_selected_theme',
      'free_charge_policy',
      'free_delivery_policy',
      'cs_whatsapp_number',
      'featured_theme',
      'payment_methods'
    ])
  }

  navigateScreen = () => {
    const { navigation, user, setting, lastUsed } = this.props
    if (setting.finishFetch) {
      if (user) {
        navigation.navigate('NavigationDrawer')
      } else {
        if (_.isEmpty(lastUsed.code)) {
          navigation.navigate('IntroNavigation')
        }
        else{
          navigation.navigate('NavigationDrawer')
        }
      }
    }
  }

  render() {
    return (
      <View
        testID='LaunchScreen'
        style={{
          ...styles.mainContainer,
          ...styles.centered
        }}
      >
        {
          AppConfig.is_uat === '1'
            ? (
              <View>
                <Image
                  source={Images.YummyboxUAT}
                  style={styles.logo}
                  resizeMode='contain'
                />
              </View>
            )
            : (
              <View>
                <Image
                  source={Images.YummyboxBeta}
                  style={styles.logo}
                  resizeMode='contain'
                />
              </View>
            )
        }
      </View>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    isConnected: state.network.isConnected,
    setting: state.setting.data,
    user: state.login.user,
    lastUsed: state.lastUsed,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSetting: (key: string[]) => dispatch(SettingActions.fetchSetting(key)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
