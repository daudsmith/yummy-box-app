import _ from 'lodash'
import {
  Container,
  Content,
  Footer,
} from 'native-base'
import * as React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import { moderateScale } from 'react-native-size-matters'
import { connect } from 'react-redux'
import AlertBox from '../Components/AlertBox'
import NavigationBar from '../Components/NavigationBar'
import PaymentMethodPicker from '../Components/Payment/PaymentMethodPicker'
import SalesOrderResultModal, { SalesOrderModalType } from '../Components/SalesOrderResultModal'
import WebView from '../Components/WebView'
import YummyboxIcon from '../Components/YummyboxIcon'
import CustomerCardActions, {
  Card,
  InitCustomerCard,
} from '../Redux/CustomerCardRedux'
import MyMealPlanActions, {
  CreditCardInfo,
  MyMealPlanDetail,
  MyMealPlanDetailSuccess,
} from '../Redux/MyMealPlanRedux'
import OrderActions, { SalesOrderReduxInterface } from '../Redux/SalesOrderRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import LocaleFormatter from '../Services/LocaleFormatter'
import { buildNewCardData, buildNewCCPaymentInfo } from '../Services/PaymentInfoService'
import XenditService, { xenditCreditTokenResponse } from '../Services/XenditService'
import Colors from '../Themes/Colors'
import styles from './Styles/SingleOrderCheckoutPaymentScreenStyle'
import { ScrollView } from 'react-native-gesture-handler'
import ApiCatalog from '../Services/ApiCatalog'
import { AppState } from '../Redux/CreateStore'
import { User } from '../Redux/LoginRedux'
import { SettingDataInterface } from '../Redux/SettingRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { StackNavigationProp } from 'react-navigation-stack/lib/typescript/src/vendor/types'
import { Item } from '../Redux/CartRedux'

interface mmpPaymentProps {
  navigation: NavigationDrawerProp & StackNavigationProp,
  currentMMPDetail: MyMealPlanDetailSuccess,
  modifiedMMPDetail: MyMealPlanDetailSuccess,
  customerCard: InitCustomerCard,
  user: User,
  modifying: boolean,
  modifySuccess: boolean,
  modifyErrorMessage: string,
  modifyType: string,
  wallet: number,
  setting: SettingDataInterface,
  total: number,
  order: SalesOrderReduxInterface,
  fetchingWallet: boolean,
  updateModifiedMyMealPlanDetail: (updatedModifiedMyPlanDetail: MyMealPlanDetailSuccess) => void,
  updateMyMealPlanWithPayment: () => void,
  resetModifyState: () => void,
  addCard: (card: Card) => void,
  setModifyType: (modifyType: string) => void,
  setMmpNewCreditCardInfo: (newCreditCardInfo: CreditCardInfo) => void,
  modifyMmpSubscriptionSelectedPayment: (selectedPayment: string) => void,
  setSelectedPayment: (selectedPayment: string) => void,
  fetchWallet: () => void,
  fetchCards: () => void,
}

interface mmpPaymentStates {
  authenticationUrl?: string,
  showLoading: boolean,
  showLoader: boolean,
  showWebView: boolean,
  currentMMPDetail: MyMealPlanDetailSuccess,
  modifiedMMPDetail: MyMealPlanDetailSuccess,
  updatingDelivery: boolean,
  showModifyConfirmationModal: boolean,
  modifying: boolean,
  modifyText: string,
  scrollY: number,
  showErrorModal: boolean,
  modalType: SalesOrderModalType,
  showModalModify: boolean,
  disabledConfirm: boolean,
  creditCardToken: xenditCreditTokenResponse,
  showInvalidModal: boolean,
  failedItem?: Item,
  failedDate?: string,
  failedKitchenCode?: string,
}

class MyMealPlanModifyPaymentScreen extends React.Component<mmpPaymentProps, mmpPaymentStates> {
  constructor(props) {
    super(props)
    this.state = {
      authenticationUrl: null,
      showLoading: false,
      showLoader: false,
      showWebView: false,
      currentMMPDetail: this.props.currentMMPDetail,
      modifiedMMPDetail: this.props.modifiedMMPDetail,
      updatingDelivery: false,
      showModifyConfirmationModal: false,
      modifying: false,
      modifyText: '',
      scrollY: 0,
      showErrorModal: false,
      modalType: 'sorry',
      showModalModify: false,
      disabledConfirm: true,
      creditCardToken: null,
      showInvalidModal: false,
      failedItem: null,
      failedDate: null,
      failedKitchenCode: null,
    }
  }

  componentDidMount() {
    const {
      selectedPayment,
      payment_info,
    } = this.props.modifiedMMPDetail
    let cardId = payment_info.id
    if (payment_info.hasOwnProperty('payment_info_json') && payment_info.payment_info_json !== '') {
      const parsedPaymentInfoJSON = JSON.parse(this.props.modifiedMMPDetail.payment_info.payment_info_json)
      cardId = isNaN(parsedPaymentInfoJSON.id) ? null : parsedPaymentInfoJSON.id
    }

    if (cardId !== null && selectedPayment === 'credit-card') {
      this.setState({ disabledConfirm: false })
    }
    this.props.fetchWallet()
    this.props.fetchCards()
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (newProps.modifyType === 'payment') {
      this.setState({
        modifying: newProps.modifying,
        showLoading: newProps.modifying
      })
      if (newProps.modifySuccess && !newProps.modifying) {
        this.setState({
          showModalModify: true,
          modifyText: newProps.modifySuccess ? 'Your meal updated successfully' : '',
          modalType: newProps.modifySuccess ? 'thank-you' : 'sorry'
        })
      }
    }

    this.setState({
      currentMMPDetail: newProps.currentMMPDetail,
      modifiedMMPDetail: newProps.modifiedMMPDetail,
    })
  }
  creditCard3DSValidationSuccess(authData) {
    const { user, modifiedMMPDetail } = this.props
    const { newCreditCardInfo } = modifiedMMPDetail
    const { saveCreditCard } = newCreditCardInfo
    const { creditCardToken } = this.state

    const newPaymentInfo = buildNewCCPaymentInfo(creditCardToken, authData, newCreditCardInfo)
    this.setPaymentInfo(newPaymentInfo)
    if (saveCreditCard) {
      const cardData = buildNewCardData(user, creditCardToken, authData, newCreditCardInfo)
      this.props.addCard(cardData)
    }
    this.props.setModifyType('payment')
    this.props.updateMyMealPlanWithPayment()
  }
  renderDetailTransaction() {
    const { modifiedMMPDetail, currentMMPDetail } = this.state

    let subtotal = (modifiedMMPDetail.data as MyMealPlanDetail).subtotal - (currentMMPDetail.data as MyMealPlanDetail).subtotal
    const total = (modifiedMMPDetail.data as MyMealPlanDetail).subtotal - (currentMMPDetail.data as MyMealPlanDetail).subtotal

    return (
      <View style={styles.detailBox}>
        {this.renderDetailTransactionRow('Subtotal', LocaleFormatter.numberToCurrency(subtotal), null, null, null, null)}
        <View style={styles.detailBoxBorder} />
        {this.renderDetailTransactionRow('Total', LocaleFormatter.numberToCurrency(total), '500', '500', Colors.greyishBrownTwo, Colors.greyishBrownTwo)}
      </View>
    )
  }
  renderDetailTransactionRow(label, value, labelFontStyle, valueFontStyle, labelFontColor, valueFontColor) {
    const textLabelFontStyle = labelFontStyle !== null ? labelFontStyle : 'normal'
    const textValueFontStyle = valueFontStyle !== null ? valueFontStyle : 'normal'
    const textLabelFontColor = labelFontColor !== null ? labelFontColor : Colors.brownishGrey
    const textValueFontColor = valueFontColor !== null ? valueFontColor : Colors.brownishGrey

    return (
      <View style={{ flexDirection: 'row', marginBottom: moderateScale(8) }}>
        <View style={{ flex: 1, alignItems: 'flex-start' }}>
          <Text style={[styles.detailText, styles.detailTextLabel, { color: textLabelFontColor, fontWeight: textLabelFontStyle }]}>{label}</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'flex-end' }}>
          <Text style={[styles.detailText, { color: textValueFontColor, fontWeight: textValueFontStyle }]}>{value}</Text>
        </View>
      </View>
    )
  }
  renderPlaceYourOrderButton() {
    const buttonText = this.state.updatingDelivery ? 'Updating' : 'Confirm'
    return (
      <Footer
        style={{
          backgroundColor: this.state.disabledConfirm
            ? Colors.pinkishGrey
            : Colors.bloodOrange,
        }}
      >
        <TouchableOpacity
          testID='placeYourOrderButton'
          disabled={this.state.disabledConfirm}
          style={{
            ...styles.placeYourOrderButton,
            backgroundColor: this.state.disabledConfirm ? Colors.pinkishGrey : Colors.bloodOrange,
            flex: 1,
          }}
          onPress={() => this.setState({ showModifyConfirmationModal: true })}
        >
          <View style={{ flex: 5, justifyContent: 'center' }}>
            <Text style={styles.placeYourOrderText}>{buttonText}</Text>
          </View>

          <View style={styles.placeYourOrderIcon}>
            <YummyboxIcon name='basket' color='white' size={30} />
          </View>
        </TouchableOpacity>
      </Footer>
    )
  }

  processPaymentTransaction() {
    const {
      currentMMPDetail,
      modifiedMMPDetail,
    } = this.state
    const {
      newCreditCardInfo,
      selectedPayment,
    } = modifiedMMPDetail

    if (newCreditCardInfo !== null && selectedPayment === 'credit-card') {
      // didnt hit setPaymentInfo if not change payment method when default is CC
      // need to calculate total_amount_paid for authorize amount
      const total = (modifiedMMPDetail.data as MyMealPlanDetail).subtotal - (currentMMPDetail.data as MyMealPlanDetail).subtotal
      this.setState({ showLoader: true })
      XenditService.createToken(
        newCreditCardInfo,
        (authenticationUrl) => this.verification3DSCallBack(authenticationUrl),
        () => {
          this.setState(
            { showLoading: false },
            () => this.setState({ showErrorModal: true })
          )
        },
        (creditCardToken) => this.setState({ creditCardToken }),
        total
      )
    } else {
      this.props.setModifyType('payment')
      this.props.updateMyMealPlanWithPayment()
    }
  }

  modifyAction = async () => {
    const { modifiedMMPDetail } = this.state
    const { data } = modifiedMMPDetail
    const { items, delivery_date, kitchen_code } = (data as MyMealPlanDetail)
    const itemsData = items.data
    let validItem = false
    if (itemsData) {
      for (const item of itemsData) {
        const { detail, quantity } = item
        if (detail) {
          const { updated, id } = detail.data
          if (updated) {
            await ApiCatalog.create()
              .getMealAvailability(id, delivery_date, kitchen_code)
              .then((response) => response.data)
              .then((responseBody) => responseBody.data)
              .then((responseData) => {
                validItem = responseData.remaining_quantity - quantity >= 0
                if (!validItem) {
                  this.setState({
                    failedItem: item,
                    failedDate: delivery_date,
                    failedKitchenCode: kitchen_code,
                  })
                }
              })
          }
        }
      }
    }

    if (validItem) {
      this.processPaymentTransaction()
    } else {
      this.setState({
        showLoading: false,
        showInvalidModal: true,
      })
    }
  }

  verification3DSCallBack(authenticationUrl) {
    this.setState({ authenticationUrl }, () => this.setState({ showLoading: false }, () => this.setState({ showWebView: true })))
  }
  setPaymentInfo(paymentInfo) {
    const { currentMMPDetail, modifiedMMPDetail } = this.state
    const total = (modifiedMMPDetail.data as MyMealPlanDetail).subtotal - (currentMMPDetail.data as MyMealPlanDetail).subtotal
    let modifiedMealPlanPaymentInfo = modifiedMMPDetail.payment_info

    if (paymentInfo !== null) {
      const selectedPayment = typeof paymentInfo.authorization_id !== 'undefined' ? 'credit-card' : 'wallet'
      modifiedMealPlanPaymentInfo.method = selectedPayment
      modifiedMealPlanPaymentInfo.payment_info_json = JSON.stringify(paymentInfo)
      modifiedMealPlanPaymentInfo.is_multiple_use = !(selectedPayment === 'credit-card' && !paymentInfo.is_multiple_use)
    }

    modifiedMealPlanPaymentInfo.status = 'NEW'
    modifiedMealPlanPaymentInfo.total_amount_charged = total
    modifiedMealPlanPaymentInfo.total_amount_due = 0
    modifiedMealPlanPaymentInfo.total_amount_paid = total

    const updatedModifiedMyPlanDetail = {
      ...modifiedMMPDetail,
      payment_info: modifiedMealPlanPaymentInfo
    }

    this.props.updateModifiedMyMealPlanDetail(updatedModifiedMyPlanDetail)
  }
  handleScroll(event) {
    this.setState({ scrollY: event.nativeEvent.contentOffset.y })
  }
  resetToMyMealPlan() {
    this.props.resetModifyState()
    // need to hit componentWillUnmount on each screen
    this.props.navigation.pop(3)
  }
  disabledConfirmButton(status) {
    const { fetchingWallet } = this.props
    this.setState({ disabledConfirm: status || fetchingWallet })
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  navigateToTopUpScreen = () => {
    this.props.navigation.navigate('RootTopUpScreen', {navigationParams: 'back'})
  }

  _updateOrder = () => {
    setTimeout(() => {
      this.setState({
        showInvalidModal: false,
      }, () => {
        const { failedItem, failedDate, failedKitchenCode } = this.state
        this.props.navigation.navigate(
          'MyMealPlanModifyCatalogScreen', {
            deliveryDate: failedDate,
            item: failedItem,
            kitchen_code: failedKitchenCode,
          })
      })
    }, 500)
  }

  _keepOrder = () => {
    this.setState({
      showInvalidModal: false,
    }, () => {
      this.props.navigation.goBack('modifySingleOrderFromDetailScreenKey')
    })
  }

  render() {
    const {
      showLoading,
      modalType,
      showWebView,
      showErrorModal,
      modifiedMMPDetail,
      showModalModify,
      modifyText,
    } = this.state
    _.set(this.refs, 'Content._root.resetCoords', { x: 0, y: this.state.scrollY > 100 ? this.state.scrollY - 100 : this.state.scrollY })

    return (
      <Container>
        <NavigationBar leftSide='back' title='Payment' leftSideNavigation={this.navigationGoBack} />
        <ScrollView onScroll={event => this.handleScroll(event)}>
          <Content style={[styles.container]} keyboardShouldPersistTaps='handled' ref='Content'>
            {modifiedMMPDetail !== null && this.renderDetailTransaction()}
            <PaymentMethodPicker
              navigateToTopUpScreen={this.navigateToTopUpScreen}
              type='modify_delivery'
              wallet={this.props.wallet}
              setting={this.props.setting}
              order={this.props.modifiedMMPDetail}
              customerCard={this.props.customerCard}
              myMealPlanDetail={this.props.currentMMPDetail}
              modifiedMyMealPlan={this.props.modifiedMMPDetail}
              setMmpNewCreditCardInfo={this.props.setMmpNewCreditCardInfo}
              modifyMmpSubscriptionSelectedPayment={this.props.modifyMmpSubscriptionSelectedPayment}
              paymentInfoUpdater={(paymentInfo) => this.setPaymentInfo(paymentInfo)}
              confirmIsValid={(isValid) => this.disabledConfirmButton(!isValid)}
              fetchingWallet={this.props.fetchingWallet}
              setSelectedPayment={this.props.setSelectedPayment}
            />
          </Content>
          <Spinner visible={showLoading} />
          {this.state.authenticationUrl !== null &&
          <WebView
            visible={showWebView}
            dismiss={() => this.setState({ showWebView: false })}
            url={this.state.authenticationUrl}
            validationSuccess={(authData) => this.creditCard3DSValidationSuccess(authData)}
          />
          }
          <SalesOrderResultModal
            visible={showModalModify}
            modalType={modalType}
            text={modifyText}
            invoiceUrl={null}
            onPress={() => {
              this.setState({ showModalModify: false }, () => this.resetToMyMealPlan())
            }}
          />
          <AlertBox
            primaryAction={() => setTimeout(() => this.setState({ showLoading: true }, () => this.modifyAction()), 500)}
            dismiss={() => this.setState({ showModifyConfirmationModal: false })}
            primaryActionText='Yes'
            secondaryAction={() => this.setState({ showModifyConfirmationModal: false })}
            secondaryActionText='Cancel'
            text={'Are you sure want to modify this delivery ?'}
            visible={this.state.showModifyConfirmationModal}
          />
          <AlertBox
            text={'We can\'t process your credit card.\nPlease use another card'}
            dismiss={() => this.setState({ showErrorModal: false })}
            primaryAction={() => { }}
            primaryActionText='Ok'
            visible={showErrorModal}
          />
          <AlertBox
            primaryAction={this._keepOrder}
            dismiss={this._keepOrder}
            primaryActionText='Keep Existing'
            secondaryAction={this._updateOrder}
            secondaryActionText='Edit My Order'
            text={'Oops! One or more products that you chose have recently sold out or partially available. Please choose another product.'}
            title={'Product Availability Change'}
            visible={this.state.showInvalidModal}
            primaryStyle={'primary'}
            secondaryStyle={'secondary'}
          />
        </ScrollView>
        {this.renderPlaceYourOrderButton()}
      </Container>
    )
  }
}

const mapStateToProp = (state: AppState) => {
  return {
    currentMMPDetail: state.myMealPlan.myMealPlanDetail,
    modifiedMMPDetail: state.myMealPlan.modifiedMyMealPlanDetail,
    customerCard: state.customerCard,
    user: state.login.user,
    modifying: state.myMealPlan.modifying,
    modifySuccess: state.myMealPlan.modifySuccess,
    modifyErrorMessage: state.myMealPlan.modifyErrorMessage,
    modifyType: state.myMealPlan.modifyType,
    wallet: state.customerAccount.wallet,
    setting: state.setting.data,
    total: state.cart.totals.total,
    order: state.order,
    fetchingWallet: state.customerAccount.fetchingWallet,
  }
}
const mapDispatchToProp = dispatch => {
  return {
    updateModifiedMyMealPlanDetail: (updatedModifiedMyPlanDetail: MyMealPlanDetailSuccess) => dispatch(MyMealPlanActions.updateModifiedMyMealPlanDetail(updatedModifiedMyPlanDetail)),
    updateMyMealPlanWithPayment: () => dispatch(MyMealPlanActions.updateMyMealPlanWithPayment()),
    resetModifyState: () => dispatch(MyMealPlanActions.resetModifyState()),
    addCard: (card: Card) => dispatch(CustomerCardActions.addCard(card)),
    setModifyType: (modifyType: string) => dispatch(MyMealPlanActions.setModifyType(modifyType)),
    setMmpNewCreditCardInfo: (newCreditCardInfo: CreditCardInfo) => dispatch(MyMealPlanActions.setMmpNewCreditCardInfo(newCreditCardInfo)),
    modifyMmpSubscriptionSelectedPayment: (selectedPayment: string) => dispatch(MyMealPlanActions.modifyMmpSubscriptionSelectedPayment(selectedPayment)),
    setSelectedPayment: (selectedPayment: string) => dispatch(OrderActions.setSelectedPayment(selectedPayment)),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
  }
}

export default connect(mapStateToProp, mapDispatchToProp)(MyMealPlanModifyPaymentScreen)
