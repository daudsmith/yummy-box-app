import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native'
import { Images } from '../../../../Themes'
import Styles from '../../../Styles/SearchLocationScreenStyle'

class MyCurrentLocation extends Component {
  render() {
    const {
      setDestination,
    } = this.props
    return (
      <TouchableOpacity
        testID="currentLocation"
        accessibilityLabel="currentLocation"
        onPress={() => setDestination()}
        style={Styles.currentLocationRow}
      >
        <View style={Styles.currentLocationImage}>
          <Image
            source={Images.currentLocation}
            resizeMode='contain'
            style={Styles.currentLocationImage}
          />
        </View>
        <View style={Styles.resultListTextContainer}>
          <Text style={Styles.currentLocationText} numberOfLines={0}>
            Use My Current Location
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}

MyCurrentLocation.propTypes = {
  setDestination: PropTypes.func.isRequired,
}

export default MyCurrentLocation
