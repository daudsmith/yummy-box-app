import { Body, Header, Left } from 'native-base'
import React from 'react'
import { Image, TouchableOpacity, View } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'

import NavigationBarStyle from '../../../../Components/Styles/NavigationBarStyle'
import { Images, Colors } from '../../../../Themes'
import Styles from '../../../Styles/SearchLocationScreenStyle'
import SearchBox from './SearchBox'

export interface SearchNavBarProps {
  showClearText: boolean,
  searchValue: string,
  navigation: NavigationDrawerProp,
  onFocus: () => void,
  onClearText: () => void,
  getPlacePredictionsBasedOnText: (address: string) => void,
}

const SearchNavBar: React.FC<SearchNavBarProps> = (props) => {
  const {
    searchValue,
    showClearText,
    onClearText,
    onFocus,
    getPlacePredictionsBasedOnText,
    navigation,
  } = props
  const { noSearchLocation } = navigation.state.params
  return (
    <Header
      style={{
        ...NavigationBarStyle.container,
        paddingTop: verticalScale(20),
        paddingHorizontal: scale(20),
        paddingBottom: verticalScale(10),
        borderBottomColor: 'transparent',
      }}
      translucent={false}
      androidStatusBarColor={Colors.darkerDark}
      noShadow
    >
      {
        !noSearchLocation && <Left style={{ flex: 2, alignItems: 'center' }}>
          <TouchableOpacity
            testID={'backFrom_search_location_navigation_bar'}
            onPress={() => {
              navigation.goBack()
            }}
            hitSlop={{
              top: scale(26),
              left: scale(26),
              bottom: scale(26),
              right: scale(5),
            }}
          >
            <View>
              <Image source={Images.arrowLeft} style={{ width: scale(16) }} resizeMode='contain' />
            </View>
          </TouchableOpacity>
        </Left>
      }
      <Body
        style={Styles.bodyContainer}
      >
        <SearchBox
          addressInputValue={searchValue}
          onSearchBoxFocus={onFocus}
          getPlacePredictionsBasedOnText={getPlacePredictionsBasedOnText}
          onClearText={onClearText}
          showClearText={showClearText}
        />
      </Body>
    </Header>
  )
}

export default SearchNavBar
