import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Colors } from '../../../../Themes'
import Styles from '../../../Styles/SearchLocationScreenStyle'

class AddressList extends Component {
  render () {
    const {
      predictionList,
      onAddressPress,
    } = this.props

    return (
      <View>
        {
          predictionList.map((address) => {
            return (
              <TouchableOpacity
                testID={`addressList_${address.placeID}`}
                style={Styles.resultListRow}
                key={address.placeID}
                onPress={() => onAddressPress(address)}
              >
                <View
                  style={Styles.resultListIconContainer}
                >
                  <Ionicons
                    name='ios-pin'
                    style={{
                      fontSize: moderateScale(20),
                      color: Colors.primaryDark,
                    }}
                  />
                </View>
                <View
                  style={Styles.resultListTextContainer}
                >
                  <Text
                    style={{
                      fontFamily: 'Rubik-Medium',
                      color: Colors.primaryDark,
                      fontSize:16
                    }}
                  >
                    {address.primaryText}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Rubik-Regular',
                      fontSize: 14,
                      color: Colors.primaryGrey
                    }}
                  >
                    {address.fullText}
                  </Text>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </View>
    )
  }
}

AddressList.propTypes = {
  predictionList: PropTypes.array.isRequired,
  onAddressPress: PropTypes.func.isRequired,
}

export default AddressList
