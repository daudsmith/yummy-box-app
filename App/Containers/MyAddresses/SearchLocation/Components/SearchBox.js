import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { TextInput, TouchableWithoutFeedback, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Colors } from '../../../../Themes'
import Styles from '../../../Styles/SearchLocationScreenStyle'

class SearchBox extends Component {
  render() {
    const {
      addressInputValue,
      onSearchBoxFocus,
      getPlacePredictionsBasedOnText,
      showClearText,
      onClearText,
    } = this.props

    return (
      <View
        style={Styles.searchBox}
      >
        <View
          style={Styles.searchForm}
        >
          <Ionicons
            name="ios-search"
            size={20}
            style={Styles.searchBoxIcon}
          />
          <TextInput
            testID="inputAddress"
            accessibilityLabel="inputAddress"
            ref="search"
            value={addressInputValue}
            placeholder="Search Area"
            placeholderTextColor={Colors.primaryGrey}
            underlineColorAndroid="transparent"
            onFocus={() => onSearchBoxFocus()}
            onChangeText={(text) => getPlacePredictionsBasedOnText(text)}
            style={Styles.searchBoxTextInput}
            autoCapitalize='none'
            autoCorrect={false}
            autoFocus
          />
          {showClearText && (
            <TouchableWithoutFeedback
              onPress={onClearText}
            >
              <View style={{position:'absolute',right:1}}>
                <Ionicons
                  name='close-circle-outline'
                  size={24}
                  color={Colors.pinkishGrey}
                />
              </View>
            </TouchableWithoutFeedback>
          )}
        </View>
      </View>
    )
  }
}

SearchBox.propTypes = {
  addressInputValue: PropTypes.string.isRequired,
  onSearchBoxFocus: PropTypes.func.isRequired,
  getPlacePredictionsBasedOnText: PropTypes.func.isRequired,
  onClearText: PropTypes.func.isRequired,
  showClearText: PropTypes.bool.isRequired,
}

export default SearchBox
