import React from 'react'
import {
  Alert,
  Platform,
  ScrollView,
  Text,
  View,
  BackHandler,
} from 'react-native'
import RNGooglePlaces, { GMSTypes } from 'react-native-google-places'
import Permissions, {
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions'
import { connect } from 'react-redux'
import AlertBox from '../../../Components/AlertBox'
import WithSafeAreaView from '../../../Components/Common/WithSafeAreaView'
import AddressHistoryActions from '../../../Redux/AddressHistoryRedux'
import LastUsedActions, { baseLastUsed, Location } from '../../../Redux/V2/LastUsedRedux'
import Styles from '../../Styles/SearchLocationScreenStyle'
import AddressList from './Components/AddressList'
import MyCurrentLocation from './Components/MyCurrentLocation'
import SearchNavBar from './Components/SearchNavBar'
import { MenuSlider } from '../../../Components/Common'
import ApiKitchen from '../../../Services/V2/ApiKitchen'
import CustomerAddressActions, { Address } from '../../../Redux/CustomerAddressRedux'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import { Colors } from '../../../Themes'
import { Icon } from 'native-base'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import GoogleMatrixService from '../../../Services/GoogleMatrixService'
import SalesOrderService from '../../../Services/SalesOrderService'
import { CompanyAddress } from '../../../Redux/AddressTypeRedux'
import { TouchableOpacity } from 'react-native-gesture-handler'
import CartActions, { DeliveryAddress } from '../../../Redux/V2/CartRedux'
export interface AddressHistoryInterface {
  address: string,
  address_details: string,
  id: number,
  landmark: string,
  latitude: string,
  longitude: string,
  name: string,
  note: string,
  used: number
}

export interface SearchLocationPropsInterface {
  addressHistory: AddressHistoryInterface[],
  fetchAddressHistory: () => void,
  fetchAddress: () => void,
  addresses: string[],
  navigation: NavigationDrawerProp,
  addLastUsed: (code: string, location: Location, name: string, address?: string) => void,
  token: string,
  lastUsed: baseLastUsed,
  updateCartAddress: (address) => void,
  updateCartKitchen: (code: string) => void,
  resetCart: () => void,
}

export interface SearchLocationStateInterface {
  showErrorMessage: boolean,
  errorMessage: string,
  addressInputValue: string,
  showClearText: boolean,
  predictionList: GMSTypes.AutocompletePrediction[],
  showList: boolean,
  showHistory: boolean,
  showOutsideCoverageAreaModal: boolean,
  titleErrorMessage: string,
  savedAddresses: (CompanyAddress | Address)[]
}

class SearchLocation extends React.Component<SearchLocationPropsInterface, SearchLocationStateInterface> {
  autoCompleteTimeout
  geocodeTimeout

  constructor(props) {
    super(props)
    this.state = {
      showErrorMessage: false,
      errorMessage: '',
      titleErrorMessage: '',
      addressInputValue: '',
      showClearText: false,
      predictionList: [],
      showList: false,
      showHistory: false,
      showOutsideCoverageAreaModal: false,
      savedAddresses: []
    }
  }

  componentDidMount() {
    const { token, navigation } = this.props
    const { noSearchLocation } = navigation.state.params
    if (token) {
      this.props.fetchAddressHistory()
      this.props.fetchAddress()
      SalesOrderService.getUserAddress(token).then(res => {
        if (res.corporate && res.corporate.length > 0) {
          this.setState({
            savedAddresses: [...res.corporate, ...res.personal]
          })
        } else if(res.personal.length > 0) {
          this.setState({
            savedAddresses: [...res.personal]
          })
        }
      })
    }
    if (noSearchLocation) {
      BackHandler.addEventListener('hardwareBackPress', () => true)
    }
  }

  onSearchBoxFocus = () => {
    this.setState({
      showClearText: true,
    })
  }

  onClearText = () => {
    this.setState({
      showClearText: false,
      addressInputValue: '',
      predictionList: [],
      showHistory: true,
      showList: false,
    })
  }

  getPlacePredictionsBasedOnText = (address) => {
    clearTimeout(this.autoCompleteTimeout)

    this.setState({
      addressInputValue: address,
    }, () => {
      this.autoCompleteTimeout = setTimeout(() => {
        const { addressHistory } = this.props
        if (address !== '') {
          RNGooglePlaces.getAutocompletePredictions(address, {
            country: 'ID',
            useSessionToken: true,
          })
            .then((places) => {
              const prediction = places.filter(item => this.checkInputAddressCoverage(item.fullText))
              this.setState({
                predictionList: prediction,
                showClearText: true,
                showList: true,
                showHistory: false,
              })
            })
        } else {
          this.setState({
            predictionList: [],
            showClearText: false,
            showList: false,
            showHistory: addressHistory.length > 0,
          })
        }
      }, 300)
    })
  }

  checkInputAddressCoverage = (place) => {
    // Add more specific area based on city location
    let specificCoverageAreaTangerang = [
      'bsd',
      'tang sel',
      'tangsel',
      'tangerang selatan',
      'south tangerang',
    ]
    let specificCoverageAreaJakarta = ['jakarta']
    let allCoverageArea = [
      ...specificCoverageAreaTangerang,
      ...specificCoverageAreaJakarta,
    ]

    for (let index = 0; index <= allCoverageArea.length; index++) {
      if (place.toLowerCase()
        .indexOf(allCoverageArea[index]) > -1) {
        return true
      }
    }

    return false
  }

  getAddressByPlaceId = (place) => {
    clearTimeout(this.geocodeTimeout)
    this.geocodeTimeout = setTimeout(() => {
      RNGooglePlaces.lookUpPlaceByID(place.placeID, [
        'location',
        'name',
        'address',
      ])
        .then((place) => this.setSelectedAddress(place))
        .catch()
    }, 500)
  }

  setSelectedAddress = (place) => {
    const { longitude, latitude } = place.location
    const selectedPlace = {
      ...place,
      longitude,
      latitude,
    }
    this.navigateTo(selectedPlace, true)
  }

  setDestinationFromCurrentLocation = () => {
    Permissions
      .check(
        Platform.select({
          android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
          ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
        }),
      )
      .then(result => {
        switch (result) {
          case RESULTS.BLOCKED:
            Alert.alert(
              'Permission Blocked',
              'Please allow YummyBox to access you current location',
              [],
              { cancelable: true },
            )
            break
          case RESULTS.DENIED:
            Permissions.request(
              Platform.select({
                android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
              }),
            )
            break
          case RESULTS.UNAVAILABLE:
            Permissions.request(
              Platform.select({
                android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
              }),
            )
            break
          default:
            RNGooglePlaces.getCurrentPlace([
              'location',
              'name',
              'address',
            ])
              .then((results) => {
                if (results && typeof results[0] !== 'undefined') {
                  this.setSelectedAddress(results[0])
                } else {
                  this.setState({
                    showErrorMessage: true,
                    errorMessage: 'Location not found',
                  })
                }
              })
              .catch((error) => {
                const response = error.response.data
                const { message } = response
                if (message) {
                  this.setState({
                    showErrorMessage: true,
                    errorMessage: message,
                  })
                }
                return response
              })
        }
      })
  }

  setDestinationFromHistory = (history) => {    
    const formatAddress = {
      name: history.landmark,
      address: history.address,
      longitude: history.longitude,
      latitude: history.latitude,
      recipient: history.name,
      phone: history.phone,
    }

    this.navigateTo(formatAddress)
  }

  onBadgePress = (address) => {
    const formatAddress = {
      name: address.label,
      address: address.address,
      longitude: address.longitude,
      latitude: address.latitude,
      recipient: address.name,
      phone: address.phone,
    }

    this.navigateTo(formatAddress)
  }

  formatHistoryToAddressList = (historyList) => {
    return historyList.map(history => {
      return {
        ...history,
        primaryText: history.landmark,
        placeID: history.id,
        secondaryText: history.address,
        fullText: history.address,
      }
    })
  }

  navigateTo = (selectedAddress, shouldResetCart = false) => {
    const {
      navigation,
      addLastUsed,
      token,
      updateCartAddress,
      resetCart,
      updateCartKitchen
    } = this.props
    let navigateParams = {
      selectedAddress: {
        addressDetail: selectedAddress.address,
        latitude: selectedAddress.latitude,
        longitude: selectedAddress.longitude,
      },
      ...navigation.state.params
    }
    const location = {
      latitude: selectedAddress.latitude,
      longitude: selectedAddress.longitude,
    }
    ApiKitchen.create()
      .kitchenList(selectedAddress.latitude, selectedAddress.longitude)
      .then(async (response) => {
        const data = response.data.data        
        if (data) {
          if (navigation.state.key !== 'CreateFromMyAddressKey') {
            if (data.length > 1) {
              const destination = data.map(item => {
                return `${item.latitude},${item.longitude}`
              }).join('|')
              await GoogleMatrixService.checkGoogleMatrix(selectedAddress.latitude, selectedAddress.longitude, destination).then(index => {
                updateCartKitchen(data[index].code)
                addLastUsed(data[index].code, location, selectedAddress.name, selectedAddress.address)
              })
            } else {
              updateCartKitchen(data[0].code)
              addLastUsed(data[0].code, location, selectedAddress.name, selectedAddress.address)
            }
          }

          if (token) {
            if (navigation.state.key === 'CreateFromMyAddressKey' || 
                navigation.state.key === 'SearchLocationKey' || 
                navigation.state.key === 'CreateAddressFromAddressOptionKey') {
              navigation.navigate({
                routeName: navigation.state.params.mapScreenName || 'MapScreen',
                params: navigateParams,
              })
            } else {
              if (shouldResetCart) {
                resetCart()
              }
              else{
                updateCartAddress({...selectedAddress, name:selectedAddress.recipient})
              }
              navigation.navigate({
                routeName: 'HomeScreen',
              })
            }
          } else {
            navigation.navigate({
              routeName: 'HomeScreen',
            })
          }
        } else {
          this.setState({
            showErrorMessage: true,
            titleErrorMessage: 'Out of Delivery Coverage',
            errorMessage: 'Unfortunately, we’re not able to deliver to this location yet. Please select another location.',
          })
        }
      })
  }

  closeAlertBox = () => {
    this.setState({
      showErrorMessage: false,
      errorMessage: '',
    })
  }

  renderSavedAddressBadge = ({ item }) => {    
    return (
      <TouchableOpacity onPress={()=>this.onBadgePress(item)} key={item.id}>
        <View style={{
          padding: moderateScale(10),
          marginHorizontal: scale(7),
          marginVertical: moderateScale(2),
          borderRadius: moderateScale(20),
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: Colors.white,
          shadowColor: Colors.darkBlue,
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.15,
          shadowRadius: 1,
          elevation: 2,
        }}>
          <Icon
            name="ios-bookmark"
            style={{
              color: Colors.primaryTurquoise,
              fontSize: moderateScale(16),
              marginRight: scale(5)
            }}
          />
          <Text>{item.display_label}</Text>
        </View>
      </TouchableOpacity >
    )
  }

  render() {
    const {
      predictionList,
      addressInputValue,
      showClearText,
      showErrorMessage,
      errorMessage,
      showList,
      titleErrorMessage
    } = this.state
    const {
      navigation,
      addressHistory,
    } = this.props
    const historyList = this.formatHistoryToAddressList(addressHistory)
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}
      >
        <SearchNavBar
          searchValue={addressInputValue}
          showClearText={showClearText}
          onClearText={this.onClearText}
          onFocus={this.onSearchBoxFocus}
          getPlacePredictionsBasedOnText={this.getPlacePredictionsBasedOnText}
          navigation={navigation}
        />
        <ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
        >

          {showList ? (
            <AddressList
              predictionList={predictionList}
              onAddressPress={this.getAddressByPlaceId}
            />
          ) : (
              <View>
                <MyCurrentLocation
                  setDestination={this.setDestinationFromCurrentLocation}
                />
                {
                  (!['CreateFromMyAddressKey', 'CreateAddressFromAddressOptionKey'].some(key => navigation.state.key.includes(key)) && this.state.savedAddresses.length > 0) &&
                  <MenuSlider
                    style={{
                      paddingHorizontal: scale(20),
                      paddingBottom: verticalScale(20),
                    }}
                    items={this.state.savedAddresses}
                    renderItem={this.renderSavedAddressBadge}
                    maxRender={6}
                    initialRender={3}
                    windowSize={1}
                    removeClippedSubviews={true}
                  />
                }

                <Text
                  style={Styles.historyText}
                >
                  Recently Used Address
                </Text>

                <AddressList
                  predictionList={historyList}
                  onAddressPress={this.setDestinationFromHistory}
                />
              </View>
            )}
        </ScrollView>

        <View>
          <Text
            style={Styles.coverageAreaText}
          >
            We cover all areas within Jakarta & South Tangerang.
            </Text>
        </View>
        <AlertBox
          primaryAction={this.closeAlertBox}
          primaryActionText='Search Another Location'
          dismiss={this.closeAlertBox}
          title={titleErrorMessage}
          text={errorMessage}
          visible={showErrorMessage}
        />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    addressHistory: state.addressHistory.addresses,
    addresses: state.customerAddress.addresses,
    token: state.login.token,
    lastUsed: state.lastUsed
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAddressHistory: () => dispatch(AddressHistoryActions.fetchAddressHistory()),
    fetchAddress: () => dispatch(CustomerAddressActions.fetchAddress()),
    addLastUsed: (code: string, location: Location, name: string, address?: string) => dispatch(LastUsedActions.addLastUsed(code, location, name, address)),
    updateCartAddress: (address: DeliveryAddress) => dispatch(CartActions.updateAddress(address)),
    resetCart: () => dispatch(CartActions.resetCart()),
    updateCartKitchen: (code: string) => dispatch(CartActions.updateKitchen(code)),

  }
}

export default WithSafeAreaView(connect(mapStateToProps, mapDispatchToProps)(SearchLocation))
