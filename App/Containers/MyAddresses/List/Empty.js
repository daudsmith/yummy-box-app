import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'
import { Colors, Images } from '../../../Themes'
import styles from '../../Styles/CustomerMyOrderListScreenStyle'

class Empty extends Component {
  render() {
    return (
      <View
        style={{
          flexDirection: 'column',
          backgroundColor: 'white',
          marginHorizontal: 40,
          marginTop: 40,
        }}
      >
        <View
          style={{
            alignItems: 'center',
          }}
        >
          <View
            style={{
              marginTop: 31,
            }}
          >
            <Image
              source={Images.noSavedAddress}
              style={{
                width: 260,
                height: 122,
              }}
              resizeMode='contain'
            />
          </View>
          <View
            style={{
              flex: 1,
              marginTop: 31,
            }}
          >
            <Text style={[
              styles.orderStatusText,
              {
                color: Colors.primaryDark,
                textAlign: 'center',
                fontSize: 14,
              },
            ]}>
              Save More Time with Saved Address
            </Text>
            <Text style={[
              styles.descriptionText,
              {
                color: Colors.primaryGrey,
                fontFamily: 'Rubik-Regular',
                textAlign: 'center',
                fontSize: 14,
                paddingTop: 8,
              },
            ]}>
              Add your detailed address for faster checkout
            </Text>
          </View>
        </View>
      </View>
    )
  }
}

export default Empty
