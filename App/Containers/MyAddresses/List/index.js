import { Icon } from 'native-base'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { FlatList, RefreshControl, TouchableWithoutFeedback, View } from 'react-native'
import { connect } from 'react-redux'
import NavigationBar from '../../../Components/NavigationBar'
import OfflineModal from '../../../Components/OfflineModal'
import OfflinePage from '../../../Components/OfflinePage'
import WithSafeAreaView from '../../../Components/Common/WithSafeAreaView'
import CustomerAddressActions from '../../../Redux/CustomerAddressRedux'
import { Colors } from '../../../Themes'
import Empty from './Empty'
import Item from './Item'
import {
  NavigationActions,
  StackActions,
} from 'react-navigation'

class List extends Component {
  state = {
    init: true,
  }

  componentDidMount() {
    this.getAddresses()
  }

  getAddresses = () => {
    const {
      fetchAddress,
    } = this.props
    fetchAddress()
    this.setState({
      init: false,
    })
  }

  handleItemPress = (item) => {
    const {
      navigation,
    } = this.props
    navigation.navigate({
      routeName: 'CreateAddress',
      params: {
        selectedAddress: this.formatAddressObject(item),
        backKey: 'CreateFromMyAddressKey',
      },
      key: 'CreateFromMyAddressKey',
    })
  }

  addNewAddress = () => {
    const {
      navigation,
    } = this.props
    const {
      params,
    } = navigation.state
    if (params) {
      const {
        selectHandler,
      } = params
      if (selectHandler) {
        navigation.navigate({
          routeName: 'SearchLocation',
          params: {
            selectHandler: (address) => selectHandler(address),
            backKey: 'CreateFromMyAddressKey'
          },
          key: 'CreateFromMyAddressKey',
        })
      } else {
        navigation.navigate({
          routeName: 'SearchLocation',
          params: {
            backKey: 'CreateFromMyAddressKey'
          },
          key: 'CreateFromMyAddressKey',
        })
      }
    } else {
      navigation.navigate({
        routeName: 'SearchLocation',
        params: {
          backKey: 'CreateFromMyAddressKey'
        },
        key: 'CreateFromMyAddressKey',
      })
    }
  }

  addButton = () => {
    return (
      <TouchableWithoutFeedback
        testID="newAddress"
        accessibilityLabel="newAddress"
        onPress={this.addNewAddress}
      >
        <View>
          <Icon
            name="ios-add"
            style={{
              color: Colors.primaryTurquoise,
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    )
  }

  formatAddressObject = (item) => {
    return {
      addressId: item.addressId || '',
      latitude: item.latitude || '',
      longitude: item.longitude || '',
      addressLabel: item.label || '',
      addressPinpoint: item.landmark || '',
      addressDetail: item.address || '',
      recipientName: item.name || '',
      recipientNumber: item.phone || '',
      deliveryInstructions: item.note || '',
    }
  }

  goBackAction = () => {
    const {
      navigation,
    } = this.props
    navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'AccountScreen' })],
      })
    )
  }

  render() {
    const {
      navigation,
      loading,
      addresses,
      isConnected,
    } = this.props
    const {
      init,
    } = this.state
    let offline = false
    if (!isConnected && init) {
      offline = true
    }

    return (
      <View style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
        <NavigationBar
          leftSide="back"
          leftSideNavigation={this.goBackAction}
          title='Alamat Tersimpan'
          rightSide={this.addButton}
        />
        {offline && (
          <OfflinePage />
        )}
        {!offline && addresses.length > 0 && (
          <FlatList
            data={addresses}
            keyExtractor={item => item.addressId.toString()}
            renderItem={({ item, index }) => (
              <Item
                index={index}
                item={item}
                renderSeparator={(index !== (addresses.length - 1))}
                onItemPress={this.handleItemPress}
                navigation={navigation}
              />
            )}
            refreshControl={
              <RefreshControl
                refreshing={loading}
                onRefresh={this.getAddresses}
                progressBackgroundColor={Colors.bloodOrange}
                tintColor={Colors.bloodOrange}
                colors={[Colors.white]}
              />
            }
            viewabilityConfig={{
              viewAreaCoveragePercentThreshold: 90,
            }}
          />
        )}
        {!offline && !loading && addresses.length === 0 && (
          <Empty />
        )}
        <OfflineModal isConnected={isConnected} />
      </View>
    )
  }
}

List.propTypes = {
  navigation: PropTypes.object.isRequired,
  addresses: PropTypes.array.isRequired,
  isConnected: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  fetchAddress: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => {
  return {
    loading: state.customerAddress.loading,
    addresses: state.customerAddress.addresses,
    isConnected: state.network.isConnected,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAddress: () => dispatch(CustomerAddressActions.fetchAddress()),
  }
}

export default WithSafeAreaView(connect(mapStateToProps, mapDispatchToProps)(List))
