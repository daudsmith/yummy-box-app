import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Image, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import Icon from 'react-native-vector-icons/Entypo'
import { Colors, Images } from '../../../Themes'
import styles from '../../Styles/CustomerMyOrderListScreenStyle'

class Item extends Component {
  itemPressHandler = (item) => {
    const {
      onItemPress,
    } = this.props
    onItemPress(item)
  }

  render() {
    const {
      item,
      index,
      renderSeparator,
    } = this.props
    const separator = renderSeparator
      ? {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: Colors.pinkishGrey,
      }
      : {}
    return (
      <TouchableWithoutFeedback
        testID={`addressDetail${index}`}
        accessibilityLabel={`addressDetail${index}`}
        key={item.id}
        onPress={() => this.itemPressHandler(item)}
      >
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: 'white',
          }}
        >
          <View
            style={[
              styles.row,
              separator,
            ]}
          >
            <View
              style={{
                justifyContent: 'flex-start',
              }}
            >
              <Image
                source={Images.bookmark}
                style={{
                  width: 20,
                  height: 20,
                  marginRight: 14,
                }}
                resizeMode='contain'
              />
            </View>
            <View style={{ flex: 1 }}>
              <Text style={[
                styles.orderStatusText,
                {
                  color: Colors.primaryDark,
                  textAlign: 'left',
                },
              ]}>
                {item.display_label}
              </Text>
              <View style={{ marginTop: 8 }}>
                <Text style={[
                  styles.descriptionText,
                  { color: Colors.primaryGrey },
                ]}>
                  {item.display_address}
                </Text>
              </View>
              <View style={{ marginTop: 8 }}>
                <Text style={[
                  styles.descriptionText,
                  { color: Colors.primaryGrey },
                ]}>
                  {item.recipient}
                </Text>
              </View>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <Icon
                name='chevron-right'
                color={Colors.primaryDark}
                size={20}
                style={styles.arrow}
              />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

Item.propTypes = {
  item: PropTypes.object.isRequired,
  renderSeparator: PropTypes.bool.isRequired,
  onItemPress: PropTypes.func.isRequired,
}

export default Item
