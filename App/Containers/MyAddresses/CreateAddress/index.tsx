import { Content } from 'native-base'
import * as React from 'react'
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { connect } from 'react-redux'
import AlertBox from '../../../Components/AlertBox'
import BottomButton from '../../../Components/BottomButton'
import NavigationBar from '../../../Components/NavigationBar'
import WithSafeAreaView from '../../../Components/Common/WithSafeAreaView'
import CustomerAddressActions, { Address } from '../../../Redux/CustomerAddressRedux'
import CartActions, {
  CartState,
  DeliveryAddress,
} from '../../../Redux/V2/CartRedux'
import { Images } from '../../../Themes'
import Styles from '../../Styles/SearchLocationScreenStyle'
import {
  scale, verticalScale, moderateScale,
} from 'react-native-size-matters'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { AppState } from '../../../Redux/CreateStore'
import { User } from '../../../Redux/LoginRedux'
import Colors from '../../../Themes/Colors'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { StackNavigationProp } from 'react-navigation-stack/lib/typescript/src/vendor/types'

export interface CreateAddressProps {
  navigation: NavigationDrawerProp & StackNavigationProp,
  isConnected: boolean,
  loading: boolean
  addresses: Address[],
  error: string,
  saved?: boolean,
  edited?: boolean,
  deleted?: boolean,
  saveAddress: (address) => void, // TODO: recheck type
  editAddress: (addressId, address) => void, // TODO: recheck type
  deleteAddress: (addressId) => void, // TODO: recheck type
  onSelectAddress: (place: DeliveryAddress) => void,
  cart: CartState,
  user: User
}

export interface CreateAddressStates {
  addressParams: any,
  addressId?: number,
  latitude: number,
  longitude: number,
  addressLabel: string,
  addressPinpoint: string,
  addressDetail: string,
  recipientName: string,
  recipientNumber: string,
  deliveryInstructions: string,
  errorModal: boolean,
  errorMessage: string,
  instructionLimit: number,
  instructionFill: number,
  deleteConfirm: boolean,
  discardConfirm: boolean,
  modified: boolean,
  addressLabelNull: boolean,
  addressPinpointNull: boolean,
  addressDetailNull: boolean,
  recipientNameNull: boolean,
  recipientNumberInvalid: boolean,
  recipientNumberNull: boolean,
  selectHandler: (address) => void,
}

class CreateAddress extends React.Component<CreateAddressProps, CreateAddressStates> {
  constructor(props) {
    super(props)

    let params: any = {}
    const {
      selectedAddress,
      selectHandler,
    } = this.props.navigation.state.params
    if (typeof selectedAddress !== 'undefined' && selectedAddress) {
      params = selectedAddress
    }

    this.state = {
      addressParams: params,
      addressId: params.addressId || '',
      latitude: params.latitude || '',
      longitude: params.longitude || '',
      addressLabel: params.addressLabel || '',
      addressPinpoint: params.addressPinpoint || '',
      addressDetail: params.addressDetail || '',
      recipientName: params.recipientName || this.props.user.first_name || '',
      recipientNumber: params.recipientNumber || this.props.user.phone || '',
      deliveryInstructions: params.deliveryInstructions || '',
      errorModal: false,
      errorMessage: '',
      instructionLimit: 80,
      instructionFill: params.deliveryInstructions && params.deliveryInstructions.length || 0,
      deleteConfirm: false,
      discardConfirm: false,
      modified: false,
      addressLabelNull: false,
      addressPinpointNull: false,
      addressDetailNull: false,
      recipientNameNull: false,
      recipientNumberInvalid: false,
      recipientNumberNull: false,
      selectHandler: selectHandler,
    }
  }

  componentDidUpdate(prevProps) {
    const { navigation } = prevProps
    const { params } = navigation.state

    let newParams: any = {}
    const { selectedAddress } = this.props.navigation.state.params
    if (typeof selectedAddress !== 'undefined' && selectedAddress) {
      newParams = selectedAddress
    }
    if (params.selectedAddress.addressPinpoint !== newParams.addressPinpoint) {
      this.setState({
        addressPinpoint: newParams.addressPinpoint,
        addressDetail: newParams.addressDetail,
        latitude: newParams.latitude,
        longitude: newParams.longitude,
      })
    }
  }

  isModifyAddress = () => {
    const { addressId } = this.state
    return !!addressId
  }

  handleSubmitAddress = () => {
    if (this.validateForm()) {
      this.props.saveAddress(this.state)
      const { navigation, loading, error } = this.props
      if (!loading && error) {
        this.setState({
          errorModal: true,
          errorMessage: error,
        })
      } else {
        const {
          params: {
            selectHandler,
          },
        } = navigation.state
        const {
          addressPinpoint,
          addressDetail,
          latitude,
          longitude,
          recipientName,
          recipientNumber,
          addressLabel,
          deliveryInstructions,
        } = this.state
        const selectedAddress = {
          landmark: addressPinpoint,
          address: addressDetail,
          latitude: latitude,
          longitude: longitude,
          name: recipientName,
          phone: recipientNumber,
          label: addressLabel,
          note: deliveryInstructions,
        }
        if (selectHandler) {
          selectHandler(selectedAddress)
        } else {
          navigation.pop(navigation.state.params.pop || 3)
        }
      }
    }
    return false
  }

  handleEditAddress = () => {
    const { addressId } = this.state
    if (this.validateForm()) {
      this.props.editAddress(addressId, this.state)
      const { navigation, loading, error } = this.props
      if (!loading && error) {
        this.setState({
          errorModal: true,
          errorMessage: error,
        })
      } else {
        const {
          params: {
            selectHandler,
          },
        } = navigation.state
        const {
          addressPinpoint,
          addressDetail,
          latitude,
          longitude,
          recipientName,
          recipientNumber,
          addressLabel,
          deliveryInstructions,
        } = this.state
        const selectedAddress = {
          landmark: addressPinpoint,
          address: addressDetail,
          latitude: latitude,
          longitude: longitude,
          name: recipientName,
          phone: recipientNumber,
          label: addressLabel,
          note: deliveryInstructions,
        }
        if (selectHandler) {
          selectHandler(selectedAddress)
        } else {
          navigation.goBack()
        }
      }
    }
    return false
  }

  handleDeleteAddress = () => {
    const { addressId } = this.state
    this.props.deleteAddress(addressId)
    const { navigation, loading, error } = this.props
    if (!loading && error) {
      this.setState({
        errorModal: true,
        errorMessage: error,
      })
    } else {
      navigation.goBack()
    }
  }

  navigateBack = () => {
    const { navigation } = this.props
    this.setState({
      discardConfirm: false,
    }, () => {
      navigation.goBack()
    })
  }

  navigatePinPoint = () => {
    const { navigation } = this.props
    const {
      params,
    } = navigation.state
    const {
      selectHandler,
      searchScreenName,
      mapScreenName,
      createScreenName
    } = params
    const {
      addressParams,
    } = this.state    
    
    if (searchScreenName == 'CoSearchLocation' && !this.isModifyAddress()) {
      navigation.goBack()
    }
    else{
      navigation.navigate({
        routeName: searchScreenName || 'SearchLocation',
        params: {
          editAddressParams: addressParams,
          selectHandler,
          mapScreenName,
          createScreenName
        },
        key: 'SearchLocationKey',
      })
    }
  }

  deleteButton = () => {
    const { loading } = this.props
    return (
      <TouchableWithoutFeedback
        testID="deleteAddress"
        accessibilityLabel="deleteAddress"
        onPress={() => this.setState({ deleteConfirm: true })}
        disabled={loading}
      >
        <View>
          <Image source={Images.trash} style={{ width: scale(24) }} resizeMode='contain' />
        </View>
      </TouchableWithoutFeedback>
    )
  }

  validateForm = () => {
    const {
      latitude,
      longitude,
      addressLabel,
      addressPinpoint,
      addressDetail,
      recipientName,
      recipientNumber,
    } = this.state
    if (!latitude || !longitude) {
      alert('Can not Empty Longitude & Latitude')
      return false
    } else if (!this.addressLabelValidation(addressLabel)) {
      return false
    } else if (!this.addressPinpointValidation(addressPinpoint)) {
      return false
    } else if (!this.addressDetailValidation(addressDetail)) {
      return false
    } else if (!this.recipientNameValidation(recipientName)) {
      return false
    } else {
      return this.recipientNumberValidation(recipientNumber)
    }
  }

  setInstruction = (text) => {
    const {
      instructionLimit,
      deliveryInstructions,
    } = this.state
    this.setState({
      deliveryInstructions: text.length < instructionLimit
        ? text
        : deliveryInstructions,
      instructionFill: text.length,
      modified: true,
    })
  }

  onBackPress = () => {
    const {
      modified,
    } = this.state
    if (modified) {
      this.setState({
        discardConfirm: true,
      })
    } else {
      this.navigateBack()
    }
  }

  addressLabelValidation = (addressLabel) => {
    if (!addressLabel) {
      this.setState({
        addressLabelNull: true,
      })
      return false
    } else {
      this.setState({
        addressLabelNull: false,
      })
      return true
    }
  }

  addressPinpointValidation = (addressPinpoint) => {
    if (!addressPinpoint) {
      this.setState({
        addressPinpointNull: true,
      })
      return false
    } else {
      this.setState({
        addressPinpointNull: false,
      })
      return true
    }
  }

  addressDetailValidation = (addressDetail) => {
    if (!addressDetail) {
      this.setState({
        addressDetailNull: true,
      })
      return false
    } else {
      this.setState({
        addressDetailNull: false,
      })
      return true
    }
  }

  recipientNameValidation = (recipientName) => {
    if (!recipientName) {
      this.setState({
        recipientNameNull: true,
      })
      return false
    } else {
      this.setState({
        recipientNameNull: false,
      })
      return true
    }
  }

  recipientNumberValidation = (recipientNumber) => {
    var phoneNumberCheck = /^[0-9]*$/

    if (!this.state.recipientNumber || recipientNumber.length < 10) {
      this.setState({
        recipientNumberNull: true,
        modified: true,
      })
      return false
    } else if (!phoneNumberCheck.test(recipientNumber)) {
      this.setState({
        recipientNumberInvalid: true,
        modified: true
      })
      return false
    } else {
      this.setState({
        recipientNumberInvalid: false,
        recipientNumberNull: false,
        modified: true,
      })
      return true
    }
  }

  handleTextChange = (fieldName: string, value: string) => {
    this.setState({
      [fieldName]: value,
      modified: true,
    } as any)
  }

  render() {
    const {
      addressLabel,
      addressPinpoint,
      addressDetail,
      recipientName,
      recipientNumber,
      deliveryInstructions,
      errorModal,
      errorMessage,
      instructionLimit,
      instructionFill,
      deleteConfirm,
      discardConfirm,
      addressLabelNull,
      addressPinpointNull,
      addressDetailNull,
      recipientNameNull,
      recipientNumberInvalid,
      recipientNumberNull,
      selectHandler,
    } = this.state
    const { loading } = this.props
    let isModify = this.isModifyAddress()
    let haveSelect = typeof selectHandler === 'function'

    return (
      <View style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
        <NavigationBar
          leftSide="back"
          leftSideNavigation={this.onBackPress}
          title={!isModify
            ? 'Tambah Alamat Baru'
            : 'Ubah Alamat'}
          rightSide={isModify && !haveSelect && this.deleteButton}
        />
        <ScrollView>
          <KeyboardAvoidingView>
            <Content>
              <View style={addressLabelNull
                ? Styles.formControlError
                : Styles.formControl}>
                <View style={Styles.labelContainer}>
                  <Text style={Styles.label}>Label Alamat</Text>
                  <Text style={Styles.remark}>*</Text>
                </View>
                <View style={[Styles.labelContainer, { marginVertical: verticalScale(8) }]}>
                  <Text style={Styles.textWarningAddressLabel}>
                  Label alamat hanya sebagai referensi dan tidak akan dimunculkan saat pengiriman
                </Text>
                </View>
                <TextInput
                  // placeholder='E.g. Work, Home'
                  placeholder='Contoh: Kerja, Rumah'
                  autoCapitalize='none'
                  onChangeText={(text) => this.handleTextChange('addressLabel', text)}
                  underlineColorAndroid='transparent'
                  value={addressLabel}
                  style={Styles.inputForm}
                  testID="addressLabel"
                  accessibilityLabel="addressLabel"
                />
              </View>

              {addressLabelNull &&
                <Text style={Styles.labelError}>This field is required</Text>
              }

              <View style={Styles.divider} />

              <View style={recipientNameNull
                ? Styles.formControlError
                : Styles.formControl}>
                <View style={Styles.labelContainer}>
                  <Text style={Styles.label}>Nama Penerima</Text>
                  <Text style={Styles.remark}>*</Text>
                </View>
                <TextInput
                  autoCapitalize='none'
                  onChangeText={(text) => this.handleTextChange('recipientName', text)}
                  underlineColorAndroid='transparent'
                  value={recipientName}
                  style={Styles.inputForm}
                  testID="recipName"
                  accessibilityLabel="recipName"
                />
              </View>
              {recipientNameNull &&
                <Text style={Styles.labelError}>This field is required</Text>
              }
              <View style={recipientNumberNull || recipientNumberInvalid
                ? Styles.formControlError
                : Styles.formControl}>
                <View style={Styles.labelContainer}>
                  <Text style={Styles.label}>Nomor Kontak Penerima</Text>
                  <Text style={Styles.remark}>*</Text>
                </View>
                <TextInput
                  autoCapitalize='none'
                  onChangeText={(text) => this.handleTextChange('recipientNumber', text)}
                  underlineColorAndroid='transparent'
                  value={recipientNumber}
                  style={Styles.inputForm}
                  maxLength={20}
                  testID="recipNum"
                  accessibilityLabel="recipNum"
                />
              </View>
              {recipientNumberInvalid &&
                <Text style={Styles.labelError}>This field is invalid. Only can accept number 0-9.</Text>
              }
              {recipientNumberNull &&
                <Text style={Styles.labelError}>This field is required</Text>
              }

              <View style={Styles.divider} />

              <View style={[Styles.formControl, {
                borderBottomColor: Colors.white
              }]}>
                <View style={[Styles.labelContainer, {
                  justifyContent: 'space-between',
                }]}>
                  <Text style={Styles.label}>Lokasi Pinpoint Pengiriman</Text>
                  <View>

                    <TouchableWithoutFeedback
                      testID="change"
                      accessibilityLabel="change"
                      onPress={this.navigatePinPoint}
                    >
                      <View style={Styles.formControlWithButton}>
                        <Image
                          source={Images.currentLocation}
                          resizeMode='contain'
                          style={Styles.changeButtonIcon}
                        />
                        <Text style={Styles.changeButtonText}>
                          Ubah
                      </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                </View>
                <View>
                  <Text style={Styles.helper}>{addressDetail}</Text>
                </View>
              </View>
              {addressDetailNull &&
                <Text style={Styles.labelError}>This field is required</Text>
              }
              <View style={addressPinpointNull
                ? Styles.formControlError
                : Styles.formControl}>
                <View style={Styles.labelContainer}>
                  <Text style={Styles.label}>Alamat Lengkap</Text>
                  <Text style={Styles.remark}>*</Text>
                </View>
                <View style={{marginTop:4}}>
                  <Text style={Styles.textWarningAddressLabel}>Alamat ini akan dimunculkan pada paket pengiriman anda</Text>
                </View>
                <TextInput
                  autoCapitalize='none'
                  onChangeText={(text) => this.handleTextChange('addressPinpoint', text)}
                  underlineColorAndroid='transparent'
                  value={addressPinpoint}
                  style={Styles.inputForm}
                  multiline
                  numberOfLines={2}
                  testID="addressPinpoint"
                  accessibilityLabel="addressPinpoint"
                />
              </View>
              {addressPinpointNull &&
                <Text style={Styles.labelError}>This field is required</Text>
              }
              <View style={Styles.formControl}>
                <Text style={Styles.label}>Instruksi Pengiriman</Text>
                <TextInput
                  placeholder='Rumah di sudut jalan'
                  autoCapitalize='none'
                  onChangeText={(text) => this.setInstruction(text)}
                  underlineColorAndroid='transparent'
                  value={deliveryInstructions}
                  style={Styles.inputForm}
                  multiline
                  numberOfLines={2}
                  testID="delivInstruction"
                  accessibilityLabel="delivInstruction"
                />
              </View>
              <Text style={Styles.textAreaHelper}>{instructionFill}/{instructionLimit}</Text>

              <View style={Styles.warningBox}>
                <View style={{ marginRight: verticalScale(8), paddingTop: scale(8) }}>
                  <Icon name='info' size={moderateScale(24)} color={Colors.darkerWarning} />

                </View>
                <View style={{ flex: 1 }}>
                  <Text style={{ color: Colors.darkerWarning, textAlign: 'justify', fontSize: moderateScale(12) }}>
                    Tolong masukkan detail pinpoin penerima dan alamat lengkap yang spesifik untuk memastikan pengalaman pengiriman yang lebih baik
                  </Text>
                </View>
              </View>
            </Content>

          </KeyboardAvoidingView>
        </ScrollView>
        <BottomButton
          testID="saveAddress"
          accessibilityLabel="saveAddress"
          buttonText="Simpan Alamat"
          pressHandler={!isModify
            ? this.handleSubmitAddress
            : this.handleEditAddress}
          disabled={loading}
        />

        <AlertBox
          primaryAction={() => this.setState({ deleteConfirm: false })}
          dismiss={() => this.setState({ deleteConfirm: false })}
          primaryActionTestProp='deleteCancel'
          primaryActionText='Cancel'
          secondaryAction={this.handleDeleteAddress}
          secondaryActionText='Remove'
          secondaryActionTestProp='deleteRemove'
          text={'Are you sure you want to delete this address?'}
          visible={deleteConfirm}
          title={'Remove Address?'}
        />

        <AlertBox
          primaryAction={() => this.setState({ discardConfirm: false })}
          dismiss={() => this.setState({ discardConfirm: false })}
          primaryActionText='Cancel'
          secondaryAction={() => this.navigateBack()}
          secondaryActionText='Discard'
          text={'You have unsaved changes. Are you sure you want to leave this screen?'}
          visible={discardConfirm}
          title={'Discard Changes?'}
        />

        <AlertBox
          primaryAction={() => { }}
          primaryActionText='OK'
          dismiss={() => this.setState({ errorModal: false })}
          text={errorMessage}
          visible={errorModal}
        />
      </View>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    loading: state.customerAddress.loading,
    error: state.customerAddress.error,
    addresses: state.customerAddress.addresses,
    saved: state.customerAddress.saved,
    edited: state.customerAddress.edited,
    deleted: state.customerAddress.deleted,
    isConnected: state.network.isConnected,
    cart: state.cart,
    user: state.login.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveAddress: (address: Address) => dispatch(CustomerAddressActions.saveAddress(address)),
    editAddress: (addressId: number, address: Address) => dispatch(CustomerAddressActions.editAddress(addressId, address)),
    deleteAddress: (addressId: number) => dispatch(CustomerAddressActions.deleteAddress(addressId)),
    onSelectAddress: (place: DeliveryAddress) => dispatch(CartActions.updateAddress(place)),
  }
}

export default WithSafeAreaView(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CreateAddress))
