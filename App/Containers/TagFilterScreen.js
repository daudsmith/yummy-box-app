import _ from 'lodash'
import { Button, Container, Content, Footer, Icon, Text } from 'native-base'
import React, { Component } from 'react'
import { TouchableOpacity, TouchableWithoutFeedback, View, StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import NavigationBar from '../Components/NavigationBar'
import PrimaryButton from '../Components/PrimaryButton'
import { getIncludeAndExcludeTags } from '../Services/MealService'
import { Colors } from '../Themes'
import Style from './Styles/TagFilterScreenStyle'

class TagFilterScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tags: this.props.tags
    }
  }

  clearFilter () {
    let {tags} = this.state

    for (let index in tags) {
      if (tags.hasOwnProperty(index)) {
        tags[index].isSelected = false
        if (tags[index].hasOwnProperty('children')) {
          var children = tags[index].children
          for (let childIndex in children) {
            if (children.hasOwnProperty(childIndex)) {
              children[childIndex].isSelected = false
            }
          }
        }
      }
    }

    this.setState({tags: tags})
  }

  applyFilter () {
    const {includeTags, excludeTags} = getIncludeAndExcludeTags(this.state.tags)
    this.props.applyFilter(includeTags, excludeTags)
  }

  renderTagsWithChildren (tag, index, tagsLength) {
    const childLength = tag.children.length

    return (
      <View style={styles.singleRowContainerStyle} key={index}>
        <View>
          <Text style={Style.parentText}>{tag.name}</Text>
        </View>
        {tag.children.map((child, childIndex) => {
          const marginTop = childIndex !== 0 ? 0 : child.description !== '' ? verticalScale(16) : verticalScale(26)
          const marginBottom = child.description !== null || child.description !== '' ? verticalScale(16) : verticalScale(20)
          const checkBoxBorderColor = child.isSelected ? Colors.bloodOrange : Colors.greyishBrown
          const checkColor = child.isSelected ? Colors.bloodOrange : 'transparent'
          const isIncludeTags = child.filter_type === 'include'
          const checkBoxIcon = isIncludeTags ? 'check' : 'times'
          if (childIndex < 3 || tag.isExpanded) {
            return (
              <View style={[Style.childContainer, {marginBottom: marginBottom, marginTop: marginTop}]} key={childIndex}>
                <View style={{flex: 4}}>
                  <Text style={Style.childText}>{child.name}</Text>
                  {child.description !== null && child.description !== '' &&
                  <Text style={Style.descriptionText}>{child.description}</Text>
                  }
                </View>
                <View style={{flex: 1, alignItems: 'flex-end', paddingRight: scale(10), justifyContent: 'center'}}>
                  <TouchableWithoutFeedback testID={`filter_${index}_${childIndex}`} onPress={() => this.toggleChildCheckBox(index, childIndex, !child.isSelected)}>
                    <View style={{justifyContent: 'center', alignItems: 'center',borderWidth: 1, padding: 2, borderColor: checkBoxBorderColor, borderRadius: 2, height: scale(20), width: scale(20)}}>
                      <FontAwesome name={checkBoxIcon} color={checkColor} />
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            )
          }
        })}
        {childLength > 3 &&
        <View style={{marginBottom: verticalScale(20)}}>
          <TouchableWithoutFeedback onPress={() => this.toggleTagExpandable(index, !tag.isExpanded)}>
            <View>
              <Text style={Style.expandableButton}>{tag.isExpanded ? 'Show Less' : 'Show More'}</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        }
        {index !== tagsLength - 1 &&
        <View style={Style.groupingBorder} />
        }
      </View>
    )
  }

  toggleTagExpandable (index, isExpanded) {
    var {tags} = this.state
    tags[index].isExpanded = isExpanded
    this.setState({tags: tags})
  }

  toggleChildCheckBox (parentIndex, index, isSelected) {
    var {tags} = this.state
    var children = tags[parentIndex].children
    children[index].isSelected = isSelected
    tags.children = children
    tags[parentIndex].isSelected = this.checkSelectedChildrenExist()
    this.setState({tags: tags})
  }

  checkSelectedChildrenExist () {
    const {children} = this.state.tags
    const findIndex = _.findIndex(children, (children) => {
      return children.isSelected === true
    })
    return findIndex > -1
  }

  renderRow (tags) {
    const tagsLength = tags.length
    return tags.map((tag, index) => {
      if (tag.hasOwnProperty('children')) {
        return this.renderTagsWithChildren(tag, index, tagsLength)
      }
    })
  }
  closeButton () {
    return (
      <Button transparent onPress={() => this.props.dismissModal()}>
        <Icon name='md-close' style={{color: Colors.bloodOrange}} />
      </Button>
    )
  }
  resetButton () {
    return (
      <TouchableOpacity testID='resetFilter' onPress={() => this.clearFilter()} style={Style.resetButton}>
        <Text style={Style.resetText} uppercase={false}>Reset</Text>
      </TouchableOpacity>
    )
  }

  render () {
    const {tags} = this.state
    return (
      <Container>
        <NavigationBar
          title='Filters'
          leftSide={() => this.closeButton()}
          rightSide={() => this.resetButton()}
        />
        <Content>
          <View style={styles.taglistViewStyle}>
            {this.renderRow(tags)}
          </View>
        </Content>
        <Footer style={{ paddingHorizontal: scale(20), alignItems: 'center', height: verticalScale(70), backgroundColor: 'white'}}>
          <PrimaryButton
            label='Show My Meals'
            position='center'
            onPressMethod={() => this.applyFilter()}
            buttonStyle={{height: verticalScale(50)}}
            testID='applyFilterButton'
          />
        </Footer>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
  taglistViewStyle: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 20,
    marginRight: 20
  },
  singleRowContainerStyle: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10
  }
})


export default TagFilterScreen
