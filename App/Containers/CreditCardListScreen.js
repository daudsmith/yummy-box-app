import { Container, Content } from 'native-base'
import React, { Component } from 'react'
import { ActivityIndicator, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import Modal from 'react-native-modal'
import RadioButton from 'react-native-radio-button'
import { moderateScale } from 'react-native-size-matters'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import NavigationBar from '../Components/NavigationBar'
import OfflineModal from '../Components/OfflineModal'
import OfflinePage from '../Components/OfflinePage'
import icoMoonConfig from '../Images/SvgIcon/selection.json'
import CustomerCardActions from '../Redux/CustomerCardRedux'
import StartupActions from '../Redux/StartupRedux'
import Colors from '../Themes/Colors'
import Styles from './Styles/CreditCardListScreenStyle'
import { Images } from '../Themes'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

const cardIcons = {
  'VISA': 'visa',
  'MASTER': 'mastercard',
  'AMEX': 'amex',
  'JCB': 'jcb-input',
  'UNKNOWN': Images.unknownCard
}

class CreditCardListScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      customerCard: this.props.customerCard,
      loading: true,
      showConfirmationModal: false,
      selectedCardIdToBeDeleted: null,
      showBlankOfflinePage: !this.props.isConnected && !this.props.hasFetchedStartupData,
      hasFetchedData: false,
      errorMessage: null
    }
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    const {customerCard} = newProps
    this.setState({customerCard, loading: customerCard.loading, errorMessage: customerCard.error})

    if (!newProps.loading && newProps.customerCard.error === null) {
      this.setState({hasFetchedData: true})
    }

    if (!this.props.isConnected && newProps.isConnected) {
      if (!this.props.hasFetchedStartupData) {
        this.props.startupOnCreditCardList()
        this.setState({showBlankOfflinePage: false})
      } else {
        this.props.fetchCards()
      }
    }
  }

  componentDidMount () {
    if (this.props.isConnected) {
      this.props.fetchCards()
    }
  }

  handleSetDefault(card) {
    if(card.is_default === 1) {
      return
    }
    this.props.setDefaultCard(card.id)
  }

  handleCardDeletePress (cardId) {
    {this.setState({showConfirmationModal: true, selectedCardIdToBeDeleted: cardId})}
  }

  renderDeleteConfirmationModal () {
    const {selectedCardIdToBeDeleted} = this.state
    return (
      <View style={Styles.alertBoxContainer}>
        <View>
          <Text style={{fontFamily: 'Rubik-Regular', color: Colors.greyishBrown, textAlign: 'center'}}>Are you sure want to delete this card from your card list?</Text>
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TouchableOpacity
            style={Styles.whiteButton}
            onPress={() => {
              this.props.removeCard(selectedCardIdToBeDeleted)
              this.setState({showConfirmationModal: false, selectedCardIdToBeDeleted: null})
            }}
          >
            <Text style={{color: Colors.bloodOrange, fontFamily: 'Rubik-Regular', fontSize: moderateScale(16)}}>Yes</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Styles.orangeButton}
            onPress={() => this.setState({showConfirmationModal: false, selectedCardIdToBeDeleted: null})}
          >
            <Text style={{color: 'white', fontFamily: 'Rubik-Regular', fontSize: moderateScale(16)}}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderCreditCardList () {
    const {cards} = this.state.customerCard
    return cards.map((card, index) => {
      const isSelected = card.is_default === 1
      const cardIcon = cardIcons[card.type]
      return (
        <View style={Styles.listContainer} key={index}>
          <View style={{flex: 1}}>
            <RadioButton
              animation={'bounceIn'}
              isSelected={isSelected}
              size={moderateScale(10)}
              innerColor={Colors.bloodOrange}
              outerColor={card.is_default ? Colors.bloodOrange : Colors.pinkishGrey}
              onPress={() => this.handleSetDefault(card)}
            />
            {isSelected &&
              <Text style={Styles.defaultText}>Default</Text>
            }
          </View>
          <View style={Styles.listContainerCenterView}>
            <YumboxIcon name={cardIcon} size={20} color={Colors.greyishBrown} style={{marginRight: moderateScale(12)}} />
            <Text style={Styles.cardNumberText}>**** **** **** {card.ends_with}</Text>
          </View>
          <View style={{flex: 1, alignContent: 'center'}}>
            <TouchableWithoutFeedback onPress={() => this.handleCardDeletePress(card.id)}>
            <Ionicons name='ios-trash' size={25} style={{color: Colors.pinkishGrey}} />
            </TouchableWithoutFeedback>
          </View>
        </View>
      )
    })
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const {loading, showConfirmationModal, showBlankOfflinePage} = this.state
    return (
      <Container>
        <NavigationBar leftSide='back' title='My Cards' leftSideNavigation={this.navigationGoBack} />
        <Content contentContainerStyle={{flexGrow: 1}} style={{backgroundColor: 'white', paddingHorizontal: moderateScale(20)}}>
          {!this.props.isConnected && showBlankOfflinePage ?
            <OfflinePage />
          :
            <View>
              {loading ? <ActivityIndicator style={{marginTop: moderateScale(15)}} /> : this.renderCreditCardList()}
              <TouchableOpacity style={{alignItems: 'center', marginTop: moderateScale(16)}} onPress={() => this.props.navigation.navigate('CreditCardAddScreen')}>
                <Text style={Styles.addNewCardButtonText}>+ Add New Card</Text>
              </TouchableOpacity>
            </View>
          }

        </Content>
        <Modal
          backdropColor='white'
          backdropOpacity={0.8}
          isVisible={showConfirmationModal}
          onBackButtonPress={() => this.setState({showConfirmationModal: false})}
        >
          {this.renderDeleteConfirmationModal()}
        </Modal>
        {!showBlankOfflinePage && <OfflineModal isConnected={this.props.isConnected} />}
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    customerCard: state.customerCard,
    isConnected: state.network.isConnected,
    hasFetchedStartupData: state.startup.hasFetchedStartupData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setDefaultCard: (cardId) => dispatch(CustomerCardActions.setDefaultCard(cardId)),
    removeCard: (cardId) => dispatch(CustomerCardActions.removeCard(cardId)),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
    startupOnCreditCardList: () => dispatch(StartupActions.startupOnCreditCardList())
  }
}

export default connect (mapStateToProps, mapDispatchToProps)(CreditCardListScreen)
