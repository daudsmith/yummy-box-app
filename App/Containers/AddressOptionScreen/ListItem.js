import { Icon } from 'native-base'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import RadioForm, { RadioButton, RadioButtonInput } from 'react-native-simple-radio-button'
import { Colors } from '../../Themes'
import Styles from './AddressOptionScreenStyle'

class ListItem extends Component {
  onSelect = (address) => {
    const {
      onSelect,
    } = this.props
    onSelect(address)
  }

  onEdit = (address) => {
    const {
      onEdit,
    } = this.props
    onEdit(address)
  }

  render() {
    const {
      address,
      selected,
      addressType,
      readOnly
    } = this.props

    let pic = address.recipient
      ? address.recipient
      : ''

    return (
      <View
        style={{
          flexDirection: 'row',
          paddingVertical: 12,
          borderBottomColor: Colors.lightGrey,
          borderBottomWidth: 1,
        }}
      >
        <RadioForm
          formHorizontal={true}
          animation={true}
        >
          <RadioButton
            labelHorizontal={true}
            key={address.addressId}
          >
            <RadioButtonInput
              testID='test'
              accessibilityLabel='tst'
              obj={address}
              index={address.addressId}
              isSelected={selected.addressId === address.addressId}
              onPress={() => this.onSelect(address)}
              borderWidth={1}
              buttonInnerColor={Colors.primaryOrange}
              buttonOuterColor={selected.addressId === address.addressId
                ? Colors.primaryOrange
                : Colors.primaryGrey}
              buttonSize={8}
              buttonOuterSize={16}
              buttonStyle={{ marginTop: 2 }}
            />
          </RadioButton>
        </RadioForm>
        <View style={Styles.radioLabel}>
          <TouchableOpacity
            onPress={() => this.onSelect(address)}
          >
            <View>
              <Text
                style={Styles.titleText}>
                {address.display_label}
              </Text>
            </View>
            <View>
              <Text style={Styles.addressText}>
                {address.display_address}
              </Text>
            </View>
            <View>
              <Text style={Styles.addressText}>
                {pic}
              </Text>
            </View>
          </TouchableOpacity>

          {(addressType === 'personal' && !readOnly) && (
            <View
              style={Styles.buttonEdit}
            >
              <TouchableOpacity
                testID="edit"
                accessibilityLabel="edit"
                onPress={() => this.onEdit(address)}
              >
                <View
                  style={Styles.buttonEditWrapper}
                >
                  <Icon
                    name="md-create"
                    style={Styles.buttonEditIcon}
                  />
                  <Text
                    style={Styles.buttonEditText}
                  >
                    Edit
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>

    )
  }
}
ListItem.defaultProps={
  readOnly: false
}

ListItem.propTypes = {
  addressType: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  address: PropTypes.object.isRequired,
  selected: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
}

export default ListItem
