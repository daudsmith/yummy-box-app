import { Icon } from 'native-base'
import * as React from 'react'
import {
  Container,
  Content,
} from 'native-base'
import {
  View,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native'
import { connect } from 'react-redux'
import NavigationBar from '../../Components/NavigationBar'
import Styles from './AddressOptionScreenStyle'
import ListItem from './ListItem'
import BottomButton from '../../Components/BottomButton'
import {
  Colors,
} from '../../Themes'
import CartActions, {
  BaseCart,
  DeliveryAddress,
} from '../../Redux/V2/CartRedux'
import _ from 'lodash'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { AppState } from '../../Redux/CreateStore'
import { User } from '../../Redux/LoginRedux'
import CustomerOrderTypeActions from '../../Redux/CustomerOrderRedux'
import MyMealPlanActions, {
  MyMealPlanDetailSuccess,
} from '../../Redux/MyMealPlanRedux'
import { Address } from '../../Redux/CustomerAddressRedux'
import { StackNavigationProp } from 'react-navigation-stack/lib/typescript/src/vendor/types'
import LastUsedActions, { Location } from '../../Redux/V2/LastUsedRedux'
import ApiKitchen from '../../Services/V2/ApiKitchen'
import GoogleMatrixService from '../../Services/GoogleMatrixService'

const Title = 'Select Address'

export interface AddressOptionProps {
  navigation: NavigationDrawerProp & StackNavigationProp,
  onCheckout: (address) => void,
  onModifySubscription: (keys: string[], value: string[]) => void,
  onModifyMMP: (data: MyMealPlanDetailSuccess) => void,
  cart: BaseCart,
  user: User,
  modifiedMMPDetail: MyMealPlanDetailSuccess,
  addLastUsed: (code: string, location: Location, name: string, address: string) => void,
  updateCartKitchen: (code: string) => void,
}

export interface AddressOptionStates {
  selectedAddress: any,
}

class AddressOptionScreen extends React.Component<AddressOptionProps, AddressOptionStates> {
  constructor(props) {
    super(props)

    const { addresses } = this.props.navigation.state.params
    const selectedAddressIndex = this.getSelectedAddress(addresses)

    let selectedAddress = addresses && addresses.length > 0
      ? addresses[selectedAddressIndex]
      : null

    this.state = {
      selectedAddress,
    }
  }

  getSelectedAddress = (addreses) => {
    let selectedAdressIndex = 0
    const { cartItems } = this.props.cart

    const cartItem = cartItems[0]
    if (cartItem && !_.isEmpty(cartItem.deliveryAddress)) {
      const selectedAddressLat = cartItem.deliveryAddress.latitude
      const selectedAddressLon = cartItem.deliveryAddress.longitude
      selectedAdressIndex = _.findIndex(addreses, (address) => {
        return address.latitude == selectedAddressLat && address.longitude == selectedAddressLon
      })
    }
    return selectedAdressIndex === -1
      ? 0
      : selectedAdressIndex
  }

  selectHandler = (address) => {
    this.setState({
      selectedAddress: address,
    })
  }

  editHandler = (address) => {
    const { navigation } = this.props
    const { createScreenName, mapScreenName, searchScreenName } = navigation.state.params
    
    navigation.navigate({
      routeName: createScreenName
        ? createScreenName
        : 'CoCreateAddress',
      params: {
        selectedAddress: this.formatAddressObject(address),
        selectHandler: this.createAddressHandler,
        searchScreenName: searchScreenName,
        mapScreenName: mapScreenName,
        createScreenName: createScreenName,
      },
      key: 'CreateAddressFromAddressOptionKey',
    })
  }

  formatAddressObject = (item) => {
    return {
      addressId: item.addressId || '',
      latitude: item.latitude || '',
      longitude: item.longitude || '',
      addressLabel: item.label || '',
      addressPinpoint: item.landmark || '',
      addressDetail: item.address || '',
      recipientName: item.name || '',
      recipientNumber: item.phone || '',
      deliveryInstructions: item.note || '',
    }
  }

  addNewAddress = () => {
    const {
      navigation,
    } = this.props
    const { createScreenName, searchScreenName, mapScreenName } = navigation.state.params    
    navigation.navigate({
      routeName: searchScreenName || 'searchLocation',
      params: {
        selectHandler: this.createAddressHandler,
        createScreenName: createScreenName,
        mapScreenName: mapScreenName,
        searchScreenName: searchScreenName
      },
      key: 'CreateAddressFromAddressOptionKey',
    })
  }

  useAnotherLocButtonHandler = () => {
    const {
      navigation,
    } = this.props
    navigation.navigate('SearchLocation', {noSearchLocation: true})
  }
  
  addButton = () => {
    const { addressType } = this.props.navigation.state.params
    if (addressType === 'personal') {
      return (
        <TouchableWithoutFeedback
          testID="newAddress"
          accessibilityLabel="newAddress"
          onPress={this.addNewAddress}
        >
          <View>
            <Icon
              name="ios-add"
              style={{
                color: Colors.primaryTurquoise,
              }}
            />
          </View>
        </TouchableWithoutFeedback>
      )
    }
  }

  useAddressHandler = () => {
    const {
      navigation,
      addLastUsed,
      updateCartKitchen
    } = this.props

    const { selectPinpoint, checkoutScreenName, changeState } = navigation.state.params
    const { selectedAddress } = this.state

    const latitude = selectedAddress.latitude
    const longitude = selectedAddress.longitude
    const name = selectedAddress.label
    const address = selectedAddress.address
    const location = {
      latitude,
      longitude,
    }
    if(changeState == 'checkout'){
      this.changeAddressHandler(selectedAddress)
    }
    ApiKitchen.create()
    .kitchenList(latitude, longitude)
    .then(async (response) => {
      const data = response.data.data
        const destination = data.map(item => {
          return `${item.latitude},${item.longitude}`
        }).join('|')
        if (data) {
          if (data.length > 1) {
            await GoogleMatrixService.checkGoogleMatrix(latitude, longitude, destination).then(index => {
              updateCartKitchen(data[index].code)
              addLastUsed(data[index].code, location, name, address)
            })
          } else {
            updateCartKitchen(data[0].code)
            addLastUsed(data[0].code, location, name, address)
          }
          if(changeState != 'checkout'){
            this.changeAddressHandler(selectedAddress, data[0].code)
          }

          if (!selectPinpoint) {
            // for checkout
            navigation.navigate(checkoutScreenName)
          } else if (selectPinpoint) {
            // for selecting pinpoint 
            navigation.navigate('HomeScreen')
          }      
          else {
            navigation.goBack()
          }  
        }
      }).catch(() => {
        navigation.navigate('SearchLocation', { noSearchLocation: false })
      })
  } 


  changeAddressHandler = (
    address: Address,
    kitchenCode: string = null
  ) => {
    const {
      navigation,
      onModifySubscription,
      onModifyMMP,
      onCheckout,
      modifiedMMPDetail,
    } = this.props
    const {
      changeState,
    } = navigation.state.params

    if (changeState === 'modifySubscription') {
      const keys: string[] = [
        'default_delivery_address',
        'default_delivery_latitude',
        'default_delivery_longitude',
        'default_delivery_note',
        'default_recipient_name',
        'default_recipient_phone',
      ]
      const values: string[] = [
        address.address,
        String(address.latitude),
        String(address.longitude),
        address.note,
        address.name,
        address.phone,
      ]
      onModifySubscription(keys, values)
    } else if (changeState === 'modifyMMP') {
      const { data } = modifiedMMPDetail
      const newData = {
        ...data,
        address: address.address,
        note: address.note,
        name: address.name,
        phone: address.phone,
        longitude: address.longitude,
        latitude: address.latitude,
        landmark: address.landmark || address.label,
        id: modifiedMMPDetail.data.id,
        status: modifiedMMPDetail.data.status,
        kitchen_code: kitchenCode
      }
      onModifyMMP({
        ...modifiedMMPDetail,
        data: newData,
      })
    } else {
      onCheckout(address)
    }
  }

  createAddressHandler = (address) => {
    this.setState({
      selectedAddress: address,
    }, this.useAddressHandler)
  }

  render() {
    const {
      navigation,
    } = this.props
    const {
      addresses,
      addressType,
      showAnotherLocationButton,
      showBackButton,
      selectPinpoint,
      readOnly
    } = navigation.state.params
    const {
      selectedAddress,
    } = this.state

    return (
      <Container>
        <NavigationBar
          leftSide={!selectPinpoint || showBackButton ? 'back' : ''}
          leftSideNavigation={() => this.props.navigation.goBack()}
          title={Title}
          rightSide={!showAnotherLocationButton || !readOnly && this.addButton}
        />
        <Content contentContainerStyle={{ flex: 1 }}>
          <FlatList
            style={Styles.flatListStyle}
            data={addresses}
            keyExtractor={item => { return item.addressId.toString() }}
            extraData={this.state.selectedAddress}
            renderItem={({ item }) => (
              <ListItem
                addressType={addressType}
                address={item}
                selected={selectedAddress}
                onSelect={this.selectHandler}
                onEdit={this.editHandler}
                readOnly={showAnotherLocationButton || readOnly}
              />
            )}
          />
        </Content>

        {showAnotherLocationButton && <BottomButton
          customStyle={{
            borderWidth: 1,
            borderColor: Colors.primaryOrange,
            backgroundColor: Colors.white,
          }}
          customTextStyle={{
            color: Colors.primaryOrange
          }}
          buttonText="Use Another Location"
          pressHandler={this.useAnotherLocButtonHandler}
        />}

        <BottomButton
          buttonText="Use This Address"
          pressHandler={() => this.useAddressHandler()}
          disabled={(addresses.length <= 0)}
        />
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    cart: state.cart,
    user: state.login.user,
    modifiedMMPDetail: state.myMealPlan.modifiedMyMealPlanDetail,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onCheckout: (address: DeliveryAddress) =>
      dispatch(CartActions.updateAddress(address)),
    onModifySubscription: (keys: string[], values: string[]) =>
      dispatch(CustomerOrderTypeActions.updateSubscriptionDefaultData(keys, values)),
    onModifyMMP: (data: MyMealPlanDetailSuccess) =>
      dispatch(MyMealPlanActions.updateModifiedMmp(data)),
    addLastUsed: (code: string, location: Location, name: string, address: string) => dispatch(LastUsedActions.addLastUsed(code, location, name, address)),
    updateCartKitchen: (code: string) => dispatch(CartActions.updateKitchen(code)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddressOptionScreen)
