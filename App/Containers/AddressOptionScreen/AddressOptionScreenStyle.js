import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

const confirmButtonColor = Colors.bloodOrange

export default StyleSheet.create({
  flatListStyle: {
    backgroundColor: 'white',
    paddingHorizontal: scale(20),
    paddingBottom: verticalScale(5),
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: verticalScale(7),
    paddingTop: verticalScale(16),
    paddingBottom: verticalScale(14),
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.lightGrey,
  },
  titleText: {
    fontFamily: 'Rubik-Medium',
    fontSize: scale(16),
    color: Colors.primaryDark,
  },
  addressText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.primaryGrey,
    fontSize: scale(12),
    marginTop: verticalScale(4),
  },
  radioLabel: {
    justifyContent: 'flex-start',
    marginLeft: scale(8),
    flex: 1,
  },
  confirmButton: {
    backgroundColor: confirmButtonColor,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    padding: scale(20),
  },
  confirmButtonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: scale(16),
  },
  bottomBackground: {
    backgroundColor: Colors.white,
    position: 'absolute',
    left: 0,
    right: 0,
    height: verticalScale(100),
    bottom: verticalScale(-50),
  },
  buttonEdit: {
    marginTop: verticalScale(12),
    flexDirection: 'row',
  },
  buttonEditWrapper: {
    flexDirection: 'row',
  },
  buttonEditIcon: {
    color: Colors.primaryTurquoise,
    fontSize: scale(16),
  },
  buttonEditText: {
    color: Colors.primaryTurquoise,
    marginLeft: scale(4),
    fontSize: scale(14),
  },
})
