import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  contentText: {
    fontFamily: 'Rubik-Light',
    color: Colors.greyishBrownThree,
    letterSpacing: 0.1,
    textAlign: 'center'
  },
  swipeButton: {
    flex: 1,
    backgroundColor: Colors.bloodOrange,
    justifyContent: 'center',
    paddingLeft: 10
  },
  notificationItem: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightGrey,
    marginHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#fff',
    width: '100%',
    marginLeft: 0,
    paddingHorizontal: 10
  },
  notificationItemText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12
  },
  notificationItemTextUnread: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12
  },
  notificationTimeText: {
    fontFamily: 'Rubik-Light',
    fontSize: 9,
    marginTop: 3
  }
})
