import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes'

export interface EmailPhoneVerificationInterface {
  registrationBoxContainer: object
  registrationBox: object
  titleContainer: object
  title: object
  description: object
  facebookButtonText: object
  errorMessage: object
  submitButtonView: object,
  altContainer: object
  textAlt: object
}

const Styles: EmailPhoneVerificationInterface = StyleSheet.create({
  registrationBoxContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  registrationBox: {
    backgroundColor: 'white',
    marginHorizontal: 20,
    paddingHorizontal: 30,
    borderRadius: 3,
    shadowColor: 'rgba(223, 223, 223, 0.5)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 6,
    shadowOpacity: 1,
    elevation: 5
  },
  titleContainer: {
    paddingTop: 34,
    paddingBottom: 40
  },
  title: {
    fontFamily: 'Rubik-Bold',
    fontSize: 20,
    color: Colors.greyishBrownTwo
  },
  description: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    fontWeight: '300',
    color: Colors.greyishBrown
  },
  facebookButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: 'white'
  },
  errorMessage: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    lineHeight: 18,
    color: 'red'
  },
  submitButtonView: {
    marginTop: 30
  },
  altContainer: {
    marginBottom: 15,
    alignItems: 'center'
  },
  textAlt: {
    fontSize: 12, 
    color: Colors.primaryGrey
  },
})

export default Styles