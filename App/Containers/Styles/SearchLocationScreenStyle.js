import { StyleSheet } from 'react-native'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  bodyContainer: {
    flex: 11,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  searchBoxContainer: {
    justifyContent: 'center',
  },
  currentLocationImage: {
    height: verticalScale(20),
    width: scale(20),
    marginRight: scale(3),
  },
  historyText: {
    fontFamily: 'Rubik-Medium',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    color: Colors.primaryGrey,
    paddingHorizontal: scale(20),
    paddingVertical: verticalScale(16),
  },
  searchBox: {
    backgroundColor: 'white',
    justifyContent: 'center',
    height: verticalScale(35),
    paddingHorizontal: scale(5),
    borderColor: Colors.lightGrey,
    borderRadius: 4,
    borderWidth: 1,
    width: '100%',
  },
  searchForm: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 0,
    paddingHorizontal: scale(4),
    width: '100%',
  },
  searchBoxTextInput: {
    fontFamily: 'Rubik-Light',
    width: '87%',
    fontSize: scale(14),
    padding: 0,
    marginTop: verticalScale(1),
    color: Colors.primaryDark,
  },
  searchBoxIcon: {
    marginTop: verticalScale(4),
    marginRight: scale(8),
    color: Colors.primaryGrey,
  },
  resultContainer: {
    marginTop: verticalScale(13),
  },
  historyListHeaderTextView: {
    paddingVertical: verticalScale(8),
    alignItems: 'center',
    borderBottomColor: Colors.pinkishGrey,
    borderBottomWidth: 0.5,
  },
  historyListHeaderText: {
    fontFamily: 'Rubik-Medium',
    fontSize: scale(14),
    color: Colors.brownishGrey,
  },
  resultListRow: {
    flexDirection: 'row',
    paddingVertical: verticalScale(20),
    paddingHorizontal: scale(30),
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightGrey,
  },
  currentLocationRow: {
    flexDirection: 'row',
    paddingVertical: verticalScale(20),
    paddingHorizontal: scale(24),
    alignItems: 'center',
  },
  currentLocationText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    color: Colors.primaryTurquoise,
  },
  resultListIconContainer: {
    marginRight: scale(10),
  },
  resultListTextContainer: {
    marginRight: scale(15),
  },
  resultListText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo,
  },
  setDestinationButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.greyishBrownTwo,
    borderRadius: 3,
  },
  setDestinationButtonText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.white,
    fontSize: scale(14),
  },
  animateToCurrentLocationButton: {
    backgroundColor: 'white',
    height: 50,
    width: 50,
    position: 'absolute',
    bottom: verticalScale(20),
    right: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: Colors.pinkishGrey,
    shadowOffset: {
      height: 5,
      width: 5,
    },
    elevation: 2,
    borderRadius: 5,
  },
  formControl: {
    flex: 1,
    marginTop: verticalScale(16),
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightDark,
    marginHorizontal: scale(20),
  },
  formControlWithButton: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center'
  },
  warningAddressLabelContainer: {
    marginTop: scale(3),
    marginBottom: scale(5),
    marginHorizontal: scale(20),
  },
  textWarningAddressLabel: {
    fontSize: scale(12),
    color: Colors.primaryGrey,
  },
  labelContainer: {
    flexDirection: 'row',
  },
  label: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.primaryDark,
  },
  inputForm: {
    marginTop: verticalScale(6),
    padding: 0,
    marginBottom: verticalScale(8),
    color: Colors.primaryDark,
  },
  remark: {
    fontSize: scale(12),
    color: Colors.primaryError,
    marginLeft: scale(1),
    marginTop: -4,
  },
  helper: {
    fontSize: 12,
    color: Colors.primaryGrey,
    paddingVertical: verticalScale(4),
  },
  divider: {
    height: verticalScale(8),
    backgroundColor: Colors.lightGrey,
    marginTop: verticalScale(16),
  },
  inputWithButton: {
    flex: 3,
  },
  changeButtonText: {
    fontSize: 14,
    color: Colors.primaryTurquoise,
    marginBottom: verticalScale(4),
  },
  changeButtonIcon: {
    height: verticalScale(16),
    width: scale(16),
    marginRight: scale(2),
    marginTop: verticalScale(2),
    marginBottom: verticalScale(4),
  },
  textAreaHelper: {
    flex: 1,
    textAlign: 'right',
    fontSize: 12,
    fontFamily: 'Rubik-Regular',
    marginTop: 4,
    color: Colors.primaryGrey,
    marginHorizontal: scale(20),
  },
  leftButtonStyle: {
    paddingLeft: 5,
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5
  },
  formControlError: {
    flex: 1,
    marginTop: verticalScale(16),
    borderBottomWidth: 1,
    borderBottomColor: 'red',
    marginHorizontal: scale(20),
  },
  labelError: {
    marginTop: 4,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: 'red',
    marginHorizontal: scale(20),
  },
  coverageAreaText: {
    fontFamily: 'Rubik-Regular',
    justifyContent: 'center',
    textAlign: 'center',
    color: Colors.primaryGrey,
    fontSize: 12,
    paddingHorizontal: scale(20),
    paddingVertical: verticalScale(20),
  },
  warningBox: {
    flexDirection: 'row',
    borderRadius: moderateScale(4),
    paddingVertical: verticalScale(8),
    paddingHorizontal: scale(12),
    margin: moderateScale(20),
    backgroundColor: Colors.lightWarning2
  },
})
