import { StyleSheet, Dimensions } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

const {width, height} = Dimensions.get('window')

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  welcomeScreenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.offWhite
  },
  slideContainer: {
    flex: 1,
    width: width,
  },
  imageGrid: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  image: {
    height: height * 0.25,
    width: height * 0.25
  },
  textGrid: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 30
  },
  title: {
    paddingTop: 17,
    fontSize: 20,
    color: Colors.black,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    textAlign: 'center'
  },
  description: {
    paddingTop: 5,
    fontSize: 14,
    color: Colors.black,
    marginLeft: width * 0.1,
    marginRight: width * 0.1,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular'
  },
  dot: {
    height: 7,
    width: 7,
    margin: 4,
    borderRadius: 3.5,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: Colors.bloodOrange
  },
  dotContainer: {
    position: 'absolute',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    bottom: 63
  }
})
