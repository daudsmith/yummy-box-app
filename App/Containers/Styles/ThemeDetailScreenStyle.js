import { Dimensions, StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import { ApplicationStyles, Colors } from '../../Themes'

const {width} = Dimensions.get('window')
const imageWidth = width - (2 * 15)
const imageHeight = (344/688) * imageWidth

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  activityIndicatorContainer: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  descriptionSectionContainer: {
    marginTop: 20,
    marginHorizontal: 15,
    borderRadius: 3
  },
  themeImage: {
    width: imageWidth,
    height: imageHeight,
    borderRadius: 3
  },
  descriptionText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    marginTop: 12,
    lineHeight: 17,
    color: Colors.greyishBrown,
    textAlign: 'justify'
  },
  viewMore: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    color: Colors.grennBlue,
    backgroundColor: 'transparent',
    textDecorationLine: 'underline'
  },
  sectionTitleText: {
    fontFamily: 'Rubik-Medium',
    fontSize: 16,
    color: Colors.greyishBrownTwo,
    marginLeft: 15,
    marginBottom: 7
  },
  gradientBox: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: '100%'
  },
  packageContainer: {
    marginHorizontal: moderateScale(15)
  },
  packageButton: {
    borderWidth: 1,
    borderColor: Colors.pinkishGrey,
    borderRadius: 3,
    paddingVertical: moderateScale(15),
    paddingHorizontal: moderateScale(20),
    marginBottom: moderateScale(9)
  },
  packageNameText: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    color: Colors.greyishBrownTwo,
    fontWeight: '500'
  },
  basePriceText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(10),
    textDecorationLine: 'line-through',
    color: Colors.greyishBrownTwo
  },
  sellPriceText: {
    fontSize: moderateScale(16),
    fontFamily: 'Rubik-Regular',
    flex: 1,
    textAlign: 'left'
  },
  tagLineText: {
    flex: 1,
    fontSize: moderateScale(14),
    fontWeight: '300',
    fontFamily: 'Rubik-Regular',
    textAlign: 'right',
    color: Colors.bloodOrange
  },
  proceedButton: {
    flexDirection: 'row',
    height: moderateScale(55),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.bloodOrange,
    paddingHorizontal: moderateScale(20)
  },
  proceedButtonText: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    color: 'white'
  },
  mealDetailModalBackground: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255, 0.8)',
    alignItems: 'center'
  }
})
