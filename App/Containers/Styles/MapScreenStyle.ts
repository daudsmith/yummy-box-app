import { StyleSheet, ViewStyle, TextStyle } from 'react-native'
import { Colors } from '../../Themes'
import { scale, verticalScale } from 'react-native-size-matters'

export interface MapScreenInterface {
  labelContainer: ViewStyle,
  label: TextStyle,
  inputForm: ViewStyle,
  remark: TextStyle,
  card: ViewStyle,
  bottomButton: ViewStyle,
  buttonCenterPosition: ViewStyle,
  text: TextStyle,
}

const Styles: MapScreenInterface = StyleSheet.create({
  card: {
    marginTop: -5,
    flex: 3,
    borderTopLeftRadius: 8, 
    borderTopRightRadius: 8, 
    borderTopWidth: 1, 
    borderLeftWidth: 1, 
    borderRightWidth: 1,
    borderColor: 'transparent',
    backgroundColor: Colors.primaryWhite,
    shadowOpacity: 0.2,
    shadowRadius: 0,
    shadowColor: Colors.primaryDark,
    shadowOffset: { width: 0, height: 2 },
    elevation: 5,
    padding: 20,
  },
  labelContainer: {
    flexDirection: 'row',
  },
  label: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.primaryDark,
    marginBottom: 4,
  },
  inputForm: {
    marginTop: verticalScale(6),
    padding: 0,
    marginBottom: verticalScale(8),
    color: Colors.primaryDark,
  },
  remark: {
    fontSize: scale(12),
    color: Colors.primaryError,
    marginLeft: scale(1),
    marginTop: -4,
  },
  helper: {
    fontSize: 12,
    color: Colors.primaryGrey,
    paddingVertical: verticalScale(4),
  },
  bottomButton: { 
    marginHorizontal: 0, 
    marginTop: scale(20), 
    height: scale(48),
  },
  buttonCenterPosition: {
    width: 40, 
    height: 40, 
    backgroundColor: Colors.primaryWhite, 
    borderRadius: 50,
    shadowOpacity: 0.2,
    shadowRadius: 0,
    shadowColor: Colors.primaryDark,
    shadowOffset: { width: 0, height: 2 },
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontFamily: 'Rubik-Regular', 
    color: Colors.primaryGrey, 
    fontSize: 12
  }
})

export default Styles
