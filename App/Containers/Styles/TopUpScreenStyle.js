import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { ApplicationStyles } from '../../Themes/'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  scrollViewContainer: {
    backgroundColor: '#fff',
    marginHorizontal: scale(15),
    flex: 1,
  },
  walletBox: {
    marginTop: verticalScale(15),
  },
  balanceBox: {
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  currencyText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(24),
    color: Colors.brownishGrey,
    marginLeft: scale(10)
  },
  label: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(12)
  },
  balanceLabelText: {
    paddingTop: verticalScale(5)
  },
  topUpMethodContainer: {
    paddingVertical: scale(23)
  },
  topUpMethodIntroduction: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(12),
    marginBottom: verticalScale(10),
    lineHeight: verticalScale(18),
  },
  methodHeader: {
    paddingVertical: verticalScale(20),
    borderWidth: 1,
    borderColor: Colors.frost,
    paddingLeft: scale(10),
    paddingRight: scale(20),
    borderTopRightRadius: 3,
    borderTopLeftRadius: 3,
    marginTop: verticalScale(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  methodContent: {
    borderWidth: 1,
    borderColor: Colors.frost,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    paddingVertical: 0,
    paddingHorizontal: 0,
    borderTopWidth: 0
  },
  topUpLabel: {
    fontFamily: 'Rubik-Medium',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo
  },
  textInputToken: {
    color: Colors.brownishGrey,
    borderWidth: 0,
    flex: 1,
    paddingLeft: 0,
    paddingTop: verticalScale(8),
    height: verticalScale(38),
    paddingBottom: 0,
    fontSize: scale(18),
    fontFamily: 'Rubik-Regular'
  },
  creditText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.warmGrey,
    marginBottom: verticalScale(8),
  }
})
