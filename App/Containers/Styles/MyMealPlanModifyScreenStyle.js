import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  resetText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    color: Colors.bloodOrange,
  },
  resetButton: {
    marginRight: 5,
    backgroundColor: 'transparent',
  },
  itemListSectionContainer: {
    marginTop: moderateScale(17),
    paddingBottom: moderateScale(24),
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.lightGrey,
  },
  deliveryDateText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(18),
    color: Colors.greyishBrown,
    marginRight: moderateScale(10),
  },
  selectDeliveryTimeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: moderateScale(22),
    borderStyle: 'solid',
  },
  deliverySectionText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.greyishBrownThree,
  },
  paymentMethodContainer: {
    flex: 1,
    marginTop: moderateScale(25),
    marginBottom: moderateScale(40),
    backgroundColor: Colors.lightGrey2,
    borderRadius: 3,
    paddingTop: moderateScale(24),
    paddingHorizontal: moderateScale(18),
    paddingBottom: moderateScale(27),
  },
  paymentMethodTypeText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownThree,
  },
  paymentMethodText: {
    fontFamily: 'Rubik-Bold',
    fontWeight: '500',
    color: Colors.greyishBrownTwo,
  },
  cardNumberText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.greyishBrownThree,
  },
  confirmButton: {
    height: moderateScale(55),
    backgroundColor: Colors.bloodOrange,
    alignItems: 'center',
    justifyContent: 'center',
  },
  confirmButtonText: {
    fontFamily: 'Rubik-Regular',
    color: 'white',
    fontSize: moderateScale(16),
  },
})
