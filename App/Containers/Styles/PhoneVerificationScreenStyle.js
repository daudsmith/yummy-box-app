import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  buttonContainer: {
    marginTop: 25,
  },
  numericInputContainer: {
    marginBottom: 25
  },
  form: {
    backgroundColor: 'white',
    marginTop: 0
  },
  verificationCodeView: {
    alignItems:'center',
    flex: 1,
    marginHorizontal: 20,
    marginTop: 18,
    flexDirection: 'row'
  },
  resendCodeView: {
    marginTop: 15,
  },
  resendCodeText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    color: Colors.pinkishGrey,
    textAlign: 'center',
    textDecorationLine: 'underline'
  },
  confirmButtonView: {
    marginTop: 24
  },
  phoneInput: {
    marginVertical: 15,
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 5,
    borderBottomColor: Colors.greyishBrown,
    borderBottomWidth: 1,
    fontFamily: 'Rubik-Regular'
  },
  descriptionText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    lineHeight: 18,
    color: Colors.greyishBrown
  },
  mobileNumberText: {
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    color: Colors.greyishBrownTwo,
    marginTop: 16,
    marginBottom: -12
  }
})