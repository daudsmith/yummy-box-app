import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes'
import { scale } from 'react-native-size-matters'

export interface LoginScreenInterface {
  forgetPasswordButton: object,
  forgetPasswordText: object,
  errorText: object,
  altContainer: object
  textAlt: object
}

const Styles: LoginScreenInterface = StyleSheet.create({
  forgetPasswordButton: {
    alignItems: 'flex-end',
    backgroundColor: 'transparent'
  },
  forgetPasswordText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.primaryOrange,
  },
  errorText: {
    fontSize: 12,
    color: 'red',
    fontFamily: 'Rubik-Regular',
    lineHeight: 15
  },
  altContainer: {
    marginBottom: 15,
    alignItems: 'center'
  },
  textAlt: {
    fontSize: 12, 
    color: Colors.primaryGrey
  },
  
})

export default Styles
