import { ImageStyle, StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export interface AccountScreenStyleInterface {
  listButton: ViewStyle,
  listButtonText: TextStyle,
  customerNameText: TextStyle,
  phoneNumberText: TextStyle,
  noPhoneNumberText: TextStyle,
  emailText: TextStyle,
  yumCreditText: TextStyle,
  avatarContainer: ViewStyle,
  avatar: ImageStyle,
  changePictureText: TextStyle,
  logoutButtonContainer: ViewStyle,
  logoutText: TextStyle,
  arrow: ViewStyle,
}

export default StyleSheet.create<AccountScreenStyleInterface>({
  listButton: {
    flexDirection: 'row',
    paddingVertical: verticalScale(18),
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 1,
  },
  listButtonText: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo,
  },
  customerNameText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    color: Colors.greyishBrownTwo,
    marginBottom: verticalScale(5),
  },
  phoneNumberText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.warmGrey,
    fontWeight: '300',
    marginBottom: verticalScale(5),
  },
  noPhoneNumberText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.green,
    fontWeight: '300',
    marginBottom: verticalScale(5),
  },
  emailText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.warmGrey,
    fontWeight: '300',
    marginBottom: verticalScale(10),
  },
  yumCreditText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.greyishBrownTwo,
    marginHorizontal: scale(8),
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: scale(66),
    height: scale(66),
  },
  changePictureText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.green,
    fontSize: scale(10),
    textAlign: 'center',
  },
  logoutButtonContainer: {
    backgroundColor: 'white',
    marginHorizontal: scale(20),
    marginBottom: verticalScale(5),
    elevation: 0,
    height: verticalScale(55),
  },
  logoutText: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    color: Colors.pinkishOrange,
  },
  arrow: {
    paddingLeft: scale(15),
  },
})
