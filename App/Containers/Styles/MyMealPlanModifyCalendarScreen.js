import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  calendarContainer: {
    marginTop: moderateScale(18),
    marginHorizontal: moderateScale(14)
  },
  calendarDescriptionContainer: {
    marginTop: moderateScale(32),
    marginHorizontal: moderateScale(10)
  },
  calendarDescriptionText: {
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    fontWeight: '300',
    color: Colors.pinkishGrey,
    fontSize: moderateScale(10)
  },
  footerButton: {
    backgroundColor: Colors.bloodOrange,
    height: moderateScale(55),
    alignItems: 'center',
    justifyContent: 'center'
  },
  footerButtonText: {
    fontFamily: 'Rubik-Regular',
    color: 'white',
    fontSize: moderateScale(16)
  },
  alertBoxText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: 'normal',
    textAlign: 'center',
    color: Colors.greyishBrown
  },
  alertBoxTextBold: {
    fontFamily: 'Rubik-Light',
    color: Colors.panther,
    fontWeight: '400'
  }
})
