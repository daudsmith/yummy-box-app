import { ImageStyle, StyleSheet, ViewStyle, TextStyle } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { Colors } from '../../Themes'

export interface IntroOneStylesInterface {
  container: ViewStyle,
  backgroundImage: ImageStyle,
  form: ViewStyle,
  logo: ViewStyle,
  text: TextStyle,
  loginButton: ViewStyle,
  guestButton: ViewStyle,
}

const styles: IntroOneStylesInterface = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  form: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'space-between',
    paddingTop: 40,
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  logo: {
    alignItems: 'flex-end',
  },
  text: {
    color: 'white',
    fontSize: scale(39),
    fontFamily: 'DMSans-Bold',
    marginBottom: 30,
  },
  loginButton: {
    backgroundColor: Colors.white,
    paddingVertical: verticalScale(15),
    borderRadius: scale(6),
    borderWidth: 1,
    borderColor: Colors.primaryOrange,
  },
  guestButton: {
    backgroundColor: Colors.primaryOrange,
    paddingVertical: verticalScale(15),
    marginTop: verticalScale(5),
    borderRadius: scale(6),
  }
})

export default styles
