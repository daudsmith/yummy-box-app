import { StyleSheet } from 'react-native'
import { scale } from 'react-native-size-matters'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  singleText: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(18)
  },
  groupingBorder: {
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.pinkishGrey
  },
  parentText: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(18)
  },
  childContainer: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  childText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrown,
    fontSize: scale(18),
    fontWeight: '300'
  },
  descriptionText: {
    fontSize: scale(14),
    fontFamily: 'Rubik-Regular',
    fontWeight: '300',
    color: Colors.brownishGrey
  },
  expandableButton: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    fontWeight: '300',
    color: Colors.green
  },
  resetText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.bloodOrange,
    fontSize: scale(14)
  },
  resetButton: {
    marginRight: scale(10),
    backgroundColor: 'transparent'
  }
})
