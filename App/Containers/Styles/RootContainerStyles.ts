import { StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { Colors, Fonts, Metrics } from '../../Themes/'

export interface RootContainerStylesInterface {
  applicationView: ViewStyle,
  container: ViewStyle,
  welcome: TextStyle,
  myImage: ViewStyle,
}

const styles: RootContainerStylesInterface = StyleSheet.create({
  applicationView: {
    flex: 1
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.background
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    margin: Metrics.baseMargin
  },
  myImage: {
    width: 200,
    height: 200,
    alignSelf: 'center'
  }
})

export default styles
