import { StyleSheet } from 'react-native'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  content: {
    backgroundColor: 'white',
    marginTop: verticalScale(7),
    paddingHorizontal: scale(20),
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: verticalScale(15),
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.lightGrey,
  },
  defaultText: {
    color: Colors.bloodOrange,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(10),
    textAlign: 'right',
    marginTop: verticalScale(4),
  },
  optionItemText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownTwo,
    fontSize: scale(14),
  },
  viewArrow: {
    flex: 1,
    alignItems: 'flex-end',
  },
})
