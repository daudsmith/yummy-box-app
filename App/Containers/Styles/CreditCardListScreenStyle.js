import { Dimensions, StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

const {height} = Dimensions.get('window')

export default StyleSheet.create({
  alertBoxContainer: {
    backgroundColor: 'white',
    height: moderateScale(145),
    marginHorizontal: moderateScale(30),
    paddingHorizontal: moderateScale(30),
    paddingVertical: moderateScale(24),
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1
  },
  whiteButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: moderateScale(42),
    marginTop: moderateScale(15),
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Colors.bloodOrange,
    borderRadius: 5,
    marginRight: moderateScale(2.5)
  },
  orangeButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.bloodOrange,
    height: moderateScale(42),
    marginTop: moderateScale(15),
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Colors.bloodOrange,
    borderRadius: 5,
    marginLeft: moderateScale(2.5)
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: moderateScale(18),
    paddingBottom: moderateScale(6),
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.lightGreyX
  },
  defaultText: {
    color: Colors.bloodOrange,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(10),
    textAlign: 'center',
    marginTop: moderateScale(4)
  },
  listContainerCenterView: {
    flex: 6,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  cardNumberText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownTwo,
    fontSize: moderateScale(14)
  },
  addNewCardButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.bloodOrange
  }
})
