import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  calendarInstructionContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: verticalScale(11),
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.pinkishGrey
  },
  calendarInstructionText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    fontWeight: '500',
    color: Colors.greyishBrownTwo
  },
  tapADateInstructionContainer: {
    paddingVertical: verticalScale(24),
    marginHorizontal: scale(22),
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.pinkishGrey
  },
  tapADateInstructionText: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(10),
    color: Colors.greyishBrownTwo,
    textAlign: 'center',
    lineHeight: verticalScale(16)
  },
  selectedItemContainer: {
    borderTopColor: '#f9fafc',
    borderTopWidth: verticalScale(16),
    paddingVertical: verticalScale(16),
    paddingHorizontal: scale(20),
  },
  mealListPackageTitleContainer: {
    paddingTop: verticalScale(15),
    marginLeft: scale(15),
    paddingBottom: verticalScale(11),
  },
  mealListPackageTitleText: {
    fontFamily: 'Rubik-Medium',
    color: Colors.primaryDark,
    fontSize: scale(16),
  },
  mealListSliderContainer: {
    marginBottom: verticalScale(31)
  },
  mealEmptyText: {
    marginBottom: verticalScale(31),
    paddingBottom: verticalScale(140),
    fontSize: scale(12),
    color: Colors.primaryGrey,
    fontFamily: 'Rubik-Regular',
    marginTop: verticalScale(16),
  },
  nextButtonContainer: {
    flexDirection: 'row',
    backgroundColor: Colors.bloodOrange,
    alignItems: 'center',
    justifyContent: 'center',
    height: verticalScale(55),
    paddingHorizontal: scale(20)
  },
  footerButtonText: {
    color: 'white',
    fontSize: scale(16),
    fontFamily: 'Rubik-Regular',
  },
  proceedToPaymentButton: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.primaryOrange,
    alignItems: 'center',
    justifyContent: 'center',
    height: verticalScale(55),
  },

  mealDetailModalBackground: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255, 0.8)',
    alignItems: 'center'
  },
})
