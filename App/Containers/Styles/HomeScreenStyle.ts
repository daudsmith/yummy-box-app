import {
  Dimensions,
  ImageStyle,
  StyleSheet,
  TextStyle,
  ViewStyle,
} from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'
import { scale, verticalScale } from 'react-native-size-matters'
import { ScreenStylesInterface } from '../../Themes/ApplicationStyles'

const {width} = Dimensions.get('window')
const corporateImageWidth = ((width-40))
const corporateImageHeight = ((344 / 688) * corporateImageWidth)

export interface HomeScreenStyleInterface {
  slider: ViewStyle,
  textBox: ViewStyle,
  text: TextStyle,
  corporateThemeImage: ImageStyle,
  corporateThemeSection: ViewStyle,
}

const styles: ScreenStylesInterface & HomeScreenStyleInterface = StyleSheet.create(
{
  ...ApplicationStyles.screen,
  slider: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBox: {
    flexDirection: 'row',
    height: verticalScale(40),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    fontSize: scale(16),
    fontFamily: 'Rubik-Medium',
    color: Colors.darkBlue,
  },
  corporateThemeImage: {
    width: corporateImageWidth,
    height: corporateImageHeight,
    borderRadius: 3
  },
  corporateThemeSection: {
    justifyContent: 'center',
    marginBottom: verticalScale(8)
  },
})

export default styles
