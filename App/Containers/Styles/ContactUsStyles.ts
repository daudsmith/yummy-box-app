import { ImageStyle, StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { Colors } from '../../Themes/'

export interface ContactUsStylesInterface {
  phoneText: TextStyle,
  waText: TextStyle,
  waIcon: ImageStyle,
  button: ViewStyle,
  buttonContainer: ViewStyle,
  image: ImageStyle,
  getInTouch: TextStyle,
  phoneInformationContainer: ViewStyle,
  sendEmailContainer: ViewStyle,
  textBoxContainer: ViewStyle,
  textBoxStyle: TextStyle,
  textInputEmail: ViewStyle,
  sendEmailButton: ViewStyle
  sendText: TextStyle,
  emailText: TextStyle,
  emailAddress: TextStyle,
  supportText: TextStyle,
}

export default StyleSheet.create<ContactUsStylesInterface>({
  phoneText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    marginLeft: scale(10),
    lineHeight: verticalScale(20),
    color: 'rgb(0, 115, 255)',
    alignItems: 'center'
  },
  waText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    marginLeft: scale(4),
    lineHeight: verticalScale(20),
    color: Colors.primaryGrey,
    alignItems: 'center'
  },
  waIcon: {
    width: scale(20),
    height: verticalScale(20),
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: verticalScale(5),
    flexDirection: 'row'
  },
  buttonContainer: {
    marginTop: verticalScale(14),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: 145.5,
    height: 139.4,
    marginTop: 138.9
  },
  getInTouch: {
    fontFamily: 'Rubik-Medium',
    fontSize: scale(16),
    lineHeight: verticalScale(24),
    color: Colors.primaryDark,
    marginBottom: verticalScale(12)
  },
  phoneInformationContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: verticalScale(12),
  },
  sendEmailContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: verticalScale(12),
  },
  textBoxContainer: {
    marginBottom: verticalScale(32),
  },
  textBoxStyle: {
    fontSize: scale(12),
    fontFamily: 'Rubik-Regular',
    color: Colors.primaryGrey,
    lineHeight: verticalScale(16),
  },
  textInputEmail: {
    width: scale(215),
    fontSize: scale(14),
    fontFamily: 'Rubik-Regular',
    borderBottomWidth: 1,
    color: Colors.brownishGrey,
    backgroundColor: 'transparent',
    bottom: verticalScale(5),
    paddingBottom: verticalScale(3),
    lineHeight: verticalScale(20)
  },
  sendEmailButton: {
    marginLeft: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
    height: verticalScale(35),
    width: scale(60),
    backgroundColor: 'rgb(216, 216, 216)',
    borderRadius: 3
  },
  sendText: {
    fontSize: 16,
    fontFamily: 'Rubik-Regular',
    color: '#fff'
  },
  emailText: {
    fontSize: 11,
    fontFamily: 'Rubik-Light',
    color: Colors.warmGrey
  },
  emailAddress: {
    color: 'rgb(0,115,255)',
    textDecorationLine: 'underline'
  },
  supportText: {
    color: Colors.primaryDark,
    fontSize: scale(16),
    lineHeight: verticalScale(24),
    fontFamily: 'Rubik-Medium'
  },
})
