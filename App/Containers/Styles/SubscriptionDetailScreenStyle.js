import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'
import {
  scale,
  verticalScale,
  moderateScale,
} from 'react-native-size-matters'

export default StyleSheet.create({
  headerContainer: {
    marginTop: verticalScale(21),
    marginHorizontal: scale(15)
  },
  headerThemeNameText: {
    fontFamily: 'Rubik-Medium',
    fontSize: moderateScale(16),
    color: Colors.greyishBrownTwo,
    paddingBottom: verticalScale(13),
  },
  headerInformationContainer: {
    flexDirection: 'row',
    marginBottom: verticalScale(5),
    paddingBottom: verticalScale(12),
  },
  headerInformationKeyText: {
    flex: 1,
    fontFamily: 'Rubik-Light',
    fontSize: moderateScale(14),
    color: Colors.greyishBrown
  },
  headerInformationValueText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.greyishBrown
  },
  mealListContainer: {
    marginTop: verticalScale(10),
  },
  mealInformationContainer: {
    marginLeft: scale(15),
    alignItems:'center'
  },
  mealInformationText: {
    fontFamily: 'Rubik-Light',
    fontSize: moderateScale(12),
    color: Colors.warmGrey
  },
  divider: {
    height: verticalScale(16),
    backgroundColor: Colors.lightGrey,
  },
  headerTextLine: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightGrey,
    marginBottom: verticalScale(5)
  },
  containerFooterButton: {
    height: verticalScale(55),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(15),
    justifyContent: 'center',
    flex: 1
  },
  textFooterButton: {
    fontFamily: 'Rubik-Regular',
    color: 'white',
    fontSize: moderateScale(16)
  }
})
