import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  addressDetailLabel: {
    fontFamily: 'Rubik-Regular',
    color: Colors.greyishBrownThree,
    fontWeight: 'normal',
    fontSize: scale(14)
  },
  dateText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(18),
    color: Colors.greyishBrownThree
  },
  checkBoxContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: verticalScale(20),
    marginBottom: verticalScale(33),
    marginHorizontal: scale(20)
  },
  checkBox: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    padding: 2,
    marginRight: scale(13),
    height: scale(16),
    width: scale(16)
  },
  checkBoxText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.warmGreyTwo
  },
  proceedToPaymentContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(20),
    height: verticalScale(55)
  },
  proceedToPaymentText: {
    color: 'white',
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    textAlign:'center'
  },
  itemRowLineContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: verticalScale(16),
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 1,
  },
  itemNameText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    fontWeight: '500',
    color: Colors.greyishBrownTwo
  },
  priceText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.primaryGrey,
    fontSize: scale(14),
    fontWeight: 'normal'
  },
  themeStartDateText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.warmGreyTwo,
    fontWeight: '300',
    fontSize: scale(12)
  },
  alertBoxContainer: {
    backgroundColor: 'white',
    marginHorizontal: scale(30),
    paddingHorizontal: scale(30),
    borderRadius: 5,
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: scale(2),
      height: scale(2)
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1
  },
  alertModalText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    fontWeight: 'normal',
    textAlign: 'center',
    color: Colors.greyishBrown
  },
  coverageAreaAlertBox: {
    height: verticalScale(179),
    paddingTop: verticalScale(22),
    paddingBottom: verticalScale(28),
  },
  coverageAreaAlertButton: {
    marginHorizontal: scale(60),
    marginTop: verticalScale(10),
  },
  removeMultipleAddressAlertBox: {
    height: verticalScale(212),
    paddingVertical: verticalScale(37),
  },
  removeMultipleAddressButton: {
    flex: 1,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Colors.bloodOrange,
    height: verticalScale(42),
    marginHorizontal: scale(2.5),
    borderRadius: 5,
    justifyContent: 'center'
  },
  removeMultipleAddressButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    fontWeight: 'normal',
    textAlign: 'center',
  },
  stockText: {
    fontSize: scale(11),
    color: Colors.primaryWarning,
    fontStyle: 'italic',
  },
  invalidText: {
    fontSize: scale(11),
    color: Colors.primaryGrey,
    fontStyle: 'italic',
  },
})
