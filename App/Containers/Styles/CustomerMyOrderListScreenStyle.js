import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  listItem: {
    borderBottomWidth: 1/2,
    borderBottomColor: '#d8d8d8',
    paddingBottom: 10,
    paddingHorizontal: 20,
    backgroundColor: 'white'
  },
  listTitle: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14
  },
  descriptionText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    color: Colors.greyishBrown
  },
  orderStatusText: {
    fontFamily: 'Rubik-Medium',
    color: Colors.greyishBrownTwo,
    fontSize: 14,
    textAlign: 'right'
  },
  priceText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.greyishBrown,
    marginLeft: 3,
    textAlign: 'right'
  },
  arrow: {
    paddingLeft: 15,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 20,
    paddingTop: 19,
    paddingBottom: 15
  },
  descriptionText14: {
    fontFamily: 'Rubik-Light',
    fontSize: 14,
    color: Colors.greyishBrown
  },
})
