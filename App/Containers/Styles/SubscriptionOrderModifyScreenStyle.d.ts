import { TextStyle } from 'react-native'
interface styleObject {
  calendarContainer: TextStyle
  subscriptionProfileContainer: TextStyle
  title: TextStyle
  themeName: TextStyle
  calendarSection: TextStyle
  calendarTitleText: TextStyle
  startDateText: TextStyle
  repeatPeriodContainer: TextStyle
  repeatPeriodTitleText: TextStyle
  deliveryDetailsContainer: TextStyle
  paymentSectionContainer: TextStyle
  resetText: TextStyle
  resetButton: TextStyle
}
declare const Styles: styleObject

export default Styles