import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  activityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  mealDeliveryDateContainer: {
    flexDirection: 'row',
    marginTop: 10
  },
  deliveryDateLabelText: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: Colors.greyishBrown
  },
  deliveryDateValueText: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: Colors.greyishBrown
  },
  deliveryMealContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.pinkishGrey,
    paddingBottom: 18,
    marginBottom: 10
  },
  deliveryStatusText: {
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    fontSize: 14,
    color: Colors.greyishBrown
  },
  deliveryItemInfoText: {
    marginTop: 4,
    fontFamily: 'Rubik-Regular',
    fontWeight: '300',
    fontSize: 12,
    color: Colors.greyishBrown
  },
  deliveryTotalContainer: {
    flexDirection: 'row',
    marginTop: 18
  },
  deliveryTotalText: {
    marginLeft: 4,
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.greyishBrown
  },
  rightNavigationButtonLeftIconContainer: {
    borderRightWidth: 1,
    justifyContent: 'center',
    borderRightColor: '#979797',
    paddingRight: 6
  },
  rightNavigationButtonRightIconContainer: {
    marginLeft: 6,
    paddingRight: 10,
    justifyContent: 'center'
  },
  messageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  messageText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.pinkishGrey,
    fontSize: moderateScale(14)
  },
  calendarInfoContainer: {
    alignItems: 'center',
    paddingVertical: moderateScale(18),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Colors.pinkishGrey,
    marginBottom: moderateScale(14)
  },
  calendarInformationText: {
    fontFamily: 'Rubik-Light',
    fontSize: moderateScale(10),
    color: Colors.greyishBrown
  }
})
