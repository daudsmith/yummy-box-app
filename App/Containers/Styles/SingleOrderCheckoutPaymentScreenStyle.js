import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingHorizontal: moderateScale(20)
  },
  couponSectionContainer: {
    flexDirection: 'row',
    marginTop: 23,
    alignItems: 'flex-end'
  },
  couponTextInputBox: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    paddingBottom: 5
  },
  couponTextInput: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    padding: 0,
    color: Colors.primaryDark,
  },
  couponIcon: {
    position: 'absolute',
    right: 0,
    bottom: moderateScale(6)
  },
  couponButton: {
    width: 67,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: Colors.grennBlue,
    backgroundColor: Colors.grennBlue,
    marginLeft: 40
  },
  couponButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.white
  },
  couponMessageText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    marginTop: 5
  },
  detailBox: {
    backgroundColor: Colors.iconBorder,
    padding: moderateScale(17),
    marginTop: 12
  },
  detailTextGrey: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    marginTop: moderateScale(8),
    color: Colors.brownishGrey
  },
  detailTextGreen: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    marginTop: moderateScale(8),
    color: Colors.grennBlue
  },
  detailTextLabel: {
    color: Colors.brownishGrey
  },
  detailText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14)
  },
  detailBoxBorder: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: Colors.pinkishGrey,
    height: moderateScale(1),
    marginBottom: moderateScale(16)
  },
  detailBoxTotal: {
    fontFamily: 'Rubik-Medium',
    fontSize: moderateScale(14),
    color: Colors.greyishBrownTwo
  },
  detailTextTotal: {
    flex: 1,
    height: moderateScale(50),
    paddingLeft: moderateScale(17),
    paddingTop: moderateScale(17)
  },
  placeYourOrderIcon: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  placeYourOrderText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    color: Colors.white,
  },
  placeYourOrderButton: {
    height: 55,
    paddingHorizontal: moderateScale(15),
    flexDirection: 'row',
    backgroundColor: Colors.primaryOrange,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
})
