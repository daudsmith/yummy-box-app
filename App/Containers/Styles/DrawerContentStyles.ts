import { StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { Colors } from '../../Themes'

export interface DrawerStyleProps {
  topBox: object
  logoAndBellBox: object
  iconBell: object
  bellNotif: object
  iconYummie: object
  userNameBox: object
  userName: object
  iconGear: object
  walletBox: object
  currencyText: object
  activityIndicator: object
  topUpText: object
  topUpBox: object
  userNotLoginBox: object
  welcomeText: object
  registerNow: object
  memberBox: object
  alreadyMember: object
  signIn: object
  menuNavigationButton: ViewStyle,
  navigationText: TextStyle,
  navigationTextNotification: TextStyle,
  faq: object
  faqBox: object
  policyBox: object
  bottomBox: object
  bottomBoxText: object
  userLoginBox: object
  userButtonBox: object
  photoBox: object
  photoProfile: object
  creditsText: object
  balanceText: object
  yummyCreditsContainer: object
}

const styles: DrawerStyleProps = StyleSheet.create({
  topBox: {
    backgroundColor: '#fff',
    height: verticalScale(142),
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightGrey,
    marginBottom: verticalScale(-10),
  },
  logoAndBellBox: {
    height: verticalScale(47),
    flexDirection: 'row',
  },
  iconBell: {
    width: scale(20),
    height: verticalScale(20),
    right: scale(20),
  },
  bellNotif: {
    backgroundColor: Colors.bloodOrange,
    width: scale(10),
    height: verticalScale(10),
    position: 'absolute',
    borderRadius: 5,
    right: scale(20),
    top: verticalScale(12),
  },
  iconYummie: {
    height: verticalScale(40),
    width: scale(40),
    top: verticalScale(27),
    left: scale(20),
    position: 'absolute',
  },
  userNameBox: {
    paddingTop: verticalScale(8),
    flexDirection: 'row',
    alignItems: 'center',
  },
  userName: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    lineHeight: verticalScale(24),
    color: Colors.darkBlue,
  },
  iconGear: {
    left: scale(5),
    width: scale(20),
  },
  walletBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: verticalScale(12),
  },
  currencyText: {
    fontFamily: 'Rubik-Light',
    width: scale(180),
    fontSize: scale(16),
    color: Colors.darkBlue,
    marginLeft: scale(8.6),
  },
  activityIndicator: {
    color: Colors.brownishGrey,
    width: scale(95),
  },
  topUpText: {
    color: Colors.grennBlue,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    textDecorationLine: 'underline',
  },
  topUpBox: {
    marginTop: verticalScale(5),
    width: scale(46),
    height: verticalScale(17),
    justifyContent: 'center',
  },
  userNotLoginBox: {
    backgroundColor: Colors.whiteGray,
    height: verticalScale(145),
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightGrey,
    paddingLeft: scale(23),
    paddingTop: verticalScale(31),
  },
  welcomeText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(20),
    color: Colors.brownishGrey,
  },
  registerNow: {
    color: Colors.bloodOrange,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
  },
  memberBox: {
    flexDirection: 'row',
    paddingTop: verticalScale(6),
    alignItems: 'center',
  },
  alreadyMember: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(14),
    color: Colors.brownishGrey,
  },
  signIn: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.bloodOrange,
    textDecorationLine: 'underline',
  },
  menuNavigationButton: {
    alignItems: 'center',
    height: verticalScale(22),
    flexDirection: 'row',
    marginTop: verticalScale(22),
  },
  navigationText: {
    marginLeft: scale(36),
    fontFamily: 'Rubik-Light',
    color: Colors.greyishBrownTwo,
    fontSize: scale(14),
  },
  navigationTextNotification: {
    marginLeft: scale(36),
    fontFamily: 'Rubik-Light',
    color: Colors.darkBlue,
    fontSize: scale(14),
  },
  faq: {
    fontSize: scale(14),
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Light',
    backgroundColor: 'transparent',
  },
  faqBox: {
    width: '100%',
    justifyContent: 'center',
    borderTopColor: Colors.lightGrey,
    borderTopWidth: 1,
    paddingLeft: scale(20),
    paddingTop: verticalScale(10),
  },
  policyBox: {
    width: '100%',
    justifyContent: 'center',
    marginTop: verticalScale(23),
    paddingLeft: scale(20),
  },
  bottomBox: {
    paddingTop: verticalScale(10),
    paddingLeft: scale(20),
    borderTopWidth: 1,
    marginTop: verticalScale(11),
    borderColor: Colors.lightGrey,
    flex: 1,
  },
  bottomBoxText: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(14),
    color: Colors.darkBlue,
  },
  userLoginBox: {
    width: scale(275),
    height: 'auto',
    paddingVertical: verticalScale(20),
    paddingHorizontal: scale(20),
    borderBottomWidth: 1,
    backgroundColor: Colors.whiteGray,
    borderColor: Colors.lightGrey,
  },
  userButtonBox: {
    width: scale(275),
    height: 'auto',
  },
  photoBox: {
    marginTop: verticalScale(22),
    height: verticalScale(45),
    width: scale(275),
  },
  photoProfile: {
    width: scale(70),
    height: scale(70),
    borderRadius: 8,
    marginLeft: scale(3),
  },
  creditsText: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(12),
    color: Colors.warmGrey,
    marginBottom: verticalScale(8),
  },
  balanceText: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(12),
    color: Colors.pinkishOrange,
  },
  yummyCreditsContainer: {
    marginTop: verticalScale(16),
  },
})

export default styles
