import { Colors } from '../../Themes/'

const styles = {
  loginButtonContainer: {
    marginTop: 31,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  loginButtonText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.warmGrey,
    fontSize: 12
  }
}

export default styles
