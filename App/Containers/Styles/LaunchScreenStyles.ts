import { ImageStyle, StyleSheet, ViewStyle } from 'react-native'
import { ApplicationStyles } from '../../Themes/'
import { ScreenStylesInterface } from '../../Themes/ApplicationStyles'
import { scale, verticalScale } from 'react-native-size-matters'

export interface LaunchScreenStylesInterface {
  logo: ImageStyle,
  centered: ViewStyle,
}

const styles: ScreenStylesInterface & LaunchScreenStylesInterface = StyleSheet.create({
  ...ApplicationStyles.screen,
  logo: {
    width: scale(200),
    height: verticalScale(140)
  },
  centered: {
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default styles
