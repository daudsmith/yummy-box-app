import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes'

export default StyleSheet.create({
  contentContainer: {
    backgroundColor: 'rgb(245,245,245)',
    flex: 1,
    paddingTop: 19,
    paddingHorizontal: 20,
    alignItems: 'center'
  },
  contentMainBox: {
    backgroundColor: '#fff',
    paddingTop: 27,
    paddingHorizontal: 15,
    flex: 3,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0.2,
      height: 0.3
    },
    shadowOpacity: 1,
    shadowRadius: 0.5,
    elevation: 3,
  },
  h1: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Medium',
    fontSize: 14
  },
  p: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    lineHeight: 15,
    marginTop: 9
  },
  button1: {
    width: '100%',
    height: 38,
    backgroundColor: Colors.bloodOrange,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button1Text: {
    color: '#fff',
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  },
  phoneValidationText: {
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    marginTop: 6
  }
})
