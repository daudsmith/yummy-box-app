import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  artwork: {
    marginTop: 20,
    height: 90,
    backgroundColor: Colors.grennBlue,
    borderRadius: 3
  },
  deliveryDetailsContainer: {
    marginVertical: 20,
  },
  deliveryDetailsTitleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: Colors.greyishBrownThree,
    textAlign: 'center'
  },
  firstMealText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    color: Colors.brownishGrey,
    textAlign: 'center'
  },
  firstMealBoldText: {
    fontFamily: 'Rubik-Regular'
  },
  confirmSubscriptionButton: {
    height: 55,
    backgroundColor: Colors.bloodOrange,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  confirmSubscriptionButtonText: {
    color: 'white',
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  }
})