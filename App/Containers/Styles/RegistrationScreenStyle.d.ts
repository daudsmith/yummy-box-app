import { TextStyle, ViewStyle } from 'react-native'
interface styleObject {
  loginButtonContainer: ViewStyle
  loginButtonText: TextStyle
}
declare const styles: styleObject

export default styles
