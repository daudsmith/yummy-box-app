const styles = {
    cardDetailsContainer: {
      flex: 1,
      flexDirection: 'column',
      height: 500,
      backgroundColor: '#FEFDFF'
    },
    cardDetailsContainer2: {
      flex: 1,
      borderTopWidth: 1,
      flexDirection: 'column',
      backgroundColor: '#FEFDFF'
    },
    modalDetailsContainer: {
      flexDirection: 'row',
      paddingTop: 20,
      backgroundColor: '#FEFDFF'
    },
}

export default styles