import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes'

export default StyleSheet.create({
  ScreenContainer: {
    flex: 1,
    backgroundColor: Colors.whiteGray,
    alignItems: 'center',
    paddingVertical: 19,
    paddingHorizontal: 20,
  },
  contentText: {
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    color: Colors.greyishBrown,
    lineHeight: 15
  },
  screenBox: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: 23,
    paddingHorizontal: 15,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0.2,
      height: 0.3
    },
    shadowOpacity: 1,
    shadowRadius: 0.5,
    elevation: 3,
  },
  OtpTextInput: {
    height: 50,
  },
  textInputBorder: {
    height: 37,
    width: 33,
    borderBottomWidth: 1
  },
  textInput: {
    height: '100%',
    paddingBottom: 0,
    width: '100%',
    textAlign: 'center',
    fontFamily: 'Rubik-Light',
    fontSize: 24
  },
  button1: {
    flex: 1,
    backgroundColor: Colors.bloodOrange,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button1text: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: '#fff'
  },
  timeText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    color: Colors.greyishBrown,
    marginTop: 19,
    alignSelf: 'center',
  },
  resendText: {
    textDecorationLine: 'underline',
    fontFamily: 'Rubik-Regular'
  }
})