import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'
import { scale, verticalScale } from 'react-native-size-matters'

const Styles = StyleSheet.create({
  calendarContainer: {
    backgroundColor: 'white',
    shadowOpacity: 1,
    shadowRadius: 4,
    shadowColor: Colors.pinkishGrey,
    shadowOffset: {
      width: 0,
      height: verticalScale(2),
    },
  },
  subscriptionProfileContainer: {
    marginTop: verticalScale(15),
    marginHorizontal: scale(20),
  },
  title: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    color: Colors.greyishBrown,
    textAlign: 'center',
  },
  themeName: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    lineHeight: verticalScale(18),
    color: Colors.greyishBrownThree,
    marginTop: verticalScale(23),
  },
  calendarSection: {
    marginTop: verticalScale(30),
  },
  calendarTitleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo,
  },
  startDateText: {
    fontFamily: 'Rubik-Medium',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo,
  },
  repeatPeriodContainer: {
    marginTop: verticalScale(24),
  },
  repeatPeriodTitleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo,
  },
  deliveryDetailsContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: Colors.pinkishGrey,
    paddingTop: verticalScale(23),
    paddingHorizontal: scale(20),
  },
  paymentSectionContainer: {
    paddingHorizontal: scale(20),
    paddingBottom: verticalScale(58),
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: Colors.pinkishGrey,
    marginTop: verticalScale(37),
  },
  resetText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    color: Colors.bloodOrange,
  },
  resetButton: {
    marginRight: scale(5),
    backgroundColor: 'transparent',
  },
})

export default Styles
