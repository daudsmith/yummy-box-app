import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { Colors } from '../../Themes'

const styles = StyleSheet.create({
  boxTop: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  middleBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textFb: {
    marginLeft: scale(10),
    color: 'white',
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
  },
  loginButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: verticalScale(8),
    paddingVertical: verticalScale(10),
    paddingHorizontal: scale(10),
  },
  loginText: {
    color: Colors.black,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    textDecorationLine: 'underline',
    backgroundColor: 'transparent',
  },
  footerContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  browseMenuButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: verticalScale(17),
    marginBottom: verticalScale(33),
    paddingVertical: verticalScale(10),
    paddingHorizontal: scale(10),
  },
  BrowseMenuText: {
    color: Colors.black,
    fontFamily: 'Rubik-Regular',
    fontSize: scale(13),
    textDecorationLine: 'underline',
    backgroundColor: 'transparent',
  },
  imageBackground: {
    flex: 1,
    width: null,
    alignSelf: 'stretch',
  },
})

export default styles
