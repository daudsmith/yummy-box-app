import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  resetButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    color: Colors.bloodOrange
  },
  descriptionContainer: {
    marginTop: verticalScale(20)
  },
  descriptionText: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(12),
    lineHeight: verticalScale(16),
    color: Colors.greyishBrownTwo
  },
  calendarSection: {
    marginTop: verticalScale(30)
  },
  calendarTitleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo
  },
  startDateText: {
    fontFamily: 'Rubik-Medium',
    fontSize: scale(14),
    color: Colors.bloodOrange
  },
  repeatDayContainer: {
    marginTop: verticalScale(30)
  },
  repeatDayTitleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo
  },
  dayButtonsContainer: {
    marginTop: verticalScale(10)
  },
  dayButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: scale(40),
    height: verticalScale(40),
    borderRadius: 20,
    borderWidth: 2,
    borderColor: Colors.bloodOrange,
    marginRight: scale(13)
  },
  dayButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.greyishBrownTwo
  },
  repeatPeriodContainer: {
    marginTop: verticalScale(33)
  },
  repeatPeriodTitleText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    color: Colors.greyishBrownTwo
  },
  calendarContainer: {
    backgroundColor: 'white',
    shadowOpacity: 1,
    shadowRadius: 4,
    shadowColor: Colors.pinkishGrey,
    shadowOffset: { width: 0, height: 2 }
  },
  bottomDescription: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(12),
    color: Colors.warmGrey,
    lineHeight: verticalScale(17)
  }
})
