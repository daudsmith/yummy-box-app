import { Platform, StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  searchBoxContainer: {
    marginHorizontal: moderateScale(20),
    marginTop: moderateScale(20)
  },
  searchBox: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: Platform.OS === 'ios' ? moderateScale(10) : moderateScale(0),
    paddingHorizontal: 5,
    height: moderateScale(40)
  },
  searchBoxTextInput: {
    fontFamily: 'Rubik-Light',
    color: Colors.greyishBrownTwo,
    fontSize: 14
  },
  resultContainer: {
    marginTop: moderateScale(13)
  },
  resultListContainer: {
    maxHeight: moderateScale(230)
  },
  historyListHeaderTextView: {
    paddingVertical: moderateScale(8),
    alignItems: 'center',
    borderBottomColor: Colors.pinkishGrey,
    borderBottomWidth: 0.5
  },
  historyListHeaderText: {
    fontFamily: 'Rubik-Medium',
    fontSize: moderateScale(14),
    color: Colors.brownishGrey
  },
  resultListRow: {
    flexDirection: 'row',
    height: moderateScale(78),
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.pinkishGrey
  },
  resultListIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  resultListTextContainer: {
    flex: 6,
    justifyContent: 'center',
    marginRight: moderateScale(15)
  },
  resultListText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.greyishBrownTwo
  },
  setDestinationButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.greyishBrownTwo,
    borderRadius: 3
  },
  setDestinationButtonText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.white,
    fontSize: 14
  },
  animateToCurrentLocationButton: {
    backgroundColor: 'white',
    height: 50,
    width: 50,
    position: 'absolute',
    bottom: moderateScale(20),
    right: moderateScale(20),
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: Colors.pinkishGrey,
    shadowOffset: { height: 5, width: 5 },
    elevation: 2,
    borderRadius: 5
  }
})
