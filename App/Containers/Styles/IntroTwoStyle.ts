import { ImageStyle, StyleSheet, ViewStyle, TextStyle, Platform } from 'react-native'
import { scale } from 'react-native-size-matters'
import { Colors } from '../../Themes'

export interface IntroTwoStylesInterface {
	container: ViewStyle,
	containerCustom: ViewStyle,
	leftButtonCustom: ViewStyle,
	backgroundImage: ImageStyle,
	card: ViewStyle,
	containerTitle: ViewStyle,
	title: TextStyle,
	containerDesc: ViewStyle,
	desc: TextStyle,
	containerButtonSearch: ViewStyle,
	containerIcon: ViewStyle,
	textSearch: TextStyle,
}

const styles: IntroTwoStylesInterface = StyleSheet.create({
	container: {
		flex: 1,
	},
	containerCustom: {
		backgroundColor: 'transparent',
		marginTop: Platform.OS === 'ios' ? 15 : 25
	},
	leftButtonCustom: {
		width: 40,
		height: 40,
		backgroundColor: Colors.primaryWhite,
		borderRadius: 50,
		shadowOpacity: 0.2,
		shadowRadius: 0,
		shadowColor: Colors.primaryDark,
		shadowOffset: { width: 0, height: 2 },
		elevation: 5,
		alignItems: 'center',
	},
	backgroundImage: {
		flex: 1,
		resizeMode: 'contain',
	},
	card: {
		width: '100%',
		position: 'absolute',
		bottom: 0,
		borderTopLeftRadius: 8,
		borderTopRightRadius: 8,
		borderTopWidth: 1,
		borderLeftWidth: 1,
		borderRightWidth: 1,
		borderColor: 'transparent',
		backgroundColor: Colors.primaryWhite,
		shadowOpacity: 0.2,
		shadowRadius: 0,
		shadowColor: Colors.primaryDark,
		shadowOffset: { width: 0, height: 2 },
		elevation: 5,
		padding: 20,
	},
	containerTitle: {
		marginVertical: 15
	},
	title: {
		fontFamily: 'Rubik-Medium',
		fontSize: scale(19),
		color: Colors.primaryDark,
		lineHeight: 24,
	},
	containerDesc: {
		marginBottom: 20
	},
	desc: {
		fontFamily: 'Rubik-Regular',
		fontSize: scale(13),
		color: Colors.primaryDark,
		lineHeight: 20,
	},
	containerButtonSearch: {
		flexDirection: 'row',
		borderWidth: 1,
		borderColor: Colors.secondaryGrey,
		borderRadius: 4,
		padding: 10,
		alignItems: 'center'
	},
	containerIcon: {
		marginRight: 10
	},
	textSearch: {
		fontFamily: 'Rubik-Regular',
		fontSize: scale(13),
		color: Colors.primaryGrey
	}
})

export default styles
