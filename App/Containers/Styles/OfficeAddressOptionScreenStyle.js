import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

const confirmButtonColor = Colors.bloodOrange

export default StyleSheet.create({
  flatListStyle: {
    backgroundColor: 'white',
    paddingHorizontal: moderateScale(20),
    marginBottom: 100,
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: moderateScale(7),
    paddingTop: moderateScale(16),
    paddingBottom: moderateScale(14),
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
    borderBottomColor: Colors.lightGrey
  },
  titleText: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    fontWeight: 'bold'
  },
  addressText: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    marginTop: moderateScale(15)
  },
  checkIcon: {
    paddingLeft: moderateScale(16)
  },
  confirmButton: {
    backgroundColor: confirmButtonColor,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    padding: moderateScale(20)
  },
  confirmButtonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: moderateScale(16)
  },
  bottomBackground: {
    backgroundColor: confirmButtonColor,
    position: 'absolute',
    left: 0,
    right: 0,
    height: 100,
    bottom: -50
  }
})
