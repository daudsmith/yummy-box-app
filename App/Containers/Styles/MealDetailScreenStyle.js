import { Dimensions, StyleSheet } from 'react-native'
import { moderateScale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

const { width } = Dimensions.get('window')

// COLOR THEMES
const lightGray = 'rgb(190, 190, 190)'

export default StyleSheet.create({
  dot: {
    height: moderateScale(8),
    width: moderateScale(8),
    marginHorizontal: moderateScale(5),
    borderRadius: 5,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'white'
  },
  dotContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    bottom: moderateScale(30)
  },
  scrollViewContainer: {
    backgroundColor: '#fff',
    marginLeft: 15,
    marginRight: 15
  },
  foodNameContainer: {
    flexDirection: 'row',
    paddingBottom: moderateScale(15),
  },
  leftArrowButton: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: moderateScale(24)
  },
  rightArrowButton: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    paddingTop: moderateScale(24)
  },
  foodNameBox: {
    flex: 6,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: moderateScale(15)
  },
  foodNameText: {
    lineHeight: moderateScale(24),
    textAlign: 'center',
    color: Colors.greyishBrown,
    fontSize: moderateScale(18),
    fontFamily: 'Rubik-Medium'
  },
  foodTagText: {
    color: Colors.bloodOrange,
    fontSize: moderateScale(12),
    fontFamily: 'Rubik-Regular',
    lineHeight: moderateScale(18)
  },
  swiperContainer: {
    height: width - moderateScale(30),
    backgroundColor: 'transparent',
    marginTop: moderateScale(10)
  },
  priceAndQuantityContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    top: 5,
    backgroundColor: 'transparent',
    justifyContent: 'space-between'
  },
  priceText: {
    color: Colors.primaryOrange,
    fontSize: moderateScale(16),
    fontFamily: 'Rubik-Medium',
    lineHeight: verticalScale(24),
  },
  basePriceText: {
    color: Colors.primaryGrey,
    fontSize: moderateScale(12),
    fontFamily: 'Rubik-Medium',
    lineHeight: verticalScale(24),
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    marginLeft: moderateScale(4),
  },
  addButtonContainer: {
    width: moderateScale(65),
    height: moderateScale(28),
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(3),
    borderColor: Colors.bloodOrange,
    borderWidth: moderateScale(1)
  },
  addText: {
    color: Colors.bloodOrange,
    fontSize: moderateScale(16),
    fontFamily: 'Rubik-Regular'
  },
  mealDescriptionContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10)
  },
  descriptionText: {
    color: Colors.greyishBrown,
    fontSize: 14,
    fontFamily: 'Rubik-Regular',
    lineHeight: verticalScale(20)
  },
  noDinnerContainer: {
    marginBottom: verticalScale(8),
  },
  noDinnerText: {
    color: Colors.primaryGrey,
    fontSize: 14,
    fontFamily: 'Rubik-Regular',
    lineHeight: verticalScale(20)
  },
  ingredientsAndNutritionContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    marginTop: verticalScale(5),
    marginBottom: verticalScale(80),
    borderWidth: 1,
    borderColor: lightGray,
    borderRadius: 5
  },
  ingridientAndNutritionsButtonBox: {
    height: verticalScale(45),
    flexDirection: 'row'
  },
  ingridientButton: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  ingridientsBorder: {
    top: verticalScale(4),
    height: verticalScale(1.2),
    width: moderateScale(50)
  },
  ingridientsText: {
    lineHeight: verticalScale(25),
    fontSize: 16,
    fontFamily: 'Rubik-Regular'
    // color: 'rgb(86, 86, 86)'
  },
  nutritionsBox: {
    flexDirection: 'row',
    marginBottom: moderateScale(5)
  },
  nutritionsText: {
    fontSize: 12,
    lineHeight: verticalScale(16),
    fontFamily: 'Rubik-Regular',
    color: Colors.primaryGrey
  },
  whiteSpaceContainer: {
    width: '100%',
    backgroundColor: 'transparent'
  },
  addToCartButton: {
    height: 30,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: Colors.bloodOrange
  }
})
