import { StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  alertBoxContainer: {
    backgroundColor: 'white',
    marginHorizontal: scale(30),
    paddingHorizontal: scale(30),
    borderRadius: 5,
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1,
    height: verticalScale(150),
    paddingTop: verticalScale(22),
    paddingBottom: scale(28),
  },
  alertModalText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
    fontWeight: 'normal',
    textAlign: 'center',
    color: Colors.greyishBrown
  },
  button: {
    marginHorizontal: scale(60),
  },
  inputContainer: {
  },
  label: {
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    fontSize: scale(14),
  },
  errorIcon: {
    fontSize: scale(14),
    fontWeight: 'bold',
  },
  errorMessage: {
    fontFamily: 'Rubik-Regular',
    color: 'red',
    fontSize: scale(10),
    marginTop: verticalScale(3)
  }
})
