import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes'

export default StyleSheet.create({
  contentContainer: {
    backgroundColor: 'rgb(245,245,245)',
    flex: 1,
    paddingTop: 19,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  contentMainBox: {
    backgroundColor: '#fff',
    paddingTop: 27,
    paddingHorizontal: 15,
    flex: 1,
  },
  contentShadow: {
    shadowColor: '#000000',
    shadowOffset: {
      width: 0.2,
      height: 0.3
    },
    shadowOpacity: 1,
    shadowRadius: 0.5,
    elevation: 3,
    height: 210,
    backgroundColor: 'transparent',
    width: '100%'
  },
  h1: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    marginTop: 19
  },
  p: {
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    lineHeight: 15
  },
  button1: {
    width: '100%',
    height: 38,
    backgroundColor: Colors.bloodOrange,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 18
  },
  button1Text: {
    color: '#fff',
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  },
  phoneValidationText: {
    color: 'red',
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    marginTop: 6
  },
  textInputContainer: {
    width: 250,
    height: 28,
    borderBottomWidth: 1,
    borderColor: Colors.lightGrey
  },
  modalConfirmationContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalConfimationBox: {
    width: 280,
    height: 169,
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 14
  },
  modalConfirmationText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.greyishBrown,
    textAlign: 'center'
  },
  button2: {
    borderRadius: 10,
    width: 90,
    height: 35,
    backgroundColor: Colors.bloodOrange,
    alignItems: 'center',
    justifyContent: 'center'
  },
  button2Text: {
    fontFamily: 'Rubik-Regular',
    color: '#fff',
    fontSize: 16
  },
  button3: {
    borderRadius: 10,
    width: 90,
    height: 35,
    backgroundColor: Colors.lightGrey,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalConfirmationBox: {
    width: 280,
    height: 169,
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 14
  },
  modalSuccessBox: {
    width: 280,
    height: 238,
    backgroundColor: '#fff',
    borderRadius: 10,
    paddingTop: 36
  },
  modalSuccessh1: {
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    color: Colors.greyishBrownTwo,
    marginTop: 9,
    alignSelf: 'center'
  },
  modalSuccessP: {
    fontFamily: 'Rubik-Light',
    fontSize: 14,
    color: Colors.greyishBrownTwo,
    marginTop: 4,
    alignSelf: 'center',
    textAlign: 'center'
  },
  button4: {
    backgroundColor: Colors.bloodOrange,
    borderRadius: 21,
    width: 76,
    height: 35,
    marginTop: 27,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  button4Text: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: '#fff'
  }
})