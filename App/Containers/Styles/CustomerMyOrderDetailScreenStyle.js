import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  columns: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'stretch'
  },
  orderNumberText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    flex: 1
  },
  orderStatusText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    flex: 1,
    textAlign: 'right',
    marginRight: 20
  },
  orderDetailText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    flex: 1
  },

  accordionContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: Colors.pinkishGrey,
    borderRadius: 3,
    borderTopColor: 'transparent'
  },
  accordionBox: {
    height: null,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.pinkishGrey,
    width: '100%',
    marginTop: 10
  },
  accordionQty: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  },
  accordionQtyBox: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  imageOrderDetail: {
    width: 30,
    height: 30,
    borderRadius: 3
  },
  accordionName: {
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    lineHeight: 18,
    color: Colors.greyishBrownTwo
  },
  accordionNameBox: {
    flex: 3,
    justifyContent: 'center',
    marginLeft: 10
  },
  accordionDescBox: {
    flexDirection: 'row',
    paddingBottom: 22,

  },
  accordionImage: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  descBox: {
    height: 51,
    width: '100%',
    borderBottomWidth: StyleSheet.hairlineWidth,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.pinkishGrey,
  },
  addressBox: {
    height: null,
    width: '100%',
    borderColor: Colors.pinkishGrey,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: 12,
    borderBottomWidth: 0,
    flexWrap: 'wrap'
  },
  addressText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    color: Colors.greyishBrownTwo,
    flex: 1,
    lineHeight: 18
  },
  addressIcon: {
    width: 32,
    justifyContent: 'center',
    alignItems: 'center'
  },
  addressNote: {
    fontFamily: 'Rubik-Light',
    fontSize: 10,
    color: Colors.greyishBrownTwo,
    flex: 1,
    lineHeight: 18
  },
  addressNoteBox: {
    height: null,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.pinkishGrey,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: 8,
    paddingBottom: 5
  },
  nameBox: {
    height: 38,
    width: '100%',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.pinkishGrey,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5
  },
  phoneBox: {
    width: '100%',
    borderColor: Colors.pinkishGrey,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
    height: 38,
    borderBottomWidth: 0,
    marginBottom: 0
  },
  nameText: {
    fontFamily: 'Rubik-Light',
    fontSize: 12,
    color: Colors.greyishBrownTwo,
    flex: 7
  },
  regular16: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  },
  regular14: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.greyishBrownThree
  },
  regular12: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    color: Colors.greyishBrownThree
  },
  medium14: {
    fontFamily: 'Rubik-Medium',
    fontSize: 14
  },
  light16: {
    fontFamily: 'Rubik-Light',
    fontSize: 16
  },
  light12: {
    fontFamily: 'Rubik-Light',
    fontSize: 12
  },
  headerAccordion: {
    paddingLeft: 18,
    paddingRight: 21,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
    height: 43,
    borderWidth: 1,
    borderColor: Colors.pinkishGrey,
    borderRadius: 3,
  },
  
  renderContentBox: {
    flex: 1,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: Colors.pinkishGrey,
    borderRadius: 3,
    paddingLeft: 17,
    paddingRight: 15,
    marginTop: -2
  },
  accordionPaddingBox: {
    paddingTop: 20
  },
  modifyButton: {
    height: 52,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  modifyText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    color: Colors.grennBlue
  }
})
