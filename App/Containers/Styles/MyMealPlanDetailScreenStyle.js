import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  needHelpButtonContainer: {
    backgroundColor: Colors.bloodOrange,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
  },
  needHelpText: {
    color: 'white',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
  },
  cancelOrderModalContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: moderateScale(30),
  },
  cancelOrderModalBox: {
    height: moderateScale(145),
    borderRadius: 5,
    paddingHorizontal: moderateScale(30),
    paddingVertical: moderateScale(24),
    backgroundColor: '#ffffff',
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 4,
    shadowOpacity: 1,
  },
  modalButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: moderateScale(42),
    marginTop: moderateScale(15),
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 5,
    marginRight: moderateScale(2.5),
  },
  modalButtonText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
  },
  activityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mealPlanDetailContainer: {
    flex: 1,
    marginHorizontal: moderateScale(20),
  },
  deliveryDateAndStatusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: moderateScale(18),
  },
  deliveryDateText: {
    flex: 1,
    justifyContent: 'center',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(18),
    color: Colors.greyishBrownThree,
  },
  deliveryStatusText: {
    flex: 1,
    justifyContent: 'center',
    textAlign: 'right',
    fontWeight: '500',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    color: Colors.greyishBrown,
  },
  mealItemsContainer: {
    marginTop: moderateScale(18),
    marginBottom: moderateScale(7),
  },
  deliveryTimeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: moderateScale(20),
  },
  deliveryTimeLabelText: {
    flex: 1,
    alignItems: 'center',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.greyishBrownThree,
  },
  deliveryTimeValueText: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    fontWeight: '500',
    color: Colors.greyishBrownThree,
  },
  deliveryAddressText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.greyishBrownThree,
    marginVertical: moderateScale(9),
  },
  itemRowContainer: {
    flexDirection: 'row',
    marginBottom: moderateScale(15),
  },
  itemRowDataContainer: {
    flex: 2,
    justifyContent: 'space-between',
    paddingTop: moderateScale(3),
    paddingBottom: moderateScale(7),
  },
  foodNameText: {
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(18),
    color: Colors.greyishBrownTwo,
  },
  priceText: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.warmGreyTwo,
  },
  quantityText: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(16),
    color: Colors.greyishBrown,
  },
  addressSectionLineContainer: {
    flexDirection: 'row',
    marginVertical: moderateScale(11),
  },
  addressSectionLineIconContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addressSectionLineValueContainer: {
    flex: 7,
    justifyContent: 'center',
  },
  addressSectionLineValueText: {
    color: Colors.greyishBrownTwo,
    fontSize: moderateScale(12),
    lineHeight: 18,
  },
  transactionTotalBoxContainer: {
    backgroundColor: Colors.lightGrey2,
    padding: moderateScale(17),
    marginTop: moderateScale(25),
    marginBottom: moderateScale(31),
  },
  paymentMethodText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.brownishGrey,
  },
  transactionLineContainer: {
    flexDirection: 'row',
    marginBottom: moderateScale(8),
  },
  transactionLineLabelText: {
    flex: 1,
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
  },
  transactionLineValueText: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
  },
  paymentMethodValueText: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.brownishGrey,
  },
  alertBoxText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: 'normal',
    textAlign: 'center',
    color: Colors.greyishBrown,
  },
  alertBoxTextBold: {
    fontFamily: 'Rubik-Light',
    color: Colors.panther,
    fontWeight: '400',
  },
})
