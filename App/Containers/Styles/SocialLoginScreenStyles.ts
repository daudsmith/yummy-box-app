import { StyleSheet } from 'react-native'
import { scale } from 'react-native-size-matters'
import { Colors } from '../../Themes'

export interface SocialLoginProps {
  textSocial: object
  buttonFb: object
  buttonFbIos: object
  buttonIos: object
}

const styles: SocialLoginProps = StyleSheet.create({
  textSocial: {
    marginLeft: scale(10),
    color: 'white',
    fontFamily: 'Rubik-Regular',
    fontSize: scale(14),
  },
  buttonFb: {
    width: '100%',
    backgroundColor: Colors.duskBlue, 
  },
  buttonFbIos: {
    width: '100%',
    backgroundColor: Colors.duskBlue,
    marginBottom:8
  },
  buttonIos: {
    width: '100%',
    backgroundColor: '#000', 
  }
})

export default styles
