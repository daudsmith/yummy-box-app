import { StyleSheet } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  description: {
    fontFamily: 'Rubik-Light',
    fontSize: 11,
    fontWeight: '300',
    color: Colors.greyishBrown,
    lineHeight: 14
  },
  alertBoxContainer: {
    backgroundColor: 'white',
    marginHorizontal: moderateScale(30),
    paddingHorizontal: moderateScale(30),
    borderRadius: 5,
    shadowColor: 'rgba(145, 145, 145, 0.5)',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    elevation: 1,
    height: moderateScale(150),
    paddingTop: moderateScale(22),
    paddingBottom: moderateScale(28),
  },
  alertModalText: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    fontWeight: 'normal',
    textAlign: 'center',
    color: Colors.greyishBrown
  },
  button: {
    marginHorizontal: moderateScale(60),
  },
  label: {
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    fontSize: moderateScale(14),
  },
  errorIcon: {
    fontSize: moderateScale(14),
    fontWeight: 'bold',
  },
  errorMessage: {
    fontFamily: 'Rubik-Regular',
    color: 'red',
    fontSize: moderateScale(10),
    marginTop: moderateScale(3)
  }
})
