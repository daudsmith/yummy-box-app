import { Dimensions, StyleSheet } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { Colors } from '../../Themes/'

const {width} = Dimensions.get('window')

export default StyleSheet.create({
  mealNameText: {
    marginTop: verticalScale(5),
    fontFamily: 'Rubik-Regular',
    fontWeight: '500',
    color: Colors.greyishBrownTwo,
    fontSize: scale(18)
  },
  priceContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: verticalScale(10),
    marginBottom: verticalScale(35)
  },
  priceText: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    color: Colors.brownishGrey
  },
  addToCartButton: {
    height: verticalScale(30),
    width: width * 0.18,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: Colors.bloodOrange
  },
  tagContainer: {
    marginTop: verticalScale(8)
  },
  tagText: {
    fontFamily: 'Rubik-Regular',
    color: Colors.bloodOrange,
    fontSize: scale(12)
  },
  calendarPickerContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  calendarPickerButton: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Colors.bloodOrange,
    width: scale(120),
    height: verticalScale(30),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginHorizontal: scale(15),
  },
  calendarPickerText: {
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: scale(14),
    fontFamily: 'Rubik-Regular'
  },
  calendarPickerIcon: {
    fontSize: scale(12),
    paddingLeft: scale(7),
    textAlign: 'center',
    color: Colors.bloodOrange
  },
  badge: {
    width: scale(10),
    height: scale(10),
    borderRadius: 5,
    backgroundColor: Colors.bloodOrange,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: scale(3),
    top: 0
  }
})
