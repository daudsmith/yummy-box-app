import { Content, Container } from 'native-base'
import React from 'react'
import { Text, TouchableWithoutFeedback, View } from 'react-native'
import { scale } from 'react-native-size-matters'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import NavigationBar from '../Components/NavigationBar'
import Styles from './Styles/AddressTypeScreenStyle'
import { NavigationDrawerProp } from 'react-navigation-drawer'

const Title = 'Address Type'

export interface AddressTypeProps {
  navigation: NavigationDrawerProp
}

export interface AddressTypeState { }

class AddressTypeScreen extends React.Component<AddressTypeProps, AddressTypeState> {

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  navigateHandler = (type) => {
    const {
      navigation,
    } = this.props

    const {
      params
    } = navigation.state
    if (params) {
      const {
        nextParams,
        corporate,
        personal,
        optionScreen,
        showBackButton,
        showAnotherLocationButton,
        selectPinpoint
      } = params
      const newParams = {
        ...nextParams,
        addresses: type === 'corporate' ? corporate : personal,
        addressType: type,
        showAnotherLocationButton,
        selectPinpoint,
        showBackButton,
      }
      navigation.navigate(
        optionScreen,
        newParams,
      )
    }
  }

  render() {
    const { selectPinpoint } = this.props.navigation.state.params
    return (
      <Container style={{ flex: 1 }}>
        <NavigationBar
          leftSide={!selectPinpoint ? 'back' : '' }
          title={Title}
          leftSideNavigation={this.navigationGoBack}
        />
        <Content
          contentContainerStyle={{ flexGrow: 1 }}
          style={Styles.content}
        >
          <View>
            <TouchableWithoutFeedback
              onPress={() => this.navigateHandler('corporate')}
            >
              <View style={Styles.listContainer}>
                <View style={{ flex: 1 }}>
                  <Text style={Styles.optionItemText}>Office Address</Text>
                </View>
                <View style={Styles.viewArrow}>
                  <FontAwesome
                    name="chevron-right"
                    size={scale(14)}
                    color="rgb(86,86,86)"
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => this.navigateHandler('personal')}
            >
              <View style={Styles.listContainer}>
                <View style={{ flex: 1 }}>
                  <Text style={Styles.optionItemText}>Personal Address</Text>
                </View>
                <View style={Styles.viewArrow}>
                  <FontAwesome
                    name="chevron-right"
                    size={scale(14)}
                    color="rgb(86,86,86)"
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </Content>
      </Container>
    )
  }
}

export default AddressTypeScreen
