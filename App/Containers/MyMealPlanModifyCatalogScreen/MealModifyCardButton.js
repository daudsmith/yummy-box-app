import { Text } from 'native-base'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ActivityIndicator, TouchableOpacity } from 'react-native'
import ApiCatalog from '../../Services/ApiCatalog'
import { Colors } from '../../Themes'


class MealModifyCardButton extends Component {
  static propTypes = {
    mealIndex: PropTypes.number.isRequired,
    meal: PropTypes.object.isRequired,
    onSelectPress: PropTypes.func.isRequired,
    deliveryDate: PropTypes.string.isRequired,
  }

  
  render () {
    const {
      onSelectPress,
      mealIndex,
      available
    } = this.props

    if (!available) {
      return (
        <Text style={{color: Colors.greyishBrownThree}}>Sold Out</Text>
      )
    }
    return (
      <TouchableOpacity
        onPress={onSelectPress}
        testID={`selectProduct${mealIndex}`}
        accessibilityLabel={`selectProduct${mealIndex}`}
        style={{borderWidth: 1, borderColor: Colors.bloodOrange, borderRadius: 3, padding: 5}}
        >
        <Text style={{color: Colors.bloodOrange}}>SELECT</Text>
      </TouchableOpacity>
    )
  }
}

export default MealModifyCardButton
