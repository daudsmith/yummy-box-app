import moment from 'moment'
import { ScrollableTab, Tab, Tabs, Text, View } from 'native-base'
import React, { Component } from 'react'
import { ActivityIndicator, Dimensions, Image, Modal, ScrollView, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import { connect } from 'react-redux'
import NavigationBar from '../../Components/NavigationBar'
import OfflineModal from '../../Components/OfflineModal'
import StaticMealDetailPopUp from '../../Components/StaticMealDetailPopUp'
import YummyboxIcon from '../../Components/YummyboxIcon'
import WithSafeAreaView from '../../Components/Common/WithSafeAreaView'
import MealsActions from '../../Redux/MealsRedux'
import MyMealPlanActions from '../../Redux/MyMealPlanRedux'
import LocaleFormatter from '../../Services/LocaleFormatter'
import { checkIfFilterActive } from '../../Services/MealService'
import { Colors } from '../../Themes'
import Styles from '../Styles/MealListScreenStyle'
import TagFilterScreen from '../TagFilterScreen'
import MealModifyCardButton from './MealModifyCardButton'
import FastImage from 'react-native-fast-image'
import StockService from '../../Services/StockService'
import ApiCatalog from '../../Services/ApiCatalog'

const { width } = Dimensions.get('window')

const ITEM_IMAGE_WIDTH = width * 0.9
const ITEM_IMAGE_HEIGHT = width * 0.9

class MyMealPlanModifyCatalogScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      deliveryDate: props.navigation.state.params.deliveryDate,
      item: props.navigation.state.params.item,
      itemIndex: props.navigation.state.params.index,
      delivery_fee: props.navigation.state.params.delivery_fee,
      kitchenCode: props.navigation.state.params.kitchen_code,
      loading: false,
      categories: [],
      initialTab: 0,
      datePickerVisible: false,
      fetchingMeals: false,
      showTagFilter: false,
      showMealDetailModal: false,
      mealDetailId: null,
      modifiedMMPDetail: null,
      currentMMPDetail: null,
      showPopUpMealDetail: null,
    }
  }

  componentDidMount() {
    this.getMealListScreenData(
      this.state.deliveryDate,
      this.state.kitchenCode,
    )
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    this.forceUpdate()
    this.setState({
      fetchingMeals: newProps.meals.fetching,
      categories: newProps.meals.meals,
      modifiedMMPDetail: newProps.modifiedMMPDetail,
      currentMMPDetail: newProps.currentMMPDetail,
    })
    if (!this.props.isConnected && newProps.isConnected) {
      this.getMealListScreenData(
        this.state.deliveryDate,
        this.state.kitchenCode,
      )
    }
  }

  tabOnChange(index) {
    this.setState({ activeTab: index })
  }

  getMealListScreenData(deliveryDate, kitchenCode) {
    if (this.props.isConnected) {
      this.props.fetchMeals(deliveryDate, kitchenCode)
    }
  }

  setCategoriesState(categories) {
    if (categories === null) {
      return false
    }
    if (categories.hasOwnProperty('meals')) {
      this.setState({
        categories: categories.meals
      })
    } else {
      this.setState({
        categories
      })
    }
  }

  applyFilter(includeTags, excludeTags) {
    this.setState({ showTagFilter: false })

    this.props.fetchFilteredMeals(this.state.deliveryDate, includeTags, excludeTags)
    if (!this.props.meals.fetching) {
      this.setCategoriesState(this.props.meals.meals)
    }
  }

  filterButton() {
    const { tags } = this.props.meals
    const disable = tags === null
    const filterActive = tags === null ? false : checkIfFilterActive(tags)
    return (
      <TouchableOpacity testID='filterButton' disabled={disable} onPress={() => this.setState({ showTagFilter: true })} style={{ paddingRight: 5 }}>
        <YummyboxIcon name='filter' color={Colors.bloodOrange} size={25} />
        {filterActive &&
          <View style={Styles.badge} />
        }
      </TouchableOpacity>
    )
  }

  renderTagFilterModal() {
    const { tags } = this.props.meals

    return (
      <TagFilterScreen
        dismissModal={() => this.setState({ showTagFilter: false })}
        applyFilter={(includeTags, excludeTags) => this.applyFilter(includeTags, excludeTags)}
        tags={tags}
      />
    )
  }

  renderCategoryTabs(categories) {
    let isDinner = true
    const { modifiedMMPDetail } = this.props
    const { time } = modifiedMMPDetail !== null && modifiedMMPDetail.data
    if (time && time.indexOf('12') >= 0) {
      isDinner = false
    }
    if (categories === null) {
      this.props.fetchMeals(this.state.deliveryDate)
      return null
    }

    return (
      <Tabs
        initialPage={this.state.initialTab}
        renderTabBar={() => <ScrollableTab
          style={{
            borderBottomWidth: 0,
            justifyContent: 'flex-start',
            height: 31,
            marginBottom: 20,
            backgroundColor: 'white'
          }}
          underlineStyle={{ backgroundColor: 'transparent' }} />}
        style={{ backgroundColor: 'white' }}
        tabBarUnderlineStyle={{ backgroundColor: Colors.grennBlue, height: 1, width: 30, marginLeft: 23 }}
        onChangeTab={({ i }) => this.tabOnChange(i)} >
        {categories.map((category, index) => {
          const items = category.items.data
          const haveItems = items.length > 0
          return (
            <Tab
              heading={category.name}
              key={index}
              tabStyle={{
                backgroundColor: 'white',
                marginHorizontal: 0,
                paddingLeft: 5,
                paddingRight: 5,
                height: 40,
                flexWrap: 'nowrap',
                width: 'auto',
                justifyContent: 'center'
              }}
              activeTabStyle={{
                backgroundColor: 'white',
                height: 40,
                paddingLeft: 5,
                paddingRight: 5,
                width: 'auto',
                justifyContent: 'center'
              }}
              textStyle={{ color: Colors.warmGreyTwo, fontFamily: 'Rubik-Regular', fontSize: 16, paddingHorizontal: 0 }}
              activeTextStyle={{ color: Colors.green, fontFamily: 'Rubik-Regular', fontSize: 16 }}
            >
              <ScrollView testID={`mealScroll_${index}`} contentContainerStyle={{ flexGrow: 1 }} style={{ marginHorizontal: width * 0.05 }} showsVerticalScrollIndicator={false}>
                {haveItems ? (
                  this.renderProductRows(items, index, isDinner)
                ) : (
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ fontFamily: 'Rubik-Regular', textAlign: 'center' }}>Sorry - No products match the filters criteria you applied for the selected date.{'\n'}Please select other filters to apply</Text>
                    </View>
                  )}
              </ScrollView>
            </Tab>
          )
        })}
      </Tabs>
    )
  }

  renderProductRows(items, categoryIndex, isDinner) {
    const itemLength = items.length

    return (
      items.map((meal, mealIndex) => {
        if (meal.dinner === '' && isDinner || !isDinner) {
          return this.renderProductCard(meal, mealIndex, categoryIndex, itemLength)
        }
      })
    )
  }

  renderProductTags(tags) {
    const tagsLength = tags.length
    var tagsString = ''
    tags.map((tag, index) => {
      if (index === tagsLength - 1) {
        tagsString = tagsString + tag.name
      } else {
        tagsString = tagsString + tag.name + ' \u2022 '
      }
    })

    return (
      <View style={Styles.tagContainer}>
        <Text style={[Styles.tagText, { fontFamily: 'Rubik-Regular' }]}>{tagsString}</Text>
      </View>
    )
  }

  renderProductImage(item) {
    const itemImage = item.catalog_image.original
    return (
      <FastImage
        style={{ width: ITEM_IMAGE_WIDTH, height: ITEM_IMAGE_HEIGHT, borderRadius: 3 }}
        source={{
          uri: itemImage,
          priority: FastImage.priority.normal,
        }}
      />
    )
  }

  handleRowItemPress(meal) {
    this.setState({ mealDetailId: meal.id, showMealDetailModal: true, showPopUpMealDetail: meal })
  }

  getPriceDifference(itemSalePrice) {
    const { currentMMPDetail, itemIndex, item } = this.state
    const { free_charge_policy } = this.props.setting
    let index = itemIndex
    if (!index) {
      index = 0
    }
    const currentItem = currentMMPDetail.data.items.data[index]
    let currentItemPrice = currentItem.subtotal
    if (item.detail.data.base_price !== item.unit_price) {
      currentItemPrice = item.subtotal
    }
    let diff = (itemSalePrice * item.quantity) - currentItemPrice
    return diff > free_charge_policy ? LocaleFormatter.numberToCurrency(diff) : 'Free'
  }

  async getAvailability(mealId, deliveryDate, quantity) {
    const { kitchenCode } = this.state
    let available = false
    await ApiCatalog.create()
    .getMealAvailability(mealId, deliveryDate, kitchenCode)
    .then((response) => response.data)
    .then((responseBody) => responseBody.data)
    .then((responseData) => {
      if (responseData.remaining_quantity - quantity >= 0) {
        available = true
      }
    })
    .catch()

    return available
  }

  async handleMealSelectPress(meal, deliveryDate) {
    const { item, modifiedMMPDetail, itemIndex, currentMMPDetail } = this.state
    const available = await this.getAvailability(meal.id, deliveryDate, item.quantity)
    if (!available) {
      return alert('Item is out of stock')
    }
    const { free_delivery_policy, free_charge_policy } = this.props.setting
    if (modifiedMMPDetail === null) {
      return false
    }
    let indexItem = itemIndex
    if (!indexItem) {
      indexItem = 0
    }

    const currentItem = currentMMPDetail.data.items.data[indexItem]
    let currentItemPrice = currentItem.subtotal
    if (item.detail.data.base_price !== item.unit_price) {
      currentItemPrice = item.subtotal
    }
    const freeDeliveryPolicy = JSON.parse(free_delivery_policy)
    let modifiedMyMealPlanDetail = JSON.parse(JSON.stringify(modifiedMMPDetail))
    const priceDiff = (meal.sale_price * item.quantity) - currentItemPrice
    if (priceDiff >= free_charge_policy) {
      meal.diff_price = priceDiff
    } else {
      meal.diff_price = 0
    }
    meal = {
      ...meal,
      updated: true,
    }

    let newSubTotal = 0
    modifiedMyMealPlanDetail.data.items.data = modifiedMyMealPlanDetail.data.items.data.map((deliveryItem, index) => {
      if (index === indexItem) {
        let subtotal = currentItemPrice
        if (meal.diff_price > 0) {
          subtotal = meal.sale_price * deliveryItem.quantity
        }
        newSubTotal += subtotal
        const detail = {
          data: meal
        }
        return {
          ...deliveryItem,
          detail,
          unit_price: meal.sale_price,
          subtotal,
        }
      } else {
        newSubTotal += deliveryItem.subtotal
        return deliveryItem
      }
    })

    if (modifiedMyMealPlanDetail.type === 'package') {
      modifiedMyMealPlanDetail.data.subtotal = currentMMPDetail.data.subtotal
      if (priceDiff >= free_charge_policy) {
        modifiedMyMealPlanDetail.data.subtotal += priceDiff
      }
    } else {
      modifiedMyMealPlanDetail.data.subtotal = newSubTotal
    }

    if (newSubTotal >= freeDeliveryPolicy.delivery_subtotal) {
      modifiedMyMealPlanDetail.data.delivery_fee = 0
    }

    this.props.updateModifiedMmp(modifiedMyMealPlanDetail)
    this.props.navigation.goBack()
  }

  renderProductCard(meal, mealIndex, categoryIndex, mealLength) {
    const tags = meal.tags.data
    const haveTags = tags.length > 0
    const { deliveryDate, item } = this.state
    const lastProductMargin = mealIndex === mealLength - 1 ? { marginBottom: moderateScale(60) } : {}
    const isPurchasedMeal = item.detail.data.id === meal.id
    const diffPrice = this.getPriceDifference(meal.sale_price)

    const remainingStock = StockService.getItemStock(meal.id, this.props.meals.stock.main)
    const available = remainingStock - item.quantity >= 0

    return (
      <TouchableWithoutFeedback testID={`meal_${categoryIndex}_${mealIndex}`} onPress={() => this.handleRowItemPress(meal)} key={mealIndex}>
        <View>
          <View>
            {this.renderProductImage(meal)}
          </View>
          {haveTags && this.renderProductTags(tags)}
          <View style={{ marginTop: 5 }}>
            <Text style={Styles.mealNameText}>{meal.name}</Text>
          </View>
          <View style={[Styles.priceContainer, lastProductMargin]}>
            <View style={{ flex: 5, justifyContent: 'center' }}>
              {diffPrice === 'Free' ? (
                <Text style={[Styles.priceText, { color: Colors.grennBlue }]}>{diffPrice}</Text>
              ) : (
                  <Text style={Styles.priceText}>+{diffPrice}</Text>
                )}
            </View>
            <View style={{ flex: 2, alignItems: 'flex-end' }}>
              {isPurchasedMeal
                ? (
                  <Text style={{ color: Colors.grennBlue }}>Purchased</Text>
                )
                : (
                  <MealModifyCardButton
                    mealIndex={mealIndex}
                    meal={meal}
                    available={available}
                    onSelectPress={() => this.handleMealSelectPress(meal, deliveryDate)}
                    deliveryDate={deliveryDate}
                  />
                )
              }
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  renderLoadingView() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator size='large' />
        <Text style={{ fontFamily: 'Rubik-Regular', marginTop: moderateScale(5), color: Colors.brownishGrey }}>Loading...</Text>
      </View>
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render() {
    const { fetchingMeals, categories, deliveryDate, showMealDetailModal } = this.state
    const readableDeliveryDate = moment(deliveryDate).format('ddd, DD MMM')
    return (
      <View testID='MealListScreen' style={{ flex: 1 }}>
        <NavigationBar
          title={'Select Product'}
          leftSideNavigation={this.navigationGoBack}
          leftSide={'back'}
          rightSide={() => this.filterButton()}
        />
        <View style={{ flexGrow: 1, borderTopWidth: 0.5, borderTopColor: Colors.lightGrey, paddingTop: 13 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }} />
            <Text
              style={{
                flex: 1,
                color: Colors.greyishBrownThree,
                textAlign: 'center',
                marginBottom: 8,
                paddingVertical: 8,
                fontSize: 14,
              }}
            >
              {readableDeliveryDate}
            </Text>
            <View style={{ flex: 1 }} />
          </View>
          {
            !fetchingMeals
              ? this.renderCategoryTabs(categories)
              : this.renderLoadingView()
          }
        </View>
        <Modal
          animationType='slide'
          transparent={true}
          visible={this.state.showTagFilter}
          onRequestClose={() => this.setState({ showTagFilter: false })}
        >
          {this.renderTagFilterModal()}
        </Modal>
        {this.state.mealDetailId !== null &&
          <StaticMealDetailPopUp
            closeModal={() => this.setState({ showMealDetailModal: false })}
            visible={showMealDetailModal}
            mealId={this.state.mealDetailId}
            date={deliveryDate}
            catalogScreenMealDetailPopUp={this.state.showPopUpMealDetail}
          />
        }
        <OfflineModal isConnected={this.props.isConnected} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    meals: state.meals,
    isConnected: state.network.isConnected,
    setting: state.setting.data,
    currentMMPDetail: state.myMealPlan.myMealPlanDetail,
    modifiedMMPDetail: state.myMealPlan.modifiedMyMealPlanDetail,
    myMealPlan: state.myMealPlan
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMeals: (date, kitchenCode) => dispatch(MealsActions.fetchMeals(date, kitchenCode)),
    setInitialModifiedMyMealPlan: () => dispatch(MyMealPlanActions.setInitialModifiedMyMealPlan()),
    fetchFilteredMeals: (date, includeTags, excludeTags) => dispatch(MealsActions.fetchFilteredMeals(date, includeTags, excludeTags)),
    updateModifiedMmp: (modifiedMyMealPlanDetail) => dispatch(MyMealPlanActions.updateModifiedMmp(modifiedMyMealPlanDetail))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithSafeAreaView(MyMealPlanModifyCatalogScreen))
