import * as React from 'react'
import {
  Dimensions,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { Section } from '../../../../Components/Layout'
import BannerPlaceholder from '../../Placeholder/Banner'
import { scale, verticalScale } from 'react-native-size-matters'
import { Colors } from '../../../../Themes'
import { Banner } from '../../../../Redux/PromotionRedux'
import api from '../../../../Services/ApiPromotion'
import homeStyles from '../../../Styles/HomeScreenStyle'

export interface BannerProps {
  onPressed: (id: number, title: string) => void,
}

export interface StateInterface {
  banners: Banner[],
  fetching: boolean,
  error: boolean,
  active: number,
}

const { width: VIEWPORT_WIDTH } = Dimensions.get('window')
const BANNER_WIDTH = scale(310)
const BANNER_HEIGHT = verticalScale(140)

class BannerSection extends React.Component<BannerProps, StateInterface> {
  sliderRef

  state = {
    banners: [],
    fetching: false,
    error: false,
    active: 0,
  }

  componentDidMount(): void {
    this._fetchBanners()
  }

  _fetchBanners = () => {
    this.setState({
      fetching: true,
    }, () => {
      api.create()
        .getPromotionBanners()
        .then(response => response.data)
        .then(responseBody => {
          this.setState({
            fetching: false,
            banners: responseBody.data,
          })
        })
        .catch(error => error)
    })
  }

  _snapHandler = (index) => {
    this.setState({
      active: index,
    })
  }

  _handleSliderRef = (ref) => {
    this.sliderRef = ref
  }

  _renderBanner = (_banner, _index) => {
    const {onPressed} = this.props
    return (
      <TouchableWithoutFeedback
        onPress={() => onPressed(_banner.id, _banner.title)}
        key={_index}
        style={{
          width: BANNER_WIDTH,
          height: BANNER_HEIGHT,
          paddingHorizontal: 8,
          paddingBottom: 18,
        }}
      >
        <View style={[
          homeStyles.slider,
          {
            width: BANNER_WIDTH,
            height: BANNER_HEIGHT,
            borderRadius: 8,
            overflow: 'hidden',
          },
        ]}>
          <Image
            source={{ uri: _banner.banner_image }}
            style={{
              ...StyleSheet.absoluteFillObject,
              resizeMode: 'cover',
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    )
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      banners,
      fetching,
      active,
    } = this.state
    return (
      <Section
        full={true}
      >
        {fetching === true
          ? <BannerPlaceholder/>
          : (
            <View
              style={{
                flex: 1,
                paddingVertical: verticalScale(16),
              }}
            >
              <Carousel
                ref={this._handleSliderRef}
                data={banners}
                renderItem={({ item, index }) => this._renderBanner(item, index)}
                sliderWidth={VIEWPORT_WIDTH}
                itemWidth={BANNER_WIDTH}
                firstItem={0}
                loop={true}
                loopClonesPerSide={2}
                onSnapToItem={this._snapHandler}
                enableMomentum={true}
              />
              <Pagination
                dotsLength={banners.length}
                activeDotIndex={banners.length > 0
                  ? active
                  : 0
                }
                containerStyle={{
                  paddingTop: 0,
                  paddingBottom: 0,
                  paddingLeft: 0,
                  marginLeft: -(
                    BANNER_WIDTH - scale(40) - (
                      scale(banners.length * 13)
                    )
                  ), // hicks
                  paddingRight: 0,
                  bottom: verticalScale(15),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                dotColor={Colors.white}
                dotContainerStyle={{
                  paddingTop: 0,
                  paddingBottom: 0,
                  paddingLeft: 0,
                  paddingRight: 0,
                  marginTop: 0,
                  marginBottom: 0,
                  marginLeft: 0,
                  marginRight: 8,
                }}
                dotStyle={{
                  width: scale(6),
                  height: scale(6),
                  borderRadius: 5,
                }}
                inactiveDotColor={Colors.white}
                inactiveDotOpacity={0.4}
                inactiveDotScale={1}
                carouselRef={this.sliderRef}
                tappableDots={(banners.length > 0)}
              />
            </View>
          )
        }
      </Section>
    )
  }
}

export default BannerSection
