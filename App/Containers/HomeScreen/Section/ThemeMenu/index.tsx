import * as React from 'react'
import { ActivityIndicator, View } from 'react-native'
import { MenuSlider } from '../../../../Components/Common'
import { Section, SectionHeader } from '../../../../Components/Layout'
import { Themes } from '../../../../Redux/HomeContentRedux'
import { scale, verticalScale } from 'react-native-size-matters'
import api, { GetThemeListResponse } from '../../../../Services/ApiCatalog'
import { ApiResponse } from 'apisauce'
import { baseLastUsed } from '../../../../Redux/V2/LastUsedRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'

export interface ThemeMenuProps {
  renderItem: (item) => JSX.Element,
  lastUsed: baseLastUsed,
  navigation: NavigationDrawerProp,
}

export interface StateInterface {
  themes: Themes[],
}

class ThemeMenu extends React.PureComponent<ThemeMenuProps, StateInterface> {
  willFocusSubscription

  state = {
    themes: [],
  }

  componentDidMount(): void {
    this._fetchThemes()
    this.willFocusSubscription = this.props.navigation
      .addListener('willFocus', this._fetchThemes)
  }

  _fetchThemes = () => {
    api.create()
      .getThemeList(this.props.lastUsed.code)
      .then((response: ApiResponse<GetThemeListResponse>) => response.data)
      .then(responseBody => {
        this.setState({
          themes: responseBody.data,
        })
      })
      .catch((error: ApiResponse<GetThemeListResponse>) => error)
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      renderItem,
    } = this.props

    const {
      themes,
    } = this.state
    return (
      <Section
        style={{ paddingTop: verticalScale(20) }}
        full={true}
        header={(
          <SectionHeader
            style={{
              marginHorizontal: scale(20),
            }}
            captionText="More Subscription"
            titleText="Food Playlists"
          />
        )}
      >
        {themes === null
          ?
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: verticalScale(581),
            }}
          >
            <ActivityIndicator size='small' color='#ff5100'/>
          </View>
          :
          <View>
            <MenuSlider
              style={{
                paddingHorizontal: scale(20),
                paddingBottom: verticalScale(16),
              }}
              items={themes}
              renderItem={renderItem}
              maxRender={6}
              initialRender={3}
              windowSize={1}
              removeClippedSubviews={true}
            />
          </View>
        }
      </Section>
    )
  }
}

export default ThemeMenu
