import * as React from 'react'
import { Image, View } from 'react-native'
import { SectionHeader } from '../../../../Components/Layout'
import { Colors, Images } from '../../../../Themes'
import { scale } from 'react-native-size-matters'

export interface propsInterface {
  title: string,
  captionText: string,
}

const SectionHead: React.FC<propsInterface> = ({title = '', captionText='Chef’s Recommendation'}) => {
  return (
    <SectionHeader
      style={{
        marginHorizontal: scale(20),
        backgroundColor: Colors.yellowishOrange,
      }}
      captionTextStyle={{ color: Colors.white }}
      titleTextStyle={{ color: Colors.white }}
      leftStyle={{
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: scale(15),
      }}
      captionText={captionText}
      titleText={title}
      leftIcon={
        <View
          style={{
            width: scale(40),
            height: scale(40),
            borderRadius: 50,
            backgroundColor: Colors.white,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            source={Images.favourite}
            style={{ width: scale(16) }}
            resizeMode='contain'
          />
        </View>
      }
    />
  )
}

export default SectionHead
