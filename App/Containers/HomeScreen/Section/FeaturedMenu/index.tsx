import * as React from 'react'
import { Dimensions, Text, View } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { MenuSlider } from '../../../../Components/Common'
import LocaleFormatter from '../../../../Services/LocaleFormatter'
import PrimaryButton from '../../../../Components/PrimaryButton'
import { Section } from '../../../../Components/Layout'
import { Colors } from '../../../../Themes'
import SectionHead from './SectionHeader'
import {
  Item,
  ThemeDetail,
} from '../../../../Redux/HomeContentRedux'
import FeaturedPlaceHolder from '../../Placeholder/FeaturedPlaceholder'
import FeaturedThemePlaceholder
  from '../../Placeholder/FeaturedThemePlaceholder'
import ThemeApi, { GetThemeDetailResponse } from '../../../../Services/ApiTheme'
import { ApiResponse } from 'apisauce'
import moment  from 'moment'
import _ from 'lodash'
import { baseLastUsed } from '../../../../Redux/V2/LastUsedRedux'

export interface FeaturedMenuProps {
  navigateToThemesDetail: (detail: ThemeDetail, isChefRecommendation: boolean) => void,
  renderThemeItem: (item) => JSX.Element,
  mealDate: string
  featuredThemeSetting: string,
  lastUsed: baseLastUsed
}

export interface StateInterface {
  items: Item[],
  packageName: string,
  packageBasePrice: number,
  packageSellPrice: number,
  fetching: boolean,
  packageDetail: ThemeDetail,
}

const { width: VIEWPORT_WIDTH } = Dimensions.get('window')

class FeaturedMenu extends React.PureComponent<FeaturedMenuProps, StateInterface> {
  state = {
    fetching: false,
    items: [],
    packageName: '',
    packageBasePrice: 0,
    packageSellPrice: 0,
    packageDetail: null,
  }

  componentDidMount(): void {
    this._fetchFeatured()
  }

  _fetchFeatured = () => {
    const { themeId } = JSON.parse(this.props.featuredThemeSetting)
    let { mealDate } = this.props
    this.setState({
      fetching: true,
    }, () => {
      ThemeApi.create()
        .getThemeDetail(themeId, this.props.lastUsed.code)
        .then((response: ApiResponse<GetThemeDetailResponse>) => response.data)
        .then(responseBody => {
          const date = moment(mealDate)
          const data = responseBody.data
          let items = data.items
          items = items.filter((item) =>
            moment(item.date).isSameOrAfter(date)
          )
          let periods = data.theme_periods
          periods = _.sortBy(periods, ['sell_price'])
          let basePrice = 0
          let sellPrice = 0
          if (periods.length > 0) {
            const firstPackagePeriod = periods[0]
            if (firstPackagePeriod && firstPackagePeriod.base_price) {
              basePrice = firstPackagePeriod.base_price
            }
            if (firstPackagePeriod && firstPackagePeriod.sell_price) {
              sellPrice = firstPackagePeriod.sell_price
            }
          }
          this.setState({
            items: items,
            packageName: data.name,
            packageBasePrice: basePrice,
            packageSellPrice: sellPrice,
            fetching: false,
            packageDetail: data,
          })
        })
        .catch((error: ApiResponse<GetThemeDetailResponse>) => error)
    })
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      fetching,
      items,
      packageBasePrice,
      packageName,
      packageSellPrice,
      packageDetail,
    } = this.state

    const {
      renderThemeItem,
      navigateToThemesDetail,
    } = this.props
    const {
      captionText
    } = JSON.parse(this.props.featuredThemeSetting)
    
    return (
      <Section
        style={{
          paddingTop: verticalScale(20),
          backgroundColor: Colors.yellowishOrange,
        }}
        full={true}
        header={
          <SectionHead
            title={packageName}
            captionText={captionText}
          />
        }
      >
        <View
          style={{
            height: 120,
            backgroundColor: Colors.yellowishOrange,
          }}
        />
        <View
          style={{
            height: 155,
            backgroundColor: Colors.white,
          }}
        />
        {fetching
          ? <FeaturedPlaceHolder/>
          : (
            <View style={{ position: 'absolute' }}>
              <MenuSlider
                style={{
                  paddingHorizontal: 20,
                  paddingBottom: 16,
                  width: VIEWPORT_WIDTH + 8,
                  top: -8, // handle shadow
                  left: -4, // handle shadow
                }}
                items={items}
                renderItem={renderThemeItem}
                maxRender={8}
                initialRender={4}
                removeClippedSubviews={true}
                windowSize={1}
              />
              {(packageSellPrice === 0 && packageBasePrice === 0)
                ? <FeaturedThemePlaceholder/>
                : (
                  <View
                    style={{
                      flexDirection: 'row',
                      paddingHorizontal: scale(20),
                      paddingBottom: verticalScale(16),
                      bottom: verticalScale(18), // handle shadow
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'column',
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: 'Rubik-Regular',
                          fontSize: scale(14),
                          color: Colors.greyishBrownTwo,
                        }}
                      >
                        Package start from
                      </Text>
                      <View
                        style={{
                          marginTop: verticalScale(4),
                          flexDirection: 'row',
                          alignItems: 'flex-end',
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: 'Rubik-Medium',
                            fontSize: scale(16),
                            color: Colors.pinkishOrange,
                            marginRight: scale(4),
                          }}
                        >
                          {LocaleFormatter.numberToCurrency(packageSellPrice)}
                        </Text>
                        {(packageSellPrice < packageBasePrice) && (
                          <Text
                            style={{
                              fontFamily: 'Rubik-Regular',
                              fontSize: scale(12),
                              color: Colors.warmGreyThree,
                              marginRight: scale(4),
                              textDecorationLine: 'line-through',
                              textDecorationStyle: 'solid',
                            }}
                          >
                            {LocaleFormatter.numberToCurrency(packageBasePrice)}
                          </Text>
                        )}
                      </View>
                    </View>
                    <View
                      style={{
                        width: scale(116),
                        height: verticalScale(44),
                      }}
                    >
                      <PrimaryButton
                        label='Subscribe Now'
                        labelStyle={{
                          fontFamily: 'Rubik-Regular',
                          fontSize: scale(14),
                        }}
                        onPressMethod={() => navigateToThemesDetail(
                          packageDetail,
                          true,
                        )}
                        buttonStyle={{ backgroundColor: Colors.pinkishOrange }}
                        testID='subscribeNowChef'
                        accessibilityLabel='subscribeNowChef'
                      />
                    </View>
                  </View>
                )
              }
            </View>
          )
        }
      </Section>
    )
  }
}

export default FeaturedMenu
