import * as React from 'react'
import {
  TouchableWithoutFeedback,
  Image,
  View,
  Platform,
  Dimensions,
} from 'react-native'
import moment, { Moment } from 'moment'
import { ImmutableObject } from 'seamless-immutable'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import MenuList from './MenuList'
import PromoteMenuPlaceholder from '../../Placeholder/PromoteMenu'
import {
  Meal,
  Meals,
  ItemStock,
  Stock,
} from '../../../../Redux/MealsRedux'
import {
  Section,
  SectionHeader,
  SectionFooter,
} from '../../../../Components/Layout'
import {
  Images,
  Colors,
} from '../../../../Themes'
import CartActions, { CartState } from '../../../../Redux/V2/CartRedux'
import api from '../../../../Services/ApiCatalog'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import RNModal from 'react-native-modal'
import NavigationBar from '../../../../Components/NavigationBar'
import { Calendar } from 'react-native-calendars'
import {
  Button,
  Icon,
} from 'native-base'
import { connect } from 'react-redux'
import MealsActions from '../../../../Redux/MealsRedux'
import SalesOrderService from '../../../../Services/SalesOrderService'
import { AppState } from '../../../../Redux/CreateStore'
import { responseData } from '../../../../Services/V2/ApiCart'
import { SettingDataInterface } from '../../../../Redux/SettingRedux'
import ShoppingCartAlertModal from '../../../../Components/ShoppingCartAlertModal'
import { Params } from '../../index'
import { NavigationActions } from 'react-navigation'
import LogEventService from '../../../../Services/LogEventService'
import { baseLastUsed } from '../../../../Redux/V2/LastUsedRedux'

export interface PromotedMenuProps {
  cart: CartState,
  navigation: NavigationDrawerProp,
  stock: ImmutableObject<Stock>,
  setPromotedItemStock: (itemStock: ItemStock[]) => void,
  setting: SettingDataInterface,
  isLoggedIn: boolean,
  addToCart: (shoppingCart: responseData) => void,
  resetCart: () => void,
  loading: (status: boolean) => void,
  removeFromCart: (itemId: number, date: Moment) => void,
  lastUsed: baseLastUsed,
}

export interface StateInterface {
  promoted: Meals,
  fetching: boolean,
  availableDates: object[],
  marketSelected: object[],
  showCalendar: boolean,
  selectedDate: string,
  showCombinationAlertModal: boolean,
  newCartCandidate?: {
    item: Meal,
    date: Moment,
    quantity: number,
  },
}

const { height } = Dimensions.get('window')

class PromotedMenu extends React.PureComponent<PromotedMenuProps, StateInterface> {
  state = {
    promoted: null,
    fetching: false,
    availableDates: [],
    marketSelected: [],
    showCalendar: false,
    selectedDate: '',
    showCombinationAlertModal: false,
    newCartCandidate: null,
  }

  componentDidMount(): void {
    this._fetchCategories()
  }
  _getStock = (meals): ItemStock[] => {
    return  meals.items.data.map((meal) => {
        return {
          item_id: meal.id,
          quantity: meal.availabilities.data[0].remaining_quantity
        }
    })
  }

  _fetchCategories = () => {
    const {
      selectedDate,
    } = this.state
    this.setState({
      fetching: true,
    }, () => {
      api.create()
        .getPromoteCategories(selectedDate, this.props.lastUsed.code)
        .then(response => response.data)
        .then(responseBody => {
          const availableDates = this._getAvailableDates(responseBody.data.available_dates)
          const marked = this._getMarkedSelected(responseBody.data.selected_date)
          this.setState({
            promoted: responseBody.data,
            fetching: false,
            availableDates: availableDates,
            marketSelected: marked,
            selectedDate: responseBody.data.selected_date,
          })
          this.props.setPromotedItemStock(this._getStock(responseBody.data))
        })
        .catch(error => error)
    })
  }

  _setSelectedDate = (date) => {
    this.setState({
      selectedDate: date.dateString,
      showCalendar: false,
    }, this._fetchCategories)
  }

  _getAvailableDates = (availableDates: string[]) => {
    let enabledDays = []
    availableDates.map(date => {
      enabledDays[date] = {
        disabled: false,
        disableTouchEvent: false,
      }
    })
    return enabledDays
  }

  _getMarkedSelected = (selectedDate) => {
    let marked = []
    marked[selectedDate] = { selected: true }
    return marked
  }

  _toggleCalendar = () => {
    this.setState({
      showCalendar: !this.state.showCalendar,
    })
  }

  _closePickerButton = () => {
    return (
      <Button
        transparent
        onPress={this._toggleCalendar}
      >
        <Icon
          name='md-close'
          style={{
            color: Colors.bloodOrange,
            fontSize: scale(28),
          }}
        />
      </Button>
    )
  }

  renderDatePickerModal = () => {
    const { showCalendar, availableDates, marketSelected } = this.state
    const markedDates = { ...availableDates, ...marketSelected }
    const modalContainerHeight = height * 0.75 >= 420
      ? height * 0.75
      : 420
    return (
      <RNModal
        isVisible={showCalendar}
        onBackButtonPress={() => this._toggleCalendar()}
        onBackdropPress={() => this._toggleCalendar()}
        backdropColor={Platform.OS === 'ios'
          ? 'rgba(255,255,255,0.9)'
          : 'rgba(58, 60, 63, 0.7)'}
        backdropOpacity={1}
        animationIn={'slideInDown'}
        animationOut={'slideOutUp'}
        animationInTiming={300}
        animationOutTiming={300}
        backdropTransitionInTiming={300}
        backdropTransitionOutTiming={300}
        style={{
          marginVertical: 0,
          marginHorizontal: 0,
        }}
      >
        <>
          <View style={{
            height: modalContainerHeight,
            backgroundColor: 'white',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowRadius: 4,
            shadowOpacity: 1,
            shadowColor: 'rgba(174,174,174, 0.5)',
          }}>
            <NavigationBar
              title='Select Date'
              leftSide={() => this._closePickerButton()}
            />
            <View style={{ flex: 4 }}>
              <Calendar
                markedDates={markedDates}
                hideExtraDays
                disabledByDefault
                onDayPress={this._setSelectedDate}
                style={{ marginHorizontal: scale(30) }}
                theme={{
                  arrowColor: Colors.green,
                  monthTextColor: Colors.green,
                  selectedDayTextColor: Colors.white,
                  dayTextColor: Colors.greyishBrownTwo,
                  textSectionTitleColor: Colors.greyishBrownTwo,
                  textMonthFontFamily: 'Rubik-Regular',
                  textDayFontFamily: 'Rubik-Regular',
                  textDayHeaderFontFamily: 'Rubik-Regular',
                  selectedDayBackgroundColor: Colors.pinkishOrange,
                  selectedDotColor: Colors.bloodOrange,
                }}
              />
            </View>
          </View>
          <View style={{ flex: 1 }} />
        </>
      </RNModal>
    )
  }

  showCombineAlert = (
    item: Meal,
    date: Moment,
    quantity: number,
  ) => {
    this.setState({
      showCombinationAlertModal: true,
      newCartCandidate: {
        item: item,
        quantity: quantity,
        date: date,
      },
    })
  }

  onPressClearCart = () => {
    this.props.resetCart()
    setTimeout(() => {
      if (this.state.newCartCandidate) {
        const {
          cart,
          loading,
          resetCart,
          addToCart,
          lastUsed,
        } = this.props

        const {
          item,
          date,
          quantity,
        } = this.state.newCartCandidate

        SalesOrderService.addToCart(
          cart,
          item,
          date,
          quantity,
          lastUsed.code,
          loading,
          resetCart,
          addToCart,
        )

        this.setState({
          newCartCandidate: null,
        })
      }
    }, 100)
  }

  onPressCompleteOrder = () => {
    const {
      isLoggedIn,
      cart,
      navigation,
    } = this.props
    if (isLoggedIn) {
      if (cart.type === 'subscription') {
        navigation.navigate('HomeSubscriptionSchedule')
      } else {
        navigation.navigate('SingleOrderCheckoutScreen')
      }
    } else {
      navigation.navigate('Login')
    }
  }

  handleCatalogItemPress = (_selectedMealIndex, _meals, _category, _exclusive) => {
    const mealBaseOnIndex: Meal[] = _meals.filter((item, index) => index === _selectedMealIndex)
    const params: Params = {
      meal_id: mealBaseOnIndex[0].id,
      meal_name: mealBaseOnIndex[0].name,
      meal_category_name: _category,
    }
    const navigationAction = NavigationActions.navigate({
      routeName: 'MealDetailScreen',
      params: {
        meals: _meals,
        selectedMealIndex: _selectedMealIndex,
        category: _category,
        exclusive: _exclusive || _meals[_selectedMealIndex].exclusive || this.props.cart.exclusive,
        selectedDate: this.state.selectedDate,
        transition: Platform.OS,
      },
    })
    LogEventService.logEvent('click_meal_detail', params, ['default'])
    this.props.navigation.dispatch(navigationAction)
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      cart,
      navigation,
      loading,
      resetCart,
      addToCart,
      isLoggedIn
    } = this.props

    const {
      promoted,
      fetching,
      showCombinationAlertModal,
    } = this.state
    if (!promoted) {
      return <View />
    } else {
      return (
        <View>
          {this.renderDatePickerModal()}

          <Section
            style={{
              paddingTop: verticalScale(16),
              paddingBottom: verticalScale(20),
            }}
            full={true}
            header={(
              <SectionHeader
                style={{
                  marginHorizontal: scale(20),
                }}
                captionText={moment(promoted.selected_date)
                  .format('ddd, DD MMM YYYY')}
                titleText={promoted.name}
                titleRight={(
                  <TouchableWithoutFeedback onPress={this._toggleCalendar}>
                    <Image source={Images.arrowDown} />
                  </TouchableWithoutFeedback>
                )}
              />
            )}
            footer={(promoted.note !== '') && (
              <SectionFooter
                text={promoted.note}
              />
            )}
          >
            {fetching
              ? <PromoteMenuPlaceholder />
              : (
                <MenuList
                  promoteCategories={promoted}
                  handleCatalogItemPress={this.handleCatalogItemPress}
                  cart={cart.cartItems.length
                    ? cart.cartItems
                    : []}
                  mealDate={moment(promoted.selected_date)}
                  onCartIncrease={(item, date, quantity) => SalesOrderService
                    .incrementHandler(
                      item,
                      date,
                      quantity,
                      navigation,
                      loading,
                      resetCart,
                      addToCart,
                      () => this.showCombineAlert(item, date, quantity),
                      isLoggedIn,
                      cart,
                      () => SalesOrderService.logEvent(item, quantity, promoted.name),
                      promoted.name,
                      this.props.lastUsed.code,
                    )}
                  onCartDecrease={(item, date, quantity) => SalesOrderService
                    .decreaseHandler(
                      item,
                      date,
                      quantity,
                      loading,
                      resetCart,
                      addToCart,
                      cart,
                      null,
                      this.props.removeFromCart,
                      this.props.lastUsed.code,
                    )}
                  onMaxItemEvent={SalesOrderService.onMaxItem}
                  navigation={navigation}
                  stock={this.props.stock}
                />
              )
            }
          </Section>

          <ShoppingCartAlertModal
            primaryAction={this.onPressCompleteOrder}
            secondaryAction={this.onPressClearCart}
            visible={showCombinationAlertModal}
            dismiss={() => this.setState({
              showCombinationAlertModal: false,
            })}
          />
        </View>
      )
    }
  }
}

const mapStateToProps = (state: AppState) => ({
  stock: state.meals.stock,
  setting: state.setting.data,
  isLoggedIn: state.login.token !== null,
  cart: state.cart,
  lastUsed: state.lastUsed,
})

const mapDispatchToProps = dispatch => ({
  setPromotedItemStock: (stock: ItemStock[]) =>
    dispatch(MealsActions.setPromotedItemStock(stock)),
  addToCart: (shoppingCart: responseData) => dispatch(CartActions.addToCart(shoppingCart)),
  removeFromCart: (itemId: number, date: Moment) => dispatch(CartActions.removeFromCart(itemId, date)),
  resetCart: () => dispatch(CartActions.resetCart()),
  loading: (status: boolean) => dispatch(CartActions.loading(status)),
})

export default connect(mapStateToProps,mapDispatchToProps)(PromotedMenu)
