import * as React from 'react'
import {
  Dimensions,
  View,
} from 'react-native'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'

import {
  MealCard,
  MenuSlider,
} from '../../../../Components/Common'
import CartService from '../../../../Services/MealCartService'
import { Meal, Meals, Stock } from '../../../../Redux/MealsRedux'
import { Delivery } from '../../../../Redux/V2/CartRedux'
import { Moment } from 'moment'
import MenuCard from '../Categories/MenuCard'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import StockService from '../../../../Services/StockService'
import { ImmutableObject } from 'seamless-immutable'

export interface MenuListProps {
  promoteCategories: Meals
  handleCatalogItemPress: (index, meals, category: string, exclusive: boolean, promoted: string) => void
  cart?: Delivery[]
  mealDate: Moment
  onCartIncrease: (item: Meal, date: Moment, quantity: number) => void
  onCartDecrease: (item: Meal, date: Moment, quantity: number) => void
  onMaxItemEvent?: (maximum: number, remaining: number) => void
  max?: number,
  navigation: NavigationDrawerProp,
  stock: ImmutableObject<Stock>
}

class MenuList extends React.PureComponent<MenuListProps> {


  renderActiveCardContent = (_item, _category, _exclusive) => {
    const {
      cart,
      mealDate,
      onCartDecrease,
      onMaxItemEvent,
      onCartIncrease,
      navigation,
      stock
    } = this.props
    let itemQty = 0
    let isAvailable = false
    const availability = StockService.getItemStock(_item.id, stock.promoted)
    let inCartQty = CartService.getItemQuantity(cart, _item.id, mealDate)
    let maxItem = availability - inCartQty

    if (availability != inCartQty) {
      isAvailable = true
    }

    if (CartService.isItemExistInCart(cart, _item, mealDate)) {
      const cartItem = CartService.getCartItem(cart, _item,
        mealDate.format('YYYY-MM-DD'))

      if (cartItem !== null) {
        itemQty = cartItem.quantity
        maxItem = maxItem + itemQty
        isAvailable = StockService.isAvailableStock(stock.promoted, _item, mealDate, itemQty, cart)
      }
    }

    return (
      <MenuCard
        item={_item}
        mealDate={mealDate}
        category={_category}
        exclusive={_exclusive}
        max={maxItem}
        itemQty={itemQty}
        onCartIncrease={onCartIncrease}
        onCartDecrease={onCartDecrease}
        onMaxItemEvent={onMaxItemEvent}
        navigation={navigation}
        available={isAvailable}
        availability={availability}
      />
    )
  }

  renderItem = (_meals, _item, _index, _category, _exclusive) => {
    const {
      handleCatalogItemPress,
      mealDate,
    } = this.props
    const image = (
      _item.hasOwnProperty('catalog_image')
    )
      ?
      _item.catalog_image.medium_thumb.replace('_medium_thumb', '')
      :
      _item.hasOwnProperty('images')
        ? _item.images.medium_thumb.replace(
        '_medium_thumb', '')
        : null
    const title = _item.name

    return (
      <MealCard
        style={{
          height: 288,
          borderRadius: 8,
          marginTop: 8, // handle shadow
          marginBottom: 12, // handle shadow
          marginHorizontal: 4, // handle shadow
        }}
        contentStyle={{
          height: 128,
          paddingTop: 4,
        }}
        image={image}
        title={title}
        description={this.renderActiveCardContent(_item, _category, _exclusive)}
        onPress={() => handleCatalogItemPress(_index, _meals, _category, _exclusive, mealDate.format('YYYY-MM-DD'))}
      />
    )
  }


  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const { promoteCategories } = this.props
    const items = promoteCategories.items.data
    const { name, exclusive } = promoteCategories
    if (!items || items.length === 0) {
      return (
        <View
          style={{
              height: 288,
              marginTop: 33,
          }}
        />
      )
    }

    return (
      <MenuSlider
        style={{
          paddingTop: 0,
          paddingHorizontal: scale(20),
          paddingBottom: 0,
          width: Dimensions.get('window').width + 8, // handle shadow
          top: verticalScale(-8), // handle shadow
          left: scale(-4), // handle shadow
        }}
        items={items}
        renderItem={({ item, index }) => this.renderItem(items, item, index, name, exclusive)}
        maxRender={8}
        initialRender={4}
      />
    )
  }
}

export default MenuList
