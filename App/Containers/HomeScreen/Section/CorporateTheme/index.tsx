import * as React from 'react'
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native'
import homeStyles from '../../../Styles/HomeScreenStyle'
import * as Progress from 'react-native-progress'
import { Section } from '../../../../Components/Layout'
import { CorporateThemes } from '../../../../Redux/HomeContentRedux'
import api, { GetThemeListResponse } from '../../../../Services/ApiCatalog'
import { ApiResponse } from 'apisauce'

export interface CorporateThemeProps {
  navigateToThemesDetail: (item, isChefRecommendation) => void,
  token: string,
}

export interface StateInterface {
  themes: CorporateThemes[],
}

class CorporateTheme extends React.PureComponent<CorporateThemeProps, StateInterface> {
  state = {
    themes: [],
  }

  componentDidMount(): void {
    this._fetchTheme()
  }

  _fetchTheme = () => {
    api.create()
      .getCorporateThemeList(this.props.token)
      .then((response: ApiResponse<GetThemeListResponse>) => response.data)
      .then(responseBody => {
        this.setState({
          themes: responseBody.data,
        })
      })
      .catch((error: ApiResponse<GetThemeListResponse>) => error)
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      navigateToThemesDetail,
    } = this.props

    const {
      themes,
    } = this.state
    if (themes.length <= 0) {
      return <View/>
    }
    return (
      <Section
        padder={true}
      >
        {themes.map((item) => {
          return (
            <View key={item.id}>
              <View style={homeStyles.textBox}>
                <Text style={homeStyles.text}>{item.name}</Text>
              </View>
              <TouchableWithoutFeedback
                onPress={() => navigateToThemesDetail(item, false)}>
                <View style={homeStyles.corporateThemeSection}>
                  <Image
                    source={{ uri: item.detail_image }}
                    style={homeStyles.corporateThemeImage}
                    loadingIndicatorSource={{ uri: Progress.CircleSnail.toString() }}
                    borderRadius={3}
                    resizeMode='contain'
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
          )
        })}
      </Section>
    )
  }
}

export default CorporateTheme
