import * as React from 'react'
import {
  TouchableWithoutFeedback,
  StyleSheet,
  Text,
  View, ViewStyle, TextStyle,
} from 'react-native'
import Colors from '../../../../Themes/Colors'

export interface ActiveMenuTab {
  name: string,
  page: number,
}

export interface CustomTabBarProps {
  goToPage?: (page, name) => void,
  activeTab?: number,
  tabs?: ActiveMenuTab[],
  backgroundColor?: string,
  activeTextColor?: string,
  inactiveTextColor?: string,
  style?: ViewStyle,
  textStyle?: TextStyle,
  tabStyle?: ViewStyle,
  renderTab?: (name, page, isTabActive, goToPage, tabStyles) => JSX.Element,
  activeTabStyle?: ViewStyle,
  activeTextStyle?: TextStyle,
  navigationState?: any
}

const renderTabFunction = (name, page, isTabActive, onPressHandler, tabStyles) => {
  const { activeTextColor, inactiveTextColor, textStyle, activeTabStyle, tabStyle, activeTextStyle } = tabStyles
  const textColor = isTabActive ? activeTextColor : inactiveTextColor
  const fontFamily = isTabActive ? 'Rubik-Medium' : 'Rubik-Light'

  let normalizeTabText = {...textStyle}
  let normalizeActiveTabText = {...activeTextStyle}

  let containerStyle = {
    ...styles.tab,
    ...tabStyle
  }
  let tabsTextStyle = {
    ...normalizeTabText,
    color: textColor,
    fontFamily,
  }
  if (isTabActive) {
    containerStyle = {
      ...containerStyle,
      ...activeTabStyle
    }
    tabsTextStyle = {
      ...tabsTextStyle,
      ...normalizeActiveTabText,
    }
  }
  return (
    <TouchableWithoutFeedback
      key={name.toLowerCase()}
      testID={name.toLowerCase()}
      accessibilityLabel={name.toLowerCase()}
      accessible={true}
      accessibilityTraits='button'
      onPress={() => onPressHandler(page, name)}
    >
      <View style={containerStyle}>
        <Text style={tabsTextStyle}>
          {name}
        </Text>
        {isTabActive &&
        <View
          style={{
            marginTop: 8,
            height: 0,
            borderWidth: 2,
            borderColor: Colors.pinkishOrange,
            borderRadius: 2,
          }}
        />
        }
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  tab: {},
  tabs: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})

const CustomTabBar: React.FC<CustomTabBarProps> = (props) => {
  const {
    activeTextColor = 'navy', inactiveTextColor = 'black', backgroundColor = ''
    , goToPage, activeTab, navigationState
    , textStyle, tabStyle, renderTab
    , style, activeTabStyle, activeTextStyle,
  } = props

  const tabStyles = {
    activeTextColor,
    inactiveTextColor,
    textStyle,
    tabStyle,
    activeTabStyle,
    activeTextStyle,
  }

  return (
    <View
      style={{
        ...styles.tabs,
        backgroundColor: backgroundColor,
        ...style
      }}
    >
      {navigationState.routes.map((route, index) => {
        const isTabActive = activeTab === index
        const renderFunction = renderTab || renderTabFunction
        return renderFunction(route.title, index, isTabActive, goToPage, tabStyles)
      })}
    </View>
  )
}

export default CustomTabBar
