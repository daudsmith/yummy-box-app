import * as React from 'react'
import {
  Dimensions,
  Platform,
} from 'react-native'
import moment, { Moment } from 'moment'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import ActiveMenuPlaceholder from '../../Placeholder/ActiveMenu'
import SeeAllButton from './SeeAllButton'
import MenuList from './MenuList'
import Retry from './Retry'
import MealsActions, {
  ItemStock,
  Meal,
  Stock,
} from '../../../../Redux/MealsRedux'
import {
  Section,
  SectionHeader,
} from '../../../../Components/Layout'
import CartActions, {
  CartState,
} from '../../../../Redux/V2/CartRedux'
import apiCatalog, {
  CategoryResponseData,
  GetCatalogCategoriesResponse,
} from '../../../../Services/ApiCatalog'
import { ApiResponse } from 'apisauce'
import { connect } from 'react-redux'
import { AppState } from '../../../../Redux/CreateStore'
import { SettingDataInterface } from '../../../../Redux/SettingRedux'
import { baseLastUsed } from '../../../../Redux/V2/LastUsedRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import AvailabilityDateService from '../../../../Services/AvailabilityDateService'
import LogEventService from '../../../../Services/LogEventService'
import { NavigationActions } from 'react-navigation'
import { Params } from '../../index'
import { ImmutableObject } from 'seamless-immutable'
import SalesOrderService from '../../../../Services/SalesOrderService'
import { responseData } from '../../../../Services/V2/ApiCart'
import { AnyAction } from 'redux'
import { Toast } from 'native-base'

const { width: VIEWPORT_WIDTH } = Dimensions.get('window')

export interface CategoriesProps {
  setting: SettingDataInterface;
  cart: CartState;
  navigation: NavigationDrawerProp;
  stock: ImmutableObject<Stock>;
  setItemStock: (stock: ItemStock[]) => AnyAction;
  addToCart: (shoppingCart) => AnyAction;
  resetCart: () => AnyAction;
  loading: (status: boolean) => AnyAction;
  isLoggedIn: boolean;
  removeFromCart: (id: number, date: Moment) => void;
  lastUsed: baseLastUsed;
  showCombineAlert: (item: Meal, date: Moment, quantity: number) => void;
}

export interface CategoriesState {
  fetching: boolean,
  error: boolean,
  startedObserveCutOffTime: boolean,
  meals: CategoryResponseData[],
  mealDate: Moment,
  dateLabel: string,
  activeTab: number,
  maxItem: number,
  newCartCandidate?: {
    item: Meal,
    date: Moment,
    quantity: number,
  },
}

class Categories extends React.Component<CategoriesProps, CategoriesState> {
  willFocusSubscription

  state = {
    fetching: false,
    error: false,
    startedObserveCutOffTime: false,
    meals: [],
    mealDate: moment()
      .utcOffset(7)
      .add(1, 'days'),
    dateLabel: 'Tomorrow’s Menu',
    activeTab: 0,
    maxItem: 0,
    newCartCandidate: null,
  }

  componentDidMount(): void {
    this._setMealDate()
    this.willFocusSubscription = this.props.navigation
      .addListener('willFocus', this._fetchCategories)
    const {
      sales_order_total_limit,
    } = this.props.setting
    let limit = JSON.parse(sales_order_total_limit)
    if (typeof limit['item'] !== 'undefined'
      && typeof limit['item']['max'] === 'number'
    ) {
      this.setState({
        maxItem: limit['item']['max'],
      })
    }
  }

  componentWillUnmount(): void {
    if (this.willFocusSubscription) {
      this.willFocusSubscription.remove()
    }
  }

  increaseCart = async (item, date, quantity, categoryName) => {
    const itemAvailability = await SalesOrderService.getItemAvailability(item.id, date, this.props.lastUsed.code, quantity)

    if (!itemAvailability) {
      Toast.show({
        text: 'Someone else has just bought our last stock for this location',
        buttonText: '',
        duration: 1500,
      })
    }
    else {
      SalesOrderService
        .incrementHandler(
          item,
          date,
          quantity,
          this.props.navigation,
          this.props.loading,
          this.props.resetCart,
          this.props.addToCart,
          () => this.props.showCombineAlert(item, date, quantity),
          this.props.isLoggedIn,
          this.props.cart,
          () => SalesOrderService.logEvent(item, quantity, categoryName),
          categoryName,
          this.props.lastUsed.code,
        )
    }
  }

  _setMealDate = () => {
    const { mealDate } = this.state
    let newMealDate = mealDate

    if (typeof this.props.setting.off_dates !== 'undefined') {
      newMealDate = AvailabilityDateService.getInitialDates(
        mealDate,
        this.props.setting.off_dates,
        this.props.setting.delivery_cut_off,
      )
    }

    this.setState({
      mealDate: newMealDate,
      dateLabel: newMealDate.isAfter(mealDate)
        ? newMealDate.format('dddd') + '\'s'
        : 'Tomorrow’s Menu',
    }, this._fetchCategories)
  }

  _navigateToMealListScreen = (_selectedDate: Moment = null, _tab = 0, _meals: Meal[]) => {
    const navigationAction = NavigationActions.navigate({
      routeName: 'MealListScreen',
      params: {
        startDate: '',
        selectedDate: _selectedDate,
        tab: _tab,
        meals: _meals
      },
    })
    this.props.navigation.dispatch(navigationAction)
    LogEventService.logEvent('click_see_all_menu', null, ['default'])
  }

  _handleCatalogItemPress = (_selectedMealIndex, _meals, _category, _exclusive) => {
    const mealBaseOnIndex: Meal[] = _meals.filter((item, index) => index === _selectedMealIndex)
    const params: Params = {
      meal_id: mealBaseOnIndex[0].id,
      meal_name: mealBaseOnIndex[0].name,
      meal_category_name: _category,
    }
    const navigationAction = NavigationActions.navigate({
      routeName: 'MealDetailScreen',
      params: {
        meals: _meals,
        selectedMealIndex: _selectedMealIndex,
        category: _category,
        exclusive: _exclusive || _meals[_selectedMealIndex].exclusive || this.props.cart.exclusive,
        selectedDate: this.state.mealDate,
        transition: Platform.OS,
      },
    })
    LogEventService.logEvent('click_meal_detail', params, ['default'])
    this.props.navigation.dispatch(navigationAction)
  }

  _setActiveTab = (index, name?: string) => {
    const params: Params = {
      category_id: index,
      category_name: name,
    }
    this.setState({
      activeTab: index,
    }, () => {
      LogEventService.logEvent('click_category', params, ['default'])
    })
  }

  _getStock = (meals): ItemStock[] => {
    return meals.map((meal) => {
      return meal.items.data.map((item) => {
        return {
          item_id: item.id,
          quantity: item.daily_remaining_quantity,
        }
      })
    })
      .flat()
  }

  _fetchCategories = () => {
    const { mealDate } = this.state

    this.setState({
      fetching: true,
      error: false,
    }, () => {
      apiCatalog.create()
        .getCatalogCategories(mealDate.format('YYYY-MM-DD'), this.props.lastUsed.code)
        .then((response: ApiResponse<GetCatalogCategoriesResponse>) => response.data)
        .then((responseBody: GetCatalogCategoriesResponse) => {
          if (!responseBody.error) {
            this.setState({
              fetching: false,
              meals: responseBody.data,
            })
            this.props.setItemStock(this._getStock(responseBody.data))
          } else {
            this.setState({
              fetching: false,
              meals: [],
              error: true,
            })
          }
        })
    })
  }

  onPressClearCart = () => {
    this.props.resetCart()
    setTimeout(() => {
      if (this.state.newCartCandidate) {
        const {
          cart,
          loading,
          resetCart,
          addToCart,
          lastUsed,
        } = this.props

        const {
          item,
          date,
          quantity,
        } = this.state.newCartCandidate

        SalesOrderService.addToCart(
          cart,
          item,
          date,
          quantity,
          lastUsed.code,
          loading,
          resetCart,
          addToCart,
        )

        this.setState({
          newCartCandidate: null,
        })
      }
    }, 100)
  }

  onPressCompleteOrder = () => {
    const {
      isLoggedIn,
      cart,
      navigation,
    } = this.props
    if (isLoggedIn) {
      if (cart.type === 'subscription') {
        navigation.navigate('HomeSubscriptionSchedule')
      } else {
        navigation.navigate('SingleOrderCheckoutScreen')
      }
    } else {
      navigation.navigate('Login')
    }
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      cart,
      navigation,
      loading,
      resetCart,
      addToCart,
      lastUsed,
    } = this.props

    const {
      meals,
      fetching,
      error,
      mealDate,
      dateLabel,
      activeTab,
      maxItem,
    } = this.state

    const activeCategory = meals[activeTab]
    return (
      <Section
        style={{ paddingTop: verticalScale(16) }}
        full={true}
        header={(
          <SectionHeader
            style={{
              marginHorizontal: scale(20),
            }}
            captionText={mealDate.format('ddd, DD MMM YYYY')}
            titleText={dateLabel}
            titleRight={(
              <SeeAllButton
                onPress={() => this._navigateToMealListScreen(mealDate, activeTab, meals)}
              />
            )}
          />
        )}
      >
        {fetching === true && meals.length <= 1
          ? <ActiveMenuPlaceholder />
          : error === true
            ? (
              <Retry
                onPress={this._fetchCategories}
                width={VIEWPORT_WIDTH}
                height={verticalScale(350)}
              />
            )
            : (
              <MenuList
                mealCategories={meals}
                handleCatalogItemPress={(index, meals) => this._handleCatalogItemPress(
                  index,
                  meals,
                  activeCategory.name,
                  activeCategory.exclusive,
                )}
                cart={cart.cartItems
                  ? cart.cartItems
                  : []}
                mealDate={mealDate}
                onCartIncrease={(item, date, quantity) => this.increaseCart(item, date, quantity, activeCategory.name)}
                onCartDecrease={(item, date, quantity) => SalesOrderService
                  .decreaseHandler(
                    item,
                    date,
                    quantity,
                    loading,
                    resetCart,
                    addToCart,
                    cart,
                    null,
                    this.props.removeFromCart,
                    lastUsed.code,
                  )}
                onMaxItemEvent={SalesOrderService.onMaxItem}
                activeTab={activeTab}
                onSelectTab={this._setActiveTab}
                navigation={navigation}
                stock={this.props.stock}
                max={maxItem}
              />
            )
        }
      </Section>
    )
  }
}

const mapStateToProps = (state: AppState) => ({
  cart: state.cart,
  setting: state.setting.data,
  stock: state.meals.stock,
  isLoggedIn: state.login.token !== null,
})

const mapDispatchToProps = dispatch => ({
  setItemStock: (stock: ItemStock[]) => dispatch(MealsActions.setItemStock(stock)),
  addToCart: (shoppingCart: responseData) => dispatch(CartActions.addToCart(shoppingCart)),
  removeFromCart: (id: number, date: Moment) => dispatch(CartActions.removeFromCart(id, date)),
  resetCart: () => dispatch(CartActions.resetCart()),
  loading: (status: boolean) => dispatch(CartActions.loading(status)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Categories)
