import * as React from 'react'
import {
  Text,
  TouchableWithoutFeedback,
  View,
  StyleSheet,
  ViewStyle,
  TextStyle,
} from 'react-native'
import { Colors } from '../../../../Themes'
import { scale, verticalScale } from 'react-native-size-matters'

export interface SeeAllButtonProps {
  onPress: () => void,
}

export interface SeeAllButtonStyle {
  textContainer: ViewStyle,
  textStyle: TextStyle
}

const SeeAllButton: React.FC<SeeAllButtonProps> = ({ onPress }) => {
  return (
    <TouchableWithoutFeedback
      testID='seeAll'
      accessibilityLabel="seeAll"
      onPress={onPress}
    >
      <View style={styles.textContainer}>
        <Text style={styles.textStyle}>See All</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles: SeeAllButtonStyle = StyleSheet.create({
  textContainer: {
    borderRadius: 4,
    borderColor: Colors.pinkishOrange,
    borderWidth: 1,
  },
  textStyle: {
    fontFamily: 'Rubik-Regular',
    color: Colors.pinkishOrange,
    fontSize: scale(14),
    paddingHorizontal: scale(8),
    paddingVertical: verticalScale(8),
  },
})

export default SeeAllButton
