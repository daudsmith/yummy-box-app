import * as React from 'react'
import {
  Dimensions,
  View,
} from 'react-native'
import {
  TabView,
  SceneMap,
  Route,
} from 'react-native-tab-view'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'

import {
  MealCard,
  MenuSlider,
} from '../../../../Components/Common'
import CartService from '../../../../Services/MealCartService'
import Colors from '../../../../Themes/Colors'
import {
  Meals,
  Meal,
  Stock,
} from '../../../../Redux/MealsRedux'
import { Moment } from 'moment'
import CustomTabBar from './CustomTabBar'
import MenuCard from './MenuCard'
import { ImmutableArray, ImmutableObject } from 'seamless-immutable'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import StockService from '../../../../Services/StockService'
import { Delivery } from '../../../../Redux/V2/CartRedux'

export interface MenuListProps {
  mealCategories: Meals[] | ImmutableArray<Meals>
  handleCatalogItemPress: (index, meals) => void
  cart?: Delivery[]
  mealDate: Moment
  onCartIncrease: (item: Meal, date: Moment, quantity: number) => void
  onCartDecrease: (item: Meal, date: Moment, quantity: number) => void
  onMaxItemEvent?: (maximum: number, remaining: number) => void
  max?: number,
  activeTab: number,
  onSelectTab: (index: number, name?: string) => void,
  navigation: NavigationDrawerProp,
  stock?: ImmutableObject<Stock>,
}

export interface MenuListStates {
  activeTab: number,
}

class MenuList extends React.PureComponent<MenuListProps, MenuListStates> {
  renderActiveCardContent = (_item, _category, _exclusive, _index) => {
    const {
      cart,
      mealDate,
      onCartDecrease,
      onMaxItemEvent,
      onCartIncrease,
      navigation,
      stock,
      max,
    } = this.props
    let itemQty = 0
    let isAvailable = false
    const availability = StockService.getItemStock(_item.id, stock.main)
    let inCartQty = CartService.getItemQuantity(cart, _item.id, mealDate)
    let maxItem = availability - inCartQty

    if (availability) {
      isAvailable = true
    }

    if (CartService.isItemExistInCart(cart, _item, mealDate)) {
      const cartItem = CartService.getCartItem(
        cart,
        _item,
        mealDate.format('YYYY-MM-DD'),
      )

      if (cartItem !== null) {
        itemQty = cartItem.quantity
        maxItem = maxItem + itemQty
        if (max !== 0 && maxItem > max) {
          maxItem = max
        }
        isAvailable = StockService.isAvailableStock(stock.main, _item, mealDate, itemQty, cart)
      }
    }

    return (
      <MenuCard
        item={_item}
        category={_category}
        exclusive={_exclusive}
        mealDate={mealDate}
        max={maxItem}
        itemQty={itemQty}
        onCartIncrease={onCartIncrease}
        onCartDecrease={onCartDecrease}
        onMaxItemEvent={onMaxItemEvent}
        itemIndex={_index}
        navigation={navigation}
        available={isAvailable}
        availability={availability}
      />
    )
  }

  renderItem = (_meals, _item, _index, _category, _exclusive) => {
    const {
      handleCatalogItemPress,
    } = this.props
    const image = (
      _item.hasOwnProperty('catalog_image')
    )
      ?
      _item.catalog_image.medium_thumb.replace('_medium_thumb', '')
      :
      _item.hasOwnProperty('images')
        ? _item.images.medium_thumb.replace(
        '_medium_thumb', '')
        : null
    const title = _item.name

    return (
      <MealCard
        style={{
          height: 288,
          borderRadius: 8,
          marginTop: 8, // handle shadow
          marginBottom: 12, // handle shadow
          marginHorizontal: 4, // handle shadow
        }}
        contentStyle={{
          height: 128,
          paddingTop: 4,
        }}
        image={image}
        title={title}
        description={this.renderActiveCardContent(_item, _category, _exclusive, _index)}
        onPress={() => handleCatalogItemPress(_index, _meals)}
      />
    )
  }

  renderMenuList = () => {
    const { mealCategories, activeTab } = this.props
    const mealCategory = mealCategories[activeTab]
    const items = mealCategory.items.data
    const { name, exclusive } = mealCategory
    if (items.length === 0) {
      return (
        <View
          style={{
            height: 288,
            marginTop: 33,
          }}
        />
      )
    }
    return (
      <MenuSlider
        style={{
          paddingTop: verticalScale(16),
          paddingHorizontal: scale(20),
          paddingBottom: 0,
          width: Dimensions.get('window').width + 8, // handle shadow
          top: verticalScale(-8), // handle shadow
          left: scale(-4), // handle shadow
        }}
        items={items}
        renderItem={({ item, index }) =>
          this.renderItem(
            items,
            item,
            index,
            name,
            exclusive
          )
        }
        maxRender={8}
        initialRender={4}
      />
    )
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      mealCategories,
      activeTab,
      onSelectTab,
    } = this.props

    if (mealCategories === null) {
      return null
    }
    let renderScene = {}

    const sceneRoutes = (mealCategories as Meals[])
      .map((mealCategory): Route => {
        renderScene[mealCategory.id] = this.renderMenuList
        return {
          key: String(mealCategory.id),
          title: mealCategory.name,
        }
      })

    const scenes = SceneMap(renderScene)

    return (
      <TabView
        navigationState={{
          index: activeTab,
          routes: sceneRoutes,
        }}
        renderScene={scenes}
        onIndexChange={onSelectTab}
        swipeEnabled={false}
        initialLayout={{
          width: Dimensions.get('window').width,
        }}
        renderTabBar={props => (
          <CustomTabBar
            {...props}
            style={{
              marginHorizontal: scale(20),
            }}
            goToPage={onSelectTab}
            activeTabStyle={{
              backgroundColor: 'white',
            }}
            textStyle={{
              color: Colors.warmGreyThree,
              fontFamily: 'Rubik-Regular',
              fontSize: scale(14),
              fontWeight: '400',
            }}
            activeTextStyle={{
              color: Colors.primaryDark,
              fontFamily: 'Rubik-Regular',
              fontSize: scale(14),
              fontWeight: '600',
            }}
            activeTab={activeTab}
          />
        )}
      />
    )
  }
}

export default MenuList
