import * as React from 'react'
import {
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import Colors from '../../../../Themes/Colors'

export interface RetryProps {
  onPress: () => void,
  width: number,
  height: number,
  style?: ViewStyle,
}

const Retry: React.FC<RetryProps> = ({ onPress, width, height, style }) => (
  <View
    style={{
      ...styles.container,
      width,
      height,
      ...style
    }}
  >
    <Text
      style={styles.title}
    >
      Failed to Load Data
    </Text>
    <Text
      style={styles.caption}
    >
      Please try again
    </Text>
    <TouchableWithoutFeedback
      onPress={onPress}
    >
      <View
        style={styles.button}
      >
        <Text
          style={styles.buttonText}
        >Try Again</Text>
      </View>
    </TouchableWithoutFeedback>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: scale(16),
    marginBottom: verticalScale(12),
    color: Colors.darkBlue
  },
  caption: {
    color: Colors.warmGreyThree,
    fontSize: scale(14),
    marginBottom: verticalScale(32),
  },
  button: {
    paddingHorizontal: scale(8),
    paddingVertical: verticalScale(8),
    borderWidth: 1,
    borderRadius: 4,
    borderColor: Colors.pinkishOrange,
    backgroundColor: Colors.white,
    fontSize: scale(14),
  },
  buttonText: {
    color: Colors.pinkishOrange,
    fontSize: scale(14),
  },
})

export default Retry
