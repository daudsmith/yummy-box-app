import * as React from 'react'
import {
  StyleSheet,
  Text,
  View,
} from 'react-native'
import { AddToCart } from '../../../../Components/Common'
import LocaleFormatter from '../../../../Services/LocaleFormatter'
import Colors from '../../../../Themes/Colors'
import { Meal } from '../../../../Redux/MealsRedux'
import { Moment } from 'moment'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'

export interface MenuCardProps {
  item: Meal
  max: number
  itemIndex?: number
  itemQty: number
  mealDate: Moment
  exclusive: boolean
  category: string
  onCartIncrease: (item: Meal, date: Moment, quantity: number) => void
  onCartDecrease: (item: Meal, date: Moment, quantity: number) => void
  onMaxItemEvent?: (maximum: number, remaining: number) => void
  navigation: NavigationDrawerProp,
  available?: boolean
  availability?: number
}

class MenuCard extends React.PureComponent<MenuCardProps> {
  handleIncrement = (item, date, qty) => {
    const {
      onCartIncrease,
    } = this.props
    onCartIncrease(item, date, qty)
  }

  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    const {
      item,
      itemQty,
      mealDate,
      max,
      onCartDecrease,
      onMaxItemEvent,
      category,
      itemIndex,
      navigation,
      available,
      availability,
    } = this.props

    return (
      <View style={styles.contentContainer}>
        <View style={styles.contentPrice}>
          <Text style={styles.sellPrice}>
            {LocaleFormatter.numberToCurrency(item.sale_price)}
          </Text>
          {item.sale_price < item.base_price &&
          <Text style={styles.price}>
            {LocaleFormatter.numberToCurrency(item.base_price)}
          </Text>
          }
        </View>
        <View style={styles.contentCta}>
          <AddToCart
            quantity={itemQty}
            item={item}
            mealDate={mealDate}
            onIncrement={this.handleIncrement}
            max={max}
            onDecrement={onCartDecrease}
            onMaxItemEvent={onMaxItemEvent}
            testID={category.toLowerCase() + itemIndex}
            accessibilityLabel={category.toLowerCase() + itemIndex}
            navigation={navigation}
            available={available}
            availability={availability}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  contentPrice: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  sellPrice: {
    fontFamily: 'Rubik-Medium',
    fontSize: 12,
    paddingVertical: 4,
    color: Colors.pinkishOrange,
  },
  price: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    padding: 4,
    color: Colors.primaryGrey,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  contentCta: {
    flex: 1,
  },
})

export default MenuCard
