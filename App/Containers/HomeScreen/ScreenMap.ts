import ActiveMenu from './Section/ActiveMenu'
import PromotedMenu from './Section/PromotedMenu'
import BannerSection from './Section/Banner'
import FeaturedMenu from './Section/FeaturedMenu'
import ThemeMenu from './Section/ThemeMenu'
import CorporateTheme from './Section/CorporateTheme'

export const Sections = {
  CATEGORIES: ActiveMenu,
  PROMOTED: PromotedMenu,
  BANNER: BannerSection,
  FEATURED: FeaturedMenu,
  PLAYLIST: ThemeMenu,
  CORPORATE: CorporateTheme,
}
