import * as React from 'react'
import { View } from 'react-native'
import { CardPlaceholder } from '../../../Components/Placeholder'

const ActiveMenu: React.FC = () => {
  return (
    <View
      style={{
        height: 350,
        width: 355,
        overflow: 'hidden',
        justifyContent: 'flex-start',
        flexDirection: 'row',
      }}
    >
      <CardPlaceholder
        height={288}
        width={160}
        style={{
          marginTop: 40,
          paddingTop: 0,
          paddingRight: 0,
          paddingLeft: 20,
          paddingBottom: 0,
        }}
      />
      <CardPlaceholder
        height={288}
        width={160}
        style={{
          marginTop: 40,
          paddingTop: 0,
          paddingRight: 0,
          paddingLeft: 0,
          paddingBottom: 0,
        }}
      />
    </View>
  )
}

export default ActiveMenu
