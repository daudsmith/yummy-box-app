import * as React from 'react'
import { View } from 'react-native'
import { CardPlaceholder } from '../../../Components/Placeholder'

const FeaturedMenu = () => {
  return (
    <View
      style={{
        top: -100,
        position: 'absolute',
      }}
    >
    <View
      style={{
        width: 276,
        justifyContent: 'flex-start',
        flexDirection: 'row',
      }}
    >
      <CardPlaceholder
        height={199}
        width={120}
        style={{
          backgroundColor: 'transparent',
          paddingTop: 100,
          paddingRight: 0,
          paddingLeft: 20,
          paddingBottom: 0,
        }}
      />
      <CardPlaceholder
        height={199}
        width={120}
        style={{
          backgroundColor: 'transparent',
          paddingTop: 100,
          paddingRight: 0,
          paddingLeft: 0,
          paddingBottom: 0,
        }}
      />
    </View>
    </View>
  )
}

export default FeaturedMenu
