import * as React from 'react'
import { View } from 'react-native'
import { CardPlaceholder } from '../../../Components/Placeholder'

const Banner = () => {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        height: 172,
      }}
    >
      <CardPlaceholder
        height={140}
        width={311}
        style={{
          marginTop: 16,
          paddingTop: 0,
          paddingRight: 0,
          paddingLeft: 0,
          paddingBottom: 0,
        }}
      />
    </View>
  )
}

export default Banner
