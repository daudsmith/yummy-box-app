import * as React from 'react'
import { View } from 'react-native'
import { CardPlaceholder } from '../../../Components/Placeholder'

const FeaturedTheme = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
        paddingLeft: 20,
        paddingBottom: 16,
        paddingRight: 20,
        bottom: 18, // handle shadow
      }}
    >
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
        }}
      >
        <CardPlaceholder
          height={12}
          width={124}
          style={{
            backgroundColor: 'transparent',
            paddingTop: 0,
            paddingRight: 0,
            paddingLeft: 0,
            paddingBottom: 0,
          }}
        />
        <CardPlaceholder
          height={24}
          width={150}
          style={{
            backgroundColor: 'transparent',
            paddingTop: 0,
            paddingRight: 0,
            paddingLeft: 0,
            paddingBottom: 0,
          }}/>
      </View>
      <View
        style={{
          width: 116,
          height: 44,
        }}
      >
        <CardPlaceholder
          height={44}
          width={116}
          style={{
            backgroundColor: 'transparent',
            paddingTop: 0,
            paddingRight: 0,
            paddingLeft: 0,
            paddingBottom: 0,
          }}
        />
      </View>
    </View>
  )
}

export default FeaturedTheme
