import * as React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import moment, { Moment } from 'moment'
import {
  Image,
  Linking,
  Platform,
  Text,
  View,
} from 'react-native'
import {
  NavigationActions
} from 'react-navigation'
import homeStyles from '../Styles/HomeScreenStyle'
import { MealCard } from '../../Components/Common'
import { Row } from '../../Components/Layout'
import {
  Content,
  Container,
  Footer,
} from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'

import NavigationBar from '../../Components/NavigationBar'
import OfflineModal from '../../Components/OfflineModal'
import OfflinePage from '../../Components/OfflinePage'
import PrimaryButton from '../../Components/PrimaryButton'
import ShoppingCartButton from '../../Components/ShoppingCartButton'
import AutoRoutingActions, { InitAutoRouting } from '../../Redux/AutoRoutingRedux'
import { baseLastUsed } from '../../Redux/V2/LastUsedRedux'
import { CartState } from '../../Redux/V2/CartRedux'
import CustomerCardActions from '../../Redux/CustomerCardRedux'
import HomeContentActions, { HomeContentState } from '../../Redux/HomeContentRedux'
import {
  Meal,
} from '../../Redux/MealsRedux'
import PromotionActions, { Promotion } from '../../Redux/PromotionRedux'
import { SettingDataInterface } from '../../Redux/SettingRedux'
import ThemeActions, { Item } from '../../Redux/ThemeRedux'
import LocaleFormatter from '../../Services/LocaleFormatter'
import AvailabilityDateService from '../../Services/AvailabilityDateService'
import {
  Colors,
  Images,
} from '../../Themes'
import BannerSection from './Section/Banner'
import FeaturedMenu from './Section/FeaturedMenu'
import ThemeMenu from './Section/ThemeMenu'
import CorporateTheme from './Section/CorporateTheme'
import Categories from './Section/Categories'
import CustomerNotificationActions from '../../Redux/CustomerNotificationRedux'
import CustomerAccountActions from '../../Redux/CustomerAccountRedux'
import { AppState } from '../../Redux/CreateStore'
import {
  scale,
} from 'react-native-size-matters'
import LogEventService from '../../Services/LogEventService'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import PromotedMenu from './Section/PromotedMenu'
import { TouchableOpacity } from 'react-native-gesture-handler'
import colors from '../../Themes/Colors'
import SearchLocationService from '../../Services/SearchLocationService'
import SalesOrderService from '../../Services/SalesOrderService'
import CartActions from '../../Redux/V2/CartRedux'
import AlertBox from '../../Components/AlertBox'
import { responseData } from '../../Services/V2/ApiCart'
import ShoppingCartAlertModal from '../../Components/ShoppingCartAlertModal'
import { AnyAction } from 'redux'

export interface cartCandidate {
  item: Meal,
  date: Moment,
  category: string,
  exclusive: boolean,
}

export interface HomeScreenProps {
  navigation: NavigationDrawerProp,
  promotion: Promotion,
  homeContent: HomeContentState,
  cart: CartState,
  isLoggedIn: boolean,
  isConnected: boolean,
  hasFetchedStartupData: boolean,
  setting: SettingDataInterface,
  autoRouting: InitAutoRouting,
  unreadNotification: number,
  fetchThemes: () => void,
  fetchThemeDetail: () => void,
  fetchThemeDetailItems: () => void,
  fetchItems: () => void,
  fetchBanners: () => void,
  fetchPromote: (date: string) => void,
  clearRoute: () => void,
  fetchAvailableDate: (themeId: number) => void,
  fetchCards: () => void,
  fetchCorporateThemes: () => void,
  fetchWallet: () => void,
  fetchUnreadNotifications: () => void,
  cartLoading: (boolean) => void,
  addToCart: (cart: responseData) => void,
  token: string,
  lastUsed: baseLastUsed,
  loading: (status: boolean) => AnyAction,
  resetCart: () => AnyAction,
}

export interface HomeScreenStates {
  datePickerVisible: boolean,
  themes?: null,
  mealDate: Moment,
  selectedDate: string,
  tomorrow: Moment,
  deliveryDates?: Moment[],
  weekendDates?: Moment[],
  deliveryCutOff: number,
  sliderActive: number,
  showBlankOfflinePage: boolean,
  startedObserveCutOffTime: boolean,
  showCombinationAlertModal: boolean,
  activeTab: number,
  hasSlider: boolean,
  cartCandidate?: cartCandidate,
  availablePromotedDates: object[]
  selectedPromotedDates: object[],
  showCartAlert: boolean,
  newCartCandidate: {
    item: Meal,
    date: Moment,
    quantity: number
  }
}

export interface Params {
  meal_id?: number,
  meal_name?: string,
  meal_category_name?: string,
  category_id?: number,
  category_name?: string,
  currency?: string,
  price?: number,
  playlist_id?: number,
  playlist_name?: string,
  total_amount?: number,
  number_of_item?: number,
  banner_id?: number,
  banner_name?: string,
}

class HomeScreen extends React.Component<HomeScreenProps, HomeScreenStates> {
  constructor(props) {
    super(props)

    this.state = {
      datePickerVisible: false,
      themes: null,
      mealDate: moment()
        .utcOffset(7)
        .add(1, 'days'),
      selectedDate: '',
      tomorrow: moment()
        .utcOffset(7)
        .add(1, 'days'),
      deliveryDates: [],
      weekendDates: [],
      deliveryCutOff: 7200,
      sliderActive: 0,
      showBlankOfflinePage: false,
      startedObserveCutOffTime: false,
      showCombinationAlertModal: false,
      activeTab: 0,
      hasSlider: false,
      availablePromotedDates: [],
      selectedPromotedDates: [],
      showCartAlert: false,
      newCartCandidate: null
    }
  }

  static getDerivedStateFromProps(props, state) {
    const offDates = props.setting.off_dates
    const deliveryDates = AvailabilityDateService.getDeliveryDateList(offDates, props.setting.delivery_cut_off)
    const weekendDates = AvailabilityDateService.getWeekendDateWithinDeliveryDateList(
      offDates,
      props.setting.delivery_cut_off,
    )

    if (!state.showBlankOfflinePage && !state.hasSlider && !props.isConnected) {
      return {
        showBlankOfflinePage: true,
      }
    }
    if (state.showBlankOfflinePage && props.isConnected) {
      return {
        showBlankOfflinePage: false,
      }
    }

    return {
      deliveryDates,
      weekendDates,
    }
  }

  componentDidUpdate(
    prevProps: Readonly<HomeScreenProps>, prevState: Readonly<HomeScreenStates>): void {
    if (prevState.selectedDate !== this.state.selectedDate) {
      this.props.fetchPromote(this.state.selectedDate)
    }

    if (prevProps.lastUsed !== this.props.lastUsed) {
      if (this.props.cart.cartItems.length > 0) {
        SalesOrderService.validateCart(
          this.props.cart,
          this.props.cartLoading,
          null,
          this.props.addToCart,
          this.checkCartValid
        )
      }
      this.checkLastUsed(this.props.lastUsed)
    }

    if (!prevProps.isConnected && this.props.isConnected) {
      if (prevState.showBlankOfflinePage) {
        this._fetchInitialDataWhenOffline()
      }
      this._fetchAll()
    }

  }

  componentDidMount() {
    this.checkLastUsed(this.props.lastUsed)

    if (this.props.isConnected) {
      this._setMealDate()
        .then(() => this._fetchAll())
    }
    const { routeName, params } = this.props.autoRouting
    let theme
    if (routeName !== null) {
      switch (routeName) {
        case 'ThemeDetailScreen':
          theme = {
            name: '',
            id: params.id,
          }
          setTimeout(() => this.navigateToThemesDetail(theme, false), 500)
          break
        case 'MealListScreen':
          setTimeout(
            () => this.navigateToMealListScreen(params.date, params.tab), 1500)
          break
      }
      this.props.clearRoute()
    }

    if (this.props.setting.hasOwnProperty('delivery_cut_off')) {
      if (!this.state.startedObserveCutOffTime) {
        this.setState({
          startedObserveCutOffTime: true,
        }, () => AvailabilityDateService.listenForTimeChange(
          this._setMealDate,
          this.props.setting.delivery_cut_off,
        ))
      }
    }
  }

  checkLastUsed = (lastUsed: baseLastUsed) => {
    if (_.isEmpty(lastUsed.code)) {
      SearchLocationService.selectKitchen(this.props.token, this.props.navigation)
    }
  }

  checkCartValid = () => {
    if (!this.props.cart.valid) {
      this.setState({ showCartAlert: true })
    }
  }

  _setMealDate = async () => {
    let { mealDate } = this.state

    if (typeof this.props.setting.off_dates !== 'undefined') {
      mealDate = AvailabilityDateService.getInitialDates(
        mealDate,
        this.props.setting.off_dates,
        this.props.setting.delivery_cut_off,
      )
    }

    this.setState({
      mealDate,
    })
  }

  _fetchInitialDataWhenOffline = () => {
    if (this.props.isLoggedIn) {
      this.props.fetchCards()
    }
    // this.props.fetchItems()
  }

  _fetchAll = () => {
    this.props.fetchBanners()
    // this.props.fetchPromote(this.state.selectedDate)
    if (this.props.cart.type === 'subscription') {
      this.props.fetchAvailableDate(
        this.props.cart.cartItems[0].subscription.theme_id,
      )
    }
  }

  handleCatalogItemPress = (_selectedMealIndex, _meals, _category, _exclusive, promoted) => {
    const mealBaseOnIndex: Meal[] = _meals.filter((item, index) => index === _selectedMealIndex)
    const params: Params = {
      meal_id: mealBaseOnIndex[0].id,
      meal_name: mealBaseOnIndex[0].name,
      meal_category_name: _category,
    }
    const navigationAction = NavigationActions.navigate({
      routeName: 'MealDetailScreen',
      params: {
        meals: _meals,
        selectedMealIndex: _selectedMealIndex,
        category: _category,
        exclusive: _exclusive || _meals[_selectedMealIndex].exclusive || this.props.cart.exclusive,
        selectedDate: promoted
          ? moment(promoted)
          : this.state.mealDate,
        transition: Platform.OS,
      },
    })
    LogEventService.logEvent('click_meal_detail', params, ['default'])
    this.props.navigation.dispatch(navigationAction)
  }

  handlePressedBanner = (_bannerId, _bannerTitle) => {
    const index = _.findIndex(this.props.promotion.banners, (banner) => {
      return banner.id === _bannerId
    })
    const params: Params = {
      banner_id: _bannerId,
      banner_name: _bannerTitle,
    }
    LogEventService.logEvent('click_banner', params, ['default'])
    if (index > -1) {
      const pressedBanner = this.props.promotion.banners[index]
      // LogEventService.logEvent('home_promotion_banner_click', { banner: pressedBanner.slug })
      if (pressedBanner.action_path !== null && pressedBanner.action_path !==
        '') {
        Linking.canOpenURL(pressedBanner.action_path)
          .then((supported) => {
            if (supported) {
              return Linking.openURL(pressedBanner.action_path)
            }
          })
          .catch()
      } else if ((
        pressedBanner.description !== null &&
        pressedBanner.description !== ''
      ) &&
        (
          pressedBanner.terms_and_condition !== null &&
          pressedBanner.terms_and_condition !== ''
        )
      ) {
        this.props.navigation.navigate(
          'PromoDetailScreen',
          { promo: { ...pressedBanner } },
        )
      }
    }
  }

  navigateToMealListScreen = (_selectedDate = null, _tab = 0) => {
    const navigationAction = NavigationActions.navigate({
      routeName: 'MealListScreen',
      params: {
        startDate: '',
        selectedDate: _selectedDate,
        tab: _tab,
      },
    })
    this.props.navigation.dispatch(navigationAction)
    LogEventService.logEvent('click_see_all_menu', null, ['default'])
  }

  navigateToThemesDetail = (_item, isChefRecommendation: boolean) => {
    const params: Params = {
      playlist_id: _item.id,
      playlist_name: _item.name,
    }
    const navigationAction = NavigationActions.navigate({
      routeName: 'ThemeDetailScreen',
      params: {
        title: _item.name,
        id: _item.id,
        transition: Platform.OS,
      },
    })

    if (isChefRecommendation) {
      LogEventService.logEvent('click_subscribe_chef_rec', params, ['default'])
    } else {
      LogEventService.logEvent('click_subscribe_playlist', params, ['default'])
    }
    this.props.navigation.dispatch(navigationAction)
  }

  navigateToSearchLocation = () => this.props.navigation.navigate('SearchLocation', { noSearchLocation: false })

  _currentLocation = () => {
    const { lastUsed } = this.props
    return (
      <View style={{ flexDirection: 'row' }}>
        <Image source={Images.Map} style={{ width: scale(25), height: scale(25), marginTop: 3, marginRight: 5 }} resizeMode='contain' />
        <View>
          <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 10, color: colors.primaryGrey }}>Deliver to</Text>
          <TouchableOpacity onPress={this.navigateToSearchLocation} style={{ flexDirection: 'row' }}>
            <Text style={{ fontFamily: 'Rubik-Medium', fontSize: 14, color: colors.primaryDark }}>{lastUsed.name}</Text>
            <Ionicons name='chevron-down' size={15} style={{ color: Colors.primaryTurquoise, marginLeft: 2 }} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _renderThemes = ({ item, index }) => {
    const image = item.catalog_image
    const title = (
      item.tagline
        ? item.tagline
        : item.description
    )

    const content = (_item) => {
      let themePackage = null
      if (_item.hasOwnProperty('theme_periods')) {
        if (_item.theme_periods.length > 0) {
          themePackage = _item.theme_periods

          themePackage = _.sortBy(themePackage, ['sell_price'])
          themePackage = themePackage[0]
        }
      }
      if (themePackage !== null) {
        return (
          <View
            style={{
              flex: 1,
              marginTop: 8,
              paddingBottom: 16,
            }}
          >
            <View>
              <Text style={{
                fontFamily: 'Rubik-Regular',
                fontSize: 14,
                color: Colors.greyishBrownTwo,
              }}
              >Package start from</Text>
              <View
                style={{
                  marginTop: 4,
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                }}
              >
                <Text style={{
                  fontFamily: 'Rubik-Medium',
                  fontSize: 16,
                  color: Colors.pinkishOrange,
                  marginRight: 4,
                }}>
                  {LocaleFormatter.numberToCurrency(themePackage.sell_price)}
                </Text>
                {(
                  themePackage.sell_price < themePackage.base_price
                ) && (
                    <Text style={{
                      fontFamily: 'Rubik-Regular',
                      fontSize: 12,
                      color: Colors.warmGreyThree,
                      marginRight: 4,
                      textDecorationLine: 'line-through',
                      textDecorationStyle: 'solid',
                    }}>
                      {LocaleFormatter.numberToCurrency(themePackage.base_price)}
                    </Text>
                  )}
              </View>
            </View>
            <View
              style={{
                marginTop: 8,
                height: 40,
                paddingBottom: 4,
              }}
            >
              <PrimaryButton
                testID={`subscribeNowPlaylist${index}`}
                accessibilityLabel={`subscribeNowPlaylist${index}`}
                label='Subscribe Now'
                labelStyle={{ fontSize: 14 }}
                onPressMethod={() => this.navigateToThemesDetail(item, false)}
                buttonStyle={{ backgroundColor: Colors.pinkishOrange }}
              />
            </View>
          </View>
        )
      }

      return null
    }

    return (
      <MealCard
        style={{
          height: 446,
          width: 241,
          marginRight: 8,
        }}
        contentStyle={{
          height: 166,
          paddingLeft: 0,
          paddingRight: 0,
          borderBottomLeftRadius: 0,
          borderBottomRightRadius: 0,
        }}
        titleStyle={{
          fontSize: 14,
          color: Colors.warmGreyThree,
        }}
        imageContainerStyle={{
          height: 294,
          width: 241,
          borderTopLeftRadius: 8,
          borderTopRightRadius: 8,
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
          overflow: 'hidden',
        }}
        image={image}
        title={title}
        noShadow={true}
        description={content(item)}
        onPress={() => this.navigateToThemesDetail(item, false)}
      />
    )
  }

  _renderThemeItem = ({ item }: { item: Item }) => {
    const image = item.catalog_image
    const title = item.name
    const caption = item.formatted_date

    return (
      <MealCard
        style={{
          height: 199,
          width: 120,
          borderRadius: 8,
          marginTop: 8, // handle shadow
          marginBottom: 12, // handle shadow
          marginLeft: 4, // handle shadow
          marginRight: 4, // handle shadow
        }}
        contentStyle={{ height: 79 }}
        captionStyle={{ fontSize: 11 }}
        titleStyle={{ fontSize: 12 }}
        imageContainerStyle={{
          height: 120,
          width: 120,
        }}
        image={image}
        title={title}
        caption={caption}
        onPress={() => { }}
      />
    )
  }

  _openHamburger = () => {
    const { navigation } = this.props
    navigation.toggleDrawer()
  }

  onPressShoppingCart = () => {
    const { navigation, isLoggedIn, cart } = this.props
    if (isLoggedIn) {
      if (cart && cart.type !== 'subscription') {
        if (cart.cartItems.length > 0 && cart.totals.subtotal > 0) {
          const params: Params = {
            currency: 'IDR',
            total_amount: cart.cartItems[0].totals.subtotal,
            number_of_item: cart.type === 'package'
              ? 1
              : cart.cartItems[0].items[0].quantity,
          }
          LogEventService.logEvent('click_checkout', params, ['default'])
        }
        navigation.navigate({
          routeName: 'SingleOrderCheckoutScreen',
          key: 'SingleOrderCheckoutScreen',
        })
      } else {
        const params: Params = {
          currency: 'IDR',
          total_amount: 0,
          number_of_item: 0,
        }
        LogEventService.logEvent('click_checkout', params, ['default'])
        navigation.navigate('HomeSubscriptionSchedule')
      }
    } else {
      navigation.navigate('Login')
    }
  }

  _onPressCompleteOrder = () => {
    const {
      isLoggedIn,
      cart,
      navigation,
    } = this.props
    if (isLoggedIn) {
      if (cart.type === 'subscription') {
        navigation.navigate('HomeSubscriptionSchedule')
      } else {
        navigation.navigate('SingleOrderCheckoutScreen')
      }
    } else {
      navigation.navigate('Login')
    }
  }

  _onPressClearCart = () => {
    this.props.resetCart()
    setTimeout(() => {
      if (this.state.newCartCandidate) {
        const {
          cart,
          loading,
          resetCart,
          addToCart,
          lastUsed,
        } = this.props

        const {
          item,
          date,
          quantity,
        } = this.state.newCartCandidate

        SalesOrderService.addToCart(
          cart,
          item,
          date,
          quantity,
          lastUsed.code,
          loading,
          resetCart,
          addToCart,
        )

        this.setState({
          newCartCandidate: null,
        })
      }
    }, 100)
  }

  _showCombineAlert = (
    item: Meal,
    date: Moment,
    quantity: number,
  ) => {
    this.setState({
      showCombinationAlertModal: true,
      newCartCandidate: {
        item: item,
        quantity: quantity,
        date: date,
      },
    })
  }

  render() {
    const {
      mealDate,
      showBlankOfflinePage,
    } = this.state

    if (!this.props.isConnected && showBlankOfflinePage) {
      return (
        <View
          testID='HomeScreen'
          style={homeStyles.mainContainer}
        >
          <NavigationBar
            leftSide='menu'
            navigateFrom='home'
            title={() => this._currentLocation()}
            unread={this.props.unreadNotification}
          />
          <OfflinePage />
        </View>
      )
    }

    return (
      <Container>
        <NavigationBar
          leftSide='menu'
          navigateFrom='home'
          title={() => this._currentLocation()}
          openHamburger={this._openHamburger}
          unread={this.props.unreadNotification}
        />
        <Content
          testID='HomeScreen'
          style={homeStyles.mainContainer}
        >
          <Row style={{ marginBottom: 16 }}>
            <Categories
              navigation={this.props.navigation}
              lastUsed={this.props.lastUsed}
              showCombineAlert={this._showCombineAlert}
            />
          </Row>

          <Row style={{ marginBottom: 16 }}>
            <BannerSection
              onPressed={this.handlePressedBanner}
            />
          </Row>

          <Row>
            <PromotedMenu
              navigation={this.props.navigation}
            />
          </Row>

          <Row style={{ marginBottom: 16 }}>
            <FeaturedMenu
              renderThemeItem={this._renderThemeItem}
              navigateToThemesDetail={this.navigateToThemesDetail}
              mealDate={mealDate.format('YYYY-MM-DD')}
              featuredThemeSetting={this.props.setting.featured_theme}
              lastUsed={this.props.lastUsed}
            />
          </Row>

          <Row>
            <ThemeMenu
              renderItem={this._renderThemes}
              lastUsed={this.props.lastUsed}
              navigation={this.props.navigation}
            />
          </Row>

          <Row>
            <CorporateTheme
              navigateToThemesDetail={this.navigateToThemesDetail}
              token={this.props.token}
            />
          </Row>

          {!showBlankOfflinePage && (
            <OfflineModal isConnected={this.props.isConnected} />
          )}

        </Content>
        <ShoppingCartAlertModal
          primaryAction={this._onPressCompleteOrder}
          secondaryAction={this._onPressClearCart}
          visible={this.state.showCombinationAlertModal}
          dismiss={() => this.setState({
            showCombinationAlertModal: false,
          })}
        />
        {((this.props.cart.cartItems.length > 0 && this.props.cart.cartItems[0].items.length > 0
          || this.props.cart.cartItems.length > 0 && this.props.cart.cartItems[0].packages
          && this.props.cart.cartItems[0].packages.length > 0) && this.props.isConnected
          && !this.state.showCartAlert)
          && <Footer style={{ backgroundColor: Colors.pinkishOrange }}>
            <ShoppingCartButton
              cart={this.props.cart}
              isLoggedIn={this.props.isLoggedIn}
              onPressHandle={this.onPressShoppingCart}
              testID="checkout"
              accessibilityLabel="checkout"
            />
          </Footer>
        }
        <AlertBox
          primaryAction={this.onPressShoppingCart}
          primaryActionText='Edit My Order'
          secondaryAction={this.navigateToSearchLocation}
          secondaryActionText='Change Location'
          dismiss={() => this.setState({ showCartAlert: false })}
          title='Product Availability Change'
          text='Oops! One or more products that you chose have recently sold out, partially available or not available in your location. Please choose another product or another location'
          visible={this.state.showCartAlert && !this.props.navigation.state.params?.isFromCheckout}
        />

      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    promotion: state.promotion,
    homeContent: state.homeContent,
    cart: state.cart,
    isLoggedIn: state.login.token !== null,
    isConnected: state.network.isConnected,
    hasFetchedStartupData: state.startup.hasFetchedStartupData,
    setting: state.setting.data,
    autoRouting: state.autoRouting,
    unreadNotification: state.customerNotification.unread,
    token: state.login.token,
    lastUsed: state.lastUsed
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchThemes: () => dispatch(HomeContentActions.fetchThemes()),
    fetchItems: () => dispatch(HomeContentActions.fetchItems()),
    fetchBanners: () => dispatch(PromotionActions.fetchBanners()),
    fetchPromote: (date) => dispatch(HomeContentActions.fetchPromote(date)),
    clearRoute: () => dispatch(AutoRoutingActions.clearRoute()),
    fetchAvailableDate: (themeId) => dispatch(
      ThemeActions.fetchAvailableDate(themeId)),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
    fetchCorporateThemes: () => dispatch(
      HomeContentActions.fetchCorporateThemes()),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchUnreadNotifications: () => dispatch(
      CustomerNotificationActions.fetchTotalUnreadNotifications()),
    cartLoading: (status: boolean) => dispatch(CartActions.loading(status)),
    addToCart: (cart: responseData) => dispatch(CartActions.addToCart(cart)),
    resetCart: () => dispatch(CartActions.resetCart()),
    loading: (status: boolean) => dispatch(CartActions.loading(status)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
