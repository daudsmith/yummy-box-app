import { ScrollableTab, Tab, Tabs } from 'native-base'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ActivityIndicator, Dimensions, FlatList, Platform, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import Icon from 'react-native-vector-icons/Entypo'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import NavigationBar from '../Components/NavigationBar'
import OfflineModal from '../Components/OfflineModal'
import OfflinePage from '../Components/OfflinePage'
import PrimaryTabBar from '../Components/PrimaryTabBar'
import YummyboxIcon from '../Components/YummyboxIcon'
import withSafeAreaView from '../Components/Common/WithSafeAreaView'
import CustomerOrderActions from '../Redux/CustomerOrderRedux'
import StartupActions from '../Redux/StartupRedux'
import LocaleFormatter from '../Services/LocaleFormatter'
import OrderStatusHelper from '../Services/OrderStatusHelper'
import PaymentMethodHelper from '../Services/PaymentMethodHelper'
import { Colors } from '../Themes'
import styles from './Styles/CustomerMyOrderListScreenStyle'
import { ScrollableTabViewContent } from '../Components/Common/ScrollableTabViewContent'
import CustomerNotificationActions from '../Redux/CustomerNotificationRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'

const {height, width} = Dimensions.get('window')
const underlineLeft = (width / 6)

class CustomerMyOrderListScreen extends Component {
  navigationListener
  constructor (props) {
    super(props)
    this.state = {
      showBlankOfflinePage: !this.props.isConnected && !this.props.orderList
    }
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    if (!this.props.isConnected
      && newProps.isConnected
      && !this.props.orderList
    ) {
      if (!this.props.hasFetchedStartupData) {
        this.setState({
          showBlankOfflinePage: true
        }, () => {
          this.props.fetchOrderList()
        })
      } else {
        this.props.fetchOrderList(true)
      }
    }
  }

  fetchData = () => {
    const {
      isConnected,
      orderList,
      fetchOrderList,
    } = this.props
    if (isConnected) {
      if (orderList) {
        fetchOrderList(true)
      } else {
        fetchOrderList()
      }
    }
  }

  componentDidMount () {
    this.navigationListener = this.props.navigation.addListener('didFocus', this.fetchData)
    this.fetchData()
  }

  componentWillUnmount() {
    this.navigationListener.remove()
  }

  handleItemPress = (order) => {
    const { state } = this.props.navigation
    const navigationAction = NavigationActions.navigate({
      routeName: order.type === 'subscription' ? 'MySubscriptionDetailScreen' : 'CustomerMyOrderDetailScreen',
      params: {
        order_id: order.id,
        number: order.number.replace('KDS-INV-', ''),
        type: order.type,
        go_back_key: state.key
      }
    })
    this.props.navigation.dispatch(navigationAction)
  }

  _renderSectionAndContent (title, orders, testID) {
    const isSubscription = title === 'My Subscription'
    return (
      <View>
        <View
          style={{
            paddingLeft: 20,
            paddingTop: 13,
            paddingBottom: 16,
            backgroundColor: Colors.lightGrey3
          }}
        >
          <Text
            style={{
              fontFamily: 'Rubik-Regular',
              fontSize: 16,
              color: Colors.warmGrey
            }}
          >
            {title}
          </Text>
          <View
            style={{
              height: 1,
              width: 40,
              backgroundColor: Colors.grennBlue,
              marginTop: 7
            }}
          />
        </View>

        <FlatList
          data={orders}
          keyExtractor={item => {return item.number}}
          renderItem={({ item, index }) => this._renderListItem(testID+index, item, isSubscription, (index !== (orders.length - 1)))}
          viewabilityConfig={{
            viewAreaCoveragePercentThreshold: 90,
          }}
        />
      </View>
    )
  }

  _renderOrderTabs (subscriptionInProgress, nonSubscriptionInProgress, subscriptionCompleted, nonSubscriptionCompleted) {
    const tabHeight = 40
    const navBarHeight = 40
    const minTabHeight = tabHeight + navBarHeight
    return (
      <Tabs
        renderTabBar={() => {
          return (
            <ScrollableTab
              style={{
                backgroundColor: 'white',
                height: 40,
                marginVertical: 0,
                borderBottomWidth: 0,
              }}
              tabsContainerStyle={{
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'stretch',
              }}
            />
          )
        }}
        tabBarUnderlineStyle={{
          backgroundColor: Colors.grennBlue,
          width: underlineLeft,
          height: 1,
          marginLeft: underlineLeft
        }}
      >
        <Tab
          heading='In Progress'
          testID="inProgress"
          tabStyle={{
            backgroundColor: 'white',
            borderBottomWidth: 1/2,
            borderBottomColor: Colors.warmGreyTwo,
            marginRight: 0,
            flex: 1,
            borderRightWidth: 1/2,
            borderRightColor: Colors.warmGreyTwo
          }}
          activeTabStyle={{
            backgroundColor: 'white',
            flex: 1
          }}
          textStyle={{
            color: Colors.warmGreyTwo,
            fontFamily: 'Rubik-Regular',
            fontSize: 16
          }}
          activeTextStyle={{
            color: Colors.green,
            fontFamily: 'Rubik-Regular',
            fontSize: 16
          }}
        >
          <ScrollView nestedScrollEnabled>
            {subscriptionInProgress.length > 0 &&
              this._renderSectionAndContent('My Subscription', subscriptionInProgress, 'inProgSub')
            }
            {nonSubscriptionInProgress.length > 0 &&
              this._renderSectionAndContent('Orders', nonSubscriptionInProgress, 'inProgOrder')
            }
            {subscriptionInProgress.length + nonSubscriptionInProgress.length === 0 &&
              <View style={{minHeight: height - minTabHeight, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 16, fontFamily: 'Rubik-Light', color: Colors.brownishGrey}}>
                  You don't have any in progress orders
                </Text>
              </View>
            }
          </ScrollView>
        </Tab>
        <Tab
          heading='Completed'
          testID="completed"
          tabStyle={{
            backgroundColor: 'white',
            borderBottomWidth: 1/2,
            borderBottomColor: Colors.warmGreyTwo,
            marginRight: 0,
            flex: 1,
            borderLeftWidth: 1/2,
            borderLeftColor: Colors.warmGreyTwo
          }}
          activeTabStyle={{
            backgroundColor: 'white',
            flex: 1
          }}
          textStyle={{
            color: Colors.warmGreyTwo,
            fontFamily: 'Rubik-Regular',
            fontSize: 16
          }}
          activeTextStyle={{
            color: Colors.green,
            fontFamily: 'Rubik-Regular',
            fontSize: 16
          }}
        >
          <ScrollView>
            {subscriptionCompleted.length > 0 &&
              this._renderSectionAndContent('My Subscription', subscriptionCompleted, 'completeSub')
            }
            {nonSubscriptionCompleted.length > 0 &&
              this._renderSectionAndContent('Orders', nonSubscriptionCompleted, 'completeOrder')
            }
            {subscriptionCompleted.length + nonSubscriptionCompleted.length === 0 &&
              <View
                style={{
                  minHeight: height - minTabHeight,
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'Rubik-Light',
                    color: Colors.brownishGrey
                  }}
                >
                  You don't have any in progress orders
                </Text>
              </View>
            }
          </ScrollView>
        </Tab>
      </Tabs>
    )
  }

  _renderOrderList (subscriptionInProgress, nonSubscriptionInProgress, subscriptionCompleted, nonSubscriptionCompleted) {
    const tabHeight = 40
    const navBarHeight = 40
    const minTabHeight = tabHeight + navBarHeight
    return (
      <ScrollableTabView
        initialPage={0}
        renderTabBar={() => <PrimaryTabBar style={{height: 40}} tabStyle={{marginTop: 10}} />}
      >
        <ScrollableTabViewContent style={{flex: 1, backgroundColor: Colors.lightGrey3}} tabLabel='In Progress'>
          <ScrollView>
            {subscriptionInProgress.length > 0 &&
              this._renderSectionAndContent('My Subscription', subscriptionInProgress, 'inProgSub')
            }
            {nonSubscriptionInProgress.length > 0 &&
              this._renderSectionAndContent('Orders', nonSubscriptionInProgress, 'inProgOrder')
            }
            {subscriptionInProgress.length + nonSubscriptionInProgress.length === 0 &&
              <View style={{minHeight: height - minTabHeight, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 16, fontFamily: 'Rubik-Light', color: Colors.brownishGrey}}>You don't have any in progress orders</Text>
              </View>
            }
          </ScrollView>
        </ScrollableTabViewContent>
        <ScrollableTabViewContent style={{minHeight: height - minTabHeight, backgroundColor: Colors.lightGrey3}} tabLabel='Completed'>
          <ScrollView>
            {subscriptionCompleted.length > 0 &&
              this._renderSectionAndContent('My Subscription', subscriptionCompleted, 'completeSub')
            }
            {nonSubscriptionCompleted.length > 0 &&
              this._renderSectionAndContent('Orders', nonSubscriptionCompleted, 'completeOrder')
            }
            {subscriptionCompleted.length + nonSubscriptionCompleted === 0 &&
              <View style={{minHeight: height - minTabHeight, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 16, fontFamily: 'Rubik-Light', color: Colors.brownishGrey}}>You don't have any in progress orders</Text>
              </View>
            }
          </ScrollView>
        </ScrollableTabViewContent>
      </ScrollableTabView>
    )
  }

  _renderListItem (testID, order, isSubscription, renderSeparator) {
    const separator = renderSeparator ? {borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: Colors.pinkishGrey} : {}
    const titleColor = isSubscription ? {color: Colors.greyishBrownTwo} : {color: Colors.brownishGrey}
    return (
      <TouchableWithoutFeedback testID={testID} accessibilityLabel={testID} key={order.number} onPress={() => this.handleItemPress(order)}>
        <View style={{flexDirection: 'row', backgroundColor: 'white'}}>
          <View style={{width: 3, backgroundColor: Colors.grennBlue}} />
          <View style={{...styles.row, ...separator}}>
            <View style={{flex: 1}}>
              <Text style={{...styles.listTitle, ...titleColor}}>
                {isSubscription ? order.theme : `#${order.number.replace('KDS-INV-', '')}` }
              </Text>
              {isSubscription ?
                <View style={{marginTop: 7}}>
                  <Text style={[styles.descriptionText, {marginBottom: 5}]}>
                    Start On <Text style={{fontFamily: 'Rubik-Regular'}}>{order.start_date}</Text>
                  </Text>
                  <Text style={[styles.descriptionText, {marginBottom: 5}]}>
                    Subscribe Every <Text style={{fontFamily: 'Rubik-Regular'}}>{order.repeat_every}</Text>
                  </Text>
                  <Text style={[styles.descriptionText]}>
                    Subscribe For <Text style={{fontFamily: 'Rubik-Regular'}}>{order.repeat_interval}</Text>
                  </Text>
                </View>
              :
                <View style={{marginTop: 8}}>
                  <Text style={[styles.descriptionText, {marginBottom: 3}]}>
                    {order.order_date}
                  </Text>
                  <Text style={styles.descriptionText}>
                    {order.type === 'package' ? `${order.total_item_in_cart}-Day Package` : `Total ${order.total_item_in_cart} item(s)` }
                  </Text>
                </View>
              }
            </View>
            <View style={{alignItems: 'flex-end'}}>
              <Text style={styles.orderStatusText}>
                {OrderStatusHelper.parseToLabel(order.status)}
              </Text>
              {!isSubscription &&
                <View style={{flexDirection: 'row', marginTop: 25}}>
                  <YummyboxIcon
                    name={PaymentMethodHelper.getPaymentIconName(order.payment_method)}
                    color={Colors.bloodOrange}
                    size={20}
                  />
                  <Text style={styles.priceText}>
                    {LocaleFormatter.numberToCurrency(order.total)}
                  </Text>
                </View>
              }
            </View>
            <View style={{justifyContent: 'center'}}>
              <Icon
                name='chevron-right'
                color={Colors.pinkishGrey}
                size={20}
                style={styles.arrow}
              />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  _renderLoading () {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size={'large'} />
      </View>
    )
  }

  _renderEmpty() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontSize: 16, fontFamily: 'Rubik-Light', color: Colors.brownishGrey}}>You don't have any orders yet</Text>
      </View>
    )
  }

  _openHamburger = () => {
    const { user, isConnected, fetchWallet, fetchUnreadNotifications, navigation } = this.props
    if (user && isConnected) {
      fetchWallet()
      fetchUnreadNotifications()
    }
    navigation.toggleDrawer()
  }

  render () {
    let body = null
    const { showBlankOfflinePage } = this.state
    const { orderList, isConnected, fetchingOrderList, orderListError } = this.props
    if (!isConnected && showBlankOfflinePage) {
      body = <OfflinePage />
    } else {
      if (orderList
        && !fetchingOrderList
        && !orderListError
      ) {
        const {subscription, order} = orderList
        let subscriptionInProgress = []
        let subscriptionCompleted = []
        let nonSubscriptionInProgress = []
        let nonSubscriptionCompleted = []

        if (subscription && order) {
          if (typeof subscription.in_progress !== 'undefined') {
            subscriptionInProgress = subscription.in_progress
          }
          if (typeof subscription.complete !== 'undefined') {
            subscriptionCompleted = subscription.complete
          }
          if (typeof order.in_progress !== 'undefined') {
            nonSubscriptionInProgress = order.in_progress
          }
          if (typeof order.complete !== 'undefined') {
            nonSubscriptionCompleted = order.complete
          }
        }
        body = Platform.OS === 'ios' ?
          this._renderOrderList(subscriptionInProgress, nonSubscriptionInProgress, subscriptionCompleted, nonSubscriptionCompleted)
          : this._renderOrderTabs(subscriptionInProgress, nonSubscriptionInProgress, subscriptionCompleted, nonSubscriptionCompleted)
      } else {
        if (fetchingOrderList) {
          body = this._renderLoading()
        }
        if (!fetchingOrderList && !orderList) {
          body = this._renderEmpty()
        }
      }
    }

    return (
      <View
        style={{
          backgroundColor: 'white',
          flex: 1
        }}
      >
        <NavigationBar
          leftSide='menu'
          title='My Orders'
          openHamburger={this._openHamburger}
          unread={this.props.unreadNotification}
        />

        <View
          style={{
            flex: 1,
            backgroundColor: 'white'
          }}
        >
          {body}
        </View>

        {!showBlankOfflinePage &&
          <OfflineModal
            isConnected={this.props.isConnected}
          />
        }
      </View>
    )
  }
}

CustomerMyOrderListScreen.propTypes = {
  orderList: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.object,
  ]),
  orderListError: PropTypes.string.isRequired,
  fetchingOrderList: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  hasFetchedStartupData: PropTypes.bool.isRequired,
  fetchOrderList: PropTypes.func.isRequired,
  startupOnMyOrder: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => {
  return {
    orderList: state.customerOrder.orderList,
    fetchingOrderList: state.customerOrder.fetchingOrderList,
    orderListError: state.customerOrder.orderListError,
    isConnected: state.network.isConnected,
    hasFetchedStartupData: state.startup.hasFetchedStartupData,
    user: state.login.user,
    unreadNotification: state.customerNotification.unread,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOrderList: (silent) => dispatch(CustomerOrderActions.fetchOrderList(silent)),
    startupOnMyOrder: () => dispatch(StartupActions.startupOnMyOrder()),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchUnreadNotifications: () => dispatch(CustomerNotificationActions.fetchTotalUnreadNotifications()),
  }
}

export default withSafeAreaView(connect(mapStateToProps, mapDispatchToProps)(CustomerMyOrderListScreen))
