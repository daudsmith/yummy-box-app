import _ from 'lodash'
import Moment from 'moment'
import {
  Container,
  Content,
  Footer,
  Toast,
} from 'native-base'
import React, { Component } from 'react'
import {
  Alert,
  Image,
  Text,
  TouchableOpacity,
  View,
  Linking,
} from 'react-native'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import Feather from 'react-native-vector-icons/Feather'
import { connect } from 'react-redux'
import CartQuantitySpinner from '../Components/CartQuantitySpinner'
import NavigationBar from '../Components/NavigationBar'
import SelectDeliveryAddressButton from '../Components/SelectDeliveryAddressButton'
import SmartAddressForm from '../Components/SmartAddressForm'
import SmartDeliveryTimePicker from '../Components/SmartDeliveryTimePicker'
import Ionicons from 'react-native-vector-icons/Ionicons'
// Components
import CartActions, {
  BaseCart,
  Delivery,
  DeliveryAddress,
} from '../Redux/V2/CartRedux'
import CustomerCardActions from '../Redux/CustomerCardRedux'
import AlertBox from '../Components/AlertBox'
// Redux
import SettingActions, { SettingDataInterface } from '../Redux/SettingRedux'
// Services
import LocaleFormatter from '../Services/LocaleFormatter'
import Colors from '../Themes/Colors'
import styles from './Styles/SingleOrderCheckoutScreenStyle'
import Spinner from 'react-native-loading-spinner-overlay'
import { Images } from '../Themes'
import { AppState } from '../Redux/CreateStore'
import {
  Address,
  CustomerAddressReduxState,
} from '../Redux/CustomerAddressRedux'
import {
  CART_ITEM_STATUS,
  itemResponse,
  responseData,
} from '../Services/V2/ApiCart'
import SalesOrderService from '../Services/SalesOrderService'
import { LoginState } from '../Redux/LoginRedux'
import { StackNavigationProp } from 'react-navigation-stack/lib/typescript/src/vendor/types'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import {
  NavigationActions,
  StackActions,
} from 'react-navigation'
import StockService from '../Services/StockService'
import { ImmutableObject } from 'seamless-immutable'
import { Stock } from '../Redux/MealsRedux'
import { baseLastUsed, Location } from '../Redux/V2/LastUsedRedux'
import {row} from '../Components/AddressForm'
import LastUsedActions from '../Redux/V2/LastUsedRedux'

export interface propsInterface {
  navigation: NavigationDrawerProp & StackNavigationProp,
  cart: BaseCart,
  loginState: LoginState,
  isFetchingCompanyAddresses: boolean,
  isFetchingCustomerAddresses: boolean,
  customerAddress: CustomerAddressReduxState,
  setting: SettingDataInterface,
  isConnected: boolean,
  resetCart: () => void,
  addToCart: (cart: responseData) => void,
  loading: (status: boolean) => void,
  removeFromCart: (itemId: number, date: Moment.Moment) => void,
  updateDeliveryTime: (time: string) => void,
  updateDeliveryAddress: (address) => void,
  addLastUsed: (code: string, location: Location, name: string, address: string) => void,
  token: string,
  stock: ImmutableObject<Stock>,
  lastUsed: baseLastUsed,
}

export interface stateInterface {
  timePickerVisible: boolean,
  transitioningToEmptyScreen: boolean,
  showAddressList: boolean,
  currentSelectAddressDate?: DeliveryAddress,
  removeMultipleAddressAlert: boolean,
  showRemoveItemFromCartModal: boolean,
  itemToBeRemove?: itemResponse,
  itemDateToBeRemove?: Moment.Moment,
  showSpinner: boolean,
  showErrorModal: boolean,
  checkoutValidating: boolean,
  loading: boolean,
  maxItem: number,
  showCartAlert: boolean,
  cartAlertText: string,
  cartAlertTitle: string,
  cartAlertActionText: string
}

class SingleOrderCheckoutScreen extends Component<propsInterface, stateInterface> {
  mounted = false
  willFocus
  willBlur

  constructor(props) {
    super(props)
    this.state = {
      timePickerVisible: false,
      transitioningToEmptyScreen: false,
      showAddressList: false,
      currentSelectAddressDate: null,
      removeMultipleAddressAlert: false,
      showRemoveItemFromCartModal: false,
      itemToBeRemove: null,
      itemDateToBeRemove: null,
      showSpinner: false,
      showErrorModal: false,
      checkoutValidating: false,
      loading: false,
      maxItem: 0,
      showCartAlert: false,
      cartAlertText: '',
      cartAlertTitle: '',
      cartAlertActionText: ''
    }
  }

  componentDidUpdate(prevProps) {
    if (this.mounted) {
      const { cartItems } = this.props.cart
      const itemIsZero = this.checkCartItemsIsEmpty(cartItems)
      if (itemIsZero) {
        this.mounted = false
      }

      if (!this.props.isConnected
        && prevProps.isConnected
        && this.mounted
      ) {
        this.checkAndValidate(this.checkCartValid)
      }

      if (this.state.checkoutValidating && (this.props.cart.fetching !== prevProps.cart.fetching)) {
        if (!this.props.cart.fetching && this.props.cart.totals.subtotal > 0) {
          if (!this.props.cart.valid) {
            return Toast.show({
              text: 'One of your item in cart is out of stock',
              buttonText: '',
              duration: 2500,
            })
          }

          this.mounted = false
        }
      }
    }
  }

  destroyOrder = () => {
    this.props.resetCart()
  }

  componentDidMount() {
    this.mounted = true
    if (this.props.isConnected) {
      this.checkAndValidate(this.checkCartValid)
      this.setInitialAddress()
    }

    this.willFocus = this.props
      .navigation
      .addListener(
        'willFocus', () => {
          if (!this.mounted) {
            this.setInitialAddress()
            setTimeout(() => {
              this.checkAndValidate(this.checkCartValid)
            }, 500)
          }
        }
      )

    this.willFocus = this.props
      .navigation
      .addListener(
        'didBlur',
        () => this.mounted = false,
      )

    const {
      sales_order_total_limit,
    } = this.props.setting
    let limit = JSON.parse(sales_order_total_limit)
    if (typeof limit['item'] !== 'undefined'
      && typeof limit['item']['max'] === 'number'
    ) {
      this.setState({
        maxItem: limit['item']['max'],
      })
    }
  }

  componentWillUnmount() {
    if (this.willFocus) {
      this.willFocus.remove()
    }
    if (this.willBlur) {
      this.willBlur.remove()
    }
  }

  showCartAlert = () => {
    if (this.props.cart.type == 'package') {
      if (this.props.cart.cartItems[0].packages[0].status == CART_ITEM_STATUS.NOT_AVAILABLE) {
        this.setState({ 
          showCartAlert: true, 
          cartAlertText: 'Oops! The playlist that you chose is not available in your selected location. Please choose another playlist or another location.',
          cartAlertTitle:'Not Available',
          cartAlertActionText: 'Change Playlist'
        }) 
      }
      else{
        this.setState({ 
          showCartAlert: true, 
          cartAlertText: 'Oops! Product from one or more dates that you chose have recently sold out in your selected location. Please choose another date or another location.',
          cartAlertTitle:'Out Of Stock',
          cartAlertActionText:'Edit My Order',
        })
      }
    } else {
      this.setState({ 
        showCartAlert: true, 
        cartAlertText: 'Oops! One or more products that you chose have recently sold out, partially available or not available in your location. Please choose another product or another location',
        cartAlertTitle:'Product Availability Change',
        cartAlertActionText: 'Edit My Order'
      })
    }
  }
  
  checkCartValid = () => {
    if (!this.props.cart.valid) {
      this.showCartAlert()
    }
  }

  submitCheckout = () => {
    if (!this.props.cart.valid) {
      this.showCartAlert()
    } else {
      this.props.navigation.navigate('SingleOrderCheckoutPaymentScreen')
    }
  }

  removeUnavailableItem = (callback) => {
    for (const cartItem of this.props.cart.cartItems) {
      for (const item of cartItem.items) {
        if (item.status !== CART_ITEM_STATUS.AVAILABLE) {
          this.props.removeFromCart(item.id, Moment(cartItem.date))
        }
      }
    }
    setTimeout(() => {
      callback()
    }, 500)
  }

  checkAndValidate = (nextCallback = () => {}) => {
    const {
      cart,
      loading,
      addToCart,
    } = this.props
    SalesOrderService.validateCart(
      cart,
      loading,
      null,
      addToCart,
      nextCallback,
    )
  }

  setInitialAddress = () => {
    const {lastUsed, token, cart} = this.props
    if (_.isEmpty(cart.cartItems[0].deliveryAddress)) {
      SalesOrderService.getUserAddress(token).then(res => {
        if ((res.corporate && res.corporate.length > 0) || res.personal.length > 0) {
          let address = [...res.corporate || [], ...res.personal].find((item) => {
            return item.latitude == lastUsed.location.latitude && item.longitude == lastUsed.location.longitude
          })
          this.setDeliveryAddress(cart.cartItems[0].date, address, false)
        }
      })
    }

  }
  
  checkCartItemsIsEmpty = (cartItems: Delivery[]) => {
    let emptyCart = true

    for (let cartItem of cartItems) {
      if (cartItem.items.length > 0) {
        emptyCart = false
        break
      }
    }
    return emptyCart
  }

  checkDeliveryTimeAndAddress = (cartItem) => {
    const deliveryTimeMessage = 'Please set time for your delivery'
    const deliveryAddressMessage = 'Please set address for your delivery'
    if (cartItem.deliveryTime === '') {
      Alert.alert('Validation error', deliveryTimeMessage)
      return false
    }
    if (_.isEmpty(cartItem.deliveryAddress)) {
      Alert.alert('Validation error', deliveryAddressMessage)
      return false
    }
    return true
  }

  handleSubmitPress = () => {
    for (let cartItem of this.props.cart.cartItems) {
      if (!this.checkDeliveryTimeAndAddress(cartItem)) {
        return false
      }
    }

    this.removeUnavailableItem(() => this.checkAndValidate(this.submitCheckout))

    this.setState({
      transitioningToEmptyScreen: false,
      checkoutValidating: true,
    })
    this.mounted = true
  }

  setDeliveryAddress = (date: string, place: DeliveryAddress, isEdit: boolean) => {
    const { cartItems } = this.props.cart
    if (!isEdit) {
      this.props.updateDeliveryAddress(place)
    } else {
      const currentCartIndex = _.findIndex(cartItems, (item) => item.date === date)
      if (currentCartIndex > -1) {
        const address = cartItems[currentCartIndex].deliveryAddress
        const newAddress = {
          ...address,
          note: '',
          place,
        }
        this.props.updateDeliveryAddress(newAddress)
      }
    }
  }

  increaseCartHandler = (
    item: itemResponse,
    date: Moment.Moment,
    quantity: number,
  ) => {
    if (quantity < 1) {
      this.setState({
        showRemoveItemFromCartModal: true,
        itemToBeRemove: item,
        itemDateToBeRemove: date,
      })
    } else {
      const {
        cart,
        loading,
        resetCart,
        addToCart,
        stock,
        lastUsed,
      } = this.props

      const {
        maxItem,
      } = this.state
      if (SalesOrderService.isCategoryQtyMax(cart, date, 3)) {
        return Toast.show({
          text: `Max. order qty for ${cart.category} is 3 items per delivery date.`,
          buttonText: '',
          duration: 3000,
        })
      }
      const remaining = StockService.getItemStock(item.id, stock.main)
      if ((maxItem > 0 && quantity > maxItem) || quantity > quantity) {
        SalesOrderService.onMaxItem(maxItem, remaining)
        return
      }

      if (!cart.fetching) {
        SalesOrderService.addToCart(
          cart,
          item,
          date,
          quantity,
          lastUsed.code,
          loading,
          resetCart,
          addToCart,
        )
      }
    }
    SalesOrderService.logEvent(item, quantity, item.category)
  }

  decreaseCartHandler = (
    item: itemResponse,
    date: Moment.Moment,
    quantity: number,
  ) => {
    if (quantity < 1) {
      this.setState({
        showRemoveItemFromCartModal: true,
        itemToBeRemove: item,
        itemDateToBeRemove: date,
      })
    } else {
      const {
        cart,
        loading,
        resetCart,
        addToCart,
        stock,
        lastUsed,
      } = this.props

      const {
        maxItem,
      } = this.state
      const remaining = StockService.getItemStock(item.id, stock.main)
      if ((maxItem > 0 && quantity > maxItem) || quantity > quantity) {
        SalesOrderService.onMaxItem(maxItem, remaining)
        return
      }

      if (!cart.fetching) {
        SalesOrderService.addToCart(
          cart,
          item,
          date,
          quantity,
          lastUsed.code,
          loading,
          resetCart,
          addToCart,
        )
      }
    }
    SalesOrderService.logEvent(item, quantity, item.category)
  }
  onToggleAlertBox = () => {
    this.setState({ showErrorModal: !this.state.showErrorModal })
  }

  onConfirmRemoveItemFromCart = () => {
    const { itemToBeRemove, itemDateToBeRemove } = this.state
    const { type } = this.props.cart

    if (type === 'item') {
      this.props.removeFromCart(itemToBeRemove.id, itemDateToBeRemove)
      this.setState({
        showRemoveItemFromCartModal: false,
        itemToBeRemove: null,
        itemDateToBeRemove: null,
      }, this.checkAndValidate)
    } else {
      this.setState({
        showRemoveItemFromCartModal: false,
      }, () => {
        this.props.resetCart()
      })
    }
  }

  renderThemeItemRow = (item, date, index) => {
    return (
      <View style={styles.itemRowLineContainer} key={index}>
        <View style={{
          flex: 2,
          alignItems: 'flex-start',
        }}>
          <Image
            source={{ uri: item.cart_image }}
            style={{
              width: scale(78),
              height: scale(78),
              backgroundColor: Colors.lightGrey,
              borderRadius: 3,
            }}
            resizeMode='stretch'
          />
        </View>
        <View style={{
          flex: 5,
          alignItems: 'flex-start',
          marginLeft: scale(13),
        }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={[
              styles.itemNameText,
              { flex: 1 },
            ]}>{`${item.package_name} Package`}</Text>
          </View>
          {!isNaN(item.subtotal) &&
          <View style={{ marginTop: verticalScale(2) }}>
            <Text style={styles.priceText}>{LocaleFormatter.numberToCurrency(
              item.subtotal)}</Text>
          </View>
          }
          <View style={{ marginTop: verticalScale(2) }}>
            <Text style={styles.themeStartDateText}>{`Starting from : ${Moment(
              date)
              .format('DD MMMM YYYY')}`}</Text>
          </View>
        </View>
        <View style={{
          flex: 1,
          alignItems: 'flex-end',
          justifyContent: 'flex-start',
        }}>
          <TouchableOpacity
            testID="deletePackage"
            accessibilityLabel="deletePackage"
            onPress={() => this.setState({
              showRemoveItemFromCartModal: true,
            })}
          >
            <Feather
              name='x'
              size={scale(20)}
              color={Colors.pinkishGrey}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderItemRow = (item: itemResponse, date, index, exclusive) => {
    return (
      <View
        style={styles.itemRowLineContainer}
        key={index}
      >
        <View
          style={{
            flex: 1,
            alignItems: 'center',
          }}
        >
          <Image
            source={{ uri: item.images.small_thumb }}
            style={{
              width: scale(78),
              height: scale(78),
            }}
          />
        </View>
        <View style={{ flex: 3 }}>
          <View
            style={{
              flex: 1,
              paddingLeft: scale(13),
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
              }}
            >
              <View style={{ flex: 8 }}>
                <Text style={styles.itemNameText}>
                  {item.name}
                </Text>
              </View>
              {([CART_ITEM_STATUS.EXPIRED, CART_ITEM_STATUS.SOLD_OUT, CART_ITEM_STATUS.NOT_AVAILABLE]
                .includes(item.status) || item.remaining_quantity <= 0)  && (
                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                  }}
                >
                  <TouchableOpacity
                    testID="removeItem"
                    accessibilityLabel="removeItem"
                    onPress={() => this.setState({
                      itemToBeRemove: item,
                      itemDateToBeRemove: Moment(date),
                    },() => {
                      this.onConfirmRemoveItemFromCart()
                    })}
                  >
                    <Ionicons
                      name='ios-trash'
                      size={20}
                      style={{
                        color: Colors.primaryGrey,
                        margin: 0,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              )}
            </View>
            {item.status == CART_ITEM_STATUS.NEED_UPDATE && item.remaining_quantity > 0 && (
              <Text style={styles.stockText}>
                Stock Left: {item.remaining_quantity}
              </Text>
            )}
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              paddingLeft: scale(13),
              alignItems: 'center',
            }}
          >
            <View style={{ flex: 1 }}>
              <Text style={styles.priceText}>
                {LocaleFormatter.numberToCurrency(item.sale_price)}
              </Text>
            </View>
            {item.status == CART_ITEM_STATUS.EXPIRED && (
              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                }}
              >
                <Text style={styles.invalidText}>
                  Order Closed
                </Text>
              </View>
            )}
            {(item.status == CART_ITEM_STATUS.SOLD_OUT || item.remaining_quantity <= 0)&& (
              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                }}
              >
                <Text
                  style={styles.invalidText}
                >
                  Sold out
                </Text>
              </View>
            )}

            {item.status == CART_ITEM_STATUS.NOT_AVAILABLE && (
              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                }}
              >
                <Text
                  style={styles.invalidText}
                >
                  Not Available
                </Text>
              </View>
            )}
            {[CART_ITEM_STATUS.AVAILABLE, CART_ITEM_STATUS.NEED_UPDATE].includes(item.status) && item.remaining_quantity > 0 && (
              <View
                style={{
                  flex: 1,
                  alignItems: 'flex-end',
                }}
              >
                <CartQuantitySpinner
                  item={item}
                  date={date}
                  handleIncreasePress={this.increaseCartHandler}
                  handleDecreasePress={this.decreaseCartHandler}
                  max={item.remaining_quantity}
                  toggleAlertBox={this.onToggleAlertBox}
                  showErrorModal={this.state.showErrorModal}
                  testIDPrefix={index}
                  exclusive={exclusive}
                />
              </View>
            )}
          </View>
        </View>
      </View>
    )
  }

  renderAddressForm = (cartItem) => {
    const {
      navigation,
      cart,
      addToCart,
    } = this.props
    return (
      <SmartAddressForm
        onChangeAddressPress={this.changeAddressHandler}
        date={cartItem.date}
        navigation={navigation}
        cart={cart}
        updateUserCart={addToCart}
      />
    )
  }

  changeAddressHandler = () => {
    SalesOrderService.getDeliveryAddress(
      this.props.token,
      this.setLoading,
      this.props.navigation,
      false,
      false,
      false,
    )
  }
  
  editOrderHandler = () => {
    const {cart, navigation} = this.props
    if (cart.type == 'package') {
      if (cart.cartItems[0].packages[0].status == CART_ITEM_STATUS.NOT_AVAILABLE) {
        this.props.resetCart()
        navigation.navigate('HomeScreen', { isFromCheckout: true })
      }
      else{
        navigation.navigate('ThemeCalendarScreen', {themeId: cart.cartItems[0].packages[0].package_id},
        )
      }
    } else {
      this.setState({ showCartAlert: false })
    }
  }
  
  waLinkFormatted = () => {
    let waLink = ''
    let waSetting = {}
    const { setting } = this.props
    if (setting.hasOwnProperty('cs_whatsapp_number')) {
      waSetting = JSON.parse(setting['cs_whatsapp_number'])
      if (waSetting) {
        waLink = waSetting['number'].replace('62', 'https://wa.me/62')
      }
    }
    return waLink
  }

  handleOpenLink = (url) => {
    Linking.canOpenURL(url)
      .then((supported) => {
        if (supported) {
          return Linking.openURL(url)
            .catch()
        }
      })
      .catch()
  }

  navigationGoBack = () => {
    const {
      params,
    } = this.props.navigation.state
    if (params && params.navigateFrom) {
      this.props.navigation.navigate(
        'ThemeNavigation',
        {
          id: params.id,
          title: params.title,
        },
        NavigationActions.navigate({
          routeName: params.navigateFrom,
          params: {
            themeId: params.id,
          },
        }),
      )
    } else {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
        }),
      )
    }
  }

  whatsappButton = () => {
    return (
      <TouchableOpacity
        testID="shareWa"
        accessibilityLabel="shareWa"
        onPress={() => this.handleOpenLink(this.waLinkFormatted())}
      >
        <Image
          source={Images.whatsappIcon}
          resizeMode="contain"
          style={{
            width: scale(24),
            height: scale(24),
          }}
        />
      </TouchableOpacity>
    )
  }

  onPressBrowseMeal = () => {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'HomeScreen',
          }),
        ],
      }),
    )
  }

  setLoading = async (status: boolean) => {
    this.setState({
      loading: status,
    })
  }

  selectAddressHandler = (address: Address) => {
    const {
      navigation,
      updateDeliveryAddress,
    } = this.props
    updateDeliveryAddress(address)
    navigation.pop(3)
    this.props.addLastUsed(this.props.lastUsed.code, {
      latitude: address.latitude,
      longitude: address.longitude
    }, address.label, address.address)
  }

  navigateToSearchLocation = () => this.props.navigation.navigate('SearchLocation', { noSearchLocation: false })

  navigateToMapScreen = () => {
    const {navigation, lastUsed} = this.props
    navigation.navigate({
      routeName: 'CoMapScreen',
      params:{
        createScreenName: 'CoCreateAddress',
        mapScreenName: 'CoMapScreen',
        selectHandler: this.selectAddressHandler,
        selectedAddress:{
          addressDetail: lastUsed.address,
          latitude: lastUsed.location.latitude,
          longitude: lastUsed.location.longitude,
        }
      },
    })
  }


  renderBottomButton = () => {
    let buttonText
    let buttonAction
    let disableButton = false
    let itemsKey = this.props.cart.type == 'package' ? 'packages' : 'items'

    const cartItems = this.props.cart.cartItems[0]
    // delivery not completed, only pinpoint
    if (_.isEmpty(cartItems.deliveryAddress)) {
      buttonText = 'Enter Full Address'
      buttonAction = this.navigateToMapScreen
    }
    // has item that need to update
    else if(cartItems[itemsKey].some(item => item.status == CART_ITEM_STATUS.NEED_UPDATE)){
      buttonText = 'Proceed To Payment'
      buttonAction = this.handleSubmitPress
      disableButton = true
    }// no available item in cart
    else if(cartItems[itemsKey].every(item => item.status != CART_ITEM_STATUS.AVAILABLE)){
      buttonText = 'Clear Cart'
      buttonAction = this.destroyOrder
    }
    else{
      buttonText = 'Proceed To Payment'
      buttonAction = this.handleSubmitPress
    }

    return (
      (!this.state.showCartAlert || this.props.navigation.state.params?.isFromPayment) &&
      <Footer
        style={{
          backgroundColor: this.props.cart.fetching || disableButton
            ? Colors.disable
            : Colors.primaryOrange,
          borderTopWidth: 0,
        }}
      >
        <TouchableOpacity
          testID={'proceedToPaymentButton'}
          accessibilityLabel={'proceedToPaymentButton'}
          disabled={this.props.cart.fetching || disableButton}
          onPress={buttonAction}
          style={styles.proceedToPaymentContainer}
        >
          <View style={{ flex: 2 }}>
            <Text style={styles.proceedToPaymentText}>
              {buttonText}
            </Text>
          </View>
        </TouchableOpacity>
      </Footer>
    )
  }

  render() {
    const {
      cart: {
        cartItems,
        type,
        exclusive,
      },
      lastUsed
    } = this.props
    const {
      transitioningToEmptyScreen,
      showRemoveItemFromCartModal,
      loading,
    } = this.state
    return (
      <Container testID='SingleOrderCheckoutScreen'>
        <NavigationBar
          leftSide='back'
          title='Checkout'
          rightSide={this.whatsappButton}
          leftSideNavigation={this.navigationGoBack}
        />
        {!cartItems.length && (
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
              padding: 0,
              margin: 0,
            }}
          >
            <Image
              source={Images.emptyCart}
              style={{
                width: scale(117),
                height: verticalScale(124),
              }}
              resizeMode="contain"
            />
            <View
              style={{
                marginTop: verticalScale(24),
                marginBottom: verticalScale(20),
              }}
            >
              <Text
                style={{
                  fontFamily: 'Rubik-Medium',
                  fontSize: scale(16),
                  color: Colors.primaryDark,
                }}
              >
                Your Cart is Empty
              </Text>
            </View>

            <TouchableOpacity
              onPress={this.onPressBrowseMeal}
              style={{
                borderRadius: scale(4),
                height: verticalScale(40),
                backgroundColor: Colors.primaryOrange,
                justifyContent: 'center',
              }}
            >
              <Text
                style={{
                  paddingHorizontal: scale(70),
                  paddingVertical: verticalScale(8),
                  color: Colors.white,
                  fontSize: scale(16),
                  lineHeight: verticalScale(24),
                  textAlign: 'center',
                  fontFamily: 'Rubik-Regular',
                }}
              >
                Browse Meals
              </Text>
            </TouchableOpacity>
          </View>
        )}

        {cartItems && cartItems.length > 0 && (
          <>
            <Content>
              <Spinner
                visible={loading}
                textStyle={{ color: Colors.primaryOrange }}
              />
              {cartItems.map((cartItem, index) => {
                return (
                  <View key={index}>
                    <View
                      key={cartItem.date}
                      style={{
                        paddingTop: verticalScale(17),
                        paddingHorizontal: scale(20),
                        borderBottomColor: Colors.lightGrey,
                        borderBottomWidth: 8,
                      }}
                    >
                      <View>
                        <Text style={styles.dateText}>
                          {
                            Moment(cartItem.date)
                              .locale('en')
                              .format('ddd, DD MMM YYYY')
                          }
                        </Text>
                      </View>
                      <View>
                        {type === 'item'
                          ? (
                            cartItem.items && cartItem.items.map(
                              (item, index) =>
                                this.renderItemRow(
                                  item,
                                  cartItem.date,
                                  index,
                                  exclusive,
                                ),
                            )
                          )
                          : (
                            cartItem.packages && cartItem.packages.map(
                              (item, index) =>
                                this.renderThemeItemRow(
                                  item,
                                  cartItem.date,
                                  index,
                                ),
                            )
                          )}
                      </View>
                    </View>
                    {(index === cartItems.length - 1) &&
                    <View
                      style={{
                        paddingTop: verticalScale(17),
                        paddingHorizontal: scale(20),
                      }}
                    >
                      <SmartDeliveryTimePicker
                        type={'checkout'}
                        date={cartItem.date}
                        cart={this.props.cart}
                        setting={this.props.setting}
                        updateDeliveryTime={this.props.updateDeliveryTime}
                      />
                      {_.isEmpty(cartItem.deliveryAddress)
                        ? (
                          <SelectDeliveryAddressButton
                            testID='selectAddressDelivery'
                            accessibilityLabel="selectAddressDelivery"
                            deliveryAddressOptionPicker={this.navigateToMapScreen}
                          />
                        )
                        : (
                          this.renderAddressForm(cartItem)
                        )}

                        {
                          (lastUsed && _.isEmpty(cartItem.deliveryAddress)) &&
                          <View style={{
                            flex: 1
                          }}>
                            {row('location',Colors.bloodOrange,() => (
                              <View style={{flex:9, justifyContent: 'center'}}>
                                <Text> {lastUsed.name} </Text>
                              </View>
                            ))}
                          </View>
                        }
                    </View>
                    }
                  </View>
                )
              })}
              {cartItems.length === 1 &&
              <View style={{ height: 50 }} />
              }
            </Content>

              {this.renderBottomButton()}
          </>
        )}

        <AlertBox
          primaryAction={this.destroyOrder}
          dismiss={this.destroyOrder}
          primaryActionText='Ok'
          text="Oops! One or more of the items on your cart is now out of stock. Please choose other item on the catalogue."
          visible={transitioningToEmptyScreen}
          title="Out of Stock"
        />

        <AlertBox
          primaryAction={() => this.setState({ showRemoveItemFromCartModal: false })}
          dismiss={() => this.setState({ showRemoveItemFromCartModal: false })}
          primaryActionText='Cancel'
          secondaryAction={this.onConfirmRemoveItemFromCart}
          secondaryActionText='Yes'
          text="Are you sure you want to remove your order?"
          visible={showRemoveItemFromCartModal}
          title={'Remove Item?'}
        />

        <AlertBox
          primaryAction={this.editOrderHandler}
          primaryActionText={this.state.cartAlertActionText}
          secondaryAction={this.changeAddressHandler}
          secondaryActionText='Change Location'
          dismiss={() => this.setState({ showCartAlert: false })}
          title={this.state.cartAlertTitle}
          text={this.state.cartAlertText}
          visible={this.state.showCartAlert && !this.props.navigation.state.params?.isFromPayment}
        />
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    cart: state.cart,
    loginState: state.login,
    isFetchingCompanyAddresses: state.addressType.loading,
    isFetchingCustomerAddresses: state.customerAddress.loading,
    customerAddress: state.customerAddress,
    setting: state.setting.data,
    theme: state.theme,
    isConnected: state.network.isConnected,
    nav: state.nav,
    token: state.login.token,
    stock: state.meals.stock,
    lastUsed: state.lastUsed,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetCart: () => dispatch(CartActions.resetCart()),
    addToCart: (cart: responseData) => dispatch(CartActions.addToCart(cart)),
    loading: (status: boolean) => dispatch(CartActions.loading(status)),
    removeFromCart: (itemId: number, date: Moment.Moment) => dispatch(
      CartActions.removeFromCart(itemId, date)),
    updateDeliveryTime: (time: string) => dispatch(
      CartActions.updateTime(time)),
    updateDeliveryAddress: (address: DeliveryAddress) => dispatch(
      CartActions.updateAddress(address),
    ),
    fetchSetting: (key) => dispatch(SettingActions.fetchSetting(key)),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
    addLastUsed: (code: string, location: Location, name: string, address?: string) => dispatch(LastUsedActions.addLastUsed(code, location, name, address)),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleOrderCheckoutScreen)
