import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Modal from 'react-native-modal'
import { connect } from 'react-redux'
import {withNavigation} from 'react-navigation'
import AuthBackgroundView from '../Components/Authentication/AuthBackgroundView'
import FlexButton from '../Components/Common/FlexButton'
import TextInput from '../Components/Common/TextInput'
import PrimaryButton from '../Components/PrimaryButton'
import actions from '../Redux/ForgotPasswordRedux'
import Colors from '../Themes/Colors'
import Styles from './Styles/ForgotPasswordScreenStyle'

class ForgotPasswordScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      submitting: false,
      error: null,
      submitted: false,
      showAlertModal: false,
      email: '',
      hasError: false
    }
  }
  componentDidMount () {
    this.props.reset()
  }
  UNSAFE_componentWillReceiveProps (newProps) {
    this.forceUpdate()
    this.setState({
      submitting: newProps.forgotPassword.submitting,
      error: newProps.forgotPassword.error,
      submitted: newProps.forgotPassword.submitted
    })

    if (newProps.forgotPassword.error !== null) {
      this.setState({
        hasError: true
      })
    }
  }
  handleSubmit () {
    this.props.submit(this.state.email)
  }
  navigateToLogin () {
    this.props.navigation.goBack()
  }

  renderForgotPasswordBox () {
    const {email, submitting} = this.state
    return (
      <View>
        <TextInput
          text='Email'
          autoCapitalize='none'
          onChangeText={(text) => this.setState({email: text})}
          value={email}
          keyboardType='email-address'
          testID="forgotPasswordEmailField"
          accessibilityLabel="forgotPasswordEmailField"
        />
        <View style={{height: 25}} />
        <FlexButton
          testID="forgotPasswordSubmit"
          accessibilityLabel="forgotPasswordSubmit"
          text={submitting ? 'Submitting' : 'Submit'}
          disableShadow
          disabled={!this.props.isConnected || submitting}
          buttonStyle={{backgroundColor: Colors.bloodOrange}}
          textStyle={{color: 'white'}}
          onPress={() => this.handleSubmit()}
        />
      </View>
    )
  }
  render () {
    const {error, submitted} = this.state
    const showModal = (submitted && error === null) || error !== null

    return (
      <AuthBackgroundView
        boxTitle='Forgot Password'
        boxSubtext="Please enter the email address that you used to register. We'll send you an email with instructions to reset your password."
        navigation={this.props.navigation}
      >
        {this.renderForgotPasswordBox()}
        <Modal
          backdropColor='white'
          backdropOpacity={1}
          isVisible={showModal}
          onBackButtonPress={() => {}}
        >
          <View style={Styles.alertBoxContainer}>
            <View style={{flex: 1}}>
              <Text style={Styles.alertModalText}>{error === null ? 'A new password has been sent to your email' : error}</Text>
            </View>
            <View style={{flex: 1, marginTop: 25}}>
              <PrimaryButton
                label='OK'
                buttonStyle={Styles.button}
                onPressMethod={() => {
                  if (error === null) {
                    this.navigateToLogin()
                  } else {
                    this.props.reset()
                    this.setState({hasError: false, error: null})
                  }
                }}
              />
            </View>
          </View>
        </Modal>
      </AuthBackgroundView>
    )
  }
}

const mapStateToProps = state => {
  return {
    forgotPassword: state.forgotPassword,
    isConnected: state.network.isConnected
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reset: () => dispatch(actions.reset()),
    submit: (email) => dispatch(actions.forgotPassword(email))
  }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordScreen))
