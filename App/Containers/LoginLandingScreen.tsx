import React from 'react'
import { Image, Platform, Text, TouchableOpacity, View, Alert } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
  NavigationActions,
  SwitchActions,
} from 'react-navigation'
import { connect } from 'react-redux'
import FlexButton from '../Components/Common/FlexButton'
import LoginActions, { LoginState, User } from '../Redux/LoginRedux'
import CustomerApi from '../Services/ApiCustomer'
import { Colors, Images } from '../Themes'
import Styles from './Styles/LoginLandingScreenStyles'
import { AppState } from '../Redux/CreateStore'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import { scale, verticalScale } from 'react-native-size-matters'

export interface LoginLandingScreenProps {
  navigation: NavigationDrawerProp
  user: User,
  loginState: LoginState,
  loginError: string,
  onLoginSuccess: (data: User, token: string) => void
}

export interface LoginLandingScreenState {
  username: string,
  password: string,
  scrollEnabled: boolean,
  spinnerVisible: boolean,
}

const FBSDK = require('react-native-fbsdk')
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken,
} = FBSDK

class LoginLandingScreen extends React.Component<LoginLandingScreenProps, LoginLandingScreenState> {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      scrollEnabled: false,
      spinnerVisible: false
    }
  }

  componentDidUpdate(prevProps: Readonly<LoginLandingScreenProps>): void {
    if (this.props.user && !prevProps.user) {
      this.props.navigation.dispatch(
        SwitchActions.jumpTo({
          routeName: 'NavigationDrawer'
        })
      )
    }
  }

  navigateToPhoneVerification = (customer, login_type) => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'PhoneVerificationScreen',
      params: {
        customer: customer,
        socialLoginType: login_type
      }
    })
    this.props.navigation.dispatch(navigateAction)
    this.setState({
      spinnerVisible: false
    })
  }

  toggleSpinner = () => {
    this.setState({
      spinnerVisible: !this.state.spinnerVisible
    })
  }

  costumerData = (result, credentials, login_type) => {
    return {
      salutation: '',
      first_name: login_type === 'facebook' ? result.first_name : result.fullName.givenName,
      last_name: login_type === 'facebook' ? result.last_name : result.fullName.familyName,
      email: result.email,
      password: login_type === 'facebook' ? result.id : result.user,
      confirmPassword: login_type === 'facebook' ? result.id : result.user,
      facebookCredentials: JSON.stringify(credentials),
      isRegisterWithFacebook: login_type === 'facebook' && true,
      phone: null,
      social_id: login_type === 'facebook' ? result.id : result.user,
      countryCode: result.countryCode
    }
  }

  handleFacebookLogin = () => {
    let that = this
    this.toggleSpinner()
    // reset facebook sdk login status
    if (this._isFacebookLoggedIn()) {
      LoginManager.logOut()
    }

    LoginManager.logInWithPermissions(['email', 'public_profile']).then(
      function(result) {
        if (result.isCancelled) {
          that.toggleSpinner()
        } else {
          let req = new GraphRequest(
            '/me',
            {
              httpMethod: 'GET',
              version: 'v3.1',
              parameters: {
                fields: {
                  string: 'name,email,first_name,last_name'
                }
              }
            },
            that._infoCallback
          )
          new GraphRequestManager().addRequest(req).start()
        }
      },
      function() {
        that.toggleSpinner()
      }
    )
  }

  _infoCallback = (error, result, login_type = 'facebook') => {
    if (error) {
      this.toggleSpinner()
    } else {
      const customerApi = CustomerApi.create()
      customerApi.validate({
        login_type,
        email: result.email ? result.email : result.user,
        facebook_id: result.id,
        apple_id: result.user
      }).then((response) => {
        return response.data
      }).then((responseData) => {
        if (responseData.error) {
          this.toggleSpinner()
          Alert.alert(responseData.message)
          return
        }

        if (!responseData.exist) {
          const credentials = AccessToken.getCurrentAccessToken()
          const customer = this.costumerData(result, credentials, login_type)
          this.navigateToPhoneVerification(customer, login_type)
        } else {
          const email = result.email ? result.email : result.user
          const password = login_type === 'facebook' ? result.id : result.user
          customerApi.auth(email, password)
          .then((response) => {
            this.toggleSpinner()
            return response.data
          }).then((responseData) => {
            if (!responseData.error) {
              const {data, token} = responseData
              this.props.onLoginSuccess(data, token)
            } else {
              Alert.alert(responseData.message)
            }
          }).catch(() => {
            this.toggleSpinner()
          })
        }
      }).catch((error) => {
        if (error) {
          this.toggleSpinner()
        }
      })
    }
  }

  navigateCreateAccount = () => {
    this.props.navigation.navigate('EmailPhoneVerificationScreen')
  }

  navigateBrowseMenu = () => {
    const { navigation } = this.props
    navigation.navigate('HomeScreen')
  }

  navigateLogin = () => {
    this.props.navigation.navigate('Login')
  }

  facebookButton = () => {
    return (
      <FlexButton
        containerStyle={{paddingHorizontal: scale(50)}}
        buttonStyle={{backgroundColor: Colors.duskBlue, borderRadius: 6}}
        onPress={() => this.handleFacebookLogin()}
        testID="signInFbButton"
        accessibilityLabel="signInFbButton"
      >
        <Icon
          name='facebook'
          size={scale(18)}
          color='white'
          style={{marginLeft: scale(10)}}
        />
        <Text style={Styles.textFb}>Sign in with Facebook</Text>
      </FlexButton>
    )
  }

  content () {
    return (
      <View style={{flex: 1}}>
        <View style={Styles.boxTop}>
          <Image source={Images.YummyboxBeta} style={{width: 200, height: 140}} resizeMode='contain' />
        </View>
        <View style={Styles.middleBox}>
          {this.facebookButton()}
          <FlexButton
            containerStyle={{paddingHorizontal: scale(50)}}
            buttonStyle={{backgroundColor: 'white', borderRadius: 6}}
            text='Create Account'
            onPress={() => this.navigateCreateAccount()}
            testID="createAccountButton"
            accessibilityLabel="createAccountButton"
          />
          <TouchableOpacity
            testID='toLoginScreenButton'
            accessibilityLabel="toLoginScreenButton"
            style={Styles.loginButton}
            onPress={() => this.navigateLogin()}
          >
            <Text style={Styles.loginText}>Log In</Text>
          </TouchableOpacity>
        </View>
        <View style={Styles.footerContainer}>
          <TouchableOpacity
            testID='browseMenuButton'
            style={Styles.browseMenuButton}
            onPress={() => this.navigateBrowseMenu()}
            accessibilityLabel="browseMenuButton"
          >
            <Text style={Styles.BrowseMenuText}>Browse Menu</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _isFacebookLoggedIn = () => {
    const accessToken = AccessToken.getCurrentAccessToken()
    return accessToken != null
  }

  render () {
    let {spinnerVisible} = this.state
    return (
      <View testID='LoginLandingScreen' style={{flex: 1, backgroundColor: Colors.offWhite}}>
        <Spinner visible={spinnerVisible} textStyle={{color: '#FFF'}} />
        {
          Platform.OS === 'ios'
          ? (
              <View
                style={{
                  height: verticalScale(20),
                  backgroundColor: Colors.offWhite
                }}
              />
            )
          : <View />
        }
        {
          this.content()
        }
      </View>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    user: state.login.user,
    loginState: state.login,
    loginError: state.login.error,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onLoginSuccess: (user, token) => dispatch(LoginActions.onLoginSuccess(user, token)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginLandingScreen)
