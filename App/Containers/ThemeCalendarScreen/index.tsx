import { MealCard, MenuSlider } from '../../Components/Common'
import moment from 'moment'
import { Container, Content, Footer } from 'native-base'
import * as React from 'react'
import { Dimensions, Text, TouchableWithoutFeedback, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import { connect } from 'react-redux'
import NavigationBar from '../../Components/NavigationBar'
import StaticMealDetailPopUp from '../../Components/StaticMealDetailPopUp'
import YummyCalendar from '../../Components/YummyCalendar'
import CartActions, { CartState } from '../../Redux/V2/CartRedux'
import ThemeActions, {
  Item,
  ThemeDetail,
  ThemePeriod,
} from '../../Redux/ThemeRedux'
import AvailabilityDateService from '../../Services/AvailabilityDateService'
import CalendarService from '../../Services/CalendarService'
import LocaleFormatter from '../../Services/LocaleFormatter'
import { Colors } from '../../Themes'
import Styles from '../Styles/ThemeCalendarScreenStyle'
import MealDetailPopUpActions, { MealDetailPopUpReduxState } from '../../Redux/MealDetailPopUpRedux'
import { AppState } from '../../Redux/CreateStore'
import { responseData } from '../../Services/V2/ApiCart'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import { SettingDataInterface } from '../../Redux/SettingRedux'
import SalesOrderService from '../../Services/SalesOrderService'
import { baseLastUsed } from '../../Redux/V2/LastUsedRedux'

const { width: VIEWPORT_WIDTH } = Dimensions.get('window')

export interface ThemeCalendarProps {
  navigation: NavigationDrawerProp,
  selectedPackage: ThemePeriod,
  themeDetail: ThemeDetail,
  cart: CartState,
  isLoggedIn: boolean,
  deliveryCutOff: number | string,
  mealDetailPopUp: MealDetailPopUpReduxState,
  setting: SettingDataInterface,
  addToCart: (cart: responseData) => void,
  removeFirstDayOfAvailableDates: () => void,
  fetchMealDetail: (mealId: number, date: string) => void,
  resetMealDetailPopUp: () => void,
  resetCart: () => void,
  loading: (status: boolean) => void,
  lastUsed: baseLastUsed,
  fetchDetail: (id: number, kitchenCode: string) => void,
}

export interface ThemeCalendarStates {
  remainingDays: number,
  showPackagesMeals: boolean,
  haveSelectDate: boolean,
  activeDates: string[],
  selectedMeals: Item[],
  availableDates: string[],
  showMealDetailModal: boolean,
  waitingForSelectedMealsDownloaded: boolean,
  showLoading: boolean,
  selectedMealId?: number,
  selectedMealDate?: string,
  disabledDate?: string,
  openExtra: boolean,
}

export interface PlaylistItem {
  theme_name: string,
  theme_id: number,
  package_id: number,
  sell_price: number,
  dates: string[],
  package_name: string,
  cart_image: string,
  quantity: number,
}

class ThemeCalendarScreen extends React.Component<ThemeCalendarProps, ThemeCalendarStates> {
  deliveryCutOffTimeout
  willFocusSubscription

  constructor(props) {
    super(props)

    this.state = {
      remainingDays: 0,
      showPackagesMeals: false,
      haveSelectDate: false,
      activeDates: [],
      selectedMeals: [],
      availableDates: [],
      showMealDetailModal: false,
      waitingForSelectedMealsDownloaded: false,
      showLoading: false,
      selectedMealId: null,
      selectedMealDate: null,
      disabledDate: null,
      openExtra: false,
    }
  }

  componentDidMount() {
    if(this.props.navigation.state.params.key !== 'themeDetailScreen'){
      this.willFocusSubscription = this.props.navigation
        .addListener('willFocus', this._refreshThemeDetail)
    }

    this._updateCalendar()

    this.listenForCutOffTime()
  }

  _refreshThemeDetail = async () => {
    this.setState({
      showLoading: true,
    }, this._fetchThemeAction)
  }

  _fetchThemeAction = async () => {
    const {
      themeDetail,
      lastUsed,
      fetchDetail,
    } = this.props
    await fetchDetail(themeDetail.id, lastUsed.code)
    setTimeout(() => {
      this.setState({
        showLoading: false,
      }, () => this._updateCalendar(true))
    }, 1500)
  }

  componentWillUnmount() {
    clearTimeout(this.deliveryCutOffTimeout)
  }

  _updateCalendar = (needUpdate: boolean = false) => {
    const {
      selectedDates,
      haveSelectedDates,
      waitingForSelectedMealsDownloaded,
      availableDates,
    } = this.checkSelectedDates(needUpdate)

    this.setState({
      remainingDays: haveSelectedDates
        ? 0
        : (this.props.selectedPackage === null
          ? 0
          : this.props.selectedPackage.period),
      showPackagesMeals: haveSelectedDates,
      haveSelectDate: haveSelectedDates,
      activeDates: selectedDates,
      waitingForSelectedMealsDownloaded,
      availableDates: availableDates,
    }, () => {
      if (!haveSelectedDates) {
        const today = moment()
          .utcOffset(7)
          .add(1, 'days')
          .format('YYYY-MM-DD')
        this.selectDate({ dateString: today })
      }
    })
  }

  listenForCutOffTime = () => {
    const today = moment()
      .utcOffset(7)
      .add(1, 'days')
      .format('YYYY-MM-DD')

    this.deliveryCutOffTimeout = setTimeout(() => {
      const { availableDates, activeDates } = this.state
      this.setState({
        showLoading: true,
        disabledDate: today,
      }, () => {
        const hasSelectedFirstDayOfAvailableDates = activeDates.findIndex(date => date === today)
        if (hasSelectedFirstDayOfAvailableDates >= 0) {
          this.setState({
            activeDates: [],
            selectedMeals: [],
          }, () => {
            let newActiveDates = activeDates.filter(date => date !== today)
            this.selectDate({ dateString: newActiveDates[0] })
          })
        }
      })
      this.setState({
        availableDates: availableDates.filter(availableDate => availableDate !== today),
      })
      this.props.removeFirstDayOfAvailableDates()
      setTimeout(() => this.setState({ showLoading: false }), 500)
    }, AvailabilityDateService.getDeliveryCutOffCountDown(this.props.deliveryCutOff))
  }

  checkSelectedDates(needUpdate: boolean = false) {
    const { extend_dates, available_dates } = this.props.themeDetail
    let selectedDates = []
    let haveSelectedDates = false
    const { cartItems, type } = this.props.cart

    if (type === 'package' && cartItems.length > 0 && !needUpdate) {
      const { dates, package_id } = cartItems[0].packages[0]
      if (package_id === this.props.selectedPackage.id) {
        haveSelectedDates = true
        selectedDates = [...dates]
      }
    }
    if (this.props.selectedPackage === null) {
      haveSelectedDates = false
    }
    const waitingForSelectedMealsDownloaded = haveSelectedDates

    let availableDates = [
      ...available_dates,
    ]
    if (haveSelectedDates) {
      const extendDates = [
        ...available_dates,
        ...extend_dates,
      ]
      availableDates = CalendarService.populateAvailableDateForCalendar(
        available_dates,
        extendDates,
        selectedDates[0].date,
        this.props.selectedPackage.period,
        null,
      )
    }
    return {
      haveSelectedDates,
      selectedDates,
      waitingForSelectedMealsDownloaded,
      availableDates,
    }
  }

  selectDate = (selectedDate) => {
    const { extend_dates, available_dates, delivery_end } = this.props.themeDetail
    const {
      themeDetail,
    } = this.props
    let {
      activeDates,
    } = this.state
    if (themeDetail) {
      const {
        items,
      } = themeDetail
      if (items) {
        if (activeDates.length > 0) {
          let selectedMeal: any = items.find(item => item.date === selectedDate.dateString)
          if (!selectedMeal) {
            selectedMeal = {
              date: selectedDate.dateString,
            }
          }
          this.toggleMeals(selectedDate.dateString, selectedMeal)
        } else {
          const {
            selectedPackage,
          } = this.props
          const extendDates = [
            ...available_dates,
            ...extend_dates,
          ]
          const lastDate = extendDates[extendDates.length - 1]
          let mealDate = selectedDate.dateString
          for (let i = 0; i < selectedPackage.period; i++) {
            const unixMealDate = moment(mealDate)
              .unix()
            if (moment(lastDate)
                .unix() >= unixMealDate
              && moment(delivery_end)
                .unix() >= unixMealDate
            ) {
              while (extendDates.findIndex(date => date === mealDate) < 0) {
                mealDate = moment(mealDate)
                  .add(1, 'day')
                  .format('YYYY-MM-DD')
              }
              let selectedMeal: any = items.find(item => item.date === mealDate)
              if (!selectedMeal) {
                selectedMeal = {
                  date: mealDate,
                }
              }
              this.toggleMeals(mealDate, selectedMeal)
            }
            mealDate = moment(mealDate)
              .add(1, 'day')
              .format('YYYY-MM-DD')
          }
        }
      }
    }
  }

  toggleMeals = (selectedDate, selectedMeal) => {
    let {
      activeDates,
      selectedMeals,
      availableDates,
    } = this.state
    const {
      selectedPackage,
    } = this.props
    const { extend_dates } = this.props.themeDetail
    const selected = activeDates.findIndex(date => date === selectedDate)
    if (selected > -1) {
      activeDates = activeDates.filter(date => date !== selectedDate)
      selectedMeals = selectedMeals.filter(meal => meal.date !== selectedDate)
    } else {
      if (selectedPackage.period > activeDates.length) {
        const existsDate = availableDates.findIndex(date => date === selectedDate)
        if (existsDate > -1) {
          activeDates.push(selectedDate)
          selectedMeals.push(selectedMeal)
        } else {
          const extendDates = [
            ...availableDates,
            ...extend_dates,
          ]
          availableDates = CalendarService.populateAvailableDateForCalendar(
            availableDates,
            extendDates,
            activeDates[0],
            selectedPackage.period,
            null,
          )
          activeDates.push(selectedDate)
          selectedMeals.push(selectedMeal)
        }
      }
    }
    activeDates.sort((a, b) => {
      const A = moment(a)
        .unix()
      const B = moment(b)
        .unix()

      let comparison = 0
      if (A > B) {
        comparison = 1
      } else if (A < B) {
        comparison = -1
      }
      return comparison
    })

    selectedMeals.sort((a, b) => {
      const A = moment(a.date)
        .unix()
      const B = moment(b.date)
        .unix()

      let comparison = 0
      if (A > B) {
        comparison = 1
      } else if (A < B) {
        comparison = -1
      }
      return comparison
    })
    this.setState({
      activeDates,
      selectedMeals,
      availableDates,
      showPackagesMeals: activeDates.length > 0,
      remainingDays: selectedPackage.period - activeDates.length,
    })
  }

  addThemeToCart = () => {
    const {
      cart,
      loading,
      resetCart,
      addToCart,
      lastUsed,
    } = this.props
    const { activeDates } = this.state
    const item: PlaylistItem = {
      theme_name: this.props.themeDetail.name,
      theme_id: this.props.themeDetail.id,
      package_id: this.props.selectedPackage.id,
      sell_price: this.props.selectedPackage.sell_price,
      dates: activeDates,
      package_name: this.props.selectedPackage.name,
      cart_image: this.props.themeDetail.cart_image,
      quantity: 1
    }
    if (!cart.fetching) {
      SalesOrderService.addPlaylistCart(
        cart,
        item,
        activeDates[0],
        loading,
        resetCart,
        addToCart,
        () => this.props.navigation.navigate('SingleOrderCheckoutScreen'),
        lastUsed.code
      )
    }
    // this.props.setCartType('package')
    // this.props.setNavigation('PackageSingleOrderCheckoutScreen')
    // this.props.addToCart(item, moment(activeDates[0]))
  }

  renderMealItems = () => {
    const { selectedMeals } = this.state
    if (selectedMeals.length > 0) {
      return (
        <View style={Styles.selectedItemContainer}>
          <Text style={Styles.mealListPackageTitleText}>
            Your Selected Meals
          </Text>
          <View style={Styles.mealListSliderContainer}>
            <MenuSlider
              style={{
                marginTop: 16,
                paddingLeft: 16,
                paddingRight: 20,
                width: (VIEWPORT_WIDTH + 6),
                top: -8,
                left: -20,
              }}
              items={selectedMeals}
              renderItem={this.renderThemeItem}
            />
          </View>
        </View>
      )
    }
    return (
      <View style={Styles.selectedItemContainer}>
        <Text style={Styles.mealListPackageTitleText}>
          My Selected Meals
        </Text>
        <Text style={Styles.mealEmptyText}>
          Please select a date to show your meal
        </Text>
      </View>
    )
  }

  goBackAction = () => {
    if (!this.props.cart.valid) {
      this.props.resetCart()
    }
    this.props.navigation.goBack()
  }

  toggleExtra = () => {
    this.setState({
      openExtra: !this.state.openExtra,
    })
  }

  selectMeal = (mealId: number, date: string) => {
    this.setState({
      showMealDetailModal: true,
      selectedMealId: mealId,
      selectedMealDate: date,
    }, () => {
      this.props.fetchMealDetail(mealId, date)
    })
  }

  closeModal = () => {
    this.setState({
      showMealDetailModal: false,
      selectedMealId: null,
      selectedMealDate: null,
    }, this.props.resetMealDetailPopUp)
  }

  renderThemeItem = ({ item }) => {
    if (item.id) {
      const image = item.catalog_image
      const title = item.name
      const caption = item.formatted_date

      return (
        <MealCard
          style={{
            height: 191,
            width: 120,
            borderRadius: 8,
            marginTop: 8,
            marginBottom: 12,
            marginLeft: 4,
            marginRight: 4,
          }}
          contentStyle={{ height: 79 }}
          captionStyle={{ fontSize: 11 }}
          titleStyle={{ fontSize: 12 }}
          imageContainerStyle={{
            height: 120,
            width: 120,
          }}
          image={image}
          title={title}
          caption={caption}
          onPress={() => this.selectMeal(item.id, item.date)}
        />
      )
    }
    return (
      <MealCard
        style={{
          height: 191,
          width: 120,
          borderRadius: 8,
          marginTop: 8,
          marginBottom: 12,
          marginLeft: 4,
          marginRight: 4,
        }}
        contentStyle={{ height: 79 }}
        captionStyle={{ fontSize: 11 }}
        titleStyle={{ fontSize: 12 }}
        imageContainerStyle={{
          width: 120,
          height: 120,
        }}
        imageStyle={{
          width: 120,
        }}
        image={null}
        title={'Coming Soon'}
        caption={moment(item.date)
          .format('ddd, DD MMM YYYY')}
        onPress={() => {}}
      />
    )
  }

  render() {
    const {
      remainingDays,
      showMealDetailModal,
      availableDates,
      openExtra,
      activeDates,
      disabledDate,
    } = this.state
    const {
      cart,
    } = this.props
    const disableProceedButton = (remainingDays > 0)
    const disableProceedButtonColor = disableProceedButton
      ? { backgroundColor: Colors.disable }
      : {}
    const disableProceedTextColor = disableProceedButton
      ? { color: Colors.primaryGrey }
      : {}
    const remainingDaysText = LocaleFormatter.pluralTranslation(remainingDays, 'More Day', 'More Days')
    const proceedButtonText = disableProceedButton
      ? `Select ${remainingDaysText}`
      : 'Proceed to Checkout'

    return (
      <Container>
        <NavigationBar
          leftSide="back"
          leftSideNavigation={this.goBackAction}
          title={this.props.themeDetail.name}
        />
        <Content contentContainerStyle={{ flexGrow: 1 }}>
          <Spinner
            visible={cart.fetching}
            textStyle={{ color: '#FFF' }}
          />
          <YummyCalendar
            minDate={availableDates[0]}
            maxDate={availableDates[availableDates.length - 1]}
            availableDates={availableDates}
            actives={activeDates}
            disabled={[disabledDate]}
            onSelectDate={this.selectDate}
            warningMessage={'Tap to deselect and select the date of your choice'}
            openExtra={openExtra}
            onExtraPress={this.toggleExtra}
            extraOnClose={this.toggleExtra}
            extraTitle={'Why can’t I select the date?'}
            extraMessage={'Other than public holiday, the grayed out dates indicate that the meal on that day is sold out or isn\'t published yet.'}
            offDates={this.props.setting.off_dates}
            disabledByDefault
            disabledWeekEnd
          />
          {this.renderMealItems()}
          <StaticMealDetailPopUp
            closeModal={this.closeModal}
            visible={showMealDetailModal}
            mealId={this.state.selectedMealId}
            date={this.state.selectedMealDate}
            mealDetailPopUp={this.props.mealDetailPopUp}
          />
          <Spinner
            visible={this.state.showLoading}
          />
        </Content>
        <Footer
          style={{
            backgroundColor: disableProceedButton ? disableProceedButtonColor.backgroundColor : Styles.proceedToPaymentButton.backgroundColor
          }}
        >
          <TouchableWithoutFeedback
            testID='proceedToCheckout'
            accessibilityLabel='proceedToCheckout'
            disabled={disableProceedButton}
            onPress={this.addThemeToCart}
          >
            <View
              style={[
                Styles.proceedToPaymentButton,
                disableProceedButtonColor,
              ]}
            >
              <Text
                style={[
                  Styles.footerButtonText,
                  disableProceedTextColor,
                ]}
              >
                {proceedButtonText}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    selectedPackage: state.theme.selectedPackage,
    themeDetail: state.theme.themeDetail,
    cart: state.cart,
    isLoggedIn: state.login.user !== null,
    deliveryCutOff: state.setting.data.delivery_cut_off,
    mealDetailPopUp: state.mealDetailPopUp,
    setting: state.setting.data,
    lastUsed: state.lastUsed,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (cart: responseData) => dispatch(CartActions.addToCart(cart)),
    resetCart: () => dispatch(CartActions.resetCart()),
    loading: (status: boolean) => dispatch(CartActions.loading(status)),
    removeFirstDayOfAvailableDates: () => dispatch(ThemeActions.removeFirstDayOfAvailableDates()),
    fetchMealDetail: (mealId: number, date: string) => dispatch(MealDetailPopUpActions.fetchMealDetail(mealId, date)),
    resetMealDetailPopUp: () => dispatch(MealDetailPopUpActions.resetMealDetailPopUp()),
    fetchDetail: (id: number, kitchenCode: string) => dispatch(ThemeActions.fetchDetail(id, kitchenCode)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ThemeCalendarScreen)
