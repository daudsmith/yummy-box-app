import React from 'react'
import { Animated, Dimensions, Image, ScrollView, Text, View } from 'react-native'
import { connect } from 'react-redux'
import WelcomeScreenActions from '../Redux/WelcomeScreenRedux'
import { Colors } from '../Themes/'
import LoginLandingScreen from './LoginLandingScreen'
import Styles from './Styles/WelcomeScreenStyle'

const {width} = Dimensions.get('window')

class WelcomeScreen extends React.Component {
  scrollX = new Animated.Value(0)

  componentDidMount() {
    this.props.fetchSlides()
  }

  renderSlides (slides) {
    return (
      slides.map((slide, index) => {
        return (
          <View style={Styles.slideContainer} key={index}>
            <View style={Styles.imageGrid}>
              <Image source={{uri: slide.image}} style={Styles.image} />
            </View>
            <View style={Styles.textGrid}>
              <Text style={Styles.title}>{slide.title}</Text>
              <Text style={Styles.description}>{slide.content}</Text>
            </View>
          </View>
        )
      })
    )
  }

  render () {
    let position = Animated.divide(this.scrollX, width)
    const {slides} = this.props.welcomeScreenState
    const emptyObject = {}
    const slidesAndLoginScreen = slides.concat(emptyObject)
    let {fetching, hasViewed} = this.props.welcomeScreenState

    return (
      <View style={Styles.welcomeScreenContainer}>
        <ScrollView
          horizontal={true}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: this.scrollX}}}], {
              useNativeDriver: false
            }
          )}
          scrollEventThrottle={16}
        >
          {this.renderSlides(slides)}
          <View style={Styles.slideContainer}>

            {
              // make LoginLandingScreen have delay render, maybe around 1 second
              fetching === false && hasViewed === true
                ? (<LoginLandingScreen {...this.props} disableBackground={true} />)
                : null
            }

          </View>
        </ScrollView>
        <View style={Styles.dotContainer}>
          {
            slidesAndLoginScreen.map((_, i) => {
              let backgroundColor = position.interpolate({
                inputRange: [i - 0.50000000001, i - 0.5, i, i + 0.5, i + 0.50000000001],
                outputRange: ['transparent', Colors.bloodOrange, Colors.bloodOrange, Colors.bloodOrange, 'transparent'],
                extrapolate: 'clamp'
              })
              let dotSize = position.interpolate({
                inputRange: [i - 0.50000000001, i - 0.5, i, i + 0.5, i + 0.50000000001],
                outputRange: [7, 10.6, 10.6, 10.6, 7],
                extrapolate: 'clamp'
              })
              let borderRadius = position.interpolate({
                inputRange: [i - 0.50000000001, i - 0.5, i, i + 0.5, i + 0.50000000001],
                outputRange: [3.5, 5.3, 5.3, 5.3, 3.5],
                extrapolate: 'clamp'
              })
              return (
                <Animated.View
                  key={i}
                  style={[Styles.dot, {backgroundColor: backgroundColor, height: dotSize, width: dotSize, borderRadius: borderRadius}]}
                />
              )
            })}
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    welcomeScreenState: state.welcomeScreen
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSlides: () => dispatch(WelcomeScreenActions.fetchSlides())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen)
