import React, { Component } from 'react'
import {Text, View, TouchableOpacity} from 'react-native'
import { connect } from 'react-redux'
import Spinner from 'react-native-loading-spinner-overlay'
import WithSafeAreaView from '../Components/Common/WithSafeAreaView'
import NavigationBar from '../Components/NavigationBar'
import PhoneNumberTextInputOne from '../Components/PhoneNumberTextInputOne'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import RegistrationActions from '../Redux/RegisterRedux'
import ModalSuccessOne from '../Components/ModalSuccessOne'
import {Colors} from '../Themes'
import Styles from './Styles/ChangePhoneNumberStyle'

let initialCondition = {text: '', iconName: '', iconColor: 'black'}
let emptyCondition = {text: '', iconName: '', iconColor: Colors.lightGreyX}
let successMessage = {text: '', iconName: 'check', iconColor: 'green'}
let invalidMessage = {text: 'Invalid phone format', iconName: 'close', iconColor: 'red'}

class ChangePhoneNumberScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      invalidPhoneNumberMessage: '',
      newPhoneNumber: this.props.user.phone,
      validationMessage: successMessage,
      showSpinner: false,
      showModalValidation: false,
      showModalSuccess: false,
      disableButtonSubmit: false,
      processingValidation: false,
    }
  }

  componentDidMount () {
    this.props.resetChangeEmailAndPhone()
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    let {identityExist, validatingIdentity} = newProps.registration

    if (this.state.processingValidation) {
      this.setState({showSpinner: false})
      // Jika nomor handphone belum terdaftar
      if (!identityExist && !validatingIdentity) {
        let countryCode = '62'
        this.props.changePhoneNumberValidate(this.state.newPhoneNumber.replace('+', ''), null, countryCode)
      }

      this.setState({processingValidation: false})
    }
    // updated phonenumber
    if (this.props.customerAccount.changingPhoneNumber && newProps.customerAccount.changingPhoneNumber === false) {
      if (newProps.customerAccount.changePhoneNumberSuccess) {
        this.setState({showModalSuccess: true})
      } else if (newProps.customerAccount.changePhoneNumberError !== null) {
        this.setState({validationMessage: {text: 'Phonenumber update failed', iconName: 'close', iconColor: 'red'}})
      }
      this.setState({showSpinner: false})
      this.props.resetChangeEmailAndPhone()
    }
    // end of updated phonenumber

    // jika nomor sudah terdaftar
    if (identityExist && !validatingIdentity) {
      this.setState({validationMessage: {text: 'This mobile number is already registered', iconName: 'close', iconColor: 'red'}})
    }
  }

  setRef (ref, field) {
    this[field] = ref
  }

  handleOnChangePhoneNumber (newPhoneNumber) {
    if (newPhoneNumber.length === 0) {
      // TextInput kosong
      this.setState({newPhoneNumber, validationMessage: emptyCondition})
    } else {
      if (newPhoneNumber.length < 12) {
        // TextInput ada isi, karakter kurang dari 12 digit
        this.setState({newPhoneNumber, validationMessage: initialCondition})
      } else {
        if (this.validatePhone(newPhoneNumber)) {
          // TextInput ada isi, karakter lebih dari 12 digit, validasi true
          this.setState({newPhoneNumber, validationMessage: successMessage})
        } else {
          this.setState({newPhoneNumber, validationMessage: invalidMessage})
        }
      }
    }
  }

  validatePhone (phone) {
    if (phone.length === 1) {
      return true
    } else {
      var re = /^[0-9]+$/
      return re.test(phone.substring(1))
    }
  }

  validatePhoneAgain () {
    let {newPhoneNumber, validationMessage} = this.state
    if (validationMessage === successMessage) {
      this.setState({showSpinner: true})
      this.setState({processingValidation: true})
      this.props.validateIdentity(newPhoneNumber.replace('+', ''))
    } else {
      this.setState({validationMessage: invalidMessage})
    }
  }

  close () {
    this.setState({showModalSuccess: false})
    this.props.resetChangeEmailAndPhone()
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    let {newPhoneNumber, invalidPhoneNumberMessage, showSpinner, validationMessage} = this.state

    return (
      <View style={{flex: 1}}>
        <Spinner visible={showSpinner} textStyle={{color: '#FFF'}} />
        <NavigationBar leftSide='back' title='Update Mobile Number' leftSideNavigation={this.navigationGoBack} />
          <View style={Styles.contentContainer}>
            <View style={{height: 210, backgroundColor: 'transparent'}}>
              <View style={Styles.contentMainBox}>
                <Text style={Styles.h1}>New Mobile Number</Text>
                <View style={{marginTop: 18, height: 65}}>
                  <PhoneNumberTextInputOne
                    setRef={ref => this.setRef(ref, 'newPhoneNumber')}
                    value={newPhoneNumber}
                    onChangePhoneNumber={(newPhoneNumber) => this.handleOnChangePhoneNumber(newPhoneNumber)}
                    showMessage={validationMessage}
                  />
                  <Text style={Styles.phoneValidationText}>{invalidPhoneNumberMessage}</Text>
                </View>

                <TouchableOpacity
                  testID='updateMobile'
                  accessibilityLabel='updateMobile'
                  disabled={!newPhoneNumber}
                  style={[Styles.button1, {
                    backgroundColor: newPhoneNumber ? Colors.bloodOrange : Colors.lightGreyX
                  }]}
                  onPress={() => this.validatePhoneAgain()}
                >
                  <Text style={Styles.button1Text}>Update Mobile</Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>
        <ModalSuccessOne
          testID='doneMobile'
          accessibilityLabel='doneMobile'
          showModal={this.state.showModalSuccess}
          message={`You’re successfully update ${'\n'} your phone number.`}
          onPress={() => this.close()}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.login.user,
    registration: state.register,
    customerAccount: state.customerAccount,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    validateIdentity: (identity) => dispatch(RegistrationActions.validateIdentity(identity)),
    changePhoneNumberValidate: (phone, otp, countryCode) => dispatch(CustomerAccountActions.changePhoneNumberValidate(phone, otp, countryCode)),
    resetChangeEmailAndPhone: () => dispatch(CustomerAccountActions.resetChangeEmailAndPhone()),
  }
}

export default WithSafeAreaView(connect(mapStateToProps, mapDispatchToProps)(ChangePhoneNumberScreen))
