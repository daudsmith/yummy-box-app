import * as React from 'react'
import { Button, Icon, Container, Content } from 'native-base'
import { Linking, Platform, Text, TouchableOpacity, View, Image } from 'react-native'
import { connect } from 'react-redux'
import { scale, verticalScale } from 'react-native-size-matters'
import NavigationBar from '../Components/NavigationBar'
import YummyboxIcon from '../Components/YummyboxIcon'
import { Colors, Images } from '../Themes'
import ListButton from './AccountScreen/Components/ListButton'
import Styles from './Styles/ContactUsStyles'
import { SettingDataInterface } from '../Redux/SettingRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'

export interface ContactUsProps {
  navigation?: NavigationDrawerProp,
  setting?: SettingDataInterface,
  unreadNotification?: number,
  dismissModal?: () => void,
}

const ContactUs: React.FC<ContactUsProps> = (props) => {
  const {
    setting,
    unreadNotification,
    navigation,
    dismissModal,
  } = props
  const {
    cs_phone_number,
    cs_whatsapp_number,
    cs_email_address,
  } = setting
  const navigationBar = typeof dismissModal === 'undefined'

  const callCS = () => {
    let url = `tel:${cs_phone_number}`
    if (Platform.OS === 'ios') {
      url = `telprompt:${cs_phone_number}`
    }
    handleOpenLink(url)
  }

  const handleEmail = () => {
    const url = 'mailto:' + cs_email_address
    handleOpenLink(url)
  }

  const crossButton = () => {
    return (
      <Button transparent onPress={dismissModal}>
        <Icon
          name='md-close'
          style={{color: Colors.bloodOrange}}
        />
      </Button>
    )
  }

  const phoneNumberFormatted = () => {
    let phoneNumber = ''
    if (!cs_phone_number) {
      return phoneNumber
    }
    phoneNumber = cs_phone_number.replace('+6221', '+6221-')
    return phoneNumber
  }

  const waNumberFormatted = () => {
    let phoneNumber = ''
    let waSetting: any = {}
    if (cs_whatsapp_number) {
      waSetting = JSON.parse(cs_whatsapp_number)
      if (waSetting) {
        phoneNumber = waSetting.number.replace('628', '+628')
      }
    }
    return phoneNumber
  }

  const waDescriptionFormatted = () => {
    let description = ''
    let waSetting: any = {}
    if (cs_whatsapp_number) {
      waSetting = JSON.parse(cs_whatsapp_number)
      if (waSetting) {
        if (waSetting.description) {
          description = waSetting.description
        }
      }
    }
    return description
  }

  const waLinkFormatted = () => {
    let waLink = ''
    let waSetting: any = {}
    if (cs_whatsapp_number) {
      waSetting = JSON.parse(cs_whatsapp_number)
      if (waSetting) {
        waLink = waSetting.number.replace('62', 'https://wa.me/62')
      }
    }
    return waLink
  }

  const handleOpenLink = (url: string) => {
    Linking.canOpenURL(url)
      .then((supported) => {
        if (supported) {
          return Linking.openURL(url)
          .catch()
        }
      })
      .catch()
  }

  const getInAppLink = (key) => {
    if (setting.hasOwnProperty(key)) {
      return setting[key]
    }
    return 'https://www.yummybox.id'
  }

  const openHamburger = () => {
    navigation.toggleDrawer()
  }

  return (
    <Container>
      { navigationBar
        ? (
          <NavigationBar
            leftSide='menu'
            title='Help & Contact Us'
            openHamburger={openHamburger}
            unread={unreadNotification}
          />
        )
        : (
          <NavigationBar
            leftSide={crossButton}
            title='Help & Contact Us'
          />
        )
      }
      <Content
        style={{
          marginHorizontal: scale(20),
          marginVertical: verticalScale(12),
        }}
      >
        <Text style={Styles.getInTouch}>Get In Touch</Text>

        {/* PHONE BUTTON */}
        <View style={Styles.phoneInformationContainer}>
          <YummyboxIcon
            name='phone'
            size={scale(18)}
            color={Colors.warmGrey}
          />
          <TouchableOpacity
            onPress={callCS}
            style={Styles.button}
          >
            <Text style={Styles.phoneText}>
              {phoneNumberFormatted()}
            </Text>
          </TouchableOpacity>
        </View>

        {/* PHONE BUTTON */}
        <View style={Styles.phoneInformationContainer}>
          <Image
            source={Images.waIcon}
            style={Styles.waIcon}
            resizeMode='contain'
          />
          <TouchableOpacity
            onPress={() => handleOpenLink(waLinkFormatted())}
            style={Styles.button}
          >
            <Text style={Styles.phoneText}>{waNumberFormatted()}</Text>
            <Text style={Styles.waText}>{waDescriptionFormatted()}</Text>
          </TouchableOpacity>
        </View>

        {/* TEXTINPUT FOR SEND EMAIL */}
        <View style={Styles.sendEmailContainer}>
          <YummyboxIcon
            name='email'
            size={scale(18)}
            color={Colors.warmGrey}
          />
          <TouchableOpacity
            onPress={handleEmail}
            style={Styles.button}
          >
            <Text style={Styles.phoneText}>{cs_email_address}</Text>
          </TouchableOpacity>
        </View>

        <View style={Styles.textBoxContainer}>
          <Text style={Styles.textBoxStyle}>
            Working Hours: Monday to Friday, 08:00 - 19:00. For faster resolution about your order, please make sure that you provide us with the Order/Delivery number.
          </Text>
        </View>

        <View>
          <Text style={Styles.supportText}>Support</Text>
        </View>

        <ListButton
          text={'FAQ'}
          navigateToScreen={() => handleOpenLink(getInAppLink('app_link_faq'))}
          iconName={Images.iconFaq}
          iconType={'image'}
          navigation={navigation}
        />

        <ListButton
          text={'Privacy Policy'}
          navigateToScreen={() => handleOpenLink(getInAppLink('app_link_privacy_policy'))}
          iconName={Images.iconPrivacy}
          iconType={'image'}
          navigation={navigation}
        />

        <ListButton
          text={'Terms & Conditions'}
          navigateToScreen={() => handleOpenLink(getInAppLink('app_link_terms_and_conditions'))}
          iconName={Images.iconTnc}
          iconType={'image'}
          navigation={navigation}
        />
      </Content>
    </Container>
  )
}

const mapStateToProps = (state) => {
  return {
    setting: state.setting.data,
    unreadNotification: state.customerNotification.unread,
  }
}

export default connect(mapStateToProps)(ContactUs)
