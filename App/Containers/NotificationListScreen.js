import Moment from 'moment'
import { Button } from 'native-base'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ActivityIndicator, Linking, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { SwipeListView } from 'react-native-swipe-list-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux'
import AlertBox from '../Components/AlertBox'
import PopupBox from '../Components/Common/PopupBox'
import NavigationBar from '../Components/NavigationBar'
import OfflineModal from '../Components/OfflineModal'
import OfflinePage from '../Components/OfflinePage'
import WithSafeAreaView from '../Components/Common/WithSafeAreaView'
import CustomerNotificationActions from '../Redux/CustomerNotificationRedux'
import StartupActions from '../Redux/StartupRedux'
import OrderDeliveryHelper from '../Services/OrderDeliveryHelper'
import OrderStatusHelper from '../Services/OrderStatusHelper'
import { Colors } from '../Themes'
import Styles from './Styles/NotificationListScreenStyle'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'


class NotificationListScreen extends Component {
  navigationListener
  init = true

  constructor(props) {
    super(props)
    this.state = {
      showBlankOfflinePage: !props.isConnected && !props.notificationList.length,
      showDetailModal: false,
      showDeleteAllNotificationsModal: false,
      selectedNotification: null,
      popUpTitle: '',
      popUpContent: '',
      popUpContentButton: null
    }
  }

  fetchData = () => {
    const {
      isConnected,
      notificationList,
      fetchNotificationList,
    } = this.props
    if (isConnected) {
      if (notificationList.length) {
        fetchNotificationList(true)
      } else {
        fetchNotificationList()
      }
    }
    this.init = false
  }

  componentDidMount() {
    this.navigationListener = this.props.navigation.addListener('didFocus', () => {
      this.props.fetchNotificationList(true)
    })
    if (this.init) {
      this.fetchData()
    }
  }

  componentWillUnmount() {
    this.navigationListener.remove()
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (!this.props.isConnected
      && newProps.isConnected
      && (!newProps.fetchingNotificationList && newProps.notificationListError)
    ) {
      if (!this.props.hasFetchedStartupData) {
        this.props.startupOnNotificationList()
        this.setState({ showBlankOfflinePage: false })
      } else {
        this.props.fetchNotificationList()
      }
    }
  }

  readThisNotif(data) {
    (data.read)
      ? this.props.fetchingNotificationGetUnreadById(data.id)
      : this.props.fetchingNotificationGetReadById(data.id)
  }

  onRowDidOpen(rowKey, rowMap) {
    setTimeout(() => {
      this.closeRow(rowMap, rowKey)
    }, 1500)
  }

  closeRow(rowMap, rowKey) {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow()
    }
  }

  getNotificationTime(notificationTime) {
    if (Moment().isSame(notificationTime, 'date')) {
      return Moment(notificationTime).fromNow()
    } else {
      return Moment(notificationTime).format('ddd, DD MMM YYYY HH:mm')
    }
  }

  toggleDetailModal(notification, type) {
    const { showDetailModal } = this.state
    const parsedContent = notification !== null ? JSON.parse(notification.content) : null
    const invoiceURL = notification !== null ? notification.invoice_url : null
    const popUpContent = this.getPopUpContent(parsedContent, type)
    const popUpContentButton = this.getPopupContentButton(parsedContent, invoiceURL)

    this.setState({
      selectedNotification: notification,
      popUpContent: popUpContent.body,
      popUpTitle: popUpContent.title,
      popUpContentButton
    }, () =>
      this.setState({
        showDetailModal: !showDetailModal
      })
    )
  }

  getPopupContentButton(content, invoiceURL) {
    if (content !== null && content.status === 'PENDING_PAYMENT') {
      return {
        text: 'Instructions',
        onPress: () => {
          Linking.canOpenURL(invoiceURL).then(supported => {
            if (supported) {
              Linking.openURL(invoiceURL)
            }
          })
        }
      }
    }
    return null
  }

  getPopUpContent(content, type) {
    if (content === null) {
      return { title: '', body: '' }
    }
    switch (type) {
      case 'order':
        return this._orderPopUpContent(content)
      case 'delivery':
        return this._orderPopUpContent(content)
      case 'refund':
        return {
          title: 'Yummycredit Refund Successful.',
          body: `The amount of Rp ${content.amount} has been added to your Yummycredit balance`
        }
      case 'topup-by-cs':
        return {
          title: 'Yummycredit Top Up Successful.',
          body: `The amount of Rp ${content.amount} has been added to your Yummycredit balance`
        }
      case 'cashback':
        return {
          title: 'Congratulations! You\'ve earned some cashback.',
          body: `The amount of Rp ${content.amount} has been added to your Yummycredit balance`
        }
      case 'topup':
        return {
          title: 'Yummycredit Top Up Successful.',
          body: `The amount of Rp ${content.amount} has been added to your Yummycredit balance`
        }
      case 'reminder':
        return {
          title: content.title.replace('{', '').replace('}', ''),
          body: content.body.replace('{', '').replace('}', ''),
        }
      default:
        return { title: '', body: '' }
    }
  }

  _orderPopUpContent(content) {
    let popUpContent = {
      title: '',
      body: null
    }
    switch (content.status) {
      case 'PROCESSING':
        popUpContent = {
          title: `Your Order ${content.number} is Confirmed!`,
          body: 'Thank you for your Order! Our chefs will start preparing your meal(s), and we\'ll notify you once the Deliveries are On The Way!'
        }
        break
      case 'on the way':
        popUpContent = {
          title: `Your ${content.meal_tag || 'Lunch'} Delivery ${content.number} is On The Way!`,
          body: `Your ${content.meal_tag || 'Lunch'} is arriving soon! You will receive your Meal by ${content.time || '12:00'}. Have a Yummy Meal!`
        }
        break
      case 'delivered':
        popUpContent = {
          title: `Your Delivery ${'\n'} ${content.number} ${'\n'} is Complete.`,
          body: 'We hope you enjoy your Yummybox Meal! If you have any questions, don\'t hesitate to contact us.'
        }
        break
      case 'PENDING_PAYMENT':
        popUpContent = {
          title: 'COMPLETE YOUR PAYMENT',
          body: () => this.pendingPaymentContent(content)
        }
        break
      case 'REFUNDED':
        popUpContent = {
          title: `Your Order ${content.number} has been cancelled`,
          body: `We have credited the amount of ${content.amount} back to your YumCredits balance. To use it, choose YumCredits as your payment method on checkout page`
        }
        break
    }

    return popUpContent
  }

  pendingPaymentContent(content) {
    return (
      <Text style={Styles.contentText}>
        Your order <Text style={{ color: Colors.bloodOrangeTwo }}>{content.number}</Text> has been <Text
          style={{ color: Colors.bloodOrangeTwo }}>placed,</Text> please complete your bank transfer payment. You can follow
  the instructions by clicking this button below.
      </Text>
    )
  }

  closeModal() {
    this.toggleDetailModal(null)
  }

  renderRightNavigation() {
    return (
      <Button
        testID="deleteAll" 
        accessibilityLabel="deleteAll"
        transparent 
        onPress={() => this.setState({ showDeleteAllNotificationsModal: true })}>
        <Ionicons name='ios-trash' size={28} style={{ color: Colors.bloodOrange, marginLeft: 0 }} />
      </Button>
    )
  }

  _renderLoading() {
    return (
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <ActivityIndicator size={'large'} />
      </View>
    )
  }

  _renderEmpty() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{
          fontSize: 16,
          fontFamily: 'Rubik-Light',
          color: Colors.brownishGrey
        }}>
          You don't have any notifications
        </Text>
      </View>
    )
  }

  _renderNotificationList() {
    const { notificationList } = this.props
    let dataWithKey = Array(notificationList.length).fill('').map((_, i) => ({ key: `${i}`, data: notificationList[i] }))
    return (
      <SwipeListView
        useFlatList
        data={dataWithKey}
        closeOnRowPress
        style={{ flexGrow: 1, backgroundColor: 'white' }}
        renderItem={(data, _) => this._renderNotificationItem(data.item.data, data.item.data.type)}
        renderHiddenItem={(data, rowMap) => {
          return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <TouchableOpacity
                onPress={() => {
                  this.readThisNotif(data.item.data)
                  this.closeRow(rowMap, data.item.key)
                }}
                style={Styles.swipeButton}
              >
                <MaterialIcons name={'markunread'} size={28} color={'#fff'} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.deleteSingleNotification(data.item.data.id)
                  this.closeRow(rowMap, data.item.key)
                }}
                style={[
                  Styles.swipeButton,
                  {
                    alignItems: 'flex-end',
                    backgroundColor: 'red',
                    paddingRight: 20
                  }
                ]}
              >
                <Ionicons name='ios-trash' size={28} color='#fff' />
              </TouchableOpacity>
            </View>
          )
        }}
        leftOpenValue={55}
        rightOpenValue={-55}
        onRowDidOpen={(rowKey, rowMap) => this.onRowDidOpen(rowKey, rowMap)}
      />
    )
  }

  _renderNotificationItem(notification, type) {
    const content = JSON.parse(notification.content)
    let notificationItem = null
    const regExp = /\{([^)]+)\}/
    let matches, highlight, leftPart, rightPart
    switch (type) {
      case 'order':
        notificationItem = this._orderItemList(notification, content)
        break
      case 'refund':
        notificationItem = (
          <Text
            style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
            Yummycredit Refund Successful.
          </Text>
        )
        break
      case 'topup-by-cs':
        notificationItem = (
          <Text
            style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
            Yummycredit Top Up Successful.
          </Text>
        )
        break
      case 'cashback':
        notificationItem = (
          <Text
            style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
            Congratulations! You've earned some cashback.
          </Text>
        )
        break
      case 'topup':
        notificationItem = (
          <Text
            style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
            Yummycredit Top Up Successful.
          </Text>
        )
        break
      case 'reminder':
        matches = regExp.exec(content.title)
        highlight = (<Text
          style={{ color: Colors.bloodOrange }}
        >
          {matches[1]}
        </Text>)
        leftPart = content.title.split('{')
        rightPart = content.title.split('}')

        notificationItem = (
          <Text
            style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
            {leftPart[0]}{highlight}{rightPart[1]}
          </Text>
        )
        break
      default:
        notificationItem = (
          <Text
            style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
            Your delivery <Text
              style={{ color: Colors.bloodOrange }}>#{content.number.replace('KDS-DELV-', '')}</Text> {OrderDeliveryHelper.parseStatusToMessage(content.status)}
            <Text style={{ color: Colors.bloodOrange }}>{content.status}</Text>
          </Text>
        )
        break
    }

    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.toggleDetailModal(notification, type)
          !notification.read && this.readThisNotif(notification)
        }}
        testID={'selectNotif'+notification.id}
        accessibilityLabel={'selectNotif'+notification.id}
      >
        <View key={notification.id} style={Styles.notificationItem}>
          <View style={{ flex: 1, marginLeft: 5 }}>
            <Ionicons name={'ios-notifications-outline'} size={28} color={Colors.greyishBrown} />
            {!notification.read && <View style={{
              width: 8,
              height: 8,
              backgroundColor: Colors.bloodOrange,
              position: 'absolute',
              borderRadius: 4,
              right: 6,
              top: 4
            }} />}
          </View>
          <View style={{ flex: 12, paddingLeft: 5 }}>
            {notificationItem}
            <Text style={Styles.notificationTimeText}>{this.getNotificationTime(notification.created_at)}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  _orderItemList(notification, content) {
    let notificationItem = (
      <Text
        style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
        Your order <Text style={{ color: Colors.bloodOrange }}>#{content.number.replace('KDS-INV-', '')} </Text>
        <Text
          style={{ color: Colors.bloodOrange }}>
          {OrderStatusHelper.parseForNotification(content.status)}
        </Text>
      </Text>
    )

    if (content.status === 'REFUNDED') {
      notificationItem = (
        <Text
          style={[Styles.notificationItemText, !notification.read && Styles.notificationItemTextUnread]}>
          Your order <Text style={{ color: Colors.bloodOrange }}>#{content.number.replace('KDS-INV-', '')}</Text> has been
          cancelled and {content.amount} has been added to your YumCredits balance
        </Text>
      )
    }

    return notificationItem
  }

  _openHamburger = () => {
    const { isConnected, fetchWallet, fetchUnreadNotifications, navigation } = this.props
    if (isConnected) {
      fetchWallet()
      fetchUnreadNotifications()
    }
    navigation.toggleDrawer()
  }

  render() {
    const { showBlankOfflinePage, showDetailModal } = this.state
    const { fetchingNotificationList, notificationList } = this.props
    let body = null
    if (!this.props.isConnected && showBlankOfflinePage) {
      body = <OfflinePage />
    } else {
      if (fetchingNotificationList) {
        body = this._renderLoading()
      } else if (notificationList.length > 0 && !fetchingNotificationList) {
        body = this._renderNotificationList()
      } else {
        body = this._renderEmpty()
      }
    }
    const popUpBottomButtons = [
      {
        text: 'Delete',
        onPress: () => {
          this.closeModal()
          this.props.deleteSingleNotification(this.state.selectedNotification.id)
        },
        textColor: 'red',
        testProp: 'deleteSelectNotif'
      },
      { 
        text: 'Close', 
        onPress: () => this.closeModal(),
        testProp: 'closeSelectNotif'
      }
    ]

    return (
      <View style={{ backgroundColor: '#fff', flex: 1 }}>
        <NavigationBar
          leftSide='menu'
          title='Notifications'
          rightSide={() => this.renderRightNavigation()}
          openHamburger={this._openHamburger}
          unread={this.props.unreadNotification}
        />
        <View style={{ flex: 1 }}>
          {body}
        </View>
        <PopupBox
          title={this.state.popUpTitle}
          content={this.state.popUpContent}
          contentButton={this.state.popUpContentButton}
          visible={showDetailModal}
          bottomButtons={popUpBottomButtons}
        />
        <AlertBox
          primaryAction={() => this.props.deleteAllNotifications()}
          primaryActionText='Yes'
          primaryActionTestProp='okDeleteAll'
          secondaryAction={() => { }}
          secondaryActionText='No'
          secondaryActionTestProp='noDeleteAll'
          dismiss={() => this.setState({ showDeleteAllNotificationsModal: false })}
          visible={this.state.showDeleteAllNotificationsModal}
          text='Are you sure you want to delete all notifications?'
        />
        {!showBlankOfflinePage && <OfflineModal isConnected={this.props.isConnected} />}
      </View>
    )
  }
}

NotificationListScreen.propTypes = {
  notificationList: PropTypes.array.isRequired,
  fetchingNotificationList: PropTypes.bool.isRequired,
  notificationListError: PropTypes.string.isRequired,
  fetchNotificationList: PropTypes.func.isRequired,
  markNotificationsToRead: PropTypes.func.isRequired,
  fetchingNotificationGetReadById: PropTypes.func.isRequired,
  fetchingNotificationGetUnreadById: PropTypes.func.isRequired,
  deleteSingleNotification: PropTypes.func.isRequired,
  deleteAllNotifications: PropTypes.func.isRequired,
  startupOnNotificationList: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => {
  return {
    isConnected: state.network.isConnected,
    hasFetchedStartupData: state.startup.hasFetchedStartupData,
    notificationList: state.customerNotification.notificationList,
    fetchingNotificationList: state.customerNotification.fetchingNotificationList,
    notificationListError: state.customerNotification.notificationListError,
    unreadNotification: state.customerNotification.unread,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchNotificationList: (silent) => dispatch(CustomerNotificationActions.fetchNotificationList(silent)),
    markNotificationsToRead: () => dispatch(CustomerNotificationActions.markNotificationsToRead()),
    fetchingNotificationGetReadById: (notificationId) => dispatch(CustomerNotificationActions.fetchingNotificationGetReadById(notificationId)),
    fetchingNotificationGetUnreadById: (notificationId) => dispatch(CustomerNotificationActions.fetchingNotificationGetUnreadById(notificationId)),
    deleteSingleNotification: (notificationId) => dispatch(CustomerNotificationActions.deleteSingleNotification(notificationId)),
    deleteAllNotifications: () => dispatch(CustomerNotificationActions.deleteAllNotifications()),
    startupOnNotificationList: () => dispatch(StartupActions.startupOnNotificationList()),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchUnreadNotifications: () => dispatch(CustomerNotificationActions.fetchTotalUnreadNotifications()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithSafeAreaView(NotificationListScreen))
