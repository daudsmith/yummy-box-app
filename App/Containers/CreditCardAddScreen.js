import { Button, Container, Content, Icon } from 'native-base'
import React, { Component } from 'react'
import { Keyboard, Modal, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import { WebView } from 'react-native-webview'
import { connect } from 'react-redux'
import LoadingModal from '../Components/LoadingModal'
import NavigationBar from '../Components/NavigationBar'
import AppConfig from '../Config/AppConfig'
import { CreditCardInput } from '../Lib/react-native-credit-card-input-fullpage'
import Xendit from '../Lib/Xendit/Xendit'
import CustomerCardActions from '../Redux/CustomerCardRedux'
import Colors from '../Themes/Colors'
import styles from './Styles/CreditCardAddScreenStyle'

const EX_API_KEY = AppConfig.xenditKey

const patchPostMessageFunction = function() {
  window.postMessage = function(data) {
    window['ReactNativeWebView'].postMessage(data)
  }
}

const injectScript = '(' + String(patchPostMessageFunction) + ')();'

const cardTypes = {
  'visa': 'VISA',
  'master-card': 'MASTER',
  'american-express': 'AMEX'
}

class CreditCartAddScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      valid: false,
      validationErrors: {},
      loading: false,
      saving: this.props.saving,
      formData: {
        amount: 10000,
        card_number: null,
        card_exp_month: null,
        card_exp_year: null,
        card_cvn: null,
        is_multiple_use: true,
        should_authenticate: true,
        type: null
      },
      showError: false,
      saveThisCard: true,
      fraudData: {},
      dsWebView: false,
      creditCardToken: null,
      loadAuthForm: false,
      isAuthenticated: false,
      isTokenized: false,
      showButtonLabel: true
    }
    this.webView = null
  }

  componentDidMount () {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.keyboardDidShow())
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardDidHide())
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    this.forceUpdate()

    if(newProps.saving === false) {
      this.handleSuccess()
    }
  }

  componentWillUnmount () {
    this.keyboardDidHideListener.remove()
    this.keyboardDidShowListener.remove()
  }

  keyboardDidShow () {
    this.setState({showButtonLabel: false})
  }

  keyboardDidHide () {
    this.setState({showButtonLabel: true})
  }

  setFormDataState (formData) {
    this.setState({formData, valid: true})
  }

  setWebViewState (webView) {
    this.webView = webView
  }

  getCardType (type) {
    if (cardTypes.hasOwnProperty(type)) {
      return cardTypes[type]
    } else {
      return 'UNKNOWN'
    }
  }
  handleFormChanged (form) {
    const formData = this.state.formData
    if (form.valid) {
      const expiries = form.values.expiry.split('/')
      this.setFormDataState({
        ...formData,
        card_number: form.values.number.replace(/ /g, ''),
        card_exp_month: expiries[0],
        card_exp_year: `20${expiries[1]}`,
        card_cvn: form.values.cvc,
        type: this.getCardType(form.values.type)
      })
    } else {
      this.setState({ valid: false, validationErrors: form.status })
    }
  }
  handleSubmitPress () {
    const {valid, formData} = this.state
    if (valid) {
      this.setState({ loadAuthForm: true, loading: true})
      Xendit.setPublishableKey(EX_API_KEY)
      Xendit.card.createToken(formData, this.xenditResponseHandler.bind(this))
    } else {
      this.setState({ showError: true })
    }
  }

  xenditResponseHandler (err, creditCardToken) {
    if (err) {
      this.setState({ loadAuthForm: false})
      return this.handleError()
    }

    if (creditCardToken.status === 'APPROVED' || creditCardToken.status === 'VERIFIED') {
      const {formData} = this.state
      if (formData.should_authenticate) {
        this.setState({ creditCardToken: creditCardToken })
        Xendit.card.createAuthentication({
          amount: formData.amount,
          token_id: creditCardToken.id
        }, null, this.xenditResponseHandler.bind(this))
      } else {
        this.handleSuccess()
      }
    } else if (creditCardToken.status === 'IN_REVIEW') {
      this.setState({ source: creditCardToken.payer_authentication_url })
    } else if (creditCardToken.status === 'FRAUD') {
      this.handleError()
    } else if (creditCardToken.status === 'FAILED') {
      this.handleError()
    }

    this.setState({ loadAuthForm: false})
    this.setState({ dsWebView: true })
  }

  handleError () {
  }
  handleSuccess () {
    this.setState({loading:false})
    this.props.navigation.goBack()
  }

  onMessage (event) {
    const authData = JSON.parse(event.nativeEvent.data)
    const {creditCardToken} = this.state

    this.setState({ dsWebView: false })
    if (authData.status === 'VERIFIED') {
      this.setState({ loading: true})
      const cardData = {
        token: creditCardToken.id,
        authorization_id: authData.id,
        type: this.state.formData.type,
        ends_with: this.state.formData.card_number.substring(12, 16)
      }

      this.setState({saving: true})
      setTimeout(() => {this.props.addCard(cardData)}, 1500)
    }
  }

  renderValidationErrorMessage () {
  }
  render3dsWebView () {
    const { cardDetailsContainer, modalDetailsContainer, cardDetailsContainer2 } = styles

    return (
      <View style={cardDetailsContainer}>
        <View style={modalDetailsContainer}>
          <Button
            transparent
            onPress={() => this.setState({ dsWebView: false, loading: false })}
          >
            <Icon name='md-close' style={{color: 'black', fontSize: moderateScale(30)}} />
          </Button>
        </View>

        <View style={cardDetailsContainer2}>
          <WebView
            injectedJavaScript={injectScript}
            source={{ uri: this.state.source }}
            ref={(webView) => this.setWebViewState(webView)}
            onMessage={this.onMessage.bind(this)}
          />
        </View>
      </View>
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    return (
      <Container>
        <NavigationBar leftSide='back' title='Add a New Card' leftSideNavigation={this.navigationGoBack} />
        <Content contentContainerStyle={{flexGrow: 1}} style={{backgroundColor: 'white', paddingHorizontal: moderateScale(30), paddingTop: moderateScale(20)}} scrollEnabled={true}>
          <CreditCardInput
            onChange={(form) => this.handleFormChanged(form)}
          />
          <View style={{paddingTop: 10, paddingHorizontal: 0, paddingBottom: 20, borderRadius: 3, marginTop: 5}}>
            <Text style={{color: Colors.brownishGrey, fontFamily: 'Rubik-Light', fontSize: 12}}>Your card issuer might notify you about a transaction amounting Rp 10.000. Don’t worry, this amount is only a pre-authorization and will not actually be charged.</Text>
          </View>
          <View style={{height: 66}}>
            <TouchableOpacity onPress={() => this.handleSubmitPress()} style={{height: 42, backgroundColor: Colors.bloodOrange, borderRadius: 3, alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{fontFamily: 'Rubik-Regular', fontSize: 16, color: 'white'}}>Save Credit Card</Text>
            </TouchableOpacity>
          </View>
        </Content>
        <Modal
            animationType='slide'
            transparent={true}
            visible={this.state.dsWebView}
            onRequestClose={() => this.setState({ dsWebView: false, loading: false }) }
          >
            { this.render3dsWebView() }
          </Modal>
          <LoadingModal
              visible={this.state.loadAuthForm}
              modalText="Authenticating"
          />
          <LoadingModal
              visible={this.state.saving}
              modalText="Saving your card"
          />
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.login.user,
    saving: state.customerCard.loading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addCard: (card) => dispatch(CustomerCardActions.addCard(card))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreditCartAddScreen)
