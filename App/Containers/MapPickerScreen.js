import React, { Component } from 'react'
import { Container, Content } from 'native-base'
import { BackHandler, Dimensions, Keyboard, Platform, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import RNGooglePlaces from 'react-native-google-places'
import Spinner from 'react-native-loading-spinner-overlay'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import { moderateScale } from 'react-native-size-matters'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import AlertBox from '../Components/AlertBox'
import NavigationBar from '../Components/NavigationBar'
import YummyboxIcon from '../Components/YummyboxIcon'
import AddressHistoryActions from '../Redux/AddressHistoryRedux'
import Geocoder from '../Services/GeocodingService'
import Colors from '../Themes/Colors'
import Styles from './Styles/MapPickerScreenStyle'

const {width, height} = Dimensions.get('window')
const DEFAULT_LATITUDE = -6.117664      // Jakarta Latitude
const DEFAULT_LONGITUDE = 106.906349    // Jakarta Longitude

class MapPickerScreen extends Component {
  constructor (props) {
    super(props)
    const {coordinate} = this.props.navigation.state.params
    this.state = {
      initialRegion: {
        latitude: coordinate.latitude || DEFAULT_LATITUDE,    // Avoid <<nan>> latitude
        longitude: coordinate.longitude || DEFAULT_LONGITUDE, // Avoid <<nan>> longitude
        latitudeDelta: 0.005,
        longitudeDelta: 0.005
      },
      region: {
        latitude: coordinate.latitude || DEFAULT_LATITUDE,    // Avoid <<nan>> latitude
        longitude: coordinate.longitude || DEFAULT_LONGITUDE, // Avoid <<nan>> longitude
        latitudeDelta: 0.005,
        longitudeDelta: 0.005
      },
      selectedAddress: null,
      isSelectDestinationByAddress: false,
      setAddressToInputValue: false,
      selectAddressSetRegionOnProcess: false,
      addressInputValue: '',
      mapViewHeight: height,
      showClearText: false,
      predictionList: [],
      historyList: [],
      showList: false,
      showHistory: false,
      showOutsideCoverageAreaModal: false,
      showSpinner: false
    }
    this.inputRefs = React.createRef()
  }

  componentDidMount () {
    this.props.fetchAddressHistory()
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.goBack()
      return true
    })
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    const {addresses} = newProps.addressHistory
    this.setState({historyList: addresses})
  }

  componentWillUnmount () {
    Keyboard.dismiss()
    this.backHandler.remove()
  }

  goBack() {
    if (typeof this.props.navigation.state.params.navigationPop === 'number') {
      return this.props.navigation.pop(this.props.navigation.state.params.navigationPop)
    }
    return this.props.navigation.goBack()
  }

  setDestination () {
    const {isSelectDestinationByAddress, region} = this.state

    if (!isSelectDestinationByAddress) {
      this.setDestinationByCoordinate(region.longitude, region.latitude)
    } else {
      this.setDestinationByPlace()
    }
  }

  setDestinationFromHistory (addressHistory) {
    this.props.navigation.state.params.setDeliveryAddress(addressHistory)
    this.props.navigation.pop(2)
  }

  setDestinationByPlace () {
    const {selectedAddress} = this.state

    const place = {
      address: `${selectedAddress.name}, ${selectedAddress.address}`,
      longitude: selectedAddress.longitude,
      latitude: selectedAddress.latitude
    }
    this.setDestinationForDeliveryAddress(place)
  }

  setDestinationByCoordinate (longitude, latitude) {
    Geocoder.getFromLatLng(latitude, longitude)
    .then((response) => {
      const {results} = response
      const place = {
        address: results[0].formatted_address,
        latitude: results[0].geometry.location.lat,
        longitude: results[0].geometry.location.lng
      }
      this.setDestinationForDeliveryAddress(place)
    })
  }

  checkInputAddressCoverage (place) {
    // Add more spesific area based on city location
    let spesificCoverageAreaTangerang = ['bsd', 'tang sel', 'tangsel', 'tangerang selatan', 'south tangerang']
    let spesificCoverageAreaJakarta = ['jakarta']
    let allCoverageArea = [...spesificCoverageAreaTangerang, ...spesificCoverageAreaJakarta]

    for (let index = 0; index <= allCoverageArea.length; index++) {
      if (place.address.toLowerCase().indexOf(allCoverageArea[index]) > -1) {
        return true
      }
    }

    return false
  }

  setDestinationForDeliveryAddress (place) {
    let isValidSelectedAddress = this.checkInputAddressCoverage(place)
    if (isValidSelectedAddress) {
      this.props.navigation.state.params.setDeliveryAddress(place)
      this.goBack()
    } else {
      this.setState({showOutsideCoverageAreaModal: true})
    }
  }

  changeCoordinate (longitude, latitude) {
    const {region, isSelectDestinationByAddress, setAddressToInputValue} = this.state
    const newRegion = {...region, latitude, longitude}
    if (!isSelectDestinationByAddress) {
      if (setAddressToInputValue) {
        Geocoder.getFromLatLng(latitude, longitude)
        .then((response) => this.setState({addressInputValue: response.results[0].formatted_address}))
      } else {
        this.setState({setAddressToInputValue: true})
      }
    }

    this.setState({region: newRegion, predictionList: []})
  }

  getPlacePredictionsBasedOnText (address) {
    const {historyList} = this.state

    if (address !== '') {
      RNGooglePlaces.getAutocompletePredictions(address, {country: 'ID', useSessionToken: true})
        .then((places) => {
          this.setState({predictionList: places, showClearText: true, showList: true, showHistory: false})
        })
    } else {
      this.setState({predictionList: [], showClearText: false, showList: false, showHistory: historyList.length > 0})
    }
    this.setState({addressInputValue: address})
  }

  onAddressPress (address) {
    RNGooglePlaces.lookUpPlaceByID(address.placeID, ['location', 'name', 'address'])
      .then((place) => this.setSelectedAddress(place, address))
      .catch()
  }

  setSelectedAddress (place, address) {
    const currentRegion = this.state.region
    const {longitude, latitude} = place.location
    const region = {...currentRegion, longitude, latitude}
    const custPlace = {...place, longitude, latitude}
    this.inputRefs['search'].blur()
    Keyboard.dismiss()
    this.setState({selectedAddress: custPlace, region, addressInputValue: address.fullText, predictionList: [], showHistory: false, showList: false, isSelectDestinationByAddress: true, selectAddressSetRegionOnProcess: true})
    this.inputRefs['map'].animateToRegion(region, 1)
  }

  onMapPress () {
    Keyboard.dismiss()
    this.setState({showHistory: false, showList: false})
  }

  onSearchBoxFocus () {
    const {addressInputValue, historyList} = this.state

    if (addressInputValue === '' && historyList.length > 0) {
      this.setState({showHistory: true})
    }

    this.setState({showClearText: true})
  }

  onClearText () {
    this.setState({showClearText: false, addressInputValue: '', predictionList: [], showHistory: true, showList: false})
  }

  renderPin () {
    const setDestinationButtonWidth = 175
    const setDestinationButtonHeight = 40
    const top = (0.5 * height) - (setDestinationButtonHeight + 10)
    const left = (0.5 * width) - (setDestinationButtonWidth / 2) - 10

    return (
      <View style={{position: 'absolute', top: top, left: left, alignItems: 'center'}}>
        <TouchableOpacity testID='setDestinationButton' onPress={() => this.setDestination()} style={[Styles.setDestinationButton, {height: moderateScale(setDestinationButtonHeight), width: moderateScale(setDestinationButtonWidth)}]}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={Styles.setDestinationButtonText}>SET DESTINATION</Text>
            <Ionicons name='ios-arrow-forward' size={moderateScale(25)} color={Colors.white} style={{paddingTop: moderateScale(2), marginLeft: moderateScale(10)}} />
          </View>
        </TouchableOpacity>
        <Ionicons name='ios-pin' color={Colors.bloodOrange} size={46} style={{backgroundColor: 'transparent'}} />
      </View>
    )
  }

  renderAddressList () {
    const {predictionList} = this.state

    return (
      <View style={Styles.resultListContainer}>
        <ScrollView contentContainerStyle={{backgroundColor: 'white'}} keyboardShouldPersistTaps='handled'>
          {predictionList.map((address, index) => {
            return (
              <TouchableOpacity testID={`addressList_${index}`} style={Styles.resultListRow} key={index} onPress={() => this.onAddressPress(address)}>
                <View style={Styles.resultListIconContainer}>
                  <Ionicons name='ios-pin' style={{fontSize: moderateScale(24), color: Colors.greyishBrownTwo}} />
                </View>
                <View style={Styles.resultListTextContainer}>
                  <Text style={Styles.resultListText}>{address.fullText}</Text>
                </View>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    )
  }

  renderHistoryList () {
    const {historyList} = this.state

    return (
      <View style={Styles.resultListContainer}>
        <ScrollView contentContainerStyle={{flexGrow: 1, backgroundColor: 'white'}} keyboardShouldPersistTaps='handled'>
          <View style={Styles.historyListHeaderTextView}>
            <Text style={Styles.historyListHeaderText}>History</Text>
          </View>
          {historyList.map((history, index) => {
            return (
              <TouchableOpacity style={Styles.resultListRow} onPress={() => this.setDestinationFromHistory(history)} key={index} >
                <View style={Styles.resultListIconContainer}>
                  <Ionicons name='ios-time-outline' style={{fontSize: moderateScale(24), color: Colors.greyishBrownTwo}} />
                </View>
                <View style={Styles.resultListTextContainer}>
                  <Text style={Styles.resultListText} numberOfLines={3}>
                    {history.address}
                  </Text>
                </View>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    )
  }

  renderPlacePicker () {
    const {showClearText, addressInputValue, showHistory, showList} = this.state
    return (
      <View style={Styles.searchBoxContainer}>
        <View style={Styles.searchBox}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Ionicons name='ios-pin' color={Colors.bloodOrange} size={23} style={{backgroundColor: 'transparent'}} />
          </View>
          <View style={{flex: 10}}>
            <TextInput
              testID='addressField'
              ref='search'
              value={addressInputValue}
              placeholder='Search Address'
              placeholderTextColor={Colors.pinkishGrey}
              underlineColorAndroid='transparent'
              onFocus={() => this.onSearchBoxFocus()}
              onChangeText={(text) => this.getPlacePredictionsBasedOnText(text)}
              style={Styles.searchBoxTextInput}
              autoCapitalize='none'
              autoCorrect={false}
            />
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            {showClearText &&
              <TouchableWithoutFeedback onPress={() => this.onClearText()} style={{paddingLeft: 2}}>
                <View>
                  <Ionicons name='ios-close' size={30} color={Colors.pinkishGrey} />
                </View>
              </TouchableWithoutFeedback>
            }
          </View>
        </View>
        <View style={Styles.resultContainer}>
          {showList && this.renderAddressList()}
          {showHistory && this.renderHistoryList()}
        </View>
      </View>
    )
  }

  navigationGoBack = (navigationPop) => {
    this.props.navigation.pop(navigationPop)
  }

  render () {
    const {initialRegion, showSpinner} = this.state
    const { addressType: {
      companyAddresses,
      loading,
      error
    } } = this.props
    let navigationPop = 1
    if (!loading) {
      if((companyAddresses && companyAddresses.length === 0) || error) {
        navigationPop = 2
      }
    }

    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title='Delivery Address'
          leftSideNavigation={this.navigationGoBack(navigationPop)}
        />
        <Content>
          <Spinner visible={showSpinner} textStyle={{color: '#FFF'}} />
          <View style={[StyleSheet.absoluteFillObject]}>
            {
              initialRegion !== null &&
              <MapView
                ref='map'
                provider={PROVIDER_GOOGLE}
                style={[StyleSheet.absoluteFillObject, {top: 65}]}
                initialRegion={initialRegion}
                showsUserLocation
                showsMyLocationButton
                onPress={() => this.onMapPress()}
                onRegionChange={() => {
                  if (this.state.isSelectDestinationByAddress && !this.state.selectAddressSetRegionOnProcess) {
                    this.setState({isSelectDestinationByAddress: false})
                  }
                }}
                onRegionChangeComplete={(region) => {
                  if (this.state.selectAddressSetRegionOnProcess) {
                    this.setState({selectAddressSetRegionOnProcess: false})
                  } else {
                    this.changeCoordinate(region.longitude, region.latitude)
                  }
                }}
              >
              </MapView>
            }

            {this.renderPin()}
            {Platform.OS === 'android' &&
              <TouchableOpacity onPress={() => this.inputRefs['map'].animateToRegion(this.state.initialRegion)} style={Styles.animateToCurrentLocationButton}>
                <YummyboxIcon name='location-gmaps' size={30} color='black' />
              </TouchableOpacity>
            }
          </View>

            {this.renderPlacePicker()}
            <AlertBox
              primaryAction={() => {}}
              primaryActionText='OK'
              dismiss={() => this.setState({showOutsideCoverageAreaModal: false})}
              text='Sorry! Your address is out of our delivery area. We currently serve deliveries within Jakarta & South Tangerang only.'
              visible={this.state.showOutsideCoverageAreaModal}
            />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    addressHistory: state.addressHistory,
    addressType: state.addressType,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAddressHistory: () => dispatch(AddressHistoryActions.fetchAddressHistory())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapPickerScreen)
