import React from 'react'
import { ImageBackground, Image, View, Text, StatusBar, Linking } from 'react-native'
import { Images } from '../Themes'
import FlexButton from '../Components/Common/FlexButton'
import styles from './Styles/IntroOneStyle'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { Colors } from '../Themes'
import { AppState } from '../Redux/CreateStore'
import ApiSetting from '../Services/ApiSetting'
import SettingActions, { SettingDataInterface } from '../Redux/SettingRedux'
import moment from 'moment'
import { connect } from 'react-redux'
import UpdateVersionModal from '../Components/UpdateVersionModal'

export interface IntroOneScreenProps {
  navigation: NavigationDrawerProp,
  isConnected: boolean,
  setting: SettingDataInterface,
  fetchedSetting: (settings: any) => void,
}

export interface IntroOneScreenState {
  needUpdate: boolean,
  storeUrl?: string,
  updateMessage: string,
}

class IntroOneScreen extends React.Component<IntroOneScreenProps, IntroOneScreenState> {
  state = {
    needUpdate: false,
    storeUrl: null,
    updateMessage: '',
  }

  componentDidMount() {
    this._checkVersion()
  }

  componentWillUnmount() {
    this.setState({ needUpdate: false })
  }

  _checkVersion = () => {
    // check redux time interval
    let mustCheck = true
    const { version_check } = this.props.setting
    const now = moment()
    if (version_check) {
      const lastCheck = moment(version_check.last_checked)
      if (lastCheck
        .add(version_check.interval_minute, 'minutes')
        .isAfter(now)
      ) {
        mustCheck = false
      }

      if (version_check.on_start_up && version_check.poped) {
        mustCheck = false
      }
    }

    if (mustCheck) {
      ApiSetting.create()
        .checkVersion()
        .then((response) => {
          if (response.data.data.update && response.data.data.mandatory) {
            this.setState({
              needUpdate: true,
              storeUrl: response.data.data.store_url,
              updateMessage: response.data.data.message,
            })
          }

          // save data update to redux persist
          const versionCheck = {
            version_check: {
              ...response.data.data,
              last_checked: moment(),
              poped: true,
            },
          }
          this.props.fetchedSetting(versionCheck)
        })
        .catch(() => {
          this.setState({
            needUpdate: false,
            storeUrl: null,
            updateMessage: '',
          })
        })
    } else {
      if (version_check.on_start_up) {
        // save data update to redux persist
        const versionCheck = {
          version_check: {
            ...version_check,
            poped: true,
          },
        }
        this.props.fetchedSetting(versionCheck)
      }
    }
  }

  goToStore = () => {
    this.setState({ needUpdate: false }, () => {
      Linking.canOpenURL(this.state.storeUrl)
        .then((supported) => {
          if (supported) {
            return Linking.openURL(this.state.storeUrl)
          }
        })
        .catch()
    })
  }

  handlePress = (to: string) => () => {
    const { navigation } = this.props
    if (to === 'login') {
      return navigation.navigate('Login')
    } else if (to === 'guest') {
      return navigation.navigate('IntroTwoScreen', { to: 'guest' })
    }
  }

  render() {
    const { needUpdate, updateMessage } = this.state
    return (
      <>
        <StatusBar translucent backgroundColor='transparent' />
        <View style={styles.container}>
          <ImageBackground source={Images.IntroOneBackground} style={styles.backgroundImage}>
            {
              !needUpdate &&
              <View style={styles.form}>
                <View style={styles.logo}><Image source={Images.Logo} /></View>
                <View>
                  <View>
                    <Text style={styles.text}>Tasty Meals, Delivered Daily</Text>
                    <FlexButton
                      text="Login / Register"
                      onPress={this.handlePress('login')}
                      disableShadow
                      buttonStyle={styles.loginButton}
                      textStyle={{ color: Colors.primaryOrange }}
                      testID="signInButton"
                      accessibilityLabel="signInButton"
                    />
                    <FlexButton
                      text="Continue as a Guest"
                      onPress={this.handlePress('guest')}
                      disableShadow
                      buttonStyle={styles.guestButton}
                      textStyle={{ color: Colors.white }}
                      testID="asGuestButton"
                      accessibilityLabel="asGuestButton"
                    />
                  </View>
                </View>
              </View>
            }
          </ImageBackground>
        </View>
        {needUpdate &&
          <UpdateVersionModal
            open={needUpdate}
            content={updateMessage !== ''
              ? updateMessage
              : null}
            onClose={() => {
              this.setState({
                needUpdate: false,
                updateMessage: '',
              })
            }}
            onSubmit={this.goToStore}
          />
        }
      </>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    setting: state.setting.data,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchedSetting: (settings) => dispatch(SettingActions.settingFetched(settings)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IntroOneScreen)

