import * as React from 'react'
import { Platform } from 'react-native'
import * as Sentry from '@sentry/react-native'
import codePush from 'react-native-code-push'
import { Provider } from 'react-redux'
import '../Config'
import AppConfig from '../Config/AppConfig'
import createStore from '../Redux'
import RootContainer from './RootContainer'
import { PersistGate } from 'redux-persist/integration/react'

// create our store
const {store, persistor} = createStore()

const sentryDsn = AppConfig.SentryDSN

let codePushKey = AppConfig.codePushKey
if (Platform.OS === 'ios') {
  codePushKey = AppConfig.iosCodePushKey
}

Sentry.init({
  dsn: sentryDsn
})
/**
 * Provides an entry point into our application. Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */

class App extends React.Component {
  render () {
    console.disableYellowBox = true
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootContainer />
        </PersistGate>
      </Provider>
    )
  }
}

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.ON_NEXT_RESTART,
  deploymentKey: codePushKey,
}
codePush.getUpdateMetadata().then((update) => {
  if (update) {
    Sentry.setRelease(update.appVersion + '-codepush:' + update.label)
  }
})

export default __DEV__ ? App : codePush(codePushOptions)(App)
