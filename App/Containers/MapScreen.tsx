import React from 'react'
import {
  View,
  StyleSheet,
  Text,
  Platform,
  PermissionsAndroid,
  Image,
  KeyboardAvoidingView,
} from 'react-native'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import GeoService from '../Services/GeocodingService'
import AlertBox from '../Components/AlertBox'
import Colors from '../Themes/Colors'
import Styles from './Styles/MapScreenStyle'
import TextInput from '../Components/Common/TextInput'
import BottomButton from '../Components/BottomButton'
import ApiKitchen from '../Services/V2/ApiKitchen'
import { Images } from '../Themes'
import { scale, verticalScale } from 'react-native-size-matters'
import { Button } from 'native-base'

export interface initialRegionInterface {
  latitude: number,
  longitude: number,
  latitudeDelta: number,
  longitudeDelta: number
}

export interface MapScreenPropsInterface {
  navigation: NavigationDrawerProp,
}

export interface MapScreenStatesInterface {
  initialRegion: initialRegionInterface,
  coordinate: initialRegionInterface,
  selectedAddress: string,
  addressDetail: string,
  showOutsideCoverageAreaModal: boolean,
  mapMargin: number,
}

class MapScreen extends React.Component<MapScreenPropsInterface, MapScreenStatesInterface> {
  map
  willFocus
  willBlur
  mounted = false

  constructor(props: MapScreenPropsInterface) {
    super(props)
    this.state = {
      initialRegion: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      coordinate: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      },
      selectedAddress: '',
      addressDetail: '',
      showOutsideCoverageAreaModal: false,
      mapMargin: 1,
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.navigation.state.params.selectedAddress !== this.props.navigation.state.params.selectedAddress) {
      this.getParam()
    }
  }

  componentDidMount() {
    this.getParam()
    this.checkPermission()
    this.willFocus = this.props
      .navigation
      .addListener(
        'willFocus', () => {
          if (!this.mounted) {
            this.backToFirstLocation()
          }
        }
      )

    this.willBlur = this.props
      .navigation
      .addListener(
        'didBlur',
        () => this.mounted = false,
      )
    this.mounted = true
  }

  componentWillUnmount() {
    if (this.willFocus) {
      this.willFocus.remove()
    }
    if (this.willBlur) {
      this.willBlur.remove()
    }
  }

  checkPermission = () => {
    Platform.OS === 'android' &&
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      )
  }

  setMargin = () => {
    this.setState({ mapMargin: 0 })
  }

  getParam = () => {
    if (this.props.navigation.state.params.selectedAddress) {
      const latitude = this.props.navigation.state.params.selectedAddress.latitude && Number(this.props.navigation.state.params.selectedAddress.latitude)
      const longitude = this.props.navigation.state.params.selectedAddress.longitude && Number(this.props.navigation.state.params.selectedAddress.longitude)
      this.setState({
        initialRegion: {
          latitude,
          longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        },
        coordinate: {
          latitude,
          longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        },
        selectedAddress: this.props.navigation.state.params.selectedAddress.addressDetail,
      })
    }
  }

  checkAddressCoverage = (lat: number, lng: number) => {
    ApiKitchen.create()
      .kitchenList(lat, lng)
      .then(response => {
        const data = response.data.data
        if (data === undefined) {
          this.setState({ showOutsideCoverageAreaModal: true })
        }
      })
  }

  formatSelectedAddress = (selectedAdress: string) => {
    const selectedAddressToArray = selectedAdress.split(',')
    let deliveryAddress = ''
    let addressDetail = selectedAddressToArray[0] + selectedAddressToArray[1]
    if (selectedAddressToArray.length === 5) {
      const selectedAddressSlice = selectedAddressToArray.slice(1, 6)
      deliveryAddress = selectedAddressSlice.toString().trim()
    } else if (selectedAddressToArray.length === 6) {
      const selectedAddressSlice = selectedAddressToArray.slice(1, 7)
      deliveryAddress = selectedAddressSlice.toString().trim()
    } else if (selectedAddressToArray.length === 7) {
      const selectedAddressSlice = selectedAddressToArray.slice(1, 8)
      deliveryAddress = selectedAddressSlice.toString().trim()
    } else if (selectedAddressToArray.length === 8) {
      const selectedAddressSlice = selectedAddressToArray.slice(3, 9)
      deliveryAddress = selectedAddressSlice.toString().trim()
    } else {
      deliveryAddress = selectedAdress
    }
    return { deliveryAddress, addressDetail }
  }

  onConfirm = () => {
    const { navigation } = this.props
    const { selectedAddress, addressDetail, coordinate } = this.state
    const newSelectedAddress = this.formatSelectedAddress(selectedAddress).deliveryAddress
    navigation.navigate(navigation.state.params.createScreenName || 'CreateAddress', {
      ...navigation.state.params,
      selectedAddress: {
        latitude: coordinate.latitude,
        longitude: coordinate.longitude,
        addressPinpoint: addressDetail,
        addressDetail: newSelectedAddress,
      },
    })
  }

  setLocation = async (region) => {
    if (region && region.longitude !== 0) {
      const latitude = region.latitude
      const longitude = region.longitude
      this.checkAddressCoverage(latitude, longitude)
      const res = await GeoService.from(latitude, longitude)
      const selectedAddress = res && res.results[0].formatted_address
      const newSelectedAddress = this.formatSelectedAddress(selectedAddress).deliveryAddress
      const { addressDetail } = this.formatSelectedAddress(selectedAddress)
      this.setState({
        coordinate: {
          latitude,
          longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        },
        selectedAddress: newSelectedAddress,
        addressDetail
      })
    }
  }

  onRegionChange = region => {
    const latitude = region.latitude
    const longitude = region.longitude
    this.setState({
      coordinate: {
        latitude,
        longitude,
        latitudeDelta: 0.005,
        longitudeDelta: 0.005,
      },
      selectedAddress: 'Searching...'
    })
  }

  backToFirstLocation = () => {
    this.map.animateToRegion(this.state.initialRegion, 300)
  }

  navigateToSearchLocation = () => {
    const { navigation } = this.props
    navigation.navigate({
      routeName: navigation.state.params.searchScreenName || 'SearchLocation',
      params: {
        noSearchLocation: false,
        mapScreenName: navigation.state.params.mapScreenName
      },
      key: 'SearchLocationKey',
    })
  }

  renderAlert = () => {
    if (Platform.OS == 'ios') {
      return (
        <AlertBox
          primaryAction={this.backToFirstLocation}
          primaryActionText='Back to Original Location'
          secondaryAction={this.navigateToSearchLocation}
          secondaryActionText='Search Another Location'
          dismiss={() => this.setState({ showOutsideCoverageAreaModal: false })}
          title='Out of Delivery Coverage'
          text='Unfortunately, we’re not able to deliver to this location yet. Please select another location.'
          visible={this.state.showOutsideCoverageAreaModal}
        />
      )
    }
    return (
      this.state.showOutsideCoverageAreaModal &&
      <View style={{ position: 'absolute', top: verticalScale(-150), bottom: 0, left: 0, right: 0, zIndex: this.state.showOutsideCoverageAreaModal ? 100 : 0 }}>
        <AlertBox
          primaryAction={this.backToFirstLocation}
          primaryActionText='Back to Original Location'
          secondaryAction={this.navigateToSearchLocation}
          secondaryActionText='Search Another Location'
          dismiss={() => this.setState({ showOutsideCoverageAreaModal: false })}
          title='Out of Delivery Coverage'
          text='Unfortunately, we’re not able to deliver to this location yet. Please select another location.'
          visible={this.state.showOutsideCoverageAreaModal || true}
        />
      </View>
    )
  }


  render() {
    const { initialRegion, selectedAddress, addressDetail, mapMargin } = this.state
    const { navigation } = this.props
    const newSelectedAddress = this.formatSelectedAddress(selectedAddress).deliveryAddress
    return (
      <KeyboardAvoidingView
        behavior={
          Platform.OS == 'ios'
            ? 'padding'
            : 'height'
        }
        keyboardVerticalOffset={80}
        style={{
          flex: 1,
        }}
      >
        <View style={{ flex: 4 }}>
          <MapView
            ref={ref => this.map = ref}
            style={[StyleSheet.absoluteFillObject, { marginBottom: mapMargin, zIndex: 5 }]}
            onMapReady={this.setMargin}
            provider={PROVIDER_GOOGLE}
            // region={initialRegion}
            initialRegion={initialRegion}
            showsUserLocation={true}
            followsUserLocation={true}
            showsMyLocationButton
            onRegionChangeComplete={this.setLocation}
          />

          <View
            style={{
              left: '50%',
              marginLeft: -18,
              marginTop: -36,
              position: 'absolute',
              top: '50%',
              zIndex: 100,
            }}
          >
            <Image
              style={{
                height: 36,
                width: 36,
              }}
              source={Images.mapPin}
            />
          </View>

          <Button
              transparent
              onPress={() => navigation.goBack()}
              testID="back"
              accessibilityLabel="back"
              style={{
                position: 'absolute',
                zIndex:10,
                top: 25,
                left: 20,
                width: 40,
                height: 40,
                backgroundColor: Colors.primaryWhite,
                borderRadius: 50,
                shadowOpacity: 0.2,
                shadowRadius: 0,
                shadowColor: Colors.primaryDark,
                shadowOffset: { width: 0, height: 2 },
                elevation: 5,
                justifyContent: 'center'
              }}
            >
              <Image
                source={Images.arrowLeft}
                style={{ width: scale(16) }}
                resizeMode='contain'
              />
            </Button>
          {/* <View style={{ position: 'absolute', bottom: 20, right: 20 }}>
            <TouchableWithoutFeedback
              style={Styles.buttonCenterPosition}
              onPress={this.moveToRegion}
            >
              <Image source={Images.Gps} />
            </TouchableWithoutFeedback>
          </View> */}
        </View>
        <View style={Styles.card}>
          <View style={{ marginBottom: 14, height: 50 }}>
            <Text style={Styles.label}>Lokasi Pinpoint Pengiriman</Text>
            <Text style={Styles.text}>{newSelectedAddress}</Text>
          </View>
          <View style={{}}>
            <View style={Styles.labelContainer}>
              <Text style={Styles.label}>Alamat Lengkap</Text>
              <Text style={Styles.remark}>*</Text>
            </View>
            <View style={{ marginBottom: 10 }}>
              <Text style={Styles.text}>Alamat ini akan dimunculkan pada paket pengiriman anda</Text>
            </View>
            <TextInput
              placeholderTextColor={Colors.secondaryGrey}
              placeholder='Isi dengan nama jalan, nomor rumah, 
              nama gedung, lantai, atau nomor unit
              '
              autoCapitalize='none'
              onChangeText={(text) => this.setState({addressDetail: text})}
              value={addressDetail}
              testID="addressDetail"
              accessibilityLabel="addressDetail"
              multiline={true}
            />
          </View>
          <BottomButton
            disabled={addressDetail === '' ? true : false}
            customStyle={Styles.bottomButton}
            testID="saveAddress"
            accessibilityLabel="saveAddress"
            buttonText="Selesai"
            pressHandler={this.onConfirm}
          />
        </View>
        {this.renderAlert()}
      </KeyboardAvoidingView>
    )
  }
}

export default MapScreen
