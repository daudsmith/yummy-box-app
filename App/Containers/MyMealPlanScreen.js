import _ from 'lodash'
import moment from 'moment'
import { Container, Content } from 'native-base'
import React, { Component } from 'react'
import { ActivityIndicator, Text, TouchableWithoutFeedback, View } from 'react-native'
import { Calendar } from 'react-native-calendars'
import { moderateScale } from 'react-native-size-matters'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import NavigationBar from '../Components/NavigationBar'
import OfflineModal from '../Components/OfflineModal'
import OfflinePage from '../Components/OfflinePage'
import icoMoonConfig from '../Images/SvgIcon/selection.json'
import MyMealPlanActions from '../Redux/MyMealPlanRedux'
import StartupActions from '../Redux/StartupRedux'
import ThemeActions from '../Redux/ThemeRedux'
import AvailabilityDateService from '../Services/AvailabilityDateService'
import LocaleFormatter from '../Services/LocaleFormatter'
import OrderDeliveryHelper from '../Services/OrderDeliveryHelper'
import Colors from '../Themes/Colors'
import Styles from './Styles/MyMealPlanScreenStyle'
import CustomerNotificationActions from '../Redux/CustomerNotificationRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import FastImage from 'react-native-fast-image'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

class MyMealPlanScreen extends Component {
  navigationListener
  constructor (props) {
    super(props)
    const openDates = AvailabilityDateService.getDeliveryDateList(this.props.offDates, this.props.setting.delivery_cut_off)
    this.state = {
      fetching: false,
      myMealPlans: [],
      isCalendarView: true,
      selectedDate: null,
      deliveryDates: null,
      showBlankOfflinePage: !this.props.isConnected && !this.props.hasFetchedData,
      hasFetchedData: false,
      openDates
    }
  }

  componentDidMount () {
    if (this.props.isConnected) {
      this.props.getMyMealPlans()
    }
    this.navigationListener = this.props.navigation.addListener('didFocus', this.props.getMyMealPlans)
  }

  componentWillUnmount () {
    this.navigationListener.remove()
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    const {myMealPlans, fetching, errorMessage} = newProps.myMealPlan
    this.setState({myMealPlans, fetching}, () => {
      const deliveryDates = this.markDeliveryDatesAndSelectedDates()
      if (deliveryDates !== null && !this.state.hasFetchedData) {
        let deliveryDatesWithOrder = []
        for (let key in deliveryDates) {
          if (deliveryDates[key].marked === true) {
            deliveryDatesWithOrder.push(key)
          }
        }

        this.setState({selectedDate: deliveryDatesWithOrder[0], hasFetchedData: true})
      }
    })

    if (!fetching && errorMessage === null) {
      this.setState({hasFetchedData: true})
    }
    if (!this.props.isConnected && newProps.isConnected && !this.state.hasFetchedData) {
      if (!this.props.hasFetchedStartupData) {
        this.props.startupOnMmpScreen()
        this.setState({showBlankOfflinePage: false})
      } else {
        this.props.getMyMealPlans()
      }
    }
  }

  rightButton () {
    const {isCalendarView} = this.state
    return (
      <View style={{flexDirection: 'row'}}>
        <TouchableWithoutFeedback onPress={() => this.setState({isCalendarView: true})}>
          <View style={Styles.rightNavigationButtonLeftIconContainer}>
            <YumboxIcon name='calendar-orange' size={moderateScale(20)} color={isCalendarView ? Colors.bloodOrange : Colors.pinkishGrey} />
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => this.setState({isCalendarView: false})}>
          <View style={Styles.rightNavigationButtonRightIconContainer}>
            <YumboxIcon name='list-view' size={moderateScale(20)} color={isCalendarView ? Colors.pinkishGrey : Colors.bloodOrange} />
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }

  markDeliveryDatesAndSelectedDates () {
    const {myMealPlans, selectedDate, openDates} = this.state
    let dates = {}

    dates[moment().format('YYYY-MM-DD')] = {disabled: false}

    _.map(openDates, (value) => {
      dates[value.format('YYYY-MM-DD')] = {disabled: false}
    })

    _.map(myMealPlans, (value, date) => {
      if (dates.hasOwnProperty(date)) {
        if (selectedDate === date) {
          dates[date] = {...dates[date], marked: true, dotColor: Colors.bloodOrange, selected: true}
        } else {
          dates[date] = {...dates[date], marked: true, dotColor: Colors.bloodOrange, disabled: false}
        }
      } else {
        dates[date] = {dotColor: Colors.bloodOrange, marked: true}
      }
    })

    return dates
  }

  selectDeliveryDate (date) {
    this.setState({selectedDate: date})
  }

  navigateToMyMealPlanDetail (id, deliveryNumber, type) {
    this.props.clearMmpDetail()
    this.props.resetAvailableDates()
    this.props.resetModifyState()
    this.props.navigation.navigate('MyMealPlanDetailScreen', {id, deliveryNumber, type})
  }

  getPaymentMethodIcon (payment) {
    switch (payment) {
      case 'Credit Card':
        return 'payment-cc'
      case 'Bank Transfer':
        return 'transfer-icon'
      case 'YumCredits':
        return 'wallet'
      default:
        return 'payment-cash'
    }
  }

  renderMealPlanDay (mealPlan, date, listViewItemIndex, listViewLastItemIndex) {
    const dateText = moment(date).format('ddd, D MMM YYYY')
    return (
      <View>
        <View style={Styles.mealDeliveryDateContainer}>
          <Text style={Styles.deliveryDateLabelText}>Delivery Date</Text>
          <Text style={Styles.deliveryDateValueText}>{dateText}</Text>
        </View>
        <View style={{marginTop: moderateScale(23)}}>
          {mealPlan.map((delivery, index) => {
            const deliveryNumber = `#${delivery.number.replace('KDS-DELV-', '')}`
            const paymentMethodIcon = this.getPaymentMethodIcon(delivery.payment)
            const iterationLastIndex = this.state.isCalendarView ? index : listViewItemIndex + index
            const lastItemIndex = this.state.isCalendarView ? mealPlan.length - 1 : listViewLastItemIndex
            const lastItemBorderWidth = iterationLastIndex === lastItemIndex ? {borderBottomWidth: 0} : {}
            return (
              <TouchableWithoutFeedback onPress={() => this.navigateToMyMealPlanDetail(delivery.id, deliveryNumber, delivery.type)} key={index}>
                <View key={index} style={[Styles.deliveryMealContainer, lastItemBorderWidth]}>
                  <View style={{flex: 2, justifyContent: 'center'}}>
                    <FastImage
                      style={{height: moderateScale(90), width: moderateScale(90), borderRadius: 5}}
                      source={{
                        uri: delivery.item_image,
                        priority: FastImage.priority.normal,
                      }}
                    />
                  </View>
                  <View style={{flex: 3.5}}>
                    <View>
                      <Text style={{fontFamily: 'Rubik-Regular', fontWeight: '500', fontSize: moderateScale(14), color: Colors.greyishBrown}}>{OrderDeliveryHelper.parseStatusToLabel(delivery.status)}</Text>
                      <Text style={Styles.deliveryItemInfoText}>{`${delivery.time} delivery time`}</Text>
                      {delivery.type === 'package' && <Text style={Styles.deliveryItemInfoText}>{`${delivery.theme_name} - ${delivery.deliveredCount === 0 ? `${delivery.package} Package`: `${delivery.deliveredCount} of ${delivery.deliveryCount} Delivered`}`}</Text>}
                      {delivery.type === 'subscription' && <Text style={Styles.deliveryItemInfoText}>{`${delivery.theme_name} - Subscription`}</Text>}
                      <Text style={Styles.deliveryItemInfoText}>{delivery.item_name}</Text>
                      <Text style={Styles.deliveryItemInfoText}>{`Delivery ${deliveryNumber}`}</Text>
                    </View>
                    <View style={Styles.deliveryTotalContainer}>
                      <YumboxIcon name={paymentMethodIcon} color={Colors.bloodOrange} size={moderateScale(18)} />
                      <Text style={Styles.deliveryTotalText}>{LocaleFormatter.numberToCurrency(delivery.total)}</Text>
                    </View>
                  </View>
                  <View style={{flex: 0.5, justifyContent: 'center', alignItems: 'flex-end'}}>
                    <Ionicons name='chevron-forward' style={{color: Colors.pinkishGrey, fontSize: moderateScale(16)}} />
                  </View>
                </View>
              </TouchableWithoutFeedback>
            )
          })}
        </View>
      </View>
    )
  }

  renderMealPlanCalendarView = () => {
    const {selectedDate, myMealPlans} = this.state
    const deliveryDates = this.markDeliveryDatesAndSelectedDates()
    const selectedMeal = myMealPlans.hasOwnProperty(selectedDate) ? myMealPlans[selectedDate] : null
    return (
      <View style={{flex: 1}}>
        <View>
          <Calendar
            minDate={deliveryDates[0]}
            maxDate={deliveryDates[deliveryDates.length - 1]}
            disabledByDefault
            markedDates={deliveryDates}
            onDayPress={(date) => this.selectDeliveryDate(date.dateString)}
            theme={{
              arrowColor: Colors.green,
              monthTextColor: Colors.green,
              selectedDayTextColor: Colors.bloodOrange,
              todayTextColor: Colors.greyishBrownTwo,
              dayTextColor: Colors.greyishBrownTwo,
              textSectionTitleColor: Colors.greyishBrownTwo,
              textMonthFontFamily: 'Rubik-Regular',
              textDayFontFamily: 'Rubik-Regular',
              textDayHeaderFontFamily: 'Rubik-Regular',
              selectedDayBackgroundColor: Colors.white,
              selectedDotColor: Colors.bloodOrange
            }}
          />
        </View>
        <View style={Styles.calendarInfoContainer}>
          <Text style={Styles.calendarInformationText}>Showing only meal plans for next 14 days</Text>
        </View>
        {
          selectedMeal !== null ? (
              this.renderMealPlanDay(selectedMeal, selectedDate)
            ) : (
              <View style={Styles.messageContainer}>
                <Text style={Styles.messageText}>You don’t have any meal plans yet</Text>
              </View>
          )
        }
      </View>
    )
  }

  renderMealPlanListView () {
    const {myMealPlans} = this.state
    let index = 0
    let totalDelivery = 0

    _.map(myMealPlans, (value) => {
      totalDelivery += value.length
    })

    return _.map(myMealPlans, (value, date) => {
      index += value.length
      return (
        <View key={date}>
          {this.renderMealPlanDay(value, date, index - value.length, totalDelivery - 1)}
        </View>
      )
    })
  }

  renderLoadingView () {
    return (
      <View style={Styles.activityIndicatorContainer}>
        <ActivityIndicator size='large' />
      </View>
    )
  }

  _openHamburger = () => {
    const { user, isConnected, fetchWallet, fetchUnreadNotifications, navigation } = this.props
    if (user && isConnected) {
      fetchWallet()
      fetchUnreadNotifications()
    }
    navigation.toggleDrawer()
  }

  render () {
    const {fetching, isCalendarView, showBlankOfflinePage, myMealPlans} = this.state
    return (
      <Container>
        <NavigationBar
          leftSide='menu'
          title='My Meal Plan'
          rightSide={() => this.rightButton()}
          openHamburger={this._openHamburger}
          unread={this.props.unreadNotification}
        />
        <Content contentContainerStyle={{flexGrow: 1, paddingHorizontal: moderateScale(20)}} style={{backgroundColor: 'white'}}>
          {!this.props.isConnected && showBlankOfflinePage ? (
            <OfflinePage />
          ) : (
            fetching ? (
              this.renderLoadingView()
            ) : (
              isCalendarView ? (
                <View style={{flex: 1}}>
                  {this.renderMealPlanCalendarView()}
                </View>
              ) : (
                myMealPlans.length === 0 ? (
                  <View style={Styles.messageContainer}>
                    <Text style={Styles.messageText}>You don’t have any meal plans yet</Text>
                  </View>
                ) : (
                  this.renderMealPlanListView()
                )
              )
            )
          )}
          {!showBlankOfflinePage &&
            <OfflineModal isConnected={this.props.isConnected} />
          }
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    myMealPlan: state.myMealPlan,
    offDates: state.meals.offDates,
    isConnected: state.network.isConnected,
    hasFetchedStartupData: state.startup.hasFetchedStartupData,
    setting: state.setting.data,
    user: state.login.user,
    unreadNotification: state.customerNotification.unread,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getMyMealPlans: () => dispatch(MyMealPlanActions.getMyMealPlans()),
    startupOnMmpScreen: () => dispatch(StartupActions.startupOnMmpScreen()),
    clearMmpDetail: () => dispatch(MyMealPlanActions.clearMmpDetail()),
    resetAvailableDates: () => dispatch(ThemeActions.resetAvailableDates()),
    resetModifyState: () => dispatch(MyMealPlanActions.resetModifyState()),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchUnreadNotifications: () => dispatch(CustomerNotificationActions.fetchTotalUnreadNotifications()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyMealPlanScreen)
