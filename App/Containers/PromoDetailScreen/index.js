import { Content, Toast } from 'native-base'
import React, { Component } from 'react'
import { Clipboard, Dimensions, Image, View } from 'react-native'
import { connect } from 'react-redux'
import NavigationBar from '../../Components/NavigationBar'
import WithSafeAreaView from '../../Components/Common/WithSafeAreaView'
import Body from './Body'
import FooterPromo from './FooterPromo'
import { ScrollView } from 'react-native-gesture-handler'


class PromoDetailScreen extends Component {
  constructor (props) {
    super(props)

    this._writeToClipboard = this._writeToClipboard.bind(this)
  }

  _writeToClipboard = async () => {
    const { promo } = this.props.navigation.state.params

    await Clipboard.setString(promo.code_text)
    Toast.show({
      text: 'Copied to clipboard',
      buttonText: '',
      duration: 1500
    })
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const { promo } = this.props.navigation.state.params
    const { isConnected } = this.props
    const ratioErr = 6.5
    const {width, height} = Dimensions.get('window')

    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <NavigationBar
          leftSide={'back'}
          title={promo.title}
          leftSideNavigation={this.navigationGoBack}
          currentScreen='PromoDetailScreen'
          />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Content>
            <Image
              source={{uri: promo.banner_image}}
              resizeMode='contain'
              style={{
                width: width,
                height: (((width / height) * width)-ratioErr),
              }}
            />
            <Body
              content={promo.description}
              toc={promo.terms_and_condition}
            />
          </Content>
        </ScrollView>
        {(promo.code_text !== null && promo.code_text !== '') &&
        <FooterPromo
          code={promo.code_text}
          onCopy={this._writeToClipboard}
          disabled={!isConnected}
        />
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isConnected: state.network.isConnected
  }
}

export default WithSafeAreaView(connect(mapStateToProps, null)(PromoDetailScreen))
