import MarkdownIt from 'markdown-it'
import React from 'react'
import { Platform, StyleSheet, Text, View } from 'react-native'
import Markdown from 'react-native-markdown-renderer'
import hasParents from '../../../node_modules/react-native-markdown-renderer/src/lib/util/hasParents'
import { Colors } from '../../Themes'

const md = MarkdownIt({
  linkify: true,
})

export default ({ style = {}, content, toc, ...props }) => (
  <View style={[{
    margin: 24,
    alignItems: 'flex-start'
  }, style]}
    {...props}
  >
    <Text style={{ color: Colors.blueGrey, fontSize: 14, fontWeight: 'bold', marginVertical: 8 }}>DESCRIPTION</Text>
    <Markdown
      markdownit={md}
      style={styles}
      rules={rules}
    >
      {content}
    </Markdown>
    {(toc !== null && toc !== '') &&
      <View style={{ marginTop: 24, width: '100%' }}>
        <Text style={{ color: Colors.blueGrey, fontSize: 14, fontWeight: 'bold', marginVertical: 8 }}>TERMS AND CONDITIONS</Text>
        <Markdown
          markdownit={md}
          style={styles}
          rules={rules}
        >
          {toc}
        </Markdown>
      </View>
    }
  </View>
)

const rules = {
  list_item: (node, children, parent, styles) => {
    if (hasParents(parent, 'bullet_list')) {
      return (
        <View key={node.key} style={styles.listUnorderedItem}>
          <Text style={styles.listUnorderedItemIcon}>{'\u00B7'}</Text>
          <View style={[styles.listItem]}>{children}</View>
        </View>
      )
    }

    if (hasParents(parent, 'ordered_list')) {
      return (
        <View key={node.key} style={styles.listOrderedItem}>
          <Text style={styles.listOrderedItemIcon}>{node.index + 1}.</Text>
          <View style={[styles.listItem]}>{children}</View>
        </View>
      )
    }

    return (
      <View key={node.key} style={[styles.listItem]}>
        {children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    color: '#000',
    fontSize: 15,
    lineHeight: 22
  },
  heading: {
    color: Colors.blueGrey,
    fontWeight: 'bold',
  },
  listOrderedItemIcon: {
    color: Colors.blueGrey,
    fontSize: 15,
    marginLeft: 10,
    marginRight: 10,
    fontWeight: 'bold',
    ...Platform.select({
      ['ios']: {
        lineHeight: (22 + 12),
      },
      ['android']: {
        lineHeight: (22 + 13),
      },
    }),
  },
  listUnorderedItemIcon: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 42,
    ...Platform.select({
      ['ios']: {
        lineHeight: 39,
      },
      ['android']: {
        lineHeight: 44,
      },
    }),
  },
  paragraph: {
    marginTop: 6,
    marginBottom: 6,
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  link: {
    textDecorationLine: 'underline',
  },
})
