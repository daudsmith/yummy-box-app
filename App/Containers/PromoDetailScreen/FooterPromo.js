import { Footer } from 'native-base'
import React from 'react'
import { Text, View } from 'react-native'
import PrimaryButton from '../../Components/PrimaryButton'
import { Colors } from '../../Themes'



export default ({ disabled, onCopy, code }) => (
  <Footer style={{ paddingHorizontal: 20, alignItems: 'center', height: 64, backgroundColor: 'white'}}>
    <View style={{flex: 1}}>
      <Text style={{color: 'black',fontSize: 16, fontWeight: 'bold' }}>{ code }</Text>
      <Text style={{fontSize: 12, color: Colors.blueGrey}}>Promo Code</Text>
    </View>
    <View style={{flex: 1, alignItems: 'flex-end'}}>
      <PrimaryButton
        label='Copy'
        iconName='copy'
        iconColor='white'
        iconSize={15}
        iconPosition='right'
        onPressMethod={onCopy}
        disabled={disabled}
        labelStyle={{fontSize: 14}}
        buttonStyle={[
          {
            marginVertical: 10,
            width: 100
          }, disabled ? {backgroundColor: Colors.pinkishGrey} : {}
        ]}
      />
    </View>
  </Footer>
)
