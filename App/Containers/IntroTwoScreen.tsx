import React from 'react'
import { ImageBackground, Image, View, Text, StatusBar } from 'react-native'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { scale } from 'react-native-size-matters'
import { Images } from '../Themes'
import Styles from './Styles/IntroTwoStyle'
import { Colors } from '../Themes'
import NavigationBar from '../Components/NavigationBar'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'
import { AppState } from '../Redux/CreateStore'
import LastUsedActions, { Location } from '../Redux/V2/LastUsedRedux'
import { connect } from 'react-redux'

export interface IntroTwoScreenProps {
  navigation: NavigationDrawerProp,
  isConnected: boolean,
  token: string,
  addLastUsed: (code: string, location: Location, name: string, address: string) => void,
}

class IntroTwoScreen extends React.Component<IntroTwoScreenProps> {

  handlePress = () => {
    const { navigation, token } = this.props
    navigation.navigate('SearchLocation', { noSearchLocation: token ? true : false })
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={{ flex: 1 }}>
        <StatusBar translucent backgroundColor='transparent' />
        <View style={{ flex: 1 }}>
          <ImageBackground source={Images.IntroTwoBackground} style={Styles.backgroundImage}>
            {
              navigation.state.params && navigation.state.params.to === 'guest' && (
                <NavigationBar
                  title=''
                  leftSide='back'
                  leftSideNavigation={() => navigation.goBack()}
                  statusBarColor='transparent'
                  containerCustomStyle={Styles.containerCustom}
                  leftButtonCustomStyle={Styles.leftButtonCustom}
                />
              )
            }
          </ImageBackground>
        </View>
        <View style={Styles.card}>
          <Image source={Images.Map} />
          <View style={Styles.containerTitle}>
            <Text style={Styles.title}>Select a Location to Continue</Text>
          </View>
          <View style={Styles.containerDesc}>
            <Text style={Styles.desc}>Your favorite meal will be delivered fresh from our closest kitchen!</Text>
          </View>
          <TouchableWithoutFeedback onPress={() => this.handlePress()} style={Styles.containerButtonSearch}>
            <View style={Styles.containerIcon}>
              <Ionicons name='search-outline' size={scale(25)} color={Colors.secondaryGrey} />
            </View>
            <Text style={Styles.textSearch}>Company, Building, or Residence...</Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    token: state.login.token,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    addLastUsed: (code: string, location: Location, name: string, address: string) => dispatch(LastUsedActions.addLastUsed(code, location, name, address))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(IntroTwoScreen)
