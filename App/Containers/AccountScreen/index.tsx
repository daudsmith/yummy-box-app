import React, { Component } from 'react'
import { Alert, RefreshControl, ScrollView, Text, TouchableWithoutFeedback, View } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { moderateScale } from 'react-native-size-matters'
import { SwitchActions } from 'react-navigation'
import { connect } from 'react-redux'
import NavigationBar from '../../Components/NavigationBar'
import YummyboxIcon from '../../Components/YummyboxIcon'
import WithSafeAreaView from '../../Components/Common/WithSafeAreaView'
import CustomerAccountActions from '../../Redux/CustomerAccountRedux'
import LoginActions from '../../Redux/LoginRedux'
import RegisterActions from '../../Redux/RegisterRedux'
import { Images } from '../../Themes'
import Colors from '../../Themes/Colors'
import Styles from '../Styles/AccountScreenStyle'
import drawerStyles from '../Styles/DrawerContentStyles'
import ListButton from './Components/ListButton'
import ProfileDetail from './Components/ProfileDetail'
import ProfilePicture from './Components/ProfilePicture'
import YummyCredits from './Components/YummyCredits'
import CustomerNotificationActions from '../../Redux/CustomerNotificationRedux'
import CookieManager from '@react-native-community/cookies'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'

const FBSDK = require('react-native-fbsdk')
const {
  LoginManager,
} = FBSDK

const facebookLogout = () => {
  LoginManager.logOut()
}

export interface customerAddressInterface {
  addresses: string[], 
  deleted: boolean, 
  edited: boolean, 
  error: string, 
  loading: boolean, 
  saved: boolean
}

export interface customerCardInterface {
  cards: string[], 
  error: string, 
  loading: boolean
}

export interface userInterface {
  current_credits: number, 
  email: string, 
  first_name: string, 
  id: number, 
  invitation_code: string, 
  invitation_uri: string, 
  invite_code: string, 
  last_name: string, 
  newsletter_subscribed: number, 
  phone: string, 
  photo: string, 
  salutation: string, 
  social_login_id: string, 
  social_login_type: string
}

export interface customerAccountInterface {
  changeEmailError: string, 
  changeEmailSuccess: string, 
  changePasswordError: string, 
  changePasswordSuccess: string, 
  changePhoneNumberError: string, 
  changePhoneNumberSuccess: string, 
  changingEmail: boolean, 
  changingPassword: boolean, 
  changingPhoneNumber: boolean, 
  checkValidateOtp: boolean, 
  checkValidateOtpDone: string, 
  checkValidateOtpFailed: string, 
  corporateBalance: number, 
  fetchingWallet: boolean, 
  isClient: boolean, 
  personalBalance: number, 
  wallet: number, 
  walletError: string
}

export interface AccountPropsInterface {
  customerAddress: customerAddressInterface
  customerCard: customerCardInterface,
  user: userInterface,
  resetChangeOtpStatus: () => void,
  resetChangeEmailAndPhone: () => void,
  navigation: NavigationDrawerProp,
  doLogout: () => void,
  uploadPhoto: (postData) => void,
  fetchWallet: () => void,
  isConnected: boolean,
  fetchUnreadNotifications: () => void,
  customerAccount: customerAccountInterface,
  unreadNotification: number
}

export interface AccountStateInterface {
  customerAddress: customerAddressInterface,
  customerCard: customerCardInterface,
  user: userInterface,
  currentScreen: string,
  uploadingPhoto: boolean,
  uploadingPhotoError: string,
  refreshing: boolean,
}

class AccountScreen extends Component<AccountPropsInterface, AccountStateInterface> {
  constructor(props) {
    super(props)
    this.state = {
      customerAddress: props.customerAddress,
      customerCard: props.customerCard,
      user: props.user,
      currentScreen: 'AccountScreen',
      uploadingPhoto: false,
      uploadingPhotoError: null,
      refreshing: false,
    }

    this._fetchingWallet = this._fetchingWallet.bind(this)
  }

  componentDidMount() {
    this.props.resetChangeOtpStatus()
    this.props.resetChangeEmailAndPhone()
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    this.forceUpdate()
    if (newProps.user !== null) {
      this.setState({
        uploadingPhoto: newProps.uploadingPhoto,
        uploadingPhotoError: newProps.uploadingPhotoError,
        user: newProps.user,
      })
    } else {
      this.resetToIntroOneScreen()
    }
    if (this.state.refreshing && !newProps.customerAccount.fetchingWallet) {
      this.setState({ refreshing: false })
    }

  }

  resetToIntroOneScreen = () => {
    this.props.navigation.dispatch(
      SwitchActions.jumpTo({
        routeName: 'IntroNavigation',
      }),
    )
  }

  handleLogoutPress = () => {
    Alert.alert(
      'Logout Confirmation',
      'Are you sure want to logout from Yummybox?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            this.props.doLogout()
            facebookLogout()
            CookieManager.clearAll()
          },
        },
      ],
      { cancelable: false },
    )
  }

  selectPhotoTapped = (isConnected) => {
    if (isConnected) {
      const options = {
        quality: 1.0,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
        },
      }

      ImagePicker.showImagePicker(options, (response) => {
        let postData = new FormData()
        if (!response.didCancel) {
          postData.append('photo', 'data:image/jpeg;base64,' + response.data)
          postData.append('uri', response.uri)
          this.props.uploadPhoto(postData)
        }
      })
    }
  }

  requestChangePhoneNumber = () => {
    this.props.navigation.navigate('ChangePhoneNumberScreen')
  }

  requestChangeEmail = () => {
    this.props.navigation.navigate('ChangeEmail')
  }

  _fetchingWallet = () => {
    this.setState({
      refreshing: true,
    }, this.props.fetchWallet)
  }

  _openHamburger = () => {
    const { user, isConnected, fetchWallet, fetchUnreadNotifications, navigation } = this.props
    if (user && isConnected) {
      fetchWallet()
      fetchUnreadNotifications()
    }
    navigation.toggleDrawer()
  }

  render() {
    const { user, refreshing } = this.state
    const { isConnected, navigation, customerAccount } = this.props
    const { wallet, personalBalance, corporateBalance, isClient, fetchingWallet } = customerAccount

    const customerLastName = user.last_name !== null
      ? user.last_name
      : ' '
    const customerName = user
      ? `${user.first_name} ${customerLastName}`
      : null
    const customerEmail = user
      ? user.email
      : null
    let customerPhone = ''
    if (user) {
      customerPhone = user.phone && user.phone[0] === '+'
        ? user.phone.substr(1)
        : user.phone
    }

    return (
      <View style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
        <NavigationBar
          leftSide='menu'
          title='My Profile'
          openHamburger={this._openHamburger}
          unread={this.props.unreadNotification}
        />
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this._fetchingWallet}
              progressBackgroundColor={Colors.bloodOrange}
              tintColor={Colors.bloodOrange}
              colors={[Colors.white]}
            />
          }
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              paddingHorizontal: moderateScale(21),
              paddingTop: moderateScale(19),
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                paddingBottom: moderateScale(10),
                borderBottomWidth: 1,
                borderBottomColor: Colors.lightGrey,
              }}
            >
              <ProfileDetail
                name={customerName}
                phone={customerPhone}
                email={customerEmail}
                changeEmailAction={this.requestChangeEmail}
                changePhoneAction={this.requestChangePhoneNumber}
              />

              <ProfilePicture
                styles={Styles}
                photo={!user.photo
                  ? Images.YumboxAvatarDefault
                  : { uri: user.photo }}
                isConnected={isConnected}
                onPress={this.selectPhotoTapped}
              />
            </View>

            <YummyCredits
              fetchingWallet={fetchingWallet}
              wallet={wallet}
              personalBalance={personalBalance}
              corporateBalance={corporateBalance}
              isClient={isClient}
              styles={drawerStyles}
              navigation={navigation}
            />

            <View
              style={{
                borderBottomColor: Colors.lightGrey,
                borderBottomWidth: 1,
                marginTop: 5,
                marginBottom: 5,
              }}
            />

            <ListButton
              text={'My Cards'}
              navigateToScreen={'CreditCardListScreen'}
              iconName={'chevron-right'}
              iconType={'iconEntypo'}
              navigation={navigation}
            />

            <ListButton
              text={'Saved Addresses'}
              navigateToScreen={'MyAddressList'}
              iconName={'chevron-right'}
              iconType={'iconEntypo'}
              navigation={navigation}
            />

            {user.social_login_type !== 'facebook' &&
            <ListButton
              text={'Change Password'}
              navigateToScreen={'ChangePasswordScreen'}
              iconName={'chevron-right'}
              iconType={'iconEntypo'}
              navigation={navigation}
            />
            }
          </View>

          <View style={Styles.logoutButtonContainer}>
            <TouchableWithoutFeedback
              testID='logout'
              accessibilityLabel='logout'
              onPress={() => this.handleLogoutPress()}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  paddingVertical: moderateScale(16),
                }}
              >
                <Text
                  style={Styles.logoutText}
                >
                  Log Out
                </Text>
                <YummyboxIcon
                  name='logout'
                  size={moderateScale(16)}
                  color={Colors.darkBlue}
                />
              </View>
            </TouchableWithoutFeedback>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.login.user,
    customerAddress: state.customerAddress,
    customerCard: state.customerCard,
    uploadingPhotoError: state.login.uploadingPhotoError,
    uploadingPhoto: state.login.uploadingPhoto,
    customerAccount: state.customerAccount,
    isConnected: state.network.isConnected,
    unreadNotification: state.customerNotification.unread,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    doLogout: () => dispatch(LoginActions.logout()),
    uploadPhoto: (data) => dispatch(LoginActions.uploadPhoto(data)),
    resetChangeOtpStatus: () => dispatch(RegisterActions.resetChangeOtpStatus()),
    resetChangeEmailAndPhone: () => dispatch(CustomerAccountActions.resetChangeEmailAndPhone()),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchUnreadNotifications: () => dispatch(CustomerNotificationActions.fetchTotalUnreadNotifications()),
  }
}

export default WithSafeAreaView(connect(mapStateToProps, mapDispatchToProps)(AccountScreen))
