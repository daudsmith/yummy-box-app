import * as React from 'react'
import {
  Image,
  ImageSourcePropType,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { scale } from 'react-native-size-matters'
import Icon from 'react-native-vector-icons/Entypo'
import YummyboxIcon from '../../../Components/YummyboxIcon'
import Colors from '../../../Themes/Colors'
import Styles from '../../Styles/AccountScreenStyle'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'

export interface ListButtonProps {
  navigation: NavigationDrawerProp,
  iconName: ImageSourcePropType | string,
  text: string,
  navigateToScreen: string | Function,
  iconType?: string,
}

class ListButton extends React.PureComponent<ListButtonProps> {
  static defaultProps = {
    iconType: 'icon',
  }

  goToScreen = () => {
    const {
      navigation,
      navigateToScreen,
    } = this.props
    if (typeof navigateToScreen === 'function') {
      navigateToScreen()
    } else {
      navigation.navigate({
        routeName: navigateToScreen,
        key: navigateToScreen + 'Key'
      })
    }
  }

  render() {
    const {
      iconName,
      iconType,
      text,
    } = this.props
    let renderedIcon
    if (iconType !== 'icon') {
      if (iconType === 'iconEntypo') {
        renderedIcon = (
          <Icon
            name={iconName}
            color={Colors.primaryDark}
            size={scale(20)}
            style={Styles.arrow}
          />
        )
      } else {
        renderedIcon = (
          <Image
            source={(iconName as ImageSourcePropType)}
            style={{
              width: scale(21),
              height: 'auto',
            }}
            resizeMode='contain'
          />
        )
      }
    } else {
      renderedIcon = (
        <YummyboxIcon
          name={iconName}
          size={scale(21)}
          color={Colors.primaryDark}
        />
      )
    }

    return (
      <TouchableWithoutFeedback onPress={this.goToScreen}>
        <View style={Styles.listButton}>
          <Text style={Styles.listButtonText}>{text}</Text>
          {renderedIcon}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

export default ListButton
