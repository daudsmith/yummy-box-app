import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native'
import Colors from '../../../Themes/Colors'
import { scale, verticalScale } from 'react-native-size-matters'

class ProfilePicture extends Component {
  render () {
    const {
      photo,
      styles,
      isConnected,
      onPress,
    } = this.props
    const linkButtonColor = isConnected ? {color: Colors.green} : {color: Colors.pinkishGrey}
    return (
      <TouchableWithoutFeedback onPress={() => onPress(isConnected)}>
        <View
          style={{
            flex: 1,
            alignItems: 'center'
          }}
        >
          <Image
            source={photo}
            style={{
              width: scale(70),
              height: scale(70),
              borderRadius: 8,
              marginBottom: verticalScale(4),
            }}
          />
          <Text
            style={{
              ...styles.changePictureText,
              ...linkButtonColor,
            }}
          >
            Change Picture
          </Text>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

ProfilePicture.propTypes = {
  photo: PropTypes.any,
  styles: PropTypes.object.isRequired,
  isConnected: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
}

export default ProfilePicture
