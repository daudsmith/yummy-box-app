import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ActivityIndicator, Text, View } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../../Images/SvgIcon/selection.json'
import LocaleFormatter from '../../../Services/LocaleFormatter'
import { Colors } from '../../../Themes'
import DrawerButton from '../../DrawerContent/Components/DrawerButton'
import BalanceComponent from './Balance'
import { scale, verticalScale } from 'react-native-size-matters'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

class YummyCredits extends Component {
  render () {
    const {
      fetchingWallet,
      isClient,
      wallet,
      styles,
      personalBalance,
      corporateBalance,
      navigation,
    } = this.props
    return (
      <View style={styles.yummyCreditsContainer}>
        <View style={{ flexDirection: 'row', marginBottom: verticalScale(16), alignItems: 'center' }}>
          <View style={{ flexDirection: 'column', flex: 1}}>
            <View>
              <Text style={styles.creditsText}>Yummycredits</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'row' }}>
                <YumboxIcon name='payment-yumCredits' size={scale(16)} color={Colors.pinkishOrange}/>
                {fetchingWallet ? (
                  <ActivityIndicator size={'small'} style={styles.activityIndicator}/>
                ) : (
                  <Text style={styles.currencyText}>{LocaleFormatter.numberToCurrency(wallet)}</Text>
                )}
              </View>
            </View>
          </View>
          <DrawerButton
            label={'Top-up'}
            onPress={() => navigation.navigate('TopUpScreen', { navigationParams: 'menu' })}
          />
        </View>
        {!fetchingWallet && isClient &&
        <View style={{ flexDirection: 'row', marginBottom: verticalScale(16) }}>
          <BalanceComponent
            label={'Personal Balance'}
            balance={personalBalance}
            styles={styles}
          />
          <BalanceComponent
            label={'Corporate Balance'}
            balance={corporateBalance}
            styles={styles}
          />
        </View>
        }
      </View>
    )
  }
}

YummyCredits.propTypes = {
  isClient: PropTypes.bool.isRequired,
  wallet: PropTypes.number.isRequired,
  personalBalance: PropTypes.number,
  corporateBalance: PropTypes.number,
  styles: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired,
}

export default YummyCredits
