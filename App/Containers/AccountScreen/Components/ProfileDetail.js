import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import Styles from '../../../Containers/Styles/AccountScreenStyle'
import LocaleFormatter from '../../../Services/LocaleFormatter'

class ProfileDetail extends Component{
  render () {
    const {
      name,
      phone,
      email,
      changePhoneAction,
      changeEmailAction,
    } = this.props
    return (
      <View style={{flex: 3}}>
        <Text style={Styles.customerNameText}>{name}</Text>
        <TouchableOpacity 
          testID='changePhone' 
          accessibilityLabel='changePhone' 
          onPress={changePhoneAction}
        >
          <Text 
            style={phone ? Styles.phoneNumberText : Styles.noPhoneNumberText}
          >{phone ? LocaleFormatter.addSpacingToPhoneNumber(`+${phone}`) : 'Add Phone Number'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity 
          testID='changeEmail' 
          accessibilityLabel='changeEmail' 
          onPress={changeEmailAction}
        >
          <Text style={Styles.emailText}>{email}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

ProfileDetail.propTypes = {
  name: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  changePhoneAction: PropTypes.func.isRequired,
  changeEmailAction: PropTypes.func.isRequired,
}

export default ProfileDetail
