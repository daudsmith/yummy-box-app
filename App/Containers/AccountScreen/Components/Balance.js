import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import LocaleFormatter from '../../../Services/LocaleFormatter'
import { scale } from 'react-native-size-matters'

class Balance extends Component{
  render() {
    const {
      label,
      balance,
      styles,
    } = this.props
    return (
      <View style={{flexDirection: 'column', width: scale(114), marginRight: scale(12)}}>
        <Text style={styles.creditsText}>{label}</Text>
        <Text style={styles.balanceText}>{LocaleFormatter.numberToCurrency(balance)}</Text>
      </View>
    )
  }
}

Balance.propTypes = {
  balance: PropTypes.number.isRequired,
  styles: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
}

export default Balance
