import { Container, Content, Icon } from 'native-base'
import React, { Component } from 'react'
import { Text, TextInput, View } from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import { connect } from 'react-redux'
import AlertBox from '../Components/AlertBox'
import GenericErrorMessage from '../Components/GenericErrorMessage'
import NavigationBar from '../Components/NavigationBar'
import PrimaryButton from '../Components/PrimaryButton'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import Colors from '../Themes/Colors'
import Styles from './Styles/ChangePasswordScreenStyle'

class ChangePasswordScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      newPasswordIsNotMatch: false,
      showErrorMessage: false,
      errorMessage: null,
      success: null,
      loading: false,
    }
  }

  onPasswordChange (text) {
    const {confirmPassword} = this.state

    if (confirmPassword !== '') {
      if (confirmPassword === text) {
        this.setState({newPasswordIsNotMatch: false})
      } else {
        this.setState({newPasswordIsNotMatch: true})
      }
    }

    this.setState({newPassword: text})
  }

  checkPasswordConfirmation (text) {
    const {newPassword} = this.state

    if (newPassword === text) {
      this.setState({confirmPassword: text, newPasswordIsNotMatch: false})
    } else {
      this.setState({confirmPassword: text, newPasswordIsNotMatch: true})
    }
  }

  navigateBackToProfile () {
    this.props.clearChangePasswordSuccess()
    this.props.navigation.goBack()
  }

  handleSubmit () {
    const {oldPassword, newPassword, confirmPassword, newPasswordIsNotMatch} = this.state
    if (!newPasswordIsNotMatch) {
      this.setState({loading: true})
      this.props.changePassword(oldPassword, newPassword, confirmPassword)
    }
  }

  renderAlertModal () {
    const {changePasswordSuccess, changePasswordError} = this.props.customerAccount
    const showAlertModal = changePasswordSuccess !== null
    const text = changePasswordSuccess ? 'You\'re successfully change your password.' : changePasswordError
    const icon = changePasswordSuccess ? 'check' : 'close'
    const title = changePasswordSuccess ? 'Success!' : 'Failed!'
    const primaryAction = changePasswordSuccess ? () => this.navigateBackToProfile() : () => this.props.clearChangePasswordSuccess()

    return (
      <AlertBox
        primaryAction={primaryAction}
        primaryActionText='Done'
        dismiss={() => this.props.clearChangePasswordSuccess()}
        text={text}
        title={title}
        icon={icon}
        visible={showAlertModal}
      />
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const {oldPassword, newPassword, confirmPassword, newPasswordIsNotMatch} = this.state
    const marginTop = verticalScale(20)
    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title='Change Password'
          leftSideNavigation={this.navigationGoBack}
        />
        <Content style={{backgroundColor: 'white', paddingHorizontal: scale(21)}}>
          {this.state.showErrorMessage &&
            <View style={{marginTop: marginTop}}>
              <GenericErrorMessage error={this.state.errorMessage} close={() => this.setState({showErrorMessage: false})}/>
            </View>
          }
          <View style={{flex: 1, marginTop: marginTop, borderBottomWidth: 0.5, borderStyle: 'solid', borderBottomColor: oldPassword === '' ? Colors.lightGreyX : Colors.greyishBrownTwo}}>
            <Text style={Styles.label}>Old Password</Text>
            <TextInput
              autoCapitalize='none'
              onChangeText={(text) => this.setState({oldPassword: text})}
              underlineColorAndroid='transparent'
              secureTextEntry
              value={oldPassword}
              style={{marginTop: marginTop, padding: 0, color: Colors.greyishBrown,}}
            />
          </View>
          <View style={{flex: 1, marginTop: verticalScale(20), borderBottomWidth: 0.5, borderStyle: 'solid', borderBottomColor: newPassword === '' ? Colors.lightGreyX : Colors.greyishBrownTwo}}>
            <Text style={Styles.label}>New Password</Text>
            <TextInput
              autoCapitalize='none'
              onChangeText={(text) => this.onPasswordChange(text)}
              underlineColorAndroid='transparent'
              secureTextEntry
              value={newPassword}
              style={{marginTop: marginTop, padding: 0}}
            />
          </View>
          <View style={{flex: 1, marginTop: marginTop, borderBottomWidth: 0.5, borderStyle: 'solid', borderBottomColor: newPasswordIsNotMatch ? 'red' : confirmPassword === '' ? Colors.lightGreyX : Colors.greyishBrownTwo}}>
            <Text style={Styles.label}>Confirm Password</Text>
            <View style={{flexDirection: 'row', marginTop: marginTop}}>
              <TextInput
                autoCapitalize='none'
                onChangeText={(text) => this.checkPasswordConfirmation(text)}
                underlineColorAndroid='transparent'
                secureTextEntry
                value={confirmPassword}
                style={{flex: 9, padding: 0}}
              />
              <Icon name='md-close' style={[Styles.errorIcon, {color: newPasswordIsNotMatch ? 'red' : 'transparent'}]} />
            </View>
          </View>
          {newPasswordIsNotMatch &&
            <Text style={Styles.errorMessage}>Confirmation password is not match</Text>
          }
          <View style={{marginTop: verticalScale(23)}}>
            <PrimaryButton
              label='Save'
              onPressMethod={() => this.handleSubmit()}
              disabled={!this.props.isConnected}
              buttonStyle={!this.props.isConnected ? {backgroundColor: Colors.pinkishGrey} : {}}
            />
          </View>
          {this.renderAlertModal()}
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    customerAccount: state.customerAccount,
    isConnected: state.network.isConnected
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changePassword: (old_password, password, password_confirmation) => dispatch(CustomerAccountActions.changePassword(old_password, password, password_confirmation)),
    clearChangePasswordSuccess: () => dispatch(CustomerAccountActions.clearChangePasswordSuccess())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordScreen)
