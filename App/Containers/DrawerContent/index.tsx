import * as React from 'react'
import { FlatList, Image, Platform, Text, TouchableOpacity, View } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import crashlytics from '@react-native-firebase/crashlytics'
import {scale, verticalScale} from 'react-native-size-matters'
import AppConfig from '../../Config/AppConfig'
import styles from '../../Containers/Styles/DrawerContentStyles'
import icoMoonConfig from '../../Images/SvgIcon/selection.json'
import CustomerAccountActions from '../../Redux/CustomerAccountRedux'
import CustomerNotificationActions from '../../Redux/CustomerNotificationRedux'
import { Colors, Images } from '../../Themes'
import MarginTopIOS from '../../Themes/MarginTopIOS'
import NotLoginComponent from './Components/NotLogin'
import UserProfileComponent from './Components/UserProfile'
import { AppState } from '../../Redux/CreateStore'
import { User } from '../../Redux/LoginRedux'
import { SettingDataInterface } from '../../Redux/SettingRedux'
import {
  DrawerContentComponentProps,
  DrawerNavigatorItemsProps,
  NavigationDrawerProp,
} from 'react-navigation-drawer/lib/typescript/src/types'
import { NavigationStackScreenProps } from 'react-navigation-stack'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)
const renderedDrawerNavigation = [
  {
    routeName: 'HomeScreen',
    icon: 'home'
  },
  {
    routeName: 'MyMealPlanScreen',
    icon: 'calendar-orange'
  },
  {
    routeName: 'CustomerMyOrderListScreen',
    icon: 'my-order'
  },
  {
    routeName: 'NotificationListScreen',
    icon: 'notifications'
  },
  {
    routeName: 'ContactUs',
    icon: 'phone'
  }
]

export interface DrawerContentProps {
  navigation: NavigationDrawerProp,
  user: User,
  fetchingWallet: boolean,
  wallet: number,
  personalBalance: number,
  corporateBalance: number,
  isClient: boolean,
  unread: number,
  isConnected: boolean,
  setting: SettingDataInterface,
  getWallet: () => void,
  fetchUnreadNotifications: () => void,
}

class DrawerContent extends React.PureComponent<DrawerNavigatorItemsProps & NavigationStackScreenProps & DrawerContentComponentProps & DrawerContentProps> {
  willFocusSubscription

  state = {
    isFetched: false
  }

  componentDidMount() {
    if (this.props.isConnected) {
      this.fetchDrawerData()
    }
    this.willFocusSubscription = this.props.navigation.addListener('willFocus', this.fetchDrawerData)
  }

  componentWillUnmount() {
    if (this.willFocusSubscription) {
      this.willFocusSubscription.remove()
    }
  }

  componentDidUpdate(
    prevProps: Readonly<DrawerContentProps & DrawerNavigatorItemsProps & NavigationStackScreenProps & DrawerContentComponentProps>
  ): void {
    if (this.props.isConnected && !prevProps.isConnected) {
      this.fetchDrawerData()
    }

    if (this.props.navigation.state.isDrawerOpen
      && !this.props.fetchingWallet
      && !this.state.isFetched
    ) {
      this.fetchDrawerData()
    }

    if (!this.props.navigation.state.isDrawerOpen
      && this.state.isFetched
    ) {
      this.setState({
        isFetched: false
      })
    }
  }

  fetchDrawerData = () => {
    if (this.props.user !== null) {
      this.props.getWallet()
      this.props.fetchUnreadNotifications()
      this.setState({
        isFetched: true
      })
    }
  }

  navigateToLoginScreen = () => {
    const {navigation} = this.props
    navigation.navigate('Login')
  }

  navigateToRegisterScreen = () => {
    const {navigation} = this.props
    navigation.navigate('EmailPhoneVerificationScreen')
  }

  navigateToEditProfileScreen = () => {
    const {navigation} = this.props
    navigation.navigate('AccountScreen')
  }

  navigateToTopUpScreen = () => {
    const {navigation} = this.props
    navigation.navigate('TopUpScreen', { navigationParams: 'menu' })
  }

  render() {
    const {
      wallet,
      personalBalance,
      corporateBalance,
      unread,
      fetchingWallet,
      navigation,
      user,
      isClient,
      items,
    } = this.props
    let drawerItems: any = items
    let userFirstName = ''
    let userLastName = ''

    if (user !== null) {
      userFirstName = user.first_name === null ? '' : user.first_name
      userLastName = user.last_name === null ? '' : user.last_name
    }

    let photo = user && user.photo
    let fullName = `${userFirstName} ${userLastName}`
    if (fullName.length > 20) {
      fullName = `${fullName.substring(0, 15)}...`
    }

    const AppLogo = AppConfig.is_uat === '1' ? Images.YummyboxUAT : Images.YummyboxBeta
    const versionText = `${AppConfig.appEnvironment} version ${Platform.OS === 'android' ? AppConfig.AndroidAppVersion : AppConfig.iOSAppVersion} (build: ${AppConfig.patchVersion}#${AppConfig.buildRev || ''})`

    return (
      <View style={{ flex: 1, width: scale(275) }}>
        {Platform.OS === 'ios' ? <View style={MarginTopIOS.container} /> : <View />}
        {
          user
            // IF USER ALREADY LOGIN
            ? (
              <UserProfileComponent
                name={fullName}
                styles={styles}
                photo={!photo ? Images.YumboxAvatarDefault : { uri: photo }}
                fetchDrawerData={this.fetchDrawerData}
                fetchingWallet={fetchingWallet}
                wallet={wallet}
                personalBalance={personalBalance}
                corporateBalance={corporateBalance}
                isClient={isClient}
                navigateToEditProfileScreen={this.navigateToEditProfileScreen}
                navigateToTopUpScreen={this.navigateToTopUpScreen}
              />
            )
            // IF USER NOT LOGIN
            : (
              <NotLoginComponent
                navigateToLoginScreen={this.navigateToLoginScreen}
                navigateToRegisterScreen={this.navigateToRegisterScreen}
              />
            )
        }

        <View
          style={{
            marginTop: verticalScale(-10),
            backgroundColor: 'transparent',
            marginRight: scale(10)
          }}
        >
          <FlatList
            scrollEnabled={false}
            data={drawerItems}
            renderItem={({ item }) => {
              let isAllowed = renderedDrawerNavigation.find(allowed => allowed.routeName === item.routeName)
              if (isAllowed) {
                if (item.routeName === 'NotificationListScreen') {
                  return (
                    <TouchableOpacity
                      testID="NotificationListScreen"
                      accessibilityLabel="NotificationListScreen"
                      onPress={() => navigation.navigate(item.routeName)}
                      style={styles.menuNavigationButton}
                    >
                      <YumboxIcon
                        name={isAllowed.icon}
                        size={scale(18)}
                        color={Colors.bloodOrange}
                        style={{
                          left: scale(18),
                          flex: 1
                        }}
                      />
                      <Text
                        style={{
                          ...styles.navigationTextNotification,
                          flex: 10
                        }}
                      >
                        {
                          this.props.getLabel({
                            index: item.index,
                            focused: false,
                            tintColor: '',
                            route: item,
                          })
                        }
                      </Text>
                      {
                        unread > 0 &&
                        <Text
                          style={{
                            flex: 2,
                            color: Colors.pinkishGrey,
                            fontFamily: 'Rubik-Regular',
                            fontSize: scale(12),
                            textAlign: 'right',
                          }}
                        >
                          {unread}
                        </Text>
                      }
                    </TouchableOpacity>
                  )
                } else {
                  return (
                    <TouchableOpacity
                      testID={item.routeName}
                      accessibilityLabel={item.routeName}
                      onPress={() => navigation.navigate(item.routeName)}
                      style={styles.menuNavigationButton}
                    >
                      <YumboxIcon
                        name={isAllowed.icon}
                        size={scale(18)}
                        color={Colors.bloodOrange}
                        style={{ left: scale(18) }}
                      />
                      <Text style={styles.navigationText}>
                        {
                          this.props.getLabel({
                            index: item.index,
                            focused: false,
                            tintColor: '',
                            route: item,
                          })
                        }
                      </Text>
                    </TouchableOpacity>
                  )
                }
              } else {
                return null
              }
            }}
            keyExtractor={(item, index) => String(index)}
          />
        </View>

        <View style={styles.bottomBox}>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              alignContent: 'flex-end',
              marginBottom: verticalScale(20),
            }}
          >
            <View>
              <TouchableOpacity
                testID="crash-test"
                accessibilityLabel="crash-test"
                onPress={() => {
                  if (AppConfig.appEnvironment !== 'Release') {
                    crashlytics()
                      .crash()
                  }
                }}
              >
              <Image
                source={AppLogo}
                style={{
                  width: scale(105),
                  height: verticalScale(50),
                  top: verticalScale(10)
                }}
                resizeMode='contain'
              />
              </TouchableOpacity>
              <View>
                <Text
                  style={{
                    color: Colors.blueGrey,
                    fontSize: scale(10)
                  }}
                >
                  {versionText}
                </Text>
              </View>
            </View>
          </View>

        </View>
      </View>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    user: state.login.user,
    customerAccount: state.customerAccount,
    fetchingWallet: state.customerAccount.fetchingWallet,
    wallet: state.customerAccount.wallet,
    personalBalance: state.customerAccount.personalBalance,
    corporateBalance: state.customerAccount.corporateBalance,
    isClient: state.customerAccount.isClient,
    customerNotification: state.customerNotification,
    unread: state.customerNotification.unread,
    isConnected: state.network.isConnected,
    setting: state.setting.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchUnreadNotifications: () => dispatch(CustomerNotificationActions.fetchTotalUnreadNotifications()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent)
