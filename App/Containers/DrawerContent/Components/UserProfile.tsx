import React from 'react'
import { Image, Text, TouchableOpacity, View, ImageSourcePropType } from 'react-native'
import { Colors } from '../../../Themes'
import ProfileButtonComponent from './ProfileButton'
import YummyCreditsComponent from './YummyCredits'
import {DrawerStyleProps} from '../../Styles/DrawerContentStyles'
import {scale, verticalScale} from 'react-native-size-matters'

interface UserProfileProps {
  photo: ImageSourcePropType
  styles: DrawerStyleProps
  name: string
  fetchDrawerData: () => void
  wallet: number
  personalBalance: number
  corporateBalance: number
  fetchingWallet: boolean
  isClient: boolean
  navigateToEditProfileScreen: () => void
  navigateToTopUpScreen: () => void
}

const UserProfile: React.FC<UserProfileProps> = props => {
  const {
    name,
    photo,
    styles,
    wallet,
    personalBalance,
    corporateBalance,
    fetchingWallet,
    isClient,
    fetchDrawerData,
    navigateToEditProfileScreen,
    navigateToTopUpScreen,
  } = props
  return (
    <View style={styles.userLoginBox}>
      <View style={{flexDirection: 'row'}}>
        <View>
          <Image source={photo} style={styles.photoProfile} />
        </View>
        <View style={{flex: 1, flexDirection: 'column', marginLeft: scale(16), marginTop: verticalScale(12)}}>
          <Text style={styles.userName}>{name}</Text>

          <TouchableOpacity
            style={{marginTop: verticalScale(6)}}
            onPress={navigateToEditProfileScreen}
            testID="editProfileLink"
            accessibilityLabel="editProfileLink"
          >
            <Text style={{fontSize: scale(12), fontFamily: 'Rubik-Light', color: Colors.grennBlue}} >Edit My Profile</Text>
          </TouchableOpacity>
        </View>
      </View>

      <YummyCreditsComponent
        wallet={wallet}
        personalBalance={personalBalance}
        corporateBalance={corporateBalance}
        fetchingWallet={fetchingWallet}
        styles={styles}
        isClient={isClient}
      />

      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 1, flexDirection: 'column'}}>
          <ProfileButtonComponent
            fetchDrawerData={fetchDrawerData}
            navigateToTopUpScreen={navigateToTopUpScreen}
            styles={styles}
          />
        </View>
      </View>
    </View>
  )
}

export default UserProfile

