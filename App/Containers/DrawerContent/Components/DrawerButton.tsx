import React from 'react'
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Colors from '../../../Themes/Colors'
import {scale, verticalScale} from 'react-native-size-matters'

interface DrawerButtonStyleProps {
  button: object
  buttonWithIcon: object
  text: object
}

interface DrawerButtonProps {
  onPress?: () => void
  label: string
  iconName?: string
  iconColor?: string
  iconSize?: number
  buttonStyle?: object
  textStyle?: object
  testID?: string
  accessibilityLabel?: string
}

const Styles: DrawerButtonStyleProps = StyleSheet.create({
  button: {
    borderRadius: 4,
    borderColor: Colors.pinkishOrange,
    borderWidth: 1,
    width: scale(114),
    height: verticalScale(32),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.pinkishOrange,
    flexDirection: 'row',
  },
  buttonWithIcon: {
    flexDirection: 'row',
  },
  text: {
    fontFamily: 'Rubik-Regular',
    color: Colors.white,
    fontSize: scale(14),
    paddingVertical: verticalScale(6),
    paddingHorizontal: scale(8),
  }
})

const DrawerButton: React.FC<DrawerButtonProps> = ({
  onPress,
  label,
  buttonStyle,
  iconName,
  iconSize = scale(14),
  iconColor = Colors.white,
  textStyle,
  testID,
  accessibilityLabel,
}) => {
  const {
    button,
    buttonWithIcon,
    text,
  } = Styles

  let buttonStyling = {
    ...button,
    ...buttonStyle,
  }
  if (iconName) {
    buttonStyling = {
      ...buttonStyling,
      ...buttonWithIcon,
    }
  }
  let textStyling = {
    ...text,
    ...textStyle,
  }

  return (
    <TouchableOpacity
      testID={testID}
      accessibilityLabel={accessibilityLabel}
      onPress={onPress}
    >
      <View
        style={buttonStyling}
      >
        {iconName &&(
          <Icon name={iconName} size={Number(iconSize)} color={iconColor} />
        )}
        <Text
          style={textStyling}
        >
          {label}
        </Text>
      </View>
    </TouchableOpacity>
  )
}

export default DrawerButton
