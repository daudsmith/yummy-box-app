import React from 'react'
import {Text, TouchableOpacity, View} from 'react-native'
import styles from '../../Styles/DrawerContentStyles'
import {scale, verticalScale} from 'react-native-size-matters'

interface NotLoginProps {
  navigateToLoginScreen: () => void
  navigateToRegisterScreen: () => void
}

const NotLogin: React.FC<NotLoginProps> = props => {
  const {
    navigateToLoginScreen,
    navigateToRegisterScreen,
  } = props
  return (
    <View style={styles.userNotLoginBox}>
      <Text style={styles.welcomeText}>Welcome to Yummybox</Text>
      <TouchableOpacity
        testID="registerNow"
        accessibilityLabel="registerNow"
        style={{paddingTop: verticalScale(19)}}
        onPress={navigateToRegisterScreen}
      >
        <Text style={styles.registerNow}>Register Now</Text>
      </TouchableOpacity>

      <View style={styles.memberBox}>
        <Text style={styles.alreadyMember}>Already a Member?</Text>
        <TouchableOpacity
          testID="login"
          accessibilityLabel="login"
          style={{paddingLeft: scale(6)}}
          onPress={navigateToLoginScreen}
        >
          <Text style={styles.signIn}>Log In</Text>
        </TouchableOpacity>
      </View>
    </View>
  )

}

export default NotLogin
