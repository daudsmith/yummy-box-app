import React from 'react'
import { Text, View } from 'react-native'
import LocaleFormatter from '../../../Services/LocaleFormatter'
import {DrawerStyleProps} from '../../Styles/DrawerContentStyles'
import {scale} from 'react-native-size-matters'

interface BalanceProps {
  balance: number
  styles: DrawerStyleProps
  label: string
}

const Balance: React.FC<BalanceProps> = props => {
  const {
    label,
    balance,
    styles,
  } = props
  return (
    <View style={{flexDirection: 'column', width: scale(114), marginRight: scale(12)}}>
      <Text style={styles.creditsText}>{label}</Text>
      <Text style={styles.balanceText}>{LocaleFormatter.numberToCurrency(balance)}</Text>
    </View>
  )
}

export default Balance
