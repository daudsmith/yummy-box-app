import React  from 'react'
import { ActivityIndicator, Text, View } from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../../Images/SvgIcon/selection.json'
import LocaleFormatter from '../../../Services/LocaleFormatter'
import { Colors } from '../../../Themes'
import BalanceComponent from './Balance'
import {DrawerStyleProps} from '../../Styles/DrawerContentStyles'
import {scale, verticalScale} from 'react-native-size-matters'

const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

interface YummyCreditsProps {
  fetchingWallet: boolean
  isClient: boolean
  wallet: number
  personalBalance?: number
  corporateBalance?: number
  styles: DrawerStyleProps
}

const YummyCredits: React.FC<YummyCreditsProps> = props => {
    const {
      fetchingWallet,
      isClient,
      wallet,
      styles,
      personalBalance,
      corporateBalance,
    } = props
    return (
      <View style={styles.yummyCreditsContainer}>
        <View>
          <Text style={styles.creditsText}>Yummycredits</Text>
        </View>
        <View style={{ flexDirection: 'row', marginBottom: verticalScale(16) }}>
          <YumboxIcon name='payment-yumCredits' size={scale(16)} color={Colors.pinkishOrange}/>
          {fetchingWallet ? (
            <ActivityIndicator size={'small'} style={styles.activityIndicator}/>
          ) : (
            <Text style={styles.currencyText}>{LocaleFormatter.numberToCurrency(wallet)}</Text>
          )}
        </View>
        {!fetchingWallet && isClient && (
          <View style={{ flexDirection: 'row', marginBottom: verticalScale(16) }}>
            <BalanceComponent
              label={'Personal Balance'}
              balance={personalBalance}
              styles={styles}
            />
            <BalanceComponent
              label={'Corporate Balance'}
              balance={corporateBalance}
              styles={styles}
            />
          </View>
        )}
      </View>
    )
}

export default YummyCredits
