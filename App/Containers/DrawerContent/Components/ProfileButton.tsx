import React  from 'react'
import { View } from 'react-native'
import Colors from '../../../Themes/Colors'
import DrawerButton from './DrawerButton'
import {DrawerStyleProps} from '../../Styles/DrawerContentStyles'
import {scale} from 'react-native-size-matters'

interface ProfileButtonProps {
  fetchDrawerData: () => void
  styles?: DrawerStyleProps
  navigateToEditProfileScreen?: () => void
  navigateToTopUpScreen: () => void
}

const ProfileButton: React.FC<ProfileButtonProps> = props => {
    const {
      styles,
      fetchDrawerData,
      navigateToTopUpScreen,
    } = props
    return (
      <View style={styles.userButtonBox}>
        <View style={{ flexDirection: 'row' }}>
          <DrawerButton
            testID="topUpButton"
            accessibilityLabel="topUpButton"
            label={'Top-up'}
            onPress={navigateToTopUpScreen}
          />

          <View style={{width: scale(12)}}/>

          <DrawerButton
            testID="refreshButton"
            accessibilityLabel="refreshButton"
            label={'Refresh'}
            iconName={'refresh'}
            iconColor={Colors.pinkishOrange}
            onPress={fetchDrawerData}
            buttonStyle={{
              backgroundColor: Colors.white,
              borderColor: Colors.pinkishOrange,
            }}
            textStyle={{
              color: Colors.pinkishOrange,
            }}
          />
        </View>
      </View>
    )
}

export default ProfileButton
