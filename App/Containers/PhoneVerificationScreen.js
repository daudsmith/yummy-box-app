import React, { Component } from 'react'
import { View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import {
  SwitchActions,
} from 'react-navigation'
import { connect } from 'react-redux'
import {withNavigation} from 'react-navigation'
import AuthBackgroundView from '../Components/Authentication/AuthBackgroundView'
import FlexButton from '../Components/Common/FlexButton'
import PopupBox from '../Components/Common/PopupBox'
import PhoneNumberTextInput from '../Components/PhoneNumberTextInput'
import RegisterActions from '../Redux/RegisterRedux'
import { Colors } from '../Themes'
import Styles from './Styles/PhoneVerificationScreenStyle'


class PhoneVerificationScreen extends Component {
  constructor (props) {
    super(props)
    const customer = typeof this.props.customer !== 'undefined' ? this.props.customer : this.props.navigation.state.params.customer
    this.state = {
      customer: customer,
      countingDownOtp: false,
      countDownSeconds: 60,
      loading: this.props.registration.verifyingOtpState,
      registerRetry: 0,
      maxRetry: 5,
      registering: false,
      sendOtp: true,
      errorMessage: '',
      showAlertModal: false,
      alertMessage: ''
    }
  }

  componentDidMount () {
    this.props.resetRegistrationErrorMessage()

    this.interval = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    const {verifyOtpError, registrationError} = newProps.registration
    const errorMessage = verifyOtpError !== null ? verifyOtpError : registrationError !== null ? registrationError : ''
    this.setState({
      registering: newProps.registration.registering,
      loading: newProps.registration.verifyingOtpState,
      errorMessage
    })

    if (newProps.login.token !== null) {
      this.props.navigation.dispatch(
        SwitchActions.jumpTo({
          routeName: 'NavigationDrawer'
        })
      )

      if (typeof this.props.dismissModal !== 'undefined') {
        setTimeout(() => this.props.dismissModal(), 1000)
      }
    }
  }

  tick () {
    const {countDownSeconds, countingDownOtp} = this.state
    if (countingDownOtp) {
      if (countDownSeconds === 0) {
        this.setState({countingDownOtp: false})
        return
      }
      this.setState({
        countDownSeconds: countDownSeconds - 1
      })
    }
  }

  renderPhoneNumberInput () {
    return (
      <View>
        <PhoneNumberTextInput
          setRef={(ref) => this.phone = ref}
          onChange={() => {}}
        />
        <View style={Styles.buttonContainer}>
          <FlexButton
            text='Submit'
            disableShadow
            buttonStyle={{backgroundColor: Colors.bloodOrange}}
            textStyle={{color: 'white'}}
            onPress={() => this.handleVerifyPress()}
          />
        </View>
      </View>
    )
  }

  handleSubmitPress () {
    this.setState({
      loading: true
    })
    const {customer} = this.state
    const isPhoneNumberEmpty = customer.phone === null

    if (!isPhoneNumberEmpty) {
      const socialLoginType = typeof this.props.socialLoginType !== 'undefined' ? this.props.socialLoginType : this.props.navigation.state.params.socialLoginType
      const firstName = customer.firstName || customer.first_name
      const lastName =  customer.lastName || customer.last_name
      const customerInfo = {
        name: `${firstName} ${lastName}`,
        salutation: customer.salutation,
        first_name: firstName,
        last_name: lastName,
        email: customer.email,
        phone: customer.phone,
        password: customer.password,
        password_confirmation: customer.confirmPassword,
        social_login_type: socialLoginType,
        social_login_id: customer.social_id,
        facebook_credentials: customer.facebookCredentials,
        countryCode: customer.countryCode,
      }
      this.props.register(customerInfo)
    }
  }

  handleVerifyPress () {
    let {customer} = this.state
    const isValid = this.phone.isValidNumber()
    if (isValid) {
      customer.phone = this.phone.getValue()
      customer.countryCode = this.phone.getDialCode()
      this.setState({
        customer
      }, this.handleSubmitPress)
    } else {
      this.setState({showErrorModal: true, errorMessage: 'Please input a valid phone number'})
    }
  }

  render () {
    const {registering, loading, errorMessage, showAlertModal, alertMessage} = this.state
    const showErrorModal = errorMessage !== ''
    const showedModally = typeof this.props.dismiss !== 'undefined'
    const boxSubtext = 'Please input your mobile number below to complete your registration'

    return (
      <AuthBackgroundView
        boxTitle='Phone Verification'
        boxSubtext={boxSubtext}
        showedModally={showedModally}
        dismiss={() => this.props.dismiss()}
        navigation={this.props.navigation}
      >
        <View style={{marginTop: 18}}>
          {this.renderPhoneNumberInput()}
        </View>

        <Spinner visible={loading} textContent='Registering' textStyle={{color: 'white'}} />
        <Spinner visible={registering} textContent='Registering' textStyle={{color: 'white'}} />
        <PopupBox
          visible={showErrorModal}
          content={errorMessage}
          contentButton={{
            text: 'Close',
            onPress: () => {
              this.setState({errorMessage: ''})
            }}}
        />
        <PopupBox
          visible={showAlertModal}
          content={alertMessage}
          contentButton={{
            text: 'Close',
            onPress: () => {
              this.setState({
                showAlertModal: false,
                alertMessage: ''
              })
            }}}
        />
      </AuthBackgroundView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    login: state.login,
    registration: state.register,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    register: (customer) => dispatch(RegisterActions.registerCustomer(customer)),
    verifyOtp: (phone, otp, countryCode) => dispatch(RegisterActions.verifyOtp(phone, otp, countryCode)),
    resetRegistrationErrorMessage: () => dispatch(RegisterActions.resetRegistrationErrorMessage())
  }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(PhoneVerificationScreen))
