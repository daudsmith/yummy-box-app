import * as React from 'react'
import moment, { Moment } from 'moment'
import {
  Content,
  Container,
  Footer,
  Toast,
} from 'native-base'
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  PanResponder,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  PanResponderInstance,
} from 'react-native'
import * as Animatable from 'react-native-animatable'
import Image from 'react-native-image-progress'
import Spinner from 'react-native-loading-spinner-overlay'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import Icon from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import CartQuantitySpinner from '../Components/CartQuantitySpinner'
import NavigationBar from '../Components/NavigationBar'
import ShoppingCartAlertModal from '../Components/ShoppingCartAlertModal'
import ShoppingCartButton from '../Components/ShoppingCartButton'
import AddToCartButton, { cartCandidateInterface } from '../Components/AddToCartButton'
import CartActions, {
  BaseCart,
  Delivery,
} from '../Redux/V2/CartRedux'
import MealsActions, {
  InitMealsState,
  Meal,
  Stock,
} from '../Redux/MealsRedux'
import LocaleFormatter from '../Services/LocaleFormatter'
import CartService from '../Services/MealCartService'
import { Colors } from '../Themes'
import styles from './Styles/MealDetailScreenStyle'
import { AppState } from '../Redux/CreateStore'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { ImmutableObject } from 'seamless-immutable'
import LogEventService from '../Services/LogEventService'
import StockService from '../Services/StockService'
import {
  itemResponse,
  responseData,
} from '../Services/V2/ApiCart'
import SalesOrderService from '../Services/SalesOrderService'
import { baseLastUsed } from '../Redux/V2/LastUsedRedux'

const themesColor = '#ff5100'
const greenColor = Colors.green
const greyColor = Colors.greyishBrown
const lightGray = 'rgb(190, 190, 190)'
const { width } = Dimensions.get('window')
const swipeThreshold = 0.15 * width
const imageSize = width - 35

export interface MealDetailScreenProps {
  navigation: NavigationDrawerProp
  cart: BaseCart,
  isLoggedIn: boolean,
  meals: ImmutableObject<InitMealsState>,
  stock: ImmutableObject<Stock>,
  isConnected: boolean,
  fetchMealDetails: (id: number, date: string) => void,
  addToCart: (cart: responseData) => void,
  resetCart: () => void,
  removeFromCart: (item: itemResponse | Meal, date: Moment) => void,
  loading: (status: boolean) => void,
  lastUsed: baseLastUsed,
}

export interface MealDetailScreenState {
  params?: any,
  selectedDate?: any,
  datePickerVisible?: boolean,
  meals?: Meal[],
  selectedMealIndex?: number,
  slideAnimation?: string,
  cart?: Delivery[],
  emptyCart?: boolean,
  fetchingDetail?: boolean,
  is_available?: boolean,
  enableScroll?: boolean,
  activateTabs?: string,
  textNutritions?: string,
  borderNutritions?: string,
  borderWidthNutritions?: number,
  textIngredients?: string,
  borderIngredients?: string,
  borderWidthIngredients?: number,
  prevMealButtonDisabled?: boolean,
  nextMealButtonDisabled?: boolean,
  panResponder: PanResponderInstance,
  showCombinationAlertModal?: boolean,
  showErrorModal?: boolean,
  category: string,
  exclusive: boolean,
  cartCandidate?: cartCandidateInterface,
}

const getAllMeals = (meals) => {
  // GET ALL MEALS DATA FROM REDUCER MEALS
  let allMeals = []
  if (meals.meals !== null) {
    meals.meals.map(meal => {
      meal.items.data.map(item => {
        allMeals.push(item)
        return item
      })
    })
  }
  return allMeals
}

class MealDetailScreen extends React.PureComponent<MealDetailScreenProps, MealDetailScreenState> {
  imageRefs
  view
  position
  scrollX

  constructor(props) {
    super(props)
    this.initializePositionAndScrollX()
    const panResponder = this.initializePanResponder(this.position)

    this.state = {
      datePickerVisible: false,
      panResponder: panResponder,
      slideAnimation: '',
      fetchingDetail: false,
      is_available: true,
      enableScroll: true,
      activateTabs: 'ingredients',
      textNutritions: greyColor,
      borderNutritions: 'none',
      borderWidthNutritions: 1,
      textIngredients: greenColor,
      borderIngredients: 'flex',
      borderWidthIngredients: 0,
      showCombinationAlertModal: false,
      showErrorModal: false,
      category: '',
      exclusive: false,
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { params } = props.navigation.state
    const {
      meals,
      selectedMealIndex,
      category,
      exclusive,
      selectedDate,
    } = params
    if (selectedMealIndex !== state.selectedMealIndex) {
      return {
        params: params,
        selectedDate: selectedDate
          ? moment(selectedDate)
          : moment()
            .add(1, 'days'),
        meals: typeof meals !== 'undefined'
          ? meals
          : getAllMeals(props.meals),
        selectedMealIndex: typeof state.selectedMealIndex === 'undefined'
          ? selectedMealIndex
          : state.selectedMealIndex,
        cart: props.cart.cartItems,
        emptyCart: props.cart.isEmpty,
        prevMealButtonDisabled: state.prevMealButtonDisabled === undefined
          ? (selectedMealIndex === 0)
          : state.prevMealButtonDisabled,
        nextMealButtonDisabled: state.nextMealButtonDisabled === undefined
          ? (selectedMealIndex === meals.length - 1)
          : state.nextMealButtonDisabled,
        slideAnimation: state.slideAnimation,
        category,
        exclusive,
      }
    }
    return null
  }

  componentDidMount() {
    if (this.props.isConnected) {
      this.getMealDetailData()
    }
  }

  componentDidUpdate(prevProps: Readonly<MealDetailScreenProps>): void {
    const { fetchingDetail } = this.props.meals
    if (prevProps.meals.fetchingDetail !== fetchingDetail) {
      this.setState({
        fetchingDetail,
      })

    }
    const {
      selectedMealIndex,
      meals,
      category,
      exclusive,
    } = this.props.navigation.state.params

    if (selectedMealIndex !== prevProps.navigation.state.params.selectedMealIndex) {
      this.setState({
        selectedMealIndex,
        prevMealButtonDisabled: (selectedMealIndex === 0),
        nextMealButtonDisabled: (selectedMealIndex === (meals.length - 1)),
        category,
        exclusive,
      })
    }

    if (this.props.isConnected && !prevProps.isConnected) {
      this.getMealDetailData()
    }
  }

  handleViewRef = ref => this.view = ref

  getMealDetailData = () => {
    const {
      meals,
      selectedMealIndex,
      selectedDate,
    } = this.state
    this.props.fetchMealDetails(
      meals[selectedMealIndex].id,
      selectedDate.format('YYYY-MM-DD'),
    )
  }

  initializePositionAndScrollX = (): void => {
    this.position = new Animated.ValueXY()
    this.scrollX = new Animated.Value(0)
  }

  initializePanResponder = (position): PanResponderInstance => {
    return PanResponder.create({
      onStartShouldSetPanResponder: () => {
        this.setState({ enableScroll: false })
        return true
      },
      onPanResponderMove: (event, gesture) => {
        position.setValue({
          x: gesture.dx,
          y: 0,
        })
      },
      onPanResponderTerminate: (event, gesture) => {
        this.swipeToNextOrPrevious(gesture.dx)
        this.resetPosition()
      },
      onPanResponderRelease: (event, gesture) => {
        this.swipeToNextOrPrevious(gesture.dx)
        this.resetPosition()
      },
    })
  }

  resetPosition = () => {
    Animated.spring(this.position, {
      useNativeDriver: false,
      toValue: {
        x: 0,
        y: 0
      }
    }).start()
    this.setState({
      enableScroll: true,
    })
  }

  isItemInCart = (item) => {
    const { cartItems } = this.props.cart
    const { selectedDate } = this.state
    if (item === null) {
      return false
    }
    return CartService.isItemExistInCart(
      cartItems,
      item,
      selectedDate,
    )
  }

  _changeTabs = (changeTo) => {
    this.state.activateTabs !== changeTo
      ? changeTo === 'nutritions'
      ?
      this.setState({
        activateTabs: 'nutritions',
        borderNutritions: 'flex',
        borderIngredients: 'none',
        borderWidthNutritions: 0,
        borderWidthIngredients: 1,
        textNutritions: greenColor,
        textIngredients: greyColor,
      })
      : this.setState({
        activateTabs: 'ingredients',
        borderNutritions: 'none',
        borderIngredients: 'flex',
        borderWidthNutritions: 1,
        borderWidthIngredients: 0,
        textNutritions: greyColor,
        textIngredients: greenColor,
      })
      : null
  }

  swipeToNextOrPrevious = (swipeDistance) => {
    const { selectedMealIndex, meals } = this.state
    if (swipeDistance > swipeThreshold && selectedMealIndex !== 0) {
      this.togglePreviousOrNextFood(false)
    } else if (swipeDistance < -swipeThreshold && selectedMealIndex !== meals.length - 1) {
      this.togglePreviousOrNextFood(true)
    }
  }

  togglePreviousOrNextFood = (isNextMeal) => {
    const { selectedMealIndex, meals, selectedDate } = this.state
    const newSelectedMealIndex = isNextMeal
      ? selectedMealIndex + 1
      : selectedMealIndex - 1
    const newNextButtonDisabled = (newSelectedMealIndex === (meals.length - 1))
    const newPrevButtonDisabled = (newSelectedMealIndex === 0)
    const newSlideAnimation = isNextMeal
      ? 'slideInLeft'
      : 'slideInRight'
    this.setState({
      slideAnimation: '',
    }, () => {
      this.setState({
        prevMealButtonDisabled: newPrevButtonDisabled,
        nextMealButtonDisabled: newNextButtonDisabled,
        selectedMealIndex: newSelectedMealIndex,
        slideAnimation: newSlideAnimation,
      })
    })
    this.props.fetchMealDetails(meals[newSelectedMealIndex].id, selectedDate.format('YYYY-MM-DD'))
    this.imageRefs.scrollTo({
      x: 0,
      y: 0,
      animated: false,
    })
    this._changeTabs('ingredients')
  }

  renderImageSlider = () => {
    let position = Animated.divide(this.scrollX, width)
    const { selectedMealIndex, meals } = this.state
    const selectedMealImage = meals[selectedMealIndex].images.data
    return (
      <View style={{
        height: imageSize,
        width: imageSize,
      }}>
        <ScrollView
          ref={imageRefs => this.imageRefs = imageRefs}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: this.scrollX } } }], {
              useNativeDriver: false
            }
          )}
          scrollEventThrottle={16}
        >
          {
            selectedMealImage.map((image, index) => {
              return (
                <Image
                  key={index}
                  source={{
                    uri: image.thumbs.medium_thumb
                      .replace('_medium_thumb', ''),
                    cache: 'force-cache',
                  }}
                  style={{
                    width: imageSize,
                    height: imageSize,
                  }}
                  borderRadius={3}
                  resizeMode='contain'
                  prefetch={image.thumbs.medium_thumb}
                />
              )
            })
          }
        </ScrollView>
        <View style={styles.dotContainer}>
          {
            selectedMealImage.map((_, i) => {
              let backgroundColor = position.interpolate({
                inputRange: [
                  i - 0.50000000001,
                  i - 0.5,
                  i,
                  i + 0.5,
                  i + 0.50000000001,
                ],
                outputRange: [
                  'transparent',
                  'white',
                  'white',
                  'white',
                  'transparent',
                ],
                extrapolate: 'clamp',
              })
              return (
                <Animated.View
                  key={i}
                  style={[
                    styles.dot,
                    { backgroundColor: backgroundColor },
                  ]}
                />
              )
            })
          }
        </View>
      </View>
    )
  }

  renderTagFilterString = (tags) => {
    return tags.map((tag, index) => {
      if (index !== tags.length - 1) {
        return (
          <Text key={index} style={styles.foodTagText}>
            {tag.name} {'\u2022'}
          </Text>
        )
      } else {
        return (
          <Text key={index} style={styles.foodTagText}>
            {tag.name}
          </Text>
        )
      }
    })
  }

  renderFoodNameSlider = (item) => {
    const {
      prevMealButtonDisabled,
      nextMealButtonDisabled,
      slideAnimation,
      panResponder,
    } = this.state
    const previousButtonColor = prevMealButtonDisabled
      ? 'transparent'
      : Colors.bloodOrange
    const nextButtonColor = nextMealButtonDisabled
      ? 'transparent'
      : Colors.bloodOrange
    const index = this.state.selectedMealIndex
    return (
      <Animated.View style={[this.position.getLayout()]}>
        <Animatable.View
          {...panResponder.panHandlers}
          ref={this.handleViewRef}
          animation={slideAnimation}
          duration={300}
        >
          <View style={styles.foodNameContainer}>
            <TouchableOpacity
              testID={`previousMealButton_${index}`}
              style={styles.leftArrowButton}
              disabled={prevMealButtonDisabled}
              onPress={() => this.togglePreviousOrNextFood(false)}
            >
              <Icon
                name='caret-left'
                size={scale(26)}
                color={previousButtonColor}
              />
            </TouchableOpacity>
            <View style={[
              styles.foodNameBox,
              { flex: 10 },
            ]}>
              <Text style={styles.foodNameText}>
                {item.name}
              </Text>
              <View style={{ flexDirection: 'row' }}>
                {this.renderTagFilterString(item.tags.data)}
              </View>
            </View>
            <TouchableOpacity
              testID={`nextMealButton_${index}`}
              style={styles.rightArrowButton}
              disabled={nextMealButtonDisabled}
              onPress={() => this.togglePreviousOrNextFood(true)}
            >
              <Icon name='caret-right' size={scale(26)} color={nextButtonColor} />
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </Animated.View>
    )
  }

  renderMealDescription = (selectedMeal) => {
    return (
      <View style={styles.mealDescriptionContainer}>
        {selectedMeal.dinner !== '' && (
          <View style={styles.noDinnerContainer}>
            <Text style={styles.noDinnerText}>{selectedMeal.dinner}</Text>
          </View>
        )}
        <Text style={styles.descriptionText}>{selectedMeal.description}</Text>
      </View>
    )
  }

  logEvent = (item) => {
    let logData = {
      currency: 'IDR',
      item_price: item.sale_price,
      item_id: item.id,
      item_name: item.name,
      item_category: 'item',
    }
    LogEventService.logEvent('add_to_cart', logData, ['default'])
  }

  addButton = (selectedMeal, selectedDate, mealIndex) => {
    const meal = { ...selectedMeal }
    return (
      <AddToCartButton
        testIDSuffix={`${mealIndex}`}
        handleAddToCartButtonPress={() =>
          this.incrementHandler(meal, selectedDate, 1)}
      />
    )
  }

  cardQuantityButton = (cartItem, selectedDate, availability, inCartQty) => {
    const {
      exclusive,
    } = this.state
    return (
      <CartQuantitySpinner
        item={cartItem}
        date={selectedDate}
        handleIncreasePress={this.incrementHandler}
        handleDecreasePress={this.decreaseHandler}
        max={availability}
        inCartQty={inCartQty}
        toggleAlertBox={this.onToggleAlertBox}
        showErrorModal={this.state.showErrorModal}
        exclusive={exclusive}
      />
    )
  }

  onToggleAlertBox = () => {
    this.setState({ showErrorModal: !this.state.showErrorModal })
  }

  onPressCompleteOrder = () => {
    const { isLoggedIn } = this.props
    if (isLoggedIn) {
      if (this.props.cart.type === 'subscription') {
        this.props.navigation.navigate('SubscriptionScheduleScreen')
      } else {
        this.props.navigation.navigate('SingleOrderCheckoutScreen')
      }
    } else {
      this.props.navigation.navigate('LoginLandingScreen')
    }
  }

  onPressClearCart = () => {
    this.props.resetCart()
    setTimeout(() => {
      if (this.state.cartCandidate) {
        const {
          cart,
          loading,
          resetCart,
          addToCart,
          lastUsed,
        } = this.props

        const {
          item,
          date,
          quantity,
        } = this.state.cartCandidate

        SalesOrderService.addToCart(
          cart,
          item,
          date,
          quantity,
          lastUsed.code,
          loading,
          resetCart,
          addToCart,
        )

        this.setState({
          cartCandidate: null,
        })
      }
    }, 100)
  }

  onPressShoppingCart = () => {
    const { navigation, isLoggedIn, cart } = this.props

    if (isLoggedIn) {
      if (cart.type !== 'subscription') {
        navigation.navigate({
          routeName: 'SingleOrderCheckoutScreen',
          key: 'SingleOrderCheckoutScreen',
        })
      } else {
        navigation.navigate('SubscriptionScheduleScreen')
      }
    } else {
      navigation.navigate('LoginLandingScreen')
    }
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  incrementHandler = async (
    item: Meal,
    date: Moment,
    quantity: number,
  ) => {
    const {
      cart,
      loading,
      resetCart,
      addToCart,
      isLoggedIn,
      navigation,
      lastUsed,
    } = this.props

    if (!isLoggedIn) {
      navigation.navigate('Login')
    } else {

    
      if (SalesOrderService.isCategoryExclusive(cart, item)) {
        return this.setState({
          showCombinationAlertModal: true,
          cartCandidate: {
            item: item,
            quantity: quantity,
            date: date,
          },
        })
      }
      if (SalesOrderService.isCategoryQtyMax(cart, date, 3)) {
        return Toast.show({
          text: `Max. order qty for ${cart.category} is 3 items per delivery date.`,
          buttonText: '',
          duration: 3000,
        })
      }
      if (!SalesOrderService.isExclusive(cart, item)
        && SalesOrderService.isSameType(cart, 'item')
      ) {
        if (!cart.fetching) {
          const itemAvailability = await SalesOrderService.getItemAvailability(item.id, date, this.props.lastUsed.code, quantity)

          if (!itemAvailability) {
            Toast.show({
              text: 'Someone else has just bought our last stock for this location',
              buttonText: '',
              duration: 3000,
            })
          }
          else {

            SalesOrderService.addToCart(
              cart,
              item,
              date,
              quantity,
              lastUsed.code,
              loading,
              resetCart,
              addToCart,
            )
          }
        }
      } else {
        this.setState({
          showCombinationAlertModal: true,
          cartCandidate: {
            item: item,
            quantity: quantity,
            date: date,
          },
        })
      }
    }
    // this.logEvent(item, quantity, item.category)
  }

  decreaseHandler = (
    item: Meal,
    date: Moment,
    quantity: number,
  ) => {
    const {
      cart,
      loading,
      resetCart,
      addToCart,
      lastUsed,
    } = this.props
    if (!cart.fetching) {
      SalesOrderService.addToCart(
        cart,
        item,
        date,
        quantity,
        lastUsed.code,
        loading,
        resetCart,
        addToCart,
      )
    }
  }

  render() {
    const {
      showCombinationAlertModal,
      slideAnimation,
      meals,
      selectedMealIndex,
      selectedDate,
      borderIngredients,
      borderNutritions,
      borderWidthIngredients,
      borderWidthNutritions,
      textNutritions,
      textIngredients,
      activateTabs,
      fetchingDetail,
      enableScroll,
    } = this.state
    const {
      isLoggedIn,
      cart,
    } = this.props
    const selectedMeal = meals[selectedMealIndex]

    let cartItem = CartService.getCartItem(
      cart.cartItems,
      selectedMeal,
      selectedDate.format('YYYY-MM-DD'),
    )

    let availability = StockService.getItemStock(
      selectedMeal.id,
      this.props.stock.main,
    )
    if (!availability) {
      availability = StockService.getItemStock(
        selectedMeal.id,
        this.props.stock.promoted,
      )
    }

    let inCartQty = CartService.getItemQuantity(
      this.props.cart.cartItems,
      selectedMeal.id,
      selectedDate
    )
    let available = availability - inCartQty > 0

    const isMealInCart = this.isItemInCart(selectedMeal)
    const titleDate = selectedDate.format('ddd, D MMM YYYY')
    const mealIndex = this.state.selectedMealIndex

    return (
      <Container testID='MealDetailScreen' style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
        <NavigationBar
          leftSide='back'
          title={titleDate}
          leftSideNavigation={this.navigationGoBack}
          currentScreen='MealDetailScreen'
        />
        <Content
          contentContainerStyle={styles.scrollViewContainer}
          scrollEnabled={enableScroll}
        >
          {selectedMeal === null
            ?
            <Spinner visible color={themesColor} />
            :
            <View>
              {this.renderFoodNameSlider(selectedMeal)}
              <Animatable.View
                ref={this.handleViewRef}
                animation={slideAnimation}
                duration={300}
              >
                <View style={{ alignItems: 'center' }}>
                  {this.renderImageSlider()}
                </View>
                <View style={styles.priceAndQuantityContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.priceText}>
                      {LocaleFormatter.numberToCurrency(selectedMeal.sale_price)}
                    </Text>
                    {selectedMeal.sale_price < selectedMeal.base_price && (
                      <Text style={styles.basePriceText}>
                        {LocaleFormatter.numberToCurrency(selectedMeal.base_price)}
                      </Text>
                    )}
                  </View>
                  {fetchingDetail
                    ? (
                      <ActivityIndicator />
                    )
                    :
                    (
                      !isMealInCart
                        ? (
                          !available
                            ?
                            <Text style={[
                              styles.priceText,
                              { fontSize: 14 },
                            ]}>Sold Out</Text>
                            :
                            <View style={{ height: 28 }}>
                              {this.addButton(selectedMeal, selectedDate, mealIndex)}
                            </View>
                        )
                        : (
                          this.cardQuantityButton(cartItem, selectedDate, availability, inCartQty)
                        )
                    )
                  }
                </View>

                {this.renderMealDescription(selectedMeal)}

                <View style={styles.ingredientsAndNutritionContainer}>

                  {/* BUTTON INGRIDIENT & NUTRITIONS */}
                  <View style={styles.ingridientAndNutritionsButtonBox}>
                    <TouchableOpacity
                      onPress={() => this._changeTabs('ingredients')}
                      style={[
                        styles.ingridientButton,
                        {
                          borderBottomWidth: borderWidthIngredients,
                          borderRightWidth: borderWidthIngredients,
                          borderColor: lightGray,
                        },
                      ]}
                    >
                      <Text style={[
                        styles.ingridientsText,
                        { color: textIngredients },
                      ]}>Ingredients</Text>
                      <View
                        style={[
                          styles.ingridientsBorder,
                          { backgroundColor: greenColor },
                          borderIngredients === 'flex'
                            ? borderStyle.borderDisplayFlex
                            : borderStyle.borderDisplayNone,
                        ]}
                      />
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => this._changeTabs('nutritions')}
                      style={[
                        styles.ingridientButton,
                        {
                          borderBottomWidth: borderWidthNutritions,
                          borderLeftWidth: borderWidthNutritions,
                          borderColor: 'rgb(190, 190, 190)',
                        },
                      ]}
                    >
                      <Text
                        style={[
                          styles.ingridientsText,
                          { color: textNutritions },
                        ]}
                      >
                        Nutrition
                      </Text>
                      <View
                        style={[
                          styles.ingridientsBorder,
                          { backgroundColor: greenColor },
                          borderNutritions === 'flex'
                            ? borderStyle.borderDisplayFlex
                            : borderStyle.borderDisplayNone,
                        ]}
                      />
                    </TouchableOpacity>
                  </View>

                  {/* EXPLANATION */}
                  <View style={{
                    marginHorizontal: scale(10),
                    marginVertical: verticalScale(10),
                  }}>
                    {
                      // IF ELSE FOR SHOW CONTENT
                      activateTabs === 'ingredients'
                        ? (selectedMeal.description
                        ? (<Text style={styles.nutritionsText}>{selectedMeal.ingredients}</Text>)
                        : (<Text> {'\n \n \n \n \n \n'} </Text>
                        ))
                        : (
                          selectedMeal.nutrition.data.map((nutrition, index) => {
                            return (
                              <View key={index} style={styles.nutritionsBox}>
                                <View style={{ flex: 3 }}>
                                  <Text style={styles.nutritionsText}>
                                    {nutrition.name}
                                  </Text>
                                </View>

                                <View style={{
                                  flex: 1,
                                  alignItems: 'flex-end',
                                }}>
                                  <Text style={styles.nutritionsText}>
                                    {`${nutrition.amount} ${nutrition.unit}`}
                                  </Text>
                                </View>
                              </View>
                            )
                          })
                        )
                    }
                  </View>
                </View>
              </Animatable.View>
            </View>
          }

        </Content>
        <ShoppingCartAlertModal
          primaryAction={this.onPressCompleteOrder}
          secondaryAction={this.onPressClearCart}
          visible={showCombinationAlertModal}
          dismiss={() => this.setState({
            showCombinationAlertModal: false,
          })}
        />
        {(cart.cartItems.length > 0 && cart.cartItems[0].items.length > 0) &&
        <Footer style={{ backgroundColor: Colors.pinkishOrange }}>
          <ShoppingCartButton
            cart={this.props.cart}
            isLoggedIn={isLoggedIn}
            onPressHandle={this.onPressShoppingCart}
            testID='shoppingCartButtonOnMealDetail'
          />
        </Footer>
        }
      </Container>
    )
  }
}

const borderStyle = StyleSheet.create({
  borderDisplayFlex: {
    display: 'flex',
  },
  borderDisplayNone: {
    display: 'none',
  },
})

const mapStateToProps = (state: AppState) => {
  return {
    cart: state.cart,
    isLoggedIn: state.login.user !== null,
    meals: state.meals,
    stock: state.meals.stock,
    isConnected: state.network.isConnected,
    lastUsed: state.lastUsed,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMealDetails: (id, date) => dispatch(MealsActions.fetchMealDetails(id, date)),
    removeFromCart: (item, date) => dispatch(CartActions.removeFromCart(item, date)),
    resetCart: () => dispatch(CartActions.resetCart()),
    addToCart: (cart: responseData) => dispatch(CartActions.addToCart(cart)),
    loading: (status: boolean) => dispatch(CartActions.loading(status)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MealDetailScreen)
