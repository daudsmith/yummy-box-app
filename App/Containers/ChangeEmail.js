import React, { Component } from 'react'
import { Container, Content } from 'native-base'
import { Text, TouchableOpacity, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import { connect } from 'react-redux'
import ModalConfirmationOne from '../Components/ModalConfirmationOne'
import ModalSuccessOne from '../Components/ModalSuccessOne'
import NavigationBar from '../Components/NavigationBar'
import TextInputOne from '../Components/TextInputOne'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import RegistrationActions from '../Redux/RegisterRedux'
import { Colors } from '../Themes'
import Styles from './Styles/ChangeEmailStyle.js'

let initialCondition = {text: '', iconName: '', iconColor: 'black'}
let emptyCondition = {text: '', iconName: '', iconColor: Colors.lightGreyX}
let successMessage = {text: '', iconName: 'check', iconColor: 'green'}
let invalidMessage = {text: 'Invalid email format', iconName: 'close', iconColor: 'red'}

class ChangeEmail extends Component {
  constructor (props) {
    super(props)
    this.state = {
      newEmail: '',
      validationMessage: emptyCondition,
      showSpinner: false,
      showModalValidation: false,
      showModalSuccess: false
    }
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    let {changeEmailSuccess, changingEmail} = newProps.customerAccount
    let {validatingIdentity, identityExist} = newProps.registration

    if (!validatingIdentity && identityExist) {
      this.setState({
        validationMessage: {
          text: 'This email is already registered',
          iconName: 'close',
          iconColor: 'red'
        }
      })
    } else if (!validatingIdentity && !identityExist && !changingEmail) {
      this.toggleModal()
    }

    if (!validatingIdentity && changeEmailSuccess && !changingEmail) {
      setTimeout(() => {
        this.toggleModalSuccess()
      }, 500)
    }
  }

  requestChangeEmail = () => {
    const { newEmail } = this.state
    this.props.changeEmail(newEmail)
  }

  toggleModal = () => {
    let {showModalValidation} = this.state
    this.setState({showModalValidation: !showModalValidation})
  }

  toggleModalSuccess = () => {
    let {showModalSuccess} = this.state
    this.setState({
      showModalSuccess: !showModalSuccess
    })
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  }

  handleOnChangeEmail = (newEmail) => {
    if (newEmail.length === 0) {
      // TextInput kosong
      this.setState({newEmail, validationMessage: emptyCondition})
    } else {
      // TextInput ada isi, karakter kurang dari 7 digit
      if (newEmail.length < 7) {
        this.setState({newEmail, validationMessage: initialCondition})
      } else {
        if (this.validateEmail(newEmail)) {
          // TextInput ada isi, karakter lebih dari 7 digit, validasi true
          this.setState({newEmail, validationMessage: successMessage})
        } else {
          this.setState({newEmail, validationMessage: invalidMessage})
        }
      }
    }
  }

  validateEmailAgain = () => {
    let {newEmail} = this.state
    // check user email format first
    if (!this.validateEmail(newEmail)) {
      this.setState({validationMessage: invalidMessage})
      return false
    }

    // check user email with some
    this.props.validateIdentity(newEmail)
  }

  backToProfileScreen = () => {
    this.toggleModalSuccess()
    this.props.resetChangeEmailAndPhone()
    this.props.navigation.goBack()
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    let { newEmail, showModalValidation, validationMessage, showModalSuccess } = this.state
    const { changingEmail } = this.props.customerAccount
    const { validatingIdentity } = this.props.registration
    return (
      <Container>
        <NavigationBar leftSide='back' title='Update Email Address' leftSideNavigation={this.navigationGoBack} />
        <Content>
          <Spinner visible={(changingEmail || validatingIdentity)} textStyle={{color: '#FFF'}} />
          <View style={Styles.contentShadow}>
            <View style={Styles.contentMainBox}>
              <Text style={Styles.p}>Please enter your new email address.</Text>
              <Text style={Styles.h1}>New Email Address</Text>
              <View style={{marginTop: 10}}>
                <TextInputOne
                  testID='newEmail'
                  accessibilityLabel='newEmail'
                  value={newEmail}
                  onChangeText={(newEmail) => this.handleOnChangeEmail(newEmail)}
                  showMessage={validationMessage}
                />
              </View>

              <TouchableOpacity
                testID='updateEmail'
                accessibilityLabel='updateEmail'
                style={[Styles.button1, {
                  backgroundColor: newEmail ? Colors.bloodOrange : Colors.lightGreyX
                }]}
                disabled={!newEmail}
                onPress={() => this.validateEmailAgain()}
              >
                <Text style={Styles.button1Text}>Update Email</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Content>

        {/* MODAL CONFIRMATION */}
        <ModalConfirmationOne
          showModal={showModalValidation}
          message={`Are you sure want to update ${'\n'} your email address to ${'\n'}`}
          confirmationType='email'
          value={newEmail}
          onPressYes={() => this.requestChangeEmail()}
          onPressNo={() => this.toggleModal()}
        />

        {/* MODAL SUCCESS CHANGE EMAIL */}
        <ModalSuccessOne
          testID='doneEmail'
          accessibilityLabel='doneEmail'
          showModal={showModalSuccess}
          message={`You’re successfully updated ${'\n'} your email address.`}
          onPress={() => this.backToProfileScreen()}
        />
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    customerAccount: state.customerAccount,
    user: state.login.user,
    registration: state.register,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeEmail: (newEmail) => dispatch(CustomerAccountActions.changeEmail(newEmail)),
    resetChangeEmailAndPhone: () => dispatch(CustomerAccountActions.resetChangeEmailAndPhone()),
    validateIdentity: (identity) => dispatch(RegistrationActions.validateIdentity(identity))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeEmail)
