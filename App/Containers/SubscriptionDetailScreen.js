import { MealCard, MenuSlider } from '../Components/Common'
import { Container, Content, Footer } from 'native-base'
import { Row } from '../Components/Layout'
import { CardPlaceholder } from '../Components/Placeholder'
import moment from 'moment'
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Dimensions, Text, TouchableWithoutFeedback, View } from 'react-native'
import { connect } from 'react-redux'
import NavigationBar from '../Components/NavigationBar'
import StaticMealDetailPopUp from '../Components/StaticMealDetailPopUp'
import ThemeActions from '../Redux/ThemeRedux'
import Colors from '../Themes/Colors'
import Styles from './Styles/SubscriptionDetailScreenStyle'
import MealDetailPopUpActions from '../Redux/MealDetailPopUpRedux'
import Analytics from '../Services/LogEventService'

const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri']
const { width: VIEWPORT_WIDTH } = Dimensions.get('window')

class SubscriptionDetailScreen extends Component {
  static propTypes = {
    cart: PropTypes.object.isRequired,
    fetchingTotals: PropTypes.bool.isRequired,
    theme: PropTypes.object.isRequired,
    setting: PropTypes.object.isRequired,
    mealDetailPopUp: PropTypes.object.isRequired,
    fetchAvailableDate: PropTypes.func.isRequired,
    fetchMealDetail: PropTypes.func.isRequired,
    resetMealDetailPopUp: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this.repeatIntervals = JSON.parse(props.setting.subscription_repeat_interval)
    this.state = {
      subscriptionDetail: props.cart.cartItems[0].subscription,
      showMealDetailModal: false,
      meals: [],
      selectedMealId: null,
      selectedMealDate: null
    }
  }

  componentDidMount () {
    this.props.fetchAvailableDate(this.state.subscriptionDetail.theme_id)
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    const {availableMeals, fetchingAvailableMeals} = newProps.theme
    if (!fetchingAvailableMeals
      && newProps.cart.cartItems.length
    ) {
      this.filterMealsBasedOnSubscriptionDay(availableMeals)
    }
  }

  filterMealsBasedOnSubscriptionDay (availableMeals) {
    const {cart} = this.props
    if (cart) {
      const { subscription } = cart.cartItems[0]
      const { repeat_every, start_date, end_date } = subscription
      let repeatedDayAvailableMeals
      if (end_date === 'Unlimited') {
        const allowedMeals = availableMeals.filter((meal) => moment(meal.date).isSameOrAfter(moment(start_date), 'day'))
        repeatedDayAvailableMeals = allowedMeals.filter((meal) => repeat_every.includes(moment(meal.date).day() - 1))
      } else {
        const allowedMeals = availableMeals.filter((meal) => moment(meal.date).isSameOrBefore(moment(end_date), 'day') && moment(meal.date).isSameOrAfter(moment(start_date), 'day'))
        repeatedDayAvailableMeals = allowedMeals.filter((meal) => repeat_every.includes(moment(meal.date).day() - 1))
      }

      this.setState({ meals: repeatedDayAvailableMeals })
    }
  }
  getRepeatDayString (indices) {
    let dayString = ''
    indices.map((dayIndex, index) => {
      if (index === indices.length - 1) {
        dayString = dayString + days[dayIndex]
      } else {
        dayString = dayString + days[dayIndex] + ', '
      }
    })
    return dayString
  }

  closeModal = () => {
    this.setState({
      showMealDetailModal: false,
      selectedMealId: null,
      selectedMealDate: null
    }, this.props.resetMealDetailPopUp)
  }

  handleCheckoutOnPress = () => {
    const { cart } = this.props
    const cartValue = cart.cartItems[0].first_item.price
    const params = {
      order_type: 'subscription',
      currency: 'IDR',
      value: cartValue
    }
    Analytics.logEvent('begin_checkout', params, ['default'])
    if (this.props.navigation.state.params !== undefined) {
      this.props.navigation.navigate({
        routeName: 'SubscriptionConfirmationScreen',
        key: 'SubscriptionConfirmationScreen'
      })
    } else {
      this.props.navigation.navigate({
        routeName: 'HomeSubscriptionConfirmation',
        key: 'HomeSubscriptionConfirmation'
      })
    }
  }

  selectMeal = (mealId, date) => {
    this.setState({
      showMealDetailModal: true,
      selectedMealId: mealId,
      selectedMealDate: date
    }, () => {
      this.props.fetchMealDetail(mealId, date)
    })
  }

  renderMealListSection () {
    const {fetchingTotals} = this.props

    const meals = this.state.meals.map((value) => {
      return {...value.item, date: value.date}
    })
    return (
      <View style={Styles.mealListContainer}>
        {fetchingTotals ?
          <View
            style={{
              width: 276,
              position: 'absolute',
              left: 20,
              justifyContent: 'flex-start',
              flexDirection: 'row',
            }}
          >
            <CardPlaceholder height={199} width={120} style={{
              backgroundColor: 'transparent',
              paddingTop: 0,
              paddingRight: 0,
              paddingLeft: 0,
              paddingBottom: 0,
            }}/>
            <CardPlaceholder height={199} width={120} style={{
              backgroundColor: 'transparent',
              paddingTop: 0,
              paddingRight: 0,
              paddingLeft: 0,
              paddingBottom: 0,
            }}/>
          </View>
          :
          <Fragment>
            <View style={{left: 15}}>
              <Text style={Styles.headerThemeNameText}>Your Selected Meals</Text>
            </View>
            <MenuSlider
              style={{
                marginTop: 5,
                marginLeft: 15,
                width: (VIEWPORT_WIDTH+15), // handle shadow
                top: -8, // handle shadow
                left: -4, // handle shadow
              }}
              items={meals}
              renderItem={this._renderThemeItem}
            />
            <View style={Styles.mealInformationContainer}>
              <Text style={Styles.mealInformationText}>Two weeks of meals are displayed at a time</Text>
            </View>
          </Fragment>
        }
      </View>
    )
  }

  renderHeaderInformationKeyValuePair (key, value) {
    return (
      <View style={Styles.headerInformationContainer}>
        <Text style={Styles.headerInformationKeyText}>{key}</Text>
        <Text style={Styles.headerInformationValueText}>{value}</Text>
      </View>
    )
  }

  renderHeaderSection () {
    const {fetchingTotals, cart} = this.props
    const {subscription} = cart.cartItems[0]
    let endDate = subscription.end_date
    if (subscription.end_date !== 'Unlimited') {
      endDate = moment(subscription.end_date).format('ddd, DD MMM YYYY')
    }
    return (
      <View style={Styles.headerContainer}>
        <View style={{ marginTop: 11, marginBottom: 5 }}>
          {fetchingTotals ?
            <View
              style={{
                height: 130,
              }}
            >
              <CardPlaceholder height={20} width={150} style={{
                backgroundColor: 'transparent',
                paddingTop: 0,
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0,
              }}/>
              <CardPlaceholder height={20} width={250} style={{
                backgroundColor: 'transparent',
                paddingTop: 0,
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0,
              }}/>
              <CardPlaceholder height={20} width={175} style={{
                backgroundColor: 'transparent',
                paddingTop: 0,
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0,
              }}/>
              <CardPlaceholder height={20} width={250} style={{
                backgroundColor: 'transparent',
                paddingTop: 0,
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0,
              }}/>
              <CardPlaceholder height={20} width={150} style={{
                backgroundColor: 'transparent',
                paddingTop: 0,
                paddingRight: 0,
                paddingLeft: 0,
                paddingBottom: 0,
              }}/>
            </View>
            :
            <Fragment>
              <View style={Styles.headerTextLine}>
                <Text style={Styles.headerThemeNameText}>Subscribe Information</Text>
              </View>
              {this.renderHeaderInformationKeyValuePair('Start Date', moment(subscription.start_date).format('ddd, DD MMM YYYY'))}
              {this.renderHeaderInformationKeyValuePair('Delivery Every', this.getRepeatDayString(subscription.repeat_every))}
              {this.renderHeaderInformationKeyValuePair('End Date', endDate)}
            </Fragment>
          }
        </View>
      </View>
    )
  }

  _renderThemeItem = ({item}) => {
    const image = item.catalog_image.medium_thumb
    const title = item.name
    const caption = moment(item.date).format('ddd, DD MMM YYYY')
    return (
      <MealCard
        style={{
          height: 199,
          width: 120,
          borderRadius: 8,
          marginTop: 8, // handle shadow
          marginBottom: 12, // handle shadow
          marginLeft: 4, // handle shadow
          marginRight: 4, // handle shadow
        }}
        contentStyle={{height: 79}}
        captionStyle={{fontSize: 11}}
        titleStyle={{fontSize: 12}}
        imageContainerStyle={{height: 120, width: 120}}
        image={image}
        title={title}
        caption={caption}
        onPress={() => this.selectMeal(item.item_id, item.date)}
      />
    )
  }

  renderFooterButton = (fetchingTotals) => {
    const backgroundColor = fetchingTotals
      ? {backgroundColor: Colors.pinkishGrey}
      : {backgroundColor: Colors.primaryOrange}
    return (
      <Footer style={backgroundColor}>
        <TouchableWithoutFeedback
          testID='proceedToCheckoutSub'
          accessibilityLabel='proceedToCheckoutSub'
          style={{flex: 1}}
          onPress={this.handleCheckoutOnPress}
          disabled={fetchingTotals}
        >
          <View
            style={{
              ...Styles.containerFooterButton,
              ...backgroundColor,
            }}
          >
            <Text style={Styles.textFooterButton}>Proceed to Checkout</Text>
          </View>
        </TouchableWithoutFeedback>
      </Footer>
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    const {showMealDetailModal} = this.state
    const {cart, fetchingTotals, mealDetailPopUp} = this.props
    return (
      <Container>
        {cart.cartItems.length > 0 &&
          <NavigationBar
            leftSide='back'
            title='Subscription Review'
            leftSideNavigation={this.navigationGoBack}
          />
        }
        <Content contentContainerStyle={{flexGrow: 1}}>
          <Row style={{marginBottom: 16, paddingBottom: 0}}>
            {cart.cartItems.length > 0 &&
              <View style={{ flex: 1 }}>
                {this.renderHeaderSection()}
                <View style={Styles.divider}/>
                {this.renderMealListSection()}
              </View>
            }
          </Row>
          <StaticMealDetailPopUp
            closeModal={() => this.closeModal()}
            visible={showMealDetailModal}
            mealId={this.state.selectedMealId}
            date={this.state.selectedMealDate}
            mealDetailPopUp={mealDetailPopUp}
          />
        </Content>

        {this.renderFooterButton(fetchingTotals)}

      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    cart: state.cart,
    fetchingTotals: state.cart.fetchingTotals,
    theme: state.theme,
    setting: state.setting.data,
    mealDetailPopUp: state.mealDetailPopUp,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAvailableDate: (themeId) => dispatch(ThemeActions.fetchAvailableDate(themeId)),
    fetchMealDetail: (mealId, date) => dispatch(MealDetailPopUpActions.fetchMealDetail(mealId, date)),
    resetMealDetailPopUp: () => dispatch(MealDetailPopUpActions.resetMealDetailPopUp()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionDetailScreen)
