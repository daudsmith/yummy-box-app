import moment, { Moment } from 'moment'
import * as React from 'react'
import {
  Container,
  Content,
  Footer,
} from 'native-base'
import {
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { Calendar } from 'react-native-calendars'
import Spinner from 'react-native-loading-spinner-overlay'
import Modal from 'react-native-modal'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import AlertBox from '../Components/AlertBox'
import FooterButton from '../Components/FooterButton'
import NavigationBar from '../Components/NavigationBar'
import RoundedDropdown from '../Components/RoundedDropdown'
import icomoonConfig from '../Images/SvgIcon/selection.json'
import CartActions, { BaseCart } from '../Redux/V2/CartRedux'
import ThemeActions, { InitStateTheme } from '../Redux/ThemeRedux'
import AvailabilityDateService from '../Services/AvailabilityDateService'
import Colors from '../Themes/Colors'
import Styles from './Styles/SubscriptionScheduleScreenStyle'
import { AppState } from '../Redux/CreateStore'
import { SettingDataInterface } from '../Redux/SettingRedux'
import { responseData } from '../Services/V2/ApiCart'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import SalesOrderService from '../Services/SalesOrderService'
import { CartItemSubscription } from '../Redux/CartRedux'
import { baseLastUsed } from '../Redux/V2/LastUsedRedux'

const YumboxIcon = createIconSetFromIcoMoon(icomoonConfig)

export interface SubsScheduleProps {
  navigation: NavigationDrawerProp,
  theme: InitStateTheme,
  offDates: Moment[],
  cart: BaseCart,
  setting: SettingDataInterface,
  fetchDetail: (id: number, kitchenCode: string) => void,
  addToCart: (cart: responseData) => void,
  resetCart: () => void,
  loading: (status: boolean) => void,
  lastUsed: baseLastUsed
}

export interface filteredDropdownValues {
  label: string,
  index: number,
}

export interface dropdownValuesInterface {
  label: string,
  index: number,
  value: number,
}

export interface SubsScheduleStates {
  themeId: number,
  themeName: string,
  disableNextButton: boolean,
  selectedDayIndices: string[] | number[],
  selectedDate?: string,
  selectedRepeatIntervalIndex: number,
  selectedDropdownValue: filteredDropdownValues,
  showCalendar: boolean,
  availableDates: Moment[],
  weekendDates: Moment[],
  showResetAlert: boolean,
  showLoading: boolean,
  dropdownValues: filteredDropdownValues[],
}

class SubscriptionScheduleScreen extends React.Component<SubsScheduleProps, SubsScheduleStates> {
  dropdownValues
  deliveryCutOffListener

  constructor(props: SubsScheduleProps) {
    super(props)
    this.dropdownValues = JSON.parse(props.setting.subscription_repeat_interval)
    const { theme, offDates, cart } = props
    const { cartItems } = cart
    const cartIsEmpty = cartItems.length === 0
    const subscriptionInCart = cartIsEmpty
      ? null
      : cartItems[0].subscription
    const selectedThemeIsInCart = this.checkIfThemeIsInCart()
    const fromThemeDetailScreen = props.navigation.state.params !== undefined
    let selectedDate = null
    let availableDates = null
    let weekendDates = AvailabilityDateService.getWeekendDateWithinDeliveryDateList(offDates,
      props.setting.delivery_cut_off)
    const { themeDetail } = theme
    if (fromThemeDetailScreen) {
      if (themeDetail) {
        const { available_dates } = themeDetail
        if (available_dates) {
          selectedDate = available_dates[0]
          availableDates = available_dates.map((date) => moment(date))
        }
      }
    }
    if (!availableDates) {
      availableDates = AvailabilityDateService.getDeliveryDateList(offDates, props.setting.delivery_cut_off)
    }

    let dropdownValues = this.filterDropdownValues(this.dropdownValues, themeDetail)
    let selectedInCart
    if (selectedThemeIsInCart) {
      selectedInCart = dropdownValues.find(dropdown => dropdown.index === subscriptionInCart.repeat_interval)
    }

    this.state = {
      themeId: fromThemeDetailScreen
        ? theme.themeDetail.id
        : subscriptionInCart.theme_id,
      themeName: fromThemeDetailScreen
        ? theme.themeDetail.name
        : subscriptionInCart.theme_name,
      disableNextButton: cartIsEmpty
        ? true
        : !selectedThemeIsInCart,
      selectedDayIndices: cartIsEmpty
        ? [
          0,
          1,
          2,
          3,
          4,
        ]
        : selectedThemeIsInCart
          ? subscriptionInCart.repeat_every
          : [
            0,
            1,
            2,
            3,
            4,
          ],
      selectedDate: cartIsEmpty
        ? selectedDate
        : (selectedThemeIsInCart
          ? subscriptionInCart.start_date
          : selectedDate),
      selectedRepeatIntervalIndex: cartIsEmpty
        ? dropdownValues[0].index
        : (selectedThemeIsInCart
          ? selectedInCart.index
          : dropdownValues[0].index),
      selectedDropdownValue: cartIsEmpty
        ? dropdownValues[0]
        : (selectedThemeIsInCart
          ? selectedInCart
          : dropdownValues[0]),
      showCalendar: false,
      availableDates,
      weekendDates,
      showResetAlert: false,
      showLoading: false,
      dropdownValues,
    }
  }

  filterDropdownValues = (dropdownConfigValues: dropdownValuesInterface[], themeDetail): filteredDropdownValues[] => {
    let dropdownValues = dropdownConfigValues.map(dropdown => {
      return {
        label: dropdown.label,
        index: dropdown.index,
      }
    })

    if (themeDetail) {
      const { max_subscription } = themeDetail
      dropdownValues = dropdownConfigValues.map(dropdown => {
        if (dropdown.index === 0) {
          if (max_subscription >= 90) {
            return {
              label: dropdown.label,
              index: dropdown.index,
            }
          }
        } else {
          if (dropdown.value <= max_subscription) {
            return {
              label: dropdown.label,
              index: dropdown.index,
            }
          }
        }
      })
    }

    return dropdownValues.filter((dropdown) => dropdown != null)
  }

  componentDidMount() {
    this.listenForDeliveryCutOff()
    if (this.props.cart) {
      const { cartItems } = this.props.cart
      if (cartItems.length) {
        if (this.props.navigation.state.params === undefined) {
          this.props.fetchDetail(cartItems[0].subscription.theme_id, this.props.lastUsed.code )
        }
      }
    }
  }

  componentWillUnmount() {
    clearTimeout(this.deliveryCutOffListener)
  }

  listenForDeliveryCutOff = () => {
    this.deliveryCutOffListener = setTimeout(() => {
      this.setState({ showLoading: true })
      const hasSelectTomorrow = this.state.selectedDate === moment()
        .utcOffset(7)
        .add(1, 'days')
        .format('YYYY-MM-DD')
      setTimeout(() => {
        const availableDates = AvailabilityDateService.getDeliveryDateList(this.props.offDates,
          this.props.setting.delivery_cut_off)
        const weekendDates = AvailabilityDateService.getWeekendDateWithinDeliveryDateList(this.props.offDates,
          this.props.setting.delivery_cut_off)
        this.setState({
          availableDates,
          weekendDates,
          selectedDate: hasSelectTomorrow
            ? null
            : this.state.selectedDate,
          disableNextButton: hasSelectTomorrow
            ? true
            : this.state.disableNextButton,
          showLoading: false,
        })
      }, 500)
    }, AvailabilityDateService.getDeliveryCutOffCountDown(this.props.setting.delivery_cut_off))
  }

  checkIfThemeIsInCart = () => {
    const { cart, navigation } = this.props
    if (cart.type === 'subscription') {
      if (navigation.state.params === undefined) {
        return true
      } else {
        return this.props.theme.themeDetail.id === cart.cartItems[0].subscription.theme_id
      }
    } else {
      return false
    }
  }

  resetAll = () => {
    this.props.resetCart()
    this.setState({
      selectedDayIndices: [
        0,
        1,
        2,
        3,
        4,
      ],
      selectedDate: null,
      selectedDropdownValue: this.dropdownValues[0],
      disableNextButton: true,
    })
  }

  checkIfUserAllowToProceed = () => {
    const { selectedDate, selectedDayIndices } = this.state
    let allowToProceed = true
    if (selectedDate === null || selectedDayIndices.length === 0) {
      allowToProceed = false
      return allowToProceed
    }
    return allowToProceed
  }

  toggleRepeatDay = (index, isAddingIndex) => {
    const { selectedDayIndices } = this.state
    if (isAddingIndex) {
      const newSelectedDayIndices = [
        ...selectedDayIndices,
        index,
      ]
      this.setState({ selectedDayIndices: newSelectedDayIndices.sort() })
    } else {
      let newSelectedDayIndices = []
      for (let selectedIndex of selectedDayIndices) {
        if (selectedIndex !== index) {
          newSelectedDayIndices.push(selectedIndex)
        }
      }
      this.setState({ selectedDayIndices: newSelectedDayIndices })
    }
  }

  proceedAndAddToCart = () => {
    let navigateTo
    if (this.props.navigation.state.params === undefined) {
      navigateTo = 'HomeSubscriptionDetail'
    } else {
      navigateTo = 'SubscriptionDetailScreen'
    }
    const {
      themeId,
      themeName,
      selectedDate,
      selectedDayIndices,
      selectedDropdownValue,
    } = this.state

    const {
      cart,
      loading,
      resetCart,
      addToCart,
      navigation,
      lastUsed
    } = this.props

    const item: CartItemSubscription = {
      theme_id: themeId,
      theme_name: themeName,
      start_date: selectedDate,
      repeat_every: selectedDayIndices,
      repeat_interval: selectedDropdownValue.index,
    }
    SalesOrderService.addSubscriptionCart(
      cart,
      item,
      selectedDate,
      loading,
      resetCart,
      addToCart,
      () => navigation.navigate(navigateTo),
      lastUsed.code
    )
  }

  renderCalendar = () => {
    const { selectedDate, availableDates, weekendDates } = this.state
    const disabledDates = AvailabilityDateService.disableWeekendAndOffDates(availableDates, this.props.offDates,
      weekendDates)
    const markedDates = selectedDate !== null
      ? {
        ...disabledDates,
        [selectedDate]: { selected: true },
      }
      : { ...disabledDates }
    return (
      <>
        <NavigationBar title='Select Start Date' leftSide={() => {}} />
        <Calendar
          currentDate={selectedDate}
          minDate={availableDates[0].format('YYYY-MM-DD')}
          maxDate={availableDates[availableDates.length - 1].format('YYYY-MM-DD')}
          markedDates={markedDates}
          onDayPress={(date) => this.setState({
            selectedDate: date.dateString,
            showCalendar: false,
            disableNextButton: false,
          })}
          disable
          style={{ marginHorizontal: 25 }}
          theme={{
            arrowColor: Colors.green,
            monthTextColor: Colors.green,
            selectedDayTextColor: Colors.bloodOrange,
            dayTextColor: Colors.greyishBrownTwo,
            todayTextColor: Colors.bloodOrange,
            textSectionTitleColor: Colors.greyishBrownTwo,
            textMonthFontFamily: 'Rubik-Regular',
            textDayFontFamily: 'Rubik-Regular',
            textDayHeaderFontFamily: 'Rubik-Regular',
            selectedDayBackgroundColor: 'transparent',
            selectedDotColor: Colors.bloodOrange,
          }}
        />
      </>
    )
  }

  renderRepeatPeriodSection = () => {
    let {
      selectedRepeatIntervalIndex,
      selectedDropdownValue,
      dropdownValues,
    } = this.state

    return (
      <View style={Styles.repeatPeriodContainer}>
        <Text style={Styles.repeatPeriodTitleText}>Repeat For</Text>
        <View style={{
          marginTop: 10,
          marginBottom: 15,
        }}>
          <RoundedDropdown
            textDropdown='repeatFor'
            textList='selectRepeatFor'
            label={selectedDropdownValue.label}
            values={dropdownValues}
            selectedIndex={selectedRepeatIntervalIndex}
            showCheck
            onPress={(selected) => {
              this.setState({
                selectedRepeatIntervalIndex: selected.index,
                selectedDropdownValue: selected,
              })
            }}
            disabledIndex={0}
          />
        </View>
      </View>
    )
  }

  renderDayButtons = () => {
    const { selectedDayIndices } = this.state
    const days = [
      'Mon',
      'Tue',
      'Wed',
      'Thu',
      'Fri',
    ]
    return (
      <View style={Styles.dayButtonsContainer}>
        <View style={{ flexDirection: 'row' }}>
          {days.map((day, index: never) => {
            const indexIsNotSelected = selectedDayIndices.indexOf(index) === -1
            const borderStyle = indexIsNotSelected
              ? {
                borderColor: Colors.lightGrey,
                borderWidth: 1,
              }
              : {}
            const textStyle = indexIsNotSelected
              ? { fontFamily: 'Rubik-Regular' }
              : { fontFamily: 'Rubik-Medium' }
            return (
              <TouchableWithoutFeedback
                onPress={() => this.toggleRepeatDay(index, indexIsNotSelected)}
                key={`renderDayButtons${index}`}
              >
                <View style={[
                  Styles.dayButton,
                  borderStyle,
                ]}>
                  <Text style={[
                    Styles.dayButtonText,
                    textStyle,
                  ]}>{day}</Text>
                </View>
              </TouchableWithoutFeedback>
            )
          })}
        </View>
      </View>
    )
  }

  renderRepeatDaySection = () => {
    return (
      <View style={Styles.repeatDayContainer}>
        <Text style={Styles.repeatDayTitleText}>I want to eat every</Text>
        {this.renderDayButtons()}
      </View>
    )
  }

  renderCalendarSection = () => {
    const { selectedDate } = this.state
    const dateText = selectedDate === null
      ? 'Start Date'
      : moment(selectedDate)
        .format('DD MMM YYYY')
    const dateColor = selectedDate !== null
      ? { color: Colors.greyishBrownTwo }
      : {}
    return (
      <View style={Styles.calendarSection}>
        <Text style={Styles.calendarTitleText}>When do you want to start?</Text>
        <TouchableWithoutFeedback
          testID='datepickerSub'
          accessibilityLabel='datepickerSub'
          onPress={() => this.setState({ showCalendar: true })}
        >
          <View style={{
            flexDirection: 'row',
            marginTop: 10,
            alignItems: 'center',
          }}>
            <Text style={[
              Styles.startDateText,
              dateColor,
            ]}>{dateText}</Text>
            <YumboxIcon name='calendar-orange' size={20} color={Colors.bloodOrange} style={{ marginLeft: 8 }} />
            <FontAwesome name='sort-down' color={Colors.bloodOrange} style={{
              paddingBottom: 4,
              marginLeft: 5,
            }} />
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }

  renderDescription = () => {
    return (
      <View style={Styles.descriptionContainer}>
        <Text style={Styles.descriptionText}>
          What is Subscription? {'\n'}Don't waste anymore time thinking about what to eat! Set your subscription schedule below, and we’ll send you a delicious meal based on the days you select.
        </Text>
      </View>
    )
  }

  rightButton = () => {
    return (
      <TouchableOpacity style={{ marginRight: 5 }} onPress={() => this.setState({ showResetAlert: true })}>
        <Text style={Styles.resetButtonText}>Reset</Text>
      </TouchableOpacity>
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render() {
    const {
      showCalendar,
      showResetAlert,
      themeName,
      // hasError,
      // cartValidationError,
    } = this.state
    const enableProceed = this.checkIfUserAllowToProceed()
    return (
      <Container>
        <NavigationBar
          leftSide='back'
          rightSide={() => this.rightButton()}
          title={themeName}
          leftSideNavigation={this.navigationGoBack}
        />
        <Content padder>
          {this.renderDescription()}
          {this.renderCalendarSection()}
          {this.renderRepeatDaySection()}
          {this.renderRepeatPeriodSection()}
          <View style={{
            flex: 1,
            justifyContent: 'flex-start',
            paddingBottom: 25,
          }}>
            <Text
              style={Styles.bottomDescription}>Once your subscription is active, you can easily customize your deliveries or skip a meal before payment. Payment is deducted at 18:30 the night before each scheduled delivery via your Saved Card or YumCredits.</Text>
          </View>
          <Spinner visible={this.state.showLoading} />
          <Modal
            animationIn='slideInDown'
            animationOut='slideOutUp'
            isVisible={showCalendar}
            backdropColor='white'
            backdropOpacity={0.8}
            onBackdropPress={() => this.setState({ showCalendar: false })}
            onBackButtonPress={() => this.setState({ showCalendar: false })}
            style={{
              margin: 0,
              justifyContent: 'flex-start',
            }}
          >
            {this.renderCalendar()}
          </Modal>
          <AlertBox
            primaryAction={() => {}}
            secondaryAction={this.resetAll}
            dismiss={() => this.setState({ showResetAlert: false })}
            text='Are you sure want to reset your subscription?'
            primaryActionText='No'
            secondaryActionText='Yes'
            visible={showResetAlert}
          />
          {/*<AlertBox
           primaryAction={this.props.resetCart}
           dismiss={this.props.resetCart}
           title="Error"
           text={cartValidationError}
           primaryActionText='OK'
           visible={hasError}
           />*/}
        </Content>
        <Footer style={{
          backgroundColor: enableProceed
            ? Colors.bloodOrange
            : Colors.pinkishGrey,
        }}>
          <FooterButton
            testID='nextSubscription'
            accessibilityLabel='nextSubscription'
            text='Next'
            onPress={this.proceedAndAddToCart}
            disabled={!enableProceed}
          />
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    theme: state.theme,
    offDates: state.setting.data.off_dates,
    cart: state.cart,
    setting: state.setting.data,
    lastUsed: state.lastUsed
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDetail: (id: number, kitchenCode: string) => dispatch(ThemeActions.fetchDetail(id, kitchenCode)),
    addToCart: (cart: responseData) => dispatch(CartActions.addToCart(cart)),
    resetCart: () => dispatch(CartActions.resetCart()),
    loading: (status: boolean) => dispatch(CartActions.loading(status)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionScheduleScreen)
