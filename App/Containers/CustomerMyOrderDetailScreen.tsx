import _ from 'lodash'
import React, { Component } from 'react'
import {
  ActivityIndicator,
  AppState as RN_AppState,
  AppStateStatus,
  Dimensions,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View
} from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'
import FastImage from 'react-native-fast-image'
import Spinner from 'react-native-loading-spinner-overlay'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'

import AlertBox from '../Components/AlertBox'
import {
  AlertConfirmLinkedToOtherApp,
  AlertPaymentDeepLink
} from '../Components/AlertLinkedToOtherApp'
import { Button } from '../Components/Common'
import ActionSheet from '../Components/Common/ActionSheet'
import WithSafeAreaView from '../Components/Common/WithSafeAreaView'
import NavigationBar from '../Components/NavigationBar'
import WebView from '../Components/WebView'
import YummyboxIcon from '../Components/YummyboxIcon'
import CustomerOrderActions from '../Redux/CustomerOrderRedux'
import CustomerVirtualAccountActions from '../Redux/CustomerVirtualAccountRedux'
import SettingActions from '../Redux/SettingRedux'
import ContinuePaymentService from '../Services/ContinuePaymentService'
import LocaleFormatter from '../Services/LocaleFormatter'
import OrderDeliveryHelper from '../Services/OrderDeliveryHelper'
import OrderStatusHelper from '../Services/OrderStatusHelper'
import PaymentMethodHelper from '../Services/PaymentMethodHelper'
import { Colors } from '../Themes'
import {
  _linkingFromOtherApp,
  _linkingToOtherApp,
  CheckoutUrlType
} from '../util/_linkingApp'
import Styles from './Styles/CustomerMyOrderDetailScreenStyle'

const { height } = Dimensions.get('window')

interface CustomerMyOrderDetailScreenProps {
  isConnected: boolean;
  orderDetail: {
    id: string;
    action: {
      cancel: any;
    };
    payments: {
      data: Array<any>;
    };
    total_amount_charged: any;
    type: string;
    deliveries: {
      data: any;
    };
    order_number: string;
  };
  navigation: any;
  customerCards: any;
  orderDetailError: string;
  message: string;
  cancelledOrder: boolean;
  fetchingOrderDetail: boolean;
  setting: Object;
  fetchSetting: () => void;
  fetchVirtualAccount: () => void;
  fetchOrderDetail: (order_id: string, type?: string) => void;
  cancelling: () => void;
  clearOrderDetail: () => void;
  cancelOrder: (id: string) => void;
  resetErrorMessage: () => void;
  token: string;
}
interface CustomerMyOrderDetailScreenState {
  params: {
    order_id: string;
    type: string;
    number: number;
  };
  hasFetchedData: boolean;
  sectionsAccordion: Array<any>;
  showActionSheet: boolean;
  actionSheetButtons: Array<any>;
  showCancelOrderModal: boolean;
  errorMessage: string;
  haveError: boolean;
  banks: boolean;
  activeAccordion: Array<any>;
  isContinuePaymentSuccess: boolean;
  isContinuePaymentAlert: boolean;
  isContinuePaymentResponseAlert: boolean;
  isContinuePaymentLoading: boolean;
  webViewURL: CheckoutUrlType;
  appState: AppStateStatus;
}

class CustomerMyOrderDetailScreen extends Component<
  CustomerMyOrderDetailScreenProps,
  CustomerMyOrderDetailScreenState
> {
  constructor(props) {
    super(props)
    this.state = {
      params: this.props.navigation.state.params,
      hasFetchedData: false,
      sectionsAccordion: [],
      showActionSheet: false,
      actionSheetButtons: [],
      showCancelOrderModal: false,
      errorMessage: '',
      haveError: false,
      banks: false,
      activeAccordion: [],
      isContinuePaymentSuccess: false,
      isContinuePaymentAlert: false,
      isContinuePaymentResponseAlert: false,
      isContinuePaymentLoading: false,
      webViewURL: null,
      appState: RN_AppState.currentState
    }
  }

  componentDidMount() {
    if (this.props.isConnected) {
      this.props.fetchVirtualAccount()
      this.props.fetchOrderDetail(
        this.state.params.order_id,
        this.state.params.type
      )
    }
    this._checkDeepLink().onMounted()
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      !this.props.isConnected &&
      newProps.isConnected &&
      (!newProps.fetchingOrderDetail && !newProps.orderDetailError)
    ) {
      this.props.fetchOrderDetail(this.state.params.order_id)
    }
  }

  componentWillUnmount() {
    this.props.clearOrderDetail()
    this._checkDeepLink().onUnmounted()
  }

  _checkDeepLink = () => {
    const _handleAppStateChange = (nextAppState: AppStateStatus) => {
      if (
        this.state.isContinuePaymentLoading && this.state.appState.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        this.setState({
          isContinuePaymentLoading: false,
          isContinuePaymentResponseAlert: true,
          isContinuePaymentSuccess: false,
          webViewURL: null
        })
      }
      this.setState({ appState: nextAppState })
    }

    const onCheckParam = {
      onFailure: () => {
        this.setState({
          isContinuePaymentLoading: false,
          isContinuePaymentResponseAlert: true,
          isContinuePaymentSuccess: false,
          webViewURL: null
        })
      },
      onSuccess: () => {
        this.setState({
          isContinuePaymentLoading: false,
          isContinuePaymentResponseAlert: true,
          isContinuePaymentSuccess: true,
          webViewURL: null
        })
        this.props.fetchOrderDetail(
          this.state.params.order_id,
          this.state.params.type
        )
      }
    }

    const onMounted = () => {
      RN_AppState.addEventListener('change', _handleAppStateChange)
      _linkingFromOtherApp(onCheckParam).mountData()
    }
    const onUnmounted = () => {
      RN_AppState.removeEventListener('change', _handleAppStateChange)
      _linkingFromOtherApp(onCheckParam).unmountData()
    }

    return {
      onMounted,
      onUnmounted
    }
  };

  modifyDelivery(id, deliveryNumber) {
    this.props.navigation.navigate('MyMealPlanDetailScreen', {
      id,
      deliveryNumber,
      type: this.props.orderDetail.type,
      myOrderParams: {
        orderId: this.state.params.order_id,
        orderType: this.state.params.type
      }
    })
  }

  onClickContinuePaymentBtn = () => {
    this.setState({
      isContinuePaymentAlert: true
    })
  };
  _renderOrderDetail(order) {
    const { orderDetail } = this.props
    let sectionsAccordion = []
    if (_.has(orderDetail, 'deliveries')) {
      sectionsAccordion = orderDetail.deliveries.data
    }

    const paymentMethod = orderDetail.payments.data[0].method ?? null

    const isPendingPayment = order.status === 'PENDING_PAYMENT'
    const isPaymentCanContinue =
      paymentMethod &&
      (paymentMethod === 'shopeepay' || paymentMethod === 'gopay')

    return (
      <View style={styles.container}>
        <View>
          <View style={styles.columns}>
            <Text style={styles.orderStatusLabel}>Order Status</Text>
            <Text style={styles.orderStatus}>
              {OrderStatusHelper.parseToLabel(order.status)}
            </Text>
          </View>
          <View style={[styles.columns, { marginTop: 8 }]}>
            <Text style={[styles.orderStatusLabel, { fontSize: 12 }]}>
              Order Date
            </Text>
            <Text style={[styles.orderStatus, { fontSize: 12 }]}>
              {order.created_at}
            </Text>
          </View>
          <View style={[styles.columns, { marginTop: 5 }]}>
            {order.type === 'package' ? (
              <Text style={[styles.orderStatusLabel, { fontSize: 12 }]}>{`${
                order.total_item_in_cart
              }-Day Package`}</Text>
            ) : (
              <Text
                style={[styles.orderStatusLabel, { fontSize: 12 }]}
              >{`Total ${order.total_item_in_cart} item(s)`}</Text>
            )}

            {order.type === 'package' ? (
              <Text style={[styles.orderStatus, { fontSize: 12 }]}>
                {' '}
                {`${
                  order.deliveries.data.filter(
                    data => data.status === 'COMPLETE'
                  ).length
                } of ${order.deliveries.data.length} Delivered`}
              </Text>
            ) : null}
          </View>

          <View
            style={{
              marginTop: 40,
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: 12
            }}
          >
            {order.deliveries.data.length > 1 ? (
              <Text style={[styles.orderStatusLabel]}>Deliveries</Text>
            ) : (
              <Text style={[styles.orderStatusLabel]}>Delivery</Text>
            )}
          </View>

          {sectionsAccordion && (
            <Accordion
              sections={sectionsAccordion}
              underlayColor={'white'}
              renderHeader={(section, index, isActive) =>
                this.renderHeaderAccordion(section, index, isActive)
              }
              renderContent={(section, index) =>
                this.renderContentAccordion(section, index)
              }
              onChange={indexes => {
                this.setState({ activeAccordion: indexes })
              }}
              activeSections={this.state.activeAccordion}
            />
          )}

          {this._renderOrderTotal(order)}
        </View>
        {isPendingPayment && isPaymentCanContinue && (
          <Button
            transparent
            onPress={this.onClickContinuePaymentBtn}
            buttonStyle={{ marginTop: 25, backgroundColor: '#FF6C4A' }}
            text="Continue Payment"
          />
        )}
      </View>
    )
  }

  renderHeaderAccordion = (section, _index, isActive) => {
    return (
      <View
        testID={`arrowDelivery${_index}`}
        accessibilityLabel={`arrowDelivery${_index}`}
        style={[
          Styles.headerAccordion,
          {
            borderBottomColor: isActive ? 'transparent' : Colors.pinkishGrey,
            marginBottom: isActive ? -10 : 10
          }
        ]}
      >
        <Text style={styles.headerTitle}>{section.date}</Text>
        {isActive ? (
          <Ionicons name="chevron-up" size={20} color={Colors.brownishGrey} />
        ) : (
          <Ionicons name="chevron-down" size={20} color={Colors.brownishGrey} />
        )}
      </View>
    )
  };

  renderContentAccordion = (section, index) => {
    return (
      <View style={[Styles.renderContentBox, {}]}>
        <View
          style={{
            width: '100%',
            height: StyleSheet.hairlineWidth,
            backgroundColor: Colors.pinkishGrey,
            marginTop: 10
          }}
        />
        <View style={Styles.accordionPaddingBox}>
          {section.items.data.map((item, index) => {
            return (
              <View style={Styles.accordionDescBox} key={index}>
                <View style={Styles.accordionImage}>
                  <FastImage
                    style={Styles.imageOrderDetail}
                    source={{
                      uri: item.detail.data.images.data[0].thumbs.medium_thumb,
                      priority: FastImage.priority.normal
                    }}
                  />
                </View>
                <View style={Styles.accordionNameBox}>
                  <Text style={Styles.accordionName}>
                    {item.detail.data.name}
                  </Text>
                </View>
                <View style={Styles.accordionQtyBox}>
                  {item.quantity === 1 ? null : (
                    <Text style={Styles.accordionQty}>{`x${
                      item.quantity
                    }`}</Text>
                  )}
                </View>
              </View>
            )
          })}
        </View>

        <View style={Styles.descBox}>
          <Text style={Styles.regular12}>
            #{section.number.replace('KDS-DELV-', '')}
          </Text>
          <Text style={[Styles.medium14, { color: Colors.greyishBrownTwo }]}>
            {OrderDeliveryHelper.parseStatusToLabel(section.status)}
          </Text>
        </View>

        <View style={[Styles.descBox, { borderTopColor: 'transparent' }]}>
          <Text style={Styles.regular14}>Delivery Time</Text>
          <Text style={[Styles.medium14, { color: Colors.greyishBrownTwo }]}>
            {section.time}
          </Text>
        </View>
        {section.status !== 'CANCELLED' && (
          <View style={Styles.modifyButton}>
            <TouchableWithoutFeedback
              onPress={() => this.modifyDelivery(section.id, section.number)}
              testID={'modifyDetails' + index}
              accessibilityLabel={'modifyDetails' + index}
            >
              <View>
                <Text style={Styles.modifyText}>Modify</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        )}
      </View>
    )
  };

  _renderLoading = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: height / 2 - 40
        }}
      >
        <ActivityIndicator size={'large'} />
      </View>
    )
  };

  _renderOrderTotal(order) {
    const deliveryFee = OrderDeliveryHelper.deliveryFeeTotal(
      order.deliveries.data
    )
    let deliveryColor = Colors.greyishBrown
    let deliveryFeeStr
    if (deliveryFee > 0) {
      deliveryFeeStr = LocaleFormatter.numberToCurrency(deliveryFee)
    } else {
      deliveryFeeStr = 'Free'
      deliveryColor = Colors.grennBlue
    }
    let paymentMethod = []
    order.payment
      .split(' ')
      .map(word =>
        paymentMethod.push(
          word[0].toUpperCase() + word.substring(1).toLowerCase()
        )
      )

    return (
      <View
        style={[
          styles.box,
          {
            marginTop: 18,
            backgroundColor: 'rgb(238, 238, 238)',
            borderColor: 'rgb(238, 238, 238)'
          }
        ]}
      >
        <View style={[styles.columns]}>
          <Text style={styles.orderTotalLabel}>Total Items</Text>
          <Text style={styles.orderTotalLabel}>
            {LocaleFormatter.pluralTranslation(
              order.total_item_in_cart,
              'Item',
              'Items'
            )}
          </Text>
        </View>
        <View style={[styles.columns, { marginTop: 8 }]}>
          <Text style={styles.orderTotalLabel}>Subtotal</Text>
          <Text style={styles.orderTotalLabel}>
            {LocaleFormatter.numberToCurrency(order.base_total_charged)}
          </Text>
        </View>
        <View style={[styles.columns, { marginTop: 8 }]}>
          <Text style={styles.orderTotalLabel}>Delivery Fee</Text>
          <Text style={[{ color: deliveryColor }]}>{deliveryFeeStr}</Text>
        </View>
        {order.discount_amount > 0 && (
          <View style={[styles.columns, { marginTop: 8 }]}>
            <Text style={styles.orderTotalLabel}>Discount</Text>
            <Text style={[styles.orderTotalLabel, { color: Colors.grennBlue }]}>
              {LocaleFormatter.numberToCurrency(order.discount_amount)}
            </Text>
          </View>
        )}
        {order.total_free_charged > 0 && (
          <View style={[styles.columns, { marginTop: 8 }]}>
            <Text style={styles.orderTotalLabel}>Free Charged</Text>
            <Text style={[styles.orderTotalLabel, { color: Colors.grennBlue }]}>
              {LocaleFormatter.numberToCurrency(order.total_free_charged)}
            </Text>
          </View>
        )}
        <View
          style={[
            styles.columns,
            {
              marginTop: 16,
              borderTopWidth: 1,
              borderTopColor: Colors.pinkishGrey,
              paddingTop: 16
            }
          ]}
        >
          <Text style={[styles.boldLabel, { fontFamily: 'Rubik-Medium' }]}>
            Total
          </Text>
          <Text style={[styles.boldLabel, { fontFamily: 'Rubik-Medium' }]}>
            {LocaleFormatter.numberToCurrency(order.total_amount_paid)}
          </Text>
        </View>
        <View style={[styles.columns, { marginTop: 14 }]}>
          <Text style={styles.boldLabel}>Payment Method</Text>
          {this.renderOrderTotalPaymentMethod(order)}
        </View>
      </View>
    )
  }

  renderOrderTotalPaymentMethod(order) {
    if (order.payment === 'Credit Card') {
      const { customerCards } = this.props
      const maskedCardNumber = order.payments.data[0].info.masked_card_number
      const creditCardDetail = customerCards.filter(
        card =>
          card.type ===
          PaymentMethodHelper.getCreditCardBrand(
            order.payments.data[0].info.card_brand
          )
      )
      const cardType = PaymentMethodHelper.getCreditCardBrand(
        order.payments.data[0].info.card_brand
      )

      return (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center'
          }}
        >
          <Text style={[styles.orderTotalLabel]}>
            *
            {maskedCardNumber.substring(
              maskedCardNumber.length - 4,
              maskedCardNumber.length
            )}
          </Text>
          <YummyboxIcon
            name={PaymentMethodHelper.getCreditCardIconName(
              typeof creditCardDetail[0] === 'undefined'
                ? cardType
                : creditCardDetail[0].type
            )}
            size={20}
            color={Colors.brownishGrey}
            style={{ paddingLeft: 4 }}
          />
        </View>
      )
    } else if (order.payment === 'Bank Transfer') {
      return <Text style={styles.orderTotalLabel}>Bank Transfer</Text>
    } else if (order.payment === 'Dana') {
      return <Text style={styles.orderTotalLabel}>DANA</Text>
    } else if (order.payment === 'YumCredits') {
      return (
        <Text
          style={styles.orderTotalLabel}
        >{`-${LocaleFormatter.numberToCurrency(order.total_amount_paid).replace(
          'Rp. ',
          ''
        )} YumCredits`}</Text>
      )
    } else if (order.payment === 'ShopeePay') {
      return <Text style={styles.orderTotalLabel}>ShopeePay</Text>
    } else if (order.payment === 'Gopay') {
      return <Text style={styles.orderTotalLabel}>GoPay</Text>
    } else {
      return <Text style={styles.orderTotalLabel}>Cash</Text>
    }
  }

  showHeaderMenu() {
    const { orderDetail } = this.props
    let buttons = []
    if (orderDetail) {
      if (orderDetail.action.cancel) {
        buttons.push({
          text: 'Cancel Order',
          onPress: () => {
            if (this.state.showActionSheet) {
              this.setState(
                {
                  showActionSheet: false
                },
                () => {
                  // iOS need timeout, cant show and closing modal in the same time
                  // this modal too (showActionSheet)
                  setTimeout(() => {
                    this.setState({
                      showCancelOrderModal: true
                    })
                  }, 500)
                }
              )
            }
          }
        })
      }
    }

    buttons.push({
      text: 'FAQ',
      onPress: () => Linking.openURL('http://help.yummybox.id')
    })

    if (Platform.OS === 'ios') {
      buttons.push({
        text: 'Close',
        onPress: () => this.setState({ showActionSheet: false })
      })
    }

    this.setState({
      showActionSheet: true,
      actionSheetButtons: buttons
    })
  }

  cancelOrder(id) {
    this.setState(
      {
        showCancelOrderModal: false
      },
      () => {
        this.props.cancelOrder(id)
      }
    )
  }

  getCancelConfirmationMessage() {
    const { orderDetail } = this.props
    if (!orderDetail) {
      return {
        title: '',
        body: ''
      }
    }
    const orderPayment = orderDetail.payments.data[0]
    const isCod = orderPayment.method === 'cod'
    const isUnpaidBankTransfer =
      orderPayment.method === 'virtual-account' &&
      orderPayment.status === 'PENDING'
    if (isCod || isUnpaidBankTransfer) {
      return {
        title: 'Confirm Cancel',
        body: 'Are you sure you want to cancel this order?'
      }
    }
    const amount = LocaleFormatter.numberToCurrency(
      orderDetail.total_amount_charged
    )
    return {
      title: 'Cancel this order?',
      body: `\n${amount} will be added to your YumCredits balance`
    }
  }

  onCancelSuccessHandler = () => {
    this.props.resetErrorMessage()
    this.props.navigation.navigate('CustomerMyOrderListScreen')
  };

  navigationGoBack = () => {
    this.props.navigation.goBack()
  };

  onLinkedAppNextBtn = () => {
    const { orderDetail, token } = this.props

    ContinuePaymentService.create()
      .continuePaymentAPI({
        order_number: orderDetail.order_number,
        token
      })
      .then(response => {
        const isCheckoutUrlObject =
          typeof response.data.checkout_url === 'object'
        const checkoutUrl = response.data.checkout_url
        if (isCheckoutUrlObject) {
          _linkingToOtherApp({ checkout_url: checkoutUrl })
          this.setState({ isContinuePaymentLoading: true })
        } else {
          this.setState({
            webViewURL: checkoutUrl
          })
        }
      })
      .catch(() => {
        this.setState({
          isContinuePaymentLoading: false
        })
      })
  };

  render() {
    const title = `#${this.state.params.number}` || 'Order Details'
    const {
      showCancelOrderModal,
      isContinuePaymentAlert,
      isContinuePaymentLoading,
      isContinuePaymentSuccess,
      isContinuePaymentResponseAlert,
      webViewURL
    } = this.state
    const { orderDetail, fetchingOrderDetail, cancelling } = this.props
    let checkDeliveriesData = _.has(orderDetail, 'deliveries')
    let body = null
    if (orderDetail && !fetchingOrderDetail && checkDeliveriesData) {
      body = this._renderOrderDetail(orderDetail)
    } else if (fetchingOrderDetail) {
      body = this._renderLoading()
    }
    const paymentMethod =
      orderDetail &&
      orderDetail.payments &&
      orderDetail.payments.data[0] &&
      orderDetail.payments.data[0].method

    const cancellationConfirmation = this.getCancelConfirmationMessage()

    return (
      <View
        style={{
          backgroundColor: '#fff',
          flex: 1
        }}
      >
        <NavigationBar
          leftSide={'back'}
          title={title}
          leftSideNavigation={this.navigationGoBack}
          rightSide={() => {
            return (
              <TouchableWithoutFeedback
                onPress={() => this.showHeaderMenu()}
                testID="threeButton"
                accessibilityLabel="threeButton"
                hitSlop={{
                  top: 26,
                  left: 26,
                  bottom: 26,
                  right: 26
                }}
              >
                <View>
                  <Ionicons
                    name={'ellipsis-vertical'}
                    size={23}
                    color={Colors.bloodOrange}
                    style={{
                      marginRight: 10
                    }}
                  />
                </View>
              </TouchableWithoutFeedback>
            )
          }}
        />
        <ScrollView style={{ backgroundColor: 'white' }}>{body}</ScrollView>
        <ActionSheet
          buttons={this.state.actionSheetButtons}
          visible={this.state.showActionSheet}
          dismiss={() => this.setState({ showActionSheet: false })}
        />
        {/*cancel order confirmation*/}
        <AlertBox
          primaryAction={() => {}}
          primaryActionText="Back"
          secondaryAction={() => {
            this.cancelOrder(orderDetail.id)
          }}
          secondaryActionText="Yes, Cancel"
          dismiss={() => this.setState({ showCancelOrderModal: false })}
          text={cancellationConfirmation.body}
          title={cancellationConfirmation.title}
          visible={showCancelOrderModal}
        />
        {/*cancel error alert*/}
        <AlertBox
          primaryAction={() => this.props.resetErrorMessage()}
          primaryActionText="Ok"
          dismiss={() => this.props.resetErrorMessage()}
          text={this.props.orderDetailError}
          visible={!!this.props.orderDetailError}
        />
        {/*cancel success alert*/}
        <AlertBox
          primaryAction={() => this.onCancelSuccessHandler()}
          primaryActionText="Ok"
          dismiss={() => this.onCancelSuccessHandler()}
          text={this.props.message}
          visible={this.props.cancelledOrder}
        />
        <WebView
          visible={!!webViewURL}
          url={webViewURL && webViewURL.toString()}
          dismiss={() =>
            this.setState({
              isContinuePaymentSuccess: true,
              isContinuePaymentResponseAlert: true,
              webViewURL: null
            })
          }
          canGoBack
        />
        <AlertConfirmLinkedToOtherApp
          showAlert={isContinuePaymentAlert}
          onNextBtn={this.onLinkedAppNextBtn}
          dismiss={() =>
            this.setState({
              isContinuePaymentAlert: false,
              isContinuePaymentLoading: false
            })
          }
          paymentMethod={paymentMethod}
        />
        <AlertPaymentDeepLink
          isSuccess={isContinuePaymentSuccess}
          showAlert={isContinuePaymentResponseAlert}
          dismiss={() =>
            this.setState({
              isContinuePaymentAlert: false,
              isContinuePaymentResponseAlert: false
            })
          }
        />
        <Spinner
          visible={cancelling || isContinuePaymentLoading}
          textStyle={{ color: Colors.bloodOrange }}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20,
    paddingBottom: 20
  },
  columns: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'space-between'
  },
  orderStatusLabel: {
    fontFamily: 'Rubik-Light',
    fontSize: 16,
    color: Colors.brownishGrey
  },
  orderStatus: {
    flex: 1,
    textAlign: 'right',
    color: Colors.greyishBrownTwo,
    fontFamily: 'Rubik-Regular',
    fontSize: 16
  },
  box: {
    borderWidth: 1,
    borderColor: Colors.pinkishGrey,
    borderRadius: 3,
    flex: 1,
    paddingVertical: 14,
    paddingHorizontal: 18,
    marginTop: 12
  },
  orderTotalLabel: {
    color: Colors.brownishGrey,
    fontFamily: 'Rubik-Regular',
    fontSize: 14
  },
  boldLabel: {
    color: Colors.greyishBrownThree,
    fontFamily: 'Rubik-Medium',
    fontSize: 14
  },
  headerTitle: {
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    color: Colors.greyishBrownTwo
  }
})

const mapStateToProps = state => {
  return {
    orderDetail: state.customerOrder.orderDetail,
    fetchingOrderDetail: state.customerOrder.fetchingOrderDetail,
    orderDetailError: state.customerOrder.orderDetailError,
    cancelledOrder: state.customerOrder.cancelledOrder,
    cancelling: state.customerOrder.cancelling,
    message: state.customerOrder.message,
    isConnected: state.network.isConnected,
    myMealPlan: state.myMealPlan,
    customerVirtualAccount: state.customerVirtualAccount,
    setting: state.setting.data,
    customerCards: state.customerCard.cards,
    login: state.login,
    token: state.login.token
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSetting: key => dispatch(SettingActions.fetchSetting(key)),
    fetchOrderDetail: (id, orderType) =>
      dispatch(CustomerOrderActions.fetchOrderDetail(id, orderType)),
    clearOrderDetail: () => dispatch(CustomerOrderActions.clearOrderDetail()),
    fetchVirtualAccount: () =>
      dispatch(CustomerVirtualAccountActions.fetchVirtualAccount()),
    cancelOrder: id => dispatch(CustomerOrderActions.cancelCustomerOrder(id)),
    resetErrorMessage: () => dispatch(CustomerOrderActions.resetErrorMessage())
  }
}

export default WithSafeAreaView(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CustomerMyOrderDetailScreen)
)
