import moment from 'moment'
import {
  Content,
  Container,
} from 'native-base'
import React, { Component } from 'react'
import {
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { Calendar } from 'react-native-calendars'
import Spinner from 'react-native-loading-spinner-overlay'
import Modal from 'react-native-modal'
import { connect } from 'react-redux'
import CustomerOrderActions from '../Redux/CustomerOrderRedux'
import CustomerCardActions from '../Redux/CustomerCardRedux'
import CartActions from '../Redux/CartRedux'
import ThemeActions from '../Redux/ThemeRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import AlertBox from '../Components/AlertBox'
import FooterButton from '../Components/FooterButton'
import NavigationBar from '../Components/NavigationBar'
import SmartPaymentMethodPicker from '../Components/Payment/PaymentMethodPicker'
import RoundedDropdown from '../Components/RoundedDropdown'
import SmartAddressForm from '../Components/SmartAddressForm'
import SmartDeliveryTimePicker from '../Components/SmartDeliveryTimePicker'
import WebView from '../Components/WebView'
import AvailabilityDateService from '../Services/AvailabilityDateService'
import {
  buildNewCardData,
  buildNewCCPaymentInfo,
} from '../Services/PaymentInfoService'
import XenditService from '../Services/XenditService'
import Colors from '../Themes/Colors'
import Styles from './Styles/SubscriptionOrderModifyScreenStyle'
import SalesOrderService from '../Services/SalesOrderService'

class SubscriptionOrderModifyScreen extends Component {
  constructor(props) {
    super(props)
    this.dropdownValues = JSON.parse(this.props.setting.subscription_repeat_interval)
    this.state = {
      availableDates: AvailabilityDateService.getDeliveryDateList(this.props.offDates,
        this.props.setting.delivery_cut_off),
      showCalendar: false,
      showResetAlert: false,
      showLoader: false,
      authenticationUrl: null,
      showWebView: false,
      creditCardToken: null,
      showErrorModal: false,
      selectedIntervalIndex: null,
      init: true,
      oldIndex: false,
      fetchAddress: false,
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { modifySubscriptionSuccess } = newProps
    if (modifySubscriptionSuccess) {
      this.props.navigation.goBack()
    }
  }

  componentDidMount() {
    this.props.fetchWallet()
    this.props.fetchCards()
    const { modifiedOrderDetail, lastUsed } = this.props
    const { subscription_raw_data } = modifiedOrderDetail
    const { theme_id } = subscription_raw_data
    if (theme_id) {
      this.props.fetchDetail(theme_id, lastUsed.code)
    }
  }

  componentWillUnmount() {
    this.props.clearModifiedOrderDetail()
  }

  resetButton() {
    return (
      <TouchableOpacity onPress={() => this.setState({ showResetAlert: true })} style={Styles.resetButton}>
        <Text style={Styles.resetText}>Reset</Text>
      </TouchableOpacity>
    )
  }

  onDayPress(date) {
    this.props.updateSubscriptionDefaultData('start_date', date)
    this.setState({ showCalendar: false })
  }

  onRepeatIntervalPress(selected) {
    const { repeat_interval } = this.props.modifiedOrderDetail.subscription_raw_data
    let { oldIndex } = this.state
    if (oldIndex === false) {
      oldIndex = repeat_interval
    }
    this.setState({
      selectedIntervalIndex: selected.index,
      init: false,
      oldIndex,
    }, () => {
      this.props.updateSubscriptionDefaultData('repeat_interval', selected.index)
    })
  }

  processModify() {
    const { newCreditCardInfo } = this.props.modifiedOrderDetail.subscription_raw_data
    this.setState({ showLoader: true })
    if (newCreditCardInfo != null) {
      XenditService.createToken(
        newCreditCardInfo,
        (authenticationUrl) => this.verification3DSCallBack(authenticationUrl),
        () => {
          this.setState({ showErrorModal: true })
        },
        (creditCardToken) => this.setState({ creditCardToken }),
      )
    } else {
      this.props.modifySubscription()
    }
  }

  verification3DSCallBack(authenticationUrl) {
    this.setState({ authenticationUrl },
      () => this.setState({ showLoader: false }, () => this.setState({ showWebView: true })))
  }

  creditCard3DSValidationSuccess(authData) {
    this.setState({ showLoader: true })
    const paymentInfo = this.props.modifiedOrderDetail.subscription_raw_data.newCreditCardInfo
    const { saveCreditCard } = paymentInfo

    const newPaymentInfo = buildNewCCPaymentInfo(this.state.creditCardToken, authData, paymentInfo)
    if (newPaymentInfo) {
      this.props.updateSubscriptionDefaultData('default_payment_info', newPaymentInfo)
      if (saveCreditCard) {
        const cardData = buildNewCardData(this.props.user, this.state.creditCardToken, authData, paymentInfo)
        this.props.addCard(cardData)
      }
      setTimeout(() => this.setState({ showLoader: false }, () => this.props.modifySubscription()), 1500)
    }
  }

  checkIfUsingDifferentPayment = (
    _currentPaymentMethod, modifiedPaymentMethod, currentPaymentInfo, modifiedPaymentInfo, newCreditCardInfo) => {
    if (modifiedPaymentMethod === 'credit-card') {
      if (modifiedPaymentInfo !== null || newCreditCardInfo !== null) {
        const usingNewCreditCard = newCreditCardInfo !== null
        if (usingNewCreditCard) {
          return true
        } else {
          const parsedCurrentPaymentInfo = typeof currentPaymentInfo === 'string'
            ? JSON.parse(currentPaymentInfo)
            : currentPaymentInfo
          const parsedModifiedPaymentInfo = typeof modifiedPaymentInfo === 'string'
            ? JSON.parse(modifiedPaymentInfo)
            : modifiedPaymentInfo
          if (parsedCurrentPaymentInfo.id !== parsedModifiedPaymentInfo.id) {
            return true
          }
        }
      }
    }
    if (modifiedPaymentMethod === 'wallet') {
      if (modifiedPaymentInfo !== null) {
        return true
      }
    }
    return false
  }

  enableConfirmationButton() {
    const currentRawData = this.props.currentOrderDetail.subscription_raw_data
    const modifiedRawData = this.props.modifiedOrderDetail.subscription_raw_data
    const isUsingDifferentPayment = this.checkIfUsingDifferentPayment(currentRawData.default_payment_method,
      modifiedRawData.default_payment_method, currentRawData.default_payment_info, modifiedRawData.default_payment_info,
      modifiedRawData.newCreditCardInfo)
    return currentRawData.default_delivery_address !== modifiedRawData.default_delivery_address ||
      currentRawData.default_delivery_note !== modifiedRawData.default_delivery_note ||
      currentRawData.default_delivery_latitude !== modifiedRawData.default_delivery_latitude ||
      currentRawData.default_delivery_longitude !== modifiedRawData.default_delivery_longitude ||
      currentRawData.default_recipient_name !== modifiedRawData.default_recipient_name ||
      currentRawData.default_recipient_phone !== modifiedRawData.default_recipient_phone ||
      currentRawData.start_date !== modifiedRawData.start_date ||
      currentRawData.repeat_interval !== modifiedRawData.repeat_interval ||
      currentRawData.default_delivery_time !== modifiedRawData.default_delivery_time ||
      isUsingDifferentPayment
  }

  filterDropdownValues = (dropdownConfigValues, themeDetail, selectedIntervalIndex) => {
    let { init, oldIndex } = this.state
    let dropdownValues = []
    if (themeDetail) {
      const selectedDropdown = dropdownConfigValues.find(value => value.index === selectedIntervalIndex)
      const oldSelected = dropdownConfigValues.find(value => value.index === oldIndex)
      let usedSelected
      const { max_subscription } = themeDetail
      dropdownValues = dropdownConfigValues.map(dropdown => {
        if (init) {
          usedSelected = selectedDropdown
        } else {
          usedSelected = oldSelected
        }

        if (dropdown.index === 0) {
          if (max_subscription > 90 || dropdown.index === selectedIntervalIndex || dropdown.index === oldIndex) {
            return {
              label: dropdown.label,
              index: dropdown.index,
            }
          }
        } else {
          if ((dropdown.value <= max_subscription && dropdown.value >= usedSelected.value) || dropdown.index === selectedIntervalIndex) {
            return {
              label: dropdown.label,
              index: dropdown.index,
            }
          }
        }
      })
    }

    return dropdownValues.filter((dropdown) => dropdown != null)
  }

  renderRepeatPeriodSection = () => {
    const { repeat_interval } = this.props.modifiedOrderDetail.subscription_raw_data
    let { selectedIntervalIndex } = this.state
    if (!selectedIntervalIndex) {
      selectedIntervalIndex = repeat_interval
    }
    const { theme } = this.props
    const { themeDetail } = theme
    const selectedDropdown = this.dropdownValues.find(value => value.index === selectedIntervalIndex)
    let dropdownValues = this.filterDropdownValues(this.dropdownValues, themeDetail, selectedIntervalIndex)
    return (
      <View style={Styles.repeatPeriodContainer}>
        <Text style={Styles.repeatPeriodTitleText}>Repeat For</Text>
        <View
          style={{
            marginTop: 10,
            marginBottom: 15,
          }}
        >
          {themeDetail && (
            <RoundedDropdown
              label={selectedDropdown.label}
              values={dropdownValues}
              selectedIndex={selectedIntervalIndex}
              showCheck
              onPress={selected => this.onRepeatIntervalPress(selected)}
              disabledIndex={0}
            />
          )}
        </View>
      </View>
    )
  }

  renderCalendar() {
    const selectedDate = this.props.modifiedOrderDetail.subscription_raw_data.start_date
    const availableDates = this.state.availableDates
    const disabledDates = AvailabilityDateService.disableWeekendAndOffDates(availableDates, this.props.offDates)
    const markedDates = selectedDate !== null
      ? {
        ...disabledDates,
        [selectedDate]: { selected: true },
      }
      : { ...disabledDates }
    return (
      <>
        <NavigationBar title='Select Start Date' leftSide={() => {}} />
        <Calendar
          currentDate={selectedDate}
          minDate={availableDates[0].format('YYYY-MM-DD')}
          maxDate={availableDates[availableDates.length - 1].format('YYYY-MM-DD')}
          markedDates={markedDates}
          onDayPress={(date) => this.onDayPress(date.dateString)}
          disable
          style={{ marginHorizontal: 25 }}
          theme={{
            arrowColor: Colors.green,
            monthTextColor: Colors.green,
            selectedDayTextColor: Colors.bloodOrange,
            dayTextColor: Colors.greyishBrownTwo,
            todayTextColor: Colors.bloodOrange,
            textSectionTitleColor: Colors.greyishBrownTwo,
            textMonthFontFamily: 'Rubik-Regular',
            textDayFontFamily: 'Rubik-Regular',
            textDayHeaderFontFamily: 'Rubik-Regular',
            selectedDayBackgroundColor: 'transparent',
            selectedDotColor: Colors.bloodOrange,
          }}
        />
      </>
    )
  }

  renderSubscriptionProfile() {
    const { subscription, subscription_raw_data } = this.props.modifiedOrderDetail
    const { start_date } = subscription_raw_data
    return (
      <View style={Styles.subscriptionProfileContainer}>
        <Text style={Styles.title}>Subscription Profile</Text>
        <Text style={Styles.themeName}>{subscription.theme}</Text>
        <View style={Styles.calendarSection}>
          <Text style={Styles.calendarTitleText}>Start Date</Text>
          <View style={{
            flexDirection: 'row',
            marginTop: 10,
            alignItems: 'center',
          }}>
            <Text style={Styles.startDateText}>
              {
                moment(start_date)
                  .format('DD MMM YYYY')
              }
            </Text>
          </View>
        </View>
        {this.renderRepeatPeriodSection()}
      </View>
    )
  }

  renderDeliveryDetailsSection() {
    const {
      navigation: {
        state: {
          params: {
            order_id,
          },
        },
      },
      getNewDeliveryAddress,
    } = this.props
    return (
      <View style={Styles.deliveryDetailsContainer}>
        <Text style={Styles.title}>Delivery Details</Text>
        <View style={{ marginTop: 20 }}>
          <SmartDeliveryTimePicker
            date={this.props.modifiedOrderDetail.subscription_raw_data.start_date}
            modifiedOrderDetail={this.props.modifiedOrderDetail}
            modifiedMyMealPlanDetail={this.props.modifiedMyMealPlanDetail}
            type='modify_subscription_order'
            cart={this.props.cart}
            setting={this.props.setting}
            updateSubscriptionDefaultData={this.props.updateSubscriptionDefaultData}
          />
          <SmartAddressForm
            onChangeAddressPress={() => SalesOrderService
              .getDeliveryAddress(
                this.props.token,
                this.setLoading,
                this.props.navigation,
                false,
                true,
              )}
            navigation={this.props.navigation}
            type='modify_subscription_order'
            order_id={order_id}
            cart={this.props.cart}
            modifiedOrderDetail={this.props.modifiedOrderDetail}
            updateSubscriptionDefaultData={this.props.updateSubscriptionDefaultData}
          />
        </View>
      </View>
    )
  }

  renderPaymentMethodSection() {
    return (
      <View style={Styles.paymentSectionContainer}>
        <SmartPaymentMethodPicker
          navigateToTopUpScreen={this.navigateToTopUpScreen}
          type='modify_subscription_order'
          order={this.props.order}
          wallet={this.props.wallet}
          total={0}
          setting={this.props.setting}
          modifiedOrderDetail={this.props.modifiedOrderDetail}
          customerCard={this.props.customerCard}
          setSubscriptionNewCreditCard={this.props.setSubscriptionNewCreditCard}
          updateSubscriptionDefaultData={this.props.updateSubscriptionDefaultData}
          fetchingWallet={this.props.fetchingWallet}
          modifiedMyMealPlan={this.props.modifiedMyMealPlanDetail}
        />
      </View>
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  navigateToTopUpScreen = () => {
    this.props.navigation.navigate('RootTopUpScreen', { navigationParams: 'back' })
  }

  setLoading = async (status) => {
    this.setState({
      fetchAddress: status,
    })
  }

  render() {
    const {
      showCalendar,
      showResetAlert,
      showWebView,
      showErrorModal,
      authenticationUrl,
      fetchAddress,
    } = this.state
    const {
      modifying,
      resetModifiedOrderDetail,
      modifyError,
      resetModifySubscriptionState,
      modifySubscriptionSuccess,
    } = this.props
    const enableConfirmationButton = this.enableConfirmationButton()
    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title='My Subscription'
          rightSide={() => this.resetButton()}
          leftSideNavigation={this.navigationGoBack}
        />
        <Content contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled'>
          {this.renderSubscriptionProfile()}
          {this.renderDeliveryDetailsSection()}
          {this.renderPaymentMethodSection()}
          <FooterButton text='Confirm' noIcon disabled={!enableConfirmationButton}
                        onPress={() => this.processModify()} />
          <Spinner
            visible={modifying || fetchAddress}
          />
          {authenticationUrl !== null &&
          <WebView
            visible={showWebView}
            dismiss={() => this.setState({ showWebView: false })}
            url={authenticationUrl}
            validationSuccess={(authData) => this.creditCard3DSValidationSuccess(authData)}
          />
          }
          {showCalendar && (
            <Modal
              animationIn='slideInDown'
              animationOut='slideOutUp'
              isVisible={showCalendar}
              backdropColor='white'
              backdropOpacity={0.8}
              onBackdropPress={() => this.setState({ showCalendar: false })}
              onBackButtonPress={() => this.setState({ showCalendar: false })}
              style={{
                margin: 0,
                justifyContent: 'flex-start',
              }}
            >
              {this.renderCalendar()}
            </Modal>
          )}
          {showResetAlert && (
            <AlertBox
              text='Are you sure want to reset all changes ?'
              dismiss={() => this.setState({ showResetAlert: false })}
              primaryAction={() => resetModifiedOrderDetail()}
              primaryActionText='Yes'
              secondaryAction={() => {}}
              secondaryActionText='No'
              visible={showResetAlert}
            />
          )}
          {showErrorModal && (
            <AlertBox
              text={'We can\'t process your credit card.\nPlease use another card'}
              dismiss={() => this.setState({ showErrorModal: false })}
              primaryAction={() => {}}
              primaryActionText='Ok'
              visible={showErrorModal}
            />
          )}
          {modifySubscriptionSuccess === false && (
            <AlertBox
              text={modifyError}
              dismiss={() => resetModifySubscriptionState()}
              primaryAction={() => {}}
              primaryActionText='Ok'
              visible={modifySubscriptionSuccess === false}
            />
          )}
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    offDates: state.meals.offDates,
    currentOrderDetail: state.customerOrder.orderDetail,
    modifiedOrderDetail: state.customerOrder.modifiedOrderDetail,
    setting: state.setting.data,
    user: state.login.user,
    token: state.login.token,
    modifying: state.customerOrder.modifying,
    modifySubscriptionSuccess: state.customerOrder.modifySubscriptionSuccess,
    modifyError: state.customerOrder.error,
    theme: state.theme,
    modifiedMyMealPlanDetail: state.myMealPlan.modifiedMyMealPlanDetail,
    total: state.cart.totals.grandTotal,
    order: state.order,
    customerCard: state.customerCard,
    wallet: state.customerAccount.wallet,
    fetchingWallet: state.customerAccount.fetchingWallet,
    lastUsed: state.lastUsed
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearModifiedOrderDetail: () => dispatch(CustomerOrderActions.clearModifiedOrderDetail()),
    updateSubscriptionDefaultData: (key, value) => dispatch(
      CustomerOrderActions.updateSubscriptionDefaultData(key, value)),
    resetModifiedOrderDetail: () => dispatch(CustomerOrderActions.resetModifiedOrderDetail()),
    modifySubscription: () => dispatch(CustomerOrderActions.modifySubscription()),
    resetModifySubscriptionState: () => dispatch(CustomerOrderActions.resetModifySubscriptionState()),
    addCard: (card) => dispatch(CustomerCardActions.addCard(card)),
    fetchDetail: (id, kitchenCode) => dispatch(ThemeActions.fetchDetail(id, kitchenCode)),
    getNewDeliveryAddress: (screenToReturn, useMultipleAddress, type, cartItem) => dispatch(
      CartActions.getNewAddress(screenToReturn, useMultipleAddress, type, cartItem)),
    setSubscriptionNewCreditCard: (newCreditCardInfo) => dispatch(
      CustomerOrderActions.setSubscriptionNewCreditCard(newCreditCardInfo)),
    fetchWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionOrderModifyScreen)
