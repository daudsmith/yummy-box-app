import _ from 'lodash'
import {Button, Icon, Item, Toast, Container, Content} from 'native-base'
import React, {Component} from 'react'
import {
  ActivityIndicator,
  Clipboard,
  Image,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'
import {verticalScale, scale} from 'react-native-size-matters'
import Entypo from 'react-native-vector-icons/Entypo'
import {connect} from 'react-redux'
import NavigationBar from '../Components/NavigationBar'
import OfflineModal from '../Components/OfflineModal'
import OfflinePage from '../Components/OfflinePage'
import PaymentInstructionBca from '../Components/PaymentInstructionBca'
import PaymentInstructionBni from '../Components/PaymentInstructionBni'
import PaymentInstructionBri from '../Components/PaymentInstructionBri'
import PaymentInstructionHeader from '../Components/PaymentInstructionHeader'
import PaymentInstructionMandiri from '../Components/PaymentInstructionMandiri'
import YummyboxIcon from '../Components/YummyboxIcon'
import CreditTokenTopUpActions from '../Redux/CreditTokenTopUpRedux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import CustomerVirtualAccountActions from '../Redux/CustomerVirtualAccountRedux'
import StartupActions from '../Redux/StartupRedux'
import LocaleFormatter from '../Services/LocaleFormatter'
import {Colors, Images} from '../Themes'
// Styles
import styles from './Styles/TopUpScreenStyle'
import CustomerNotificationActions from '../Redux/CustomerNotificationRedux'
import { customerAccountInterface } from './AccountScreen'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'


const themesColor = '#ff5100'

export interface accountBankInterface {
  account_number: string,
  bank_code: string
}

export interface customerVirtualAccountInterface {
  error: string, 
  fetching: boolean, 
  list: accountBankInterface[]
}

export interface waInterface {
  number: string,
  description: string
}

export interface deliveryTimeInterface {
  index: string,
  value: string,
  active: boolean,
  meal_tag: string
}

export interface featureThemeInterface {
  themeId: number,
  captionText: string
}

export interface freeDeliveryPolicyInterface {
  delivery_subtotal: number,
  delivery_item_categories: number[],
  not_free_delivery_item_categories: number[]
}

export interface subscriptionRepeatIntervalInterface {
  value: number,
  index: number,
  label: string
}

export interface dataInterface {
  app_link_faq: string, 
  app_link_privacy_policy: string, 
  app_link_terms_and_conditions: string, 
  bank_transfer_payment_option_cutoff: string, 
  cs_email_address: string, 
  cs_phone_number: string, 
  cs_whatsapp_number: waInterface, 
  delivery_cut_off: string,
  delivery_times: deliveryTimeInterface[], 
  featured_theme: featureThemeInterface, 
  finishFetch: boolean, 
  free_charge_policy: string, 
  free_delivery_policy: freeDeliveryPolicyInterface, 
  off_dates: [], 
  sales_order_total_limit: {}, 
  subscription_artwork: string, 
  subscription_minimum_wallet_balance: string, 
  subscription_repeat_interval: subscriptionRepeatIntervalInterface[], 
  version_check: object[], 
  virtual_account_banks: []
}

export interface settingInterface {
  data: dataInterface,
}

export interface creditTokenTopUpInterface {
  error: string, 
  submittingToken: boolean, 
  wallet: string
}

export interface TopUpPropsInterface {
  customerAccount: customerAccountInterface,
  customerVirtualAccount: customerVirtualAccountInterface,
  setting: settingInterface,
  resetVirtualAccount: () => void,
  creditTokenTopUp: creditTokenTopUpInterface,
  isConnected: boolean,
  hasFetchedStartupData: boolean,
  startupOnTopUpScreen: () => void,
  fetchVirtualAccount: () => void,
  getWallet: () => void,
  navigation: NavigationDrawerProp,
  submitToken: (token: string) => void,
  fetchUnreadNotifications: () => void,

}

export interface TopUpStateInterface {
  wallet: number,
  personalBalance: number,
  isClient: boolean,
  fetchingWallet: boolean,
  virtualAccounts: accountBankInterface[],
  fetchingVirtualAccount: boolean,
  banks: [],
  token: string,
  valid: boolean,
  submittingToken: boolean,
  submitTokenError: string,
  showBlankOfflinePage: boolean,
  activeAccordion: number[],
  pristineTokenInput: boolean,

}

class TopUpScreen extends Component<TopUpPropsInterface, TopUpStateInterface> {
  input

  constructor(props) {
    super(props)
    this.state = {
      wallet: this.props.customerAccount.wallet,
      personalBalance: this.props.customerAccount.personalBalance,
      isClient: this.props.customerAccount.isClient,
      fetchingWallet: this.props.customerAccount.fetchingWallet,
      virtualAccounts: this.props.customerVirtualAccount.list,
      fetchingVirtualAccount: this.props.customerVirtualAccount.fetching,
      banks: this.props.setting.data.virtual_account_banks,
      token: null,
      valid: false,
      submittingToken: this.props.creditTokenTopUp.submittingToken,
      submitTokenError: this.props.creditTokenTopUp.error,
      pristineTokenInput: true,
      showBlankOfflinePage: !this.props.isConnected,
      activeAccordion: [0],
    }
  }

  componentDidMount() {
    this.props.resetVirtualAccount()

    if (this.props.isConnected) {
      this.getTopUpScreenData()
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    this.forceUpdate()
    this.setState({
      wallet: newProps.customerAccount.wallet,
      personalBalance: newProps.customerAccount.personalBalance,
      fetchingWallet: newProps.customerAccount.fetchingWallet,
      virtualAccounts: newProps.customerVirtualAccount.list,
      fetchingVirtualAccount: newProps.customerVirtualAccount.fetching,
      banks: newProps.setting.data.virtual_account_banks,
      submitTokenError: newProps.creditTokenTopUp.error,
      submittingToken: newProps.creditTokenTopUp.submittingToken,
    })
    if (newProps.creditTokenTopUp.wallet !== null) {
      this.setState({
        wallet: newProps.creditTokenTopUp.wallet,
      })
    }
    if (!this.props.isConnected && newProps.isConnected) {
      if (!this.props.hasFetchedStartupData) {
        this.props.startupOnTopUpScreen()
        this.setState({showBlankOfflinePage: false})
      } else {
        this.getTopUpScreenData()
      }
    }
  }

  getTopUpScreenData = () => {
    this.props.fetchVirtualAccount()
    this.props.getWallet()
  }

  backButton = () => {
    const {backToFunction} = this.props.navigation.state.params

    return (
      <Button transparent onPress={() => backToFunction()}>
        <Icon name='ios-arrow-back'
              style={{fontSize: scale(35), color: themesColor}}/>
      </Button>
    )
  }

  handleTokenSubmitPress = () => {
    const {token} = this.state
    const isTokenInputEmpty = token === null || token === ''
    if (!isTokenInputEmpty) {
      this.props.submitToken(token)
      this.props.getWallet()
      this.setState({pristineTokenInput: false})
    }
  }

  _renderAccordionHeader = (section, index, isActive) => {
    if (section.title === 'token') {
      return (
        <View style={styles.methodHeader}>
          <Image
            source={Images[`topupLogo${section.title.toUpperCase()}`]}
            style={{
              backgroundColor: 'transparent',
              width: scale(120),
              height: verticalScale(35),
              marginLeft: scale(8),
            }}
            resizeMode={'contain'}
          />
          {isActive ? (
            <Entypo name={'chevron-up'} size={scale(20)}/>
          ) : (
            <Entypo name={'chevron-down'} size={scale(20)}/>
          )}
        </View>
      )
    } else {
      let logo = Images[`topupLogo${section.title.toUpperCase()}`]
      return (
        <View style={styles.methodHeader}>
          <Image source={logo}
                 style={{width: scale(120), height: verticalScale(35)}}
                 resizeMode={'contain'}/>
          {isActive ? (
            <Entypo name={'chevron-up'} size={scale(20)}/>
          ) : (
            <Entypo name={'chevron-down'} size={scale(20)}/>
          )}
        </View>
      )
    }
  }

  _renderTokenInput = () => {
    const {token, submitTokenError, pristineTokenInput, submittingToken} = this.state
    const haveError = submitTokenError !== null

    if (pristineTokenInput || submittingToken) {
      return (
        <View>
          <Item>
            <TextInput
              ref={ref => { this.input = ref }}
              onChangeText={text => {
                this.setState({token: text, pristineTokenInput: true})
              }}
              style={[styles.textInputToken, {borderBottomWidth: 0}]}
              returnKeyType={'done'}
              value={token}
              onSubmitEditing={() => this.handleTokenSubmitPress()}
              underlineColorAndroid='transparent'
              autoCapitalize='characters'
              autoCorrect={false}
            />
          </Item>
        </View>
      )
    } else {
      if (haveError) {
        return (
          <View>
            <Item error>
              <TextInput
                ref={ref => { this.input = ref }}
                onChangeText={text => {
                  this.setState({token: text, pristineTokenInput: true})
                }}
                style={styles.textInputToken}
                returnKeyType={'done'}
                value={token}
                onSubmitEditing={() => this.handleTokenSubmitPress()}
                underlineColorAndroid='transparent'
                autoCapitalize='characters'
                autoCorrect={false}
              />
              <Entypo name={'cross'} size={scale(18)}
                      style={{color: Colors.error}}/>
            </Item>
            <Text style={{
              color: Colors.error,
              fontFamily: 'Rubik-Light',
              fontSize: scale(10),
              marginTop: scale(3),
            }}>{submitTokenError}</Text>
          </View>
        )
      } else {
        return (
          <View>
            <Item success>
              <TextInput
                ref={ref => { this.input = ref }}
                onChangeText={text => {
                  this.setState({token: text, pristineTokenInput: true})
                }}
                style={styles.textInputToken}
                returnKeyType={'done'}
                value={token}
                onSubmitEditing={() => this.handleTokenSubmitPress()}
                underlineColorAndroid='transparent'
                autoCapitalize='characters'
                autoCorrect={false}
              />
              <Entypo
                name={'check'}
                size={scale(18)}
                style={{color: Colors.grennBlue}}
              />
            </Item>
            <Text style={{
              color: Colors.grennBlue,
              fontFamily: 'Rubik-Light',
              fontSize: scale(10),
              marginTop: scale(3),
            }}>
              Top Up Success
            </Text>
          </View>
        )
      }
    }
  }

  _renderAccordionContent = (section) => {
    if (section.title === 'token') {
      const {token} = this.state
      const isTokenInputEmpty = token === null || token === ''
      return (
        <View style={{
          ...styles.methodContent,
          paddingVertical: 20,
          paddingHorizontal: 20,
        }}>
          {this.state.submittingToken &&
          <ActivityIndicator
            style={{position: 'absolute', left: '50%', top: '50%'}}/>
          }
          <Text style={styles.topUpLabel}>Enter Top Up Code for
            YummyCredits</Text>
          {this._renderTokenInput()}
          <TouchableWithoutFeedback
            onPress={() => this.handleTokenSubmitPress()}>
            <View style={{
              paddingVertical: 10,
              alignSelf: 'center',
              height: null,
              width: 149,
              flex: 1,
              backgroundColor: 'white',
              borderColor: !isTokenInputEmpty
                ? Colors.grennBlue
                : Colors.pinkishGrey,
              borderWidth: 1,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 3,
              marginTop: 30,
            }}>
              <Text style={{
                color: !isTokenInputEmpty
                  ? Colors.grennBlue
                  : Colors.pinkishGrey,
                fontSize: 16,
                fontFamily: 'Rubik-Regular',
              }}>Top Up</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      )
    } else {
      let content = null
      let contentHeight = 460
      switch (section.code) {
        case 'MANDIRI':
          content = (
            <View style={{flex: 1}}>
              <PaymentInstructionHeader
                accountNumber={section.accountNumber}
                onCopy={() => this._writeToClipboard(section.accountNumber)}
              />
              <PaymentInstructionMandiri accountNumber={section.accountNumber}/>
            </View>
          )
          contentHeight = 760
          break
        case 'BNI':
          content = (
            <View style={{flex: 1}}>
              <PaymentInstructionHeader
                accountNumber={section.accountNumber}
                onCopy={() => this._writeToClipboard(section.accountNumber)}
              />
              <PaymentInstructionBni accountNumber={section.accountNumber}/>
            </View>
          )
          contentHeight = 700
          break
        case 'BRI':
          content = (
            <View style={{flex: 1}}>
              <PaymentInstructionHeader
                accountNumber={section.accountNumber}
                onCopy={() => this._writeToClipboard(section.accountNumber)}
              />
              <PaymentInstructionBri accountNumber={section.accountNumber}/>
            </View>
          )
          break
        case 'BCA':
          content = (
            <View style={{flex: 1}}>
              <PaymentInstructionHeader
                accountNumber={section.accountNumber}
                onCopy={() => this._writeToClipboard(section.accountNumber)}
              />
              <PaymentInstructionBca accountNumber={section.accountNumber}/>
            </View>
          )
          break
      }

      return (
        <View style={[styles.methodContent, {minHeight: contentHeight}]}>
          {content}
        </View>
      )
    }
  }

  _writeToClipboard = async (textToCopy) => {

    await Clipboard.setString(textToCopy)
    Toast.show({
      text: 'Copied to clipboard',
      buttonText: '',
      duration: 1500,
    })
  }

  _openHamburger = () => {
    const {isConnected, getWallet, fetchUnreadNotifications, navigation} = this.props
    if (isConnected) {
      getWallet()
      fetchUnreadNotifications()
    }
    navigation.toggleDrawer()
  }

  navigationGoBack = () => {
    const {navigation} = this.props
    navigation.goBack()
  }

  render() {
    const {wallet, fetchingWallet, virtualAccounts, showBlankOfflinePage, personalBalance, isClient} = this.state
    let banks = null
    let sections = []
    sections.push({
      title: 'token',
      content: 'token',
    })

    if (typeof this.state.banks !== 'undefined') {
      banks = this.state.banks
    }
    if (typeof virtualAccounts !== 'undefined' && virtualAccounts !== null) {
      for (let i = 0; i < virtualAccounts.length; i++) {
        const virtualAccountBankIndex = _.findIndex(banks, function(value) {
          return value.code === virtualAccounts[i].bank_code
        })
        if (virtualAccountBankIndex > -1) {
          sections.push({
            title: banks[virtualAccountBankIndex].code,
            logo: banks[virtualAccountBankIndex].logo,
            code: banks[virtualAccountBankIndex].code,
            accountNumber: virtualAccounts[i].account_number,
          })
        }
      }
    }

    let {navigationParams} = this.props.navigation.state.params

    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title='Top-Up'
          leftSideNavigation={this.navigationGoBack}
        />

        <Content style={{paddingHorizontal: scale(20)}}>
          {typeof navigationParams !== 'undefined' && !this.props.isConnected &&
          showBlankOfflinePage ?
            <OfflinePage/>
            :
            <View style={{flex: 1}}>
              <View style={styles.walletBox}>
                {fetchingWallet ? (
                  <View style={[
                    styles.balanceBox,
                    {alignItems: 'center', justifyContent: 'center'}]}>
                    <ActivityIndicator/>
                  </View>
                ) : (
                  <View>
                    <View style={styles.balanceBox}>
                      {isClient
                        ? (<Text style={styles.creditText}>Personal
                          Balance</Text>)
                        : (<Text style={styles.creditText}>Yummycredits</Text>)
                      }
                    </View>
                    <View style={styles.balanceBox}>
                      <YummyboxIcon name='payment-yumCredits' size={16}
                                    color={Colors.pinkishOrange}
                                    style={{marginTop: 5}}/>
                      {isClient
                        ? (<Text
                          style={styles.currencyText}>{LocaleFormatter.numberToCurrency(
                          personalBalance)}</Text>)
                        : (<Text
                          style={styles.currencyText}>{LocaleFormatter.numberToCurrency(
                          wallet)}</Text>)
                      }
                    </View>
                  </View>
                )}
              </View>
              <View style={styles.topUpMethodContainer}>
                <Text style={styles.topUpMethodIntroduction}>Top Up your
                  YummyCredits via ATM Bank Transfer or Mobile/Internet Banking.
                  Tap your preferred Top Up method below and follow the steps to
                  continue.</Text>
                {sections.length > 1 &&
                <Accordion
                  underlayColor={'white'}
                  sections={sections}
                  renderHeader={this._renderAccordionHeader}
                  renderContent={this._renderAccordionContent}
                  activeSections={this.state.activeAccordion}
                  onChange={(indexes) => {
                    this.setState({activeAccordion: indexes})
                  }}
                />
                }
              </View>
            </View>
          }
          {typeof navigationParams !== 'undefined' && !showBlankOfflinePage &&
          <OfflineModal isConnected={this.props.isConnected}/>}
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    customerVirtualAccount: state.customerVirtualAccount,
    customerAccount: state.customerAccount,
    setting: state.setting,
    creditTokenTopUp: state.creditTokenTopUp,
    isConnected: state.network.isConnected,
    hasFetchedStartupData: state.startup.hasFetchedStartupData,
    unreadNotification: state.customerNotification.unread,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getWallet: () => dispatch(CustomerAccountActions.fetchWallet()),
    fetchVirtualAccount: () => dispatch(
      CustomerVirtualAccountActions.fetchVirtualAccount()),
    submitToken: (code) => dispatch(CreditTokenTopUpActions.submitToken(code)),
    resetVirtualAccount: () => dispatch(
      CustomerVirtualAccountActions.resetVirtualAccount()),
    startupOnTopUpScreen: () => dispatch(StartupActions.startupOnTopUpScreen()),
    fetchUnreadNotifications: () => dispatch(
      CustomerNotificationActions.fetchTotalUnreadNotifications()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TopUpScreen)
