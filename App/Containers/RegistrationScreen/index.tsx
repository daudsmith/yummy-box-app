import React, { Component } from 'react'
import { Keyboard } from 'react-native'
import {
  SwitchActions,
} from 'react-navigation'
import { connect } from 'react-redux'
import {withNavigation} from 'react-navigation'
import AuthBackgroundView from '../../Components/Authentication/AuthBackgroundView'
import PopupBox from '../../Components/Common/PopupBox'
import RegisterActions, { initStateRegister } from '../../Redux/RegisterRedux'
// Styles
import { Colors } from '../../Themes'
import RegistrationForm, { customerInfoInterface } from './RegistrationForm'
import { LoginState } from '../../Redux/LoginRedux'
import { AppState } from '../../Redux/CreateStore'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import LastUsedActions, { baseLastUsed, Location } from '../../Redux/V2/LastUsedRedux'

export interface RegistrationScreenProps {
  navigation: NavigationDrawerProp,
  login: LoginState,
  registration: initStateRegister,
  isConnected: boolean,
  registerCustomer: (customer: customerInfoInterface) => void,
  resetRegistrationMessage: () => void,
  lastUsed: baseLastUsed,
  addLastUsed: (code: string, location: Location, name: string, address: string) => void,
}

export interface RegistrationScreenState {
  showPhoneVerificationModal: boolean,
  showErrorModal: boolean,
  errorMessage: string,
  loading: boolean,
  socialLoginType: string,
  scrollViewHeight?: number,
  registrationError: string,
  verifyOtpMessage: string,
  salutationLineColor: string,
}

class RegistrationScreen extends Component<RegistrationScreenProps, RegistrationScreenState> {
  keyboardDidShowListener
  keyboardDidHideListener

  constructor(props) {
    super(props)
    this.state = {
      showPhoneVerificationModal: false,
      showErrorModal: false,
      errorMessage: '',
      loading: false,
      socialLoginType: null,
      scrollViewHeight: null,
      registrationError: null,
      verifyOtpMessage: null,
      salutationLineColor: Colors.lightGrey,
    }
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.changeScrollViewHeight)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.changeScrollViewHeight)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  componentDidUpdate(prevProps: Readonly<RegistrationScreenProps>): void {
    const { emailPhoneExist, validatingEmailPhone } = this.props.registration

    if (this.props.login.token && prevProps.login.token === null) {
      this.toggleRegLoading(false)
      this.resetAndNavigateTo()
    }

    if (emailPhoneExist !== null && !validatingEmailPhone) {
      if (emailPhoneExist) {
        this.setState({
          loading: false,
          showErrorModal: true,
          errorMessage: 'Email or phone has been registered',
        })
      }
    }

    if (this.props.registration.registrationError !== prevProps.registration.registrationError
      && this.props.registration.registrationError
    ) {
      this.setState({
        loading: false,
        showErrorModal: true,
        errorMessage: this.props.registration.registrationError,
      })
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { emailPhoneExist, validating } = newProps.registration

    if (newProps.login.token !== null) {
      this.toggleRegLoading(false)
      this.resetAndNavigateTo()
    }

    if (emailPhoneExist !== null && !validating) {
      if (emailPhoneExist) {
        this.setState({
          loading: false,
          showErrorModal: true,
          errorMessage: 'Email or phone has been registered',
        })
      }
    }

    if (newProps.registration.registrationError) {
      this.setState({
        loading: false,
        showErrorModal: true,
        errorMessage: newProps.registration.registrationError,
      })
    }
  }

  resetAndNavigateTo() {
    const { lastUsed, navigation, addLastUsed } = this.props
    if (lastUsed.code !== '') {
      navigation.dispatch(
        SwitchActions.jumpTo({
          routeName: 'NavigationDrawer',
        })
      )
    } else {
      navigation.navigate('SearchLocation', {noSearchLocation: true})
      addLastUsed(null, null, null, null)
    }
  }

  showErrorModal = (show: boolean, msg: string) => {
    this.setState({
      showErrorModal: show,
      errorMessage: msg
    })
  }

  toggleRegLoading = (loading: boolean) => {
    this.setState({
      loading
    })
  }

  changeScrollViewHeight = () => {
    let { scrollViewHeight } = this.state
    scrollViewHeight === null ? this.setState({ scrollViewHeight: 600 }) : this.setState({ scrollViewHeight: null })
  }

  closeAlert = () => {
    if (this.state.errorMessage === 'Email address or phone is already taken') {
      this.props.resetRegistrationMessage()
    }

    setTimeout(() => {
      this.setState({
        showErrorModal: false,
        errorMessage: '',
      })
    }, 100)
  }

  render() {
    const { showErrorModal, errorMessage, loading, socialLoginType } = this.state
    const { navigation, registerCustomer } = this.props
    const { params } = this.props.navigation.state
    return (
      <AuthBackgroundView
        boxTitle='Register'
        navigation={navigation}
      >
        <RegistrationForm
          params={params}
          showErrorModal={this.showErrorModal}
          submitAction={registerCustomer}
          socialLoginType={socialLoginType}
          toggleLoading={this.toggleRegLoading}
          loading={loading}
        />

        <PopupBox
          visible={showErrorModal}
          content={errorMessage}
          contentButton={{
            text: 'Close',
            onPress: this.closeAlert
          }}
        />
      </AuthBackgroundView>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    login: state.login,
    registration: state.register,
    isConnected: state.network.isConnected,
    lastUsed: state.lastUsed,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerCustomer: (customer) => dispatch(RegisterActions.registerCustomer(customer)),
    resetRegistrationMessage: () => dispatch(RegisterActions.resetRegistrationErrorMessage()),
    addLastUsed: (code: string, location: Location, name: string, address: string) => dispatch(LastUsedActions.addLastUsed(code, location, name, address))

  }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(RegistrationScreen))
