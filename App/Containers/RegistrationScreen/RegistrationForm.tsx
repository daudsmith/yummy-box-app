import * as React from 'react'
import { View } from 'react-native'
import FlexButton from '../../Components/Common/FlexButton'
import TextInput from '../../Components/Common/TextInput'
import TextInputWithSalutation from '../../Components/Common/TextInputWithSalutation'
import PhoneNumberTextInput from '../../Components/PhoneNumberTextInput'
import LocaleFormatter from '../../Services/LocaleFormatter'
import { Colors } from '../../Themes'
import { verticalScale } from 'react-native-size-matters'
import { NavigationParams } from 'react-navigation'

export interface RegistrationFormProps {
  params: NavigationParams,
  showErrorModal: (show: boolean, message: string) => void,
  submitAction: (userInfo: customerInfoInterface) => void,
  toggleLoading?: (toggle: boolean) => void,
  socialLoginType?: string,
  loading?: boolean,
}

export interface RegistrationFormState {
  customer: customerInterface,
  socialLoginType: string,
  fieldValidity: fieldValidityInterface,
  label: string,
  salutationLineColor?: string,
}

export interface customerInterface {
  salutation: string,
  firstName: string,
  lastName: string,
  email: string,
  phone: string,
  password: string,
  confirmPassword: string,
  countryCode: string,
  registrationType: string,
  facebook_id?: string,
  facebookCredentials?: any,
}

export interface customerInfoInterface {
  name: string,
  salutation: string,
  first_name: string,
  last_name: string,
  email: string,
  phone: string,
  password: string,
  password_confirmation: string,
  social_login_type: string,
  social_login_id: string,
  facebook_credentials: any,
  countryCode: string,
  registration_type: string,
}

export interface fieldValidityInterface {
  salutation: validationInterface,
  firstName: validationInterface,
  lastName: validationInterface,
  email: validationInterface,
  phone: validationInterface,
  password: validationInterface,
  confirmPassword: validationInterface
}

export interface validationInterface {
  isValid: boolean
}

const validField = { isValid: true }
const inValidField = { isValid: false }
const initialField = { isValid: undefined }

export default class RegistrationForm extends React.Component<RegistrationFormProps, RegistrationFormState> {
  firstName
  phoneNumber
  lastName

  static defaultProps = {
    loading: false,
  }

  constructor (props) {
    super(props)
    const { params } = props
    const emailInitialValidity = params.email === '' ? initialField : validField
    const phoneInitialValidity = params.phone === '+62' ? initialField : validField
    const countryCode = params.phone !== '' ? '+' + params.phone.slice(0, 2) : null
    const fieldValidity = {
      salutation: initialField,
      firstName: initialField,
      lastName: initialField,
      email: emailInitialValidity,
      phone: phoneInitialValidity,
      password: initialField,
      confirmPassword: initialField
    }
    const customer = {
      salutation: '',
      firstName: '',
      lastName: '',
      email: params.email,
      phone: params.phone,
      password: '',
      confirmPassword: '',
      countryCode: countryCode,
      registrationType: params.registrationType,
    }

    this.state = {
      customer: customer,
      socialLoginType: null,
      fieldValidity,
      label: 'Mr./Ms.',
    }
  }

  handleFieldValueChange = (field: string, text: string) => {
    const { customer, fieldValidity } = this.state
    const newCustomer = { ...customer, [field]: text }
    const newFieldValidity = { ...fieldValidity, [field]: initialField }
    this.setState({
      customer: newCustomer,
      fieldValidity: newFieldValidity
    })
  }

  handleSalutation = (salutation, index) => {
    const changeFormatSalutation = salutation.substring(0, salutation.length - 1).toUpperCase()
    const { customer, fieldValidity } = this.state
    const salutationValidity = fieldValidity.salutation
    const newSalutation = { ...customer, salutation: changeFormatSalutation }
    this.setState({
      label: ''
    })
    const lineColor = index === -1 || changeFormatSalutation === '' ? Colors.lightGrey : Colors.brownishGrey

    if (changeFormatSalutation === '') {
      const newSalutationValidity = { ...salutationValidity, ...inValidField }
      const newFieldValidity = { ...fieldValidity, salutation: newSalutationValidity }
      this.setState({
        customer: newSalutation,
        fieldValidity: newFieldValidity,
        salutationLineColor: lineColor,
      })
    } else {
      const newSalutationValidity = { ...salutationValidity, ...initialField }
      const newFieldValidity = { ...fieldValidity, salutation: newSalutationValidity }
      this.setState({
        customer: newSalutation,
        fieldValidity: newFieldValidity,
        salutationLineColor: lineColor,
      })
    }
    setTimeout(() => this.firstName.focus(), 500)
  }

  validateForm = (blackListKeys = []) => {
    const { customer, fieldValidity } = this.state
    let newFieldValidity = { ...fieldValidity }
    let isFormValid = true

    for (let key in fieldValidity) {
      if (fieldValidity.hasOwnProperty(key)) {
        if (blackListKeys.indexOf(key) < 0) {
          if (customer[key] === '') {
            newFieldValidity = { ...newFieldValidity, [key]: inValidField }
            isFormValid = false
          } else {
            newFieldValidity = { ...newFieldValidity, [key]: initialField }
          }
        }
      }
    }

    newFieldValidity = {
      ...newFieldValidity,
      email: LocaleFormatter.isEmailFormatValid(customer.email) ? validField : inValidField
    }
    newFieldValidity = {
      ...newFieldValidity,
      phone: this.phoneNumber.isValidNumber(customer.phone) ? validField : inValidField
    }

    const { email, phone } = newFieldValidity
    if (!email.isValid || !phone.isValid) {
      isFormValid = false
    }

    return { newFieldValidity, isFormValid }
  }

  handleSubmit = () => {
    this.props.toggleLoading(false)
    const { email, phone, firstName, lastName, password, confirmPassword, salutation, facebook_id, facebookCredentials, countryCode, registrationType } = this.state.customer
    const blackListKeys = ['email', 'phone']
    const { newFieldValidity, isFormValid } = this.validateForm(blackListKeys)
    let fieldValidity = newFieldValidity

    if (isFormValid) {
      if (password !== confirmPassword) {
        fieldValidity = { ...fieldValidity, confirmPassword: inValidField }
        this.props.showErrorModal(true, 'Password and retype password did not match')
      } else {
        const customerInfo = {
          name: `${firstName} ${lastName}`,
          salutation: salutation,
          first_name: firstName,
          last_name: lastName,
          email: email,
          phone: phone,
          password: password,
          password_confirmation: confirmPassword,
          social_login_type: this.props.socialLoginType,
          social_login_id: facebook_id,
          facebook_credentials: facebookCredentials,
          countryCode: countryCode,
          registration_type: registrationType,
        }
        this.props.toggleLoading(true)
        this.props.submitAction(customerInfo)
      }
    } else {
      this.props.showErrorModal(true, 'Please fill all fields!')
    }
    this.setState({
      fieldValidity
    })
  }

  handlePhoneChanged = (phone, countryCode, isValidNumber) => {
    const { customer, fieldValidity } = this.state
    const phoneValidity = fieldValidity.phone
    const newCustomer = { ...customer, phone: phone, countryCode: countryCode }

    if (!isValidNumber) {
      const newPhoneValidity = { ...phoneValidity, ...inValidField }
      const newFieldValidity = { ...fieldValidity, phone: newPhoneValidity }
      this.setState({
        customer: newCustomer,
        fieldValidity: newFieldValidity
      })
    } else {
      const newPhoneValidity = { ...phoneValidity, ...validField }
      const newFieldValidity = { ...fieldValidity, phone: newPhoneValidity }
      this.setState({
        customer: newCustomer,
        fieldValidity: newFieldValidity
      })
    }
  }

  render () {
    const { fieldValidity, customer } = this.state
    const { salutation, firstName, lastName, phone, email, password, confirmPassword } = customer
    const { loading } = this.props
    return (
      <View>
        <TextInputWithSalutation
          label='First Name'
          salutation={salutation}
          value={firstName}
          onSalutationChange={this.handleSalutation}
          onTextFieldChange={(text) => this.handleFieldValueChange('firstName', text)}
          setTextFieldRef={(ref) => this.firstName = ref}
          onTextFieldSubmitEditing={() => this.lastName.focus()}
          salutationError={fieldValidity.salutation.isValid === false}
          textFieldError={fieldValidity.firstName.isValid === false}
          testID="firstNameField"
          accessibilityLabel="firstNameField"
        />
        <View style={{ height: verticalScale(26) }}/>
        <TextInput
          text='Last Name'
          value={lastName}
          onChangeText={(text) => this.handleFieldValueChange('lastName', text)}
          setRef={(ref) => this.lastName = ref}
          returnKeyType='next'
          useValidator={fieldValidity.lastName.isValid === false ? 'plain' : undefined}
          testID="lastNameField"
          accessibilityLabel="lastNameField"
        />
        <View style={{ height: verticalScale(26) }}/>
        <PhoneNumberTextInput
          value={phone}
          setRef={ref => this.phoneNumber = ref}
          onChange={this.handlePhoneChanged}
          testID="phoneNumberField"
          accessibilityLabel="phoneNumberField"
        />
        <View style={{ height: verticalScale(13) }}/>
        <TextInput
          text='Email'
          value={email}
          onChangeText={(text) => this.handleFieldValueChange('email', text)}
          returnKeyType='next'
          useValidator='green'
          validator={(value) => LocaleFormatter.isEmailFormatValid(value)}
          testID="emailField"
          accessibilityLabel="emailField"
        />
        <View style={{ height: verticalScale(26) }}/>
        <TextInput
          text='Password'
          value={password}
          secureTextEntry
          onChangeText={(text) => this.handleFieldValueChange('password', text)}
          returnKeyType='next'
          useValidator={fieldValidity.password.isValid === false ? 'plain' : undefined}
          testID="passwordField"
          accessibilityLabel="passwordField"
        />
        <View style={{ height: verticalScale(26) }}/>
        <TextInput
          text='Retype Password'
          value={confirmPassword}
          secureTextEntry
          onChangeText={(text) => this.handleFieldValueChange('confirmPassword', text)}
          returnKeyType='done'
          lineColor={fieldValidity.confirmPassword.isValid === false ? Colors.red : undefined}
          testID="passwordConfirmationField"
          accessibilityLabel="passwordConfirmationField"
        />
        <View style={{ height: verticalScale(29) }}/>
        <FlexButton
          text='Register'
          buttonStyle={{ backgroundColor: Colors.bloodOrange }}
          textStyle={{ color: 'white' }}
          onPress={this.handleSubmit}
          disableShadow
          disabled={loading}
          loading={loading}
          testID="registerButton"
          accessibilityLabel="registerButton"
        />
      </View>
    )
  }
}
