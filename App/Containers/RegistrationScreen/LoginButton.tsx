import * as React from 'react'
import { Text } from 'native-base'
import { TouchableOpacity, View } from 'react-native'
import { Colors } from '../../Themes'
import Styles from '../Styles/RegistrationScreenStyle'
import { NavigationDrawerProp } from 'react-navigation-drawer'

export interface LoginButtonProps {
  navigation: NavigationDrawerProp
}

const LoginButton: React.FC<LoginButtonProps> = (props) => {
  return (
    <View
      style={Styles.loginButtonContainer}
    >
      <Text style={Styles.loginButtonText}>
        Already a member?
      </Text>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('Login')}
      >
        <Text
          style={{
            ...Styles.loginButtonText,
            color: Colors.bloodOrange,
            textDecorationLine: 'underline'
          }}
        >
          Log In
        </Text>
      </TouchableOpacity>
    </View>
  )
}

export default LoginButton
