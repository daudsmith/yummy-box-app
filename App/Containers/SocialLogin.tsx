import React from 'react'
import { Platform, Text, View, Alert, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
  NavigationActions,
} from 'react-navigation'
import { connect } from 'react-redux'
import jwtDecode, { JwtPayload } from 'jwt-decode'
import LoginActions, { LoginState, User } from '../Redux/LoginRedux'
import RegisterActions from '../Redux/RegisterRedux'
import CustomerApi from '../Services/ApiCustomer'
import Styles from './Styles/SocialLoginScreenStyles'
import { AppState } from '../Redux/CreateStore'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import { scale } from 'react-native-size-matters'
import {
  SwitchActions,
} from 'react-navigation'
import { appleAuth } from '@invertase/react-native-apple-authentication'
import SocialButton from '../Components/Common/SocialButton'
export interface customerInfo {
  name: string,
  salutation: string,
  first_name: any,
  last_name: any,
  email: any,
  phone: any,
  password: any,
  password_confirmation: any,
  social_login_type: string,
  social_login_id: any,
  facebook_credentials: string,
  countryCode: any,
}

export interface SocialLoginScreenProps {
  navigation: NavigationDrawerProp
  user: User,
  loginState: LoginState,
  loginError: string,
  register: (customer: customerInfo) => void,
  onLoginSuccess: (data: User, token: string) => void
}

export interface SocialLoginScreenState {
  username: string,
  password: string,
  scrollEnabled: boolean,
  spinnerVisible: boolean,
  loading: boolean,
  loginType: string
}

export interface JwtInterface extends JwtPayload {
  email: string
}

const FBSDK = require('react-native-fbsdk')
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken,
} = FBSDK

class SocialLogin extends React.Component<SocialLoginScreenProps, SocialLoginScreenState> {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      scrollEnabled: false,
      spinnerVisible: false,
      loading: false,
      loginType: ''
    }
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    if(newProps.registering){
      this.setState({
        loading: newProps.registering,
        loginType: 'apple'
      })
    }
    if (newProps.loginState.token !== null) {
      this.props.navigation.dispatch(
        SwitchActions.jumpTo({
          routeName: 'NavigationDrawer'
        })
      )
    }
  }

  navigateToPhoneVerification = (customer, login_type) => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'PhoneVerificationScreen',
      params: {
        customer: customer,
        socialLoginType: login_type
      }
    })
    this.props.navigation.dispatch(navigateAction)
    this.setState({
      spinnerVisible: false
    })
  }

  toggleSpinner = () => {
    this.setState({
      spinnerVisible: !this.state.spinnerVisible
    })
  }

  costumerData = (result, credentials, login_type) => {
    return {
      salutation: '',
      first_name: login_type === 'facebook' ? result.first_name : result.fullName.givenName,
      last_name: login_type === 'facebook' ? result.last_name : result.fullName.familyName,
      email: result.email,
      password: login_type === 'facebook' ? result.id : result.user,
      confirmPassword: login_type === 'facebook' ? result.id : result.user,
      facebookCredentials: JSON.stringify(credentials),
      isRegisterWithFacebook: login_type === 'facebook' && true,
      phone: null,
      social_id: login_type === 'facebook' ? result.id : result.user,
      countryCode: result.countryCode
    }
  }

  handleFacebookLogin = () => {
    let that = this
    this.toggleSpinner()
    // reset facebook sdk login status
    if (this._isFacebookLoggedIn()) {
      LoginManager.logOut()
    }

    LoginManager.logInWithPermissions(['email', 'public_profile']).then(
      function(result) {
        if (result.isCancelled) {
          that.toggleSpinner()
        } else {
          let req = new GraphRequest(
            '/me',
            {
              httpMethod: 'GET',
              version: 'v3.1',
              parameters: {
                fields: {
                  string: 'name,email,first_name,last_name'
                }
              }
            },
            that._infoCallback
          )
          new GraphRequestManager().addRequest(req).start()
        }
      },
      function() {
        that.toggleSpinner()
      }
    )
  }

  _infoCallback = (error, result, login_type = 'facebook') => {
    if (error) {
      this.toggleSpinner()
    } else {
      const customerApi = CustomerApi.create()
      customerApi.validate({
        login_type,
        email: result.email ? result.email : result.user,
        facebook_id: result.id,
        apple_id: result.user
      }).then((response) => {
        return response.data
      }).then((responseData) => {
        if (responseData.error) {
          this.toggleSpinner()
          Alert.alert(responseData.message)
          return
        }
        if (!responseData.exist) {
          const credentials = AccessToken.getCurrentAccessToken()
          const customer = this.costumerData(result, credentials, login_type)
          if(login_type === 'apple'){
            const decoded = jwtDecode<JwtInterface>(result.identityToken)
            const customerInfo = {
              name: `${customer.first_name} ${customer.last_name}`,
              salutation: customer.salutation,
              first_name: customer.first_name,
              last_name: customer.last_name,
              email: customer.email ? customer.email : decoded.email,
              phone: customer.phone,
              password: customer.password,
              password_confirmation: customer.confirmPassword,
              social_login_type: login_type,
              social_login_id: customer.social_id,
              facebook_credentials: customer.facebookCredentials,
              countryCode: customer.countryCode,
            }
            this.props.register(customerInfo)
          } else {
            this.navigateToPhoneVerification(customer, login_type)
          }
        } else {
          const email = result.email ? result.email : result.user
          const password = login_type === 'facebook' ? result.id : result.user
          this.setState({ loading: true, loginType: login_type })
          customerApi.auth(email, password)
          .then((response) => {
            return response.data
          }).then((responseData) => {
            this.setState({ loading: false })
            if (!responseData.error) {
              const {data, token} = responseData
              this.props.onLoginSuccess(data, token)
            } else {
              Alert.alert(responseData.message)
            }
          }).catch(() => {
            this.toggleSpinner()
            this.setState({ loading: false })
          })
        }
      }).catch((error) => {
        if (error) {
          this.toggleSpinner()
        }
      })
    }
  }

  onAppleButtonPress = async () => {
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [
          appleAuth.Scope.EMAIL,
          appleAuth.Scope.FULL_NAME
        ],
      })

      const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user)
      
      if (credentialState === appleAuth.State.AUTHORIZED) {
        this._infoCallback(null, appleAuthRequestResponse, 'apple')
      } 

    } catch (error) {
      if (error.code !== appleAuth.Error.CANCELED) {
        this.toggleSpinner()
      } 
    }
  }

  _isFacebookLoggedIn = () => {
    const accessToken = AccessToken.getCurrentAccessToken()
    return accessToken != null
  }

  render () {
      if(Platform.OS === 'ios'){
        return (
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
              <SocialButton
                buttonStyle={Styles.buttonFbIos}
                onPress={() => this.handleFacebookLogin()}
                testID="signInFbButton"
                accessibilityLabel="signInFbButton"
              >
              {
                this.state.loading && this.state.loginType === 'facebook' ?
                <ActivityIndicator color='white' /> :
                <>
                <Icon
                  name='facebook'
                  size={scale(19)}
                  color='white'
                />
                <Text style={Styles.textSocial}>Sign in with Facebook</Text>
                </>
              }
              </SocialButton>
              <SocialButton
                buttonStyle={Styles.buttonIos}
                onPress={() => this.onAppleButtonPress()}
                testID="signInAppleButton"
                accessibilityLabel="signInAppleButton"
              >
              {
                this.state.loading && this.state.loginType === 'apple' ? 
                <ActivityIndicator color='white' /> : 
                <>
                  <Icon
                    name='apple'
                    size={scale(20)}
                    color='white'
                  />
                  <Text style={Styles.textSocial}>Sign in with Apple</Text>
                </>
              }
              </SocialButton>
            </View>
        )}  else {
            return (
                <SocialButton
                buttonStyle={Styles.buttonFb}
                onPress={() => this.handleFacebookLogin()}
                testID="signInFbButton"
                accessibilityLabel="signInFbButton"
              >
              {
                this.state.loading && this.state.loginType === 'facebook' ?
                <ActivityIndicator color='white' /> :
                <>
                <Icon
                  name='facebook'
                  size={scale(19)}
                  color='white'
                />
                <Text style={Styles.textSocial}>Sign in with Facebook</Text>
                </>
              }
              </SocialButton>
            )
      }        

  }
}

const mapStateToProps = (state: AppState) => {
  return {
    user: state.login.user,
    loginState: state.login,
    loginError: state.login.error,
    registering: state.register.registering
  }
}

const mapDispatchToProps = dispatch => {
  return {
    register: (customer) => dispatch(RegisterActions.registerCustomer(customer)),
    onLoginSuccess: (user, token) => dispatch(LoginActions.onLoginSuccess(user, token)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SocialLogin)
