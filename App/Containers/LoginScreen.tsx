import * as React from 'react'
import {
  Keyboard,
  LayoutAnimation,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native'
import { connect } from 'react-redux'
import { withNavigation } from 'react-navigation'
import AuthBackgroundView from '../Components/Authentication/AuthBackgroundView'
import FlexButton from '../Components/Common/FlexButton'
import TextInput from '../Components/Common/TextInput'
import LoginActions, { LoginState, User } from '../Redux/LoginRedux'
import LocaleFormatter from '../Services/LocaleFormatter'
import { Colors } from '../Themes'
import Styles from './Styles/LoginScreenStyles'
import { AppState } from '../Redux/CreateStore'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { verticalScale } from 'react-native-size-matters'
import { SwitchActions } from 'react-navigation'
import BoxBottom from '../Components/BoxBottom'
import SocialLogin from './SocialLogin'
import ApiCustomerOrder from '../Services/ApiCustomerOrder'
import LastUsedActions, { baseLastUsed, Location } from '../Redux/V2/LastUsedRedux'
import ApiKitchen from '../Services/V2/ApiKitchen'
import GoogleMatrixService from '../Services/GoogleMatrixService'
import SearchLocationService from '../Services/SearchLocationService'

export interface LoginScreenProps {
  navigation: NavigationDrawerProp
  user: User,
  loginState: LoginState,
  loginError: string,
  attemptingLogin: boolean,
  isConnected: boolean,
  attemptLogin: (username: string, password: string) => void,
  clearErrorMessage: () => void,
  lastUsed: baseLastUsed,
  token: string,
  addLastUsed: (code: string, location: Location, name: string, address: string) => void,
}

export interface LoginScreenState {
  username: string,
  password: string,
  loading: boolean,
  scrollEnabled: boolean,
  alertVisible: string,
  validationEmail: boolean,
  errorValidation: string,
}

class LoginScreen extends React.Component<LoginScreenProps, LoginScreenState> {
  username
  password
  keyboardDidShowListener
  keyboardDidHideListener

  constructor(props) {
    super(props)
    this.state = {
      username: __DEV__
        ? 'antonius2@yummycorp.com'
        : '',
      password: __DEV__
        ? '123456'
        : '',
      loading: false,
      scrollEnabled: false,
      alertVisible: 'flex',
      validationEmail: false,
      errorValidation: '',
    }
  }

  componentDidUpdate(prevProps: Readonly<LoginScreenProps>): void {
    if (this.props.user && !prevProps.user) {
      this.navigateTo()
    }
    if (this.props.loginError !== null && prevProps.loginError === null) {
      this.setState({
        password: '',
      }, this.deleteErrorMessage)
    }
  }

  componentDidMount() {
    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
      this.keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
      this.keyboardDidHide)
    // for clear login reducer error message, every time user go to this scene
    this.props.clearErrorMessage()
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      scrollEnabled: true,
    })
  }

  keyboardDidHide = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      scrollEnabled: false,
    })
  }

  isNotPhoneNumber = (username) => {
    return isNaN(parseInt(username, 10))
  }

  checkValidation = (focusOnNextField) => {
    const { username } = this.state
    if (username === '') {
      return false
    }
    if (this.isNotPhoneNumber(username)) {
      if (!LocaleFormatter.isEmailFormatValid(username)) {
        this.setState({
          errorValidation: 'Invalid email format',
        }, () => {
          this.username.focus()
          this.deleteErrorMessage()
        })
        return false
      }
    } else {
      if (!this.checkPhoneNumber(username)) {
        this.setState({
          errorValidation: 'Invalid email format',
        }, () => {
          this.username.focus()
          this.deleteErrorMessage()
        })
        return false
      }
    }

    this.setState({
      validationEmail: true,
    }, () => {
      if (focusOnNextField) {
        this.password.focus()
      }
    })
  }

  handlePressLogin = () => {
    const { username, password } = this.state

    if (this.props.attemptingLogin) {
      return false
    }

    if (!username || !password) {
      this.setState({
        errorValidation: 'Username and Password cant empty',
      }, this.deleteErrorMessage)
      return false
    }
    let isValidInput = this.checkPhoneNumber(username)
    if (this.isNotPhoneNumber(username)) {
      isValidInput = LocaleFormatter.isEmailFormatValid(username)
    }

    if (isValidInput) {
      this.props.attemptLogin(username, password)
    }
  }

  deleteErrorMessage = () => {
    setTimeout(() => {
      this.setState({
        password: '',
        errorValidation: '',
      }, this.props.clearErrorMessage)
    }, 3000)
  }

  checkPhoneNumber = (phone) => {
    let phoneRegex = /^[0-9]+$/
    return !!phone.match(phoneRegex)
  }

  navigateTo = () => {
    const { lastUsed, token, addLastUsed, navigation } = this.props
    if (lastUsed.code !== '') {
      navigation.dispatch(
        SwitchActions.jumpTo({
          routeName: 'NavigationDrawer',
        })
      )
    } else {
      ApiCustomerOrder.create()
        .getLastOrder(token)
        .then((response) => response.data)
        .then((responseBody) => responseBody.data)
        .then((lastOrder) => {
          if (lastOrder.deliveries.data.length > 0) {
            const latitude = parseFloat(lastOrder.deliveries.data[0].latitude)
            const longitude = parseFloat(lastOrder.deliveries.data[0].longitude)
            const location = {
              latitude,
              longitude
            }
            const name = lastOrder.deliveries.data[0].landmark
            const code = lastOrder.deliveries.data[0].kitchen_code
            const address = lastOrder.deliveries.data[0].address
            if (code) {
              addLastUsed(code, location, name, address)
              navigation.dispatch(
                SwitchActions.jumpTo({
                  routeName: 'NavigationDrawer',
                })
              )
            } else {
              ApiKitchen.create()
                .kitchenList(latitude, longitude)
                .then(async (response) => {
                  const data = response.data.data
                  if (data) {
                    if (data.length > 1) {
                      const destination = data.map(item => {
                        return `${item.latitude},${item.longitude}`
                      }).join('|')
                      await GoogleMatrixService.checkGoogleMatrix(latitude, longitude, destination).then(index => {
                        addLastUsed(data[index].code, location, name, address)
                      })
                    } else {
                      addLastUsed(data[0].code, location, name, address)
                    }
                    navigation.dispatch(
                      SwitchActions.jumpTo({
                        routeName: 'NavigationDrawer',
                      })
                    )
                  } else {
                      SearchLocationService.selectKitchen(token, this.props.navigation, addLastUsed)
                  }
                })
            }
          }

        }).catch(() => {
          SearchLocationService.selectKitchen(token, this.props.navigation, addLastUsed)
        })
    }
  }

  showErrorMessage = () => {
    const { loginError } = this.props
    const { errorValidation } = this.state
    const hasError = (
      loginError || errorValidation
    )
    let errorMessage = loginError
    if (errorValidation && !loginError) {
      errorMessage = errorValidation
    }

    if (hasError) {
      return (
        <View style={{ marginTop: verticalScale(15) }}>
          <View>
            <Text style={Styles.errorText}>
              <Text
                style={{
                  ...Styles.errorText,
                  fontFamily: 'Rubik-Bold',
                }}
              >ERROR: </Text>
              {errorMessage}
            </Text>
          </View>
        </View>
      )
    }
    return null
  }

  renderLoginBox = () => {
    const { username, password, errorValidation } = this.state
    const { attemptingLogin, loginError, isConnected, navigation } = this.props
    const disabledLoginButtonStyle = (
      !isConnected
    )
      ? { backgroundColor: Colors.pinkishGrey }
      : {}
    const hasError = !!(
      loginError || errorValidation
    )
    return (
      <View>
        <TextInput
          text='Email or Phone Number'
          autoCapitalize='none'
          setRef={ref => this.username = ref}
          onChangeText={(text) => this.setState({ username: text })}
          onSubmitEditing={() => this.checkValidation(true)}
          onBlur={() => this.checkValidation(false)}
          value={username}
          testID='emailMobileNumberField'
          accessibilityLabel="emailMobileNumberField"
        />
        <View style={{ height: verticalScale(22) }} />
        <TextInput
          fromLogin={true}
          text='Password'
          autoCapitalize='none'
          setRef={ref => this.password = ref}
          returnKeyType='go'
          onChangeText={(text) => this.setState({ password: text })}
          onSubmitEditing={this.handlePressLogin}
          value={password}
          textInputStyle={{ marginBottom: Platform.OS === 'ios' ? verticalScale(-13) : verticalScale(-9) }}
          testID='passwordField'
          accessibilityLabel="passwordField"
        />
        {hasError && this.showErrorMessage()}
        {this.renderForgotPasswordButton()}
        <FlexButton
          text={attemptingLogin
            ? ''
            : 'Login'}
          loading={attemptingLogin && true}
          disableShadow
          disabled={((username === '' && password === '') || !isConnected || attemptingLogin)}
          buttonStyle={{
            backgroundColor: (username && password) || attemptingLogin ? Colors.primaryOrange : Colors.primaryGrey,
            paddingVertical: verticalScale(12),
            marginTop: verticalScale(20),
            ...disabledLoginButtonStyle,
          }}
          textStyle={{ color: 'white' }}
          onPress={this.handlePressLogin}
          testID="signInButton"
          accessibilityLabel="signInButton"
        />
        <View style={Styles.altContainer}>
          <Text style={Styles.textAlt}>or login with</Text>
        </View>
        <SocialLogin navigation={navigation} />
      </View>
    )
  }

  renderForgotPasswordButton = () => {
    return (
      <View style={{ marginTop: 20 }}>
        <TouchableOpacity
          testID="forgotPasswordLink"
          accessibilityLabel="forgotPasswordLink"
          onPress={() => this.props.navigation.navigate('ForgotPasswordScreen')}
          style={Styles.forgetPasswordButton}>
          <Text style={Styles.forgetPasswordText}>Forgot Password?</Text>
        </TouchableOpacity>
      </View>

    )
  }


  render() {
    const { navigation } = this.props
    return (
      <AuthBackgroundView
        boxTitle='LOGIN'
        navigation={this.props.navigation}
        boxBottomComponent={(
          <BoxBottom
            navigation={navigation}
            primaryText="Don’t have an account?"
            secondText="Register"
            to="EmailPhoneVerificationScreen"
          />
        )}
      >
        {this.renderLoginBox()}
      </AuthBackgroundView>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    user: state.login.user,
    loginState: state.login,
    loginError: state.login.error,
    attemptingLogin: state.login.fetching,
    isConnected: state.network.isConnected,
    lastUsed: state.lastUsed,
    token: state.login.token,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    attemptLogin: (username, password) => dispatch(
      LoginActions.loginAttempt(username, password)),
    clearErrorMessage: () => dispatch(LoginActions.clearErrorMessage()),
    addLastUsed: (code: string, location: Location, name: string, address: string) => dispatch(LastUsedActions.addLastUsed(code, location, name, address))
  }
}

export default withNavigation(
  connect(mapStateToProps, mapDispatchToProps)(LoginScreen))
