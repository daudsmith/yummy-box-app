import * as React from 'react'
import {
  Content,
  Container,
} from 'native-base'
import {
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import { moderateScale } from 'react-native-size-matters'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import SmartPaymentMethodPicker from '../../Components/Payment/PaymentMethodPicker'
import SmartDeliveryTimePicker from '../../Components/SmartDeliveryTimePicker'
import AddressForm from '../../Components/AddressForm'
import AlertBox from '../../Components/AlertBox'
import MMPDeliveryItem from '../../Components/MMPDeliveryItem'
import MMPPaymentInformation from '../../Components/MMPPaymentInformation'
import NavigationBar from '../../Components/NavigationBar'
import WebView from '../../Components/WebView'
import YummyboxIcon from '../../Components/YummyboxIcon'
import MyMealPlanActions, {
  CreditCardInfo,
  MyMealPlanDetail,
  MyMealPlanDetailSuccess,
  MyMealPlanState,
} from '../../Redux/MyMealPlanRedux'
import ThemeActions from '../../Redux/ThemeRedux'
import CustomerCardActions, {
  Card,
  InitCustomerCard,
} from '../../Redux/CustomerCardRedux'
import Colors from '../../Themes/Colors'
import XenditService, {
  xenditCreditTokenResponse,
} from '../../Services/XenditService'
import Styles from '../../Containers/Styles/MyMealPlanModifyScreenStyle'
import OrderDeliveryHelper from '../../Services/OrderDeliveryHelper'
import SalesOrderService from '../../Services/SalesOrderService'
import { AppState } from '../../Redux/CreateStore'
import {
  SettingDataInterface,
  VaInterface,
} from '../../Redux/SettingRedux'
import { User } from '../../Redux/LoginRedux'
import { CartState } from '../../Redux/V2/CartRedux'
import { SalesOrderReduxInterface } from '../../Redux/SalesOrderRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { Item } from '../../Redux/CartRedux'
import { NavigationStackProp } from 'react-navigation-stack'
import ApiCatalog from '../../Services/ApiCatalog'
import { _dateReformatter } from '../../util/_dateReformatter'

const isDeliveryInfoChanged = (
  currentData, modifiedData, currentPhone, modifiedPhone) => {
  return (
    currentData.date !== modifiedData.date ||
    currentData.address !== modifiedData.address ||
    currentData.name !== modifiedData.name ||
    currentPhone !== modifiedPhone ||
    currentData.note !== modifiedData.note ||
    currentData.time !== modifiedData.time
  )
}

const isUsingDifferentPaymentMethod = (
  currentSelectedPayment, modifiedSelectedPayment, currentPaymentInfo,
  modifiedPaymentInfo, newCreditCardInfo,
) => {
  if (currentSelectedPayment !== modifiedSelectedPayment) {
    return true
  }
  if (modifiedSelectedPayment === 'credit-card') {
    if (modifiedPaymentInfo !== null || newCreditCardInfo !== null) {
      const usingNewCreditCard = newCreditCardInfo !== null
      if (usingNewCreditCard) {
        return true
      } else {
        const paymentInfo = JSON.parse(currentPaymentInfo.payment_info_json)
        if (parseInt(paymentInfo.id) !== modifiedPaymentInfo.id) {
          return true
        }
      }
    }
  }
  return false
}

const enableConfirmationButton = (currentMMPDetail, modifiedMMPDetail) => {
  const currentData = currentMMPDetail.data
  const modifiedData = modifiedMMPDetail.data
  const currentPhone = currentData.phone[0] === '+'
    ? currentData.phone.substr(1, currentData.phone.length)
    : currentData.phone
  const modifiedPhone = modifiedData.phone[0] === '+'
    ? modifiedData.phone.substr(1, modifiedData.phone.length)
    : modifiedData.phone
  const isDifferentPayment = isUsingDifferentPaymentMethod(
    currentMMPDetail.selectedPayment, modifiedMMPDetail.selectedPayment,
    currentMMPDetail.payment_info, modifiedMMPDetail.payment_info,
    modifiedMMPDetail.newCreditCardInfo)
  const myMealPlanItems = currentMMPDetail.data.items
  const modifiedMyMealPlanItems = modifiedMMPDetail.data.items
  const isDeliveryItemChanged = myMealPlanItems !== modifiedMyMealPlanItems

  return (
    (
      isDeliveryInfoChanged(currentData, modifiedData, currentPhone,
        modifiedPhone)
      || isDifferentPayment
      || isDeliveryItemChanged
    )
    && modifiedData.name !== ''
    && modifiedPhone !== ''
  )
}

interface StateInterface {
  showResetConfirmationModal: boolean,
  showModifyConfirmationModal: boolean,
  showLoader: boolean,
  showWebView: boolean,
  virtualAccountBanks: VaInterface[],
  enableModify: boolean,
  creditCardToken?: xenditCreditTokenResponse,
  authenticationUrl?: string,
  showErrorModal: boolean,
  goToCalendar?: any,
  isSameSubtotal: boolean,
  deliveryDetail: any,
  modifiedMMPDetail: any,
  showSuccessModal: boolean,
  typeErrorModal?: string,
  messageErrorModal: string,
  loading: boolean,
  showInvalidModal: boolean,
  failedItem?: Item,
  failedDate?: string,
  failedKitchenCode?: string,
}

interface PropsInterface {
  navigation?: NavigationDrawerProp & NavigationStackProp,
  currentMMPDetail: MyMealPlanDetailSuccess,
  modifiedMMPDetail: MyMealPlanDetailSuccess,
  myMealPlan: MyMealPlanState,
  setting: SettingDataInterface,
  isConnected: boolean,
  user: User,
  token: string,
  cart: CartState,
  order: SalesOrderReduxInterface,
  total: number,
  wallet: number,
  customerCard: InitCustomerCard,
  setInitialModifiedMyMealPlan: () => void,
  removeModifiedMyMealPlanDetail: () => void,
  modifyMmpPackageDelivery: (field, value) => void,
  modifyMmpSubscriptionPaymentInfo: (paymentInfo) => void,
  addCard: (card: Card) => void,
  modifyMmp: () => void,
  fetchAvailableDate: (themeId: number) => void,
  resetModifyState: () => void,
  setModifyType: (modifyType: string) => void,
  setMmpNewCreditCardInfo: (newCreditCardInfo: CreditCardInfo) => void,
  modifyMmpSubscriptionSelectedPayment: (selectedPayment: string) => void,
  fetchCards: () => void,
}

class MyMealPlanModifyScreen extends React.Component<PropsInterface, StateInterface> {
  constructor(props) {
    super(props)
    const virtualAccountBanks = this.props.setting.virtual_account_banks

    this.state = {
      showResetConfirmationModal: false,
      showModifyConfirmationModal: false,
      showLoader: false,
      showWebView: false,
      virtualAccountBanks,
      enableModify: false,
      creditCardToken: null,
      authenticationUrl: null,
      showErrorModal: false,
      goToCalendar: this.props.navigation.state.params.goToCalendar,
      isSameSubtotal: true,
      deliveryDetail: this.props.currentMMPDetail,
      modifiedMMPDetail: this.props.modifiedMMPDetail,
      showSuccessModal: false,
      typeErrorModal: null,
      messageErrorModal: '',
      loading: false,
      showInvalidModal: false,
      failedItem: null,
      failedDate: null,
      failedKitchenCode: null,
    }

    if (this.state.goToCalendar) {
      this.props.navigation.navigate('MyMealPlanModifyCalendarScreen')
    }
  }

  componentDidMount() {
    this.props.fetchCards()
    const { currentMMPDetail } = this.props
    if (currentMMPDetail.type === 'package') {
      if (this.props.isConnected) {
        this.props.fetchAvailableDate(
          currentMMPDetail.myMealPlanPackage.theme_id)
      }
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (newProps.currentMMPDetail
      && newProps.modifiedMMPDetail
    ) {
      const enableModify = enableConfirmationButton(newProps.currentMMPDetail, newProps.modifiedMMPDetail)
      this.setState({
        enableModify,
      })
    }

    // detect success modify
    if (newProps.myMealPlan.modifySuccess &&
      !newProps.myMealPlan.modifying &&
      newProps.myMealPlan.modifyType === 'info') {
      this.setState({
        showLoader: false,
        showSuccessModal: true,
      })
    }

    // re-fetch for package
    if (!this.props.isConnected && newProps.isConnected) {
      const { theme_id } = this.props.currentMMPDetail.myMealPlanPackage
      this.props.fetchAvailableDate(theme_id)
    }

    // detect failed modify
    if (
      !newProps.myMealPlan.modifySuccess
      && !newProps.myMealPlan.modifying
      && newProps.myMealPlan.modifyErrorMessage !== null
    ) {
      this.setState({
        showLoader: false,
        showErrorModal: true,
        messageErrorModal: newProps.myMealPlan.modifyErrorMessage,
        typeErrorModal: 'error',
      })
    }

    if (newProps.modifiedMMPDetail
      && newProps.currentMMPDetail
      && newProps.setting
    ) {
      this.setState({
        isSameSubtotal: (newProps.modifiedMMPDetail.data.subtotal - newProps.currentMMPDetail.data.subtotal) <= newProps.setting.free_charge_policy,
        deliveryDetail: newProps.currentMMPDetail,
      })
    }
  }

  componentWillUnmount() {
    this.props.removeModifiedMyMealPlanDetail()
  }

  handleDeliveryDetailChange(field, value) {
    this.props.modifyMmpPackageDelivery(field, value)
  }

  resetButton() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ showResetConfirmationModal: true })
        }}
        style={Styles.resetButton}
      >
        <Text style={Styles.resetText}>Reset</Text>
      </TouchableOpacity>
    )
  }

  processModify = async () => {
    const { modifiedMMPDetail } = this.props
    const { type, newCreditCardInfo } = modifiedMMPDetail
    if (type === 'subscription') {
      if (newCreditCardInfo !== null) {
        this.setState({ showLoader: true })
        XenditService.createToken(
          newCreditCardInfo,
          (authenticationUrl) => this.verification3DSCallBack(
            authenticationUrl),
          () => {
            this.setState({
              showErrorModal: true,
              messageErrorModal: 'We can\'t process your credit card.\nPlease use another card',
            })
          },
          (creditCardToken) => this.setState({ creditCardToken }),
        )
      } else {
        this.setState({
          showLoader: true,
        }, () => {
          this.props.setModifyType('info')
          this.props.modifyMmp()
        })
      }
    } else {
      this.setState({
        showLoader: false,
      }, this.modifyAction)
    }
  }

  modifyAction = async () => {
    const { modifiedMMPDetail } = this.props
    const { data } = modifiedMMPDetail
    const { items, delivery_date, kitchen_code } = (data as MyMealPlanDetail)
    
    this.props.setModifyType('info')
    const itemsData = items.data
    let validItem = true
    if (itemsData) {
      for (const item of itemsData) {
        const { detail, quantity } = item
        if (detail) {
          const { id } = detail.data
          
          await ApiCatalog.create()
            .getMealAvailability(id, delivery_date, kitchen_code)
            .then((response) => response.data)
            .then((responseBody) => responseBody.data)
            .then((responseData) => {
              
              validItem = responseData.remaining_quantity - quantity >= 0
              if (!validItem) {
                this.setState({
                  failedItem: item,
                  failedDate: delivery_date,
                  failedKitchenCode: kitchen_code,
                })
              }
            })
          
        }
      }
    }
    if (validItem) {
      if (this.state.isSameSubtotal) {
        this.props.modifyMmp()
      } else {
        this.props.navigation.navigate('MyMealPlanModifyPaymentScreen')
      }
    } else {
      this.setState({
        showLoader: false,
        showInvalidModal: true,
      })
    }
  }

  verification3DSCallBack(authenticationUrl) {
    this.setState({ authenticationUrl },
      () => this.setState({ showLoader: false },
        () => this.setState({ showWebView: true })))
  }

  creditCard3DSValidationSuccess(authData) {
    this.setState({ showLoader: true })
    const { user, modifiedMMPDetail } = this.props
    const newCreditCardInfo = modifiedMMPDetail.newCreditCardInfo
    const { saveCreditCard } = newCreditCardInfo
    const { creditCardToken } = this.state
    const ends_with = newCreditCardInfo.hasOwnProperty('card_number')
      ? newCreditCardInfo.card_number.substring(12, 16)
      : newCreditCardInfo.ends_with
    const newPaymentInfo = {
      type: newCreditCardInfo.type,
      ends_with,
      status: authData.status,
      token: creditCardToken.id,
      authorization_id: authData.id,
    }
    this.props.modifyMmpSubscriptionPaymentInfo(newPaymentInfo)
    if (saveCreditCard) {
      const cardData: Card = {
        account_id: user.id,
        token: creditCardToken.id,
        authorization_id: authData.id,
        type: newCreditCardInfo.type,
        ends_with,
      }
      this.props.addCard(cardData)
    }
    setTimeout(() => this.setState({
      showLoader: false,
    }, () => {
      this.props.setModifyType('info')
      this.props.modifyMmp()
    }), 1500)
  }

  renderSingleOrderDeliveryItem(item, delivery_fee = 0, index = 0, kitchenCode = '') {
    const { modifiedMMPDetail } = this.props
    return (
      <MMPDeliveryItem
        key={String(index)}
        item={item}
        hideQuantity={false}
        hidePrice={false}
        subComponentOnPress={() => this.props.navigation.navigate(
          'MyMealPlanModifyCatalogScreen', {
            deliveryDate: (modifiedMMPDetail.data as MyMealPlanDetail).delivery_date,
            item: item,
            delivery_fee: delivery_fee,
            index: index,
            kitchen_code: kitchenCode,
          })
        }
        disableEdit={item.status === 'UPDATED'}
        subComponentText='Change'
      />
    )
  }

  renderDeliveryItems(items: Item[], typeMyMealPlan, delivery_fee, kitchenCode: string = '') {
    if (typeMyMealPlan === 'subscription') {
      return (
        <MMPDeliveryItem
          item={items}
          hideQuantity={false}
          hidePrice={false}
          disableEdit={true}
        />
      )
    }
    if (typeMyMealPlan === 'package') {
      const { deliveryDetail } = this.state
      if (typeof deliveryDetail.myMealPlanPackage !== 'undefined') {
        if (this.state.isSameSubtotal) {
          const packageCount = deliveryDetail.myMealPlanPackage.dates.length
          const unitPrice = Math.ceil(deliveryDetail.data.subtotal / packageCount)
          return items.map(item => this.renderSingleOrderDeliveryItem({
            ...item,
            unit_price: unitPrice,
            subtotal: (unitPrice * item.quantity),
          }, 0 , 0, kitchenCode))
        }
        return items.map(item => this.renderSingleOrderDeliveryItem(item, 0 , 0, kitchenCode))
      } else {
        return items.map(item => this.renderSingleOrderDeliveryItem(item, 0 , 0, kitchenCode))
      }
    }
    if (typeMyMealPlan === 'item') {
      return items.map((item, index) => this.renderSingleOrderDeliveryItem(item, delivery_fee, index, kitchenCode))
    }
  }

  renderItemListSection() {
    const { modifiedMMPDetail } = this.props
    const { deliveryDetail } = this.state
    const items = (modifiedMMPDetail.data as MyMealPlanDetail).items.data
    const kitchenCode = (modifiedMMPDetail.data as MyMealPlanDetail).kitchen_code
    const canSkip = (deliveryDetail.actions && deliveryDetail.actions.canSkip)

    return (
      <View style={Styles.itemListSectionContainer}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: moderateScale(18),
          }}
        >
          <Text
            style={
              [
                Styles.deliveryDateText,
                { flex: 50 },
              ]
            }
          >
            {_dateReformatter({
              inputDate: modifiedMMPDetail.data.date,
              returnFormat: 'ddd, DD MMM YYYY'
            })}
          </Text>
          
          {
            modifiedMMPDetail.type === 'package' && canSkip &&
            <TouchableWithoutFeedback
              onPress={() => this.props.navigation.navigate('MyMealPlanModifyCalendarScreen')}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <YummyboxIcon
                  name='calendar-orange'
                  size={moderateScale(20)}
                  color={Colors.bloodOrange}
                  style={{ marginRight: moderateScale(5) }}
                />
                <FontAwesome
                  name='caret-right'
                  color={Colors.bloodOrange}
                />
              </View>
            </TouchableWithoutFeedback>
          }
          <View style={{flexDirection:'column'}}>
            <Text
              style={{
                color: Colors.greyishBrownThree,
                fontSize: 16,
              }}
            >
              {OrderDeliveryHelper.parseStatusToLabel(
                modifiedMMPDetail.data.status)}
            </Text>
            { items.some(item=>item.status == 'UPDATED') && <Text style={{fontSize:12}}>(Updated)</Text> }
          </View>

        </View>
        {
          this.renderDeliveryItems(
            items,
            modifiedMMPDetail.type,
            (modifiedMMPDetail.data as MyMealPlanDetail).delivery_fee,
            kitchenCode,
          )
        }
      </View>
    )
  }

  renderDeliveryTime = () => {
    const { modifiedMMPDetail } = this.props
    return (
      <View style={Styles.selectDeliveryTimeContainer}>
        <View style={{ flex: 1 }}>
          <SmartDeliveryTimePicker
            date={modifiedMMPDetail.data.date}
            modifiedMyMealPlanDetail={modifiedMMPDetail}
            type={'modify_mmp'}
            cart={this.props.cart}
            setting={this.props.setting}
            modifyMmpPackageDelivery={this.props.modifyMmpPackageDelivery}
          />
        </View>
      </View>
    )
  }

  renderPaymentMethodSection = () => {
    const { currentMMPDetail } = this.props
    const paymentInfo = currentMMPDetail.payment_info
    const paymentMethodType = paymentInfo.method
    return (
      <MMPPaymentInformation
        paymentMethodType={paymentMethodType}
        paymentInfo={paymentInfo}
      />
    )
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  navigateToTopUpScreen = () => {
    this.props.navigation.navigate('RootTopUpScreen', {navigationParams: 'back'})
  }

  setLoading = async (status) => {
    this.setState({
      loading: status,
    })
  }

  _updateOrder = () => {
    setTimeout(() => {
      this.setState({
        showInvalidModal: false,
      }, () => {
        const { failedItem, failedDate, failedKitchenCode } = this.state
        this.props.navigation.navigate(
          'MyMealPlanModifyCatalogScreen', {
            deliveryDate: failedDate,
            item: failedItem,
            kitchen_code: failedKitchenCode,
          })
      })
    }, 500)
  }

  _keepOrder = () => {
    this.setState({
      showInvalidModal: false,
    })
  }

  navigateToAddressOption = () => {
    SalesOrderService
      .getDeliveryAddress(
        this.props.token,
        this.setLoading,
        (this.props.navigation as NavigationDrawerProp),
        false,
        false,
        true,
      )
  }
  render() {
    const { currentMMPDetail, modifiedMMPDetail, setting } = this.props
    if (!currentMMPDetail || !modifiedMMPDetail) {
      return (<View />)
    }
    const isSameSubtotal = ((modifiedMMPDetail.data as MyMealPlanDetail).subtotal - (currentMMPDetail.data as MyMealPlanDetail).subtotal) < parseInt(setting.free_charge_policy, 10)
    const { showLoader, enableModify, showWebView } = this.state
    const { address, name, phone, note, landmark } = this.props.modifiedMMPDetail.data
    const disableConfirmationButtonColor = enableModify
      ? {}
      : { backgroundColor: Colors.pinkishGrey }
    const items = (modifiedMMPDetail.data as MyMealPlanDetail).items.data
    
    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title='Modify'
          rightSide={() => this.resetButton()}
          leftSideNavigation={this.navigationGoBack}
        />
        <Content
          contentContainerStyle={{ flexGrow: 1 }}
          keyboardShouldPersistTaps='handled'
        >
          <View style={{ marginHorizontal: moderateScale(20) }}>
            {this.renderItemListSection()}
            {this.renderDeliveryTime()}
            <AddressForm
              deliveryAddress={address}
              note={note}
              phone={phone}
              name={name}
              landmark={landmark}
              disableEdit={true}
              handleDeliveryDetailChange={(field, value) => this.handleDeliveryDetailChange(field, value)}
              deliveryAddressOptionPicker={this.navigateToAddressOption}
            />
            {currentMMPDetail.type === 'subscription' && currentMMPDetail.data.status === 'QUEUE'
              ?
              <SmartPaymentMethodPicker
                navigateToTopUpScreen={this.navigateToTopUpScreen}
                type='modify_mmp'
                order={this.props.order}
                wallet={this.props.wallet}
                total={this.props.total}
                setting={this.props.setting}
                customerCard={this.props.customerCard}
                myMealPlanDetail={this.props.currentMMPDetail}
                modifiedMyMealPlan={this.props.modifiedMMPDetail}
                setMmpNewCreditCardInfo={this.props.setMmpNewCreditCardInfo}
                modifyMmpSubscriptionPaymentInfo={this.props.modifyMmpSubscriptionPaymentInfo}
                modifyMmpSubscriptionSelectedPayment={this.props.modifyMmpSubscriptionSelectedPayment}
              />
              :
              this.renderPaymentMethodSection()
            }
          </View>
          <AlertBox
            primaryAction={() => this.props.setInitialModifiedMyMealPlan()}
            dismiss={() => this.setState(
              { showResetConfirmationModal: false })}
            primaryActionText='Yes'
            secondaryAction={() => this.setState(
              { showResetConfirmationModal: false })}
            secondaryActionText='No'
            text={'Are you sure want to reset all changes ?'}
            visible={this.state.showResetConfirmationModal}
          />
          <AlertBox
            primaryAction={() => this.setState(
              { showModifyConfirmationModal: false })}
            dismiss={() => this.setState(
              { showModifyConfirmationModal: false })}
            primaryActionText='Cancel'
            primaryActionTestProp='modifyCancel'
            secondaryAction={() => setTimeout(() => this.processModify(),
              1000)}
            secondaryActionText='Yes, Modify'
            secondaryActionTestProp='modifyYes'
            text={'You can only modify once per delivery, are you sure want to modify this delivery?'}
            visible={this.state.showModifyConfirmationModal}
            title={'Modify Delivery?'}
          />
          <AlertBox
            primaryAction={() => {
              if (this.state.typeErrorModal === 'error') {
                this.props.resetModifyState()
              }
            }}
            dismiss={() => this.setState({ showErrorModal: false })}
            primaryActionText='Ok'
            text={this.state.messageErrorModal !== null
              ? this.state.messageErrorModal
              : ''}
            visible={this.state.showErrorModal}
          />
          <AlertBox
            primaryAction={() => {
              // go to my order detail screen
              (this.props.navigation as NavigationStackProp).pop(2)
              this.props.resetModifyState()
            }}
            dismiss={() => this.setState({ showSuccessModal: false })}
            primaryActionText='Ok'
            primaryActionTestProp='updateOK'
            text={'Your meal updated successfully'}
            visible={this.state.showSuccessModal}
          />

          {this.state.authenticationUrl !== null &&
          <WebView
            visible={showWebView}
            dismiss={() => this.setState({ showWebView: false })}
            url={this.state.authenticationUrl}
            validationSuccess={(authData) => this.creditCard3DSValidationSuccess(
              authData)}
          />
          }
          <Spinner
            visible={showLoader}
            textStyle={{
              color: Colors.bloodOrange,
            }}
          />
          {!isSameSubtotal
            ? (
              <TouchableWithoutFeedback
                disabled={!enableModify}
                onPress={() => this.setState({
                  showModifyConfirmationModal: true,
                  isSameSubtotal: false,
                })}
              >
                <View
                  style={[
                    Styles.confirmButton,
                    disableConfirmationButtonColor,
                    {
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    },
                  ]}
                >
                  <View style={{ flex: 1 }} />
                  <Text style={[
                    Styles.confirmButtonText,
                    { flex: 15 },
                  ]}>
                    Confirm & Proceed to Payment
                  </Text>
                  <FontAwesome
                    name={'chevron-right'}
                    style={[
                      Styles.confirmButtonText,
                      {
                        flex: 1,
                        fontWeight: 'bold',
                        fontSize: 24,
                      },
                    ]}
                  />
                  <View
                    style={{
                      flex: 1,
                    }}
                  />
                </View>
              </TouchableWithoutFeedback>
            )
            : (
              <TouchableWithoutFeedback
                testID='confirmModify'
                accessibilityLabel='confirmModify'
                disabled={!enableModify}
                onPress={() => this.setState(
                  { showModifyConfirmationModal: true })}
              >
                <View
                  style={[
                    Styles.confirmButton,
                    disableConfirmationButtonColor,
                  ]}
                >
                  <Text
                    style={Styles.confirmButtonText}>
                    Confirm
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )}

          <AlertBox
            primaryAction={this._keepOrder}
            dismiss={this._keepOrder}
            primaryActionText='Keep Existing'
            secondaryAction={items.some(item=>item.status == 'UPDATED') ? this.navigateToAddressOption : this._updateOrder}
            secondaryActionText={items.some(item=>item.status == 'UPDATED')? 'Edit My Address' :'Edit My Order'}
            text={'Oops! One or more products that you chose have recently sold out or partially available. Please choose another product.'}
            title={'Product Availability Change'}
            visible={this.state.showInvalidModal}
            primaryStyle={'primary'}
            secondaryStyle={'secondary'}
          />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    currentMMPDetail: state.myMealPlan.myMealPlanDetail,
    modifiedMMPDetail: state.myMealPlan.modifiedMyMealPlanDetail,
    myMealPlan: state.myMealPlan,
    setting: state.setting.data,
    isConnected: state.network.isConnected,
    user: state.login.user,
    token: state.login.token,
    cart: state.cart,
    order: state.order,
    total: state.cart.totals.total,
    wallet: state.customerAccount.wallet,
    customerCard: state.customerCard,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setInitialModifiedMyMealPlan: () => dispatch(
      MyMealPlanActions.setInitialModifiedMyMealPlan()),
    removeModifiedMyMealPlanDetail: () => dispatch(
      MyMealPlanActions.removeModifiedMyMealPlanDetail()),
    modifyMmpPackageDelivery: (field, value) => dispatch(
      MyMealPlanActions.modifyMmpPackageDelivery(field, value)),
    modifyMmpSubscriptionPaymentInfo: (paymentInfo) => dispatch(
      MyMealPlanActions.modifyMmpSubscriptionPaymentInfo(paymentInfo)),
    addCard: (card: Card) => dispatch(CustomerCardActions.addCard(card)),
    modifyMmp: () => dispatch(MyMealPlanActions.modifyMmp()),
    fetchAvailableDate: (themeId) => dispatch(
      ThemeActions.fetchAvailableDate(themeId)),
    resetModifyState: () => dispatch(MyMealPlanActions.resetModifyState()),
    setModifyType: (modifyType: string) => dispatch(
      MyMealPlanActions.setModifyType(modifyType)),
    setMmpNewCreditCardInfo: (newCreditCardInfo: CreditCardInfo) => dispatch(
      MyMealPlanActions.setMmpNewCreditCardInfo(newCreditCardInfo)),
    modifyMmpSubscriptionSelectedPayment: (selectedPayment: string) => dispatch(
      MyMealPlanActions.modifyMmpSubscriptionSelectedPayment(selectedPayment)),
    fetchCards: () => dispatch(CustomerCardActions.fetchCards()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyMealPlanModifyScreen)
