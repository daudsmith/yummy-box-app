import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import NumericInput from '../Components/Common/NumericInput'
import ModalSuccessOne from '../Components/ModalSuccessOne'
import NavigationBar from '../Components/NavigationBar'
import WithSafeAreaView from '../Components/Common/WithSafeAreaView'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import LoginActions from '../Redux/LoginRedux'
import { Colors } from '../Themes'
import Styles from './Styles/PhoneNumberVerificationOneStyle'

const OTP_LENGTH = 4

class PhoneNumberVerificationOne extends Component {
  constructor (props) {
    super(props)
    this.state = {
      countDownSeconds: 60,
      countingDownOtp: true,
      newPhoneNumber: this.props.navigation.state.params.newPhoneNumber,
      showSpinner: false,
      otpError: null,
      otpSuccess: false,
      showModalSuccess: false,
      OTP: ''
    }
  }

  componentDidMount () {
    let {countingDownOtp} = this.state
    if (countingDownOtp === true) {
      this.interval = setInterval(() => this.tick(), 1000)
    }
  }

  componentWillUnmount () {
    clearInterval(this.interval)
    this.props.navigation.state.params.returnData()
  }

  UNSAFE_componentWillReceiveProps (newProps) {
    let {checkValidateOtp, checkValidateOtpFailed, checkValidateOtpDone} = newProps.customerAccount
    this.setState({showSpinner: false})
    if (checkValidateOtpFailed && !checkValidateOtp) {
      this.setValue('otpError', checkValidateOtpFailed)
    }

    if (checkValidateOtpDone) {
      this.setValue('otpSuccess', true)
      this.setValue('showModalSuccess', true)
    }
  }

  tick () {
    let {countingDownOtp, countDownSeconds} = this.state
    if (countDownSeconds === 0) {
      this.setState({countingDownOtp: !countingDownOtp})
      clearInterval(this.interval)
    } else {
      this.setState(prevState => ({
        countDownSeconds: prevState.countDownSeconds - 1
      }))
    }
  }

  setValue (name, value) {
    let {showSpinner} = this.state
    switch (name) {
      case 'showSpinner':
        this.setState({showSpinner: !showSpinner})
        break

      case 'otpError':
        this.setState({otpError: value})
        break

      case 'otpSuccess':
        this.setState({otpSuccess: value})
        break

      case 'countDownSeconds':
        this.setState({countDownSeconds: value})
        break

      case 'showModalSuccess':
        this.setState({showModalSuccess: value})
        break

      default:
        break
    }
  }

  validateOTP () {
    const {OTP, newPhoneNumber} = this.state
    let countryCode = '62'
    this.props.changePhoneNumberValidate(newPhoneNumber.replace('+', ''), OTP, countryCode)
    this.setValue('countDownSeconds', 60)
    this.interval = setInterval(() => this.tick(), 1000)
  }

  resendOTP () {
    const {newPhoneNumber} = this.state
    this.props.sendOtpNumber(newPhoneNumber)
  }

  backToProfileScreen () {
    this.setValue('showModalSuccess', false)
    this.props.getNewUserData()
    this.navigateToAccountScreen()
  }

  navigateToAccountScreen () {
    const navigation = this.props.navigation
    setTimeout(() => { navigation.navigate('DrawerClose') }, 50); navigation.navigate('AccountScreen')
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render () {
    let {showSpinner, OTP, countDownSeconds, newPhoneNumber, otpError, showModalSuccess} = this.state
    let disableButton = countDownSeconds !== 0
    let timerColor = countDownSeconds === 0 ? Colors.grennBlue : Colors.pinkishGrey
    let phoneNumber = newPhoneNumber.slice(0, 3) + ' ' + newPhoneNumber.slice(3)
    let disableButtonConfirm = OTP.length !== OTP_LENGTH
    let buttonColor = OTP.length === OTP_LENGTH ? Colors.bloodOrange : Colors.lightGrey

    return (
      <View style={{flex: 1}}>
        <Spinner visible={showSpinner} textStyle={{color: '#FFF'}} />
        <NavigationBar
          leftSide='back'
          title='Verification'
          leftSideNavigation={this.navigationGoBack}
        />
        <View style={Styles.ScreenContainer}>
          <View style={{height: 260}}>
            <View style={Styles.screenBox}>
              <Text style={Styles.contentText}>We have sent a verification code to your mobile number
                <Text style={{fontFamily: 'Rubik-Medium', fontSize: 11}}> ({phoneNumber})</Text>
                . Please enter the {OTP_LENGTH}-digit code below to complete your registration.
              </Text>
              <View style={{paddingVertical: 10}}>
                <NumericInput
                  digits={OTP_LENGTH}
                  getValue={(value) => this.setState({OTP: value})} />
              </View>
              <View style={{marginTop: 8, height: 15}}>
                {
                  otpError !== null
                  ? (
                    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                      <FontAwesome name={'close'} color={'red'} size={10} style={{alignSelf: 'center'}} />
                      <Text style={{fontFamily: 'Rubik-Light', fontSize: 10, color: 'red', marginLeft: 5}}>Invalid OTP. Please input the correct OTP</Text>
                    </View>
                  ) : null
                }
              </View>

              <View style={{height: 38, marginTop: 15}}>
                <TouchableOpacity style={[Styles.button1, {backgroundColor: buttonColor}]} onPress={() => this.validateOTP()} disabled={disableButtonConfirm}>
                  <Text style={Styles.button1text}>Confirm</Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity disabled={disableButton} onPress={() => this.resendOTP()}>
                {
                  countDownSeconds === 0
                  ? (
                    <Text style={{fontFamily: 'Rubik-Light', fontSize: 12, color: Colors.greyishBrown, alignSelf: 'center', marginTop: 19}}>Didn’t receive the OTP?
                      <Text style={{fontFamily: 'Rubik-Regular', fontSize: 12, color: Colors.grennBlue}}> Resend Code</Text>
                    </Text>
                  ) : <Text style={[Styles.timeText]}>Didn’t receive the OTP?
                        <Text> </Text>
                        <Text style={[Styles.resendText, {color: timerColor}]}>Resend Code ({countDownSeconds}s)</Text>
                      </Text>
                }
              </TouchableOpacity>
            </View>
          </View>
        </View>

      <ModalSuccessOne
        showModal={showModalSuccess}
        message={`You’re successfully update ${'\n'} your phone number.`}
        onPress={() => this.backToProfileScreen()}
      />
    </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    customerAccount: state.customerAccount,
    user: state.login.user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    sendOtpNumber: (newPhoneNumber) => dispatch(CustomerAccountActions.sendOtpNumber(newPhoneNumber)),
    changePhoneNumberValidate: (phone, otp, countryCode) => dispatch(CustomerAccountActions.changePhoneNumberValidate(phone, otp, countryCode)),
    getNewUserData: () => dispatch(LoginActions.getNewUserData()),
  }
}

export default WithSafeAreaView(connect(mapStateToProps, mapDispatchToProps)(PhoneNumberVerificationOne))
