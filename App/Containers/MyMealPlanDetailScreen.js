import moment from 'moment'
import 'moment/locale/en-au'
import React, { Component } from 'react'
import {
  ActivityIndicator,
  Linking,
  Platform,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import Modal from 'react-native-modal'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import AddressForm from '../Components/AddressForm'
import AlertBox from '../Components/AlertBox'
import ActionSheet from '../Components/Common/ActionSheet'
import MMPPaymentInformation from '../Components/MMPPaymentInformation'
import NavigationBar from '../Components/NavigationBar'
import WithSafeAreaView from '../Components/Common/WithSafeAreaView'
import CustomerOrderActions from '../Redux/CustomerOrderRedux'
import MyMealPlanActions from '../Redux/MyMealPlanRedux'
import ThemeActions from '../Redux/ThemeRedux'
import AvailabilityDateService from '../Services/AvailabilityDateService'
import LocaleFormatter from '../Services/LocaleFormatter'
import OrderDeliveryHelper from '../Services/OrderDeliveryHelper'
import Colors from '../Themes/Colors'
import ContactUs from './ContactUs'
import Styles from './Styles/MyMealPlanDetailScreenStyle'
import FastImage from 'react-native-fast-image'
import { _dateReformatter } from '../util/_dateReformatter'

class MyMealPlanDetailScreen extends Component {
  constructor(props) {
    super(props)
    const { id, deliveryNumber, type } = props.navigation.state.params
    this.state = {
      deliveryNumber,
      id,
      type,
      fetchingDetail: true,
      showCancelOrderModal: false,
      showContactUsModal: false,
      showActionSheet: false,
      actionSheetButtons: [],
      banksLogo: [],
      hasFetchedData: false,
      showSkipDeliveryModal: false,
      themeAvailableDates: [],
      availableDatesFetched: false,
      nextAvailableDate: null,
      modifySuccess: false,
      modifying: false,
      showSuccessModal: false,
      modifyErrorMessage: '',
      themeAvailableMeals: [],
      myMealPlanDetail: null,
    }
  }

  componentDidMount() {
    if (this.props.isConnected) {
      this.getMMPDetailData()
      this.props.resetAvailableDates()
      this.props.resetModifyState()
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { myMealPlanDetail, fetchingDetail, detailErrorMessage } = newProps.myMealPlan
    this.setState({
      myMealPlanDetail,
      fetchingDetail,
    })

    if (typeof newProps.setting.virtual_account_banks !== 'undefined') {
      const banksLogo = newProps.setting.virtual_account_banks.map(
        (bank) => { return { ...bank } })
      this.setState({ banksLogo })
    }
    if (!fetchingDetail && detailErrorMessage === null) {
      this.setState({ hasFetchedData: true })
    }
    if (!this.props.isConnected && newProps.isConnected && !this.state.hasFetchedData) {
      this.getMMPDetailData()
    }

    if (this.state.hasFetchedData) {
      if (myMealPlanDetail !== null && !this.state.availableDatesFetched) {
        if (myMealPlanDetail.myMealPlanPackage !== null) {
          this.props.fetchAvailableDate(myMealPlanDetail.myMealPlanPackage.theme_id)
          this.setState({ availableDatesFetched: true })
        }
      }

      if (newProps.availableDates.length > 0 && myMealPlanDetail.myMealPlanPackage !== null) {
        if ((newProps.availableDates.length - 1) === myMealPlanDetail.myMealPlanPackage.dates.length) {
          this.setState(
            { nextAvailableDate: newProps.availableDates[newProps.availableDates.length - 1] })
        } else {
          let nextAvailableDate = null
          const lastDatePlanned = myMealPlanDetail.myMealPlanPackage.dates[myMealPlanDetail.myMealPlanPackage.dates.length - 1]
          newProps.availableDates.map(date => {
            if (nextAvailableDate === null && moment(date)
              .isAfter(moment(lastDatePlanned))) {
              nextAvailableDate = date
            }
          })
          this.setState({ nextAvailableDate: nextAvailableDate })
        }
      }

      if (newProps.myMealPlan.modifySuccess &&
        !newProps.myMealPlan.modifying &&
        newProps.myMealPlan.modifyType === 'skip') {
        this.setState({
          showSuccessModal: newProps.myMealPlan.modifySuccess && !newProps.myMealPlan.modifying,
          modifyErrorMessage: newProps.myMealPlan.modifyErrorMessage,
        })
      }
    }

    this.setState({
      themeAvailableMeals: newProps.themeAvailableMeals,
      myMealPlanDetail: newProps.myMealPlan.myMealPlanDetail,
    })

    this.shouldRefetchMyOrder(this.props.myMealPlan.myMealPlanDetail,
      newProps.myMealPlan.myMealPlanDetail)
  }

  componentWillUnmount() {
    this.props.clearMmpDetail()
    this.props.resetAvailableDates()
    this.props.resetModifyState()
    this.props.removeModifiedMyMealPlanDetail()
    this.setState({ availableDatesFetched: false })
  }

  shouldRefetchMyOrder(currentMyMealPlanDetail, newMyMealPlanDetail) {
    if (currentMyMealPlanDetail === null) {
      return
    }

    if (this.props.navigation.state.params.hasOwnProperty('myOrderParams') &&
      currentMyMealPlanDetail.data.status !== 'CANCELLED' &&
      newMyMealPlanDetail.data.status === 'CANCELLED') {
      const { orderId, orderType } = this.props.navigation.state.params.myOrderParams
      this.props.fetchOrderDetail(orderId, orderType)
      this.props.fetchOrderList(true)
    }
  }

  getMMPDetailData = () => {
    const { id, type } = this.state
    this.props.getMyMealPlanDetail(id, type)
  }

  separatorLine = () => {
    return (
      <View
        style={{
          height: 1,
          borderBottomWidth: 0.5,
          borderStyle: 'solid',
          borderBottomColor: Colors.pinkishGrey,
        }}
      />
    )
  }

  shouldShowOrderMoreButton(status) {
    const { myMealPlanDetail } = this.props.myMealPlan
    const { type } = myMealPlanDetail
    const { delivery_cut_off } = this.props.setting

    return type !== 'subscription'
      && status !== 'COMPLETE'
      && !AvailabilityDateService
        .isNowExceedingDateCutOff(delivery_cut_off, myMealPlanDetail.data.date)
  }

  renderTransactionTotalBox() {
    const { payment_info } = this.props.myMealPlan.myMealPlanDetail
    const paymentMethodType = payment_info.method
    if (this.state.banksLogo.length < 1) {
      return null
    }
    return (
      <MMPPaymentInformation
        paymentMethodType={paymentMethodType}
        paymentInfo={payment_info}
      />
    )
  }

  renderItemRow(item, index) {
    const foodData = item.detail.data
    const { myMealPlanDetail } = this.props.myMealPlan
    if (myMealPlanDetail === null) {
      return
    }
    const packageCount = myMealPlanDetail.myMealPlanPackage !== null
      ? myMealPlanDetail.myMealPlanPackage.dates.length
      : 1
    const unitPrice = myMealPlanDetail.type !== 'package'
      ? item.unit_price
      : Math.ceil(myMealPlanDetail.data.subtotal / packageCount)

    return (
      <View style={Styles.itemRowContainer} key={index}>
        <View style={{ flex: 1 }}>
          <FastImage
            style={{
              height: scale(78),
              width: scale(78),
              borderRadius: 5,
            }}
            source={{
              uri: foodData.images.data[0].thumbs.medium_thumb,
              priority: FastImage.priority.normal,
            }}
          />

        </View>
        <View style={Styles.itemRowDataContainer}>
          <Text style={Styles.foodNameText}>{foodData.name}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={Styles.priceText}>{LocaleFormatter.numberToCurrency(unitPrice)}</Text>
            <Text style={Styles.quantityText}>{`x${item.quantity}`}</Text>
          </View>
        </View>
      </View>
    )
  }

  renderMyMealPlanDetailView() {
    const { myMealPlanDetail } = this.props.myMealPlan
    if (myMealPlanDetail === null) {
      return
    }
    const displayedDate = _dateReformatter({
      inputDate: myMealPlanDetail.data.date,
      returnFormat: 'ddd, DD MMM YYYY'
    })
    return (
      <View>
        <View style={Styles.mealPlanDetailContainer}>
          <View style={Styles.deliveryDateAndStatusContainer}>
            <Text style={Styles.deliveryDateText}>{displayedDate}</Text>
            <Text style={Styles.deliveryStatusText}>
              {OrderDeliveryHelper.parseStatusToLabel(
                myMealPlanDetail.data.status
              )}
            </Text>
          </View>
          <View style={Styles.mealItemsContainer}>
            {Array.isArray(myMealPlanDetail.data.items.data)
              ? myMealPlanDetail.data.items.data.map((item, index) => {
                  return this.renderItemRow(item, index)
                })
              : this.renderItemRow(myMealPlanDetail.data.items.data, 0)}
          </View>
          {this.separatorLine()}

          <View style={Styles.deliveryTimeContainer}>
            <Text style={Styles.deliveryTimeLabelText}>Delivery Time</Text>
            <Text style={Styles.deliveryTimeValueText}>
              {myMealPlanDetail.data.time}
            </Text>
          </View>

          <AddressForm
            deliveryAddress={myMealPlanDetail.data.address}
            note={myMealPlanDetail.data.note}
            phone={myMealPlanDetail.data.phone}
            name={myMealPlanDetail.data.name}
            readOnly
          />

          {this.renderTransactionTotalBox()}
        </View>
      </View>
    )
  }

  renderLoadingView() {
    return (
      <View style={Styles.activityIndicatorContainer}>
        <ActivityIndicator size='large' />
      </View>
    )
  }

  renderFAQModal() {
    return (
      <ContactUs
        dismissModal={() => this.setState({ showContactUsModal: false })}
      />
    )
  }

  showActionSheet(canCancel, canModify, status) {
    const { myMealPlanDetail } = this.props.myMealPlan
    const actions = myMealPlanDetail.actions || {
      canCancel: canCancel,
      canModify: canModify,
      canSkip: false,
    }

    const { type } = myMealPlanDetail
    let buttons = []
    if (this.shouldShowOrderMoreButton(status)) {
      buttons.push({
        text: 'Order More',
        testID: 'orderMore',
        accessibilityLabel: 'orderMore',
        onPress: () => {
          let newStartDate = moment(myMealPlanDetail.data.date)
          this.props.navigation.navigate('MealListScreen', {
            selectedDate: newStartDate,
            fromPage: 'MyMealPlanDetailScreen',
          })
        },
      })
    }
    if (canCancel && status !== 'REQUEST_CANCEL') {
      if (type === 'item') {
        buttons.push({
          text: 'Cancel Delivery',
          testID: ' cancelDelivery',
          accessibilityLabel: 'cancelDelivery',
          onPress: () => setTimeout(() => this.setState({ showCancelOrderModal: true }), 500),
        })
      } else if (type === 'subscription' && status !== 'SKIP') {
        buttons.push({
          text: 'Skip Meal for Today',
          onPress: () => setTimeout(() => this.setState({ showCancelOrderModal: true }), 500),
        })
      }
    }

    if (actions.canSkip) {
      buttons.push({
        text: 'Skip Delivery',
        testID: 'skipDelivery',
        accessibilityLabel: 'skipDelivery',
        onPress: () => setTimeout(() => this.setState({ showSkipDeliveryModal: true }), 500),
      })
    }

    if (canModify && status !== 'REQUEST_CANCEL' && status !== 'SKIP') {
      buttons.push({
        text: 'Modify',
        testID: 'modifyProduct',
        accessibilityLabel: 'modifyProduct',
        onPress: () => {
          this.props.setInitialModifiedMyMealPlan()
          this.props.navigation.navigate({
            routeName: 'MyMealPlanModifyScreen',
            params: {
              goToCalendar: false,
              backKey: 'modifySingleOrderFromDetailScreenKey',
            },
            key: 'modifySingleOrderFromDetailScreenKey',
          })
        },
      })
    }
    buttons.push({
      text: 'FAQ',
      testID: 'FAQ',
      accessibilityLabel: 'FAQ',
      onPress: () => Linking.openURL('http://help.yummybox.id'),
    })
    buttons.push({
      text: 'Contact',
      testID: 'contact',
      accessibilityLabel: 'contact',
      onPress: () => setTimeout(() => this.setState({ showContactUsModal: true }), 500),
    })
    if (Platform.OS === 'ios') {
      buttons.push({
        text: 'Close',
        testID: 'closeModify',
        accessibilityLabel: 'closeModify',
        onPress: () => setTimeout(() => this.setState({ showActionSheet: false }), 500),
      })
    }
    this.setState({
      showActionSheet: true,
      actionSheetButtons: buttons,
    })
  }

  getCancelConfirmationMessage() {
    const { myMealPlanDetail } = this.props.myMealPlan
    if (myMealPlanDetail === null) {
      return {
        title: '',
        body: '',
      }
    }
    const orderPayment = myMealPlanDetail.payment_info
    const isCod = orderPayment.method === 'cod'
    const isUnpaidBankTransfer = orderPayment.method === 'virtual-account' && orderPayment.status === 'PENDING'
    const type = myMealPlanDetail.type === 'item' || myMealPlanDetail.type === 'package'
      ? 'cancel'
      : 'skip'
    if (isCod || isUnpaidBankTransfer || myMealPlanDetail.type === 'package') {
      return {
        title: 'Confirm Cancel',
        body: `Are you sure you want to ${type} this delivery?`,
      }
    }
    const amount = LocaleFormatter.numberToCurrency(
      (myMealPlanDetail.data.subtotal + myMealPlanDetail.data.delivery_fee) - myMealPlanDetail.data.free_charged)
    return {
      title: 'Cancel this delivery?',
      body: `\n${amount} will be added to your YumCredits balance`,
    }
  }

  navigationGoBack = () => {
    this.props.navigation.goBack()
  }

  render() {
    const { deliveryNumber, fetchingDetail, showCancelOrderModal, showContactUsModal, showSkipDeliveryModal, showSuccessModal } = this.state
    let nextAvailableDate = ''
    if (this.state.nextAvailableDate !== null) {
      nextAvailableDate = moment(this.state.nextAvailableDate, 'YYYY-MM-DD')
      .format('ddd, DD MMM YYYY')
    }
    const { myMealPlanDetail } = this.props.myMealPlan
    let nextAvailableMeal = null
    this.state.themeAvailableMeals.map((meal) => {
      const mealDate = _dateReformatter({
        inputDate: meal.date,
        returnFormat: 'ddd, DD MMM YYYY'
      })

      if (mealDate === this.state.nextAvailableDate) {
        nextAvailableMeal = meal.item.name
      }
    })
    let deliverySkippedToMessage = ''
    if (myMealPlanDetail !== null) {
      deliverySkippedToMessage = `You have skipped your delivery to ${myMealPlanDetail.data.date}`
    }
    const cancellationConfirmation = this.getCancelConfirmationMessage()
    return (
      <View style={{ flex: 1 }}>
        <NavigationBar
          leftSide='back'
          title={`Delivery ${deliveryNumber}`}
          leftSideNavigation={this.navigationGoBack}
          rightSide={() => {
            if (!fetchingDetail && myMealPlanDetail !== null) {
              return (
                <TouchableWithoutFeedback
                  onPress={() => this.showActionSheet(myMealPlanDetail.data.can_cancel,
                    myMealPlanDetail.data.can_modify, myMealPlanDetail.data.status)}
                  hitSlop={{
                    top: verticalScale(26),
                    left: scale(26),
                    bottom: verticalScale(26),
                    right: scale(26),
                  }}
                >
                  <View>
                    <Ionicons
                      name={'ellipsis-vertical'}
                      size={scale(23)}
                      color={Colors.bloodOrange}
                      style={{
                        marginRight: scale(10),
                      }}
                    />
                  </View>
                </TouchableWithoutFeedback>
              )
            } else {
              return null
            }
          }}
        />
        <ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          style={{ backgroundColor: 'white' }}
          showsVerticalScrollIndicator={false}
        >
          {fetchingDetail && myMealPlanDetail === null
            ?
            this.renderLoadingView()
            :
            this.renderMyMealPlanDetailView()
          }
        </ScrollView>
        <ActionSheet
          buttons={this.state.actionSheetButtons}
          visible={this.state.showActionSheet}
          dismiss={() => this.setState({ showActionSheet: false })}
        />
        {myMealPlanDetail !== null &&
        <AlertBox
          primaryAction={() => {}}
          primaryActionText='No'
          secondaryAction={() => {
            this.props.cancelOrder(myMealPlanDetail.data.id)
          }}
          secondaryActionText='Yes'
          dismiss={() => this.setState({ showCancelOrderModal: false })}
          title={cancellationConfirmation.title}
          text={cancellationConfirmation.body}
          visible={showCancelOrderModal}
        />
        }
        <Modal
          backdropColor='white'
          backdropOpacity={0.8}
          isVisible={showContactUsModal}
          onBackButtonPress={() => this.setState({ showContactUsModal: false })}
          style={{ margin: 0 }}
        >
          {this.renderFAQModal()}
        </Modal>
        <AlertBox
          primaryAction={() => {}}
          primaryActionText='No'
          secondaryAction={() => {
            if (this.state.nextAvailableDate !== null && typeof myMealPlanDetail.data !== 'undefined') {
              this.props.setModifyType('skip')
              this.props.setInitialModifiedMyMealPlan()
              this.props.modifyMmpPackageDeliveryMeal(this.state.nextAvailableDate,
                myMealPlanDetail.data.delivery_date)
              this.props.modifyMmp()
            }
          }}
          secondaryActionText='Yes'
          dismiss={() => this.setState({ showSkipDeliveryModal: false })}
          title={`Move Delivery to ${nextAvailableDate}?`}
          text={() => {
            return (
              <View>
                <Text style={Styles.alertBoxText}>Your meal will be replaced to <Text
                  style={Styles.alertBoxTextBold}>{nextAvailableMeal}</Text>. Only 1 (one) skip per delivery allowed</Text>
              </View>
            )
          }}
          visible={showSkipDeliveryModal}
          footerAction={() => {
            this.props.setInitialModifiedMyMealPlan()
            this.props.navigation.navigate({
              routeName: 'MyMealPlanModifyScreen',
              params: {
                goToCalendar: true,
                backKey: 'modifySingleOrderFromDetailScreenKey',
              },
              key: 'modifySingleOrderFromDetailScreenKey',
            })
          }}
          footerActionText={'Choose Other Date'}
        />
        <AlertBox
          primaryAction={() => {}}
          primaryActionText='Ok'
          dismiss={() => {
            this.setState({ showSuccessModal: false })
            this.props.getMyMealPlans()
            this.props.navigation.navigate('MyMealPlanScreen')
            this.props.resetModifyState()
          }}
          title={'Delivery Skipped'}
          text={deliverySkippedToMessage}
          visible={showSuccessModal}
        />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    myMealPlan: state.myMealPlan,
    setting: state.setting.data,
    isConnected: state.network.isConnected,
    availableDates: state.theme.availableDateSelectedTheme,
    themeAvailableMeals: state.theme.availableMeals,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getMyMealPlanDetail: (id, orderType) => dispatch(
      MyMealPlanActions.getMyMealPlanDetail(id, orderType)),
    clearMmpDetail: () => dispatch(MyMealPlanActions.clearMmpDetail()),
    cancelOrder: (id) => dispatch(MyMealPlanActions.cancelOrder(id)),
    setInitialModifiedMyMealPlan: () => dispatch(MyMealPlanActions.setInitialModifiedMyMealPlan()),
    fetchOrderDetail: (id, type) => dispatch(CustomerOrderActions.fetchOrderDetail(id, type)),
    fetchOrderList: (silent) => dispatch(CustomerOrderActions.fetchOrderList(silent)),
    fetchAvailableDate: (themeId) => dispatch(ThemeActions.fetchAvailableDate(themeId)),
    resetAvailableDates: () => dispatch(ThemeActions.resetAvailableDates()),
    modifyMmpPackageDeliveryMeal: (selectedDate, currentDate) => dispatch(
      MyMealPlanActions.modifyMmpPackageDeliveryMeal(selectedDate, currentDate)),
    modifyMmp: () => dispatch(MyMealPlanActions.modifyMmp()),
    removeModifiedMyMealPlanDetail: () => dispatch(
      MyMealPlanActions.removeModifiedMyMealPlanDetail()),
    resetModifyState: () => dispatch(MyMealPlanActions.resetModifyState()),
    getMyMealPlans: () => dispatch(MyMealPlanActions.getMyMealPlans()),
    setModifyType: (modifyType) => dispatch(MyMealPlanActions.setModifyType(modifyType)),
  }
}

export default WithSafeAreaView(
  connect(mapStateToProps, mapDispatchToProps)(MyMealPlanDetailScreen))
