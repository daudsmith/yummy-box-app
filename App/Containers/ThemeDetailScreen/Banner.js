import PropTypes from 'prop-types'
import React from 'react'
import { StyleSheet, View, ViewPropTypes } from 'react-native'
import FastImage from 'react-native-fast-image'

const Banner = ({ image, style }) => {
  return (
    <View style={styles.container}>
      <FastImage
        style={[styles.banner, style]}
        source={{
          uri: image,
          priority: FastImage.priority.normal,
        }}
        resizeMode={FastImage.resizeMode.contain}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 8,
  },
  banner: {
    borderRadius: 8,
  },
})

Banner.propTypes = {
  image: PropTypes.string.isRequired,
  style: ViewPropTypes.style,
}

export default Banner
