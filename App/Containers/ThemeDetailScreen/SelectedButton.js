import PropTypes from 'prop-types'
import React from 'react'
import { Animated, StyleSheet, Text, View, ViewPropTypes } from 'react-native'
import {
  scale,
  verticalScale,
  moderateScale,
} from 'react-native-size-matters'
import PrimaryButton from '../../Components/PrimaryButton'
import LocaleFormatter from '../../Services/LocaleFormatter'
import Colors from '../../Themes/Colors'

const SelectedButton = ({ title, basePrice, sellPrice, onPress, bounceValue }) => {
  return (
    <Animated.View
      style={{
        ...styles.container,
        transform: [{ translateY: bounceValue }] ,
      }}
    >
      <View style={styles.contentContainer}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.priceContainer}>
          <Text style={styles.sellPrice}>{LocaleFormatter.numberToCurrency(sellPrice)}</Text>
          {(sellPrice < basePrice) &&
            <Text style={styles.basePrice}>{LocaleFormatter.numberToCurrency(basePrice)}</Text>
          }
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <PrimaryButton
          label='Select Date'
          testID='chooseDate' 
          accessibilityLabel='chooseDate'
          labelStyle={{ fontSize: 14 }}
          onPressMethod={onPress}
          buttonStyle={{ backgroundColor: Colors.primaryOrange }}
        />
      </View>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: verticalScale(16),
    paddingHorizontal: scale(20),
    shadowColor: Colors.darkBlue,
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.white,
    zIndex: 1000,
  },
  contentContainer: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  buttonContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  priceContainer: {
    flexDirection: 'row',
  },
  title: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(14),
    color: Colors.darkBlue,
  },
  sellPrice: {
    fontFamily: 'Rubik-Medium',
    fontSize: moderateScale(16),
    color: Colors.pinkishOrange,
  },
  basePrice: {
    fontFamily: 'Rubik-Regular',
    fontSize: moderateScale(12),
    paddingVertical: verticalScale(4),
    paddingHorizontal: scale(4),
    color: Colors.warmGreyThree,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
})

SelectedButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  basePrice: PropTypes.number,
  sellPrice: PropTypes.number,
  bounceValue: PropTypes.object,
  style: ViewPropTypes.style,
}

export default SelectedButton
