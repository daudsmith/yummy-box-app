import PropTypes from 'prop-types'
import React from 'react'
import _ from 'lodash'
import { StyleSheet, Text, TouchableWithoutFeedback, View, ViewPropTypes } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import LocaleFormatter from '../../Services/LocaleFormatter'
import Colors from '../../Themes/Colors'

const PackageCard = ({
  onPress,
  name,
  badge,
  tagLine,
  basePrice,
  sellPrice,
  disabled,
  warning,
  description,
  selected,
  type,
}) => {
  let newName = _.camelCase(name)
  let bgColor = styles.defaultColor
  if (disabled) {
    bgColor = styles.inactiveColor
  } else if (selected) {
    bgColor = styles.activeColor
  }
  let textColor = styles.defaultTextColor
  if (disabled) {
    textColor = styles.inactiveTextColor
  } else if (selected) {
    textColor = styles.activeTextColor
  }
  let badgeTextColor = {color: Colors.white}
  let badgeBgColor = {backgroundColor: Colors.darkBlue}
  if (selected) {
    badgeTextColor = {color: Colors.darkBlue}
    badgeBgColor = styles.defaultColor
  }
  let discColor = styles.discountFont
  let discBGColor = {backgroundColor: Colors.discountFill, borderWidth: 1, borderColor: Colors.discountBorder}
  if (disabled) {
    discColor = styles.inactiveTextColor
    discBGColor = {backgroundColor: Colors.lightGrey2, borderWidth: 1, borderColor: Colors.pinkishGrey2}
  } else if (selected) {
    discColor = {color: Colors.darkBlue}
    discBGColor = {backgroundColor: Colors.white, borderWidth: 1, borderColor: Colors.discountBorder}
  }
  let textPriceColor = {color: Colors.pinkishOrange}
  let textBasePriceColor = {color: Colors.warmGreyThree}
  if (disabled) {
    textPriceColor = {color: Colors.warmGreyThree}
    textBasePriceColor = {color: Colors.warmGreyThree}
  } else if (selected) {
    textPriceColor = {color: Colors.white}
    textBasePriceColor = {color: Colors.white}
  }
  let bgWarningColor = {backgroundColor: Colors.lightningYellow}
  if (disabled) {
    bgWarningColor = {backgroundColor: Colors.brownGrey}
  } else if (selected) {
    bgWarningColor = {backgroundColor: Colors.persianGreen}
  }

  let packageCard = null


  if (type === 'subscription') {
    packageCard = (
      <View style={[styles.container, styles.shadow, bgColor, {flexDirection: 'column', height: null}]}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'flex-start',
          justifyContent: 'space-between',
        }}>
          <View>
            <Text style={[styles.title, textColor]}>{name}</Text>
          </View>
          {(!disabled && badge !== '') &&
          <View>
            <View style={[styles.badgeContainer, badgeBgColor]}>
              <Text style={[styles.badgeText, badgeTextColor]}>{badge}</Text>
            </View>
          </View>
          }
        </View>
        <View style={{flex: 1, marginTop: 12}}>
          <Text style={[
            {fontFamily: 'Rubik-Regular', fontSize: 14, color: Colors.warmGreyThree},
            selected ? {color: Colors.white} : {}
          ]}>
            {description}
          </Text>
        </View>
        {warning !== '' &&
        <View style={[styles.warningContainer, bgWarningColor]}>
          <Icon name='info' size={12} color={Colors.white} />
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={[styles.warningText]}>
              {warning}
            </Text>
          </View>
        </View>
        }
      </View>
    )
  } else {
    packageCard = (
      <View style={[styles.container, styles.shadow, bgColor]}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
        }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
          }}>
            <View>
              <Text style={[styles.title, textColor]}>{name} Package</Text>
            </View>
            <View>
              {tagLine !== '' &&
              <View style={[styles.badgeContainer, discBGColor]}>
                <Text style={[styles.badgeText, discColor]}>{tagLine}</Text>
              </View>
              }
            </View>
          </View>
          <View style={{
            flexDirection: 'column',
            alignItems: 'flex-end',
            justifyContent: 'space-between',
          }}>
            <View>
              {(!disabled && badge !== '') ?
                <View style={[styles.badgeContainer, badgeBgColor]}>
                  <Text style={[styles.badgeText, badgeTextColor]}>{badge}</Text>
                </View>
                :
                <View style={{height: 36}} />
              }
            </View>
            <View style={{alignItems: 'flex-end'}}>
              {sellPrice < basePrice &&
                <Text style={[styles.basePrice, textBasePriceColor]}>{LocaleFormatter.numberToCurrency(basePrice)}</Text>
              }
              <Text style={[styles.sellPriceText, textPriceColor]}>{LocaleFormatter.numberToCurrency(sellPrice)}</Text>
            </View>
          </View>
        </View>
        {warning !== '' &&
          <View style={[styles.warningContainer, bgWarningColor]}>
            <Icon name='info' size={12} color={Colors.white} />
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={[styles.warningText]}>
                {warning}
              </Text>
            </View>
          </View>
        }
      </View>
    )
  }

  if (disabled) {
    return packageCard
  }

  return (
    <TouchableWithoutFeedback 
      testID={newName}
      accessibilityLabel={newName}
      onPress={onPress}
    >
      {packageCard}
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  container: {
    minHeight: 116,
    flexDirection: 'column',
    padding: 20,
    marginVertical: 4,
    borderRadius: 8,
    backgroundColor: Colors.white,
  },
  title: {
    fontFamily: 'Rubik-Medium',
    fontSize: 18,
  },
  badgeText: {
    fontFamily: 'Rubik-Medium',
    fontSize: 12,
  },
  sellPriceText: {
    fontFamily: 'Rubik-Medium',
    fontSize: 16,
  },
  basePrice: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    padding: 4,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  badgeContainer: {
    borderRadius: 12,
    paddingVertical: 4,
    paddingHorizontal: 12,
  },
  shadow: {
    shadowColor: Colors.darkBlue,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    elevation: 2,
  },
  defaultColor: {
    backgroundColor: Colors.white,
  },
  activeColor: {
    backgroundColor: Colors.selectedPackage,
  },
  inactiveColor: {
    backgroundColor: Colors.Grey500,
  },
  defaultTextColor: {
    color: Colors.darkBlue,
  },
  activeTextColor: {
    color: Colors.white,
  },
  inactiveTextColor: {
    color: Colors.warmGreyThree,
  },
  warningContainer: {
    flexDirection: 'row',
    borderRadius: 4,
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginTop: 12,
  },
  warningText: {
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    color: Colors.white,
    marginLeft: 8,
  },
  discountFont: {
    color: Colors.darkBlue,
  }
})

PackageCard.defaultProps = {
  disabled: false,
  selected: false,
  warning: '',
  description: '',
  tagLine: '',
  badge: '',
  sellPrice: 0,
}

PackageCard.propTypes = {
  onPress: PropTypes.func,
  name: PropTypes.string.isRequired,
  sellPrice: PropTypes.number,
  badge: PropTypes.string,
  tagLine: PropTypes.string,
  basePrice: PropTypes.number,
  disabled: PropTypes.bool,
  warning: PropTypes.string,
  description: PropTypes.string,
  selected: PropTypes.bool,
  type: PropTypes.string,
  style: ViewPropTypes.style,
}

export default PackageCard
