import {
  Container,
  Content,
  Footer,
} from 'native-base'
import {
  MealCard,
  MenuSlider,
} from '../../Components/Common'
import {
  Row,
  Section,
} from '../../Components/Layout'
import { CardPlaceholder } from '../../Components/Placeholder'
import * as React from 'react'
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { connect } from 'react-redux'
import NavigationBar from '../../Components/NavigationBar'
import ShoppingCartAlertModal from '../../Components/ShoppingCartAlertModal'
import StaticMealDetailPopUp from '../../Components/StaticMealDetailPopUp'
import icomoonConfig from '../../Images/SvgIcon/selection.json'
import HomeContentActions, { Themes } from '../../Redux/HomeContentRedux'
import ThemeActions, {
  ThemeDetail,
  ThemePeriod,
} from '../../Redux/ThemeRedux'
import Colors from '../../Themes/Colors'
import Banner from './Banner'
import PackageCard from './PackageCard'
import SelectedButton from './SelectedButton'
import MealDetailPopUpActions, {
  MealDetailPopUpReduxState,
} from '../../Redux/MealDetailPopUpRedux'
import CartActions from '../../Redux/CartRedux'
import LogEventService from '../../Services/LogEventService'
import {
  StackActions,
  NavigationActions,
} from 'react-navigation'
import {
  scale,
  verticalScale,
} from 'react-native-size-matters'
import { AppState } from '../../Redux/CreateStore'
import { CartState } from '../../Redux/V2/CartRedux'
import { NavigationDrawerProp } from 'react-navigation-drawer'
import { baseLastUsed } from '../../Redux/V2/LastUsedRedux'

const YumboxIcon = createIconSetFromIcoMoon(icomoonConfig)

const { width: VIEWPORT_WIDTH } = Dimensions.get('window')
const IMAGE_WIDTH = VIEWPORT_WIDTH - (2 * 20)
const IMAGE_HEIGHT = (344 / 688) * IMAGE_WIDTH

export interface ThemeDetailProps {
  navigation: NavigationDrawerProp,
  themeDetail: ThemeDetail,
  fetchingThemeDetail: boolean,
  themeDetailError: string,
  themes: Themes[],
  cart: CartState,
  isConnected: boolean,
  isLoggedIn: boolean,
  mealDetailPopUp: MealDetailPopUpReduxState,
  lastUsed: baseLastUsed,
  fetchThemes: () => void,
  fetchDetail: (id: number, kitchenCode: string) => void,
  clearThemeDetail: () => void,
  selectPackage: (themePackage: ThemePeriod | string) => void,
  fetchMealDetail: (mealId: number, date: string) => void,
  resetMealDetailPopUp: () => void,
  resetCart: () => void,
}

export interface ThemeDetailStates {
  id: number,
  title: string,
  showMealDetailModal: boolean,
  selectedMealId: number,
  selectedMealDate: string,
  selectedPackageIndex: number,
  selectedPackage: ThemePeriod,
  showCombinationAlertModal: boolean,
  bounceValue: Animated.Value,
  isHidden: boolean,
  showLongDescription: boolean,
}

class ThemeDetailScreen extends React.Component<ThemeDetailProps, ThemeDetailStates> {
  onFocus

  constructor(props) {
    super(props)
    this.state = {
      id: null,
      title: null,
      showMealDetailModal: false,
      selectedMealId: null,
      selectedMealDate: null,
      selectedPackageIndex: null,
      selectedPackage: null,
      showCombinationAlertModal: false,
      bounceValue: new Animated.Value(100),  //This is the initial position of the subview
      isHidden: true,
      showLongDescription: false,
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.navigation.state.params &&
      (props.navigation.state.params.id !== state.id)
    ) {
      return {
        id: props.navigation.state.params.id,
        title: props.navigation.state.params.title,
      }
    }
    return null
  }

  componentDidMount() {
    if (this.props.isConnected) {
      this.props.fetchDetail(this.state.id, this.props.lastUsed.code)
      this.props.fetchThemes()
      this.onFocus = this.props.navigation
        .addListener('willFocus', () => {
          this.props.fetchDetail(this.state.id, this.props.lastUsed.code)
          this.props.fetchThemes()
        })
    }
  }

  componentWillUnmount() {
    this.props.clearThemeDetail()
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (this.props.fetchingThemeDetail === true &&
      newProps.fetchingThemeDetail === false) {
      this.setState({
        id: newProps.themeDetail.id,
        title: newProps.themeDetail.name,
      })
      this._selectPackageBasedOnCart(newProps.themeDetail.id,
        newProps.themeDetail.theme_periods)
    }
  }

  _renderThemes = ({ item }) => {
    if (this.state.id === item.id) {
      return
    }

    const image = item.detail_image
    const title = item.name
    const descriptionText = (item.tagline
      ? item.tagline
      : item.description)
    const description = (
      <Text
        numberOfLines={1}
        style={{
          fontFamily: 'Rubik-Regular',
          fontSize: scale(14),
          color: Colors.warmGreyThree,
        }}
      >
        {descriptionText}
      </Text>
    )

    return (
      <MealCard
        style={{
          width: 276,
          marginRight: scale(8),
        }}
        imageContainerStyle={{
          width: 276,
          height: 138,
          borderTopLeftRadius: 8,
          borderTopRightRadius: 8,
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
        }}
        contentStyle={{
          paddingVertical: verticalScale(9),
          paddingHorizontal: 0,
        }}
        titleContainerStyle={{ height: null }}
        titleStyle={{ fontSize: scale(16) }}
        image={image}
        title={title}
        description={description}
        noShadow={true}
        onPress={() => this._changeTheme(item.id)}
      />
    )
  }

  _renderThemeItem = ({ item }) => {
    const image = item.catalog_image
    const title = item.name
    const caption = item.formatted_date

    return (
      <MealCard
        style={{
          height: 199,
          width: 120,
          borderRadius: 8,
          marginTop: 8, // handle shadow
          marginBottom: 12, // handle shadow
          marginLeft: 4, // handle shadow
          marginRight: 4, // handle shadow
        }}
        contentStyle={{ height: 79 }}
        captionStyle={{ fontSize: 11 }}
        titleStyle={{ fontSize: 12 }}
        imageContainerStyle={{
          height: 120,
          width: 120,
        }}
        image={image}
        title={title}
        caption={caption}
        onPress={() => this._selectMeal(item.id, item.date)}
      />
    )
  }

  _selectMeal = (id, date) => {
    this.setState({
      showMealDetailModal: true,
      selectedMealId: id,
      selectedMealDate: date,
    }, () => {
      this.props.fetchMealDetail(id, date)
    })
  }

  _closeModal = () => {
    this.setState({
      showMealDetailModal: false,
      selectedMealId: null,
      selectedMealDate: null,
    }, this.props.resetMealDetailPopUp)
  }

  _selectPackage = (item: ThemePeriod, index) => {
    const { cartItems, type } = this.props.cart
    const cartIsNotEmpty = cartItems.length > 0 && cartItems[0].items.length > 0

    if (
      (cartIsNotEmpty && (type === 'item'))
      || (cartIsNotEmpty && (type === 'package') && (item.type !== 'package'))
      || (cartIsNotEmpty && (type === 'subscription') && (item.type !== 'subscription'))
    ) {
      this.setState({ showCombinationAlertModal: true })
    } else {
      if (this.state.selectedPackageIndex !== index) {
        if (item.type === 'subscription') {
          this.props.selectPackage(item.type)
        } else {
          this.props.selectPackage(item)
        }
        this.setState({
          selectedPackageIndex: index,
          selectedPackage: item,
        }, () => {
          if (this.state.isHidden) {
            this._toggleSubview()
          }
        })
      }
    }
  }

  _toggleSubview = () => {
    let toValue = 76

    if (this.state.isHidden) {
      toValue = 0
    }

    //This will animate the transalteY of the subview between 0 & 100 depending on its current state
    //100 comes from the style below, which is the height of the subview.
    Animated.spring(
      this.state.bounceValue,
      {
        toValue: toValue,
        velocity: 3,
        // tension: 2,
        friction: 6,
        useNativeDriver: true,
      },
    )
      .start()

    this.setState({ isHidden: !this.state.isHidden })
    this.props.resetCart()
  }

  _navigateToNextScreen = () => {
    const { isLoggedIn } = this.props
    const {
      selectedPackage,
    } = this.state

    if (!isLoggedIn) {
      this.props.navigation.navigate('LoginLandingScreen')
    } else {
      let logData = {
        currency: 'IDR',
        item_price: selectedPackage.sell_price,
        playlist_id: this.props.themeDetail.id,
        playlist_name: this.props.themeDetail.name,
        item_category: selectedPackage.type,
      }
      LogEventService.logEvent('add_to_cart', logData, ['default'])
      if (selectedPackage.type === 'subscription') {
        this.props.navigation.navigate('SubscriptionScheduleScreen', {})
      } else {
        this.props.navigation.navigate(
          'ThemeCalendarScreen',
          {
            themeId: this.props.themeDetail.id,
            key: 'themeDetailScreen'
          },
        )
      }
    }
  }

  _selectPackageBasedOnCart = (themeId, themePeriods) => {
    const selectedIndex = this._getPackageIndex(
      this.props.cart,
      themeId,
      themePeriods,
    )
    this.setState({
      selectedPackageIndex: selectedIndex,
    })
    if (selectedIndex !== null) {
      if (this.props.cart.type === 'subscription') {
        this.props.selectPackage(this.props.cart.type)
      } else {
        this.props.selectPackage(themePeriods[selectedIndex])
      }
      this.setState({
          selectedPackage: themePeriods[selectedIndex]
        }, this._toggleSubview)
    }
  }

  _getPackageIndex = (cart, themeId, themePackages) => {
    let selectedIndex = null
    if (cart.type === 'package' && cart.cartItems.length > 0) {
      const cartPackageItem = cart.cartItems[0].packages[0]

      if (cartPackageItem.theme_id === themeId && cart.type === 'package') {
        for (let i = 0; i < themePackages.length; i++) {
          if (themePackages[i].id === cartPackageItem.package_id) {
            selectedIndex = i
          }
        }
      }
    } else if (cart.type === 'subscription' &&
      cart.cartItems[0].subscription.theme_id === themeId &&
      cart.cartItems.length > 0) {
      selectedIndex = (themePackages.length - 1)
    }

    return selectedIndex
  }

  _changeTheme = (id) => {
    this.setState({
      showMealDetailModal: false,
      selectedMealId: null,
      selectedMealDate: null,
      selectedPackageIndex: null,
      selectedPackage: null,
      showCombinationAlertModal: false,
      bounceValue: new Animated.Value(100),
      isHidden: true,
    }, () => this.props.fetchDetail(id, this.props.lastUsed.code))
  }

  _showDescription = (description) => {
    if (description.length <= 220) {
      return (
        <Text style={styles.aboutContent}>
          {description}
        </Text>
      )
    }

    if (this.state.showLongDescription) {
      return (
        <View style={{ flex: 1 }}>
          <Text style={styles.aboutContent}>
            {description} &nbsp;
            <Text style={styles.viewMore} onPress={
              () => this.setState(
                { showLongDescription: !this.state.showLongDescription })
            }>View Less</Text>
          </Text>
        </View>
      )
    }

    return (
      <View style={{ flex: 1 }}>
        <Text style={styles.aboutContent} numberOfLines={3}>
          {description}
        </Text>
        <View style={styles.gradientBox}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.setState({
                showLongDescription: !this.state.showLongDescription,
              })
            }}
          >
            <LinearGradient
              colors={[
                'rgba(255,255,255,0)',
                '#fff',
              ]}
              style={{
                overflow: 'visible',
                alignItems: 'flex-end',
                paddingRight: 5,
              }}
              start={{
                x: 0.2,
                y: 0.5,
              }}
              end={{
                x: 0.8,
                y: 0.5,
              }}
              locations={[
                0.0,
                1.0,
              ]}
            >
              <Text
                style={[
                  styles.viewMore,
                  { left: 4 },
                ]}
                onPress={
                  () => this.setState({
                    showLongDescription: !this.state.showLongDescription,
                  })
                }
              >
                View More</Text>
            </LinearGradient>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }

  onPressCompleteOrder = () => {
    const { isLoggedIn } = this.props
    if (isLoggedIn) {
      if (this.props.cart.type === 'subscription') {
        this.props.navigation.navigate('SubscriptionScheduleScreen')
      } else {
        this.props.navigation.navigate('SingleOrderCheckoutScreen')
      }
    } else {
      this.props.navigation.navigate('LoginLandingScreen')
    }
  }

  navigationGoBack = () => {
    const parent = this.props.navigation.dangerouslyGetParent()
    if (parent && parent.state && (parent.state.routeName === 'NoAuthDrawer')) {
      this.props.navigation.goBack()
    } else {
      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
        }),
      )
    }
  }

  render() {
    const {
      mealDetailPopUp,
    } = this.props
    return (
      <Container>
        <NavigationBar
          leftSide='back'
          title={this.state.title}
          leftSideNavigation={this.navigationGoBack}
        />
        <Content style={{ flex: 1 }}>
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <Row style={{
              marginBottom: verticalScale(16),
              paddingBottom: 0,
            }}>
              <Section
                padder={true}
                style={{
                  paddingBottom: 0,
                  marginBottom: 0,
                }}
              >
                {(
                  (this.props.fetchingThemeDetail === true &&
                    this.props.themeDetailError)
                  || this.props.themeDetail === null
                )
                  ? (
                    <CardPlaceholder
                      width={IMAGE_WIDTH}
                      height={IMAGE_HEIGHT}
                      style={{
                        paddingTop: 0,
                        paddingLeft: 0,
                        paddingRight: 0,
                        paddingBottom: 0,
                      }}
                    />
                  )
                  : (
                    <Banner
                      image={this.props.themeDetail.detail_image}
                      style={{
                        width: IMAGE_WIDTH,
                        height: IMAGE_HEIGHT,
                      }}
                    />
                  )
                }
                <View>
                  <Text style={styles.heading}>Delivery Period</Text>
                  <View style={styles.periodeContainer}>
                    <View style={styles.periodeCalendar}>
                      <YumboxIcon
                        name='calendar-orange'
                        size={scale(20)}
                        color={Colors.darkBlue}
                        style={{ fontWeight: 'bold' }}
                      />
                    </View>
                    <View style={styles.periodeContent}>
                      {(
                        (this.props.fetchingThemeDetail === true &&
                          this.props.themeDetailError)
                        || this.props.themeDetail === null
                      )
                        ?
                        <View>
                          <CardPlaceholder
                            width={100}
                            height={12}
                            style={{ padding: 0 }}
                          />
                        </View>
                        :
                        <Text style={[
                          styles.periodeText,
                          (this.props.themeDetail.delivery_period.type !==
                            'regular')
                            ? styles.periodeTextColor
                            : {},
                        ]}
                        >
                          {this.props.themeDetail.delivery_period.label}
                        </Text>
                      }
                    </View>
                  </View>
                </View>
                <View>
                  <Text style={styles.heading}>About the Playlist</Text>
                  <View style={styles.aboutContainer}>
                    {(
                      (this.props.fetchingThemeDetail === true &&
                        this.props.themeDetailError)
                      || this.props.themeDetail === null
                    )
                      ?
                      <View>
                        <CardPlaceholder
                          width={250}
                          height={12}
                          style={{ padding: 0 }}
                        />
                        <CardPlaceholder
                          width={250}
                          height={12}
                          style={{ padding: 0 }}
                        />
                        <CardPlaceholder
                          width={150}
                          height={12}
                          style={{ padding: 0 }}
                        />
                      </View>
                      :
                      this.props.themeDetail.description !== null &&
                      this._showDescription(this.props.themeDetail.description)
                    }
                  </View>
                </View>
                <View>
                  <Text style={styles.heading}>Meal Plan</Text>
                  <View>
                    {(
                      (this.props.fetchingThemeDetail === true &&
                        this.props.themeDetailError)
                      || this.props.themeDetail === null
                    )
                      ?
                      <View style={{ flex: 1 }}>
                        <View
                          style={{
                            width: 276,
                            justifyContent: 'flex-start',
                            flexDirection: 'row',
                            marginBottom: verticalScale(16),
                          }}
                        >
                          <CardPlaceholder
                            height={199}
                            width={120}
                            style={{
                              backgroundColor: 'transparent',
                              marginTop: 16,
                              paddingTop: 0,
                              paddingRight: 0,
                              paddingLeft: 0,
                              paddingBottom: 0,
                            }}
                          />
                          <CardPlaceholder
                            height={199}
                            width={120}
                            style={{
                              backgroundColor: 'transparent',
                              marginTop: 16,
                              paddingTop: 0,
                              paddingRight: 0,
                              paddingLeft: 0,
                              paddingBottom: 0,
                            }}
                          />
                        </View>
                      </View>
                      :
                      <View style={{ flex: 1 }}>
                        {this.props.themeDetail.items.length > 0
                          ?
                          <MenuSlider
                            style={{
                              marginTop: verticalScale(16),
                              paddingRight: scale(20),
                              width: (VIEWPORT_WIDTH + 8), // handle shadow
                              top: verticalScale(-8), // handle shadow
                              left: scale(-4), // handle shadow
                            }}
                            items={this.props.themeDetail.items}
                            renderItem={this._renderThemeItem}
                          />
                          :
                          <View
                            style={[
                              styles.aboutContainer,
                              { marginBottom: 16 },
                            ]}>
                            <Text style={[
                              styles.aboutContent,
                              { color: Colors.warmGreyThree },
                            ]}>
                              Sorry! There are no meals scheduled for the
                              selected Food Playlist. Please select another Food
                              Playlist.
                            </Text>
                          </View>
                        }
                      </View>
                    }
                  </View>
                </View>
              </Section>
            </Row>
            <Row style={{ marginBottom: verticalScale(16) }}>
              <Section
                padder={true}
                style={{
                  paddingTop: 0,
                  marginTop: 0,
                }}
              >
                <View>
                  <Text style={styles.heading}>Select Package</Text>
                  <View style={styles.packageContainer}>
                    {(
                      (this.props.fetchingThemeDetail === true &&
                        this.props.themeDetailError)
                      || this.props.themeDetail === null
                    )
                      ?
                      <View>
                        <View
                          style={{
                            marginBottom: verticalScale(16),
                          }}
                        >
                          <CardPlaceholder
                            height={116}
                            style={{
                              backgroundColor: 'transparent',
                              marginVertical: 4,
                              paddingTop: 0,
                              paddingRight: 0,
                              paddingLeft: 0,
                              paddingBottom: 0,
                            }}
                          />
                          <CardPlaceholder
                            height={116}
                            style={{
                              backgroundColor: 'transparent',
                              marginVertical: 4,
                              paddingTop: 0,
                              paddingRight: 0,
                              paddingLeft: 0,
                              paddingBottom: 0,
                            }}
                          />
                        </View>
                      </View>
                      :
                      this.props.themeDetail.theme_periods.map(
                        (_item: ThemePeriod, index: number) => (
                          <PackageCard
                            onPress={() => this._selectPackage(_item, index)}
                            name={_item.name}
                            badge={_item.badge}
                            tagLine={_item.tag_line}
                            basePrice={_item.base_price}
                            sellPrice={_item.sell_price}
                            disabled={_item.disabled}
                            warning={_item.warning}
                            description={_item.description}
                            selected={(index ===
                              this.state.selectedPackageIndex)}
                            type={_item.type}
                            key={`PackageCard${_item.id}`}
                          />
                        ))
                    }
                  </View>
                </View>
              </Section>
            </Row>
            <Row>
              <Section
                padder={true}
                style={{
                  paddingTop: 0,
                  marginTop: 0,
                  paddingBottom: 0,
                }}
              >
                <Text
                  style={{
                    ...styles.heading,
                    fontSize: scale(20),
                  }}
                >
                  Checkout Other Playlist
                </Text>
                <View>
                  {this.props.themes === null
                    ?
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: 138,
                        marginTop: verticalScale(16),
                      }}
                    >
                      <ActivityIndicator size='small' color='#ff5100' />
                    </View>
                    :
                    <View>
                      <MenuSlider
                        style={{
                          marginTop: verticalScale(24),
                          paddingRight: scale(20),
                          width: (VIEWPORT_WIDTH + 8), // handle shadow
                          top: verticalScale(-8), // handle shadow
                          left: scale(-4), // handle shadow
                        }}
                        items={this.props.themes}
                        renderItem={this._renderThemes}
                      />
                    </View>
                  }
                </View>
              </Section>
            </Row>
            {this.state.selectedPackage !== null &&
            <Animated.View
              style={{
                flex: 1,
                height: 76,
                transform: [{ translateY: this.state.bounceValue }],
              }}
            >
            </Animated.View>
            }
          </ScrollView>
          <StaticMealDetailPopUp
            closeModal={this._closeModal}
            visible={this.state.showMealDetailModal}
            mealId={this.state.selectedMealId}
            date={this.state.selectedMealDate}
            mealDetailPopUp={mealDetailPopUp}
          />
        </Content>
        <ShoppingCartAlertModal
          primaryAction={() => this.onPressCompleteOrder()}
          secondaryAction={() => {
            this.setState({
              selectedPackageIndex: null,
              selectedPackage: null,
            }, this._toggleSubview)
          }}
          visible={this.state.showCombinationAlertModal}
          dismiss={() => this.setState({ showCombinationAlertModal: false })}
        />
        {this.state.selectedPackage &&
        <Footer style={{ backgroundColor: 'white' }}>
          <SelectedButton
            title={this.state.selectedPackage.type === 'subscription'
              ? 'Price/meal/day starts from'
              : 'Price for ' + this.state.selectedPackage.period + ' Days'
            }
            basePrice={this.state.selectedPackage.type === 'subscription'
              ? this.state.selectedPackage.lowest_price
              : this.state.selectedPackage.base_price
            }
            sellPrice={this.state.selectedPackage.type === 'subscription'
              ? this.state.selectedPackage.lowest_price
              : this.state.selectedPackage.sell_price
            }
            onPress={() => this._navigateToNextScreen()}
            bounceValue={this.state.bounceValue}
          />
        </Footer>
        }
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  heading: {
    marginTop: verticalScale(16),
    fontFamily: 'Rubik-Medium',
    fontSize: scale(16),
    color: Colors.darkBlue,
  },
  periodeContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: verticalScale(12),
  },
  periodeCalendar: {
    justifyContent: 'center',
    marginRight: scale(8),
  },
  periodeContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  periodeText: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(16),
    color: Colors.darkBlue,
  },
  periodeTextColor: {
    color: Colors.pinkishOrange,
  },
  aboutContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: verticalScale(12),
  },
  aboutContent: {
    fontFamily: 'Rubik-Light',
    fontSize: scale(16),
    lineHeight: verticalScale(24),
    textAlign: 'justify',
    color: Colors.darkBlue,
  },
  viewMore: {
    fontFamily: 'Rubik-Regular',
    fontSize: scale(16),
    color: Colors.grennBlue,
    backgroundColor: 'transparent',
    textDecorationLine: 'underline',
  },
  gradientBox: {
    position: 'absolute',
    bottom: verticalScale(2),
    right: 0,
    width: '100%',
  },
  packageContainer: {
    marginTop: verticalScale(12),
  },
})

const mapStateToProps = (state: AppState) => {
  return {
    themeDetail: state.theme.themeDetail,
    fetchingThemeDetail: state.theme.isFetchingThemeDetail,
    themeDetailError: state.theme.detailError,
    themes: state.homeContent.themes,
    cart: state.cart,
    isConnected: state.network.isConnected,
    isLoggedIn: state.login.user !== null,
    mealDetailPopUp: state.mealDetailPopUp,
    lastUsed: state.lastUsed
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchThemes: () => dispatch(HomeContentActions.fetchThemes()),
    fetchDetail: (id: number, kitchenCode: string) => dispatch(ThemeActions.fetchDetail(id, kitchenCode)),
    clearThemeDetail: () => dispatch(ThemeActions.clearThemeDetail()),
    selectPackage: (themePackage: ThemePeriod) => dispatch(
      ThemeActions.selectPackage(themePackage)),
    fetchMealDetail: (mealId: number, date: string) => dispatch(
      MealDetailPopUpActions.fetchMealDetail(mealId, date)),
    resetMealDetailPopUp: () => dispatch(
      MealDetailPopUpActions.resetMealDetailPopUp()),
    resetCart: () => dispatch(CartActions.resetCart()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ThemeDetailScreen)
