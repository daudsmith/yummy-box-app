import apisauce, { ApiOkResponse } from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { CustomerData, CustomerForm } from '../Redux/RegisterRedux'
import { User } from '../Redux/LoginRedux'
import { Card } from '../Redux/CustomerCardRedux'
import { Address } from '../Redux/CustomerAddressRedux'
import { VirtualAccount } from '../Redux/CustomerVirtualAccountRedux'
import { Addresses } from '../Redux/AddressHistoryRedux'

export interface customerProfileApiResponse {
  error: boolean
  data: User
  token: string
}

export interface authApiResponse {
  error: boolean | string
  data: User
  token: string
  message?: string
}

export interface registerApiResponse {
  error: boolean
  data: CustomerData
  message?:string
  token:string
}

export interface validateApiResponse {
  error: boolean
  exist: boolean
  message?: string
}

export interface getSavedCardsApiResponse {
  error: boolean
  data: Card[]
  message?: string
}

export interface saveCardApiResponse {
  error: boolean
  data: Card
  message?: string
}

export interface logoutApiResponse {
  error: boolean
  data: string
}

export interface defaultCardApiResponse {
  error: boolean
  data: string
  message?: string
}

export interface getSavedAddressesApiResponse {
  error: boolean
  data: Address[]
  message?: string
}

export interface getAddressHistoryApiResponse {
  error: boolean
  message?: string
  data: Addresses[]
}

export interface saveAddressApiResponse {
  error: boolean
  message: string
  data: Address[]
}

export interface deleteAddressApiResponse {
  error: boolean
  message: string
  data: Address[]
}

export interface FormAddress {
  latitude: string
  longitude: string
  addressLabel: string
  addressPinpoint: string
  addressDetail: string
  recipientName: string
  recipientNumber: string
  deliveryInstructions: string
}

export interface setDefaultAddressApiResponse {
  error: boolean
  message: string
  status_code: number
}

export interface fetchVirtualAccountApiResponse {
  error: boolean
  data: VirtualAccount[]
  message?: string
}

export interface DeviceInfo {
  id: number
  uid: string
  account_id: number
  fcm_token: string
}

export interface deviceInfoUpdateApiResponse {
  error: boolean
  data: DeviceInfo
}

export interface validateIdentityApiResponse {
  error: boolean
  data: boolean
}

export interface getWalletApiResponse {
  error: boolean
  data: number
  message?: string
  coorporate?: number
  personal?: number
}

export interface redeemCreditTokenApiResponse {
  error: boolean
  data: number
  message?: string
}

export interface photoUploadApiResponse {
  error: boolean
  data: User
  message: string
}

export interface PhotoUpload {
  uri: string
  photo: string
}

export interface updateAccountInfoFieldApiResponse {
  error: boolean
  message?: string
  data: string
}

export interface checkClientApiResponse {
  error: boolean
  data: boolean
}

const create = (baseURL: string = AppConfig.apiBaseUrl, apiCaterUrl: string = AppConfig.apiCaterUrl) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
    },
    // 10 second timeout...
    timeout: 15000,
  })

  const customerProfile = (token: string) => {
    if (token)
        api.setHeader('Authorization', `Bearer ${token}`)

    return api.get<customerProfileApiResponse>('public/account/me')
  }

  const auth = (username: string, password: string) => api.post<authApiResponse>('public/account/login', {
    email: username,
    password: password,
  })

  const register = (customer: CustomerForm) => api.post<registerApiResponse>('public/account/register', customer)

  const logout = (token: string, uid: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<logoutApiResponse>('public/account/logout', { uid })
  }
  const validate = (customer: CustomerForm) => api.post<validateApiResponse>('public/account/check', customer)

  const getSavedCards = (token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.get<getSavedCardsApiResponse>('public/account/card')
  }

  const saveCard = (card: Card, token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<saveCardApiResponse>('public/account/card/create', {
      token: card.token,
      authorization_id: card.authorization_id,
      type: card.type,
      ends_with: card.ends_with,
    })
  }

  const deleteCard = (cardId: number, token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.delete<defaultCardApiResponse>(`public/account/card/${cardId}/delete`)
  }

  const setDefaultCard = (cardId: number, token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.put<defaultCardApiResponse>(`public/account/card/${cardId}/default`)
  }

  const getSavedAddresses = (token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.get<getSavedAddressesApiResponse>('public/account/address')
      .then((response: ApiOkResponse<getSavedAddressesApiResponse>) => response.data)
      .catch(error => error.response.data)
  }

  const getAddressHistory = (token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.get<getAddressHistoryApiResponse>('public/account/address/history')
  }

  const saveAddress = (token: string, address: FormAddress) => {
    const {
      latitude,
      longitude,
      addressLabel,
      addressPinpoint,
      addressDetail,
      recipientName,
      recipientNumber,
      deliveryInstructions
    } = address

    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<saveAddressApiResponse>(
      'public/account/address/create', {
      landmark: addressPinpoint,
      address: addressDetail,
      latitude: latitude,
      longitude: longitude,
      name: recipientName,
      phone: recipientNumber,
      label: addressLabel,
      note: deliveryInstructions
    })
  }

  const deleteAddress = (addressId: number, token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.delete<deleteAddressApiResponse>(`public/account/address/${addressId}/destroy`)
  }

  const setDefaultAddress = (addressId: number, token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<setDefaultAddressApiResponse>(`public/account/address/${addressId}/default`)
  }

  const editAddress = (addressId: number, address: FormAddress, token: string) => {
    const {
      latitude,
      longitude,
      addressLabel,
      addressPinpoint,
      addressDetail,
      recipientName,
      recipientNumber,
      deliveryInstructions
    } = address

    api.setHeader('Authorization', `Bearer ${token}`)
    return api.post<saveAddressApiResponse>(
      `public/account/address/${addressId}/update`, {
      landmark: addressPinpoint,
      address: addressDetail,
      latitude: latitude,
      longitude: longitude,
      name: recipientName,
      phone: recipientNumber,
      label: addressLabel,
      note: deliveryInstructions,
    })
  }

  const fetchVirtualAccount = (bank: string, token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<fetchVirtualAccountApiResponse>('public/payment/virtual_account', { bank_code: bank })
  }

  const getVirtualAccounts = (token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.get<fetchVirtualAccountApiResponse>('public/payment/virtual_account')
  }

  const deviceInfoUpdate = (uid: string, fcmToken: string, token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<deviceInfoUpdateApiResponse>('public/account/device/update', {
      uid: uid,
      fcm_token: fcmToken,
    })
  }

  const validateIdentity = (identity: string, email: string, phone: string) => {
    return api.get<validateIdentityApiResponse>('public/account/validate', {
      identity,
      email,
      phone,
    })
  }

  const getWallet = (token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.get<getWalletApiResponse>('public/account/wallet')
  }

  const redeemCreditToken = (token: string, code: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<redeemCreditTokenApiResponse>('public/account/wallet/redeem', { token: code })
  }

  const photoUpload = (token: string, data: object) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<photoUploadApiResponse>('public/account/upload', data)
  }

  const changePassword = (token: string, old_password: string, password: string, password_confirmation: string
    ) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<updateAccountInfoFieldApiResponse>('public/account/password/update', {
      old_password,
      password,
      password_confirmation,
    })
  }

  const forgotPassword = (email: string) => {
    return api.post<updateAccountInfoFieldApiResponse>('public/forgot_password', { email })
  }

  const changePhoneNumber = (token: string, newPhoneNumber: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<updateAccountInfoFieldApiResponse>('public/account/update/phone', { phone: newPhoneNumber })
  }

  const changeEmail = (token: string, newEmail: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.post<updateAccountInfoFieldApiResponse>('public/account/update/email', { email: newEmail })
  }

  const checkClient = (token: string) => {
    if (token)
      api.setHeader('Authorization', `Bearer ${token}`)

    return api.get<checkClientApiResponse>(`${apiCaterUrl}/is_client`)
  }

  return {
    customerProfile,
    auth,
    register,
    logout,
    validate,
    getSavedCards,
    saveCard,
    deleteCard,
    setDefaultCard,
    getSavedAddresses,
    saveAddress,
    deleteAddress,
    setDefaultAddress,
    editAddress,
    fetchVirtualAccount,
    deviceInfoUpdate,
    validateIdentity,
    getWallet,
    getVirtualAccounts,
    redeemCreditToken,
    photoUpload,
    changePassword,
    forgotPassword,
    getAddressHistory,
    changePhoneNumber,
    changeEmail,
    checkClient,
  }
}

export default {
  create,
}
