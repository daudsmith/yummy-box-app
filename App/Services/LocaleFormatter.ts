import AppConfig from '../Config/AppConfig'
import Moment from 'moment'
import { Platform } from 'react-native'
import {
  Tags,
  TagsData,
} from '../Redux/MealsRedux'

const numberToCurrency = (number: number): string => {
  if (!number) {
    return AppConfig.defaultCurrencySymbol + 0
  }
  let rev = number.toString().split('').reverse().join('')
  let rev2 = ''
  for (let i = 0; i < rev.length; i++) {
    rev2 += rev[i]
    if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
      rev2 += '.'
    }
  }
  return AppConfig.defaultCurrencySymbol + rev2.split('')
    .reverse()
    .join('')
}

const convertSecondsToMinutes = (seconds: number): string => {
  let minutes: number
  if (seconds < 61) {
    return seconds.toString() + 's'
  }

  minutes = Math.floor(seconds / 60)
  seconds = seconds % 60

  return minutes.toString() + 'm' + ' ' + seconds.toString() + 's'
}

const formatBankAccountNumber = (accountNumber: string): string => {
  const divider = 4
  const sectionCount = Math.ceil(accountNumber.length / divider)
  let sections = []
  for (let i = 0; i < sectionCount; i++) {
    const start = i * divider
    const end = (i + 1) * divider
    const section = accountNumber.substring(start, end)
    sections.push(section)
  }
  return sections.join('-')
}

const pluralTranslation = (quantity: number, singularWord: string, pluralWord: string): string => {
  if (quantity > 1) {
    return `${quantity} ${pluralWord}`
  }

  return `${quantity} ${singularWord}`
}


const addSpacingToPhoneNumber = (phone: string): string => {
  return phone.replace(/(\d{2})(\d{3})(\d{4})/, '$1 $2 $3 ')
}

export interface Tag {
  data: Tags[],
  tagString: string

}
const createTagString = (tags: TagsData, limit?: number): string => {
  var tagString = ''
  const tagLimit = limit === null || typeof limit === 'undefined' || limit > tags.data.length ? tags.data.length : limit

  for (let i = 0; i < tagLimit; i++) {
    if (i === tagLimit - 1) {
      tagString = tagString + tags.data[i].name
    } else {
      tagString = tagString + tags.data[i].name + ' \u2022 '
    }
  }

  return tagString
}

const isEmailFormatValid = (email: string): boolean => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

const isPhoneFormatValid = (phone: string): boolean => {
  const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
  return re.test(phone)
}

const deepLinkUrlParser = (url: string): string => {
  if (Platform.OS === 'ios') {
    const isUrlContainLongDeepLink = url.indexOf(AppConfig.FIREBASE_LONG_DOMAIN) > -1
    return isUrlContainLongDeepLink
      ? url
      : iOSUniversalLinkParser(url)
  }
  return url
}


const iOSUniversalLinkParser = (url: string): string => {
  let deeplink = `${AppConfig.FIREBASE_LONG_DOMAIN}?`
  const parametersArray = url.replace(`${AppConfig.FIREBASE_APP_DOMAIN_HTTPS}/`, '').split('-')
  for (let parameter of parametersArray) {
    const keyAndValue = parameter.split('_')
    if (keyAndValue[0] === 'date') {
      const parsedDate = Moment(keyAndValue[1]).format('YYYY-MM-DD')
      deeplink = `${deeplink}${keyAndValue[0]}=${parsedDate}&`
    } else {
      deeplink = `${deeplink}${keyAndValue[0]}=${keyAndValue[1]}&`
    }
  }

  return deeplink
}

export default {
  numberToCurrency,
  convertSecondsToMinutes,
  formatBankAccountNumber,
  pluralTranslation,
  addSpacingToPhoneNumber,
  createTagString,
  deepLinkUrlParser,
  isEmailFormatValid,
  isPhoneFormatValid
}
