import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { Banner } from '../Redux/PromotionRedux'

export interface getPromotionBannersApiResponse {
  error: boolean
  data: Banner[]
  message?: string
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 15000
  })

  const getPromotionBanners = () => api.get<getPromotionBannersApiResponse>('public/promotion/banners')

  return {
    getPromotionBanners
  }
}

export default {
  create
}
