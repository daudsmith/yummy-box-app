import { NavigationActions, StackActions } from 'react-navigation'
import SalesOrderService from './SalesOrderService'

const selectKitchen = (token, navigation, addLastUsed = null) => {
  
    SalesOrderService.getUserAddress(token).then(res => {
      if (res.corporate && res.corporate.length > 0) {
        navigateToAddressType(res, navigation, addLastUsed)
      } else if (res.personal.length > 0) {
        navigateToAddressOption(res, navigation, addLastUsed)
      } else {
        navigateToIntroTwoScreen(navigation, addLastUsed)
      }

    })

}

const navigateToAddressType = (addresses, navigation, addLastUsed) => {
  const params = {
    corporate: addresses.corporate,
    personal: addresses.personal,
    optionScreen: 'AddressOptionScreen',
    showBackButton: true,
    showAnotherLocationButton: true,
    selectPinpoint: true
  }
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'AddressTypeScreen' ,params})],
  })
  navigation.dispatch(resetAction)

  if (addLastUsed) {
    addLastUsed(null, null, null, null)
  }
}

const navigateToAddressOption = (addresses, navigation, addLastUsed ) => {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'AddressOptionScreen' , params:{ 
      addresses: addresses.personal, 
      showBackButton: false,
      selectPinpoint: true,
      showAnotherLocationButton: true
    }})],
  })
  navigation.dispatch(resetAction)

  if (addLastUsed) {
    addLastUsed(null, null, null, null)
  }
}

const navigateToSearchLocation = (navigation, addLastUsed) => {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'SearchLocation' , params:{ 
      noSearchLocation: true,
    }})],
  })
  navigation.dispatch(resetAction)
  
  if (addLastUsed) {
    addLastUsed(null, null, null, null)
  }
}

const navigateToIntroTwoScreen = (navigation, addLastUsed) => {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'IntroTwoScreen'})],
  })
  navigation.dispatch(resetAction)
  
  if (addLastUsed) {
    addLastUsed(null, null, null, null)
  }
}


export default {
  selectKitchen, 
  navigateToAddressOption,
  navigateToAddressType,
  navigateToSearchLocation
}