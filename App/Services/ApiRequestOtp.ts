import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'

export interface authyRespons {
  success: boolean
  message: string
  cellphone: string
}

const create = (baseURL: string = AppConfig.apiRequestApi) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 15000
  })

  const requestOtp = (newPhoneNumber: string) => {
    api.setHeader('X-Authy-API-Key', 'bt2Lu39QhnfQrZW8Rh69oYmOFwZekqbK')
    return api.post<authyRespons>(`start?via=sms&country_code=+62&phone_number=${newPhoneNumber}&locale=en`)
  }

  return {
    requestOtp
  }
}

export default {
  create
}
