import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { ListOrderDetail, SalesOrderDetail, ListSubscriptionDetail, Delivery } from '../Redux/CustomerOrderRedux'

export interface SalesOrderData {
  in_progress: ListOrderDetail[]
  complete: ListOrderDetail[]
}
export interface SubscriptionOrderData {
  in_progress: ListSubscriptionDetail[]
  complete: ListSubscriptionDetail[]
}

export interface salesOrderResponse {
  error: boolean
  data: SalesOrderData
  message?: string
}

export interface subscriptionOrderResponse {
  error: boolean
  data: SubscriptionOrderData
  message?: string
}

export interface OrderDetailResponse {
  error: boolean
  data: SalesOrderDetail
  message?: string
}

export interface SubscriptPauseResponse {
  error: boolean
  data: object
  message?: string
}

export interface LastOrderResponse {
  error: boolean
  data: LastOrderData
}

export interface LastOrderData {
  id: number
  created_at: string
  updated_at: string
  status: string
  order_number: string
  type: string
  coupon_code: string
  order_comment: string
  source: string
  base_total_charged: number
  base_total_due: number
  base_total_paid: number
  base_total_refunded: number
  base_total_cancelled: number
  tax_amount_charged: number
  tax_amount_due: number
  tax_amount_paid: number
  discount_amount: number | string
  discount_amount_refunded: number
  discount_amount_cancelled: number
  delivery_fee: number
  total_amount_charged: number
  total_amount_due: number
  total_amount_paid: number
  total_amount_refunded: number
  total_amount_cancelled: number
  account_name: string
  payment: string
  total_item_in_cart: number
  client_id: number
  meal_function_id: number
  meal_function_name: string
  channel_id: number
  total_free_charged: number
  deliveries: {
    data: Delivery[]
  }
}

export interface ModifySubscriptionBody {
  default_delivery_time: string
  default_delivery_address: string
  default_delivery_note: string
  default_delivery_latitude: number
  default_delivery_longitude: number
  default_delivery_details: string
  default_recipient_name: string
  default_recipient_phone: string
  default_payment_method: string
  default_payment_info: string
  repeat_interval: string
  theme_id: number
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 15000
  })

  const getOrderList = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<salesOrderResponse>('public/sales/order')
  }
  const getSubscriptionList = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<subscriptionOrderResponse>('public/sales/subscription')
  }
  const getOrderDetail = (token: string, id: number, type: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<OrderDetailResponse>(`public/sales/order/detail/${id}?type=${type}`)
  }
  const getSubscriptionOrderDetail = (token: string, id: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<OrderDetailResponse>(`public/sales/subscription/${id}/detail`)
  }
  const stopSubscription = (token: string, id: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<SubscriptPauseResponse>(`public/sales/subscription/${id}/pause`)
  }
  const modifySubscriptionOrder = (token: string, data: ModifySubscriptionBody, id: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.post<OrderDetailResponse>(`public/sales/subscription/${id}/update`, {...data})
  }
  const getLastOrder = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<LastOrderResponse>('public/sales/order/last')
  }

  return {
    getOrderList,
    getSubscriptionList,
    getOrderDetail,
    getSubscriptionOrderDetail,
    stopSubscription,
    modifySubscriptionOrder,
    getLastOrder
  }
}

export default {
  create
}
