import { Delivery } from '../Redux/CustomerOrderRedux'

const parseStatusToLabel = (status: string): 'On The Way' | 'Delivered' | 'Complete' | 'Request For Cancel' | 'Cancelled' | 'Failed' | 'Cooking' | 'Skipped' | 'Scheduled' => {
  switch (status) {
    case 'ON_THE_WAY' || 'DISTRIBUTION' || 'DISPATCH' || 'ASSEMBLY':
      return 'On The Way'
    case 'DELIVERED':
      return 'Delivered'
    case 'COMPLETE':
      return 'Complete'
    case 'REQUEST_CANCEL':
      return 'Request For Cancel'
    case 'CANCELLED':
      return 'Cancelled'
    case 'FAILED':
      return 'Failed'
    case 'COOKING':
      return 'Cooking'
    case 'SKIP':
      return 'Skipped'
    default:
      return 'Scheduled'
  }
}


const deliveryFeeTotal = (deliveries: Delivery[]): number => {
  let deliveryFee = 0
  for (let i = 0; i < deliveries.length; i++) {
    deliveryFee += deliveries[i].delivery_fee
  }
  return deliveryFee
}
const parseStatusToMessage = (status: string): 'is ' | 'has been ' => {
  switch (status) {
    case 'on the way':
      return 'is '
    case 'delivered':
    case 'request cancel':
    case 'cancelled':
      return 'has been '
    default:
      return 'is '
  }
}

export default {
  parseStatusToLabel,
  deliveryFeeTotal,
  parseStatusToMessage
}
