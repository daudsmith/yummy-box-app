import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { CartState } from '../Redux/CartRedux'

export interface apiResponse {
  error: boolean
  data: CartState
  meta?: cartMeta
  message?: string
}

export interface cartMeta {
  promotion: promotionData
}

interface promotionData {
  isValid: boolean
  message: string
  data: any[]
}

const create = (baseURL: string = `${AppConfig.apiBaseUrl}/public/cart`) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 15000
  })

  const validate = (cart?: CartState, token?: string) => {
    if (token) {
      api.setHeader('Authorization', `Bearer ${token}`)
    }
    return api.post<apiResponse>('/refresh', cart)
  }

  return {
    validate
  }
}

export default {
  create
}
