import { ItemStock, Meal } from '../Redux/MealsRedux'
import { ImmutableArray } from 'seamless-immutable'
import { Moment } from 'moment'
import { Delivery } from '../Redux/V2/CartRedux'

const isAvailableStock = (itemStock :ImmutableArray<ItemStock>, item: Meal, mealDate: Moment, itemQty: number, cart: Delivery[] ) : boolean => {
  const stock = itemStock.find((stock) => stock.item_id === item.id )
  const dateInCart = cart.some((item) => item.date === mealDate.format('YYYY-MM-DD'))

  return stock ? (stock.quantity - itemQty) > 0 || dateInCart : false
}

const getItemStock = (itemId: number, itemStock:ImmutableArray<ItemStock>) => {
  const stock = itemStock.find(stock => itemId == stock.item_id)
  return stock ? stock.quantity || 0 : 0
}

export default{
  isAvailableStock,
  getItemStock
}
