import axios from 'axios'
import AppConfig from '../Config/AppConfig'

const checkGoogleMatrix = async (
	latitude: number,
	longitude: number,
	destination: string,
) => {
	const getGoogleMatrix = await axios.get(`https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${latitude}, ${longitude}&destinations=${destination}&key=${AppConfig.googleMapsKey}&region=id`)
	const distanceValue = []
	getGoogleMatrix && getGoogleMatrix.data.rows[0].elements.map(item => {
		distanceValue.push(item.distance.value)
	})
	const newDistanceValue = Math.min.apply(null, distanceValue)
	const index = distanceValue.indexOf(newDistanceValue)
	return index
}

export default {
	checkGoogleMatrix
}