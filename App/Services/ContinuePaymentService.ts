import apisauce, { ApiResponse } from 'apisauce'

import AppConfig from '../Config/AppConfig'
import { UrlLinkingInterface } from '../util/_linkingApp'

export interface ResponseContinuePaymentOkInterface
  extends UrlLinkingInterface {
  error: boolean;
  data: string;
  invoice_url: string;
}

export interface ResponseContinuePaymentErrorInterface {
  error: true;
  message: 'The selected order number is invalid.';
  status_code: 400;
}

function create(baseURL: string = AppConfig.apiBaseUrl) {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 2500
  })

  const continuePaymentAPI = ({
    order_number,
    token
  }: {
    order_number: string;
    token: string;
  }): Promise<ApiResponse<ResponseContinuePaymentOkInterface>> => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.post('public/payment/get_checkout_url', {
      order_number
    })
  }

  return {
    continuePaymentAPI,
    axiosInstence: api.axiosInstance
  }
}

export default {
  create
}
