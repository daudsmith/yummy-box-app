import { ImmutableArray } from 'seamless-immutable'
import { Tags } from '../Redux/MealsRedux'

export const getIncludeAndExcludeTags = (tags: Tags[] | ImmutableArray<Tags>) => {
  let includeTags = ''
  let excludeTags = ''
  let filter = ''

  for (let index in tags) {
    if (tags[index].hasOwnProperty('children')) {
      let children = tags[index].children
      for (let childIndex in children) {
        if (children[childIndex].isSelected) {
          if (children[childIndex].filter_type === 'include') {
            includeTags = includeTags + children[childIndex].id + ','
          } else {
            excludeTags = excludeTags + children[childIndex].id + ','
          }
          filter = filter + children[childIndex].id + ','
        }
      }
    } else {
      if (tags[index].isSelected) {
        filter = filter + tags[index].id + ','
      }
    }
  }
  includeTags = includeTags.substring(0, includeTags.length - 1)
  excludeTags = excludeTags.substring(0, excludeTags.length - 1)

  return {includeTags, excludeTags}
}

export const checkIfFilterActive = (tags: Tags[] | ImmutableArray<Tags>) => {
  let filterActive = false

  for (let tag of tags) {
    if (tag.hasOwnProperty('children')) {
      let childrens = tag.children
      for (let children of childrens) {
        if (children.isSelected) {
          filterActive = true
        }
      }
    } else {
      if (tag.isSelected) {
        filterActive = true
      }
    }
  }
  return filterActive
}
