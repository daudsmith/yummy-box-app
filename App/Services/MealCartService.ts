import _ from 'lodash'
import { Meal } from '../Redux/MealsRedux'
import { Moment } from 'moment'
import { Delivery } from '../Redux/V2/CartRedux'
import { itemResponse } from './V2/ApiCart'

const isItemExistInCart = (currentCart: Delivery[], item: Meal | itemResponse, date: Moment): boolean => {
  let existInCart = -1
  for (let cart of currentCart) {
    if (cart.date === date.format('YYYY-MM-DD')) {
      existInCart = _.findIndex(cart.items, (o) => {
        if (typeof o !== 'undefined') {
          const itemId = item.id
          const cartItemId = o.id
          return itemId === cartItemId
        }
      })
    }
  }
  return existInCart > -1
}

const getCartQuantity = (cartItems: Delivery[], type: string): number => {
  let quantity = 0
  for (let cartItem of cartItems) {
    if (type === 'item') {
      quantity += _.sumBy(cartItem.items, 'quantity')
    } else if (type === 'package') {
      quantity += _.sumBy(cartItem.packages, 'quantity')
    } else {
      quantity = 1
    }
  }
  return quantity
}

const getCartItem = (cartItems: Delivery[], meal: Meal | itemResponse, date: string): itemResponse | null => {
  const selectedCartItemDate = cartItems.find(cartItem => cartItem.date === date)
  if (selectedCartItemDate) {
    const itemIndex = selectedCartItemDate.items.find((item) => item.id === meal.id)
    return itemIndex
      ? itemIndex
      : null
  }
  return null
}

const getItemQuantity = (cartItems: Delivery[], itemId: number, date: Moment) => {
  return cartItems.reduce(function (current, next) {
    let item = next.items.find(item => (item.id == itemId && next.date == date.format('YYYY-MM-DD')))
    let stock = item ? item.quantity : 0
    return current + stock
  },0)
}
export default {
  isItemExistInCart,
  getCartQuantity,
  getCartItem,
  getItemQuantity
}
