import apisauce from 'apisauce'
import AppConfig from '../../Config/AppConfig'
import {
  CartState,
  DeliveryTotal,
} from '../../Redux/V2/CartRedux'
import { Total } from '../../Redux/CartRedux'

export interface apiResponse {
  error: boolean,
  data: responseData,
  message?: string,
}

export interface responseData {
  subscription: {
    theme_id: number,
    theme_name: string,
    start_date: string,
    repeat_every: Array<number>,
    repeat_interval: number,
    end_date: string,
    status: CART_ITEM_STATUS,
  },
  deliveries?: deliveryResponse[],
  cartItems?: deliveryResponse[],
  totals: Total,
  lunch: boolean,
  dinner: boolean,
  partner: string,
  type: string,
  exclusive: boolean,
  category: string,
  category_exclusive: boolean,
  promotion?: promotionData,
  valid: boolean,
}

export interface deliveryResponse {
  date: string,
  deliveryTime: string,
  deliveryAddress: [],
  totals: DeliveryTotal,
  items: itemResponse[]
}

export enum CART_ITEM_STATUS{
  'NEED_UPDATE' = 'NEED_UPDATE',
  'AVAILABLE' = 'AVAILABLE',
  'SOLD_OUT' = 'SOLD_OUT',
  'EXPIRED' = 'EXPIRED',
  'NOT_AVAILABLE' = 'NOT_AVAILABLE',
}
export interface itemResponse {
  id: number,
  quantity: number,
  name?: string,
  base_price?: number,
  sale_price?: number,
  partner?: string,
  cutoff_day?: number,
  cutoff_time?: string,
  exclusive?: boolean,
  remaining_quantity?: number,
  delivery_fee?: number,
  status?: CART_ITEM_STATUS,
  tax_class?: string,
  tags?: string[],
  subtotal?: number,
  images?: CatalogImage,
  category?: string,
}

export interface CatalogImage {
  original: string,
  small_thumb: string,
  medium_thumb: string,
}

interface promotionData {
  valid: boolean
  message: string
  coupon: string
}

const create = (baseURL: string = `${AppConfig.apiBaseUrl}/v2/public/cart`) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 15000
  })

  const validate = (cart?: CartState, token?: string) => {
    if (token) {
      api.setHeader('Authorization', `Bearer ${token}`)
    }
    return api.post<apiResponse>('/refresh', cart)
  }

  return {
    validate
  }
}

export default {
  create
}
