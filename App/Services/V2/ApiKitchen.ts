import apisauce from 'apisauce'
import AppConfig from '../../Config/AppConfig'

export interface dataKitchen {
  id: number,
  name: string,
  code: string,
  latitude: number,
  longitude: number,
  radians_distance: number
}
export interface kitchenApiResponse {
  error: boolean
  data: dataKitchen[]
  token: string
  message?: string
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
    },
    // 10 second timeout...
    timeout: 15000,
  })

  const kitchenList = (latitude: number, longitude: number) =>
    api.get<kitchenApiResponse>('v2/public/kitchen/list', { latitude, longitude })

  return {
    kitchenList,
  }
}

export default {
  create,
}
