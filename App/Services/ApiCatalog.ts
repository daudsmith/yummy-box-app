import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { Meals, Meal, Tags } from '../Redux/MealsRedux'
import { Item, Themes } from '../Redux/HomeContentRedux'

export interface GetCatalogCategoriesResponse {
  error: boolean
  status_code: number
  message?: string
  data: CategoryResponseData[]
}

export interface CategoryResponseData {
  id: number,
  name: string,
  exclusive: boolean,
  items: {
    data: Meal[]
  }
}

export interface GetPromoteResponse {
  error: boolean
  status_code: number
  message: string
  data: Meals
}

export interface GetMealsResponse {
  error: boolean
  status_code: number
  message: string
  data: Item[]
}

export interface GetMealResponse {
  error: boolean
  status_code: number
  message: string
  data: Meal
}

export interface GetMealAvailabilityData {
  item_id: number
  date: string
  remaining_quantity: number
}

export interface GetMealAvailabilityResponse {
  error: boolean
  status_code: number
  message: string
  data: GetMealAvailabilityData
}

export interface GetTagListResponse {
  error: boolean
  status_code: number
  message: string
  data: Tags[]
}

export interface GetThemeListResponse {
  error: boolean
  status_code: number
  message: string
  data: Themes[]
}


export interface GetOffDates {
  error: boolean
  data: string
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
    },
    // 30 second timeout...
    timeout: 30000,
  })

  const getCatalogCategories = (date: string, code: string) => api.get<GetCatalogCategoriesResponse>('public/catalog/category/list', {
    date: date,
    kitchen_code: code
  })

  const getPromoteCategories = (date: string, code: string) => api.get<GetPromoteResponse>('public/catalog/category/promoted', { date: date,kitchen_code: code })

  const getMeals = (date: string, category: number) => api.get<GetMealsResponse>('public/catalog/product/list', {
    category_id: category,
    date: date,
  })

  const getMealDetails = (id: number, date: string) => api.get<GetMealResponse>('public/catalog/product/detail', {
    product_id: id,
    date: date,
  })

  const getMealAvailability = (id: number, date: string, kitchenCode: string) => api.get<GetMealAvailabilityResponse>('public/catalog/product/availability', {
    product_id: id,
    date: date,
    kitchen_code: kitchenCode,
  })

  const getTagList = () => api.get<GetTagListResponse>('public/catalog/tag/list')

  const getFilteredMeals = (date: string, includeTags: boolean, excludeTags: boolean) => api.get<GetCatalogCategoriesResponse>('public/catalog/category/list', {
    date: date,
    include_tags: includeTags,
    exclude_tags: excludeTags,
  })

  const getOffDates = () => api.get<GetOffDates>('public/setting/get/off_dates')

  const getThemeList = (kitchenCode) => api.get<GetThemeListResponse>('public/catalog/theme/list', {
    kitchen_code: kitchenCode
  })

  const getCorporateThemeList = (token: string) => {
    if (token) {
      api.setHeader('Authorization', `Bearer ${token}`)
    }
    return api.get<GetThemeListResponse>('public/catalog/theme/corporate/list')
  }

  return {
    getCatalogCategories,
    getPromoteCategories,
    getMeals,
    getMealDetails,
    getTagList,
    getFilteredMeals,
    getOffDates,
    getThemeList,
    getCorporateThemeList,
    getMealAvailability,
  }
}

export default {
  create,
}
