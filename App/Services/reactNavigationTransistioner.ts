import {StackViewStyleInterpolator} from 'react-navigation-stack'
import {SceneInterpolatorProps} from 'react-navigation-stack/src/types'
import {
  Animated,
  Easing,
  Platform,
  EasingFunction,
} from 'react-native'
import { NavigationParams } from 'react-navigation'

const {
  forHorizontal,
  forVertical,
  forFadeFromBottomAndroid,
  forFade
} = StackViewStyleInterpolator

export interface TransitionSpecInterface {
  duration: number
  easing: EasingFunction
  timing: typeof Animated.timing
}

const TransitionSpec: TransitionSpecInterface = {
  duration: 500,
  easing: Easing.out(Easing.poly(4)),
  timing: Animated.timing
}

const TransitionConfig = (): any => {
  return {
    transitionSpec: TransitionSpec,
    screenInterpolator: (sceneProps: SceneInterpolatorProps): any  => {
      let transitionForMealDetail: string = ''
      if (sceneProps.scenes[0].route.routeName === 'NavigationDrawer') {
        sceneProps.scenes.forEach((data, index) => {
          if (data.route.routeName === 'MealDetailScreen') {
            transitionForMealDetail = sceneProps.scenes[index].route.params.transition
          }
        })
      }

      const params: NavigationParams | string = sceneProps.scene.route.params || transitionForMealDetail || {}
      const transition: string = (params as NavigationParams).transition || Platform.OS

      return {
        horizontal: forHorizontal(sceneProps),
        vertical: forVertical(sceneProps),
        modal: forVertical(sceneProps),
        fade: forFade(sceneProps),
        ios: forHorizontal(sceneProps),
        android: forFadeFromBottomAndroid(sceneProps),
        disable: (sceneProps) => {
          const { navigation, scene } = sceneProps

          const focused: boolean = navigation.state.index === scene.index
          const opacity: number = focused ? 1 : 0
          // If not focused, move the scene far away.
          const translate: number = focused ? 0 : 1000000
          return {
            opacity,
            transform: [{ translateX: translate }, { translateY: translate }]
          }
        }
      }[transition]
    }
  }
}

export default TransitionConfig
