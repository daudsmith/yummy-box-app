import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { CompanyAddress } from '../Redux/AddressTypeRedux'

export interface AddressListResponse {
  error: boolean
  status_code: number
  message: string
  data: CompanyAddress[]
}

const create = (baseURL: string = AppConfig.apiCaterUrl) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 15000
  })

  const getCompanyAddresses = (token: string) => {
    if (token) {
      api.setHeader('Authorization', `Bearer ${token}`)
    }
    return api.get<AddressListResponse>('/address/list')
  }

  return {
    getCompanyAddresses
  }
}

export default {
  create
}
