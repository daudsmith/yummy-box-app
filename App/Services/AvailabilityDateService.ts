import Moment from 'moment'
import _ from 'lodash'

const getInitialDates = (date: string | Moment.Moment, offDates: Array<string | Moment.Moment>, cutOffTimeSeconds: string): Moment.Moment => {
  const cutOffTime = Moment({hour: 0, minute: 0, second: 0}).utcOffset(7).add(1, 'days').subtract(parseInt(cutOffTimeSeconds) / 3600, 'hours')
  let initial = checkIfNowIsExceedingCutOffTime(cutOffTime) ? Moment(date).add(1, 'days') : Moment(date)
  var isDateValid = false
  while (!isDateValid) {
    const notOffDates = checkIfNotOffDates(initial, offDates)
    const notWeekend = checkIfNotWeekend(initial)
    if (notOffDates && notWeekend) {
      isDateValid = true
    } else {
      initial = initial.add(1, 'days')
    }
  }
  return initial
}

const getDeliveryDateList = (offDates: Array<string | Moment.Moment>, cutOffSeconds: string): Moment.Moment[] => {
  return getDeliveryDates({offDates, cutOffSeconds}).validDates
}

const getWeekendDateWithinDeliveryDateList = (offDates: Array<string | Moment.Moment>, cutOffSeconds: string): Moment.Moment[] => {
  return getDeliveryDates({offDates, cutOffSeconds}).weekendDates
}

const NUMBER_OF_VALID_DELIVERY_DATES = 14
const getDeliveryDates = ({offDates, cutOffSeconds}: {offDates: Array<string | Moment.Moment>, cutOffSeconds: string}) => {
  const cutOffTime: Moment.Moment = Moment({hour: 0, minute: 0, second: 0}).add(1, 'days').subtract(parseInt(cutOffSeconds,10) / 3600, 'hours')
  let dayCounter: number = checkIfNowIsExceedingCutOffTime(cutOffTime) ? 2 : 1
  let validDates: Moment.Moment[] = []
  let weekendDates: Moment.Moment[] = []

  while (validDates.length < NUMBER_OF_VALID_DELIVERY_DATES) {
    const day: Moment.Moment = Moment().utcOffset(7).add(dayCounter, 'days')
    const notDayOff: boolean = checkIfNotOffDates(day, offDates)
    if (notDayOff) {
      const notWeekend: boolean = checkIfNotWeekend(day)
      if (notWeekend) {
        validDates.push(day)
      } else {
        weekendDates.push(day)
      }
    }
    dayCounter++
  }

  return {
    validDates,
    weekendDates
  }
}

const disableWeekendAndOffDates = (availableDates: object[], offDates: Array<string | Moment.Moment>, weekendDates: object[] = []): object => {
  let daysToCheck = [...availableDates, ...offDates, ...weekendDates]
  let disabledDays = {}

  for (var i = 0; i < daysToCheck.length; i++) {
    const date = Moment(daysToCheck[i]).utcOffset(7)

    if (!checkIfNotWeekend(date) || !checkIfNotOffDates(date, offDates)) {
      const formattedDate = Moment(date).format('YYYY-MM-DD')
      disabledDays[formattedDate] = { disabled: true, disableTouchEvent: true }
    }
  }

  return disabledDays
}

const checkIfNotWeekend = (date: object): boolean => {
  const day = Moment(date).utcOffset(7).day()
  return day !== 0 && day !== 6
}

const checkIfNotOffDates = (date: Moment.Moment, offDates: Array<string | Moment.Moment>): boolean => {
  if (!offDates || offDates.length < 1) {
    return true
  }
  const isOffDates = _.findIndex(offDates, (offDate: object) => {
    if (Moment.isMoment(offDate)) {
      return date.isSame(offDate.format('YYYY-MM-DD'), 'day')
    } else {
      return date.isSame(Moment(offDate).format('YYYY-MM-DD'), 'day')
    }
  })

  return isOffDates === -1
}

const checkIfNowIsExceedingCutOffTime = (cutOffTimeString: string | Moment.Moment): boolean => {
  const cutOffTime = Moment(cutOffTimeString, 'hh:mm').utcOffset(7)
  const nowTime = Moment().utcOffset(7)

  return nowTime.isAfter(cutOffTime, 'minute')
}

const isNowExceedingDateCutOff = (cutOffTimeSeconds: string, date: string | Moment.Moment): boolean => {
  const now = Moment().utcOffset(7)
  const dateCutOff = Moment(date).set({hour: 0, minute: 0, second: 0}).subtract(cutOffTimeSeconds, 'second')

  return now.isAfter(dateCutOff, 'minute')
}

const listenForTimeChange = (callback: () => void, cutOffSeconds: string): void => {
  const nowTime = Moment().utcOffset(7)
  const cutOffTime = Moment({hour: 0, minute: 0, second: 0}).add(1, 'days').subtract(parseInt(cutOffSeconds,10) / 3600, 'hours')
  const startListenTime = Moment(cutOffTime).utcOffset(7).subtract(1, 'hours')
  const cutOff = Moment(cutOffTime, 'hh:mma').utcOffset(7)

  if (nowTime.isBetween(startListenTime, cutOff, 'minute')) {
    const interval = setInterval(() => {
      const isExceedingCutOff = checkIfNowIsExceedingCutOffTime(cutOffTime)
      if (isExceedingCutOff) {
        callback()
        clearInterval(interval)
      }
    }, 60000)
  }
}

const getDeliveryCutOffCountDown = (deliveryCutOff: string | number): number => {
  let cutOff = deliveryCutOff
  if (typeof cutOff === 'string') {
    cutOff = parseInt(cutOff,10)
  }
  const nowTime = Moment().utcOffset(7)
  const cutOffTime = Moment({hour: 0, minute: 0, second: 0}).add(1, 'days').subtract(cutOff / 3600, 'hours')
  const difference = cutOffTime.diff(nowTime)

  if (difference < 0) {
    return 0
  }
  return difference
}

export default {
  getDeliveryDateList,
  getInitialDates,
  checkIfNowIsExceedingCutOffTime,
  listenForTimeChange,
  checkIfNotWeekend,
  checkIfNotOffDates,
  getDeliveryCutOffCountDown,
  isNowExceedingDateCutOff,
  disableWeekendAndOffDates,
  getWeekendDateWithinDeliveryDateList
}
