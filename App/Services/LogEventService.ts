import analytics from '@react-native-firebase/analytics'
import { AppEventsLogger } from 'react-native-fbsdk'
import Config from 'react-native-config'

const logEvent = (name: string, params: object = null, log: string[]): boolean | void => {
  if (String(Config.APP_ENV).toLowerCase() === 'test') {
    return false
  }

  if(log.includes('default')){
    if (params === null) {
      analytics().logEvent(name)
      AppEventsLogger.logEvent(name)
    } else {
      analytics().logEvent(name, params)
      AppEventsLogger.logEvent(name, params)
    }
  } 

}

export default { logEvent }
