import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { OrderData } from '../Redux/SalesOrderRedux'

export interface createApiResponse {
  error: boolean
  data: string
  invoice_url: string
  checkout_url?: string
  message: string
}

export interface cancelApiResponse{
  error: boolean,
  data: [],
  message: string
}
const create = (baseURL: string = `${AppConfig.apiBaseUrl}/public/sales/order`) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 20000
  })

  const create = (order?: OrderData, token?: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.post<createApiResponse>('/create', order)
  }

  const cancel = (id: number, token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<cancelApiResponse>(`/cancel/${id}`)
  }

  return {
    create,
    cancel
  }
}

export default {
  create
}
