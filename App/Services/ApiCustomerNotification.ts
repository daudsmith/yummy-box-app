import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { Notification } from '../Redux/CustomerNotificationRedux'

export interface GetNotifications {
  error: boolean
  status_code: number
  message: string
  data: Notification[]
}

export interface GetTotalUnread {
  error: boolean
  status_code: number
  message: string
  data: number
}

export interface DeleteAllNotifications {
  error: boolean
  status_code: number
  message: string
  data: Array<string>
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 15000
  })

  const getUnread = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<GetNotifications>('public/account/notification/unread')
  }

  const getTotalUnread = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<GetTotalUnread>('public/account/notification/unread/total')
  }

  const getNotifications = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<GetNotifications>('public/account/notification/list')
  }

  const markNotificationRead = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<GetNotifications>('public/account/notification/mark_read')
  }

  const getReadByid = (token: string, notificationId: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<GetNotifications>(`public/account/notification/${notificationId}/read`)
  }

  const getUnreadByid = (token: string, notificationId: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<GetNotifications>(`public/account/notification/${notificationId}/unread`)
  }

  const deleteSingleNotification = (token: string, notificationId: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<GetNotifications>(`public/account/notification/${notificationId}/delete`)
  }

  const deleteAllNotifications = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<DeleteAllNotifications>('public/account/notification/delete/all')
  }

  return {
    getUnread,
    getTotalUnread,
    getNotifications,
    markNotificationRead,
    getReadByid,
    getUnreadByid,
    deleteSingleNotification,
    deleteAllNotifications
  }
}

export default {
  create
}
