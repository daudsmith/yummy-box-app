const parseToLabel = (status: string): 'Order Active' | 'Awaiting Payment' | 'Completed' | 'Active' | 'Paused' | 'Inactive' | 'Cancelled' | 'Refunded' => {
  switch (status) {
    case 'PROCESSING':
      return 'Order Active'
    case 'PENDING_PAYMENT':
      return 'Awaiting Payment'
    case 'COMPLETE':
      return 'Completed'
    case 'ACTIVE':
      return 'Active'
    case 'PAUSED':
      return 'Paused'
    case 'INACTIVE':
      return 'Inactive'
    case 'CANCELLED':
      return 'Cancelled'
    case 'CANCELLED_REFUNDED':
      return 'Refunded'
  }
}
const parseForNotification = (status: string): 'Active' | 'Awaiting Payment' | 'Completed' | 'Cancelled' | 'Refunded' => {
  switch (status) {
    case 'PROCESSING':
      return 'Active'
    case 'PENDING_PAYMENT':
      return 'Awaiting Payment'
    case 'COMPLETE':
      return 'Completed'
    case 'CANCELLED':
      return 'Cancelled'
    case 'REFUNDED':
      return 'Refunded'
  }
}
export default {
  parseToLabel,
  parseForNotification
}
