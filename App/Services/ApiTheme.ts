import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { ThemePackage, Item } from '../Redux/ThemeRedux'
import { ThemeDetail } from '../Redux/HomeContentRedux'

export interface GetThemeDetailResponse {
  error: boolean
  status_code: number
  message: string
  data: ThemeDetail
}

export interface GetThemeDetailMeals {
  error: boolean
  status_code: number
  message: string
  data: Item[]
}

export interface GetThemePackages {
  error: boolean
  status_code: number
  message: string
  data: ThemePackage[]
}

export interface GetThemeMealDetail {
  error: boolean
  status_code: number
  message: string
  data: Item
}

export interface GetThemeAvailableDate {
  error: boolean
  status_code: number
  message: string
  data: Array<string>
}

export interface GetMealByDates {
  error: boolean
  status_code: number
  message: string
  data: Item[]
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 20000
  })

  const getThemeDetail = (id: number, code: string) => {
    return api.get<GetThemeDetailResponse>(`public/catalog/theme/${id}/detail`,{
      kitchen_code: code
    })
  }

  const getThemeDetailMeals = (id: number) => {
    return api.get<GetThemeDetailMeals>(`public/catalog/theme/${id}/meal/list`)
  }

  const getThemePackages = (id: number) => {
    return api.get<GetThemePackages>(`public/catalog/theme/${id}/packages`)
  }

  const getAvailableDate = (themeId: number) => {
    return api.get<GetThemeAvailableDate>(`public/catalog/theme/${themeId}/available`)
  }

  const getAvailableDateExtends = (themeId: number, startDate: string) => {
    return api.get<GetThemeAvailableDate>(`public/catalog/theme/${themeId}/available?start_from=${startDate}`)
  }

  const getMealByDate = (themeId: number, date: string) => {
    return api.get<GetThemeMealDetail>(`public/catalog/theme/${themeId}/meal/by_date?date=${date}`)
  }

  const getMealByDates = (themeId: number, availableDates: Array<string>) => {
    let availableDatesString = ''
    for (let i = 0; i < availableDates.length; i++) {
      if (i === availableDates.length - 1) {
        availableDatesString = availableDatesString + 'dates[]=' +  availableDates[i]
      } else {
        availableDatesString = availableDatesString + 'dates[]=' + availableDates[i] + '&'
      }
    }
    return api.get<GetMealByDates>(`public/catalog/theme/${themeId}/meals_by_dates?${availableDatesString}`)
  }

  return {
    getThemeDetail,
    getThemeDetailMeals,
    getThemePackages,
    getAvailableDate,
    getMealByDate,
    getMealByDates,
    getAvailableDateExtends
  }
}

export default {create}
