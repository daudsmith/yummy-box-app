import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import {
  MyMealPlan,
  MyMealPlanDetail,
  PaymentInfo,
  PackageResponse,
  ActionResponse,
} from '../Redux/MyMealPlanRedux'

export interface mmpResponse {
  error: boolean
  data: MyMealPlan[]
  message?: string
}

export interface mmpDetailResponse {
  error: boolean
  data: MyMealPlanDetail
  message?: string
  payment_info?: PaymentInfo
  action?: ActionResponse
  package?: PackageResponse
  order?: {
    type: string
  }
}

export interface subscriptionModifyData {
  id: number
  created_at: string
  updated_at: string
  status: string
  type: string
  number: string
  date: string
  time: string
  name: string
  phone: string
  note: string
  landmark: string
  address: string
  latitude: number
  longitude: number
  can_cancel: boolean
  can_modify: boolean
  payment: string
}

export interface subscriptionModifyResponse {
  error: boolean;
  data: subscriptionModifyData;
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 20000
  })

  const getMyMealPlans = (token: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<mmpResponse>('public/sales/order/delivery/list')
  }
  const getMyMealPlanDetail = (token: string, id: number, type: string) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<mmpDetailResponse>(`public/sales/order/delivery/detail/${id}?type=${type}`)
  }
  const cancelOrder = (token: string, id: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<mmpDetailResponse>(`public/sales/order/delivery/cancel/${id}`)
  }

  const updateDelivery = (token: string, delivery_id: number, data: object) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.post<mmpDetailResponse>(`public/sales/order/delivery/update/${delivery_id}`, {...data})
  }
  const skipDelivery = (token: string, id: number) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.get<subscriptionModifyResponse>(`public/sales/subscription/schedule/${id}/skip`)
  }
  const updateSubscriptionDelivery = (token: string, delivery_id: number, data: object) => {
    api.setHeader('Authorization', `Bearer ${token}`)
    return api.post<subscriptionModifyResponse>(`public/sales/subscription/schedule/${delivery_id}/update`, {...data})
  }

  return {
    getMyMealPlans,
    getMyMealPlanDetail,
    cancelOrder,
    updateDelivery,
    skipDelivery,
    updateSubscriptionDelivery
  }
}

export default {
  create
}
