import R from 'ramda'
import Immutable from 'seamless-immutable'
import { Store } from 'redux'
import { AppState } from '../Redux/CreateStore'

// is this object already Immutable?
const isImmutable: boolean = R.has('asMutable')

// change this Immutable object into a JS object
const convertToJs = (state: any): any => state.asMutable({ deep: true })

// optionally convert this object into a JS object if it is Immutable
const fromImmutable: any = R.when(isImmutable, convertToJs)

// convert this JS object into an Immutable object
const toImmutable = (raw: Store<AppState>): any => Immutable(raw)

// the transform interface that redux-persist is expecting
export default {
  out: (state: Store<AppState>) => {
    return toImmutable(state)
  },
  in: (raw: any) => {
    return fromImmutable(raw)
  }
}
