import moment from 'moment'

const populateAvailableDateForCalendar = (
    availableDates: Array<string>, 
    extendedDates: Array<string>, 
    selectedDate: string, 
    period: number, 
    disabledDate: string
  ) => {
  let calendarDates = []
  const selectedDateIndex = availableDates.findIndex(date => date === selectedDate)
  if ((selectedDateIndex + period) > availableDates.length) {
    calendarDates = extendedDates
  } else {
    calendarDates = availableDates
  }

  if (disabledDate !== null) {
    return calendarDates.filter(calendarDate => moment(calendarDate).isAfter(moment(disabledDate)))
  }

  return calendarDates
}

export default {
  populateAvailableDateForCalendar,
}
