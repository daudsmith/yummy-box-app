import { xenditCreditTokenResponse, paymentInfoData } from './XenditService'
import { User } from '../Redux/LoginRedux'


export const buildNewCCPaymentInfo = (creditCardToken: xenditCreditTokenResponse, authData: xenditCreditTokenResponse, paymentInfo: paymentInfoData) => {
  const ends_with = paymentInfo.hasOwnProperty('card_number')
    ? paymentInfo.card_number.substring(12, 16)
    : paymentInfo.ends_with
  if (ends_with === undefined) {
    return null
  }

  const authorization_id = paymentInfo.is_multiple_use
    ? authData.id
    : creditCardToken.authentication_id
  const is_multiple_use = paymentInfo.is_multiple_use
    ? paymentInfo.is_multiple_use
    : false

  return {
    type: paymentInfo.type,
    ends_with,
    status: authData.status,
    token: creditCardToken.id,
    is_multiple_use,
    authorization_id,
  }
}

export const buildNewCardData = (user: User, creditCardToken: xenditCreditTokenResponse, authData: xenditCreditTokenResponse, paymentInfo: paymentInfoData) => {
  const ends_with = paymentInfo.hasOwnProperty('card_number')
    ? paymentInfo.card_number.substring(12, 16)
    : paymentInfo.ends_with
  if (ends_with === undefined) {
    return null
  }

  return {
    account_id: user.id,
    token: creditCardToken.id,
    authorization_id: authData.id,
    type: paymentInfo.type,
    ends_with,
  }
}
