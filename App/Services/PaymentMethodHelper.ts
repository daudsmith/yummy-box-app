
const getPaymentIconName = (payment: string): 'payment-cash' | 'payment-cc' | 'transfer-icon' | 'wallet' => {
  switch (payment) {
    case 'cod':
      return 'payment-cash'
    case 'credit-card':
      return 'payment-cc'
    case 'virtual-account':
      return 'transfer-icon'
    default:
      return 'wallet'
  }
}

const getCreditCardIconName = (type: string): 'mastercard' | 'visa' | 'wallet' => {
  if (type === 'MASTER') {
    return 'mastercard'
  } else if (type === 'VISA') {
    return 'visa'
  } else {
    return 'wallet'
  }
}
const getCreditCardBrand = (cardBrand: string): string => {
  if (cardBrand === 'MASTERCARD') {
    return 'MASTER'
  }
  return cardBrand
}
export default {
  getPaymentIconName,
  getCreditCardIconName,
  getCreditCardBrand
}