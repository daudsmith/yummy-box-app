import Xendit, { transactionDataInterface } from '../Lib/Xendit/Xendit'
import AppConfig from '../Config/AppConfig'

const XENDIT_API_KEY = AppConfig.xenditKey

export interface xenditTokenData {
  card_number: string
  card_exp_month: string | number
  card_exp_year: string | number
  card_cvn: string | number
  is_multiple_use: boolean
  should_authenticate: boolean
  amount: string | number
}

export interface paymentInfoData {
  card_number: string
  card_exp_month: string | number
  card_exp_year: string | number
  card_cvn: string | number
  is_multiple_use: boolean
  should_authenticate: boolean
  ends_with: string
  type: string
  token: string
  saveCreditCard: boolean
}

export interface xenditCreditTokenResponse {
  id: string
  authentication_id: string
  masked_card_number: string
  status: string
  payer_authentication_url: string
}

const createToken = (
  paymentInfo: paymentInfoData, verification3DSCallback: Function, errorCallback: Function, saveToken: Function,
  amount: number = 10000,
): void => {
  const tokenData: transactionDataInterface = {
    ...paymentInfo,
    amount,
  }
  const isMultipleUseToken = paymentInfo.is_multiple_use
  Xendit.setPublishableKey(XENDIT_API_KEY)
  Xendit.card.createToken(
    tokenData,
    (err: object, creditCardToken: xenditCreditTokenResponse) =>
      xenditResponseHandler(err, creditCardToken, verification3DSCallback, errorCallback, saveToken, amount,
        isMultipleUseToken),
  )
}

const xenditResponseHandler = (
  err: object,
  creditCardToken: xenditCreditTokenResponse,
  verification3DSCallback: Function,
  errorCallback: Function,
  saveToken: Function,
  amount: number,
  isMultipleUseToken: boolean,
): void => {
  if (err) {
    return errorCallback(err)
  }

  if (creditCardToken.status === 'APPROVED' || creditCardToken.status === 'VERIFIED') {
    if (isMultipleUseToken) {
      saveToken(creditCardToken)
    }
    Xendit.card.createAuthentication({
      amount,
      token_id: creditCardToken.id,
    }, (err, creditCardToken) => xenditResponseHandler(err, creditCardToken, verification3DSCallback, errorCallback,
      saveToken, amount, isMultipleUseToken))
  } else if (creditCardToken.status === 'IN_REVIEW') {
    if (!isMultipleUseToken) {
      saveToken(creditCardToken)
    }
    verification3DSCallback(creditCardToken.payer_authentication_url)
  } else if (creditCardToken.status === 'FRAUD') {
    errorCallback(err)
  } else if (creditCardToken.status === 'FAILED') {
    errorCallback(err)
  }
}

export default { createToken }
