import SubscriptionService from '../SubscriptionService'
import fakeState from '../../Sagas/__tests__/fakeState'

const {
  checkAllRequiredInformationValid,
} = SubscriptionService

const fakePaymentInfo = {
  info: {
    current_balance: 1405552,
  },
}

describe('SalesOrderService', () => {
  test('checkAllRequiredInformationValid subscription should return correct response', () => {
    const response = checkAllRequiredInformationValid(fakeState.cart.cartItems, { ...fakePaymentInfo }, {
      card_number: '1234567890111213',
      type: 'VISA',
      end_withs: '1213',
    })
    expect(response)
      .toEqual(true)
  })

  test('checkAllRequiredInformationValid subscription should return false response', () => {
    const response = checkAllRequiredInformationValid(fakeState.cart.cartItems, {}, {})
    expect(response)
      .toEqual(false)
  })
})
