import SalesOrderService from '../SalesOrderService'

const {
  buildSalesOrder,
  checkAllRequiredInformationValid,
  calculateItemInCart,
  setOrderTotal,
} = SalesOrderService

const fakeCart = {
  type: 'item',
  coupon: '',
  cartItems: [
    {
      date: '2019-09-09',
      deliveryTime: '09:00 - 12:00',
      items: [
        {
          id: 78,
          name: 'Pan Fried Chicken in Mushroom Sauce',
          sale_price: 30000,
          tax_class: 'none',
          images: {
            original: 'thisIsImageString1',
            small_thumb: 'thisIsImageString2',
            medium_thumb: 'thisIsImageString3',
          },
          quantity: 1,
        },
      ],
      deliveryAddress: {
        name: 'Tony Stark',
        phone: '+628174161352',
        note: '',
          landmark: 'Hotel Indonesia',
          address: 'Jl. M.H. Thamrin No.1, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310, Indonesia',
          latitude: 1,
          longitude: 1,
      },
      totals: {
        subtotal: 30000,
        delivery_fee: 0,
      },
    },
  ],
}
const fakeSelectedMethod = 'wallet'
const fakePaymentInfo = {
  info: {
    current_balance: 1405552,
  },
}
const totalAmount = {
  amount: 60000,
  tax: 0,
  deliveryFee: 0,
  discount: 12000,
  total: 48000,
}
const expectation = {
  deliveries: [
    {
      date: '2019-09-09',
      time: '09:00 - 12:00',
      name: 'Tony Stark',
      phone: '+628174161352',
      note: '',
      landmark: 'Hotel Indonesia',
      address: 'Jl. M.H. Thamrin No.1, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10310, Indonesia',
      latitude: 1,
      longitude: 1,
      items: [
        {
          id: 78,
          name: 'Pan Fried Chicken in Mushroom Sauce',
          sale_price: 30000,
          tax_class: 'none',
          images: {
            original: 'thisIsImageString1',
            small_thumb: 'thisIsImageString2',
            medium_thumb: 'thisIsImageString3',
          },
          quantity: 1,
        },
      ],
      totals: {
        subtotal: 30000,
        delivery_fee: 0,
      },
    },
  ],
  payment: {
    method: 'wallet',
    info: {
      info: {
        current_balance: 1405552,
      },
    },
    is_multiple_use: true,
  },
  coupon: '',
  type: 'item',
  total: {
    amount: 60000,
    tax: 0,
    deliveryFee: 0,
    discount: 12000,
    total: 48000,
  },
}

describe('SalesOrderService', () => {
  test('buildSalesOrder should return correct response', () => {
    const response = buildSalesOrder(fakeCart, fakeSelectedMethod, { ...fakePaymentInfo }, true, totalAmount)
    expect(response)
      .toEqual(expectation)
  })

  test('checkAllRequiredInformationValid should return correct response', () => {
    const response = checkAllRequiredInformationValid(fakeSelectedMethod, { ...fakePaymentInfo }, {
      card_number: '1234567890111213',
      type: 'VISA',
      end_withs: '1213',
    })
    expect(response)
      .toEqual(true)
  })

  test('checkAllRequiredInformationValid should return false response', () => {
    const response = checkAllRequiredInformationValid(fakeSelectedMethod, null, null)
    expect(response)
      .toEqual(false)
  })

  test('calculateItemInCart should return response 1', () => {
    const response = calculateItemInCart(fakeCart)
    expect(response)
      .toEqual(1)
  })

  test('setOrderTotal should return response amount 30000', () => {
    const total = setOrderTotal(fakeCart.cartItems[0].totals)
    expect(total.amount)
      .toBe(30000)
  })
})
