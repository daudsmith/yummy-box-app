import { checkIfFilterActive, getIncludeAndExcludeTags } from '../MealService'

describe('getIncludeAndExcludeTags', () => {
    let tags = [{
        id:51,
        filter_type:'exclude',
        children:[{
                id:52,
                filter_type:'exclude',
                isSelected:true,
            },
            {
                id:53,
                filter_type:'exclude',
                isSelected:true,
            }],
    },
    {
        id:49,
        filter_type:'include',
        children:[
            {
                id:1,
                filter_type:'include',
                isSelected:true
            },
            {
                id:45,
                filter_type:'include',
                isSelected:false,
            }],
    }]
    
    test('Should return {excludeTags: 52,53, includeTags: 1} if tags filter type is exclude and selected true', () => {
        expect(
            getIncludeAndExcludeTags(tags)
        ).toStrictEqual({excludeTags: '52,53', includeTags: '1'})
    })
})

describe('checkIfFilterActive', () => {
    let filteredTags = [{
        id:51,
        children:[{
            id:52,
            isSelected:true,
        }],
    },
    {
        id:49,
        children:[{
            id:1,
            isSelected:false
        }],
    }]

    test('Should return true if one of selected tags children is true', () => {
        expect(
            checkIfFilterActive(filteredTags)
        ).toBe(true)
    })


    let unFilteredTags = [{
        id:51,
        children:[{
            id:52,
            isSelected:false,
        }],
    },
    {
        id:49,
        children:[{
            id:1,
            isSelected:false
        }],
    }]

    test('Should return false if one of selected tags children is false', () => {
        expect(
            checkIfFilterActive(unFilteredTags)
        ).toBe(false)
    })

    let tagsSelectedTrue = [{
        id:51,
        isSelected:true,
    }]

    test('Should return true if tags have no children and selected tags is true', () => {
        expect(
            checkIfFilterActive(tagsSelectedTrue)
        ).toBe(tagsSelectedTrue[0].isSelected)
    })
})