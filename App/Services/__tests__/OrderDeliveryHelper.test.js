import OrderDeliveryHelper from '../OrderDeliveryHelper'

describe('parseStatusToLabel', () => {

  test('Should return delivery status label On The Way if delivery status is ON_THE_WAY', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('ON_THE_WAY')
    ).toBe('On The Way')
  })

  test('Should return delivery status label Delivered if delivery status is DELIVERED', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('DELIVERED')
    ).toBe('Delivered')
  })

  test('Should return delivery status label Complete if delivery status is COMPLETE', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('COMPLETE')
    ).toBe('Complete')
  })

  test('Should return delivery status label Request For Cancel if delivery status is REQUEST_CANCEL', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('REQUEST_CANCEL')
    ).toBe('Request For Cancel')
  })

  test('Should return delivery status label Cancelled if delivery status is CANCELLED', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('CANCELLED')
    ).toBe('Cancelled')
  })

  test('Should return delivery status label Failed if delivery status is FAILED', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('FAILED')
    ).toBe('Failed')
  })

  test('Should return delivery status label Cooking if delivery status is COOKING', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('COOKING')
    ).toBe('Cooking')
  })

  test('Should return delivery status label Skipped if delivery status is SKIP', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('SKIP')
    ).toBe('Skipped')
  })

  test('Should return delivery status label Scheduled if delivery status is Scheduled', () => {
    expect(
      OrderDeliveryHelper.parseStatusToLabel('Scheduled')
    ).toBe('Scheduled')
  })

})

describe('deliveryFeeTotal', () => {
  const deliveries = [
    { delivery_fee: 2000 },
    { delivery_fee: 3000 },
  ]

  test('Should return delivery fee summary', () => {
    expect(
      OrderDeliveryHelper.deliveryFeeTotal(deliveries)
    ).toBe(5000)
  })

})

describe('parseStatusToMessage', () => {

  test('Should return delivery message is if delivery status is on the way', () => {
    expect(
      OrderDeliveryHelper.parseStatusToMessage('on the way')
    ).toBe('is ')
  })

  test('Should return delivery message has been if delivery status is cancelled', () => {
    expect(
      OrderDeliveryHelper.parseStatusToMessage('cancelled')
    ).toBe('has been ')
  })

  test('Should return delivery message is if delivery status is is', () => {
    expect(
      OrderDeliveryHelper.parseStatusToMessage('is')
    ).toBe('is ')
  })

})
