import nock from 'nock'
import Xendit from '../../Lib/Xendit/Xendit'
import XenditService from '../../Services/XenditService'

describe('createToken', () => {
  test('createToken should return generated token', async () => {
    let response
    const fakeData = {
      paymentInfo: {
        is_multiple_use: true,
      },
      verification3DSCallback: () => { },
      errorCallback: () => { },
      saveToken: token => { response = token },
      amount: 1000
    }
    const { createToken } = XenditService
    nock(Xendit._getXenditURL()).post('/v2/credit_card_tokens').reply(200, {
      result: true
    })
    try {
      await createToken(fakeData.paymentInfo, fakeData.verification3DSCallback, fakeData.errorCallback, fakeData.saveToken, fakeData.amount)
      expect(response).toEqual({
        result: true
      })
    }
    catch (error) {

    }

  })

  test('createToken: method not found', async () => {
    let response
    const fakeData = {
      paymentInfo: {
        is_multiple_use: true,
      },
      verification3DSCallback: () => { },
      errorCallback: () => { },
      saveToken: token => { response = token },
      amount: 1000
    }
    const { createToken } = XenditService
    nock(Xendit._getXenditURL()).post('/v2/credit_card_tokens').reply(404, {
      error: true
    })
    try {
      await createToken(fakeData.paymentInfo, fakeData.verification3DSCallback, fakeData.errorCallback, fakeData.saveToken, fakeData.amount)
      expect(response).toEqual({
        error: true
      })
    } catch (error) {

    }


  })

  test('createToken: internal server error', async () => {
    let response
    const fakeData = {
      paymentInfo: {
        is_multiple_use: true,
      },
      verification3DSCallback: () => { },
      errorCallback: () => { },
      saveToken: token => { response = token },
      amount: 1000
    }
    const { createToken } = XenditService
    nock(Xendit._getXenditURL()).post('/v2/credit_card_tokens').reply(500, {
      error: true
    })

    try {
      await createToken(fakeData.paymentInfo, fakeData.verification3DSCallback, fakeData.errorCallback, fakeData.saveToken, fakeData.amount)

      expect(response).toEqual({
        error: true
      })
    }
    catch (error) {

    }

  })

  test('createToken should only received payment.is_multiple_use in boolean format', async () => {
    let response
    const fakeData = {
      paymentInfo: {
        is_multiple_use: true,
      },
      verification3DSCallback: () => { },
      errorCallback: () => { },
      saveToken: token => { response = token },
      amount: 1000
    }
    const { createToken } = XenditService
    nock(Xendit._getXenditURL()).post('/v2/credit_card_tokens').reply(404, {
      result: true
    })
    try {
      await createToken(fakeData.paymentInfo, fakeData.verification3DSCallback, fakeData.errorCallback, fakeData.saveToken, fakeData.amount)
      if (typeof fakeData.paymentInfo.is_multiple_use === 'boolean') {
        expect(response).toEqual({
          result: true
        })
      }
    }
    catch (error) {

    }

  })

})