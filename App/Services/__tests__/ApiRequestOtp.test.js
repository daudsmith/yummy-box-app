
import ApiRequestOtp from '../ApiRequestOtp'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

describe('ApiRequestOtp requestOtp', () => {
  test('requestOtp should return correct response', async () => {
    const fakePhone = '08381111222'
    const { requestOtp } = ApiRequestOtp.create('http://localhost')
    nock(AppConfig.apiBaseUrl).post(`/start?via=sms&country_code=+62&phone_number=${fakePhone}&locale=en`).reply(200, {
      result: {
        phone: fakePhone
      }
    })

    const response = await requestOtp(fakePhone)
    expect(response.data).toEqual({
      result: {
        phone: fakePhone
      }
    })
  })

  test('requestOtp not found', async () => {
    const fakePhone = '08381111222'
    const { requestOtp } = ApiRequestOtp.create('http://localhost')
    nock(AppConfig.apiBaseUrl).post(`/start?via=sms&country_code=+62&phone_number=${fakePhone}&locale=en`).reply(404, {
      result: {
        phone: fakePhone
      }
    })

    const response = await requestOtp(fakePhone)
    expect(response.data).toEqual({
      result: {
        phone: fakePhone
      }
    })
  })


  test('requestOtp internal server error', async () => {
    const fakePhone = '08381111222'
    const { requestOtp } = ApiRequestOtp.create('http://localhost')
    nock(AppConfig.apiBaseUrl).post(`/start?via=sms&country_code=+62&phone_number=${fakePhone}&locale=en`).reply(500, {
      result: {
        phone: fakePhone
      }
    })

    const response = await requestOtp(fakePhone)
    expect(response.data).toEqual({
      result: {
        phone: fakePhone
      }
    })
  })


})
