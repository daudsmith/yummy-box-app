import ApiCart from '../ApiCart'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

describe('create', () => {

    test('validate should return correct response', async () => {
        const { validate } = ApiCart.create()
        nock(AppConfig.apiBaseUrl).post('/public/cart/refresh').reply(200, {
            result: true
        })
        const response = await validate()
        expect(response.data).toEqual({
            result: true
        })
    })

    test('validate should return error if not found', async () => {
        const { validate } = ApiCart.create()
        nock(AppConfig.apiBaseUrl).post('/public/cart/refresh').reply(404, {
            result: true
        })
        const response = await validate()
        expect(response.data).toEqual({
            result: true
        })
    })

    test('validate should return error on server error', async () => {
        const { validate } = ApiCart.create()
        nock(AppConfig.apiBaseUrl).post('/public/cart/refresh').reply(500, {
            result: true
        })
        const response = await validate()
        expect(response.data).toEqual({
            result: true
        })
    })

})
