import NavigationService from '../NavigationService'

describe('NavigationService routingHelper', () => {

  test('should return ThemeDetailScreen if status is playlist', async () => {
    const screen = 'playlist'
    const response = 'ThemeDetailScreen'
    expect(NavigationService.routingHelper(screen)).toBe(response)
  })

  test('should return MealListScreen if status is meal', async () => {
    const status = 'meal'
    const response = 'MealListScreen'
    expect(NavigationService.routingHelper(status)).toBe(response)
  })

  test('should return HomeScreen if status is not playlist or meal ', async () => {
    const status = 'test'
    const response = 'HomeScreen'
    expect(NavigationService.routingHelper(status)).toBe(response)
  })

})

