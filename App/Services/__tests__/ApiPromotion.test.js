
import ApiPromotion from '../ApiPromotion'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

describe('ApiPromotion', () => {
  test('getPromotionBanners should return correct response', async () => {
    const { getPromotionBanners } = ApiPromotion.create()
    nock(AppConfig.apiBaseUrl).get('/public/promotion/banners').reply(200, {
      result: true
    })
    const response = await getPromotionBanners()
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getPromotionBanners not found', async () => {
    const { getPromotionBanners } = ApiPromotion.create()
    nock(AppConfig.apiBaseUrl).get('/public/promotion/banners').reply(404, {
      result: true
    })
    const response = await getPromotionBanners()
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getPromotionBanners internal server error', async () => {
    const { getPromotionBanners } = ApiPromotion.create()
    nock(AppConfig.apiBaseUrl).get('/public/promotion/banners').reply(500, {
      result: true
    })
    const response = await getPromotionBanners()
    expect(response.data).toEqual({
      result: true
    })
  })


})
