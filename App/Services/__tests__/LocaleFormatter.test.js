import LocaleFormatter from '../LocaleFormatter'
import AppConfig from '../../Config/AppConfig'

describe('numberToCurrency', () => {

    test('Should return Rp. 200.000 if number is 200000', () => {
        expect(
            LocaleFormatter.numberToCurrency(200000)
        ).toBe(AppConfig.defaultCurrencySymbol+'200.000')
    })

})

describe('convertSecondsToMinutes', () => {

    test('Should return 1m 31s if second is 91', () => {
        expect(
            LocaleFormatter.convertSecondsToMinutes(91)
        ).toBe('1m 31s')
    })

})

describe('formatBankAccountNumber', () => {

    test('Should return 1234-5678-1234-5678 if number is 1234567812345678', () => {
        expect(
            LocaleFormatter.formatBankAccountNumber('1234567812345678')
        ).toBe('1234-5678-1234-5678')
    })

})

describe('pluralTranslation', () => {

    test('Should return 2 Days if number is > 1', () => {
        expect(
            LocaleFormatter.pluralTranslation(2, 'Day', 'Days')
        ).toBe('2 Days')
    })

    test('Should return 1 Day if number is 1', () => {
        expect(
            LocaleFormatter.pluralTranslation(1, 'Day', 'Days')
        ).toBe('1 Day')
    })

})

describe('addSpacingToPhoneNumber', () => {

    test('Should return +62 123 1234 1234 if phone number is 012312341234', () => {
        expect(
            LocaleFormatter.addSpacingToPhoneNumber('+6212312341234')
        ).toBe('+62 123 1234 1234')
    })
})

describe('createTagString', () => {
    const tags = {
      data: [
        {
          id: 1,
          name: 'Tag 1',
          icon: '',
          filterable: 0,
          display_on_catalog: 1,
          filter_type: '',
          description: '',
        },
        {
          id: 2,
          name: 'Tag 2',
          icon: '',
          filterable: 0,
          display_on_catalog: 1,
          filter_type: '',
          description: '',
        },
        {
          id: 3,
          name: 'Tag 3',
          icon: '',
          filterable: 0,
          display_on_catalog: 1,
          filter_type: '',
          description: '',
        },
        {
          id: 4,
          name: 'Tag 4',
          icon: '',
          filterable: 0,
          display_on_catalog: 1,
          filter_type: '',
          description: '',
        },
        {
          id: 5,
          name: 'Tag 5',
          icon: '',
          filterable: 0,
          display_on_catalog: 1,
          filter_type: '',
          description: '',
        },
      ],
    }

    test('Should return Gluten Indonesian Nuts if tags string use limit 3', () => {
        expect(
            LocaleFormatter.createTagString(tags, 3)
        ).toBe('Tag 1 \u2022 Tag 2 \u2022 Tag 3')
    })

    test('Should return Gluten Indonesian Nuts Soy if tags string no limit', () => {
        expect(
            LocaleFormatter.createTagString(tags)
        ).toBe('Tag 1 \u2022 Tag 2 \u2022 Tag 3 \u2022 Tag 4 \u2022 Tag 5')
    })
})


describe('isEmailFormatValid', () => {

    test('Should return true if email is test@email.test', () => {
        expect(
            LocaleFormatter.isEmailFormatValid('test@email.test')
        ).toBe(true)
    })

    test('Should return false if email is test::##__test', () => {
        expect(
            LocaleFormatter.isEmailFormatValid('test::##__test')
        ).toBe(false)
    })
})

describe('isPhoneFormatValid', () => {

    test('Should return true if phone is 088899991010', () => {
        expect(
            LocaleFormatter.isPhoneFormatValid('088899991010')
        ).toBe(true)
    })

    test('Should return false if phone is +62abCD12345000', () => {
        expect(
            LocaleFormatter.isPhoneFormatValid('+62abCD12345000')
        ).toBe(false)
    })
})

describe('deepLinkUrlParser', () => {
    let url = 'https://www.yummybox.id?screen=meal&date=2018-08-31'
    test('Should return https://www.yummybox.id?screen=meal&date=2018-08-31 if url is'+url, () => {
        expect(
            LocaleFormatter.deepLinkUrlParser(url)
        ).toBe('https://www.yummybox.id?screen=meal&date=2018-08-31')
    })
})
