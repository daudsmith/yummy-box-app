import ApiCustomer from '../ApiCustomer'
import AppConfig from '../../Config/AppConfig'
import nock from 'nock'

describe('create', () => {

    test('customerProfile should return correct response', async () => {
        const { customerProfile } = ApiCustomer.create()
        nock(AppConfig.apiBaseUrl).get('/public/account/me').reply(200, {
            result: true
        })
        const response = await customerProfile('12345')
        expect(response.data).toEqual({
            result: true
        })
    })

    test('auth should return correct response', async () => {
        const { auth } = ApiCustomer.create()
        const fakeCredential = {
            email: 'email@yummycorp.com',
            password: '123123'
        }
        nock(AppConfig.apiBaseUrl).post('/public/account/login', fakeCredential).reply(200, {
            result: true
        })
        const response = await auth(fakeCredential.email, fakeCredential.password)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('register should return correct response', async () => {
        const { register } = ApiCustomer.create()
        const fakeCustomer = {
            name: 'John',
            email: 'jojohn@yummycorp.com'
        }
        nock(AppConfig.apiBaseUrl).post('/public/account/register', fakeCustomer).reply(200, {
            result: true
        })
        const response = await register(fakeCustomer)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('logout should return correct response', async () => {
        const { logout } = ApiCustomer.create()
        const fakeToken = '21wqe123'

        nock(AppConfig.apiBaseUrl).post('/public/account/logout', {uid: '1'}).reply(200, {
            result: true
        })
        const response = await logout(fakeToken, '1')
        expect(response.data).toEqual({
            result: true
        })
    })

    test('validate should return correct response', async () => {
        const { validate } = ApiCustomer.create()
        const fakeCustomer = 'Josh'
        nock(AppConfig.apiBaseUrl).post('/public/account/check', fakeCustomer).reply(200, {
            result: true
        })
        const response = await validate(fakeCustomer)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('getSavedCards should return correct response', async () => {
        const { getSavedCards } = ApiCustomer.create()
        nock(AppConfig.apiBaseUrl).get('/public/account/card').reply(200, {
            result: true
        })
        const response = await getSavedCards()
        expect(response.data).toEqual({
            result: true
        })
    })

    test('saveCard should return correct response', async () => {
        const { saveCard } = ApiCustomer.create()
        const fakeCard = {
            token: '1',
            authorization_id: '2',
            type: 'a',
            ends_with: 'x'
        }
        const fakeData = {
            token: fakeCard.token,
            authorization_id: fakeCard.authorization_id,
            type: fakeCard.type,
            ends_with: fakeCard.ends_with,
          }
        nock(AppConfig.apiBaseUrl).post('/public/account/card/create', fakeData).reply(200, {
            result: true
        })
        const response = await saveCard(fakeCard)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('deleteCard should return correct response', async () => {
        const { deleteCard } = ApiCustomer.create()
        const fakeCardId = '1'
        nock(AppConfig.apiBaseUrl).delete(`/public/account/card/${fakeCardId}/delete`).reply(200, {
            result: true
        })
        const response = await deleteCard(fakeCardId)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('setDefaultCard should return correct response', async () => {
        const { setDefaultCard } = ApiCustomer.create()
        const fakeCardId = '1'
        nock(AppConfig.apiBaseUrl).put(`/public/account/card/${fakeCardId}/default`).reply(200, {
            result: true
        })
        const response = await setDefaultCard(fakeCardId)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('getSavedAddresses should return correct response', async () => {
        const { getSavedAddresses } = ApiCustomer.create()
        const fakeToken = 'qwe123'
        nock(AppConfig.apiBaseUrl).get('/public/account/address').reply(200, {
            result: true
        })
        const response = await getSavedAddresses(fakeToken)
        expect(response).toEqual({
            result: true
        })
    })

    test('getAddressHistory should return correct response', async () => {
        const { getAddressHistory } = ApiCustomer.create()
        nock(AppConfig.apiBaseUrl).get('/public/account/address/history').reply(200, {
            result: true
        })
        const response = await getAddressHistory()
        expect(response.data).toEqual({
            result: true
        })
    })

    test('saveAddress should return correct response', async () => {
        const { saveAddress } = ApiCustomer.create()
        const fakeAddress = {
            latitude: '1',
            longitude: '2',
            addressLabel: 'abc',
            addressPinpoint: '121',
            addressDetail: 'abcde',
            recipientName: 'John',
            recipientNumber: '1212',
            deliveryInstructions: 'abcde ipsum',
        }
        const fakeData = {
            landmark: fakeAddress.addressPinpoint,
            address: fakeAddress.addressDetail,
            latitude: fakeAddress.latitude,
            longitude: fakeAddress.longitude,
            name: fakeAddress.recipientName,
            phone: fakeAddress.recipientNumber,
            label: fakeAddress.addressLabel,
            note: fakeAddress.deliveryInstructions,
        }
        nock(AppConfig.apiBaseUrl).post('/public/account/address/create', fakeData).reply(200, {
            result: true
        })
        const response = await saveAddress('fakeToken',fakeAddress)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('deleteAddress should return correct response', async () => {
        const { deleteAddress } = ApiCustomer.create()
        const fakeAddressId = '1'
        nock(AppConfig.apiBaseUrl).delete(`/public/account/address/${fakeAddressId}/destroy`).reply(200, {
            result: true
        })
        const response = await deleteAddress(fakeAddressId)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('setDefaultAddress should return correct response', async () => {
        const { setDefaultAddress } = ApiCustomer.create()
        const fakeAddressId = '1'
        nock(AppConfig.apiBaseUrl).post(`/public/account/address/${fakeAddressId}/default`).reply(200, {
            result: true
        })
        const response = await setDefaultAddress(fakeAddressId)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('editAddress should return correct response', async () => {
        const { editAddress } = ApiCustomer.create()
        const fakeAddressId = '1'
        const fakeAddress = {
            latitude: '1',
            longitude: '2',
            addressLabel: 'abc',
            addressPinpoint: '121',
            addressDetail: 'abcde',
            recipientName: 'John',
            recipientNumber: '1212',
            deliveryInstructions: 'abcde ipsum',
        }
        const fakeData = {
            landmark: fakeAddress.addressPinpoint,
            address: fakeAddress.addressDetail,
            latitude: fakeAddress.latitude,
            longitude: fakeAddress.longitude,
            name: fakeAddress.recipientName,
            phone: fakeAddress.recipientNumber,
            label: fakeAddress.addressLabel,
            note: fakeAddress.deliveryInstructions,
        }
        nock(AppConfig.apiBaseUrl).post(`/public/account/address/${fakeAddressId}/update`, fakeData).reply(200, {
            result: true
        })
        const response = await editAddress(fakeAddressId, fakeAddress, 'fakeToken')
        expect(response.data).toEqual({
            result: true
        })
    })

    test('fetchVirtualAccount should return correct response', async () => {
        const { fetchVirtualAccount } = ApiCustomer.create()
        const fakeBankCode = '012'
        const fakeToken = '21wqe123'

        nock(AppConfig.apiBaseUrl).post('/public/payment/virtual_account', {bank_code: fakeBankCode}).reply(200, {
            result: true
        })
        const response = await fetchVirtualAccount(fakeBankCode, fakeToken)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('getVirtualAccounts should return correct response', async () => {
        const { getVirtualAccounts } = ApiCustomer.create()
        nock(AppConfig.apiBaseUrl).get('/public/payment/virtual_account').reply(200, {
            result: true
        })
        const response = await getVirtualAccounts()
        expect(response.data).toEqual({
            result: true
        })
    })

    test('deviceInfoUpdate should return correct response', async () => {
        const { deviceInfoUpdate } = ApiCustomer.create()
        const fakeData = {
            uid: '123',
            fcm_token: '123123'
        }
        nock(AppConfig.apiBaseUrl).post('/public/account/device/update', fakeData).reply(200, {
            result: true
        })
        const response = await deviceInfoUpdate(fakeData.uid, fakeData.fcm_token)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('validateIdentity should return correct response', async () => {
        const { validateIdentity } = ApiCustomer.create()
        const fakeData = {
            identity: '123',
            email: 'email@domain.com',
            phone: '08110811'
        }
        nock(AppConfig.apiBaseUrl).get('/public/account/validate').query(fakeData).reply(200, {
            result: true
        })
        const response = await validateIdentity(fakeData.identity, fakeData.email, fakeData.phone)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('getWallet should return correct response', async () => {
        const { getWallet } = ApiCustomer.create()
        nock(AppConfig.apiBaseUrl).get('/public/account/wallet').reply(200, {
            result: true
        })
        const response = await getWallet()
        expect(response.data).toEqual({
            result: true
        })
    })

    test('redeemCreditToken should return correct response', async () => {
        const { redeemCreditToken } = ApiCustomer.create()
        const fakeCode = '012'
        const fakeToken = '21wqe123'

        nock(AppConfig.apiBaseUrl).post('/public/account/wallet/redeem', {token: fakeCode}).reply(200, {
            result: true
        })
        const response = await redeemCreditToken(fakeToken, fakeCode)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('photoUpload should return correct response', async () => {
        const { photoUpload } = ApiCustomer.create()
        const fakeData = '012'
        const fakeToken = '21wqe123'

        nock(AppConfig.apiBaseUrl).post('/public/account/upload', fakeData).reply(200, {
            result: true
        })
        const response = await photoUpload(fakeToken, fakeData)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('changePassword should return correct response', async () => {
        const { changePassword } = ApiCustomer.create()
        const fakePasswordData = {
            token: '123',
            old_password: '123123',
            password: 'pwd',
            password_confirmation: 'pwd_cnfrmtn'
        }
        const fakeData = {
            old_password: fakePasswordData.old_password,
            password: fakePasswordData.password,
            password_confirmation: fakePasswordData.password_confirmation,
        }
        nock(AppConfig.apiBaseUrl).post('/public/account/password/update', fakeData).reply(200, {
            result: true
        })
        const response = await changePassword(fakePasswordData.token, fakePasswordData.old_password, fakePasswordData.password, fakePasswordData.password_confirmation)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('forgotPassword should return correct response', async () => {
        const { forgotPassword } = ApiCustomer.create()
        const fakeEmail = 'email@yummycorp.com'

        nock(AppConfig.apiBaseUrl).post('/public/forgot_password', {email: fakeEmail}).reply(200, {
            result: true
        })
        const response = await forgotPassword(fakeEmail)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('changePhoneNumber should return correct response', async () => {
        const { changePhoneNumber } = ApiCustomer.create()
        const fakePhoneNumber = '08110811'
        const fakeToken = '21wqe123'

        nock(AppConfig.apiBaseUrl).post('/public/account/update/phone', {phone: fakePhoneNumber}).reply(200, {
            result: true
        })
        const response = await changePhoneNumber(fakeToken, fakePhoneNumber)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('changeEmail should return correct response', async () => {
        const { changeEmail } = ApiCustomer.create()
        const fakeNewEmail = 'email@yummycorp.com'
        const fakeToken = '21wqe123'

        nock(AppConfig.apiBaseUrl).post('/public/account/update/email', {email: fakeNewEmail}).reply(200, {
            result: true
        })
        const response = await changeEmail(fakeToken, fakeNewEmail)
        expect(response.data).toEqual({
            result: true
        })
    })

    test('checkClient should return correct response', async () => {
        const { checkClient } = ApiCustomer.create()
        nock(AppConfig.apiCaterUrl).get('/is_client').reply(200, {
            result: true
        })
        const response = await checkClient()
        expect(response.data).toEqual({
            result: true
        })
    })

})
