import MockAdapter from 'axios-mock-adapter'

import fakeState from '../../Sagas/__tests__/fakeState'
import ContinuePaymentService, {
  ResponseContinuePaymentErrorInterface,
  ResponseContinuePaymentOkInterface
} from '../ContinuePaymentService'

const api = ContinuePaymentService.create()
const mock = new MockAdapter(api.axiosInstence)

describe('Continue Payment unit testing', () => {
  test('ContinuePaymentAPI should return correct response', async () => {
    const expectationResponseOk: ResponseContinuePaymentOkInterface = {
      error: false,
      checkout_url: {
        redirect_url_http: fakeState.order.checkoutUrl,
        redirect_url_app: fakeState.order.checkoutUrl
      },
      invoice_url: '',
      data: fakeState.order.orderNumber
    }
    mock
      .onPost('public/payment/get_checkout_url')
      .reply(200, expectationResponseOk)

    const response = await api.continuePaymentAPI({
      order_number: fakeState.order.orderNumber,
      token: fakeState.login.token
    })
    expect(response.ok).toBe(true)
    expect(response.status).toBe(200)
    expect(response.data).toEqual(expectationResponseOk)
  })

  test('ContinuePaymentAPI should return The selected order number is invalid', async () => {
    const expectationResponseError: ResponseContinuePaymentErrorInterface = {
      error: true,
      message: 'The selected order number is invalid.',
      status_code: 400
    }

    mock
      .onPost('public/payment/get_checkout_url')
      .reply(400, expectationResponseError)

    const response = await api.continuePaymentAPI({
      order_number: fakeState.order.orderNumber,
      token: null
    })
    expect(response.ok).toBe(false)
    expect(response.status).toBe(400)
    expect(response.data).toEqual(expectationResponseError)
  })
})
