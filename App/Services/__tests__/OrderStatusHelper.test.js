import OrderStatusHelper from '../OrderStatusHelper'

describe('OrderStatusHelper parseToLabel', () => {

  test(' should return Order Active if status is PROCESSING', async () => {
    const status = 'PROCESSING'
    const response = 'Order Active'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })

  test(' should return Awaiting Payment if status is PENDING_PAYMENT', async () => {
    const status = 'PENDING_PAYMENT'
    const response = 'Awaiting Payment'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })


  test(' should return Completed if status is COMPLETE', async () => {
    const status = 'COMPLETE'
    const response = 'Completed'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })


  test(' should return Active if status is ACTIVE', async () => {
    const status = 'ACTIVE'
    const response = 'Active'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })


  test(' should return Paused if status is PAUSED', async () => {
    const status = 'PAUSED'
    const response = 'Paused'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })


  test(' should return Inactive if status is INACTIVE', async () => {
    const status = 'INACTIVE'
    const response = 'Inactive'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })


  test(' should return Cancelled if status is CANCELLED', async () => {
    const status = 'CANCELLED'
    const response = 'Cancelled'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })


  test(' should return Refunded if status is CANCELLED_REFUNDED', async () => {
    const status = 'CANCELLED_REFUNDED'
    const response = 'Refunded'
    expect(OrderStatusHelper.parseToLabel(status)).toBe(response)
  })


})


describe('OrderStatusHelper parseToLabel', () => {

  test(' should return Active if status is PROCESSING', async () => {
    const status = 'PROCESSING'
    const response = 'Active'
    expect(OrderStatusHelper.parseForNotification(status)).toBe(response)
  })

  test(' should return Awaiting Payment if status is PENDING_PAYMENT', async () => {
    const status = 'PENDING_PAYMENT'
    const response = 'Awaiting Payment'
    expect(OrderStatusHelper.parseForNotification(status)).toBe(response)
  })


  test(' should return Completed if status is COMPLETE', async () => {
    const status = 'COMPLETE'
    const response = 'Completed'
    expect(OrderStatusHelper.parseForNotification(status)).toBe(response)
  })


  test(' should return Cancelled if status is CANCELLED', async () => {
    const status = 'CANCELLED'
    const response = 'Cancelled'
    expect(OrderStatusHelper.parseForNotification(status)).toBe(response)
  })


  test(' should return Refunded if status is REFUNDED', async () => {
    const status = 'REFUNDED'
    const response = 'Refunded'
    expect(OrderStatusHelper.parseForNotification(status)).toBe(response)
  })


})
