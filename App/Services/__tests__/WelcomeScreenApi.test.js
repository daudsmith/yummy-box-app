import WelcomeScreenApi from '../WelcomeScreenApi'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

const {
  getWelcomeScreenSlides,
} = WelcomeScreenApi.create()
describe('getWelcomeScreenSlides', () => {
  test('getWelcomeScreenSlides should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(200, { result: true })
    const response = await getWelcomeScreenSlides()
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getWelcomeScreenSlides should return page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(404, { error: true })
    const response = await getWelcomeScreenSlides()
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getWelcomeScreenSlides should return internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(500, { error: true })
    const response = await getWelcomeScreenSlides()
    expect(response.data)
      .toEqual({ error: true })
  })
})
