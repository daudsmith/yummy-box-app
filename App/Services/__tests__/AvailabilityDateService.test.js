import Moment from 'moment'
import AvailabilityDateService from '../AvailabilityDateService'

const {
  getInitialDates,
  getDeliveryDateList,
  checkIfNowIsExceedingCutOffTime,
  getDeliveryCutOffCountDown,
} = AvailabilityDateService

const fakeDate = '2019-09-09'
const fakeOffDates = []
const cutOffTimeSecond = '7200'
const cutOffTimeString = '2019-09-09 23:00:00'

describe('AvailabilityDateService', () => {
  test('getInitialDates should return correct response', () => {
    Date.now = jest.fn(() => new Date(2019, 8, 9, 23).valueOf())
    const response = getInitialDates(fakeDate, fakeOffDates, cutOffTimeSecond)
    expect(response.format('YYYY-MM-DD'))
      .toBe('2019-09-10')
  })
  test('getInitialDates should return 14 days of calendar, and start from 2019-09-10 response', () => {
    Date.now = jest.fn(() => new Date(2019, 8, 9, 6).valueOf())
    const response = getDeliveryDateList(fakeOffDates, cutOffTimeSecond)
    expect(response.length)
      .toBe(14)
    expect(response[0].format('YYYY-MM-DD'))
      .toBe('2019-09-10')
  })
  test('checkIfNowIsExceedingCutOffTime should return true', () => {
    Date.now = jest.fn(() => new Date(2019, 8, 9, 23).valueOf())
    const response = checkIfNowIsExceedingCutOffTime(cutOffTimeString)
    expect(response)
      .toBe(true)
  })
  test('getDeliveryCutOffCountDown should return 1 hour  before cutoff', () => {
    Date.now = jest.fn(() => new Date(2019, 8, 9, 21).valueOf())
    const response = getDeliveryCutOffCountDown(cutOffTimeSecond)
    expect((response / 1000) / 3600)
      .toBe(1)
  })

  describe('disableWeekendAndOffDates', () => {
    let dayCounter = 1
    let arrayLength = 1
    let availableDates = []
    let availableDatesWithoutWeekends = []
    let weekendDates = []
    let expectedDisabledDates = {}
    while (arrayLength < 14) {
        const date = Moment().utcOffset(7).add(dayCounter, 'days')
        availableDates.push(date)

        const day = Moment(date).utcOffset(7).day()
        if (day === 0 || day === 6) {
            expectedDisabledDates[Moment(date).format('YYYY-MM-DD')] = {
                'disabled' : true,
                'disableTouchEvent': true
            }
            weekendDates.push(date)
        } else {
          availableDatesWithoutWeekends.push(date)
        }
        arrayLength = arrayLength + 1
        dayCounter = dayCounter + 1
    }

    test('Should return disabled weekend dates if weekend dathould return disabled off dates if off dates in available dateses in available dates', () => {
        expect(
          AvailabilityDateService.disableWeekendAndOffDates(availableDates, [])
        ).toStrictEqual(expectedDisabledDates)
    })

    test('Should return disabled weekend dates if weekend dates is passed as argument', () => {
      expect(
        AvailabilityDateService.disableWeekendAndOffDates(availableDatesWithoutWeekends, [], weekendDates)
      ).toStrictEqual(expectedDisabledDates)
  })

    test('Should return disabled off dates if off dates in available dates', () => {
      const offdate = availableDates[4]
      expectedDisabledDates[Moment(offdate).format('YYYY-MM-DD')] = {
        'disabled' : true,
        'disableTouchEvent': true
      }

      expect(
        AvailabilityDateService.disableWeekendAndOffDates(availableDates, [offdate])
      ).toStrictEqual(expectedDisabledDates)
  })
  })
})
