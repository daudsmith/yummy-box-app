import CalendarService from '../CalendarService'

describe('populateAvailableDateForCalendar', () => {
    let availableDates = ['2019-11-18', '2019-11-19']
    let extendedDates = ['2019-11-20', '2019-11-21']
    let selectedDate = '2019-11-18'
    let period1 = 1
    let disabledDateNull = null

    test('Should return available dates if period <= available dates array length', () => {
        expect(
            CalendarService.populateAvailableDateForCalendar(
                availableDates,
                extendedDates,
                selectedDate,
                period1,
                disabledDateNull,
            )
        ).toBe(availableDates)
    })

    let period3 = 5
    let disabledDate = '2019-11-20'
    test('Should return extended exclude disabled date if period > available dates array length and disabled date is not null', () => {
        expect(
            CalendarService.populateAvailableDateForCalendar(
                availableDates,
                extendedDates,
                selectedDate,
                period3,
                disabledDate,
            )
        ).toStrictEqual([extendedDates[1]])
    })
})
