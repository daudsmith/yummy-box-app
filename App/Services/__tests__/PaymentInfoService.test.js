import {
  buildNewCCPaymentInfo,
  buildNewCardData,
} from '../PaymentInfoService'

const fakeUser = {
  id: '1',
  first_name: 'Smith',
  last_name: 'Will',
}
const fakeCCToken = {
  id: '1',
  authentication_id: '12345',
}
const fakeAuthData = {
  id: '2',
  status: 'ACTIVE',
}
const fakePaymentInfo = {
  card_number: '1234567891012131',
  is_multiple_use: true,
  type: 'credit-card',
}

describe('PaymentInfoService', () => {
  test('buildNewCCPaymentInfo should return correct response', () => {
    const response = buildNewCCPaymentInfo(fakeCCToken, fakeAuthData, fakePaymentInfo)
    expect(response)
      .toEqual({
        type: fakePaymentInfo.type,
        ends_with: '2131',
        status: fakeAuthData.status,
        token: fakeCCToken.id,
        is_multiple_use: fakePaymentInfo.is_multiple_use,
        authorization_id: fakeAuthData.id,
      })
  })
  test('buildNewCCPaymentInfo should return invalid response', () => {
    const response = buildNewCCPaymentInfo(fakeCCToken, fakeAuthData, {})
    expect(response)
      .toBe(null)
  })

  test('buildNewCardData should return correct response', () => {
    const response = buildNewCardData(fakeUser, fakeCCToken, fakeAuthData, fakePaymentInfo)
    expect(response)
      .toEqual({
        account_id: fakeUser.id,
        token: fakeCCToken.id,
        authorization_id: fakeAuthData.id,
        type: fakePaymentInfo.type,
        ends_with: '2131',
      })
  })
  test('buildNewCardData should return invalid response', () => {
    const response = buildNewCardData(fakeUser, fakeCCToken, fakeAuthData, {})
    expect(response)
      .toBe(null)
  })
})
