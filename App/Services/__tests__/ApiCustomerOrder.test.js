import ApiCustomerOrder from '../ApiCustomerOrder'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

const {
  getOrderList,
  getSubscriptionList,
  getOrderDetail,
  getSubscriptionOrderDetail,
  stopSubscription,
  modifySubscriptionOrder,
  getLastOrder,
} = ApiCustomerOrder.create()

const fakeData = {
  default_delivery_time: '09 - 11',
  default_delivery_address: 'Jalan terus Pantang Mundur',
  default_delivery_note: '',
  default_delivery_latitude: 1,
  default_delivery_longitude: 1,
  default_delivery_details: '',
  default_recipient_name: 'Smith',
  default_recipient_phone: '0812345678910',
  default_payment_method: 'wallet',
  default_payment_info: '',
  repeat_interval: '[4]',
  theme_id: 1,
}

describe('ApiCustomerOrder', () => {
  test('getOrderList should return correct response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, { result: true })
    const response = await getOrderList(fakeToken)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getOrderList should return page not found response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(404, { error: true })
    const response = await getOrderList(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getOrderList should return internal server error response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(500, { error: true })
    const response = await getOrderList(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getSubscriptionList should return correct response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, { result: true })
    const response = await getSubscriptionList(fakeToken)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getSubscriptionList should return page not found response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(404, { error: true })
    const response = await getSubscriptionList(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getSubscriptionList should return internal server error response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(500, { error: true })
    const response = await getSubscriptionList(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getOrderDetail should return correct response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    const fakeType = 'subscription'
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeId}`)
      .query({ type: fakeType })
      .reply(200, { result: true })
    const response = await getOrderDetail(fakeToken, fakeId, fakeType)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getOrderDetail should return page not found response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    const fakeType = 'subscription'
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeId}`)
      .query({ type: fakeType })
      .reply(404, { error: true })
    const response = await getOrderDetail(fakeToken, fakeId, fakeType)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getOrderDetail should return internal server error response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    const fakeType = 'subscription'
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeId}`)
      .query({ type: fakeType })
      .reply(500, { error: true })
    const response = await getOrderDetail(fakeToken, fakeId, fakeType)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getSubscriptionOrderDetail should return correct response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeId}/detail`)
      .reply(200, { result: true })
    const response = await getSubscriptionOrderDetail(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getSubscriptionOrderDetail should return page not found response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeId}/detail`)
      .reply(404, { error: true })
    const response = await getSubscriptionOrderDetail(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getSubscriptionOrderDetail should return internal server error response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeId}/detail`)
      .reply(500, { error: true })
    const response = await getSubscriptionOrderDetail(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('stopSubscription should return correct response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeId}/pause`)
      .reply(200, { result: true })
    const response = await stopSubscription(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('stopSubscription should return page not found response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeId}/pause`)
      .reply(404, { error: true })
    const response = await stopSubscription(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('stopSubscription should return internal server error response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeId}/pause`)
      .reply(500, { error: true })
    const response = await stopSubscription(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('modifySubscriptionOrder should return correct response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/${fakeId}/update`, { ...fakeData })
      .reply(200, { result: true })
    const response = await modifySubscriptionOrder(fakeToken, fakeData, fakeId)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('modifySubscriptionOrder should return page not found response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/${fakeId}/update`, { ...fakeData })
      .reply(404, { error: true })
    const response = await modifySubscriptionOrder(fakeToken, fakeData, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('modifySubscriptionOrder should return internal server error response', async () => {
    const fakeToken = '12345'
    const fakeId = 10980
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/${fakeId}/update`, { ...fakeData })
      .reply(500, { error: true })
    const response = await modifySubscriptionOrder(fakeToken, fakeData, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getLastOrder should return correct response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/last')
      .reply(200, { result: true })
    const response = await getLastOrder(fakeToken)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getLastOrder should return page not found response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/last')
      .reply(404, { error: true })
    const response = await getLastOrder(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getLastOrder should return internal server error response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/last')
      .reply(500, { error: true })
    const response = await getLastOrder(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })
})
