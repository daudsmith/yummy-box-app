
import ApiCater from '../ApiCater'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

describe('ApiCater', () => {
  test('getCompanyAddresses should return correct response', async () => {
    const fakeToken = '12345678'
    const { getCompanyAddresses } = ApiCater.create()
    nock(AppConfig.apiBaseUrl).get('/address/list').reply(200, {
      result: true
    })
    const response = await getCompanyAddresses(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getCompanyAddresses not found', async () => {
    const fakeToken = '12345678'
    const { getCompanyAddresses } = ApiCater.create()
    nock(AppConfig.apiBaseUrl).get('/address/list').reply(404, {
      result: true
    })
    const response = await getCompanyAddresses(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getCompanyAddresses internal server error', async () => {
    const fakeToken = '12345678'
    const { getCompanyAddresses } = ApiCater.create()
    nock(AppConfig.apiBaseUrl).get('/address/list').reply(500, {
      result: true
    })
    const response = await getCompanyAddresses(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })


})
