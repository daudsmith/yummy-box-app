
import ApiCustomerNotification from '../ApiCustomerNotification'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

describe('ApiCustomerNotification getUnread', () => {

  test('getUnread should return correct response', async () => {
    const fakeToken = '12345678'
    const { getUnread } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/unread').reply(200, {
      result: true
    })
    const response = await getUnread(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getUnread internal server error', async () => {
    const fakeToken = '12345678'
    const { getUnread } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/unread').reply(500, {
      result: true
    })
    const response = await getUnread(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getUnread not found', async () => {
    const fakeToken = '12345678'
    const { getUnread } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/unread').reply(404, {
      result: true
    })
    const response = await getUnread(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

})

describe('ApiCustomerNotification getTotalUnread', () => {
  test('getTotalUnread should return correct response', async () => {
    const fakeToken = '12345678'
    const { getTotalUnread } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/unread/total').reply(200, {
      result: true
    })
    const response = await getTotalUnread(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getTotalUnread not found', async () => {
    const fakeToken = '12345678'
    const { getTotalUnread } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/unread/total').reply(404, {
      result: true
    })
    const response = await getTotalUnread(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getTotalUnread internal server error', async () => {
    const fakeToken = '12345678'
    const { getTotalUnread } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/unread/total').reply(500, {
      result: true
    })
    const response = await getTotalUnread(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })
})

describe('ApiCustomerNotification getNotifications', () => {

  test('getNotifications should return correct response', async () => {
    const fakeToken = '12345678'
    const { getNotifications } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/list').reply(200, {
      result: true
    })
    const response = await getNotifications(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getNotifications internal server error', async () => {
    const fakeToken = '12345678'
    const { getNotifications } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/list').reply(404, {
      result: true
    })
    const response = await getNotifications(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getNotifications should return ', async () => {
    const fakeToken = '12345678'
    const { getNotifications } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/list').reply(500, {
      result: true
    })
    const response = await getNotifications(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })
})

describe('ApiCustomerNotification markNotificationRead', () => {

  test('markNotificationRead should return correct response', async () => {
    const fakeToken = '12345678'
    const { markNotificationRead } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/mark_read').reply(200, {
      result: true
    })
    const response = await markNotificationRead(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('markNotificationRead not found', async () => {
    const fakeToken = '12345678'
    const { markNotificationRead } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/mark_read').reply(404, {
      result: true
    })
    const response = await markNotificationRead(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('markNotificationRead internal server error', async () => {
    const fakeToken = '12345678'
    const { markNotificationRead } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/mark_read').reply(500, {
      result: true
    })
    const response = await markNotificationRead(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })
})


describe('ApiCustomerNotification getReadByid', () => {

  test('getReadByid should return correct response', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { getReadByid } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/read`).reply(200, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await getReadByid(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

  test('getReadByid not found', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { getReadByid } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/read`).reply(404, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await getReadByid(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

  test('getReadByid internal server error', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { getReadByid } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/read`).reply(500, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await getReadByid(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

})


describe('ApiCustomerNotification getUnreadByid', () => {

  test('getUnreadByid should return correct response', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { getUnreadByid } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/unread`).reply(200, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await getUnreadByid(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

  test('getUnreadByid not found', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { getUnreadByid } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/unread`).reply(404, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await getUnreadByid(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

  test('getUnreadByid internal server error', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { getUnreadByid } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/unread`).reply(500, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await getUnreadByid(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

})

describe('ApiCustomerNotification deleteSingleNotification', () => {

  test('deleteSingleNotification should return correct response', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { deleteSingleNotification } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/delete`).reply(200, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await deleteSingleNotification(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

  test('deleteSingleNotification not found', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { deleteSingleNotification } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/delete`).reply(404, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await deleteSingleNotification(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

  test('deleteSingleNotification internal server error', async () => {
    const fakeToken = '12345678'
    const fakeNotifId = 1
    const { deleteSingleNotification } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get(`/public/account/notification/${fakeNotifId}/delete`).reply(500, {
      result: {
        notifId: fakeNotifId
      }
    })
    const response = await deleteSingleNotification(fakeToken, fakeNotifId)
    expect(response.data).toEqual({
      result: {
        notifId: fakeNotifId
      }
    })
  })

})

describe('ApiCustomerNotification deleteAllNotifications', () => {

  test('deleteAllNotifications should return correct response', async () => {
    const fakeToken = '12345678'
    const { deleteAllNotifications } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/delete/all').reply(200, {
      result: true
    })
    const response = await deleteAllNotifications(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('deleteAllNotifications int', async () => {
    const fakeToken = '12345678'
    const { deleteAllNotifications } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/delete/all').reply(404, {
      result: true
    })
    const response = await deleteAllNotifications(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('deleteAllNotifications internal server error', async () => {
    const fakeToken = '12345678'
    const { deleteAllNotifications } = ApiCustomerNotification.create()
    nock(AppConfig.apiBaseUrl).get('/public/account/notification/delete/all').reply(500, {
      result: true
    })
    const response = await deleteAllNotifications(fakeToken)
    expect(response.data).toEqual({
      result: true
    })
  })

})
