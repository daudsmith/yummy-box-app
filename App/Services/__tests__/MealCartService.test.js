import MealCartService from '../MealCartService'
import moment from 'moment'

const {
  isItemExistInCart,
  getCartQuantity,
  getCartItem,
} = MealCartService

const fakeCart = [
  {
    date: '2019-09-09',
    items: [
      {
        id: '1',
        name: 'Nasi Padang',
        quantity: 1,
      },
      {
        id: '2',
        name: 'Nasi Goreng',
        quantity: 1,
      },
    ],
  },
]
const fakeItem = {
  id: '1',
  name: 'Nasi Padang',
}
const fakeDate = moment('2019-09-09')

describe('MealCartService', () => {
  test('isItemExistInCart should return correct response', () => {
    const response = isItemExistInCart(fakeCart, fakeItem, fakeDate)
    expect(response)
      .toBe(true)
  })
  test('isItemExistInCart should return false on new cart item', () => {
    const response = isItemExistInCart(fakeCart, {
      ...fakeItem,
      id: '3',
    }, fakeDate)
    expect(response)
      .toBe(false)
  })

  test('getCartQuantity should return 2 quantity item', () => {
    const response = getCartQuantity(fakeCart, 'item')
    expect(response)
      .toBe(2)
  })

  test('getCartItem should return existing cartItem object data', () => {
    const result = getCartItem(fakeCart, fakeItem, fakeDate.format('YYYY-MM-DD'))
    expect(result.id)
      .toBe(fakeItem.id)
  })
  test('getCartItem should return null on not found item', () => {
    const result = getCartItem(fakeCart, {...fakeItem, id: 3}, fakeDate.format('YYYY-MM-DD'))
    expect(result)
      .toBe(null)
  })
})
