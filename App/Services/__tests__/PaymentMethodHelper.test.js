import PaymentMethodHepler from '../PaymentMethodHelper'

describe('getPaymentIconName', () => {
    
    test('Should return payment-cash if payment is cod', () => {
        expect(
            PaymentMethodHepler.getPaymentIconName('cod')
        ).toBe('payment-cash')
    })
    
    test('Should return payment-cc if payment is credit-card', () => {
        expect(
            PaymentMethodHepler.getPaymentIconName('credit-card')
        ).toBe('payment-cc')
    })

    test('Should return wallet if payment is wallet', () => {
        expect(
            PaymentMethodHepler.getPaymentIconName('wallet')
        ).toBe('wallet')
    })

    test('Should return transfer-icon if payment is virtual-account', () => {
        expect(
            PaymentMethodHepler.getPaymentIconName('virtual-account')
        ).toBe('transfer-icon')
    })

    test('Should return wallet if payment is empty', () => {
        expect(
            PaymentMethodHepler.getPaymentIconName('')
        ).toBe('wallet')
    })

})

describe('getCreditCardIconName', () => {

    test('Should return mastercard if type is MASTER', () => {
        expect(
            PaymentMethodHepler.getCreditCardIconName('MASTER')
        ).toBe('mastercard')
    })

    test('Should return visa if type is VISA', () => {
        expect(
            PaymentMethodHepler.getCreditCardIconName('VISA')
        ).toBe('visa')
    })

    test('Should return wallet if type is empty', () => {
        expect(
            PaymentMethodHepler.getCreditCardIconName('')
        ).toBe('wallet')
    })

})

describe('getCreditCardBrand', () => {

    test('Should return MASTER if cardBrand is MASTERCARD', () => {
        expect(
            PaymentMethodHepler.getCreditCardBrand('MASTERCARD')
        ).toBe('MASTER')
    })

    test('Should return [any input] if cardBrand is not MASTERCARD', () => {
        expect(
            PaymentMethodHepler.getCreditCardBrand('abcd')
        ).toBe('abcd')
    })

})
