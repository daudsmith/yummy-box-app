import CheckoutService from '../CheckoutService'
import fakeState from '../../Sagas/__tests__/fakeState'

const {
  buildNewDeliveryAddress,
  checkIfCartCanProceedToPayment,
} = CheckoutService

describe('CheckoutService unit testing', () => {
  test('buildNewDeliveryAddress should return correct response', () => {
    const expectation = {
      name: 'Yummy Employee',
      note: '',
      phone: '081827381',
      place: fakeState.customerAddress.addresses[0]
    }
    const response = buildNewDeliveryAddress(fakeState.login.user, fakeState.customerAddress.addresses[0])
    expect(response)
      .toEqual(expectation)
  })
  test('buildNewDeliveryAddress should return invalid response', () => {
    const response = buildNewDeliveryAddress(fakeState.login.user, null)
    expect(response)
      .toBe(null)
  })

  test('checkIfCartCanProceedToPayment should return correct response', () => {
    const response = checkIfCartCanProceedToPayment(fakeState.cart.cartItems)
    expect(response)
      .toEqual(true)
  })
  test('checkIfCartCanProceedToPayment should return invalid response', () => {
    const response = checkIfCartCanProceedToPayment([])
    expect(response)
      .toBe(false)
  })
  test('checkIfCartCanProceedToPayment should return false on invalid value of deliveryTime', () => {
    const response = checkIfCartCanProceedToPayment([])
    expect(response)
      .toBe(false)
  })
  test('checkIfCartCanProceedToPayment should return false on undefined delivery name', () => {
    const response = checkIfCartCanProceedToPayment([])
    expect(response)
      .toBe(false)
  })
  test('checkIfCartCanProceedToPayment should return false on undefined delivery phone', () => {
    const response = checkIfCartCanProceedToPayment([])
    expect(response)
      .toBe(false)
  })
  test('checkIfCartCanProceedToPayment should return false on invalid value of delivery name', () => {
    const response = checkIfCartCanProceedToPayment([])
    expect(response)
      .toBe(false)
  })
  test('checkIfCartCanProceedToPayment should return false on invalid value of delivery phone', () => {
    const response = checkIfCartCanProceedToPayment([])
    expect(response)
      .toBe(false)
  })
})
