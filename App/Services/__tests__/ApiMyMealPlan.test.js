import ApiMyMealPlan from '../ApiMyMealPlan'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

const {
  getMyMealPlans,
  getMyMealPlanDetail,
  cancelOrder,
  updateDelivery,
  skipDelivery,
  updateSubscriptionDelivery,
} = ApiMyMealPlan.create()

const fakeToken = '12345'
const fakeId = 892397
const fakeType = 'package'
const fakeData = {
  name: 'Smith',
  address: 'Jalan terus pantang mundur',
  longitude: 1,
  latitude: 1,
  date: '2019-09-09',
  time: '09 - 11',
}
describe('ApiMyMealPlan', () => {
  test('getMyMealPlans should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(200, { result: true })
    const response = await getMyMealPlans(fakeToken)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getMyMealPlans should return page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(404, { error: true })
    const response = await getMyMealPlans(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getMyMealPlans should return internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(500, { error: true })
    const response = await getMyMealPlans(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getMyMealPlanDetail should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}?type=${fakeType}`)
      .reply(200, { result: true })
    const response = await getMyMealPlanDetail(fakeToken, fakeId, fakeType)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getMyMealPlanDetail should return page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}?type=${fakeType}`)
      .reply(404, { error: true })
    const response = await getMyMealPlanDetail(fakeToken, fakeId, fakeType)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getMyMealPlanDetail should return internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}?type=${fakeType}`)
      .reply(500, { error: true })
    const response = await getMyMealPlanDetail(fakeToken, fakeId, fakeType)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('cancelOrder should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(200, { result: true })
    const response = await cancelOrder(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('cancelOrder should return page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(404, { error: true })
    const response = await cancelOrder(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('cancelOrder should return internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(500, { error: true })
    const response = await cancelOrder(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('updateDelivery should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`, { ...fakeData })
      .reply(200, { result: true })
    const response = await updateDelivery(fakeToken, fakeId, fakeData)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('updateDelivery should return page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`, { ...fakeData })
      .reply(404, { error: true })
    const response = await updateDelivery(fakeToken, fakeId, fakeData)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('updateDelivery should return internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`, { ...fakeData })
      .reply(500, { error: true })
    const response = await updateDelivery(fakeToken, fakeId, fakeData)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('skipDelivery should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/schedule/${fakeId}/skip`)
      .reply(200, { result: true })
    const response = await skipDelivery(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('skipDelivery should return page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/schedule/${fakeId}/skip`)
      .reply(404, { error: true })
    const response = await skipDelivery(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('skipDelivery should return internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/schedule/${fakeId}/skip`)
      .reply(500, { error: true })
    const response = await skipDelivery(fakeToken, fakeId)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('updateSubscriptionDelivery should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/schedule/${fakeId}/update`, { ...fakeData })
      .reply(200, { result: true })
    const response = await updateSubscriptionDelivery(fakeToken, fakeId, fakeData)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('updateSubscriptionDelivery should return page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/schedule/${fakeId}/update`, { ...fakeData })
      .reply(404, { error: true })
    const response = await updateSubscriptionDelivery(fakeToken, fakeId, fakeData)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('updateSubscriptionDelivery should return internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/schedule/${fakeId}/update`, { ...fakeData })
      .reply(500, { error: true })
    const response = await updateSubscriptionDelivery(fakeToken, fakeId, fakeData)
    expect(response.data)
      .toEqual({ error: true })
  })
})
