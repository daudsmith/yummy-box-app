import ApiSetting from '../ApiSetting'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { Platform } from 'react-native'
describe('ApiSetting get', () => {
  test('get should return correct response', async () => {
    const fakeKey = '12345678'
    const { get } = ApiSetting.create()
    nock(AppConfig.apiBaseUrl).get(`/public/setting/get/${fakeKey}`).reply(200, {
      result: {
        key: fakeKey
      }
    })
    const response = await get(fakeKey)
    expect(response.data).toEqual({
      result: {
        key: fakeKey
      }
    })
  })

  test('get not found', async () => {
    const fakeKey = '12345678'
    const { get } = ApiSetting.create()
    nock(AppConfig.apiBaseUrl).get(`/public/setting/get/${fakeKey}`).reply(404, {
      result: {
        key: fakeKey
      }
    })
    const response = await get(fakeKey)
    expect(response.data).toEqual({
      result: {
        key: fakeKey
      }
    })
  })


  test('get internal server error', async () => {
    const fakeKey = '12345678'
    const { get } = ApiSetting.create()
    nock(AppConfig.apiBaseUrl).get(`/public/setting/get/${fakeKey}`).reply(500, {
      result: {
        key: fakeKey
      }
    })
    const response = await get(fakeKey)
    expect(response.data).toEqual({
      result: {
        key: fakeKey
      }
    })
  })


})

describe('ApiSetting bulk', () => {

  test('bulk should return correct response', async () => {
    const fakeKey = [
      'fake-setting'
    ]
    const { bulk } = ApiSetting.create()
    nock(AppConfig.apiBaseUrl).get('/public/setting/get/bulk?keys=' + JSON.stringify(fakeKey)).reply(200, {
      result: true
    })
    const response = await bulk(fakeKey)
    expect(response.data).toEqual({
      result: true
    })
  })
  test('bulk should only receive JSON object on params', async () => {
    const fakeKey = [
      'fake-setting'
    ]
    const { bulk } = ApiSetting.create()
    nock(AppConfig.apiBaseUrl).get('/public/setting/get/bulk?keys=' + JSON.stringify(fakeKey)).reply(200, {
      result: true
    })
    const response = await bulk(fakeKey)
    expect(typeof fakeKey).toBe('object')
    expect(response.data).toEqual({
      result: true
    })
  })


})

describe('ApiSetting checkVersion', () => {


  test('checkversion should return correct response for Android ', async () => {
    Platform.OS = 'android'
    const { checkVersion } = ApiSetting.create()
    nock(AppConfig.apiBaseUrl).get('/public/setting/checkVersion').reply(200, {
      result: true
    })
    const response = await checkVersion()
    expect(response.data).toEqual({
      result: true
    })
  })


  test('checkversion should return correct response for Ios ', async () => {
    Platform.OS = 'ios'
    const { checkVersion } = ApiSetting.create()
    nock(AppConfig.apiBaseUrl).get('/public/setting/checkVersion').reply(200, {
      result: true
    })
    const response = await checkVersion()
    expect(response.data).toEqual({
      result: true
    })
  })

})
