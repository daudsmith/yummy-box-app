import ApiSalesOrder from '../ApiSalesOrder'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

describe('create', () => {

    test('create should return correct response', async () => {
        const { create } = ApiSalesOrder.create()
        nock(AppConfig.apiBaseUrl).post('/public/sales/order/create').reply(200, {
            result: true
        })
        const response = await create()
        expect(response.data).toEqual({
            result: true
        })
    })

    test('create should return error if not found', async () => {
        const { create } = ApiSalesOrder.create()
        nock(AppConfig.apiBaseUrl).post('/public/sales/order/create').reply(404, {
            error: true
        })
        const response = await create()
        expect(response.data).toEqual({
            error: true
        })
    })

    test('create should return error if server is error', async () => {
        const { create } = ApiSalesOrder.create()
        nock(AppConfig.apiBaseUrl).post('/public/sales/order/create').reply(500, {
            error: true
        })
        const response = await create()
        expect(response.data).toEqual({
            error: true
        })
    })

    test('cancel should return correct response', async () => {
        const { cancel } = ApiSalesOrder.create()
        const id = 123
        nock(AppConfig.apiBaseUrl).get(`/public/sales/order/cancel/${id}`).reply(200, {
            result: true
        })
        const response = await cancel(id, '12345')
        expect(response.data).toEqual({
            result: true
        })
    })

    test('cancel should return error if method not found', async () => {
        const { cancel } = ApiSalesOrder.create()
        const id = 123
        nock(AppConfig.apiBaseUrl).get(`/public/sales/order/cancel/${id}`).reply(404, {
            result: true
        })
        const response = await cancel(id, '12345')
        expect(response.data).toEqual({
            result: true
        })
    })

    test('cancel should return error if server is error', async () => {
        const { cancel } = ApiSalesOrder.create()
        const id = 123
        nock(AppConfig.apiBaseUrl).get(`/public/sales/order/cancel/${id}`).reply(500, {
            result: true
        })
        const response = await cancel(id, '12345')
        expect(response.data).toEqual({
            result: true
        })
    })

    test('cancel should return empty if ID is null', async () => {
        const { cancel } = ApiSalesOrder.create()
        const id = null
        nock(AppConfig.apiBaseUrl).get(`/public/sales/order/cancel/${id}`).reply(200, {
            result: ''
        })
        const response = await cancel(id, '12345')
        expect(response.data).toEqual({
            result: ''
        })
    })

})
