import ApiCatalog from '../ApiCatalog'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

const {
  getCatalogCategories,
  getPromoteCategories,
  getMeals,
  getMealDetails,
  getMealAvailability,
  getTagList,
  getFilteredMeals,
  getOffDates,
  getThemeList,
  getCorporateThemeList
} = ApiCatalog.create()

describe('ApiCatalog', () => {
  test('getCatalogCategories should return correct response', async () => {
    const fakeDate = '2019-09-09'
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({ date: fakeDate })
      .reply(200, { result: true })
    const response = await getCatalogCategories(fakeDate)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getCatalogCategories should return page not found response', async () => {
    const fakeDate = '2019-09-09'
    nock(AppConfig.apiBaseUrl)
    .get('/public/catalog/category/list')
    .query({ date: fakeDate })
    .reply(404, { error: true })
    const response = await getCatalogCategories(fakeDate)
    expect(response.data)
    .toEqual({ error: true })
  })
  test('getCatalogCategories should return internal system response', async () => {
    const fakeDate = 'thisIsString'
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({ date: fakeDate })
      .reply(500, { error: true })
      const response = await getCatalogCategories(fakeDate)
    expect(response.data)
    .toEqual({ error: true })
  })
  test('getPromoteCategories should return correct response', async () => {
    const fakeDate = '2019-09-09'
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/promoted')
      .query({ date: fakeDate })
      .reply(200, { result: true })
    const response = await getPromoteCategories(fakeDate)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getPromoteCategories should return page not found response', async () => {
    const fakeDate = '2019-09-09'
    nock(AppConfig.apiBaseUrl)
    .get('/public/catalog/category/promoted')
    .query({ date: fakeDate })
    .reply(404, { error: true })
    const response = await getPromoteCategories(fakeDate)
    expect(response.data)
    .toEqual({ error: true })
  })
  test('getPromoteCategories should return internal system response', async () => {
    const fakeDate = 'thisIsString'
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/promoted')
      .query({ date: fakeDate })
      .reply(500, { error: true })
      const response = await getPromoteCategories(fakeDate)
    expect(response.data)
    .toEqual({ error: true })
  })
  test('getMeals should return correct response', async () => {
    const fakeDate = '2019-09-09'
    const fakeCategoryId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: fakeDate,
        category_id: fakeCategoryId,
      })
      .reply(200, { result: true })
    const response = await getMeals(fakeDate, fakeCategoryId)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getMeals should return internal system response', async () => {
    const fakeDate = '2019-09-09'
    const fakeCategoryId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: fakeDate,
        category_id: fakeCategoryId,
      })
      .reply(404, { error: true })
    const response = await getMeals(fakeDate, fakeCategoryId)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getMeals should return internal system response', async () => {
    const fakeDate = 'thisIsString'
    const fakeCategoryId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: fakeDate,
        category_id: fakeCategoryId,
      })
      .reply(500, { error: true })
    const response = await getMeals(fakeDate, fakeCategoryId)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getMealDetails should return correct response', async () => {
    const fakeDate = '2019-09-09'
    const fakeProductId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        date: fakeDate,
        product_id: fakeProductId,
      })
      .reply(200, { result: true })
    const response = await getMealDetails(fakeProductId, fakeDate)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getMealDetails should return internal system response', async () => {
    const fakeDate = '2019-09-09'
    const fakeProductId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        date: fakeDate,
        product_id: fakeProductId,
      })
      .reply(404, { error: true })
    const response = await getMealDetails(fakeProductId, fakeDate)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getMealDetails should return internal system response', async () => {
    const fakeDate = 'thisIsString'
    const fakeProductId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        date: fakeDate,
        product_id: fakeProductId,
      })
      .reply(500, { error: true })
    const response = await getMealDetails(fakeProductId, fakeDate)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getMealAvailability should return correct response', async () => {
    const fakeDate = '2019-09-09'
    const fakeProductId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/availability')
      .query({
        date: fakeDate,
        product_id: fakeProductId,
      })
      .reply(200, { result: true })
    const response = await getMealAvailability(fakeProductId, fakeDate)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getMealAvailability should return internal system response', async () => {
    const fakeDate = '2019-09-09'
    const fakeProductId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/availability')
      .query({
        date: fakeDate,
        product_id: fakeProductId,
      })
      .reply(404, { error: true })
    const response = await getMealAvailability(fakeProductId, fakeDate)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getMealAvailability should return internal system response', async () => {
    const fakeDate = 'thisIsString'
    const fakeProductId = 2019
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/availability')
      .query({
        date: fakeDate,
        product_id: fakeProductId,
      })
      .reply(500, { error: true })
    const response = await getMealAvailability(fakeProductId, fakeDate)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getTagList should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(200, { result: true })
    const response = await getTagList()
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getTagList should return internal system response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(404, { error: true })
    const response = await getTagList()
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getTagList should return internal system response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(500, { error: true })
    const response = await getTagList()
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getFilteredMeals should return correct response', async () => {
    const fakeDate = '2019-09-09'
    const fakeIncludes = false
    const fakeExcludes = false
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeDate,
        include_tags: fakeIncludes,
        exclude_tags: fakeExcludes,
      })
      .reply(200, { result: true })
    const response = await getFilteredMeals(fakeDate, fakeIncludes, fakeExcludes)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getFilteredMeals should return internal system response', async () => {
    const fakeDate = '2019-09-09'
    const fakeIncludes = false
    const fakeExcludes = false
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeDate,
        include_tags: fakeIncludes,
        exclude_tags: fakeExcludes,
      })
      .reply(404, { error: true })
    const response = await getFilteredMeals(fakeDate, fakeIncludes, fakeExcludes)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getFilteredMeals should return internal system response', async () => {
    const fakeDate = 'thisIsString'
    const fakeIncludes = false
    const fakeExcludes = false
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeDate,
        include_tags: fakeIncludes,
        exclude_tags: fakeExcludes,
      })
      .reply(500, { error: true })
    const response = await getFilteredMeals(fakeDate, fakeIncludes, fakeExcludes)
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getOffDates should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(200, { result: true })
    const response = await getOffDates()
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getOffDates should return internal system response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(404, { error: true })
    const response = await getOffDates()
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getOffDates should return internal system response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(500, { error: true })
    const response = await getOffDates()
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getThemeList should return correct response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(200, { result: true })
    const response = await getThemeList()
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getThemeList should return internal system response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(404, { error: true })
    const response = await getThemeList()
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getThemeList should return internal system response', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(500, { error: true })
    const response = await getThemeList()
    expect(response.data)
      .toEqual({ error: true })
  })

  test('getCorporateThemeList should return correct response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      // .query({ token: fakeToken })
      .reply(200, { result: true })
    const response = await getCorporateThemeList(fakeToken)
    expect(response.data)
      .toEqual({ result: true })
  })
  test('getCorporateThemeList should return internal system response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      // .query({ token: fakeToken })
      .reply(404, { error: true })
    const response = await getCorporateThemeList(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })
  test('getCorporateThemeList should return internal system response', async () => {
    const fakeToken = '12345'
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      // .query({ token: fakeToken })
      .reply(500, { error: true })
    const response = await getCorporateThemeList(fakeToken)
    expect(response.data)
      .toEqual({ error: true })
  })
})
