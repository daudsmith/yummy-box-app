
import ApiTheme from '../ApiTheme'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'

const fakeThemeId = 1
const fakeDate = '2019-19-11'
const fakeAvailableDate = ['2019-19-11', '2019-19-12']
describe('ApiTheme getThemeDetail', () => {
  test('getThemeDetail should return correct response', async () => {
    const { getThemeDetail } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/detail`).reply(200, {
      result: true
    })
    const response = await getThemeDetail(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getThemeDetail not found', async () => {
    const { getThemeDetail } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/detail`).reply(404, {
      result: true
    })
    const response = await getThemeDetail(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getThemeDetail internal server error', async () => {
    const { getThemeDetail } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/detail`).reply(500, {
      result: true
    })
    const response = await getThemeDetail(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


})


describe('ApiTheme getThemeDetailMeals', () => {
  test('getThemeDetailMeals should return correct response', async () => {
    const { getThemeDetailMeals } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/meal/list`).reply(200, {
      result: true
    })
    const response = await getThemeDetailMeals(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getThemeDetailMeals not found', async () => {
    const { getThemeDetailMeals } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/meal/list`).reply(404, {
      result: true
    })
    const response = await getThemeDetailMeals(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getThemeDetailMeals internal server error', async () => {
    const { getThemeDetailMeals } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/meal/list`).reply(500, {
      result: true
    })
    const response = await getThemeDetailMeals(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


})


describe('ApiTheme getThemePackages', () => {
  test('getThemePackages should return correct response', async () => {
    const { getThemePackages } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/packages`).reply(200, {
      result: true
    })
    const response = await getThemePackages(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getThemePackages not found', async () => {
    const { getThemePackages } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/packages`).reply(404, {
      result: true
    })
    const response = await getThemePackages(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getThemePackages internal server error', async () => {
    const { getThemePackages } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/packages`).reply(500, {
      result: true
    })
    const response = await getThemePackages(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


})

describe('ApiTheme getAvailableDate', () => {
  test('getAvailableDate should return correct response', async () => {
    const { getAvailableDate } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/available`).reply(200, {
      result: true
    })
    const response = await getAvailableDate(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getAvailableDate not found', async () => {
    const { getAvailableDate } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/available`).reply(404, {
      result: true
    })
    const response = await getAvailableDate(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getAvailableDate internal server error', async () => {
    const { getAvailableDate } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/available`).reply(500, {
      result: true
    })
    const response = await getAvailableDate(fakeThemeId)
    expect(response.data).toEqual({
      result: true
    })
  })


})


describe('ApiTheme getAvailableDateExtends', () => {
  test('getAvailableDateExtends should return correct response', async () => {
    const { getAvailableDateExtends } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/available?start_from=${fakeDate}`).reply(200, {
      result: true
    })
    const response = await getAvailableDateExtends(fakeThemeId, fakeDate)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getAvailableDateExtends not found', async () => {
    const { getAvailableDateExtends } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/available?start_from=${fakeDate}`).reply(404, {
      result: true
    })
    const response = await getAvailableDateExtends(fakeThemeId, fakeDate)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getAvailableDateExtends internal server error', async () => {
    const { getAvailableDateExtends } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/available?start_from=${fakeDate}`).reply(500, {
      result: true
    })
    const response = await getAvailableDateExtends(fakeThemeId, fakeDate)
    expect(response.data).toEqual({
      result: true
    })
  })


})


describe('ApiTheme getMealByDates', () => {
  test('getMealByDates should return correct response', async () => {
    const { getMealByDates } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/meals_by_dates?${dateToString(fakeAvailableDate)}`).reply(200, {
      result: true
    })
    const response = await getMealByDates(fakeThemeId, fakeAvailableDate)
    expect(response.data).toEqual({
      result: true
    })
  })

  test('getMealByDates not found', async () => {
    const { getMealByDates } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/meals_by_dates?${dateToString(fakeAvailableDate)}`).reply(404, {
      result: true
    })
    const response = await getMealByDates(fakeThemeId, fakeAvailableDate)
    expect(response.data).toEqual({
      result: true
    })
  })


  test('getMealByDates internal server error', async () => {
    const { getMealByDates } = ApiTheme.create()
    nock(AppConfig.apiBaseUrl).get(`/public/catalog/theme/${fakeThemeId}/meals_by_dates?${dateToString(fakeAvailableDate)}`).reply(500, {
      result: true
    })
    const response = await getMealByDates(fakeThemeId, fakeAvailableDate)
    expect(response.data).toEqual({
      result: true
    })
  })


})
function dateToString(availableDates) {
  let availableDatesString = ''
  for (let i = 0; i < availableDates.length; i++) {
    if (i === availableDates.length - 1) {
      availableDatesString = availableDatesString + 'dates[]=' + availableDates[i]
    } else {
      availableDatesString = availableDatesString + 'dates[]=' + availableDates[i] + '&'
    }
  }
  return availableDatesString
}
