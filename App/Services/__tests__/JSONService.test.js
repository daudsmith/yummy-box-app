import JSONService from '../JSONService'

describe('isJsonString', () => {
  test('should return true if param is json stringified', () => {
    const fakeJsonString = JSON.stringify({ name: 'budi', phone: '098777636616' })
    expect(
      JSONService.isJSONString(fakeJsonString)
    ).toEqual(true)
  })

  test('should return false if param is array stringified', () => {
    const fakeJsonString = '[\'22\',\'123\']'
    expect(
      JSONService.isJSONString(fakeJsonString)
    ).toEqual(false)
  })
})
