import _ from 'lodash'
import { User } from '../Redux/LoginRedux'
import { Address } from '../Redux/CustomerAddressRedux'
import { Delivery } from '../Redux/V2/CartRedux'

const buildNewDeliveryAddress = (user: User, place: Address) => {
  if (!user
    || !place
    || Object.keys(user).length === 0
    || Object.keys(place).length === 0
    || !user.hasOwnProperty('first_name')
    || !user.hasOwnProperty('phone')
  ) {
    return null
  }
  let note = ''
  const {first_name, last_name, phone} = user
  let name = first_name + (last_name === null ? ' ' : ' ' + last_name)
  let phoneString = '+' + phone
  if (place.name) {
    name = place.name
  }
  if (place.phone) {
    phoneString = place.phone
  }
  if (place.note) {
    note = place.note
  }
  return { name, phone: phoneString, note, place }
}

const checkIfCartCanProceedToPayment = (cartItems: Delivery[]) => {
  if (cartItems.length < 1) {
    return false
  }

  for (const cartItem of cartItems) {
    if (!haveDeliveryTime(cartItem)) {
      return false
    }
    if (isDeliveryObjectEmpty(cartItem)) {
      return false
    }
    if (!haveRecipientName(cartItem)) {
      return false
    }
    if (!haveRecipientPhone(cartItem)) {
      return false
    }
  }
  return true
}

const haveDeliveryTime = (cartItem: Delivery) => {
  return (cartItem.hasOwnProperty('deliveryTime') && !!cartItem.deliveryTime)
}
const isDeliveryObjectEmpty = (cartItem: Delivery) => {
  return _.isEmpty(cartItem.deliveryAddress)
}
const haveRecipientName = (cartItem: Delivery) => {
  return (cartItem.hasOwnProperty('deliveryAddress') && cartItem.deliveryAddress.hasOwnProperty('name') && cartItem.deliveryAddress.name)
}
const haveRecipientPhone = (cartItem: Delivery) => {
  return (cartItem.hasOwnProperty('deliveryAddress') && cartItem.deliveryAddress.hasOwnProperty('phone') && cartItem.deliveryAddress.phone)
}

export default {
  buildNewDeliveryAddress,
  checkIfCartCanProceedToPayment
}
