import Geocoder from 'react-native-geocoding'

export interface GeoServiceInterface {
  init(apiKey: string, options: Object): void;
  isInit(): boolean;
  setApiKey(API_KEY: string): void;
  from(...params: any[]): Promise<any>;
  getFromLocation(address: string): Promise<any>;
  getFromLatLng(lat: number, lng: number): Promise<any>;
}

const GeoService: GeoServiceInterface = Geocoder

GeoService.init('AIzaSyABKuMOd7JHbk-Aqp3DnwiWZKx-GBEe4RE')

export default GeoService