import ReduxPersist from '../Config/ReduxPersist'
import AsyncStorage from '@react-native-community/async-storage'
import NetInfo from '@react-native-community/netinfo'
import { persistStore } from 'redux-persist'
import { offlineActionTypes } from 'react-native-offline'
import { Store } from 'redux'
import { AppState } from '../Redux/CreateStore'

const updateReducers = (store: Store<AppState>): void => {
  const reducerVersion: string = ReduxPersist.reducerVersion
  const startup = () => {
    NetInfo.fetch().then(state => {
      store.dispatch({
        type: offlineActionTypes.CONNECTION_CHANGE,
        payload: state.isConnected
      })
    })
  }

  // Check to ensure latest reducer version
  AsyncStorage.getItem('reducerVersion')
    .then((localVersion) => {
      if (localVersion !== reducerVersion) {
        // Purge store
        persistStore(store, {}, startup)
          .purge()
        AsyncStorage.setItem('reducerVersion', reducerVersion)
      } else {
        persistStore(store, {}, startup)
      }
    })
    .catch(() => {
      persistStore(store, {}, startup)
      AsyncStorage.setItem('reducerVersion', reducerVersion)
    })
}

export default { updateReducers }
