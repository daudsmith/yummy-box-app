import {StackActions, NavigationActions, NavigationProp, NavigationState} from 'react-navigation'

const reset = (navigation: NavigationProp<NavigationState>, routeName: string, params: object) => {
  const action = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({routeName, params})
    ]
  })
  navigation.dispatch(action)
}

const routingHelper = (screen: string): 'ThemeDetailScreen' | 'MealListScreen' | 'HomeScreen' => {
  switch (screen) {
    case 'playlist':
      return 'ThemeDetailScreen'
    case 'meal':
      return 'MealListScreen'
    default:
      return 'HomeScreen'
  }
}

export default {
  reset,
  routingHelper
}
