import CheckoutService from './CheckoutService'
import { Delivery } from '../Redux/V2/CartRedux'

const checkAllRequiredInformationValid = (cartItems: Delivery[], paymentInfo: object, newCreditCardInfo?: object) => {
  if (cartItems.length === 0) {
    return false
  }

  if (!CheckoutService.checkIfCartCanProceedToPayment(cartItems)) {
    return false
  }

  return !(
    (!paymentInfo || Object.keys(paymentInfo).length === 0)
    && (!newCreditCardInfo || Object.keys(newCreditCardInfo).length === 0)
  )
}

export default {
  checkAllRequiredInformationValid,
}
