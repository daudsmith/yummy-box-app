import {
  CartItemDelivery,
  CartItemSubscription,
  Total,
} from '../Redux/CartRedux'
import {
  CartState,
  Delivery,
} from '../Redux/V2/CartRedux'
import { Meal } from '../Redux/MealsRedux'
import { Moment } from 'moment'
import ApiCart, {
  itemResponse,
  responseData,
} from './V2/ApiCart'
import ApiCater from './ApiCater'
import ApiCustomer, { getSavedAddressesApiResponse } from './ApiCustomer'
import { PlaylistItem } from '../Containers/ThemeCalendarScreen'
import { NavigationDrawerProp } from 'react-navigation-drawer/lib/typescript/src/types'
import { Toast } from 'native-base'
import { Params } from '../Containers/HomeScreen'
import LogEventService from './LogEventService'
import ApiCatalog from './ApiCatalog'
import { PaymentOptionType } from '../Components/Payment/PaymentMethodPicker'

const buildSalesOrder = (
  cart: CartState,
  selectedPaymentMethod: PaymentOptionType,
  paymentInfo: object,
  isMultipleUse: boolean = true,
  orderTotal: number,
) => {
  const cartType = cart.type
  const deliveries = cart.cartItems.map((cartItem) => {
    let delivery: CartItemDelivery = {
      date: cartItem.date,
      time: cartItem.deliveryTime,
      landmark: cartItem.deliveryAddress.landmark
        ? cartItem.deliveryAddress.landmark
        : cartItem.deliveryAddress.label,
      name: cartItem.deliveryAddress.name,
      phone: cartItem.deliveryAddress.phone,
      note: cartItem.deliveryAddress.note || '',
      address: cartItem.deliveryAddress.address,
      latitude: cartItem.deliveryAddress.latitude,
      longitude: cartItem.deliveryAddress.longitude,
      items: cartItem.items,
      totals: cartItem.totals,
    }
    if (cartType === 'package') {
      delivery = {
        ...delivery,
        packages: cartItem.packages,
      }
    } else if (cartType === 'subscription') {
      delivery = {
        ...delivery,
        subscription: cartItem.subscription,
        first_item: cartItem.first_item,
      }
    }

    return delivery
  })

  return {
    deliveries,
    payment: {
      method: selectedPaymentMethod,
      info: paymentInfo,
      is_multiple_use: isMultipleUse,
    },
    coupon: cart.coupon,
    type: cartType,
    total: orderTotal || 0,
    kitchen_code: cart.cartItems[0].kitchen_code
  }
}

const checkAllRequiredInformationValid = (
  selectedPaymentMethod: string, paymentInfo: object, newCreditCardInfo: object) => {
  let allInformationIsValid = true
  if (selectedPaymentMethod !== 'cod') {
    if (paymentInfo === null && newCreditCardInfo === null) {
      allInformationIsValid = false
    }
  }
  return allInformationIsValid
}

const calculateItemInCart = (cart: CartState) => {
  const { cartItems, type } = cart
  let totalQuantity = 0
  for (let cartItem of cartItems) {
    if (type === 'item') {
      for (let item of cartItem.items) {
        totalQuantity = totalQuantity + item.quantity
      }
    } else {
      cartItem.packages.map(() => {
        totalQuantity = totalQuantity + 1
      })
    }
  }

  return totalQuantity
}

const setOrderTotal = (totals: Total) => {
  const {
    subtotal,
    delivery_fee,
    discount,
    tax,
    total,
    cashback,
  } = totals
  return {
    amount: subtotal,
    tax,
    deliveryFee: delivery_fee,
    discount,
    total,
    cashback,
  }
}

const isSameType = (cart: CartState, type) => {
  return cart.type === '' || cart.type === type
}

const isExclusive = (cart: CartState, item: Meal) => {
  if (cart.type !== '') {
    if (item.exclusive || cart.exclusive) {
      if (cart.partner !== item.partner) {
        return true
      }
    }
  }
  return false
}

const isCategoryExclusive = (cart: CartState, item: Meal) => {
  if (cart.type !== '') {
    if (item.category_exclusive || cart.category_exclusive) {
      if (cart.category !== item.category) {
        return true
      }
    }
  }
  return false
}

const isCategoryQtyMax = (cart, date, max) => {
  const itemDate = date.format('YYYY-MM-DD')

  // hardcode for hampers
  if (cart.category == 'Hampers') {
    let qty = 0
    const cartItem = cart.cartItems.find((item) => item.date == itemDate)
    if (cartItem) {
      qty += cartItem.items.reduce((prev, curr) => {
        return prev + curr.quantity
      }, 0)
  
      return qty >= max
    }
  }

  return false
}


const buildSingleOrder = (
  cart: CartState,
  item: Meal | itemResponse,
  date: Moment,
  quantity: number,
  code: string,
) => {
  let shoppingCart: CartState
  if (cart.cartItems.length === 0) {
    shoppingCart = setFirstCart(cart, item, date, quantity, code)
  } else {
    // inject existing cart
    const formattedDate = date.format('YYYY-MM-DD')
    let newDeliveries: Delivery[] = []
    let existDelivery = false
    cart.cartItems.forEach(delivery => {
      if (delivery.date === formattedDate) {
        existDelivery = true
        let newItems: itemResponse[] = []
        let existingItem = false
        delivery.items.forEach(deliveryItem => {
          if (deliveryItem.id === item.id) {
            newItems.push({
              ...(item as itemResponse),
              quantity: quantity,
            })
            existingItem = true
          } else {
            newItems.push(deliveryItem)
          }
        })

        if (!existingItem) {
          const newItem = buildCartItem(item, quantity)
          newItems.push(newItem)
        }

        newDeliveries.push({
          ...delivery,
          items: newItems,
          kitchen_code: code,
        })
      } else {
        newDeliveries.push(delivery)
      }
    })

    if (!existDelivery) {
      const delivery = setFirstDelivery(item, date, quantity, code)
      newDeliveries.push(delivery)
    }

    newDeliveries.sort((a, b) => {
      let comparison = 0
      const dateA = a.date
      const dateB = b.date
      if (dateA > dateB) {
        comparison = 1
      } else if (dateA < dateB) {
        comparison = -1
      }
      return comparison
    })

    shoppingCart = {
      type: 'item',
      cartItems: newDeliveries,
      partner: cart.partner,
      exclusive: cart.exclusive,
      category: cart.category,
      coupon: cart.coupon,
      lunch: cart.lunch,
      dinner: cart.dinner,
    }
  }
  return shoppingCart
}

const setFirstCart = (cart: CartState, item: Meal | itemResponse, date: Moment, quantity: number, code: string) => {
  const delivery = setFirstDelivery(item, date, quantity, code)
  return {
    ...cart,
    type: 'item',
    cartItems: [delivery],
  }
}

const setFirstDelivery = (item: Meal | itemResponse, date: Moment, quantity: number, code?: string): Delivery => {
  const cartItem = buildCartItem(item, quantity)
  return {
    date: date.format('YYYY-MM-DD'),
    items: [cartItem],
    kitchen_code: code,
  }
}

const buildCartItem = (item: Meal | itemResponse, quantity: number): itemResponse => {
  let sellPrice = (item as Meal).sale_price
  let images = (item as Meal).catalog_image
  if (!images) {
    images = (item as itemResponse).images
  }
  return {
    id: item.id,
    name: item.name,
    sale_price: sellPrice,
    tax_class: item.tax_class,
    images: images,
    quantity: quantity,
    category: item.category,
  }
}

const addToCart = (
  cart: CartState,
  item: Meal | itemResponse,
  date: Moment,
  quantity: number,
  code: string,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
) => {
  loadingCallback(true)

  const shoppingCart = buildSingleOrder(cart, item, date, quantity, code)
  cartRequest(
    shoppingCart,
    loadingCallback,
    resetCallback,
    reduxCallback,
  )
}

const addPlaylistCart = (
  cart: CartState,
  item: PlaylistItem,
  date: string,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
  nextCallback: () => void,
  kitchenCode: string
) => {
  loadingCallback(true)
  const playlistDelivery = setPlaylistDelivery(item, date, kitchenCode)
  const shoppingCart = {
    ...cart,
    type: 'package',
    cartItems: [playlistDelivery],
  }
  cartRequest(
    shoppingCart,
    loadingCallback,
    resetCallback,
    reduxCallback,
    nextCallback,
  )
}

const addSubscriptionCart = (
  cart: CartState,
  item: CartItemSubscription,
  date: string,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
  nextCallback: () => void,
  kitchen_code: string
) => {
  loadingCallback(true)
  const subscriptionDelivery = setSubscriptionDelivery(item, date, kitchen_code)
  const shoppingCart = {
    ...cart,
    type: 'subscription',
    cartItems: [subscriptionDelivery],
  }
  cartRequest(
    shoppingCart,
    loadingCallback,
    resetCallback,
    reduxCallback,
    nextCallback,
  )
}

const getItemAvailability = async (itemId: number, date: Moment, kitchenCode: string, quantity: number) => {
  const response = await ApiCatalog.create().getMealAvailability(itemId, date.format('YYYY-MM-DD'), kitchenCode)
  const { data } = response.data

  if (response.data.error || data?.remaining_quantity < quantity) {
    return false
  }

  return true

}

const cartRequest = (
  shoppingCart: CartState,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
  nextCallback?: () => void,
  token?: string,
) => {
  ApiCart.create()
    .validate(shoppingCart, token)
    .then(response => response.data)
    .then(responseBody => {
      const { data } = responseBody
      const { deliveries, cartItems } = data
      let deliveryData = deliveries
      if (!deliveryData) {
        deliveryData = cartItems
      }
      if (resetCallback && deliveryData.length === 0) {
        resetCallback()
      } else if (reduxCallback) {
        reduxCallback(data)
      }
      return data
    })
    .then(() => {
      loadingCallback(false)
      if (typeof nextCallback !== 'undefined') {
        nextCallback()
      }
    })
    .catch(() => loadingCallback(false))
}

const validateCart = (
  cart: CartState,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
  nextCallback: () => void,
) => {
  loadingCallback(true)
  cartRequest(
    cart,
    loadingCallback,
    resetCallback,
    reduxCallback,
    nextCallback,
  )
}

const getUserAddress = async (token: string) => {
  return ApiCustomer.create()
    .getSavedAddresses(token)
    .then((responsePersonal: getSavedAddressesApiResponse) => {
      return ApiCater.create()
        .getCompanyAddresses(token)
        .then(response => response.data)
        .then(responseBody => {
          return {
            corporate: responseBody.data,
            personal: responsePersonal.data,
          }
        })
    })
}

const applyCoupon = (
  cart: CartState,
  coupon: string,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
  token: string
) => {
  loadingCallback(true)
  const shoppingCart = buildCartWithCoupon(cart, coupon)
  cartRequest(
    shoppingCart,
    loadingCallback,
    resetCallback,
    reduxCallback,
    null,
    token
  )
}

const buildCartWithCoupon = (cart: CartState, coupon: string) => {
  return {
    ...cart,
    coupon: coupon,
  }
}

const setPlaylistDelivery = (item: PlaylistItem, date: string, kitchenCode: string): Delivery => {
  return {
    date: date,
    packages: [item],
    items: [],
    kitchen_code: kitchenCode
  }
}

const setSubscriptionDelivery = (
  item: CartItemSubscription,
  date: string,
  kitchen_code: string
): Delivery => {
  return {
    date: date,
    subscription: item,
    items: [],
    kitchen_code: kitchen_code
  }
}

const getDeliveryAddress = (
  token: string,
  loadingCallback: (status: boolean) => Promise<void>,
  navigator: NavigationDrawerProp,
  forSubscription: boolean = false,
  modifySubscription: boolean = false,
  modifyMMP: boolean = false,
) => {
  loadingCallback(true)
    .then(() => {
      addressNavigationHandler(
        token,
        loadingCallback,
        navigator,
        forSubscription,
        modifySubscription,
        modifyMMP,
      )
    })
}

const addressNavigationHandler = async (
  token: string,
  loadingCallback: (status: boolean) => Promise<void>,
  navigator: NavigationDrawerProp,
  forSubscription: boolean,
  modifySubscription: boolean,
  modifyMMP: boolean,
) => {
  const addresses = await getUserAddress(token)
  const { corporate, personal } = addresses

  let AddressTypeScreen = 'AddressTypeScreen'
  let AddressOptionScreen = 'AddressOptionScreen'
  let params = {
    addresses: personal,
    addressType: 'personal',
    checkoutScreenName: 'SingleOrderCheckoutScreen',
    createScreenName: 'CoCreateAddress',
    searchScreenName: 'CoSearchLocation',
    mapScreenName: 'CoMapScreen',
    changeState: 'checkout',
    key: 'CreateAddressKey',
    readOnly: false
  }
  if (forSubscription) {
    AddressTypeScreen = 'SubAddressTypeScreen'
    AddressOptionScreen = 'SubAddressOptionScreen'
    params = {
      ...params,
      createScreenName: 'SubCoCreateAddress',
      searchScreenName: 'SubCoSearchLocation',
      checkoutScreenName: 'SubscriptionConfirmationScreen',
    }
  }

  if (modifySubscription) {
    params = {
      ...params,
      changeState: 'modifySubscription',
    }
  }

  if (modifyMMP) {
    params = {
      ...params,
      checkoutScreenName: 'MyMealPlanModifyScreen',
      changeState: 'modifyMMP',
      readOnly: true
    }
  }

  loadingCallback(false)
    .then(() => {
      // navigate to next screen based on the addresses
      if (corporate && corporate.length > 0) {
        navigator.navigate(
          AddressTypeScreen,
          {
            personal: personal,
            corporate: corporate,
            nextParams: params,
            optionScreen: AddressOptionScreen,
          },
        )
      } else if (personal.length > 0) {
        navigator.navigate(
          AddressOptionScreen,
          params,
        )
      }
    })
}

const incrementHandler = (
  item: Meal,
  date: Moment,
  quantity: number,
  navigator: NavigationDrawerProp,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
  warningCallback: () => void,
  isLoggedIn: boolean,
  cart: CartState,
  logger?: () => void,
  category?: string,
  code?: string,
) => {
  if (!isLoggedIn) {
    navigator.navigate('Login')
  } else {
    if (isCategoryExclusive(cart, item)) {
      return warningCallback()
    }
    
    if (isCategoryQtyMax(cart, date, 3)) {
      return Toast.show({
        text: `Max. order qty for ${cart.category} is 3 items per delivery date.`,
        buttonText: '',
        duration: 3000,
      })
    }

    if (!isExclusive(cart, item)
      && isSameType(cart, 'item')
    ) {
      if (!cart.fetching) {
        if (category) {
          item['category'] = category
        }
        addToCart(
          cart,
          item,
          date,
          quantity,
          code,
          loadingCallback,
          resetCallback,
          reduxCallback,
        )
      }
    } else {
      warningCallback()
    }
  }
  if (logger) {
    logger()
  }
}

const decreaseHandler = (
  item: Meal,
  date: Moment,
  quantity: number,
  loadingCallback: (status: boolean) => void,
  resetCallback: () => void,
  reduxCallback: (data: responseData) => void,
  cart: CartState,
  logger?: () => void,
  removeCallback?: (itemId: number, date: Moment) => void,
  code?: string,
) => {
  if (!cart.fetching) {
    if (quantity <= 0) {
      if (removeCallback) {
        if (isDeliveryHasMultipleItem(cart.cartItems)) {
          removeCallback(item.id, date)
          addToCart(
            cart,
            item,
            date,
            quantity,
            code,
            loadingCallback,
            resetCallback,
            reduxCallback,
          )
          if (logger) {
            logger()
          }
        } else {
          removeCallback(item.id, date)
        }
      }
    } else {
      addToCart(
        cart,
        item,
        date,
        quantity,
        code,
        loadingCallback,
        resetCallback,
        reduxCallback,
      )
      if (logger) {
        logger()
      }
    }
  }
}

const onMaxItem = (max: number, remaining: number) => {
  let wording = 'You’ve reached our last stock for this location'
  if (remaining < max) {
    wording = 'You’ve reached our last stock for this location'
  }

  Toast.show({
    text: wording,
    buttonText: '',
    duration: 1500,
  })
}

const logEvent = (item: Meal | itemResponse, qty: number, category: string) => {
  if (qty > 1) {
    const params: Params = {
      meal_id: item.id,
      meal_name: item.name,
      meal_category_name: category,
    }
    LogEventService.logEvent('click_increase_qty', params, ['default'])
  } else {
    const params: Params = {
      meal_id: item.id,
      meal_name: item.name,
      meal_category_name: category,
      currency: 'IDR',
      price: item.sale_price,
    }
    LogEventService.logEvent('add_to_cart', params, ['default'])
  }
}

const isDeliveryHasMultipleItem = (cartItems: Delivery[]): boolean => {
  if (cartItems.length > 1) {
    return true
  }
  let isMultiple = false
  cartItems.forEach(delivery => {
    if (delivery.items.length > 1) {
      isMultiple = true
    }
  })
  return isMultiple
}

export default {
  buildSalesOrder,
  checkAllRequiredInformationValid,
  calculateItemInCart,
  setOrderTotal,
  isSameType,
  isExclusive,
  isCategoryExclusive,
  isCategoryQtyMax,
  buildSingleOrder,
  addToCart,
  validateCart,
  getUserAddress,
  applyCoupon,
  addPlaylistCart,
  addSubscriptionCart,
  getDeliveryAddress,
  incrementHandler,
  decreaseHandler,
  onMaxItem,
  logEvent,
  getItemAvailability
}
