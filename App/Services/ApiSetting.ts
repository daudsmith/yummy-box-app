import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { Platform } from 'react-native'
import { SettingItem, AppVersion } from '../Redux/SettingRedux'

export interface getSettingResponse {
  error: boolean
  data: string
}

export interface getBulkSettingResponse {
  error: boolean
  data: SettingItem[]
}

export interface checkVersionResponse {
  error: boolean
  data: AppVersion
}

const create = (baseURL: string = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 15000
  })

  const get = (key: string) => api.get<getSettingResponse>(`public/setting/get/${key}`)
  const bulk = (keys: string[]) => api.get<getBulkSettingResponse>('public/setting/get/bulk', { keys: JSON.stringify(keys) })

  const checkVersion = () => {
    api.setHeader('x-platform', Platform.OS)
    if (Platform.OS === 'ios') {
      api.setHeader('x-version', AppConfig.iOSAppVersion)
    } else {
      api.setHeader('x-version', AppConfig.AndroidAppVersion)
    }

    return api.get<checkVersionResponse>('public/setting/checkVersion')
  }

  return {
    get,
    checkVersion,
    bulk,
  }
}

export default {
  create
}
