import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'
import { Slide } from '../Redux/WelcomeScreenRedux'

export interface apiResponse {
  error: boolean;
  data: Slide[];
}

const create = (baseURL = AppConfig.apiBaseUrl) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  })

  const getWelcomeScreenSlides = () => api.get<apiResponse>('public/setting/welcome_screen')

  return {getWelcomeScreenSlides}
}

export default {
  create
}
