import { createNetworkMiddleware } from 'react-native-offline'
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers'
import { applyMiddleware, compose, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import { Persistor } from 'redux-persist/es/types'
import createSagaMiddleware from 'redux-saga'
import Config from '../Config/DebugConfig'
import ReduxPersist from '../Config/ReduxPersist'
import RehydrationServices from '../Services/RehydrationServices'
import reactotron from '../Config/ReactotronConfig'
import { Store, Reducer } from 'redux'
import { NetworkState } from 'react-native-offline/src/types'
import { InitStateNavigation } from './NavigationRedux'
import { initStateRegister } from './RegisterRedux'
import { LoginState } from './LoginRedux'
import { AddressHistory } from './AddressHistoryRedux'
import { InitCustomerCard } from './CustomerCardRedux'
import { CartState } from './V2/CartRedux'
import { AddressTypeReduxState } from './AddressTypeRedux'
import { InitMealsState } from './MealsRedux'
import { WelcomeScreen } from './WelcomeScreenRedux'
import { InitSetting } from './SettingRedux'
import { SalesOrderReduxInterface } from './SalesOrderRedux'
import { initVirtualAccount } from './CustomerVirtualAccountRedux'
import { Promotion } from './PromotionRedux'
import { HomeContentState } from './HomeContentRedux'
import { CustomerAccountReduxState } from './CustomerAccountRedux'
import { CustomerAddressReduxState } from './CustomerAddressRedux'
import { InitCreditTokenTopUp } from './CreditTokenTopUpRedux'
import { CustomerOrderState } from './CustomerOrderRedux'
import { CustomerNotificationReduxState } from './CustomerNotificationRedux'
import { MyMealPlanState } from './MyMealPlanRedux'
import { ForgotPasswordReduxState } from './ForgotPasswordRedux'
import { InitStateTheme } from './ThemeRedux'
import { StartupState } from './StartupRedux'
import { MealDetailPopUpReduxState } from './MealDetailPopUpRedux'
import { InitAutoRouting } from './AutoRoutingRedux'
import { LastUsedState } from './V2/LastUsedRedux'

// import ScreenTracking from './ScreenTrackingMiddleware'

export interface AppState {
  network?: NetworkState,
  nav?: InitStateNavigation,
  register?: initStateRegister,
  login?: LoginState,
  addressHistory?: AddressHistory,
  customerCard?: InitCustomerCard,
  cart?: CartState,
  addressType?: AddressTypeReduxState,
  meals?: InitMealsState,
  welcomeScreen?: WelcomeScreen,
  setting?: InitSetting,
  order?: SalesOrderReduxInterface,
  customerVirtualAccount?: initVirtualAccount,
  promotion?: Promotion,
  homeContent?: HomeContentState,
  customerAccount?: CustomerAccountReduxState,
  customerAddress?: CustomerAddressReduxState,
  creditTokenTopUp?: InitCreditTokenTopUp,
  customerOrder?: CustomerOrderState,
  customerNotification?: CustomerNotificationReduxState,
  myMealPlan?: MyMealPlanState,
  forgotPassword?: ForgotPasswordReduxState,
  theme?: InitStateTheme,
  startup?: StartupState,
  mealDetailPopUp?: MealDetailPopUpReduxState,
  autoRouting?: InitAutoRouting,
  lastUsed?: LastUsedState,
}

export interface storeInterface {
  store: Store<AppState>
  persistor: Persistor
}

// creates the store
export default (
  rootReducer: Reducer<AppState>, rootSaga: any): storeInterface => {
  /* ------------- Redux Configuration ------------- */

  const middleware = []
  const enhancers = []

  /* ------------- Analytics Middleware ------------- */
  // middleware.push(ScreenTracking)

  /* ------------- Saga Middleware ------------- */

  const sagaMiddleware = createSagaMiddleware()
  middleware.push(sagaMiddleware)

  const networkMiddleware = createNetworkMiddleware()
  middleware.push(networkMiddleware)

  const reactNavigationMiddleware = createReactNavigationReduxMiddleware<AppState>(
    state => state.nav)
  middleware.push(reactNavigationMiddleware)

  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware))

  /* ------------- AutoRehydrate Enhancer ------------- */

  // add the autoRehydrate enhancer
  const persistedReducer = persistReducer(ReduxPersist.storeConfig, rootReducer)

  if (Config.useReactotron) {
    enhancers.push(reactotron.createEnhancer())
  }

  const store: Store<AppState> = createStore(persistedReducer, compose(...enhancers))
  const persistor: Persistor = persistStore(store)

  // kick off root saga
  sagaMiddleware.run(rootSaga)

  // configure persistStore and check reducer version number
  if (ReduxPersist.active) {
    RehydrationServices.updateReducers(store)
  }

  return { store, persistor }
}
