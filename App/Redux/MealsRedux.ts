import { createActions, createReducer } from 'reduxsauce'
import Immutable, { ImmutableArray, ImmutableObject } from 'seamless-immutable'
import { AnyAction } from 'redux'

export interface MealsActionsCreators {
  fetchMeals: (date: string, kitchenCode: string) => AnyAction,
  fetchMealsDone: (meals: Meals[]) => AnyAction,
  fetchMealsFailed: (error: string) => AnyAction,
  fetchingMeals: () => AnyAction,
  reset: () => AnyAction,
  fetchMealDetails: (id: number, date: string) => AnyAction,
  fetchingMealDetails: () => AnyAction,
  fetchingMealDetailsDone: (meal: Meal) => AnyAction,
  fetchingMealDetailsFailed: (error: string) => AnyAction,
  fetchingTags: () => AnyAction,
  fetchingTagsDone: (tags: Tags[]) => AnyAction,
  fetchingTagsFailed: (error: string) => AnyAction,
  fetchFilteredMeals: (date: string, includeTags: boolean, excludeTags: boolean) => AnyAction,
  fetchingFilteredMeals: () => AnyAction,
  fetchingFilteredMealsDone: (meals: Meals[]) => AnyAction,
  fetchingFilteredMealsFailed: (error: string) => AnyAction,
  fetchingOffDatesDone: (offDates: object, startDate: object) => AnyAction,
  fetchingOffDatesFailed: (error: string) => AnyAction,
  setItemStock: (itemStock: ItemStock[]) => AnyAction,
  setPromotedItemStock: (itemStock: ItemStock[]) => AnyAction,
  cachingData: () => AnyAction,
  cachingDataDone: (cachingData: CachingData[]) => AnyAction,
  cachingDataFiled: (error: String) => AnyAction,
}

export interface MealsActionsTypes {
  FETCH_MEALS_DONE: 'FETCH_MEALS_DONE',
  FETCH_MEALS_FAILED: 'FETCH_MEALS_FAILED',
  FETCHING_MEALS: 'FETCHING_MEALS',
  RESET: 'RESET',
  FETCHING_MEAL_DETAILS: 'FETCHING_MEAL_DETAILS',
  FETCHING_MEAL_DETAILS_DONE: 'FETCHING_MEAL_DETAILS_DONE',
  FETCHING_MEAL_DETAILS_FAILED: 'FETCHING_MEAL_DETAILS_FAILED',
  FETCHING_TAGS: 'FETCHING_TAGS',
  FETCHING_TAGS_DONE: 'FETCHING_TAGS_DONE',
  FETCHING_TAGS_FAILED: 'FETCHING_TAGS_FAILED',
  FETCHING_FILTERED_MEALS: 'FETCHING_FILTERED_MEALS',
  FETCHING_FILTERED_MEALS_DONE: 'FETCHING_FILTERED_MEALS_DONE',
  FETCHING_FILTERED_MEALS_FAILED: 'FETCHING_FILTERED_MEALS_FAILED',
  FETCHING_OFF_DATES_DONE: 'FETCHING_OFF_DATES_DONE',
  FETCHING_OFF_DATES_FAILED: 'FETCHING_OFF_DATES_FAILED',
  FETCH_MEALS: 'FETCH_MEALS',
  FETCH_MEAL_DETAILS: 'FETCH_MEAL_DETAILS',
  FETCH_FILTERED_MEALS: 'FETCH_FILTERED_MEALS',
  SET_ITEM_STOCK: 'SET_ITEM_STOCK',
  SET_PROMOTED_ITEM_STOCK: 'SET_PROMOTED_ITEM_STOCK',
  CACHING_DATA: 'CACHING_DATA',
  CACHING_DATA_DONE: 'CACHING_DATA_DONE',
  CACHING_DATA_FAILED: 'CACHING_DATA_FAILED',
}

const { Types, Creators } = createActions <MealsActionsTypes, MealsActionsCreators>({
  fetchMeals: ['date', 'kitchenCode'],
  fetchMealsDone: ['meals'],
  fetchMealsFailed: ['error'],
  fetchingMeals: null,
  reset: null,
  fetchMealDetails: ['id', 'date'],
  fetchingMealDetails: null,
  fetchingMealDetailsDone: ['meal'],
  fetchingMealDetailsFailed: ['error'],
  fetchingTags: null,
  fetchingTagsDone: ['tags'],
  fetchingTagsFailed: ['error'],
  fetchFilteredMeals: ['date', 'includeTags', 'excludeTags'],
  fetchingFilteredMeals: [null],
  fetchingFilteredMealsDone: ['meals'],
  fetchingFilteredMealsFailed: ['error'],
  fetchingOffDatesDone: ['offDates', 'startDate'],
  fetchingOffDatesFailed: ['error'],
  setItemStock: ['itemStock'],
  setPromotedItemStock: ['itemStock'],
  cachingData: null,
  cachingDataDone: ['cachingData'],
  cachingDataFailed: ['error'],
})

export const MealsTypes = Types
export default Creators

export interface Meals {
  id: number
  name: string
  selected_date?: string,
  exclusive: boolean
  note?: string,
  item?: {
    data : Meal
  }
  items?: {
    data : Meal[]
  }
  available_dates?: Array<string>

}

export interface MealImage {
  is_catalog_image: boolean
  sort: number
  thumbs: {
    small_thumb: string
    medium_thumb: string
  }
}

export interface MealNutrition {
  id: number
  name: string
  amount: number
  unit: string
  have_child: boolean
}

export interface MealsImage {
  original: string
  small_thumb: string
  medium_thumb: string
}

export interface MealAvailability {
  id: number
  date: string
  quantity: number
  remaining_quantity: number
  sale_price: number
  base_price: number
  item_id: number
  item_name: string
  category_id: number
}

export interface MealCategory {
  id: number
  name: string
  status: string
  icon_file: string
  created_at: string
  updated_at: string
}

export interface Meal {
  id: number
  name: string
  sku: string
  date?: string
  base_price: number
  cogs?: number
  description: string
  short_description?: string
  tax_class?: string
  ingredients?: string
  sale_price: number
  daily_quantity: number
  is_available: boolean
  dinner: string
  catalog_image: MealsImage
  category: string
  daily_remaining_quantity: number
  home_sorting: number
  catalog_sorting: number
  kitchen_meal_name: string
  exclusive: boolean
  category_exclusive?: boolean
  partner?: string | null
  tags: TagsData
  images: {
    data : MealImage[]
  }
  nutrition: {
    data : MealNutrition[]
  }
  availabilities: {
    data: MealAvailability[]
  }
  categories: {
    data: MealCategory[]
  }
}

export interface Tags {
  id: number
  name: string
  icon: string
  filterable: number
  display_on_catalog: number
  filter_type: string
  description: string
  isSelected?: boolean
  children?: {
    data: Children
  }
}

export interface TagsData {
  data: Tags[]
}

export interface Children {
  filter: any;
  id: number
  name: string
  icon: string
  filterable: number
  display_on_catalog: number
  filter_type: string
  description: string
  isSelected?: boolean
}
export interface ItemStock{
  item_id: number
  quantity: number
}

interface MealsState {
  meals: ImmutableArray<Meals> | Meals[]
  stock: ImmutableObject<Stock>
  error: string
  fetching: boolean
  fetchingMealsSuccess: boolean
  details: ImmutableObject<Meal>
  fetchingDetail: boolean
  tags: ImmutableArray<Tags> | Tags[]
  offDates: string
  startDate: string,
  cachingData: ImmutableArray<CachingData> | CachingData[]
}

export interface Stock {
  main: ItemStock[]
  promoted: ItemStock[]
}

export interface CachingData{
  date: String,
  meals: ImmutableArray<Meals> | Meals[],
  tags: ImmutableArray<Tags> | Tags[],
}

export const INITIAL_STATE = Immutable<MealsState>({
  meals: [],
  error: null,
  fetching: false,
  fetchingMealsSuccess: null,
  details: null,
  fetchingDetail: true,
  tags: null,
  offDates: null,
  startDate: null,
  stock: {
    main: [],
    promoted: []
  },
  cachingData: [],
})

export type InitMealsState = typeof INITIAL_STATE


export const setItemStock: any = (state: InitMealsState, {itemStock}: {itemStock: ItemStock[]}) => {
  return {...state, stock: {...state.stock, main: itemStock}}
}

export const setPromotedItemStock: any = (state: InitMealsState, {itemStock}: {itemStock: ItemStock[]}) => {
  return {...state, stock: {...state.stock, promoted:itemStock}}
}

export const fetchMealsDone: any = (state: InitMealsState, {meals}: {meals: ImmutableArray<Meals>}): InitMealsState => {

  return {...state, meals: meals, fetching: false, fetchingMealsSuccess: true}
}
export const fetchMealsFailed: any = (state: InitMealsState, {error}: {error: string}) => {
  return {...state, fetching: false, error: error, fetchingMealsSuccess: false}
}
export const fetchingMeals: any = (state: InitMealsState): InitMealsState => {
  return {...state, fetching: true, error: null, fetchingMealsSuccess: null}
}
export const fetchingMealDetails: any = (state: InitMealsState): InitMealsState => {
  return {...state, fetchingDetail: true, error: ''}
}
export const fetchingMealDetailsDone: any = (state: InitMealsState, {meal}: {meal: ImmutableObject<Meal>}): InitMealsState => {
  return {...state, details: meal, fetchingDetail: false}
}
export const fetchingMealDetailsFailed: any = (state: InitMealsState, {error}: {error: string}): InitMealsState => {
  return {...state, fetchingDetail: false, details: null, error: error}
}

export const fetchingTags: any = (state: InitMealsState): InitMealsState => {
  return {...state, fetching: true}
}

export const fetchingTagsDone: any = (state: InitMealsState, {tags}: {tags: ImmutableArray<Tags>}): InitMealsState => {
  return {...state, tags: tags, fetching: false}
}

export const fetchingTagsFailed: any = (state: InitMealsState, {error}: {error: string}): InitMealsState => {
  return {...state, error: error, fetching: false}
}

export const fetchingFilteredMeals: any = (state: InitMealsState): InitMealsState => {
  return {...state, fetching: true}
}

export const fetchingFilteredMealsDone: any = (state: InitMealsState, {meals}: {meals: ImmutableArray<Meals>}): InitMealsState => {
  return {...state, fetching: false, meals: meals}
}

export const fetchingFilteredMealsFailed: any = (state: InitMealsState, {error}: {error: string}): InitMealsState => {
  return {...state, fetching: false, error: error}
}

export const reset: any = (): InitMealsState => INITIAL_STATE
export const fetchingOffDatesDone: any = (state: InitMealsState, {offDates, startDate}: {offDates: string, startDate: string}): InitMealsState => {
  return {...state, fetching: false, offDates, startDate}
}
export const fetchingOffDatesFailed: any = (state: InitMealsState, {error}: {error: string}): InitMealsState => {
  return {...state, fetching: false, error: error}
}

export const cachingData: any = (state: InitMealsState): InitMealsState => {
  return {...state}
}

export const cachingDataDone: any = (state: InitMealsState, {cachingData}: {cachingData: ImmutableArray<CachingData>}): InitMealsState => {
  return {...state, cachingData: cachingData, fetching: false}
}

export const cachingDataFailed: any = (state: InitMealsState, {error}: {error: string}): InitMealsState => {
  return {...state, error: error, fetching: false}
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FETCH_MEALS_DONE]: fetchMealsDone,
  [Types.FETCH_MEALS_FAILED]: fetchMealsFailed,
  [Types.FETCHING_MEALS]: fetchingMeals,
  [Types.RESET]: reset,
  [Types.FETCHING_MEAL_DETAILS]: fetchingMealDetails,
  [Types.FETCHING_MEAL_DETAILS_DONE]: fetchingMealDetailsDone,
  [Types.FETCHING_MEAL_DETAILS_FAILED]: fetchingMealDetailsFailed,
  [Types.FETCHING_TAGS]: fetchingTags,
  [Types.FETCHING_TAGS_DONE]: fetchingTagsDone,
  [Types.FETCHING_TAGS_FAILED]: fetchingTagsFailed,
  [Types.FETCHING_FILTERED_MEALS]: fetchingFilteredMeals,
  [Types.FETCHING_FILTERED_MEALS_DONE]: fetchingFilteredMealsDone,
  [Types.FETCHING_FILTERED_MEALS_FAILED]: fetchingFilteredMealsFailed,
  [Types.FETCHING_OFF_DATES_DONE]: fetchingOffDatesDone,
  [Types.FETCHING_OFF_DATES_FAILED]: fetchingOffDatesFailed,
  [Types.SET_ITEM_STOCK]: setItemStock,
  [Types.SET_PROMOTED_ITEM_STOCK]: setPromotedItemStock,

  [Types.CACHING_DATA]: cachingData,
  [Types.CACHING_DATA_DONE]: cachingDataDone,
  [Types.CACHING_DATA_FAILED]: cachingDataFailed,

  [Types.RESET]: reset
})
