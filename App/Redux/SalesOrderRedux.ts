import {
  createActions,
  createReducer,
} from 'reduxsauce'
import { AnyAction, Action } from 'redux'
import { paymentInfoData } from '../Services/XenditService'
import { PaymentOptionType } from '../Components/Payment/PaymentMethodPicker'

export interface OrderData {
  coupon?: string
  type: string
  total: orderTotalData
  payment: orderPaymentData
  deliveries: orderDeliveriesData[]
}

interface orderTotalData {
  amount: number
  tax: number
  deliveryFee: number
  discount: number
  total: number
  cashback: number
}

interface orderPaymentData {
  method: string
  info?: WalletPaymentInfo | creditCardPaymentInfo | object
  is_multiple_use?: boolean
}

interface orderDeliveriesData {
  date: string
  time: string
  landmark: string
  name: string
  phone: string
  note: string
  address: string
  latitude: number
  longitude: number
  items?: orderItemData[]
  packages?: PackagesData[]
  subscription?: SubscriptionData
  first_item?: FirstItemData
  totals: deliveryTotalData
}

export interface orderItemData {
  id: number
  name: string
  sale_price: number
  tax_class: string
  images: itemImageData
  quantity: number
  remaining_daily_quantity: number
  cutoff?: string
  cutoff_day?: number
}

export interface itemImageData {
  original: string
  small_thumb: string
  medium_thumb: string
}

interface deliveryTotalData {
  subtotal: number
  delivery_fee: number
}

export interface PackagesData {
  theme_id: number
  package_id: number
  dates: string[]
  quantity: number
  package_name: string
  cart_image: string
  subtotal?: number
  unit_price?: number
}

interface SubscriptionData {
  theme_id: number
  theme_name: string
  start_date: string
  repeat_every: Array<number>
  repeat_interval: number
  end_date: string
}

interface FirstItemData {
  item_name: string
  price: number
}

interface WalletPaymentInfo {
  info: balanceInfo
}

interface balanceInfo {
  current_balance: number
}

interface creditCardPaymentInfo {
  type: string
  ends_with: string
  status: string
  token: string
  authorization_id: string
}

export interface SalesOrderReduxCreators {
  createOrder: (order: OrderData) => AnyAction,
  resetOrder: () => AnyAction,
  createFailed: (error: string) => AnyAction,
  createSuccess: (orderNumber: string, invoiceUrl: string, checkoutUrl: string) => AnyAction,
  creatingOrder: (loadingText: string) => AnyAction,
  setNewCreditCardInfo: (newCreditCardInfo: object) => AnyAction,
  setPaymentInfo: (paymentInfo: object) => AnyAction,
  setOrderTotal: (orderTotal: number) => AnyAction,
  setSelectedPayment: (selectedPayment: string) => AnyAction,
  clearOrderSuccess: () => AnyAction,
  clearPaymentInfo: () => AnyAction,
  resetError: () => AnyAction,
}

export interface SalesOrderReduxTypes {
  CREATE_ORDER: 'CREATE_ORDER'
  RESET_ORDER: 'RESET_ORDER'
  CREATE_FAILED: 'CREATE_FAILED'
  CREATE_SUCCESS: 'CREATE_SUCCESS'
  CREATING_ORDER: 'CREATING_ORDER'
  SET_PAYMENT_INFO: 'SET_PAYMENT_INFO'
  SET_ORDER_TOTAL: 'SET_ORDER_TOTAL'
  CLEAR_ORDER_SUCCESS: 'CLEAR_ORDER_SUCCESS'
  CLEAR_PAYMENT_INFO: 'CLEAR_PAYMENT_INFO'
  SET_SELECTED_PAYMENT: 'SET_SELECTED_PAYMENT'
  SET_NEW_CREDIT_CARD_INFO: 'SET_NEW_CREDIT_CARD_INFO',
  RESET_ERROR: 'RESET_ERROR'
}

const { Types, Creators } = createActions<SalesOrderReduxTypes, SalesOrderReduxCreators>({
  createOrder: ['order'],
  resetOrder: null,
  createFailed: ['error'],
  createSuccess: [
    'orderNumber',
    'invoiceUrl',
    'checkoutUrl',
  ],
  creatingOrder: ['loadingText'],
  setNewCreditCardInfo: ['newCreditCardInfo'],
  setPaymentInfo: ['paymentInfo'],
  setOrderTotal: ['orderTotal'],
  setSelectedPayment: ['selectedPayment'],
  clearOrderSuccess: [null],
  clearPaymentInfo: null,
  resetError: null,
})

export const OrderTypes = Types
export default Creators

interface BaseSalesOrder {
  loading?: boolean
  loadingText?: string
  error?: string
  newCreditCardInfo?: paymentInfoData | null
  paymentInfo?: object | null
  selectedPayment?: PaymentOptionType
  orderTotal?: number
  orderNumber?: string
  createOrderSuccess?: boolean
  invoiceUrl?: string
  checkoutUrl?: string | null
}

export const INITIAL_STATE = <BaseSalesOrder>{
  loading: false,
  loadingText: '',
  error: '',
  newCreditCardInfo: null,
  paymentInfo: null,
  selectedPayment: '',
  orderTotal: 0,
  orderNumber: '',
  createOrderSuccess: false,
  invoiceUrl: '',
  checkoutUrl: null,
}

export type SalesOrderReduxInterface = typeof INITIAL_STATE

export const reset: any = () => INITIAL_STATE

export const resetError: any = (state: BaseSalesOrder) => {
  return {
    ...state,
    loading: false,
    error: '',
  }
}
export const failed: any = (
  state: BaseSalesOrder,
  { error }: {error: string}
): SalesOrderReduxInterface => {
  return {
    ...state,
    error,
    loading: false,
    createOrderSuccess: false,
  }
}

export const success: any = (
  state: BaseSalesOrder,
  { orderNumber, invoiceUrl, checkoutUrl }: { orderNumber: string, invoiceUrl: string, checkoutUrl: string },
): SalesOrderReduxInterface => {
  return {
    ...state,
    orderNumber,
    loading: false,
    error: '',
    createOrderSuccess: true,
    invoiceUrl,
    checkoutUrl
  }
}
export const creating: any = (
  state: BaseSalesOrder,
  { loadingText }: { loadingText: string },
): SalesOrderReduxInterface => {
  return {
    ...state,
    loading: true,
    loadingText,
    error: '',
    orderNumber: '',
    createOrderSuccess: false,
  }
}
export const setNewCreditCardInfo: any = (
  state: BaseSalesOrder,
  { newCreditCardInfo }: { newCreditCardInfo: paymentInfoData }
): SalesOrderReduxInterface => {
  return {
    ...state,
    newCreditCardInfo,
  }
}
export const setPaymentInfo: any = (
  state: BaseSalesOrder,
  { paymentInfo }: { paymentInfo: object }
): SalesOrderReduxInterface => {
  return {
    ...state,
    paymentInfo,
  }
}
export const setSelectedPayment: any = (state: BaseSalesOrder, { selectedPayment }: { selectedPayment: string }): SalesOrderReduxInterface => {
  return {
    ...state,
    selectedPayment,
  }
}
export const setOrderTotal: any = (state: BaseSalesOrder, { orderTotal }: { orderTotal: number }): SalesOrderReduxInterface => {
  return {
    ...state,
    orderTotal,
  }
}
export const clearOrderSuccess: any = (state: SalesOrderReduxInterface): SalesOrderReduxInterface => {
  return {
    ...state,
    createOrderSuccess: false,
    error: '',
  }
}
export const clearPaymentInfo: any = (state: SalesOrderReduxInterface): SalesOrderReduxInterface => {
  return {
    ...state,
    paymentInfo: null,
    selectedPayment: '',
    newCreditCardInfo: null,
  }
}

export const reducer: (state: BaseSalesOrder, action: Action) => BaseSalesOrder = createReducer(INITIAL_STATE, {
  [Types.RESET_ORDER]: reset,
  [Types.CREATE_FAILED]: failed,
  [Types.CREATE_SUCCESS]: success,
  [Types.CREATING_ORDER]: creating,
  [Types.SET_PAYMENT_INFO]: setPaymentInfo,
  [Types.SET_ORDER_TOTAL]: setOrderTotal,
  [Types.CLEAR_ORDER_SUCCESS]: clearOrderSuccess,
  [Types.CLEAR_PAYMENT_INFO]: clearPaymentInfo,
  [Types.SET_SELECTED_PAYMENT]: setSelectedPayment,
  [Types.SET_NEW_CREDIT_CARD_INFO]: setNewCreditCardInfo,
  [Types.RESET_ERROR]: resetError,
})
