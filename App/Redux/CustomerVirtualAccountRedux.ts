import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface VirtualAccountActionsCreators {
  resetVirtualAccount: () => AnyAction,
  fetchVirtualAccount: () => AnyAction,
  fetchingVirtualAccount: () => AnyAction,
  virtualAccountFetchingSuccess: (list: VirtualAccount[]) => AnyAction,
  virtualAccountFetchingFailed: (error: string) => AnyAction,
}

export interface VirtualAccountActionsTypes {
  RESET_VIRTUAL_ACCOUNT: 'RESET_VIRTUAL_ACCOUNT',
  FETCH_VIRTUAL_ACCOUNT: 'FETCH_VIRTUAL_ACCOUNT',
  FETCHING_VIRTUAL_ACCOUNT: 'FETCHING_VIRTUAL_ACCOUNT',
  VIRTUAL_ACCOUNT_FETCHING_SUCCESS: 'VIRTUAL_ACCOUNT_FETCHING_SUCCESS',
  VIRTUAL_ACCOUNT_FETCHING_FAILED: 'VIRTUAL_ACCOUNT_FETCHING_FAILED',
}

export interface VirtualAccount {
  id: number,
  external_id: string,
  account_id: number,
  account_number: string,
  bank_code: string,
  logo: string,
  merchant_code: string,
  virtual_account_callback_id: string,
  created_at: string,
  updated_at: string
}

export interface initVirtualAccount {
  fetching: boolean,
  error: string,
  list: VirtualAccount[]
}

const { Types, Creators } = createActions<VirtualAccountActionsTypes, VirtualAccountActionsCreators>({
  resetVirtualAccount: null,
  fetchVirtualAccount: null,
  fetchingVirtualAccount: null,
  virtualAccountFetchingSuccess: ['list'],
  virtualAccountFetchingFailed: ['error'],
})

export const CustomerVirtualAccountTypes = Types
export default Creators

export const INITIAL_STATE = {
  fetching: false,
  error: null,
  list: null
}

export const resetVirtualAccount: any = (): initVirtualAccount => INITIAL_STATE
export const fetchingVirtualAccount: any = (state: initVirtualAccount): initVirtualAccount => {
  return { ...state, fetching: true, error: null }
}
export const virtualAccountFetchingSuccess: any = (state: initVirtualAccount, { list }: { list: VirtualAccount[] }): initVirtualAccount => {
  return { ...state, list }
}
export const virtualAccountFetchingFailed: any = (state: initVirtualAccount, { error }: { error: string }): initVirtualAccount => {
  return { ...state, error }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_VIRTUAL_ACCOUNT]: resetVirtualAccount,
  [Types.FETCHING_VIRTUAL_ACCOUNT]: fetchingVirtualAccount,
  [Types.VIRTUAL_ACCOUNT_FETCHING_SUCCESS]: virtualAccountFetchingSuccess,
  [Types.VIRTUAL_ACCOUNT_FETCHING_FAILED]: virtualAccountFetchingFailed,
})
