import Config from 'react-native-config'
import {
  createActions,
  createReducer,
} from 'reduxsauce'
import Immutable, { ImmutableArray } from 'seamless-immutable'
import { AnyAction } from 'redux'

export interface WelcomeScreenActionsCreators {
  fetchSlides: () => AnyAction
  fetchingSlides: () => AnyAction
  fetchSlidesDone: (slides: Slide[]) => AnyAction
  fetchSlidesFailed: () => AnyAction
  skipWelcomeScreen: () => AnyAction
}

export interface WelcomeScreenActionsTypes {
  FETCH_SLIDES: 'FETCH_SLIDES'
  FETCHING_SLIDES: 'FETCHING_SLIDES'
  FETCH_SLIDES_DONE: 'FETCH_SLIDES_DONE'
  FETCH_SLIDES_FAILED: 'FETCH_SLIDES_FAILED'
  SKIP_WELCOME_SCREEN: 'SKIP_WELCOME_SCREEN'
}

const { Types, Creators } = createActions<WelcomeScreenActionsTypes, WelcomeScreenActionsCreators>({
  fetchSlides: null,
  fetchingSlides: null,
  fetchSlidesDone: ['slides'],
  fetchSlidesFailed: null,
  skipWelcomeScreen: null,
})

export const WelcomeScreenTypes = Types
export default Creators

export interface Slide {
  image: string
  title: string
  content: string
}

interface BaseWelcomeScreen {
  slides?: ImmutableArray<Slide> | []
  fetching: boolean
  hasViewed: boolean
}

export const INITIAL_STATE = Immutable<BaseWelcomeScreen>({
  slides: [],
  fetching: false,
  hasViewed: Config.IS_TEST !== undefined,
})

export type WelcomeScreen = typeof INITIAL_STATE

export const fetchingSlides: any = (state: WelcomeScreen) => {
  return {
    ...state,
    fetching: true,
  }
}

export const fetchSlidesDone: any = (state: WelcomeScreen, { slides }: { slides: ImmutableArray<Slide> }) => {
  return {
    ...state,
    fetching: false,
    hasViewed: true,
    slides: slides,
  }
}

export const fetchSlidesFailed: any = (state: WelcomeScreen) => {
  return {
    ...state,
    fetching: false,
    hasViewed: true,
  }
}

export const skipWelcomeScreen: any = (state: WelcomeScreen) => {
  return {
    ...state,
    hasViewed: true,
  }
}

export const reducer: (state: WelcomeScreen, action: any) => WelcomeScreen = createReducer(INITIAL_STATE, {
  [Types.FETCHING_SLIDES]: fetchingSlides,
  [Types.FETCH_SLIDES_DONE]: fetchSlidesDone,
  [Types.FETCH_SLIDES_FAILED]: fetchSlidesFailed,
  [Types.SKIP_WELCOME_SCREEN]: skipWelcomeScreen,
})
