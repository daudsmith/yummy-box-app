import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'
import { itemImageData } from './SalesOrderRedux'
import { Meals } from './MealsRedux'
import { Tag } from '../Services/LocaleFormatter'

export interface HomeContentActionsCreators {
  resetAll: () => AnyAction,
  fetchPromote: (date: string) => AnyAction,
  fetchingPromote: () => AnyAction,
  fetchingPromoteSuccess: (promote: Meals) => AnyAction,
  fetchingPromoteFailed: (error: string) => AnyAction,
  resetItems: () => AnyAction,
  resetThemes: () => AnyAction,
  fetchItems: () => AnyAction,
  fetchingItems: () => AnyAction,
  fetchingItemsSuccess: (items: Item[]) => AnyAction,
  fetchingItemsFailed: (error: string) => AnyAction,
  fetchThemes: () => AnyAction,
  fetchingThemes: () => AnyAction,
  fetchingThemesSuccess: (themes: Themes[]) => AnyAction,
  fetchingThemesFailed: (error: string) => AnyAction,
  fetchCorporateThemes: () => AnyAction,
  fetchingCorporateThemes: () => AnyAction,
  fetchingCorporateThemesSuccess: (themes: CorporateThemes[]) => AnyAction,
  fetchingCorporateThemesFailed: (error: string) => AnyAction,
  fetchFeaturedThemeDetail: () => AnyAction,
  fetchingFeaturedThemeDetail: () => AnyAction,
  fetchingFeaturedThemeDetailSuccess: (featuredThemeDetail: ThemeDetail) => AnyAction,
  fetchFeaturedThemeItems: () => AnyAction,
  fetchingFeaturedThemeItems: () => AnyAction,
  fetchingFeaturedThemeItemsSuccess: (featuredThemeItems: ThemeItems[]) => AnyAction,
  fetchingFeaturedThemeItemsFailed: (error: string) => AnyAction
}

export interface HomeContentActionsTypes {
  RESET_ALL: 'RESET_ALL',
  FETCH_PROMOTE: 'FETCH_PROMOTE'
  FETCHING_PROMOTE: 'FETCHING_PROMOTE'
  FETCHING_PROMOTE_SUCCESS: 'FETCHING_PROMOTE_SUCCESS'
  FETCHING_PROMOTE_FAILED: 'FETCHING_PROMOTE_FAILED'
  RESET_ITEMS: 'RESET_ITEMS',
  RESET_THEMES: 'RESET_THEMES',
  FETCH_ITEMS: 'FETCH_ITEMS'
  FETCHING_ITEMS: 'FETCHING_ITEMS',
  FETCHING_ITEMS_SUCCESS: 'FETCHING_ITEMS_SUCCESS',
  FETCHING_ITEMS_FAILED: 'FETCHING_ITEMS_FAILED',
  FETCH_THEMES: 'FETCH_THEMES'
  FETCHING_THEMES: 'FETCHING_THEMES',
  FETCHING_THEMES_SUCCESS: 'FETCHING_THEMES_SUCCESS',
  FETCHING_THEMES_FAILED: 'FETCHING_THEMES_FAILED',
  FETCH_CORPORATE_THEMES: 'FETCH_CORPORATE_THEMES'
  FETCHING_CORPORATE_THEMES: 'FETCHING_CORPORATE_THEMES',
  FETCHING_CORPORATE_THEMES_SUCCESS: 'FETCHING_CORPORATE_THEMES_SUCCESS',
  FETCHING_CORPORATE_THEMES_FAILED: 'FETCHING_CORPORATE_THEMES_FAILED',
  FETCH_FEATURED_THEME_DETAIL: 'FETCH_FEATURED_THEME_DETAIL'
  FETCHING_FEATURED_THEME_DETAIL: 'FETCHING_FEATURED_THEME_DETAIL',
  FETCHING_FEATURED_THEME_DETAIL_SUCCESS: 'FETCHING_FEATURED_THEME_DETAIL_SUCCESS',
  FETCH_FEATURED_THEME_ITEMS: 'FETCH_FEATURED_THEME_ITEMS'
  FETCHING_FEATURED_THEME_ITEMS: 'FETCHING_FEATURED_THEME_ITEMS',
  FETCHING_FEATURED_THEME_ITEMS_SUCCESS: 'FETCHING_FEATURED_THEME_ITEMS_SUCCESS',
  FETCHING_FEATURED_THEME_ITEMS_FAILED: 'FETCHING_FEATURED_THEME_ITEMS_FAILED'
}

const { Types, Creators } = createActions <HomeContentActionsTypes, HomeContentActionsCreators>({
  resetAll: null,
  fetchPromote: ['date'],
  fetchingPromote: null,
  fetchingPromoteSuccess: ['promote'],
  fetchingPromoteFailed: ['error'],
  resetItems: null,
  resetThemes: null,
  fetchItems: null,
  fetchingItems: null,
  fetchingItemsSuccess: ['items'],
  fetchingItemsFailed: ['error'],
  fetchThemes: null,
  fetchingThemes: null,
  fetchingThemesSuccess: ['themes'],
  fetchingThemesFailed: ['error'],
  fetchCorporateThemes: null,
  fetchingCorporateThemes: null,
  fetchingCorporateThemesSuccess: ['themes'],
  fetchingCorporateThemesFailed: ['error'],
  fetchFeaturedThemeDetail: null,
  fetchingFeaturedThemeDetail: null,
  fetchingFeaturedThemeDetailSuccess: ['featuredThemeDetail'],
  fetchFeaturedThemeItems: null,
  fetchingFeaturedThemeItems: null,
  fetchingFeaturedThemeItemsSuccess: ['featuredThemeItems'],
  fetchingFeaturedThemeItemsFailed: ['error']
})

export const HomeContentTypes = Types
export default Creators

export interface Themes {
  id: number
  created_at: string
  updated_at: string
  name: string
  tagline: string
  description: string
  status: number
  catalog_image: string
  detail_image: string
  cart_image: string
  customer_domains: string
  sort: number
  delivery_start: string
  delivery_end: string
  display_start: string
  display_end: string
  status_text: string
  theme_periods: ThemePeriods[]
}

export interface ThemePeriods {
  id: number
  created_at: string
  updated_at: string
  theme_id: number
  name: string
  period: string
  tag_line: string
  sku: string
  base_price: number
  sell_price: number
}

export interface CorporateThemes {
  id: number
  created_at: string
  updated_at: string
  name: string
  tagline: string
  description: string
  status: number
  catalog_image: string
  detail_image: string
  cart_image: string
  customer_domains: string
  sort: number
  delivery_start: string
  delivery_end: string
  display_start: string
  display_end: string
  status_text: string
}

export interface ThemeDetail {
  id: number
  created_at: string
  updated_at: string
  name: string
  tagline: string
  description: string
  status: number
  catalog_image: string
  detail_image: string
  cart_image: string
  customer_domains: string
  sort: number
  delivery_start: string
  delivery_end: string
  display_start: string
  display_end: string
  status_text: string
  theme_periods?: ThemePeriods[]
  delivery_period: object
  items: Item[]
  max_subscription: number
  available_dates: Array<string>
  extend_dates: Array<string>
}

export interface ThemeItems {
  id: number
  theme_id?: number
  item_id: number
  date: string
  name: string
  images: {
    original: string
    small_thumb: string
    medium_thumb: string
  }
  short_description: string
  description: string
  base_price: number
  sale_price: number
}

export interface Item {
  id: number
  theme_item_id: number
  name: string
  date: string
  formatted_date?: string
  sku: string
  base_price: number
  catalog_image?: itemImageData
  cogs?: number
  description: string
  short_description: string
  tax_class?: string
  ingredients?: string
  sale_price: number
  daily_quantity: number
  is_available: boolean
  nutrition?: any
  tags?: Tag
}

export interface HomeContentState {
  promote: Meals,
  promoteError: string,
  fetchingPromote: boolean,
  fetchingPromoteSuccess: boolean,
  items: Item[]
  itemsError: string
  fetchingItems: boolean
  fetchingItemsSuccess: Item[]
  themes: Themes[]
  themesError: string
  fetchingThemes: boolean
  fetchingThemesSuccess: boolean
  corporateThemes: CorporateThemes[]
  corporateThemesError: string
  fetchingCorporateThemes: boolean
  fetchingCorporateThemesError: string
  featuredThemeDetail: ThemeDetail
  featuredThemeItems: ThemeItems[]
  fetchingFeaturedThemeDetail: boolean
  fetchingFeaturedThemeDetailSuccess: boolean
  fetchingFeaturedThemeItems: boolean
  fetchingFeaturedThemeItemsSuccess: boolean
  featuredThemeItemsError: string
}

export const INITIAL_STATE: HomeContentState = {
  promote: null,
  promoteError: null,
  fetchingPromote: false,
  fetchingPromoteSuccess: false,
  items: null,
  itemsError: null,
  fetchingItems: false,
  fetchingItemsSuccess: null,
  themes: null,
  themesError: null,
  fetchingThemes: false,
  fetchingThemesSuccess: false,
  corporateThemes: null,
  corporateThemesError: null,
  fetchingCorporateThemes: false,
  fetchingCorporateThemesError: null,
  featuredThemeDetail: null,
  featuredThemeItems: null,
  fetchingFeaturedThemeDetail: false,
  fetchingFeaturedThemeDetailSuccess: false,
  fetchingFeaturedThemeItems: false,
  fetchingFeaturedThemeItemsSuccess: false,
  featuredThemeItemsError: null,
}

export const resetAll: any = (): HomeContentState => INITIAL_STATE
export const resetItems: any = (state: HomeContentState): HomeContentState => {
  return {...state, items: null, itemsError: null, fetchingItems: false}
}
export const resetThemes: any = (state: HomeContentState): HomeContentState => {
  return {...state, themes: null, themesError: null, fetchingThemes: false}
}

export const fetchingItems: any = (state: HomeContentState): HomeContentState => {
  return {...state, fetchingItems: true, itemsError: null, items: null}
}
export const fetchingItemsSuccess: any = (state: HomeContentState, {items}: {items: Item[]}): HomeContentState => {
  return {...state, fetchingItems: false, itemsError: null, items}
}
export const fetchingItemsFailed: any = (state: HomeContentState, {error}: {error: string}): HomeContentState => {
  return {...state, fetchingItems: false, itemsError: error, items: null}
}

export const fetchingThemes: any = (state: HomeContentState): HomeContentState => {
  return {
    ...state,
    fetchingThemes: true,
    fetchingThemesSuccess: false,
    themesError: null,
  }
}
export const fetchingThemesSuccess: any = (state: HomeContentState, {themes}: {themes: Themes[]}): HomeContentState => {
  return {
    ...state,
    fetchingThemes: false,
    fetchingThemesSuccess: true,
    themesError: null,
    themes,
  }
}
export const fetchingThemesFailed: any = (state: HomeContentState): HomeContentState => {
  return {
    ...state,
    fetchingThemes: false,
    fetchingThemesSuccess: false,
    themesError: null
  }
}

export const fetchingCorporateThemes: any = (state: HomeContentState): HomeContentState => {
  return {...state, fetchingCorporateThemes: true, corporateThemes: null, corporateThemesError: null}
}
export const fetchingCorporateThemesSuccess: any = (state: HomeContentState, {themes}: {themes: Themes[]}): HomeContentState => {
  return {...state, fetchingCorporateThemes: false, corporateThemesError: null, corporateThemes: themes}
}
export const fetchingCorporateThemesFailed: any = (state: HomeContentState): HomeContentState => {
  return {...state, fetchingCorporateThemes: false, corporateThemes: null, corporateThemesError: null}
}

export const fetchingFeaturedThemeDetail: any = (state: HomeContentState): HomeContentState => {
  return {
    ...state,
    fetchingFeaturedThemeDetail: true,
    featuredThemeDetail: null,
    fetchingFeaturedThemeDetailSuccess: false,
  }
}

export const fetchingFeaturedThemeDetailSuccess: any = (state: HomeContentState, {featuredThemeDetail}: {featuredThemeDetail: ThemeDetail}): HomeContentState => {
  return {
    ...state,
    fetchingFeaturedThemeDetail: false,
    featuredThemeDetail,
    fetchingFeaturedThemeDetailSuccess: true
  }
}

export const fetchingFeaturedThemeItems: any = (state: HomeContentState): HomeContentState => {
  return {
    ...state,
    fetchingFeaturedThemeItems: true,
    fetchingFeaturedThemeItemsSuccess: false,
    featuredThemeItemsError: null,
  }
}

export const fetchingFeaturedThemeItemsSuccess: any = (state: HomeContentState, {featuredThemeItems}: {featuredThemeItems: ThemeItems[]}): HomeContentState => {
  return {
    ...state,
    fetchingFeaturedThemeItems: false,
    featuredThemeItems,
    fetchingFeaturedThemeItemsSuccess: true,
    featuredThemeItemsError: null,
  }
}

export const fetchingFeaturedThemeItemsFailed: any = (state: HomeContentState, {error}: {error: string}): HomeContentState => {
  return {
    ...state,
    fetchingFeaturedThemeItems: false,
    fetchingFeaturedThemeItemsSuccess: false,
    featuredThemeItemsError: error,
  }
}

export const fetchingPromote: any = (state: HomeContentState) => {
  return { ...state, fetchingPromote: true, promoteError: null, fetchingPromoteSuccess: false }
}
export const fetchingPromoteSuccess: any = (state: HomeContentState, { promote }: { promote: Meals }) => {
  return {...state, promote, fetchingPromote: false, promoteError: null, fetchingPromoteSuccess: true}
}
export const fetchingPromoteFailed: any = (state: HomeContentState, {promoteError}: { promoteError: AnyAction}) => {
  return {...state, fetchingPromote: false, fetchingPromoteSuccess: false, promoteError, promote: null}
}


export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_ALL]: resetAll,
  [Types.FETCHING_PROMOTE]: fetchingPromote,
  [Types.FETCHING_PROMOTE_SUCCESS]: fetchingPromoteSuccess,
  [Types.FETCHING_PROMOTE_FAILED]: fetchingPromoteFailed,
  [Types.RESET_ITEMS]: resetItems,
  [Types.RESET_THEMES]: resetThemes,
  [Types.FETCHING_ITEMS]: fetchingItems,
  [Types.FETCHING_ITEMS_SUCCESS]: fetchingItemsSuccess,
  [Types.FETCHING_ITEMS_FAILED]: fetchingItemsFailed,
  [Types.FETCHING_THEMES]: fetchingThemes,
  [Types.FETCHING_THEMES_SUCCESS]: fetchingThemesSuccess,
  [Types.FETCHING_THEMES_FAILED]: fetchingThemesFailed,
  [Types.FETCHING_CORPORATE_THEMES]: fetchingCorporateThemes,
  [Types.FETCHING_CORPORATE_THEMES_SUCCESS]: fetchingCorporateThemesSuccess,
  [Types.FETCHING_CORPORATE_THEMES_FAILED]: fetchingCorporateThemesFailed,
  [Types.FETCHING_FEATURED_THEME_DETAIL]: fetchingFeaturedThemeDetail,
  [Types.FETCHING_FEATURED_THEME_DETAIL_SUCCESS]: fetchingFeaturedThemeDetailSuccess,
  [Types.FETCHING_FEATURED_THEME_ITEMS]: fetchingFeaturedThemeItems,
  [Types.FETCHING_FEATURED_THEME_ITEMS_SUCCESS]: fetchingFeaturedThemeItemsSuccess,
  [Types.FETCHING_FEATURED_THEME_ITEMS_FAILED]: fetchingFeaturedThemeItemsFailed
})
