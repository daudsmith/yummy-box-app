import {
  createActions,
  createReducer,
} from 'reduxsauce'
import { AnyAction } from 'redux'

export interface CustomerNotificationReduxCreators {
  fetchSlides: () => AnyAction
  resetCustomerNotification: () => AnyAction
  fetchUnreadNotifications: () => AnyAction
  fetchTotalUnreadNotifications: () => AnyAction
  fetchingUnreadNotifications: () => AnyAction
  fetchingUnreadNotificationsDone: (unread: number) => AnyAction
  fetchNotificationList: (silent: boolean) => AnyAction
  fetchingNotificationList: () => AnyAction
  fetchingNotificationListDone: (notificationList: Notification[]) => AnyAction
  fetchingNotificationListFailed: (notificationListError: string) => AnyAction
  markNotificationsToRead: () => AnyAction
  fetchingNotificationGetReadById: (notificationId: number) => AnyAction
  fetchingNotificationGetUnreadById: (notificationId: number) => AnyAction
  fetchingNotificationGetUnreadByIdDone: (notificationList: Notification[]) => AnyAction
  fetchingNotificationGetReadByIdDone: (notificationList: Notification[]) => AnyAction
  deleteSingleNotification: (notificationId: number) => AnyAction
  singleNotificationDeleted: (notificationList: Notification[], unread: number) => AnyAction
  deleteAllNotifications: () => AnyAction
  allNotificationsDeleted: () => AnyAction
  deletingNotification: () => AnyAction
  deleteNotificationFailed: (error: string) => AnyAction
  updatingReadStatus: (notificationId: number) => AnyAction
  updatingUnreadStatus: (notificationId: number) => AnyAction
  markNotificationsToReadFailed: (error: string) => AnyAction
}

export interface CustomerNotificationReduxTypes {
  RESET_CUSTOMER_NOTIFICATION: 'RESET_CUSTOMER_NOTIFICATION'
  FETCH_UNREAD_NOTIFICATIONS: 'FETCH_UNREAD_NOTIFICATIONS'
  FETCH_TOTAL_UNREAD_NOTIFICATIONS: 'FETCH_TOTAL_UNREAD_NOTIFICATIONS'
  FETCHING_UNREAD_NOTIFICATIONS: 'FETCHING_UNREAD_NOTIFICATIONS'
  FETCHING_UNREAD_NOTIFICATIONS_DONE: 'FETCHING_UNREAD_NOTIFICATIONS_DONE'
  FETCH_NOTIFICATION_LIST: 'FETCH_NOTIFICATION_LIST'
  FETCHING_NOTIFICATION_LIST: 'FETCHING_NOTIFICATION_LIST'
  FETCHING_NOTIFICATION_LIST_DONE: 'FETCHING_NOTIFICATION_LIST_DONE'
  FETCHING_NOTIFICATION_LIST_FAILED: 'FETCHING_NOTIFICATION_LIST_FAILED'
  MARK_NOTIFICATIONS_TO_READ: 'MARK_NOTIFICATIONS_TO_READ'
  FETCHING_NOTIFICATION_GET_READ_BY_ID: 'FETCHING_NOTIFICATION_GET_READ_BY_ID'
  FETCHING_NOTIFICATION_GET_UNREAD_BY_ID: 'FETCHING_NOTIFICATION_GET_UNREAD_BY_ID'
  FETCHING_NOTIFICATION_GET_UNREAD_BY_ID_DONE: 'FETCHING_NOTIFICATION_GET_UNREAD_BY_ID_DONE'
  FETCHING_NOTIFICATION_GET_READ_BY_ID_DONE: 'FETCHING_NOTIFICATION_GET_READ_BY_ID_DONE'
  DELETE_SINGLE_NOTIFICATION: 'DELETE_SINGLE_NOTIFICATION'
  SINGLE_NOTIFICATION_DELETED: 'SINGLE_NOTIFICATION_DELETED'
  DELETE_ALL_NOTIFICATIONS: 'DELETE_ALL_NOTIFICATIONS'
  ALL_NOTIFICATIONS_DELETED: 'ALL_NOTIFICATIONS_DELETED'
  DELETING_NOTIFICATION: 'DELETING_NOTIFICATION'
  DELETE_NOTIFICATION_FAILED: 'DELETE_NOTIFICATION_FAILED'
  UPDATING_READ_STATUS: 'UPDATING_READ_STATUS'
  UPDATING_UNREAD_STATUS: 'UPDATING_UNREAD_STATUS'
  MARK_NOTIFICATIONS_TO_READ_FAILED: 'MARK_NOTIFICATIONS_TO_READ_FAILED'
}

export interface Notification {
  id: number
  created_at: string
  creation_diff: string
  type: string
  content: string
  read: boolean
  invoice_url: string
}

const { Types, Creators } = createActions<CustomerNotificationReduxTypes, CustomerNotificationReduxCreators>({
  resetCustomerNotification: null,
  fetchUnreadNotifications: null,
  fetchTotalUnreadNotifications: null,
  fetchingUnreadNotifications: null,
  fetchingUnreadNotificationsDone: ['unread'],
  fetchNotificationList: ['silent'],
  fetchingNotificationList: null,
  fetchingNotificationListDone: ['notificationList'],
  fetchingNotificationListFailed: ['notificationListError'],
  markNotificationsToRead: null,
  fetchingNotificationGetReadById: ['notificationId'],
  fetchingNotificationGetUnreadById: ['notificationId'],
  fetchingNotificationGetUnreadByIdDone: ['notificationList'],
  fetchingNotificationGetReadByIdDone: ['notificationList'],
  deleteSingleNotification: ['notificationId'],
  singleNotificationDeleted: [
    'notificationList',
    'unread',
  ],
  deleteAllNotifications: null,
  allNotificationsDeleted: null,
  deletingNotification: null,
  deleteNotificationFailed: ['error'],
  updatingReadStatus: ['notificationId'],
  updatingUnreadStatus: ['notificationId'],
  markNotificationsToReadFailed: ['error'],
})

export const CustomerNotificationTypes = Types
export default Creators

export interface CustomerNotificationReduxState {
  unread: number
  fetchingUnread: boolean
  notificationList: Notification[]
  fetchingNotificationList: boolean
  notificationListError: string
  deleting: boolean
  deleteNotificationFailed: string
}

export const INITIAL_STATE: CustomerNotificationReduxState = {
  unread: 0,
  fetchingUnread: false,
  notificationList: [],
  fetchingNotificationList: false,
  notificationListError: '',
  deleting: false,
  deleteNotificationFailed: null,
}

export const resetCustomerNotification: any = () => INITIAL_STATE
export const fetchingUnreadNotifications: any = (state: CustomerNotificationReduxState): CustomerNotificationReduxState => {
  return {
    ...state,
    fetchingUnread: true,
  }
}
export const fetchingUnreadNotificationsDone: any = (
  state: CustomerNotificationReduxState,
  { unread }: { unread: number },
): CustomerNotificationReduxState => {
  return {
    ...state,
    unread: unread,
    fetchingUnread: true,
  }
}
export const fetchingNotificationList: any = (state: CustomerNotificationReduxState): CustomerNotificationReduxState => {
  return {
    ...state,
    fetchingNotificationList: true,
    notificationListError: '',
    notificationList: [],
  }
}
export const fetchingNotificationListDone: any = (
  state: CustomerNotificationReduxState,
  { notificationList }: { notificationList: Notification[] },
): CustomerNotificationReduxState => {
  return {
    ...state,
    fetchingNotificationList: false,
    notificationListError: '',
    notificationList,
  }
}
export const fetchingNotificationListFailed: any = (
  state: CustomerNotificationReduxState,
  { notificationListError }: { notificationListError: string },
): CustomerNotificationReduxState => {
  return {
    ...state,
    fetchingNotificationList: false,
    notificationListError,
    notificationList: [],
  }
}
export const fetchingNotificationGetUnreadByIdDone: any = (
  state: CustomerNotificationReduxState,
  { notificationList }: { notificationList: Notification[] },
): CustomerNotificationReduxState => {
  return {
    ...state,
    notificationList,
  }
}
export const fetchingNotificationGetReadByIdDone: any = (
  state: CustomerNotificationReduxState,
  { notificationList }: { notificationList: Notification[] },
): CustomerNotificationReduxState => {
  return {
    ...state,
    notificationList,
  }
}
export const deletingNotification: any = (state: CustomerNotificationReduxState): CustomerNotificationReduxState => {
  return {
    ...state,
    deleting: true,
    deleteNotificationFailed: null,
  }
}
export const singleNotificationDeleted: any = (
  state: CustomerNotificationReduxState,
  { notificationList, unread }: { notificationList: Notification[], unread: number },
): CustomerNotificationReduxState => {
  return {
    ...state,
    deleting: false,
    notificationList,
    unread,
  }
}
export const allNotificationsDeleted: any = (state: CustomerNotificationReduxState): CustomerNotificationReduxState => {
  return {
    ...state,
    deleting: false,
    notificationList: [],
    unread: 0,
  }
}
export const deleteNotificationFailed: any = (
  state: CustomerNotificationReduxState,
  { error }: { error: string },
): CustomerNotificationReduxState => {
  return {
    ...state,
    deleting: false,
    deleteNotificationFailed: error,
  }
}
export const updatingReadStatus: any = (
  state: CustomerNotificationReduxState,
  { notificationId }: { notificationId: number },
): CustomerNotificationReduxState => {
  let { notificationList, unread } = state
  let item = notificationList.find(obj => obj.id === notificationId)
  let key = notificationList.findIndex(obj => obj.id === notificationId)
  item['read'] = true
  notificationList[key] = item
  unread -= 1
  return {
    ...state,
    notificationList,
    unread,
  }
}
export const updatingUnreadStatus: any = (
  state: CustomerNotificationReduxState,
  { notificationId }: { notificationId: number },
): CustomerNotificationReduxState => {
  let { notificationList, unread } = state
  let item = notificationList.find(obj => obj.id === notificationId)
  let key = notificationList.findIndex(obj => obj.id === notificationId)
  item['read'] = false
  notificationList[key] = item
  unread += 1
  return {
    ...state,
    notificationList,
    unread,
  }
}

export const markNotificationsToReadFailed: any = (
  state: CustomerNotificationReduxState,
  { error }: { error: string },
): CustomerNotificationReduxState => {
  return {
    ...state,
    notificationListError: error,
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_CUSTOMER_NOTIFICATION]: resetCustomerNotification,
  [Types.FETCHING_UNREAD_NOTIFICATIONS]: fetchingUnreadNotifications,
  [Types.FETCHING_UNREAD_NOTIFICATIONS_DONE]: fetchingUnreadNotificationsDone,
  [Types.FETCHING_NOTIFICATION_LIST]: fetchingNotificationList,
  [Types.FETCHING_NOTIFICATION_LIST_DONE]: fetchingNotificationListDone,
  [Types.FETCHING_NOTIFICATION_LIST_FAILED]: fetchingNotificationListFailed,
  [Types.FETCHING_NOTIFICATION_GET_UNREAD_BY_ID_DONE]: fetchingNotificationGetUnreadByIdDone,
  [Types.FETCHING_NOTIFICATION_GET_READ_BY_ID_DONE]: fetchingNotificationGetReadByIdDone,
  [Types.SINGLE_NOTIFICATION_DELETED]: singleNotificationDeleted,
  [Types.ALL_NOTIFICATIONS_DELETED]: allNotificationsDeleted,
  [Types.DELETING_NOTIFICATION]: deletingNotification,
  [Types.DELETE_NOTIFICATION_FAILED]: deleteNotificationFailed,
  [Types.UPDATING_READ_STATUS]: updatingReadStatus,
  [Types.UPDATING_UNREAD_STATUS]: updatingUnreadStatus,
  [Types.MARK_NOTIFICATIONS_TO_READ_FAILED]: markNotificationsToReadFailed,
})
