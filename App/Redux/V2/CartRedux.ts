import _ from 'lodash'
import {
  createActions,
  createReducer,
} from 'reduxsauce'
import { AnyAction } from 'redux'
import {
  CartItemPackage,
  CartItemSubscription,
  Total,
} from '../CartRedux'
import {
  itemResponse,
  responseData,
} from '../../Services/V2/ApiCart'
import { Moment } from 'moment'

export interface CartActionsCreators {
  addToCart: (cart: responseData) => AnyAction,
  updateTime: (time: string) => AnyAction,
  updateAddress: (address: DeliveryAddress) => AnyAction,
  resetCart: () => AnyAction,
  loading: (status: boolean) => AnyAction,
  updateKitchen: (kitchenCode: string) => AnyAction,
  removeFromCart: (itemId: number, date: Moment) => AnyAction,
  clearCoupon: () => AnyAction,
}

export interface CartActionsTypes {
  ADD_TO_CART: 'ADD_TO_CART',
  UPDATE_TIME: 'UPDATE_TIME',
  UPDATE_ADDRESS: 'UPDATE_ADDRESS',
  UPDATE_KITCHEN: 'UPDATE_KITCHEN',
  RESET_CART: 'RESET_CART',
  LOADING: 'LOADING',
  REMOVE_FROM_CART: 'REMOVE_FROM_CART',
  CLEAR_COUPON: 'CLEAR_COUPON',
}

const { Types, Creators } = createActions<CartActionsTypes, CartActionsCreators>({
  addToCart: ['cart'],
  updateTime: [
    'time',
  ],
  updateAddress: ['address'],
  updateKitchen: ['kitchenCode'],
  resetCart: null,
  loading: ['status'],
  removeFromCart: [
    'itemId',
    'date',
  ],
  clearCoupon: null,
})

export const CartTypes = Types
export default Creators

export interface DeliveryTotal {
  subtotal: number,
  deliveryFee: number,
}

export interface DeliveryAddress {
  name: string
  phone?: string
  landmark? : string
  address: string
  label?: string
  latitude: number
  longitude: number
  note?: string
}

export interface Delivery {
  date: string,
  deliveryTime?: string,
  deliveryAddress?: DeliveryAddress,
  items?: itemResponse[],
  packages?: CartItemPackage[],
  totals?: DeliveryTotal,
  subscription?: CartItemSubscription,
  first_item?: {
    item_name: string
    price: number
  },
  kitchen_code?: string,
}

export interface PromotionInterface {
  valid: boolean,
  message: string,
  data: string,
}

export interface BaseCart {
  cartItems: Delivery[]
  totals?: Total
  type: string
  dinner: boolean
  lunch: boolean
  category?: string
  category_exclusive?: boolean
  exclusive: boolean
  cutoff?: string
  cutDay?: number,
  partner?: string,
  coupon?: string,
  fetching?: boolean,
  valid?: boolean,
  promotion?: PromotionInterface
}

export const INITIAL_STATE = <BaseCart>({
  cartItems: [],
  totals: {},
  type: '',
  dinner: false,
  lunch: false,
  category: '',
  category_exclusive: false,
  exclusive: false,
  cutoff: '',
  cutDay: 0,
  partner: '',
  coupon: '',
  fetching: false,
  valid: true,
})

export type CartState = typeof INITIAL_STATE

export const addToCart: any = (state: CartState, { cart }: { cart: responseData }) => {
  const deliveries = cart.deliveries
    ? cart.deliveries
    : (cart.cartItems
      ? cart.cartItems
      : [])
  return {
    ...state,
    type: cart.type,
    cartItems: deliveries,
    totals: cart.totals,
    lunch: cart.lunch,
    dinner: cart.dinner,
    partner: cart.partner,
    exclusive: cart.exclusive,
    category: cart.category,
    category_exclusive: cart.category_exclusive,
    promotion: !_.isEmpty(cart.promotion) && cart.promotion,
    coupon: cart.promotion && cart.promotion.coupon
      ? cart.promotion.coupon
      : '',
    valid: cart.valid,
  }
}

export const updateAddress: any = (state: CartState, { address }: { address: DeliveryAddress }) => {  
  let newDeliveries: Delivery[] = []
  state.cartItems.forEach(delivery => {
    newDeliveries.push({
      ...delivery,
      deliveryAddress: address,
    })

  })
  return {
    ...state,
    cartItems: newDeliveries,
  }
}


export const updateKitchen: any = (state: CartState, { kitchenCode }: { kitchenCode: string }) => {  
  let newDeliveries: Delivery[] = []
  state.cartItems.forEach(delivery => {
    newDeliveries.push({
      ...delivery,
      kitchen_code: kitchenCode,
    })

  })

  return {
    ...state,
    cartItems: newDeliveries,
  }
}

export const updateTime: any = (state: CartState, { time }: { time: string }) => {
  let newDeliveries: Delivery[] = []
  const deliveries = state.cartItems
  deliveries.forEach(delivery => {

    newDeliveries.push({
      ...delivery,
      deliveryTime: time,
    })

  })
  return {
    ...state,
    cartItems: newDeliveries,
  }
}

export const resetCart: any = (): CartState => INITIAL_STATE

export const loading: any = (state: CartState, { status }: { status: boolean }) => {
  return {
    ...state,
    fetching: status,
  }
}

export const removeFromCart: any =
  (state: CartState, { itemId, date }: { itemId: number, date: Moment }) => {
    const formattedDate = date.format('YYYY-MM-DD')
    let newDeliveries: Delivery[] = []
    state.cartItems.forEach(delivery => {
      let deliveryItems: itemResponse[] = []
      delivery.items.forEach(deliveryItem => {
        if (delivery.date === formattedDate) {
          if (itemId !== deliveryItem.id) {
            deliveryItems.push(deliveryItem)
          }
        } else {
          deliveryItems.push(deliveryItem)
        }
      })
      if (deliveryItems.length > 0) {
        newDeliveries.push({
          ...delivery,
          items: deliveryItems,
        })
      }
    })

    if (newDeliveries.length > 0) {
      return {
        ...state,
        cartItems: newDeliveries,
      }
    }

    // clean cart if no deliveries
    return INITIAL_STATE
  }

export const clearCoupon: any = (state: CartState): CartState => {
  const newTotals = {...state.totals, discount: 0, total: state.totals.total + state.totals.discount}
  return {...state, coupon: null, totals: newTotals, promotion: null}
}

export const reducer: (state: CartState, action: any) =>
  CartState = createReducer(INITIAL_STATE, {
  [Types.ADD_TO_CART]: addToCart,
  [Types.UPDATE_TIME]: updateTime,
  [Types.UPDATE_ADDRESS]: updateAddress,
  [Types.UPDATE_KITCHEN]: updateKitchen,
  [Types.RESET_CART]: resetCart,
  [Types.LOADING]: loading,
  [Types.REMOVE_FROM_CART]: removeFromCart,
  [Types.CLEAR_COUPON]: clearCoupon,
})
