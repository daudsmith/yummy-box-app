import {
	createActions,
	createReducer,
} from 'reduxsauce'
import { AnyAction } from 'redux'

export interface LastUsedActionsCreators {
	addLastUsed: (code?: string, location?: Location, name?: string, address?: string) => AnyAction,
	resetLastUsed: () => AnyAction,
}

export interface LastUsedActionsTypes {
	ADD_LAST_USED: 'ADD_LAST_USED',
	RESET_LAST_USED: 'RESET_LAST_USED',
}

export interface Location {
	latitude: number,
	longitude: number,
}

const { Types, Creators } = createActions<LastUsedActionsTypes, LastUsedActionsCreators>({
	addLastUsed: ['code', 'location', 'name', 'address'],
	resetLastUsed: null,
})

export const LastUsedTypes = Types
export default Creators

export interface baseLastUsed {
	code?: string,
	location?: Location,
	name?: string,
	address?: string,
}

export const INITIAL_STATE = <baseLastUsed>({
	code: '',
	location: {},
	name: '',
  address: ''
})

export type LastUsedState = typeof INITIAL_STATE

export const addLastUsed: any = (state: LastUsedState, { code, location, name, address }: baseLastUsed) => {
	return {
		...state,
		code,
		location,
		name,
		address,
	}
}

export const resetLastUsed: any = (): LastUsedState => INITIAL_STATE


export const reducer: (state: LastUsedState, action: any) =>
	LastUsedState = createReducer(INITIAL_STATE, {
		[Types.ADD_LAST_USED]: addLastUsed,
		[Types.RESET_LAST_USED]: resetLastUsed,
	})
