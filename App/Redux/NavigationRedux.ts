import { NavigationActions } from 'react-navigation'
import AppNavigation from '../Navigation/AppNavigation'

const { navigate } = NavigationActions
const { getStateForAction } = AppNavigation.router

export interface Route {
  index: number
  key: string,
  params: object,
  routeName: string,
  isDrawerOpen?: boolean,
  isTransitioning?: boolean,
  icon?: string,
  routes?: Route[],
  routeKeyHistory?: string[],
}

export interface InitStateNavigation {
  index: number,
  isTransitioning: boolean,
  key: string,
  routes: Route[]
}

const INITIAL_STATE: InitStateNavigation | {} = getStateForAction(
  navigate({ routeName: 'LaunchScreen' })
)
export const reducer = (state: InitStateNavigation | {} = INITIAL_STATE, action: any): InitStateNavigation | {} => {
  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
}
