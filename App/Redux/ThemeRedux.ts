import {
  createActions,
  createReducer,
} from 'reduxsauce'
import { AnyAction } from 'redux'

export interface ThemeActionsCreators {
  fetchDetail: (id: number, kitchenCode: string) => AnyAction,
  fetchingDetail: () => AnyAction,
  fetchDetailSuccess: (themeDetail: ThemeDetail) => AnyAction,
  fetchDetailFailed: (error: string) => AnyAction,
  clearThemeDetail: () => AnyAction,

  fetchDetailItems: (id: number) => AnyAction,
  fetchingDetailItems: () => AnyAction,
  fetchDetailItemsSuccess: (meals: Item[]) => AnyAction,
  fetchDetailItemsFailed: (error: string) => AnyAction,

  fetchThemePackages: (id: number) => AnyAction,
  fetchingPackages: () => AnyAction,
  fetchPackagesSuccess: (themePackages: ThemePeriod[]) => AnyAction
  fetchPackagesFailed: (error: string) => AnyAction,
  selectPackage: (themePackage: ThemePeriod) => AnyAction,

  fetchAvailableDate: (themeId: number) => AnyAction,
  fetchingAvailableDate: () => AnyAction,
  fetchingAvailableDateSuccess: (newAvailableDates: string[], initialAvailableDates: string[]) => AnyAction,
  fetchingAvailableDateFailed: (error: string) => AnyAction,
  resetToInitialAvailableDate: () => AnyAction,
  fetchAvailableDateExtends: (themeId: number, startDate: string) => AnyAction,
  fetchingAvailableDateExtends: () => AnyAction,
  fetchingAvailableDateExtendsFailed: (error: string) => AnyAction,
  removeFirstDayOfAvailableDates: () => AnyAction,

  fetchAvailableMeals: (themeId: number) => AnyAction,
  fetchingAvailableMeals: () => AnyAction,
  fetchAvailableMealsSuccess: (availableMeals: Item[]) => AnyAction,
  fetchAvailableMealsFailed: (error: string) => AnyAction,
  clearThemeMeals: () => AnyAction,
  reset: () => AnyAction,
  resetAvailableDates: () => AnyAction,
}

export interface ThemeActionsTypes {
  FETCH_DETAIL: 'FETCH_DETAIL',
  FETCHING_DETAIL: 'FETCHING_DETAIL',
  FETCH_DETAIL_SUCCESS: 'FETCH_DETAIL_SUCCESS',
  FETCH_DETAIL_FAILED: 'FETCH_DETAIL_FAILED',
  CLEAR_THEME_DETAIL: 'CLEAR_THEME_DETAIL',
  FETCHING_DETAIL_ITEMS: 'FETCHING_DETAIL_ITEMS',
  FETCH_DETAIL_ITEMS_SUCCESS: 'FETCH_DETAIL_ITEMS_SUCCESS',
  FETCH_DETAIL_ITEMS_FAILED: 'FETCH_DETAIL_ITEMS_FAILED',
  FETCHING_PACKAGES: 'FETCHING_PACKAGES',
  FETCH_PACKAGES_SUCCESS: 'FETCH_PACKAGES_SUCCESSTCHING_DETAIL',
  FETCH_PACKAGES_FAILED: 'FETCH_PACKAGES_FAILED',
  SELECT_PACKAGE: 'SELECT_PACKAGE',
  FETCHING_AVAILABLE_DATE: 'FETCHING_AVAILABLE_DATE',
  FETCHING_AVAILABLE_DATE_SUCCESS: 'FETCHING_AVAILABLE_DATE_SUCCESS',
  FETCHING_AVAILABLE_DATE_FAILED: 'FETCHING_AVAILABLE_DATE_FAILED',
  FETCHING_AVAILABLE_MEALS: 'FETCHING_AVAILABLE_MEALS',
  FETCH_AVAILABLE_MEALS_SUCCESS: 'FETCH_AVAILABLE_MEALS_SUCCESS',
  FETCH_AVAILABLE_MEALS_FAILED: 'FETCH_AVAILABLE_MEALS_FAILED',
  FETCHING_AVAILABLE_DATE_EXTENDS: 'FETCHING_AVAILABLE_DATE_EXTENDS',
  FETCHING_AVAILABLE_DATE_EXTENDS_FAILED: 'FETCHING_AVAILABLE_DATE_EXTENDS_FAILED',
  RESET_TO_INITIAL_AVAILABLE_DATE: 'RESET_TO_INITIAL_AVAILABLE_DATE',
  CLEAR_THEME_MEALS: 'CLEAR_THEME_MEALS',
  RESET: 'RESET',
  RESET_AVAILABLE_DATES: 'RESET_AVAILABLE_DATES'
  FETCH_DETAIL_ITEMS: 'FETCH_DETAIL_ITEMS'
  FETCH_THEME_PACKAGES: 'FETCH_THEME_PACKAGES'
  FETCH_AVAILABLE_DATE: 'FETCH_AVAILABLE_DATE'
  FETCH_AVAILABLE_DATE_EXTENDS: 'FETCH_AVAILABLE_DATE_EXTENDS'
  REMOVE_FIRST_DAY_OF_AVAILABLE_DATES: 'REMOVE_FIRST_DAY_OF_AVAILABLE_DATES'
  FETCH_AVAILABLE_MEALS: 'FETCH_AVAILABLE_MEALS'
}

export interface ThemePeriod {
  badge: string,
  base_price: number,
  disabled: boolean,
  id?: number,
  name: string,
  period: number,
  sell_price: number,
  sku: string,
  tag_line: string,
  type: string,
  warning?: string,
  description: string,
  lowest_price?: number,
}

export interface Item {
  base_price: number,
  catalog_image: string,
  catalog_sorting: number,
  category: string,
  cogs: number,
  daily_quantity: number,
  daily_remaining_quantity: number,
  description: string,
  home_sorting: string,
  ingredients: string,
  is_available: boolean,
  item_id: number,
  kitchen_meal_name: string,
  sale_price: number,
  short_description: string,
  date: string,
  formatted_date: string,
  id: number,
  name: string,
  sku: string,
  theme_item_id: number,
  tax_class: string,
  images: {
    original: string,
    small_thumb: string,
    medium_thumb: string
  }
}

export interface ThemeDetail {
  available_dates: string[],
  cart_image: string,
  catalog_image: string,
  created_at: string,
  customer_domains: string,
  delivery_end: string,
  delivery_period: {
    label: string,
    type: string
  },
  delivery_start: string,
  description: string,
  detail_image: string,
  display_end: string,
  display_start: string,
  extend_dates: string[],
  id: number,
  items: Item[],
  max_subscription: number,
  name: string,
  sort: number,
  status: number,
  status_text: string,
  tagline: string,
  theme_periods: ThemePeriod[],
  updated_at: string
}

export interface InitStateTheme {
  themeDetail: ThemeDetail,
  fetchingDetail: boolean,
  detailError: string,
  isFetchingThemeDetail: boolean,

  meals: Item[],
  fetchingDetailItems: boolean,
  detailItemsError: string,

  themePackages: ThemePeriod[],
  fetchingPackages: boolean,
  packagesError: string,
  selectedPackage: ThemePeriod,

  availableMeals: Item[],
  fetchingAvailableMeals: boolean,
  fetchAvailableMealsError: string,

  initialAvailableDateSelectedTheme: string[],
  availableDateSelectedTheme: string[],
  fetchingAvailableDateSelectedTheme: boolean,
  errorAvailableDateSelectedTheme: string,

  fetchingAvailableDateExtendsSelectedTheme: boolean,
  errorAvailableDateExtendsSelectedTheme: string
}

const { Types, Creators } = createActions<ThemeActionsTypes, ThemeActionsCreators>({
  fetchDetail: ['id', 'kitchenCode'],
  fetchingDetail: [null],
  fetchDetailSuccess: ['themeDetail'],
  fetchDetailFailed: ['error'],
  clearThemeDetail: [null],

  fetchDetailItems: ['id'],
  fetchingDetailItems: [null],
  fetchDetailItemsSuccess: ['meals'],
  fetchDetailItemsFailed: ['error'],

  fetchThemePackages: ['id'],
  fetchingPackages: [null],
  fetchPackagesSuccess: ['themePackages'],
  fetchPackagesFailed: ['error'],
  selectPackage: ['themePackage'],

  fetchAvailableDate: ['themeId'],
  fetchingAvailableDate: [null],
  fetchingAvailableDateSuccess: [
    'newAvailableDates',
    'initialAvailableDates',
  ],
  fetchingAvailableDateFailed: ['error'],
  resetToInitialAvailableDate: null,
  fetchAvailableDateExtends: [
    'themeId',
    'startDate',
  ],
  fetchingAvailableDateExtends: [null],
  fetchingAvailableDateExtendsFailed: ['error'],
  removeFirstDayOfAvailableDates: null,

  fetchAvailableMeals: ['themeId'],
  fetchingAvailableMeals: [null],
  fetchAvailableMealsSuccess: ['availableMeals'],
  fetchAvailableMealsFailed: ['error'],
  clearThemeMeals: null,
  reset: null,
  resetAvailableDates: null,
})

export const ThemeTypes = Types
export default Creators

export const INITIAL_STATE: InitStateTheme = {
  themeDetail: null,
  fetchingDetail: false,
  detailError: '',
  isFetchingThemeDetail: false,

  meals: [],
  fetchingDetailItems: false,
  detailItemsError: null,

  themePackages: [],
  fetchingPackages: false,
  packagesError: null,
  selectedPackage: null,

  availableMeals: [],
  fetchingAvailableMeals: false,
  fetchAvailableMealsError: null,

  initialAvailableDateSelectedTheme: [],
  availableDateSelectedTheme: [],
  fetchingAvailableDateSelectedTheme: false,
  errorAvailableDateSelectedTheme: null,

  fetchingAvailableDateExtendsSelectedTheme: false,
  errorAvailableDateExtendsSelectedTheme: null,
}

export const fetchingDetail: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    fetchingDetail: false,
    detailError: '',
    isFetchingThemeDetail: true,
  }
}
export const fetchDetailSuccess: any = (
  state: InitStateTheme, { themeDetail }: { themeDetail: ThemeDetail }): InitStateTheme => {
  return {
    ...state,
    themeDetail: themeDetail,
    isFetchingThemeDetail: false,
  }
}
export const fetchDetailFailed: any = (state: InitStateTheme, { error }: { error: string }): InitStateTheme => {
  return {
    ...state,
    fetchingDetail: false,
    detailError: error,
    isFetchingThemeDetail: false,
  }
}
export const clearThemeDetail: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    themeDetail: null,
    themePackages: [],
    selectedPackage: null,
    meals: [],
    availableDateSelectedTheme: null,
    initialAvailableDateSelectedTheme: null,
  }
}
export const fetchingDetailItems: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    meals: [],
    fetchingDetailItems: true,
    detailItemsError: null,
  }
}
export const fetchDetailItemsSuccess: any = (state: InitStateTheme, { meals }: { meals: Item[] }): InitStateTheme => {
  return {
    ...state,
    meals,
    fetchingDetailItems: false,
  }
}
export const fetchDetailItemsFailed: any = (state: InitStateTheme, { error }: { error: string }): InitStateTheme => {
  return {
    ...state,
    meals: [],
    fetchingDetailItems: false,
    detailItemsError: error,
  }
}
export const fetchingPackages: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    fetchingPackages: true,
    packagesError: null,
  }
}
export const fetchPackagesSuccess: any = (
  state: InitStateTheme, { themePackages }: { themePackages: ThemePeriod[] }): InitStateTheme => {
  return {
    ...state,
    themePackages,
    fetchingPackages: false,
  }
}
export const fetchPackagesFailed: any = (state: InitStateTheme, { error }: { error: string }): InitStateTheme => {
  return {
    ...state,
    fetchingPackages: false,
    packagesError: error,
  }
}
export const selectPackage: any = (
  state: InitStateTheme, { themePackage }: { themePackage: ThemePeriod }): InitStateTheme => {
  return {
    ...state,
    selectedPackage: themePackage,
  }
}
export const fetchingAvailableMeals: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    fetchingAvailableMeals: true,
    fetchAvailableMealsError: null,
  }
}
export const fetchAvailableMealsSuccess: any = (
  state: InitStateTheme, { availableMeals }: { availableMeals: Item[] }): InitStateTheme => {
  return {
    ...state,
    fetchingAvailableMeals: false,
    availableMeals,
  }
}
export const fetchAvailableMealsFailed: any = (state: InitStateTheme, { error }: { error: string }): InitStateTheme => {
  return {
    ...state,
    fetchingAvailableMeals: false,
    fetchAvailableMealsError: error,
  }
}
export const fetchingAvailableDate: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    fetchingAvailableDateSelectedTheme: true,
    errorAvailableDateSelectedTheme: null,
  }
}
export const fetchingAvailableDateSuccess: any = (
  state: InitStateTheme,
  { newAvailableDates, initialAvailableDates }: { newAvailableDates: string[], initialAvailableDates: string[] },
): InitStateTheme => {
  return {
    ...state,
    availableDateSelectedTheme: newAvailableDates,
    initialAvailableDateSelectedTheme: initialAvailableDates,
    fetchingAvailableDateSelectedTheme: false,
    fetchingAvailableDateExtendsSelectedTheme: false,
  }
}
export const fetchingAvailableDateFailed: any = (
  state: InitStateTheme, { error }: { error: string }): InitStateTheme => {
  return {
    ...state,
    fetchingAvailableDateSelectedTheme: false,
    errorAvailableDateSelectedTheme: error,
  }
}
export const resetToInitialAvailableDate: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    availableDateSelectedTheme: state.initialAvailableDateSelectedTheme,
  }
}
export const fetchingAvailableDateExtends: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    fetchingAvailableDateExtendsSelectedTheme: true,
    errorAvailableDateExtendsSelectedTheme: null,
  }
}
export const fetchingAvailableDateExtendsFailed: any = (
  state: InitStateTheme, { error }: { error: string }): InitStateTheme => {
  return {
    ...state,
    fetchingAvailableDateExtendsSelectedTheme: false,
    errorAvailableDateExtendsSelectedTheme: error,
  }
}
export const clearThemeMeals: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    meals: [],
  }
}
export const reset: any = () => INITIAL_STATE

export const resetAvailableDates: any = (state: InitStateTheme): InitStateTheme => {
  return {
    ...state,
    availableDateSelectedTheme: [],
  }
}

export const reducer: (state: InitStateTheme, action: any) => InitStateTheme = createReducer(INITIAL_STATE, {
  [Types.FETCHING_DETAIL]: fetchingDetail,
  [Types.FETCH_DETAIL_SUCCESS]: fetchDetailSuccess,
  [Types.FETCH_DETAIL_FAILED]: fetchDetailFailed,
  [Types.CLEAR_THEME_DETAIL]: clearThemeDetail,
  [Types.FETCHING_DETAIL_ITEMS]: fetchingDetailItems,
  [Types.FETCH_DETAIL_ITEMS_SUCCESS]: fetchDetailItemsSuccess,
  [Types.FETCH_DETAIL_ITEMS_FAILED]: fetchDetailItemsFailed,
  [Types.FETCHING_PACKAGES]: fetchingPackages,
  [Types.FETCH_PACKAGES_SUCCESS]: fetchPackagesSuccess,
  [Types.FETCH_PACKAGES_FAILED]: fetchPackagesFailed,
  [Types.SELECT_PACKAGE]: selectPackage,
  [Types.FETCHING_AVAILABLE_DATE]: fetchingAvailableDate,
  [Types.FETCHING_AVAILABLE_DATE_SUCCESS]: fetchingAvailableDateSuccess,
  [Types.FETCHING_AVAILABLE_DATE_FAILED]: fetchingAvailableDateFailed,
  [Types.FETCHING_AVAILABLE_MEALS]: fetchingAvailableMeals,
  [Types.FETCH_AVAILABLE_MEALS_SUCCESS]: fetchAvailableMealsSuccess,
  [Types.FETCH_AVAILABLE_MEALS_FAILED]: fetchAvailableMealsFailed,
  [Types.FETCHING_AVAILABLE_DATE_EXTENDS]: fetchingAvailableDateExtends,
  [Types.FETCHING_AVAILABLE_DATE_EXTENDS_FAILED]: fetchingAvailableDateExtendsFailed,
  [Types.RESET_TO_INITIAL_AVAILABLE_DATE]: resetToInitialAvailableDate,
  [Types.CLEAR_THEME_MEALS]: clearThemeMeals,
  [Types.RESET]: reset,
  [Types.RESET_AVAILABLE_DATES]: resetAvailableDates,
})
