import {
  createActions,
  createReducer,
} from 'reduxsauce'
import { AnyAction } from 'redux'

export interface CreditTokenTopUpActionsCreators {
  submitToken: (code: string) => AnyAction
  submittingToken: () => AnyAction
  submittingTokenDone: (wallet: number) => AnyAction,
  submittingTokenFailed: (error: string) => AnyAction,
  resetTokenTopUp: () => AnyAction,
}

export interface CreditTokenTopUpActionsTypes {
  SUBMIT_TOKEN: 'SUBMIT_TOKEN'
  SUBMITTING_TOKEN: 'SUBMITTING_TOKEN'
  SUBMITTING_TOKEN_DONE: 'SUBMITTING_TOKEN_DONE'
  SUBMITTING_TOKEN_FAILED: 'SUBMITTING_TOKEN_FAILED',
  RESET_TOKEN_TOP_UP: 'RESET_TOKEN_POPUP',
}

const { Types, Creators } = createActions<CreditTokenTopUpActionsTypes, CreditTokenTopUpActionsCreators>({
  submitToken: ['code'],
  submittingToken: null,
  submittingTokenDone: ['wallet'],
  submittingTokenFailed: ['error'],
  resetTokenTopUp: null,
})

export const CreditTokenTopUpTypes = Types
export default Creators

export interface InitCreditTokenTopUp {
  submittingToken: boolean,
  error: string,
  wallet: string,
}

export const INITIAL_STATE: InitCreditTokenTopUp = {
  submittingToken: false,
  error: null,
  wallet: null,
}


export const resetTokenTopUp: any = (state: InitCreditTokenTopUp): InitCreditTokenTopUp => {
  return { ...state, ...INITIAL_STATE }
}
export const submitToken: any = (state: InitCreditTokenTopUp): InitCreditTokenTopUp => {
  return {
    ...state,
    submittingToken: true,
    error: null,
    wallet: null,
  }
}
export const submittingTokenDone: any = (state: InitCreditTokenTopUp, { wallet }: { wallet: string }): InitCreditTokenTopUp => {
  return {
    ...state,
    error: null,
    wallet,
    submittingToken: false,
  }
}
export const submittingTokenFailed: any = (state: InitCreditTokenTopUp, { error }: { error: string }) => {
  return {
    ...state,
    error,
    wallet: null,
    submittingToken: false,
  }
}

export const reducer: (state: InitCreditTokenTopUp, action: any) => InitCreditTokenTopUp = createReducer(INITIAL_STATE, {
  [Types.RESET_TOKEN_TOP_UP]: resetTokenTopUp,
  [Types.SUBMIT_TOKEN]: submitToken,
  [Types.SUBMITTING_TOKEN_DONE]: submittingTokenDone,
  [Types.SUBMITTING_TOKEN_FAILED]: submittingTokenFailed,
})
