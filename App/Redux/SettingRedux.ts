import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'
import { Moment } from 'moment'

export interface SettingActionsCreators {
  fetching: (key: string[]) => AnyAction
  fetchSetting: (key: string[]) => AnyAction
  settingFetched: (settings: any) => AnyAction
  updateStatus: (status: boolean) => AnyAction
}

export interface SettingActionsTypes {
  FETCHING: 'FETCHING'
  FETCH_SETTING: 'FETCH_SETTING'
  SETTING_FETCHED: 'SETTING_FETCHED'
  UPDATE_STATUS: 'UPDATE_STATUS'
  RESET: 'RESET'
}

const { Types, Creators } = createActions<SettingActionsTypes, SettingActionsCreators>({
  fetching: ['key'],
  fetchSetting: ['key'],
  settingFetched: ['settings'],
  updateStatus: ['status'],
  reset: null,
})

export interface VaInterface {
  name: string,
  logo: string,
  code: string,
}

export interface VersionCheckInterface {
  version: string,
  mandatory: boolean,
  interval_minute: number,
  store_url: string,
  on_start_up: boolean,
  message: string,
  update: boolean,
  last_checked: string,
  poped: boolean,
}

export interface PaymentMethodOption {
  gopay: boolean;
  shopeepay: boolean;
}

export interface SettingDataInterface {
  finishFetch?: boolean,
  off_dates?: Moment[],
  delivery_cut_off?: string,
  app_link_faq?: string,
  app_link_privacy_policy?: string,
  app_link_terms_and_conditions?: string,
  cs_email_address?: string,
  cs_phone_number?: string,
  sales_order_total_limit?: string,
  subscription_minimum_wallet_balance?: string | number,
  bank_transfer_payment_option_cutoff?: string,
  subscription_repeat_interval?: string,
  delivery_times?: string,
  virtual_account_banks?: VaInterface[],
  subscription_artwork?: string,
  homescreen_selected_theme?: string,
  cs_whatsapp_number?: string,
  version_check?: VersionCheckInterface,
  featured_theme?: string,
  free_charge_policy?: string,
  payment_methods?: string
}

export interface InitSetting {
  data: SettingDataInterface
}

export const SettingTypes = Types
export default Creators

export interface SettingItem {
  key: string
  value: string
}

export interface AppVersion {
  version: string,
  mandatory: boolean,
  interval_minute: number,
  store_url: string,
  on_start_up: boolean,
  message: string,
  update: boolean
}

export const INITIAL_STATE: InitSetting = {
  data: {
    finishFetch: false,
  }
}

export const settingFetched: any = (state: InitSetting, { settings }: { settings: any }): InitSetting => {
  let newData = { ...state.data, ...settings }
  return { ...state, data: newData }
}
export const updateStatus: any = (state: InitSetting, { status }: { status: boolean }): InitSetting => {
  let newData = { ...state.data, finishFetch: status }
  return { ...state, data: newData }
}
export const reset: any = (state): InitSetting => state

export const reducer: (state: InitSetting, action: any) => InitSetting = createReducer(INITIAL_STATE, {
  [Types.RESET]: reset,
  [Types.SETTING_FETCHED]: settingFetched,
  [Types.UPDATE_STATUS]: updateStatus,
})
