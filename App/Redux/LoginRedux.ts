import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface LoginActionsCreators {
  loginRequest: (username: string, password: string) => AnyAction,
  loginSuccess: (user: User, token: string) => AnyAction,
  onLoginSuccess: (user: User, token: string) => AnyAction,
  loginFailure: (error: string) => AnyAction,
  logout: () => AnyAction,
  setFcmToken: (fcm_token: string) => AnyAction,
  setUid: (uid: string) => AnyAction,
  updateFcmToken: (fcmToken: string) => AnyAction,
  updatingDeviceInfo: (fcmToken: string) => AnyAction,
  loginAttempt: (identity: string, password: string) => AnyAction,
  uploadPhoto: (data: object) => AnyAction,
  uploadingPhoto: () => AnyAction,
  uploadingPhotoDone: (user: User) => AnyAction,
  uploadingPhotoFailed: (error: string) => AnyAction,
  clearErrorMessage: () => AnyAction,
  resetState: () => AnyAction,
  getNewUserData: () => AnyAction,
  attemptingLogin: () => AnyAction,
}

export interface LoginActionsTypes {
  LOGIN_SUCCESS: 'LOGIN_SUCCESS'
  LOGIN_FAILURE: 'LOGIN_FAILURE'
  ATTEMPTING_LOGIN: 'ATTEMPTING_LOGIN'
  UPLOADING_PHOTO: 'UPLOADING_PHOTO'
  UPLOADING_PHOTO_DONE: 'UPLOADING_PHOTO_DONE'
  UPLOADING_PHOTO_FAILED: 'UPLOADING_PHOTO_FAILED'
  CLEAR_ERROR_MESSAGE: 'CLEAR_ERROR_MESSAGE'
  RESET_STATE: 'RESET_STATE'
  SET_FCM_TOKEN: 'SET_FCM_TOKEN'
  SET_UID: 'SET_UID'
  LOGIN_REQUEST: 'LOGIN_REQUEST'
  ON_LOGIN_SUCCESS: 'ON_LOGIN_SUCCESS'
  LOGOUT: 'LOGOUT'
  UPDATE_FCM_TOKEN: 'UPDATE_FCM_TOKEN'
  UPDATING_DEVICE_INFO: 'UPDATING_DEVICE_INFO'
  LOGIN_ATTEMPT: 'LOGIN_ATTEMPT'
  UPLOAD_PHOTO: 'UPLOAD_PHOTO'
  GET_NEW_USER_DATA: 'GET_NEW_USER_DATA'
}

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions<LoginActionsTypes, LoginActionsCreators>({
  loginRequest: ['username', 'password'],
  loginSuccess: ['user', 'token'],
  onLoginSuccess: ['user', 'token'],
  loginFailure: ['error'],
  logout: null,
  setFcmToken: ['fcm_token'],
  setUid: ['uid'],
  updateFcmToken: ['fcmToken'],
  updatingDeviceInfo: ['fcmToken'],
  loginAttempt: ['identity', 'password'],
  attemptingLogin: null,
  uploadPhoto: ['data'],
  uploadingPhoto: null,
  uploadingPhotoDone: ['user'],
  uploadingPhotoFailed: ['error'],
  clearErrorMessage: null,
  resetState: null,
  getNewUserData: null
})

export const LoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export interface User {
  id: number,
  email: string,
  phone: string,
  first_name: string,
  last_name: string,
  photo: string,
  invite_code: string,
  invitation_code: string,
  invitation_uri: string,
  social_login_type: string,
  social_login_id: string,
  current_credits: number,
  newsletter_subscribed: number,
  salutation: string
}

interface BaseLogin {
  user?: User | null,
  error: string,
  fetching: boolean,
  token: string,
  uid: string,
  fcm_token: string,
  uploadingPhoto: boolean,
  uploadingPhotoError: string
}

export const INITIAL_STATE = <BaseLogin>{
  user: null,
  error: null,
  fetching: false,
  token: null,
  uid: null,
  fcm_token: null,
  uploadingPhoto: false,
  uploadingPhotoError: null,
}

export type LoginState = typeof INITIAL_STATE

/* ------------- Reducers ------------- */

// we're attempting to login
export const request: any = (state: LoginState) => {
  return {...state, fetching: true}
}

// we've successfully logged in
export const success: any = (state: LoginState,
  {user, token}: {user: User, token: string },
  ) => {
  return {...state, fetching: false, error: null, user, token}
}

// we've had a problem logging in
export const failure: any = (state: LoginState, { error }: { error: string }) => {
  return {...state, fetching: false, error}
}

// we've logged out
export const resetState: any = () => INITIAL_STATE

export const attemptingLogin: any = (state: LoginState) => {
  return {...state, error: null, fetching: true}
}

export const uploadingPhoto: any = (state: LoginState) => {
  return {...state, uploadingPhoto: true, uploadingPhotoError: null}
}

export const uploadingPhotoDone: any = (state: LoginState, {user}: {user: User}) => {
  return {...state, user, uploadingPhoto: false, uploadingPhotoError: null}
}

export const uploadingPhotoFailed: any = (state: LoginState, {error}: {error: string}) => {
  return {...state, uploadingPhoto: false, uploadingPhotoError: error}
}

export const clearErrorMessage: any = (state: LoginState) => {
  return {...state, error: null, fetching: false}
}

export const setFcmToken: any = (state: LoginState, fcm_token: string) => {
  return {...state, fcm_token}
}
export const setUid: any = (state: LoginState, {uid}: {uid: string}) => {
  return {...state, uid}
}
/* ------------- Hookup Reducers To Types ------------- */

export const reducer: (state: LoginState, action: any) => LoginState = createReducer(INITIAL_STATE, {
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.ATTEMPTING_LOGIN]: attemptingLogin,
  [Types.UPLOADING_PHOTO]: uploadingPhoto,
  [Types.UPLOADING_PHOTO_DONE]: uploadingPhotoDone,
  [Types.UPLOADING_PHOTO_FAILED]: uploadingPhotoFailed,
  [Types.CLEAR_ERROR_MESSAGE]: clearErrorMessage,
  [Types.RESET_STATE]: resetState,
  [Types.SET_FCM_TOKEN]: setFcmToken,
  [Types.SET_UID]: setUid
})

/* ------------- Selectors ------------- */

// Is the current user logged in?
export const isLoggedIn: any = (loginState: LoginState) => loginState.user !== null
