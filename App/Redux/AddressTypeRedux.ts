import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface AddressTypeReduxCreators {
  updateCompanyAddresses: (companyAddresses: CompanyAddress[]) => AnyAction,
  reset: () => AnyAction,
  fetchCompanyAddresses: () => AnyAction,
  companyAddressesFetching: () => AnyAction,
  companyAddressesFetched: () => AnyAction,
  errorOccurred: (errorMessage: boolean) => AnyAction,
}

export interface AddressTypeReduxTypes {
  UPDATE_COMPANY_ADDRESSES: 'UPDATE_COMPANY_ADDRESSES',
  RESET: 'RESET',
  FETCH_COMPANY_ADDRESSES: 'FETCH_COMPANY_ADDRESSES'
  COMPANY_ADDRESSES_FETCHING: 'COMPANY_ADDRESSES_FETCHING',
  COMPANY_ADDRESSES_FETCHED: 'COMPANY_ADDRESSES_FETCHED',
  ERROR_OCCURRED: 'ERROR_OCCURRED',
}

const { Types, Creators } = createActions<AddressTypeReduxTypes, AddressTypeReduxCreators>({
  updateCompanyAddresses: ['companyAddresses'],
  reset: null,
  fetchCompanyAddresses: null,
  companyAddressesFetching: null,
  companyAddressesFetched: null,
  errorOccurred: ['errorMessage']
})

export const AddressTypeTypes = Types
export default Creators

export interface CompanyAddress {
  addressId: number
  label: string
  note: string
  address: string
  status: string
  longitude: number
  latitude: number
  display_label: string
  display_address: string
  recipient: string
  name: string
  phone: string
}

interface baseAddressType {
  companyAddresses: CompanyAddress[],
  loading: boolean,
  error: boolean,
}

export const INITIAL_STATE = <baseAddressType>{
  companyAddresses: [],
  loading: false,
  error: null,
}

export type AddressTypeReduxState = typeof INITIAL_STATE

export const updated: any = (state: AddressTypeReduxState, action: any) => {
  const { companyAddresses } = action
  return {...state, companyAddresses, loading: false, error: null}
}

export const reset: any = () => INITIAL_STATE

export const processing: any = (state: AddressTypeReduxState) => {
  return {...state, loading: true}
}
export const finished: any = (state: AddressTypeReduxState) => {
  return {...state, loading: false, error: null}
}

export const errorOccurred: any = (state: AddressTypeReduxState, action: any) => {
  const {errorMessage} = action
  return {...state, error: errorMessage, loading: false, companyAddresses: []}
}

export const reducer: (state: AddressTypeReduxState, action: any) => AddressTypeReduxState = createReducer(INITIAL_STATE, {
  [Types.UPDATE_COMPANY_ADDRESSES]: updated,
  [Types.RESET]: reset,
  [Types.COMPANY_ADDRESSES_FETCHING]: processing,
  [Types.COMPANY_ADDRESSES_FETCHED]: finished,
  [Types.ERROR_OCCURRED]: errorOccurred
})
