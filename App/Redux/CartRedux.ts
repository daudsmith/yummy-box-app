import {
  createActions,
  createReducer
} from 'reduxsauce'
import { AnyAction } from 'redux'
import { Addresses } from './AddressHistoryRedux'
import { Moment } from 'moment'
import { cartMeta } from '../Services/ApiCart'
import { CART_ITEM_STATUS, itemResponse } from '../Services/V2/ApiCart'
import { DeliveryTotal } from './V2/CartRedux'

export interface CartActionsCreators {
  userUpdateCart: (cart: Cart) => AnyAction
  userEmptyingCart: () => AnyAction
  validateCart: () => AnyAction
  validatingCart: () => AnyAction
  addToCart: (item: Item[] , date: Moment, category: string, exclusive: boolean) => AnyAction
  removeFromCart: (item: Item[] , date: Moment) => AnyAction
  updateDeliveryTime: (date: string, time: string, useMultipleAddress: boolean) => AnyAction
  updateDeliveryAddress: (date: string, address: Array<string>, useMultipleAddress: boolean) => AnyAction
  useMultipleAddress: () => AnyAction
  cancelUsingMultipleAddress: () => AnyAction
  resetCart: () => AnyAction
  fetchCartTotals: (cart: Cart[], promotion: string) => AnyAction
  fetchingCartTotals: () => AnyAction
  fetchingCartTotalsDone: () => AnyAction
  fetchingCartTotalsFailed: (error: string) => AnyAction
  setNullAmount: () => AnyAction
  applyCoupon: (coupon: string) => AnyAction
  applyingCoupon: () => AnyAction
  applyCouponFinished: () => AnyAction
  setPromotionValidity: (isValidPromotion: boolean) => AnyAction
  clearPromotionValidity: () => AnyAction
  clearCoupon: () => AnyAction
  setCartType: (cartType: string) => AnyAction
  setNavigation: (navigateTo: string) => AnyAction
  getNewAddress: (
    screenAfterNewAddressSet: boolean,
    useMultipleAddress: boolean,
    cartType: string,
    cartItem: Item[]
  ) => AnyAction
  setNewDeliveryAddress: (place: Array<string>) => AnyAction
}

export interface CartActionsTypes {
  USER_UPDATE_CART: 'USER_UPDATE_CART'
  USER_EMPTYING_CART: 'USER_EMPTYING_CART'
  RESET_CART: 'RESET_CART'
  FETCHING_CART_TOTALS: 'FETCHING_CART_TOTALS'
  FETCHING_CART_TOTALS_DONE: 'FETCHING_CART_TOTALS_DONE'
  FETCHING_CART_TOTALS_FAILED: 'FETCHING_CART_TOTALS_FAILED'
  SET_NULL_AMOUNT: 'SET_NULL_AMOUNT'
  SET_PROMOTION_VALIDITY: 'SET_PROMOTION_VALIDITY'
  CLEAR_PROMOTION_VALIDITY: 'CLEAR_PROMOTION_VALIDITY'
  USE_MULTIPLE_ADDRESS: 'USE_MULTIPLE_ADDRESS'
  APPLYING_COUPON: 'APPLYING_COUPON'
  APPLY_COUPON_FINISHED: 'APPLY_COUPON_FINISHED'
  SET_CART_TYPE: 'SET_CART_TYPE'
  CLEAR_COUPON: 'CLEAR_COUPON'
  VALIDATE_CART: 'VALIDATE_CART'
  VALIDATING_CART: 'VALIDATING_CART'
  ADD_TO_CART: 'ADD_TO_CART'
  REMOVE_FROM_CART: 'REMOVE_FROM_CART'
  UPDATE_DELIVERY_TIME: 'UPDATE_DELIVERY_TIME'
  UPDATE_DELIVERY_ADDRESS: 'UPDATE_DELIVERY_ADDRESS'
  CANCEL_USING_MULTIPLE_ADDRESS: 'CANCEL_USING_MULTIPLE_ADDRESS'
  FETCH_CART_TOTALS: 'FETCH_CART_TOTALS'
  APPLY_COUPON: 'APPLY_COUPON'
  GET_NEW_ADDRESS: 'GET_NEW_ADDRESS'
  SET_NEW_DELIVERY_ADDRESS: 'SET_NEW_DELIVERY_ADDRESS'
  SET_NAVIGATION: 'SET_NAVIGATION'
}

const { Types, Creators } = createActions<CartActionsTypes, CartActionsCreators>({
  userUpdateCart: ['cart'],
  userEmptyingCart: null,
  validateCart: null,
  validatingCart: null,
  addToCart: ['item', 'date', 'category', 'exclusive'],
  removeFromCart: ['item', 'date'],
  updateDeliveryTime: ['date', 'time', 'useMultipleAddress'],
  updateDeliveryAddress: ['date', 'address', 'useMultipleAddress'],
  useMultipleAddress: [null],
  cancelUsingMultipleAddress: [null],
  resetCart: null,
  fetchCartTotals: ['cart', 'promotion'],
  fetchingCartTotals: null,
  fetchingCartTotalsDone: null,
  fetchingCartTotalsFailed: ['error'],
  setNullAmount: null,
  applyCoupon: ['coupon'],
  applyingCoupon: [null],
  applyCouponFinished: [null],
  setPromotionValidity: ['isValidPromotion'],
  clearPromotionValidity: null,
  clearCoupon: null,
  setCartType: ['cartType'],
  setNavigation: ['navigateTo'],
  getNewAddress: ['screenAfterNewAddressSet', 'useMultipleAddress', 'cartType', 'cartItem'],
  setNewDeliveryAddress: ['place']
})

export const CartTypes = Types
export default Creators

export interface Total {
  subtotal: number
  tax: number
  delivery_fee: number
  discount: number
  grandTotal: number
  total: number
  cashback: number
}

export interface Cart {
  isEmpty: boolean
  cartItems: CartItem[]
  amount: number
  totals?: Total
  coupon?: string
  useMultipleAddress: boolean
  proceedToPayment: boolean
  dinner?: boolean
  lunch?: boolean
  meta?: cartMeta
  category?: string,
  exclusive?: boolean,
  cutoff?: string,
  partner?: string,
  cutDay?: number,
}

export interface Item {
  id: number
  name: string
  sale_price?: number
  tax_class?: string
  images?: {
      original: string
      small_thumb: string
      medium_thumb: string
  },
  quantity?: number
  remaining_daily_quantity?: number
  cutoff?: string
  cutoff_day?: number
  short_description?: string
  description?: string
  detail?: {
    data: Item
  }
  kitchen_code: string
  updated?: boolean
  status?: string
}
export interface CartItemPackage {
  cart_image: string
  dates: string[]
  package_id: number
  package_name: string
  quantity: number
  subtotal?: number
  theme_id: number
  unit_price?: number
  theme_name?: string
  sell_price?: number
  cutoff?: string
  status?: CART_ITEM_STATUS
}

export interface CartItemSubscription{
  end_date?: string
  repeat_every: string[] | number[]
  repeat_interval: number
  start_date: string
  theme_id: number
  theme_name: string
}
export interface CartItemDelivery{
  name: string
  phone?: string
  date: string
  landmark: string
  address: string
  latitude: number
  longitude: number
  note?: string
  items?: itemResponse[]
  totals?: DeliveryTotal,
  first_item?: {
    item_name: string
    price: number
  }
  place?: Addresses
  packages?: CartItemPackage[]
  subscription?: CartItemSubscription
  time?:string
}
export interface CartItem {
  date: string
  deliveryTime: string
  delivery?: CartItemDelivery
  items?: Item[]
  packages?: CartItemPackage[]
  totals?: {
    subtotal: number
    delivery_fee: number
  }
  subscription?: CartItemSubscription
  first_item?: {
    item_name: string
    price: number
  }
}

export interface BaseCart {
  cartItems: CartItem[]
  useMultipleAddress: boolean
  proceedToPayment: boolean
  isEmpty: boolean
  amount: number
  totals: Total
  fetchingTotals: boolean
  fetchingTotalsError: string
  coupon: string
  applyingCoupon: boolean
  isValidPromotion: boolean
  type: string
  validatingCart: boolean
  dinner: boolean
  lunch: boolean
  navigateTo: string
  meta?: cartMeta
  category: string
  exclusive: boolean
  cutoff?: string
  cutDay?: number,
  partner?: string
}

export const INITIAL_STATE = <BaseCart>({
  cartItems: [],
  useMultipleAddress: false,
  proceedToPayment: false,
  isEmpty: true,
  amount: 0,
  totals: {
    subtotal: 0,
    tax: 0,
    delivery_fee: 0,
    discount: 0,
    grandTotal: 0
  },
  fetchingTotals: false,
  fetchingTotalsError: null,
  coupon: null,
  applyingCoupon: false,
  isValidPromotion: null,
  type: 'item',
  validatingCart: false,
  dinner: true,
  lunch: true,
  meta: null,
  navigateTo: '',
  category: '',
  exclusive: false,
  cutoff: null,
  cutDay: 0,
  partner: null
})
export type CartState = typeof INITIAL_STATE

export const isEmpty = (state: CartState) => state.cartItems.length < 1

export const updated: any = (state: CartState, action: any): CartState => {
  const {cart} = action
  const coupon = typeof cart.coupon === 'undefined' ? null : cart.coupon
  const totals = typeof cart.totals !== 'undefined' ? cart.totals : state.totals
  return {
    ...state,
    isEmpty: cart.isEmpty,
    cartItems: cart.cartItems,
    amount: cart.amount,
    totals,
    coupon,
    useMultipleAddress: cart.useMultipleAddress,
    proceedToPayment: cart.proceedToPayment,
    dinner: cart.dinner,
    lunch: cart.lunch,
    meta: cart.meta,
    category: cart.category,
    exclusive: cart.exclusive,
    cutoff: cart.cutoff ? cart.cutoff : null,
    cutDay: cart.cutDay > 0 ? cart.cutDay : 0,
    partner: cart.partner ? cart.partner : null,
  }
}

export const emptyCart = () => INITIAL_STATE

export const fetchingCartTotals: any = (state: CartState): CartState => {
  return {...state, fetchingTotals: true}
}
export const fetchingCartTotalsDone: any = (state: CartState): CartState => {
  return {...state, fetchingTotals: false, fetchingTotalsError: null}
}
export const fetchingCartTotalsFailed: any = (state: CartState, {error} : {error: string}): CartState => {
  return {
    ...state,
    fetchingTotals: false,
    fetchingTotalsError: error
  }
}
export const setNullAmount: any = (state: CartState) => {
  return {...state, totals: INITIAL_STATE.totals}
}
export const setPromotionValidity: any = (state: CartState, {isValidPromotion} : {isValidPromotion: boolean}): CartState => {
  return {...state, isValidPromotion}
}
export const clearPromotionValidity: any = (state: CartState): CartState => {
  return {...state, isValidPromotion: null}
}
export const clearCoupon: any = (state: CartState): CartState => {
  const newTotals = {...state.totals, discount: 0, total: state.totals.total + state.totals.discount}
  return {...state, coupon: null, totals: newTotals}
}
export const useMultipleAddress: any = (state: CartState): CartState => {
  return {...state, useMultipleAddress: true}
}
export const applyingCoupon: any = (state: CartState): CartState => {
  return {...state, applyingCoupon: true}
}
export const applyCouponFinished: any = (state: CartState): CartState => {
  return {...state, applyingCoupon: false}
}
export const setCartType: any = (state: CartState, {cartType} :  {cartType: string}): CartState => {
  return {...state, type: cartType}
}
export const setNavigation: any = (state: CartState, {navigateTo} :  {navigateTo: string}): CartState => {
  return {...state, navigateTo: navigateTo}
}

export const reducer: (state: CartState, action: any) => CartState = createReducer(INITIAL_STATE, {
  [Types.USER_UPDATE_CART]: updated,
  [Types.USER_EMPTYING_CART]: emptyCart,
  [Types.RESET_CART]: emptyCart,
  [Types.FETCHING_CART_TOTALS]: fetchingCartTotals,
  [Types.FETCHING_CART_TOTALS_DONE]: fetchingCartTotalsDone,
  [Types.FETCHING_CART_TOTALS_FAILED]: fetchingCartTotalsFailed,
  [Types.SET_NULL_AMOUNT]: setNullAmount,
  [Types.SET_PROMOTION_VALIDITY]: setPromotionValidity,
  [Types.CLEAR_PROMOTION_VALIDITY]: clearPromotionValidity,
  [Types.USE_MULTIPLE_ADDRESS]: useMultipleAddress,
  [Types.APPLYING_COUPON]: applyingCoupon,
  [Types.APPLY_COUPON_FINISHED]: applyCouponFinished,
  [Types.SET_CART_TYPE]: setCartType,
  [Types.CLEAR_COUPON]: clearCoupon,
  [Types.SET_NAVIGATION]: setNavigation,
})
