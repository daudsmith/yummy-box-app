import {
  createActions,
  createReducer,
} from 'reduxsauce'
import { AnyAction } from 'redux'
import { Meal as MealDetail } from './MealsRedux'

export interface MealDetailPopUpReduxCreators {
  fetchMealDetail: (mealId: number, date: string) => AnyAction
  fetchingMealDetail: () => AnyAction
  fetchMealDetailSuccess: (mealDetail: MealDetail) => AnyAction
  fetchMealDetailFailed: (error: string) => AnyAction
  resetMealDetailPopUp: () => AnyAction
}

export interface MealDetailPopUpReduxTypes {
  FETCH_MEAL_DETAIL: 'FETCH_MEAL_DETAIL'
  FETCHING_MEAL_DETAIL: 'FETCHING_MEAL_DETAIL'
  FETCH_MEAL_DETAIL_SUCCESS: 'FETCH_MEAL_DETAIL_SUCCESS'
  FETCH_MEAL_DETAIL_FAILED: 'FETCH_MEAL_DETAIL_FAILED'
  RESET_MEAL_DETAIL_POP_UP: 'RESET_MEAL_DETAIL_POP_UP'
}

const { Types, Creators } = createActions<MealDetailPopUpReduxTypes, MealDetailPopUpReduxCreators>({
  fetchMealDetail: [
    'mealId',
    'date',
  ],
  fetchingMealDetail: null,
  fetchMealDetailSuccess: ['mealDetail'],
  fetchMealDetailFailed: ['error'],
  resetMealDetailPopUp: null,
})

export const MealDetailPopUpTypes = Types
export default Creators

interface BaseMealDetailPopUpRedux {
  mealDetail: MealDetail
  loading: boolean
  error: string
}

export const INITIAL_STATE: BaseMealDetailPopUpRedux = {
  mealDetail: null,
  loading: false,
  error: null,
}

export type MealDetailPopUpReduxState = typeof INITIAL_STATE

export const fetchingMealDetail: any = (state: MealDetailPopUpReduxState): MealDetailPopUpReduxState => {
  return {
    ...state,
    loading: true,
    error: null,
  }
}

export const fetchMealDetailSuccess: any = (
  state: MealDetailPopUpReduxState,
  { mealDetail }: { mealDetail: MealDetail }
): MealDetailPopUpReduxState => {
  return {
    ...state,
    loading: false,
    mealDetail,
  }
}

export const fetchMealDetailFailed: any = (
  state: MealDetailPopUpReduxState, { error }: { error: string }): MealDetailPopUpReduxState => {
  return {
    ...state,
    loading: false,
    error,
  }
}

export const reset = () => INITIAL_STATE

export const reducer: (state: MealDetailPopUpReduxState, action: any) => MealDetailPopUpReduxState = createReducer(INITIAL_STATE, {
  [Types.FETCHING_MEAL_DETAIL]: fetchingMealDetail,
  [Types.FETCH_MEAL_DETAIL_SUCCESS]: fetchMealDetailSuccess,
  [Types.FETCH_MEAL_DETAIL_FAILED]: fetchMealDetailFailed,
  [Types.RESET_MEAL_DETAIL_POP_UP]: reset,
})
