export default {
  regexActionType: /FETCH*FAILED/,
  actionTypes: [
    'GET_OFF_DATES',
    'VALIDATE_CART',
    'STARTUP'
  ]
}