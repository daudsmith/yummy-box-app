import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface CustomerAddressReduxCreators {
  updateAddress: (addresses: Address[]) => AnyAction,
  reset: () => AnyAction,
  saveAddress: (address: Address) => AnyAction,
  savingAddress: () => AnyAction,
  savingAddressDone: (addresses: Address[]) => AnyAction,
  savingAddressFailed: (error: string) => AnyAction,
  addressSaving: () => AnyAction,
  addressSaved: () => AnyAction,
  deleteAddress: (addressId: number) => AnyAction,
  deleteAddressDone: (addresses: Address[]) => AnyAction,
  deleteAddressFailed: (error: string) => AnyAction,
  addressDeleting: () => AnyAction,
  addressDeleted: () => AnyAction,
  setDefaultAddress: (addressId: number) => AnyAction,
  setDefaultOnProcess: () => AnyAction,
  setDefaultFinished: () => AnyAction,
  editAddress: (addressId: number, address: Address) => AnyAction,
  editingAddress: () => AnyAction,
  editAddressDone: (addresses: Address[]) => AnyAction,
  editAddressFailed: (error: string) => AnyAction,
  addressEditing: () => AnyAction,
  addressEdited: () => AnyAction,
  errorOccurred: (errorMessage: string) => AnyAction,
  fetchAddress: () => AnyAction,
  fetchingAddress: () => AnyAction,
  fetchingAddressDone: (addresses: Address[]) => AnyAction,
  fetchingAddressFailed: (error: string) => AnyAction,
}

export interface CustomerAddressReduxTypes {
  FETCHING_ADDRESS: 'FETCHING_ADDRESS',
  FETCHING_ADDRESS_DONE: 'FETCHING_ADDRESS_DONE',
  FETCHING_ADDRESS_FAILED: 'FETCHING_ADDRESS_FAILED',
  SAVING_ADDRESS: 'SAVING_ADDRESS',
  SAVING_ADDRESS_DONE: 'SAVING_ADDRESS_DONE',
  SAVING_ADDRESS_FAILED: 'SAVING_ADDRESS_FAILED',
  EDITING_ADDRESS: 'EDITING_ADDRESS',
  EDIT_ADDRESS_DONE: 'EDIT_ADDRESS_DONE',
  EDIT_ADDRESS_FAILED: 'EDIT_ADDRESS_FAILED',
  DELETE_ADDRESS_DONE: 'DELETE_ADDRESS_DONE',
  DELETE_ADDRESS_FAILED: 'DELETE_ADDRESS_FAILED',
  UPDATE_ADDRESS: 'UPDATE_ADDRESS'
  RESET: 'RESET'
  SAVE_ADDRESS: 'SAVE_ADDRESS'
  ADDRESS_SAVING: 'ADDRESS_SAVING'
  ADDRESS_SAVED: 'ADDRESS_SAVED'
  DELETE_ADDRESS: 'DELETE_ADDRESS'
  ADDRESS_DELETING: 'ADDRESS_DELETING'
  ADDRESS_DELETED: 'ADDRESS_DELETED'
  SET_DEFAULT_ADDRESS: 'SET_DEFAULT_ADDRESS'
  SET_DEFAULT_ON_PROCESS: 'SET_DEFAULT_ON_PROCESS'
  SET_DEFAULT_FINISHED: 'SET_DEFAULT_FINISHED'
  EDIT_ADDRESS: 'EDIT_ADDRESS'
  ADDRESS_EDITING: 'ADDRESS_EDITING'
  ADDRESS_EDITED: 'ADDRESS_EDITED'
  ERROR_OCCURRED: 'ERROR_OCCURRED'
  FETCH_ADDRESS: 'FETCH_ADDRESS'
}

const { Types, Creators } = createActions<CustomerAddressReduxTypes, CustomerAddressReduxCreators>({
  updateAddress: ['addresses'],
  reset: null,
  saveAddress: ['address'],
  savingAddress: null,
  savingAddressDone: ['addresses'],
  savingAddressFailed: ['error'],
  addressSaving: null,
  addressSaved: null,
  deleteAddress: ['addressId'],
  deleteAddressDone: ['addresses'],
  deleteAddressFailed: ['error'],
  addressDeleting: null,
  addressDeleted: null,
  setDefaultAddress: ['addressId'],
  setDefaultOnProcess: null,
  setDefaultFinished: null,
  editAddress: [
    'addressId',
    'address',
  ],
  editingAddress: null,
  editAddressDone: ['addresses'],
  editAddressFailed: ['error'],
  addressEditing: null,
  addressEdited: null,
  errorOccurred: ['errorMessage'],
  fetchAddress: null,
  fetchingAddress: null,
  fetchingAddressDone: ['addresses'],
  fetchingAddressFailed: ['error'],
})

export const CustomerAddressTypes = Types
export default Creators

export interface Address {
  addressId: number,
  name: string,
  phone: string,
  label: string,
  landmark: string,
  address: string,
  latitude: number,
  longitude: number,
  note: string,
  display_label: string,
  display_address: string,
  recipient: string,
}

interface Action {
  addresses: Address[],
  errorMessage: string,
}

interface baseCustomerAddress {
  loading: boolean,
  error: string,
  addresses: Address[],
  saved: boolean,
  edited: boolean,
  deleted: boolean,
}

export const INITIAL_STATE = <baseCustomerAddress> {
  loading: false,
  error: '',
  addresses: [],
  saved: false,
  edited: false,
  deleted: false,
}

export type CustomerAddressReduxState = typeof INITIAL_STATE

export const updated: any = (state: CustomerAddressReduxState, action: Action) => {
  const { addresses }: { addresses: Address[] } = action
  return {
    ...state,
    addresses: addresses,
  }
}

export const reset = () => INITIAL_STATE

export const processing: any = (state: CustomerAddressReduxState) => {
  return {
    ...state,
    loading: true,
  }
}
export const finished: any = (state: CustomerAddressReduxState) => {
  return {
    ...state,
    loading: false,
    error: null,
  }
}

export const errorOccurred: any = (state: CustomerAddressReduxState, action: Action) => {
  const { errorMessage } = action
  return {
    ...state,
    error: errorMessage,
    loading: false,
  }
}

export const fetchingAddress: any = (state: CustomerAddressReduxState) => {
  return {
    ...state,
    addresses: [],
    loading: true,
  }
}

export const fetchingAddressDone: any = (state: CustomerAddressReduxState, { addresses }: { addresses: Address[] }) => {
  return {
    ...state,
    loading: false,
    addresses,
  }
}

export const fetchingAddressFailed: any = (state: CustomerAddressReduxState, { error }: { error: string }) => {
  return {
    ...state,
    loading: false,
    addresses: [],
    error,
  }
}


export const savingAddress: any = (state: CustomerAddressReduxState) => {
  return {
    ...state,
    addresses: [],
    loading: true,
    error: null,
  }
}

export const savingAddressDone: any = (state: CustomerAddressReduxState, { addresses }: { addresses: Address[] }) => {
  return {
    ...state,
    loading: false,
    saved: true,
    addresses,
    error: null,
  }
}

export const savingAddressFailed: any = (state: CustomerAddressReduxState, { error }: { error: string }) => {
  return {
    ...state,
    loading: false,
    saved: true,
    addresses: [],
    error,
  }
}

export const editingAddress: any = (state: CustomerAddressReduxState) => {
  return {
    ...state,
    addresses: [],
    loading: true,
    error: null,
  }
}

export const editAddressDone: any = (state: CustomerAddressReduxState, { addresses }: { addresses: Address[] }) => {
  return {
    ...state,
    loading: false,
    edited: true,
    addresses,
    error: null,
  }
}

export const editAddressFailed: any = (state: CustomerAddressReduxState, { error }: { error: string }) => {
  return {
    ...state,
    loading: false,
    edited: true,
    addresses: [],
    error,
  }
}

export const deleteAddressDone: any = (state: CustomerAddressReduxState, { addresses }: { addresses: Address[] }) => {
  return {
    ...state,
    loading: false,
    deleted: true,
    addresses,
    error: null,
  }
}

export const deleteAddressFailed: any = (state: CustomerAddressReduxState, { error }: { error: string }) => {
  return {
    ...state,
    loading: false,
    deleted: true,
    addresses: [],
    error,
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FETCHING_ADDRESS]: fetchingAddress,
  [Types.FETCHING_ADDRESS_DONE]: fetchingAddressDone,
  [Types.FETCHING_ADDRESS_FAILED]: fetchingAddressFailed,
  [Types.SAVING_ADDRESS]: savingAddress,
  [Types.SAVING_ADDRESS_DONE]: savingAddressDone,
  [Types.SAVING_ADDRESS_FAILED]: savingAddressFailed,
  [Types.EDITING_ADDRESS]: editingAddress,
  [Types.EDIT_ADDRESS_DONE]: editAddressDone,
  [Types.EDIT_ADDRESS_FAILED]: editAddressFailed,
  [Types.DELETE_ADDRESS_DONE]: deleteAddressDone,
  [Types.DELETE_ADDRESS_FAILED]: deleteAddressFailed,
})
