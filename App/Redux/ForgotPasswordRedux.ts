import {
  createActions,
  createReducer,
} from 'reduxsauce'
import { AnyAction } from 'redux'

export interface ForgotPasswordReduxCreators {
  reset: () => AnyAction
  forgotPassword: (email: string) => AnyAction
  submitting: () => AnyAction
  submittingFailed: (error: string) => AnyAction
  submittingDone: () => AnyAction
}

export interface ForgotPasswordReduxTypes {
  FORGOT_PASSWORD: 'FORGOT_PASSWORD'
  SUBMITTING: 'SUBMITTING'
  SUBMITTING_FAILED: 'SUBMITTING_FAILED'
  SUBMITTING_DONE: 'SUBMITTING_DONE'
  RESET: 'RESET'
}

const { Types, Creators } = createActions<ForgotPasswordReduxTypes, ForgotPasswordReduxCreators>({
  reset: null,
  forgotPassword: ['email'],
  submitting: null,
  submittingDone: null,
  submittingFailed: ['error'],
})

export const ForgotPasswordTypes = Types
export default Creators

export interface ForgotPasswordReduxState {
  submitting: boolean
  error: string
  submitted: boolean
}

export const INITIAL_STATE: ForgotPasswordReduxState = {
  submitting: false,
  error: null,
  submitted: false,
}

export const reset: any = () => INITIAL_STATE
export const submitting: any = (state: ForgotPasswordReduxState): ForgotPasswordReduxState => {
  return {
    ...state,
    submitting: true,
    error: null,
    submitted: false,
  }
}
export const submittingDone: any = (state: ForgotPasswordReduxState): ForgotPasswordReduxState => {
  return {
    ...state,
    submitting: false,
    submitted: true,
    error: null,
  }
}
export const submittingFailed: any = (state: ForgotPasswordReduxState, { error }: { error: string }): ForgotPasswordReduxState => {
  return {
    ...state,
    submitting: false,
    submitted: false,
    error,
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET]: reset,
  [Types.SUBMITTING]: submitting,
  [Types.SUBMITTING_DONE]: submittingDone,
  [Types.SUBMITTING_FAILED]: submittingFailed,
})
