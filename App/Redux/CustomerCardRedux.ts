import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'


export interface CustomerCardActionsCreators {
  updateCards: (cards: Card[]) => AnyAction,
  resetCards: () => AnyAction,
  addCard: (card: Card) => AnyAction,
  addingCard: () => AnyAction,
  addCardSuccess: () => AnyAction,
  removeCard: (cardId: number) => AnyAction,
  removingCard: () => AnyAction,
  removeCardSuccess: () => AnyAction,
  setDefaultCard: (cardId: number) => AnyAction,
  setDefaultCardOnProcess: () => AnyAction,
  setDefaultCardSuccess: () => AnyAction,
  fetchCards: () => AnyAction,
  fetchingCards: () => AnyAction,
  fetchingCardsDone: () => AnyAction,
  errorOccurred: (error: string) => AnyAction,

}

export interface CustomerCardActionsTypes {
  UPDATE_CARDS: 'UPDATE_CARDS'
  RESET_CARDS: 'RESET_CARDS'
  ADD_CARD: 'ADD_CARD'
  ADDING_CARD: 'ADDING_CARD'
  ADD_CARD_SUCCESS: 'ADD_CARD_SUCCESS'
  REMOVE_CARD: 'REMOVE_CARD'
  REMOVING_CARD: 'REMOVING_CARD'
  REMOVE_CARD_SUCCESS: 'REMOVE_CARD_SUCCESS'
  SET_DEFAULT_CARD: 'SET_DEFAULT_CARD'
  SET_DEFAULT_CARD_ON_PROCESS: 'SET_DEFAULT_CARD_ON_PROCESS'
  SET_DEFAULT_CARD_SUCCESS: 'SET_DEFAULT_CARD_SUCCESS'
  FETCH_CARDS: 'FETCH_CARDS'
  FETCHING_CARDS: 'FETCHING_CARDS'
  FETCHING_CARDS_DONE: 'FETCHING_CARDS_DONE'
  ERROR_OCCURRED: 'ERROR_OCCURRED'
}

export interface Card {
  id?: number,
  created_at?: string,
  updated_at?: string,
  account_id: number,
  token: string,
  authorization_id: string,
  type: string,
  ends_with: string,
  is_default?: number
}

export interface InitCustomerCard {
  cards: Card[],
  loading: boolean,
  error: string
}

const { Types, Creators } = createActions<CustomerCardActionsTypes, CustomerCardActionsCreators>({
  updateCards: ['cards'],
  resetCards: null,
  addCard: ['card'],
  addingCard: [null],
  addCardSuccess: [null],
  removeCard: ['cardId'],
  removingCard: [null],
  removeCardSuccess: [null],
  setDefaultCard: ['cardId'],
  setDefaultCardOnProcess: [null],
  setDefaultCardSuccess: [null],
  fetchCards: [],
  fetchingCards: null,
  fetchingCardsDone: [null],
  errorOccurred: ['error'],
})

export const CustomerCardTypes = Types
export default Creators

export const INITIAL_STATE: InitCustomerCard = {
  cards: [],
  loading: false,
  error: null
}

export const updated: any = (state: InitCustomerCard, { cards }: { cards: Card[] }): InitCustomerCard => {
  return { ...state, cards: cards }
}

export const reset: any = (): InitCustomerCard => INITIAL_STATE

export const onProcess: any = (state: InitCustomerCard): InitCustomerCard => {
  return { ...state, loading: true }
}
export const success: any = (state: InitCustomerCard): InitCustomerCard => {
  return { ...state, loading: false, error: null }
}
export const errorOccured: any = (state: InitCustomerCard, { error }: { error: string }): InitCustomerCard => {
  return { ...state, loading: false, error: error }
}

export const reducer: (state: InitCustomerCard, action: any) => InitCustomerCard = createReducer(INITIAL_STATE, {
  [Types.UPDATE_CARDS]: updated,
  [Types.RESET_CARDS]: reset,
  [Types.ADDING_CARD]: onProcess,
  [Types.ADD_CARD_SUCCESS]: success,
  [Types.REMOVING_CARD]: onProcess,
  [Types.REMOVE_CARD_SUCCESS]: success,
  [Types.SET_DEFAULT_CARD_ON_PROCESS]: onProcess,
  [Types.SET_DEFAULT_CARD_SUCCESS]: success,
  [Types.FETCHING_CARDS]: onProcess,
  [Types.FETCHING_CARDS_DONE]: success,
  [Types.ERROR_OCCURRED]: errorOccured
})
