import {
  createActions,
  createReducer,
} from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { AnyAction } from 'redux'

export interface AddressHistoryActionsCreators {
  fetchAddressHistory: () => AnyAction,
  fetchingAddressHistory: () => AnyAction,
  fetchAddressHistorySuccess: (addresses: Addresses[]) => AnyAction,
  fetchAddressHistoryFailed: (error: string) => AnyAction,
  reset: () => AnyAction
}

export interface AddressHistoryActionsTypes {
  FETCH_ADDRESS_HISTORY: 'FETCH_ADDRESS_HISTORY'
  FETCHING_ADDRESS_HISTORY: 'FETCHING_ADDRESS_HISTORY'
  FETCH_ADDRESS_HISTORY_SUCCESS: 'FETCH_ADDRESS_HISTORY_SUCCESS'
  FETCH_ADDRESS_HISTORY_FAILED: 'FETCH_ADDRESS_HISTORY_FAILED'
  RESET: 'RESET'
}

const { Types, Creators } = createActions<AddressHistoryActionsTypes, AddressHistoryActionsCreators>({
  fetchAddressHistory: [null],
  fetchingAddressHistory: [null],
  fetchAddressHistorySuccess: ['addresses'],
  fetchAddressHistoryFailed: ['error'],
  reset: null,
})

export const AddressHistoryTypes = Types
export default Creators

export interface Addresses {
  address: string
  latitude: number
  longitude: number
  address_details?: string
  note: string
  name: string
  used?: number
  landmark: string
  id?: number,
  label: string,
  addressId?: number,
}

interface BaseAddressHistory {
  addresses: Addresses[]
  fetching: boolean
  error: string
}

export const INITIAL_STATE = Immutable<BaseAddressHistory>({
  addresses: [],
  fetching: false,
  error: null,
})

export type AddressHistory = typeof INITIAL_STATE

export const fetchingAddressHistory: any = (state: AddressHistory) => {
  return {
    ...state,
    fetching: true,
  }
}
export const fetchAddressHistorySuccess: any = (
  state: AddressHistory, { addresses }: { addresses: Addresses[] },
) => {
  return {
    ...state,
    addresses,
    fetching: false,
  }
}
export const fetchAddressHistoryFailed: any = (
  state: AddressHistory, { error }: { error: string },
) => {
  return {
    ...state,
    error,
    fetching: false,
  }
}
export const reset = () => INITIAL_STATE

export const reducer: (state: AddressHistory, action: any) => AddressHistory = createReducer(INITIAL_STATE, {
  [Types.FETCHING_ADDRESS_HISTORY]: fetchingAddressHistory,
  [Types.FETCH_ADDRESS_HISTORY_SUCCESS]: fetchAddressHistorySuccess,
  [Types.FETCH_ADDRESS_HISTORY_FAILED]: fetchAddressHistoryFailed,
  [Types.RESET]: reset,
})
