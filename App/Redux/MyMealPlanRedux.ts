import { createActions, createReducer } from 'reduxsauce'
import { Item } from './CartRedux'
import { AnyAction } from 'redux'
import { subscriptionModifyData } from '../Services/ApiMyMealPlan'
import { paymentInfoData } from '../Services/XenditService'

export interface MyMealPlanActionsCreators {
  getMyMealPlans: () => AnyAction,
  gettingMyMealPlans: () => AnyAction,
  getMyMealPlansSuccess: (myMealPlans: MyMealPlan[]) =>  AnyAction,
  getMyMealPlansFailed: (error: string) => AnyAction,
  getMyMealPlanDetail: (id: number, orderType: string) => AnyAction
  gettingMyMealPlanDetail: () => AnyAction,
  getMyMealPlanDetailSuccess: (myMealPlanDetail: MyMealPlanDetailSuccess) => AnyAction,
  getMyMealPlanDetailFailed: (error: string) => AnyAction,
  cancelOrder: (id: number) => AnyAction,
  cancelingOrder: () => AnyAction,
  cancelOrderSuccess: () => AnyAction,
  cancelOrderFailed: (error: string) => AnyAction,
  setInitialModifiedMyMealPlan: () => AnyAction,
  removeModifiedMyMealPlanDetail: () => AnyAction,
  updateModifiedMmp: (modifiedMyMealPlanDetail: MyMealPlanDetailSuccess) => AnyAction,
  modifyMmpPackageDelivery: (field: string, value: any | object) => AnyAction
  modifyMmpPackageDeliveryMeal: (selectedDate: string, currentDate: string ) => AnyAction,
  modifyMmpSubscriptionSelectedPayment: (selectedPayment: string) => AnyAction,
  modifyMmpSubscriptionPaymentInfo: (paymentInfo: PaymentInfo) => AnyAction,
  setMmpNewCreditCardInfo: (newCreditCardInfo: CreditCardInfo) => AnyAction,
  modifyMmp: () => AnyAction,
  modifyingMmp: () => AnyAction,
  modifyMmpSuccess: () => AnyAction,
  modifyMmpFailed: (error: string) => AnyAction,
  clearMmpDetail: () => AnyAction,
  reset: () => AnyAction,
  resetModifyState: () => AnyAction,
  updateModifiedMyMealPlanDetail: (updatedModifiedMyPlanDetail: MyMealPlanDetailSuccess) => AnyAction,
  updateMyMealPlanWithPayment: () => AnyAction,
  setModifyType: (modifyType: string) => AnyAction,
}

export interface MyMealPlanActionsTypes {
  GETTING_MY_MEAL_PLANS: 'GETTING_MY_MEAL_PLANS',
  GET_MY_MEAL_PLANS_SUCCESS: 'GET_MY_MEAL_PLANS_SUCCESS',
  GET_MY_MEAL_PLANS_FAILED: 'GET_MY_MEAL_PLANS_FAILED',
  GETTING_MY_MEAL_PLAN_DETAIL: 'GETTING_MY_MEAL_PLAN_DETAIL',
  GET_MY_MEAL_PLAN_DETAIL_SUCCESS: 'GET_MY_MEAL_PLAN_DETAIL_SUCCESS',
  GET_MY_MEAL_PLAN_DETAIL_FAILED: 'GET_MY_MEAL_PLAN_DETAIL_FAILED',
  CANCELING_ORDER: 'CANCELING_ORDER',
  CANCEL_ORDER_SUCCESS: 'CANCEL_ORDER_SUCCESS',
  CANCEL_ORDER_FAILED: 'CANCEL_ORDER_FAILED',
  SET_INITIAL_MODIFIED_MY_MEAL_PLAN: 'SET_INITIAL_MODIFIED_MY_MEAL_PLAN',
  REMOVE_MODIFIED_MY_MEAL_PLAN_DETAIL: 'REMOVE_MODIFIED_MY_MEAL_PLAN_DETAIL',
  UPDATE_MODIFIED_MMP: 'UPDATE_MODIFIED_MMP',
  MODIFYING_MMP: 'MODIFYING_MMP',
  MODIFY_MMP_SUCCESS: 'MODIFY_MMP_SUCCESS',
  MODIFY_MMP_FAILED: 'MODIFY_MMP_FAILED',
  CLEAR_MMP_DETAIL: 'CLEAR_MMP_DETAIL',
  RESET: 'RESET',
  SET_MMP_NEW_CREDIT_CARD_INFO: 'SET_MMP_NEW_CREDIT_CARD_INFO',
  RESET_MODIFY_STATE: 'RESET_MODIFY_STATE',
  MODIFY_MY_MEAL_PLAN_DELIVERY_ITEM: 'MODIFY_MY_MEAL_PLAN_DELIVERY_ITEM',
  UPDATE_MODIFIED_MY_MEAL_PLAN_DETAIL: 'UPDATE_MODIFIED_MY_MEAL_PLAN_DETAIL',
  SET_MODIFY_TYPE: 'SET_MODIFY_TYPE',
  GET_MY_MEAL_PLANS: 'GET_MY_MEAL_PLANS'
  GET_MY_MEAL_PLAN_DETAIL: 'GET_MY_MEAL_PLAN_DETAIL'
  CANCEL_ORDER: 'CANCEL_ORDER'
  MODIFY_MMP_PACKAGE_DELIVERY: 'MODIFY_MMP_PACKAGE_DELIVERY'
  MODIFY_MMP_PACKAGE_DELIVERY_MEAL: 'MODIFY_MMP_PACKAGE_DELIVERY_MEAL'
  MODIFY_MMP_SUBSCRIPTION_SELECTED_PAYMENT: 'MODIFY_MMP_SUBSCRIPTION_SELECTED_PAYMENT'
  MODIFY_MMP_SUBSCRIPTION_PAYMENT_INFO: 'MODIFY_MMP_SUBSCRIPTION_PAYMENT_INFO'
  MODIFY_MMP: 'MODIFY_MMP'
  UPDATE_MY_MEAL_PLAN_WITH_PAYMENT: 'UPDATE_MY_MEAL_PLAN_WITH_PAYMENT'
}

const { Types, Creators } = createActions<MyMealPlanActionsTypes, MyMealPlanActionsCreators>({
  getMyMealPlans: [null],
  gettingMyMealPlans: [null],
  getMyMealPlansSuccess: ['myMealPlans'],
  getMyMealPlansFailed: ['error'],
  getMyMealPlanDetail: [
    'id',
    'orderType',
  ],
  gettingMyMealPlanDetail: [null],
  getMyMealPlanDetailSuccess: ['myMealPlanDetail'],
  getMyMealPlanDetailFailed: ['error'],
  cancelOrder: ['id'],
  cancelingOrder: [null],
  cancelOrderSuccess: [null],
  cancelOrderFailed: ['error'],
  setInitialModifiedMyMealPlan: [null],
  removeModifiedMyMealPlanDetail: [null],
  updateModifiedMmp: ['modifiedMyMealPlanDetail'],
  modifyMmpPackageDelivery: [
    'field',
    'value',
  ],
  modifyMmpPackageDeliveryMeal: [
    'selectedDate',
    'currentDate',
  ],
  modifyMmpSubscriptionSelectedPayment: ['selectedPayment'],
  modifyMmpSubscriptionPaymentInfo: ['paymentInfo'],
  setMmpNewCreditCardInfo: ['newCreditCardInfo'],
  modifyMmp: [null],
  modifyingMmp: [null],
  modifyMmpSuccess: [null],
  modifyMmpFailed: ['error'],
  clearMmpDetail: null,
  reset: null,
  resetModifyState: null,
  updateModifiedMyMealPlanDetail: ['updatedModifiedMyPlanDetail'],
  updateMyMealPlanWithPayment: null,
  setModifyType: ['modifyType'],
})

export const MyMealPlanTypes = Types
export default Creators

export interface MyMealPlan {
  id: number,
  number: string,
  date: string,
  time: string,
  status: string,
  order_number: string,
  order_id: number,
  items_in_cart: number,
  total: number,
  payment: string,
  item_name: string,
  item_image: string,
  can_modify: number,
  can_cancel: number,
  type: string,
  theme_name: string,
  theme_image: string,
  package: string,
  deliveryCount: number,
  deliveredCount: number
}

export interface MyMealPlanDetail {
  id: number,
  created_at: string,
  updated_at: string,
  status: string,
  number: string,
  date: string,
  time: string,
  name: string,
  phone: string,
  address: string,
  latitude: number,
  longitude: number,
  address_details: string,
  note: string,
  provider: string,
  provider_note: string,
  order_number: string,
  order_id: number,
  payment: string,
  subtotal: number,
  delivery_fee: number,
  can_modify: boolean,
  can_cancel: boolean,
  delivery_date: string,
  free_charged: number,
  landmark: string,
  type?: string,
  items?: {
    data: Item[]
  },
  payment_info?: PaymentInfo
  selectedPayment?: string
  kitchen_code?: string
}

export interface MyMealPlanDetailSuccess {
  data: MyMealPlanDetail | subscriptionModifyData
  message?: string
  order?: {
    type: string
  }
  type?: string
  error?: boolean
  payment_info?: PaymentInfo
  package?: PackageResponse
  selectedPayment?: string
  myMealPlanPackage?: {
    dates: string[]
    theme_id: number
  }
  action?: ActionResponse
  actions?: ActionResponse
  newCreditCardInfo: paymentInfoData
  payment_info_selectedPayment?: string
}

export interface PackageResponse {
  theme_id: number
  theme_name: string
  package_id: number
  package: string
  dates: string[]
}

export interface ActionResponse {
  canSkip: boolean
  canModify: boolean
  canChangeMeal: boolean
  canChangeDeliveryInfo: boolean
}

export interface PaymentInfo {
  id?: number,
  created_at?: string,
  updated_at?: string,
  account_id?: number,
  token?: string,
  authorization_id?: string,
  type?: string,
  ends_with?: string,
  is_default?: number,
  is_multiple_use?: boolean,
  payment_info_json?: string,
  method?: string,
  status?: string,
  total_amount_charged?: number,
  total_amount_due?: number,
  total_amount_paid?: number,
}

export interface CreditCardInfo {
  id: number,
  created_at: string,
  updated_at: string,
  account_id: number,
  token: string,
  authorization_id: string,
  type: string,
  ends_with: string,
  is_default: number,
}

interface BaseMyMealPlan {
  myMealPlans: MyMealPlan[],
  fetching: boolean,
  myMealPlanDetail: MyMealPlanDetailSuccess,
  modifiedMyMealPlanDetail: MyMealPlanDetailSuccess,
  fetchingDetail: boolean,
  cancelingOrder: boolean,
  modifying: boolean,
  modifySuccess: boolean,
  errorMessage?: string,
  detailErrorMessage?: string,
  cancelErrorMessage?: string,
  modifyErrorMessage?: string,
  modifyType?: string,
}

export const INITIAL_STATE = <BaseMyMealPlan>{
  myMealPlans: [],
  fetching: false,
  myMealPlanDetail: null,
  modifiedMyMealPlanDetail: null,
  fetchingDetail: false,
  cancelingOrder: false,
  modifying: false,
  modifySuccess: false,
  errorMessage: null,
  detailErrorMessage: null,
  cancelErrorMessage: null,
  modifyErrorMessage: null,
  modifyType: null,
}

export type MyMealPlanState = typeof INITIAL_STATE

export const gettingMyMealPlans: any = (state: MyMealPlanState) => {
  return {
    ...state,
    fetching: true,
    errorMessage: null,
  }
}
export const getMyMealPlansSuccess: any = (state: MyMealPlanState, { myMealPlans }: {myMealPlans: MyMealPlan[]}) => {
  return {
    ...state,
    fetching: false,
    myMealPlans,
  }
}
export const getMyMealPlansFailed: any = (state: MyMealPlanState, { error }: { error: string }) => {
  return {
    ...state,
    fetching: false,
    errorMessage: error,
  }
}
export const gettingMyMealPlanDetail: any = (state: MyMealPlanState) => {
  return {
    ...state,
    fetchingDetail: true,
    detailErrorMessage: null,
  }
}
export const getMyMealPlanDetailSuccess: any = (state: MyMealPlanState, { myMealPlanDetail }: {myMealPlanDetail: MyMealPlanDetail}) => {
  return {
    ...state,
    fetchingDetail: false,
    myMealPlanDetail,
  }
}
export const getMyMealPlanDetailFailed: any = (state: MyMealPlanState, { error }: { error: string }) => {
  return {
    ...state,
    fetchingDetail: false,
    detailErrorMessage: error,
  }
}
export const cancelingOrder: any = (state: MyMealPlanState) => {
  return {
    ...state,
    cancelingOrder: true,
    cancelErrorMessage: null,
  }
}
export const cancelOrderSuccess: any = (state: MyMealPlanState) => {
  return {
    ...state,
    cancelingOrder: false,
  }
}
export const cancelOrderFailed: any = (state: MyMealPlanState, { error }: { error: string }) => {
  return {
    ...state,
    cancelErrorMessage: error,
    cancelingOrder: false,
  }
}
export const setInitialModifiedMyMealPlan: any = (state: MyMealPlanState) => {
  const modifiedMyMealPlanDetail = {
    ...state.myMealPlanDetail,
    newCreditCardInfo: null,
  }
  return {
    ...state,
    modifiedMyMealPlanDetail,
  }
}
export const removeModifiedMyMealPlanDetail: any = (state: MyMealPlanState) => {
  return {
    ...state,
    modifyType: null,
    modifiedMyMealPlanDetail: null,
  }
}
export const updateModifiedMmp: any = (state: MyMealPlanState, { modifiedMyMealPlanDetail }: { modifiedMyMealPlanDetail: MyMealPlanDetail }) => {
  return {
    ...state,
    modifiedMyMealPlanDetail: modifiedMyMealPlanDetail,
  }
}
export const modifyingMmp: any = (state: MyMealPlanState) => {
  return {
    ...state,
    modifying: true,
    modifyErrorMessage: null,
    modifySuccess: null,
  }
}
export const modifyMmpSuccess: any = (state: MyMealPlanState) => {
  return {
    ...state,
    modifying: false,
    modifySuccess: true,
  }
}
export const modifyMmpFailed: any = (state: MyMealPlanState, { error }: { error: string }) => {
  return {
    ...state,
    modifying: false,
    modifyErrorMessage: error,
    modifySuccess: false,
  }
}
export const clearMmpDetail: any = (state: MyMealPlanState) => {
  return {
    ...state,
    myMealPlanDetail: null,
  }
}
export const reset = () => INITIAL_STATE
export const setMmpNewCreditCardInfo: any = (state: MyMealPlanState, { newCreditCardInfo }: { newCreditCardInfo: paymentInfoData }) => {
  const modifiedMyMealPlanDetail = {
    ...state.modifiedMyMealPlanDetail,
    newCreditCardInfo,
  }
  return {
    ...state,
    modifiedMyMealPlanDetail,
  }
}
export const resetModifyState: any = (state: MyMealPlanState) => {
  return {
    ...state,
    modifyType: null,
    modifySuccess: false,
    modifying: false,
    modifyErrorMessage: null,
  }
}

export const updateModifiedMyMealPlanDetail: any = (state: MyMealPlanState, { updatedModifiedMyPlanDetail }: {updatedModifiedMyPlanDetail: MyMealPlanDetail}) => {
  return {
    ...state,
    modifiedMyMealPlanDetail: updatedModifiedMyPlanDetail,
  }
}

export const setModifyType: any = (state: MyMealPlanState, { modifyType }: { modifyType: string }) => {
  return {
    ...state,
    modifyType: modifyType,
  }
}

export const reducer: (state: MyMealPlanState, action: any) => MyMealPlanState = createReducer(INITIAL_STATE, {
  [Types.GETTING_MY_MEAL_PLANS]: gettingMyMealPlans,
  [Types.GET_MY_MEAL_PLANS_SUCCESS]: getMyMealPlansSuccess,
  [Types.GET_MY_MEAL_PLANS_FAILED]: getMyMealPlansFailed,
  [Types.GETTING_MY_MEAL_PLAN_DETAIL]: gettingMyMealPlanDetail,
  [Types.GET_MY_MEAL_PLAN_DETAIL_SUCCESS]: getMyMealPlanDetailSuccess,
  [Types.GET_MY_MEAL_PLAN_DETAIL_FAILED]: getMyMealPlanDetailFailed,
  [Types.CANCELING_ORDER]: cancelingOrder,
  [Types.CANCEL_ORDER_SUCCESS]: cancelOrderSuccess,
  [Types.CANCEL_ORDER_FAILED]: cancelOrderFailed,
  [Types.SET_INITIAL_MODIFIED_MY_MEAL_PLAN]: setInitialModifiedMyMealPlan,
  [Types.REMOVE_MODIFIED_MY_MEAL_PLAN_DETAIL]: removeModifiedMyMealPlanDetail,
  [Types.UPDATE_MODIFIED_MMP]: updateModifiedMmp,
  [Types.MODIFYING_MMP]: modifyingMmp,
  [Types.MODIFY_MMP_SUCCESS]: modifyMmpSuccess,
  [Types.MODIFY_MMP_FAILED]: modifyMmpFailed,
  [Types.CLEAR_MMP_DETAIL]: clearMmpDetail,
  [Types.RESET]: reset,
  [Types.SET_MMP_NEW_CREDIT_CARD_INFO]: setMmpNewCreditCardInfo,
  [Types.RESET_MODIFY_STATE]: resetModifyState,
  [Types.UPDATE_MODIFIED_MY_MEAL_PLAN_DETAIL]: updateModifiedMyMealPlanDetail,
  [Types.SET_MODIFY_TYPE]: setModifyType,
})
