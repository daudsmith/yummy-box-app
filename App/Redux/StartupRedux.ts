import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface StartupActionsCreators {
  startupFinished: () => AnyAction
  startupOnAppLaunch: () => AnyAction
  startupOnHomeScreen: () => AnyAction
  startupOnMmpScreen: () => AnyAction
  startupOnMyOrder: () => AnyAction
  startupOnNotificationList: () => AnyAction
  startupOnTopUpScreen: () => AnyAction
  startupOnCreditCardList: () => AnyAction
}

export interface StartupActionsTypes {
  STARTUP_FINISHED: 'STARTUP_FINISHED'
  STARTUP_ON_APP_LAUNCH: 'STARTUP_ON_APP_LAUNCH'
  STARTUP_ON_HOME_SCREEN: 'STARTUP_ON_HOME_SCREEN'
  STARTUP_ON_MMP_SCREEN: 'STARTUP_ON_MMP_SCREEN'
  STARTUP_ON_MY_ORDER: 'STARTUP_ON_MY_ORDER'
  STARTUP_ON_NOTIFICATION_LIST: 'STARTUP_ON_NOTIFICATION_LIST'
  STARTUP_ON_TOP_UP_SCREEN: 'STARTUP_ON_TOP_UP_SCREEN'
  STARTUP_ON_CREDIT_CARD_LIST: 'STARTUP_ON_CREDIT_CARD_LIST'
}
/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions<StartupActionsTypes, StartupActionsCreators>({
  startupOnAppLaunch: null,
  startupOnHomeScreen: null,
  startupOnMmpScreen: null,
  startupOnMyOrder: null,
  startupOnNotificationList: null,
  startupOnTopUpScreen: null,
  startupOnCreditCardList: null,
  startupFinished: null
})

export const StartupTypes = Types
export default Creators

export interface StartupState {
  hasFetchedStartupData: boolean
}

export const INITIAL_STATE: StartupState = {
  hasFetchedStartupData: false
}

export const startupFinished: any = (state: StartupState): StartupState => {
  return {...state, hasFetchedStartupData: true}
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.STARTUP_FINISHED]: startupFinished
})
