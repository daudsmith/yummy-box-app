import { reducer as network } from 'react-native-offline'
import { CombinedState, combineReducers, Reducer } from 'redux'
import rootSaga from '../Sagas/'
import configureStore, { AppState, storeInterface } from './CreateStore'

export default (): storeInterface => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer: Reducer<CombinedState<AppState>> = combineReducers<AppState>({
    network,
    nav: require('./NavigationRedux').reducer,
    register: require('./RegisterRedux').reducer,
    login: require('./LoginRedux').reducer,
    addressHistory: require('./AddressHistoryRedux').reducer,
    customerCard: require('./CustomerCardRedux').reducer,
    cart: require('./V2/CartRedux').reducer,
    addressType: require('./AddressTypeRedux').reducer,
    meals: require('./MealsRedux').reducer,
    welcomeScreen: require('./WelcomeScreenRedux').reducer,
    setting: require('./SettingRedux').reducer,
    order: require('./SalesOrderRedux').reducer,
    customerVirtualAccount: require('./CustomerVirtualAccountRedux').reducer,
    promotion: require('./PromotionRedux').reducer,
    homeContent: require('./HomeContentRedux').reducer,
    customerAccount: require('./CustomerAccountRedux').reducer,
    customerAddress: require('./CustomerAddressRedux').reducer,
    creditTokenTopUp: require('./CreditTokenTopUpRedux').reducer,
    customerOrder: require('./CustomerOrderRedux').reducer,
    customerNotification: require('./CustomerNotificationRedux').reducer,
    myMealPlan: require('./MyMealPlanRedux').reducer,
    forgotPassword: require('./ForgotPasswordRedux').reducer,
    theme: require('./ThemeRedux').reducer,
    startup: require('./StartupRedux').reducer,
    mealDetailPopUp: require('./MealDetailPopUpRedux').reducer,
    autoRouting: require('./AutoRoutingRedux').reducer,
    lastUsed: require('./V2/LastUsedRedux').reducer,
  })

  return configureStore(rootReducer, rootSaga)
}
