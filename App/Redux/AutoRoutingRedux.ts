import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface AutoRoutingCreators {
  addRoute: (routeName: string, params: any, isOpeningApp: boolean) => AnyAction
  setRoute: (routeName: string, params: any) => AnyAction
  clearRoute: () => AnyAction
}

export interface AutoRoutingTypes {
  ADD_ROUTE: 'ADD_ROUTE'
  SET_ROUTE: 'SET_ROUTE'
  CLEAR_ROUTE: 'CLEAR_ROUTE'
}
const { Types, Creators } = createActions<AutoRoutingTypes, AutoRoutingCreators>({
  addRoute: ['routeName', 'params', 'isOpeningApp'],
  setRoute: ['routeName', 'params'],
  clearRoute: null
})

export const AutoRoutingTypes = Types
export default Creators

export interface InitAutoRouting {
  routeName: string,
  params: any
}

export const INITIAL_STATE = {
  routeName: null,
  params: null
}

export const setRoute: any = (state: InitAutoRouting, { routeName, params }: { routeName: string, params: any }): InitAutoRouting => {
  return { ...state, routeName, params }
}
export const clearRoute: any = (): InitAutoRouting => INITIAL_STATE

export const reducer: (state: InitAutoRouting, action: any) => InitAutoRouting = createReducer(INITIAL_STATE, {
  [Types.SET_ROUTE]: setRoute,
  [Types.CLEAR_ROUTE]: clearRoute
})
