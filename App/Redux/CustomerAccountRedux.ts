import {
  createActions,
  createReducer,
} from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { AnyAction } from 'redux'

export interface CustomerAccountReduxCreators {
  fetchWallet: () => AnyAction
  fetchingWallet: () => AnyAction
  fetchingWalletDone: (wallet: Wallet) => AnyAction
  fetchingWalletFailed: (error: string) => AnyAction
  changePassword: (
    old_password: string, password: string,
    password_confirmation: string) => AnyAction
  changingPassword: () => AnyAction
  changePasswordSuccess: () => AnyAction
  changePasswordFailed: (error: string) => AnyAction
  clearChangePasswordSuccess: () => AnyAction
  reset: () => AnyAction,
  resetChangeEmailAndPhone: () => AnyAction
  changingPhoneNumber: () => AnyAction
  changePhoneNumber: (newPhoneNumber: string) => AnyAction
  changePhoneNumberSuccess: () => AnyAction
  changePhoneNumberFailed: (error: string) => AnyAction
  changePhoneNumberValidate: (
    phone: string, otp: string, countryCode: string) => AnyAction
  changingEmail: () => AnyAction
  changeEmail: (newEmail: string) => AnyAction
  changeEmailSuccess: () => AnyAction
  changeEmailFailed: (error: string) => AnyAction
  sendOtpNumber: (newPhoneNumber: string) => AnyAction
  checkValidateOtp: () => AnyAction
  checkValidateOtpDone: () => AnyAction
  checkValidateOtpFailed: (error: string) => AnyAction
  checkClientDone: (isClient: boolean) => AnyAction
  checkClientFailed: () => AnyAction
}

export interface CustomerAccountReduxTypes {
  FETCH_WALLET: 'FETCH_WALLET'
  FETCHING_WALLET: 'FETCHING_WALLET'
  FETCHING_WALLET_DONE: 'FETCHING_WALLET_DONE'
  FETCHING_WALLET_FAILED: 'FETCHING_WALLET_FAILED'
  CHANGE_PASSWORD: 'CHANGE_PASSWORD'
  CHANGING_PASSWORD: 'CHANGING_PASSWORD'
  CHANGE_PASSWORD_SUCCESS: 'CHANGE_PASSWORD_SUCCESS'
  CHANGE_PASSWORD_FAILED: 'CHANGE_PASSWORD_FAILED'
  CLEAR_CHANGE_PASSWORD_SUCCESS: 'CLEAR_CHANGE_PASSWORD_SUCCESS'
  RESET: 'RESET'
  RESET_CHANGE_EMAIL_AND_PHONE: 'RESET_CHANGE_EMAIL_AND_PHONE'
  CHANGING_PHONE_NUMBER: 'CHANGING_PHONE_NUMBER'
  CHANGE_PHONE_NUMBER: 'CHANGE_PHONE_NUMBER'
  CHANGE_PHONE_NUMBER_SUCCESS: 'CHANGE_PHONE_NUMBER_SUCCESS'
  CHANGE_PHONE_NUMBER_FAILED: 'CHANGE_PHONE_NUMBER_FAILED'
  CHANGE_PHONE_NUMBER_VALIDATE: 'CHANGE_PHONE_NUMBER_VALIDATE'
  CHANGING_EMAIL: 'CHANGING_EMAIL'
  CHANGE_EMAIL: 'CHANGE_EMAIL'
  CHANGE_EMAIL_SUCCESS: 'CHANGE_EMAIL_SUCCESS'
  CHANGE_EMAIL_FAILED: 'CHANGE_EMAIL_FAILED'
  SEND_OTP_NUMBER: 'SEND_OTP_NUMBER'
  CHECK_VALIDATE_OTP: 'CHECK_VALIDATE_OTP'
  CHECK_VALIDATE_OTP_DONE: 'CHECK_VALIDATE_OTP_DONE'
  CHECK_VALIDATE_OTP_FAILED: 'CHECK_VALIDATE_OTP_FAILED'
  CHECK_CLIENT_DONE: 'CHECK_CLIENT_DONE'
  CHECK_CLIENT_FAILED: 'CHECK_CLIENT_FAILED'
}

const { Types, Creators } = createActions<CustomerAccountReduxTypes, CustomerAccountReduxCreators>(
  {
    fetchWallet: null,
    fetchingWallet: null,
    fetchingWalletDone: ['wallet'],
    fetchingWalletFailed: ['error'],
    changePassword: [
      'old_password',
      'password',
      'password_confirmation',
    ],
    changingPassword: [null],
    changePasswordSuccess: [null],
    changePasswordFailed: ['error'],
    clearChangePasswordSuccess: [null],
    reset: null,
    resetChangeEmailAndPhone: null,
    changingPhoneNumber: [null],
    changePhoneNumber: ['newPhoneNumber'],
    changePhoneNumberSuccess: [null],
    changePhoneNumberFailed: ['error'],
    changePhoneNumberValidate: [
      'phone',
      'otp',
      'countryCode',
    ],
    changingEmail: [null],
    changeEmail: ['newEmail'],
    changeEmailSuccess: [null],
    changeEmailFailed: ['error'],
    sendOtpNumber: ['newPhoneNumber'],
    checkValidateOtp: null,
    checkValidateOtpDone: null,
    checkValidateOtpFailed: ['error'],
    checkClientDone: ['isClient'],
    checkClientFailed: null,
  })

export const CustomerAccountTypes = Types
export default Creators

export interface Wallet {
  error: boolean
  data: number
  personal?: number
  corporate?: number
}

interface CustomerAccount {
  wallet: number
  personalBalance: number
  corporateBalance: number
  fetchingWallet: boolean
  walletError?: string
  changingPassword: boolean
  changePasswordSuccess?: boolean
  changePasswordError: string
  changingEmail: boolean
  changeEmailSuccess: boolean
  changeEmailError?: string
  changingPhoneNumber: boolean
  changePhoneNumberSuccess?: boolean
  changePhoneNumberError?: string
  checkValidateOtp: boolean
  checkValidateOtpDone: boolean
  checkValidateOtpFailed?: string
  isClient: boolean
  validatingIdentity?: boolean
  identityExist?: boolean
}

export const INITIAL_STATE = Immutable<CustomerAccount>({
  wallet: 0,
  personalBalance: 0,
  corporateBalance: 0,
  fetchingWallet: false,
  walletError: null,
  changingPassword: false,
  changePasswordSuccess: null,
  changePasswordError: '',
  changingEmail: false,
  changeEmailSuccess: null,
  changeEmailError: null,
  changingPhoneNumber: false,
  changePhoneNumberSuccess: null,
  changePhoneNumberError: null,
  checkValidateOtp: false,
  checkValidateOtpDone: null,
  checkValidateOtpFailed: '',
  isClient: false,
})

export type CustomerAccountReduxState = typeof INITIAL_STATE

export const fetchingWallet: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    fetchingWallet: true,
    walletError: null,
  }
}

export const fetchingWalletDone: any = (
  state: CustomerAccountReduxState,
  { wallet }: { wallet: Wallet },
): CustomerAccountReduxState => {
  return {
    ...state,
    fetchingWallet: false,
    wallet: wallet.data,
    personalBalance: typeof wallet.personal !== 'undefined'
      ? wallet.personal
      : 0,
    corporateBalance: typeof wallet.corporate !== 'undefined'
      ? wallet.corporate
      : 0,
    walletError: null,
  }
}

export const fetchingWalletFailed: any = (
  state: CustomerAccountReduxState,
  { walletError }: { walletError: string },
): CustomerAccountReduxState => {
  return {
    ...state,
    fetchingWallet: false,
    walletError,
    wallet: 0,
  }
}

export const changingPassword: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changingPassword: true,
    changePasswordSuccess: null,
    changePasswordError: '',
  }
}

export const changePasswordSuccess: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changingPassword: false,
    changePasswordSuccess: true,
  }
}

export const changePasswordFailed: any = (
  state: CustomerAccountReduxState,
  { error }: { error: string },
): CustomerAccountReduxState => {
  return {
    ...state,
    changingPassword: false,
    changePasswordSuccess: false,
    changePasswordError: error,
  }
}
export const clearChangePasswordSuccess: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changePasswordSuccess: null,
    changePasswordError: '',
  }
}

export const resetChangeEmailAndPhone: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changingPassword: false,
    changePasswordSuccess: null,
    changePasswordError: '',
    changingEmail: false,
    changeEmailSuccess: null,
    changeEmailError: null,
    changingPhoneNumber: false,
    changePhoneNumberSuccess: null,
    changePhoneNumberError: null,
    checkValidateOtp: false,
    checkValidateOtpDone: false,
    checkValidateOtpFailed: null,
    validatingIdentity: false,
    identityExist: null,
  }
}

export const reset: any = (): CustomerAccountReduxState => INITIAL_STATE

export const changingPhoneNumber: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changingPhoneNumber: true,
    changePhoneNumberSuccess: null,
    changePhoneNumberError: null,
  }
}

export const changePhoneNumberSuccess: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changingPhoneNumber: false,
    changePhoneNumberSuccess: true,
  }
}

export const changePhoneNumberFailed: any = (
  state: CustomerAccountReduxState,
  { error }: { error: string },
): CustomerAccountReduxState => {
  return {
    ...state,
    changingPhoneNumber: false,
    changePhoneNumberSuccess: false,
    changePhoneNumberError: error,
  }
}

export const changingEmail: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changingEmail: true,
    changeEmailSuccess: null,
    changeEmailError: null,
  }
}

export const changeEmailSuccess: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    changingEmail: false,
    changeEmailSuccess: true,
  }
}

export const changeEmailFailed: any = (
  state: CustomerAccountReduxState,
  { error }: { error: string },
): CustomerAccountReduxState => {
  return {
    ...state,
    changingEmail: false,
    changeEmailSuccess: null,
    changeEmailError: error,
  }
}

// VALIDASI OTP
export const checkValidateOtp: any = (state: CustomerAccountReduxState) => {
  return {
    ...state,
    checkValidateOtp: true,
    checkValidateOtpDone: false,
  }
}

export const checkValidateOtpDone: any = (state: CustomerAccountReduxState) => {
  return {
    ...state,
    checkValidateOtp: false,
    checkValidateOtpFailed: null,
    checkValidateOtpDone: true,
  }
}

export const checkValidateOtpFailed: any = (
  state: CustomerAccountReduxState,
  { error }: { error: string },
): CustomerAccountReduxState => {
  return {
    ...state,
    checkValidateOtp: false,
    checkValidateOtpFailed: error,
    checkValidateOtpDone: false,
  }
}

export const checkClientDone: any = (
  state: CustomerAccountReduxState,
  { isClient }: { isClient: boolean },
): CustomerAccountReduxState => {
  return {
    ...state,
    isClient,
  }
}

export const checkClientFailed: any = (state: CustomerAccountReduxState): CustomerAccountReduxState => {
  return {
    ...state,
    isClient: false,
  }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer: (
  state: CustomerAccountReduxState,
  action: any) => CustomerAccountReduxState = createReducer(
  INITIAL_STATE, {
    [Types.FETCHING_WALLET]: fetchingWallet,
    [Types.FETCHING_WALLET_DONE]: fetchingWalletDone,
    [Types.FETCHING_WALLET_FAILED]: fetchingWalletFailed,
    [Types.CHANGING_PASSWORD]: changingPassword,
    [Types.CHANGE_PASSWORD_SUCCESS]: changePasswordSuccess,
    [Types.CHANGE_PASSWORD_FAILED]: changePasswordFailed,
    [Types.CLEAR_CHANGE_PASSWORD_SUCCESS]: clearChangePasswordSuccess,
    [Types.RESET]: reset,
    [Types.CHANGING_PHONE_NUMBER]: changingPhoneNumber,
    [Types.CHANGE_PHONE_NUMBER_FAILED]: changePhoneNumberFailed,
    [Types.CHANGE_PHONE_NUMBER_SUCCESS]: changePhoneNumberSuccess,
    [Types.CHANGING_EMAIL]: changingEmail,
    [Types.CHANGE_EMAIL_SUCCESS]: changeEmailSuccess,
    [Types.CHANGE_EMAIL_FAILED]: changeEmailFailed,
    [Types.RESET_CHANGE_EMAIL_AND_PHONE]: resetChangeEmailAndPhone,
    [Types.CHECK_VALIDATE_OTP]: checkValidateOtp,
    [Types.CHECK_VALIDATE_OTP_DONE]: checkValidateOtpDone,
    [Types.CHECK_VALIDATE_OTP_FAILED]: checkValidateOtpFailed,
    [Types.CHECK_CLIENT_DONE]: checkClientDone,
    [Types.CHECK_CLIENT_FAILED]: checkClientFailed,
  })
