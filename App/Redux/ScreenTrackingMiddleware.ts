import { NavigationActions, NavigationState } from 'react-navigation'
import analytics from '@react-native-firebase/analytics'
import { AnyAction, Dispatch } from 'redux'

// gets the current screen from navigation state
const getCurrentRouteName = (navigationState: NavigationState) : string => {
  if (!navigationState) {
    return null
  }
  const route = navigationState.routes[navigationState.index]
  // dive into nested navigators
  if (route.routes) {
    return getCurrentRouteName(route)
  }
  return route.routeName
}
const screenTracking = ({ getState }) => (next: Dispatch<AnyAction>) => (action: AnyAction) => {
  if (
    action.type !== NavigationActions.NAVIGATE &&
    action.type !== NavigationActions.BACK
  ) {
    return next(action)
  }

  const result = next(action)
  const nextScreen = getCurrentRouteName(getState().nav)
  analytics().setCurrentScreen(nextScreen)
  return result
}

export default screenTracking
