import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'
import analytics from '@react-native-firebase/analytics'
import Analytics from '../Services/LogEventService'
import { registerApiResponse } from '../Services/ApiCustomer'

export interface RegisterActionsCreators {
  resetRegister: () => AnyAction,
  registerCustomer: (customer: CustomerForm) => AnyAction,
  reset: () => AnyAction,
  registrationSuccess: (customer: registerApiResponse) => AnyAction,
  registrationFailed: (error: string) => AnyAction,
  registeringCustomer: () => AnyAction,
  verifyOtp: (phone: string, otp: string, countryCode: string) => AnyAction,
  verifyingOtp: () => AnyAction,
  verifyOtpDone: () => AnyAction,
  verifyOtpFailed: (error: string) => AnyAction,
  validatingIdentity: () => AnyAction,
  validateIdentity: (identity: string) => AnyAction,
  identityExist: () => AnyAction,
  identityNotExist: () => AnyAction,
  validateEmailPhone: (email: string, phone: string) => AnyAction,
  validatingEmailPhone: () => AnyAction,
  emailPhoneExist: () => AnyAction,
  emailPhoneNotExist: () => AnyAction,
  validateRegisteredWithFacebook: (customer: CustomerForm) => AnyAction,
  validatingRegisterWithFacebook: () => AnyAction,
  customerIsRegisteredWithFacebook: () => AnyAction,
  customerNotRegisteredWithFacebook: () => AnyAction,
  resetOtpErrorMessage: () => AnyAction,
  resetRegistrationErrorMessage: () => AnyAction,
  resetChangeOtpStatus: () => AnyAction

}

export interface RegisterActionsTypes {
  RESET_REGISTER: 'RESET_REGISTER',
  REGISTER_CUSTOMER: 'REGISTER_CUSTOMER',
  RESET: 'RESET',
  REGISTRATION_SUCCESS: 'REGISTRATION_SUCCESS',
  REGISTRATION_FAILED: 'REGISTRATION_FAILED',
  REGISTERING_CUSTOMER: 'REGISTERING_CUSTOMER',
  VERIFY_OTP: 'VERIFY_OTP',
  VERIFYING_OTP: 'VERIFYING_OTP',
  VERIFY_OTP_DONE: 'VERIFY_OTP_DONE',
  VERIFY_OTP_FAILED: 'VERIFY_OTP_FAILED',
  VALIDATE_IDENTITY: 'VALIDATE_IDENTITY',
  VALIDATING_IDENTITY: 'VALIDATING_IDENTITY',
  IDENTITY_EXIST: 'IDENTITY_EXIST',
  IDENTITY_NOT_EXIST: 'IDENTITY_NOT_EXIST',
  VALIDATE_EMAIL_PHONE: 'VALIDATE_EMAIL_PHONE',
  VALIDATING_EMAIL_PHONE: 'VALIDATING_EMAIL_PHONE',
  EMAIL_PHONE_EXIST: 'EMAIL_PHONE_EXIST',
  EMAIL_PHONE_NOT_EXIST: 'EMAIL_PHONE_NOT_EXIST',
  VALIDATE_REGISTERED_WITH_FACEBOOK: 'VALIDATE_REGISTERED_WITH_FACEBOOK',
  VALIDATING_REGISTER_WITH_FACEBOOK: 'VALIDATING_REGISTER_WITH_FACEBOOK',
  CUSTOMER_IS_REGISTERED_WITH_FACEBOOK: 'CUSTOMER_IS_REGISTERED_WITH_FACEBOOK',
  CUSTOMER_NOT_REGISTERED_WITH_FACEBOOK: 'CUSTOMER_NOT_REGISTERED_WITH_FACEBOOK',
  RESET_OTP_ERROR_MESSAGE: 'RESET_OTP_ERROR_MESSAGE',
  RESET_REGISTRATION_ERROR_MESSAGE: 'RESET_REGISTRATION_ERROR_MESSAGE',
  RESET_CHANGE_OTP_STATUS: 'RESET_CHANGE_OTP_STATUS',
}

export interface CustomerForm {
  countryCode?: string,
  email: string,
  first_name?: string,
  last_name?: string,
  name?: string,
  password?: string,
  password_confirmation?: string,
  phone?: string,
  salutation?: string,
  social_login_id?: string,
  social_login_type?: string,
  facebook_credentials?: string,
  registration_type?: string,
  login_type?: string
  facebook_id?: string
  apple_id?: string
}
export interface CustomerData {
  id: number,
  newsletter_subscribed: number,
  invitation_code: string,
  invitation_uri: string,
  current_credits: number,
  invite_code: string
  email: string,
  first_name: string,
  last_name: string,
  name: string,
  phone: string,
  photo: string,
  salutation: string,
  social_login_id: string,
  social_login_type: string,
  registration_type: string,
}

export interface initStateRegister {
  customer_name: string,
  email: string,
  phone: string,
  password: string,
  isRegisterWithFacebook: boolean,
  registering: boolean,
  registrationError: string,
  verifyingOtpState: boolean,
  verifyOtpSuccess: boolean,
  verifyOtpError: string,
  customerData: CustomerData,
  token: string,
  validatingIdentity: boolean,
  identityExist: boolean,
  validatingEmailPhone: boolean,
  emailPhoneExist: boolean,
  validatingRegisterWithFacebook: boolean,
  customerRegisteredWithFacebook: boolean
}

const { Types, Creators } = createActions<RegisterActionsTypes, RegisterActionsCreators>({
  registerCustomer: ['customer'],
  reset: null,
  registrationSuccess: ['customer'],
  registrationFailed: ['error'],
  registeringCustomer: null,
  verifyOtp: ['phone', 'otp', 'countryCode'],
  verifyingOtp: null,
  verifyOtpDone: null,
  verifyOtpFailed: ['error'],
  validatingIdentity: [null],
  validateIdentity: ['identity'],
  identityExist: [null],
  identityNotExist: [null],
  validateEmailPhone: ['email', 'phone'],
  validatingEmailPhone: [null],
  emailPhoneExist: [null],
  emailPhoneNotExist: [null],
  validateRegisteredWithFacebook: ['customer'],
  validatingRegisterWithFacebook: [null],
  customerIsRegisteredWithFacebook: [null],
  customerNotRegisteredWithFacebook: [null],
  resetOtpErrorMessage: null,
  resetRegistrationErrorMessage: null,
  resetChangeOtpStatus: null
})

export const RegisterTypes = Types
export default Creators

export const INITIAL_STATE: initStateRegister = {
  customer_name: null,
  email: null,
  phone: null,
  password: null,
  isRegisterWithFacebook: false,
  registering: false,
  registrationError: null,
  verifyingOtpState: false,
  verifyOtpSuccess: false,
  verifyOtpError: null,
  customerData: null,
  token: null,
  validatingIdentity: false,
  identityExist: null,
  validatingEmailPhone: false,
  emailPhoneExist: null,
  validatingRegisterWithFacebook: false,
  customerRegisteredWithFacebook: null
}

export const registrationSuccess: any = (state: initStateRegister, { customer }: { customer:registerApiResponse }): initStateRegister => {
  const registration_type = {
    method: customer.data.registration_type
  }
  analytics().setUserId(String(customer.data.id))
  Analytics.logEvent('sign_up', registration_type, ['default'])

  return {
    ...state,
    registering: false,
    registrationError: null,
    customerData: customer.data,
    token: customer.token
  }
}
export const registrationFailed: any = (state: initStateRegister, { error }: { error: string }): initStateRegister => {
  return { ...state, registering: false, registrationError: error }
}
export const registeringCustomer: any = (state: initStateRegister): initStateRegister => {
  return { ...state, registering: true }
}
export const reset = () => INITIAL_STATE
export const verifyingOtp: any = (state: initStateRegister): initStateRegister => {
  return { ...state, verifyingOtpState: true, verifyOtpSuccess: false }
}
export const verifyOtpDone: any = (state: initStateRegister): initStateRegister => {
  return { ...state, verifyingOtpState: false, verifyOtpError: null, verifyOtpSuccess: true }
}
export const verifyOtpFailed: any = (state: initStateRegister, { error }: { error: string }): initStateRegister => {
  return { ...state, verifyingOtpState: false, verifyOtpError: error, verifyOtpSuccess: false }
}
export const resetChangeOtpStatus: any = (state: initStateRegister): initStateRegister => {
  return { ...state, verifyingOtpState: false, verifyOtpError: null, verifyOtpSuccess: false }
}
export const validatingIdentity: any = (state: initStateRegister): initStateRegister => {
  return { ...state, validatingIdentity: true }
}
export const identityExist: any = (state: initStateRegister): initStateRegister => {
  return { ...state, validatingIdentity: false, identityExist: true }
}
export const identityNotExist: any = (state: initStateRegister): initStateRegister => {
  return { ...state, validatingIdentity: false, identityExist: false }
}
export const validatingEmailPhone: any = (state: initStateRegister): initStateRegister => {
  return { ...state, validatingEmailPhone: true, emailPhoneExist: null }
}
export const emailPhoneExist: any = (state: initStateRegister): initStateRegister => {
  return { ...state, validatingEmailPhone: false, emailPhoneExist: true }
}
export const emailPhoneNotExist: any = (state: initStateRegister): initStateRegister => {
  return { ...state, validatingEmailPhone: false, emailPhoneExist: false }
}
export const validatingRegisterWithFacebook: any = (state: initStateRegister): initStateRegister => {
  return { ...state, validatingRegisterWithFacebook: true }
}
export const customerIsRegisteredWithFacebook: any = (state: initStateRegister): initStateRegister => {
  return { ...state, customerRegisteredWithFacebook: true, validatingRegisterWithFacebook: false }
}
export const customerNotRegisteredWithFacebook: any = (state: initStateRegister): initStateRegister => {
  return { ...state, customerRegisteredWithFacebook: false, validatingRegisterWithFacebook: false }
}
export const resetOtpErrorMessage: any = (state: initStateRegister): initStateRegister => {
  return { ...state, verifyOtpError: null }
}
export const resetRegistrationErrorMessage: any = (state: initStateRegister): initStateRegister => {
  return { ...state, registrationError: null }
}

export const reducer: (state: initStateRegister, action: any) => initStateRegister = createReducer(INITIAL_STATE, {
  [Types.REGISTERING_CUSTOMER]: registeringCustomer,
  [Types.RESET]: reset,
  [Types.REGISTRATION_SUCCESS]: registrationSuccess,
  [Types.REGISTRATION_FAILED]: registrationFailed,
  [Types.VERIFYING_OTP]: verifyingOtp,
  [Types.VERIFY_OTP_DONE]: verifyOtpDone,
  [Types.VERIFY_OTP_FAILED]: verifyOtpFailed,
  [Types.VALIDATING_IDENTITY]: validatingIdentity,
  [Types.IDENTITY_EXIST]: identityExist,
  [Types.IDENTITY_NOT_EXIST]: identityNotExist,
  [Types.VALIDATING_EMAIL_PHONE]: validatingEmailPhone,
  [Types.EMAIL_PHONE_EXIST]: emailPhoneExist,
  [Types.EMAIL_PHONE_NOT_EXIST]: emailPhoneNotExist,
  [Types.VALIDATING_REGISTER_WITH_FACEBOOK]: validatingRegisterWithFacebook,
  [Types.CUSTOMER_IS_REGISTERED_WITH_FACEBOOK]: customerIsRegisteredWithFacebook,
  [Types.CUSTOMER_NOT_REGISTERED_WITH_FACEBOOK]: customerNotRegisteredWithFacebook,
  [Types.RESET_OTP_ERROR_MESSAGE]: resetOtpErrorMessage,
  [Types.RESET_REGISTRATION_ERROR_MESSAGE]: resetRegistrationErrorMessage,
  [Types.RESET_CHANGE_OTP_STATUS]: resetChangeOtpStatus
})
