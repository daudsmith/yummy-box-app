import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface CustomeOrderActionsCreators {
  resetCustomerOrder: () => AnyAction,
  fetchOrderList: (silent: boolean) => AnyAction,
  fetchingOrderList: () => AnyAction,
  fetchingOrderListDone: (list: List) => AnyAction,
  fetchingOrderListFailed: (error: string) => AnyAction,
  fetchOrderDetail: (id: number, orderType: string) => AnyAction,
  fetchingOrderDetail: () => AnyAction,
  fetchingOrderDetailDone: (detail: SalesOrderDetail) => AnyAction,
  fetchingOrderDetailFailed: (error: string) => AnyAction,
  clearOrderDetail: () => AnyAction,
  setModifiedOrderDetail: () => AnyAction,
  resetModifiedOrderDetail: () => AnyAction,
  clearModifiedOrderDetail: () => AnyAction,
  stopSubscription: (id: number) => AnyAction,
  stoppingSubscription: () => AnyAction,
  stopSubscriptionSuccess: () => AnyAction,
  stopSubscriptionFailed: (error: string) => AnyAction,
  clearSuccessState: () => AnyAction,
  updateModifySubscriptionData: (subscription: SalesOrderDetail) => AnyAction,
  updateSubscriptionDefaultData: (key: string[], value: Array<string>) => AnyAction,
  setSubscriptionNewCreditCard: (newCreditCardInfo: number) => AnyAction,
  modifySubscription: () => AnyAction,
  modifyingSubscription: () => AnyAction,
  modifySubscriptionSuccess: () => AnyAction,
  modifySubscriptionFailed: (error: string) => AnyAction,
  resetModifySubscriptionState: () => AnyAction,
  clearMealDetail: () => AnyAction,
  cancelCustomerOrder: (id: number) => AnyAction,
  cancellingOrder: () => AnyAction,
  cancellingOrderDone: (id: number, message: string) => AnyAction,
  cancellingOrderFailed: (error: string) => AnyAction,
  resetErrorMessage: () => AnyAction,
}

export interface CustomeOrderActionsTypes {
  RESET_CUSTOMER_ORDER: 'RESET_CUSTOMER_ORDER',
  FETCH_ORDER_LIST: 'FETCH_ORDER_LIST'
  FETCHING_ORDER_LIST: 'FETCHING_ORDER_LIST',
  FETCHING_ORDER_LIST_DONE: 'FETCHING_ORDER_LIST_DONE',
  FETCHING_ORDER_LIST_FAILED: 'FETCHING_ORDER_LIST_FAILED',
  FETCH_ORDER_DETAIL: 'FETCH_ORDER_DETAIL'
  FETCHING_ORDER_DETAIL: 'FETCHING_ORDER_DETAIL',
  FETCHING_ORDER_DETAIL_DONE: 'FETCHING_ORDER_DETAIL_DONE',
  FETCHING_ORDER_DETAIL_FAILED: 'FETCHING_ORDER_DETAIL_FAILED',
  CLEAR_ORDER_DETAIL: 'CLEAR_ORDER_DETAIL',
  SET_MODIFIED_ORDER_DETAIL: 'SET_MODIFIED_ORDER_DETAIL',
  CLEAR_MODIFIED_ORDER_DETAIL: 'CLEAR_MODIFIED_ORDER_DETAIL',
  STOP_SUBSCRIPTION: 'STOP_SUBSCRIPTION'
  STOPPING_SUBSCRIPTION: 'STOPPING_SUBSCRIPTION',
  STOP_SUBSCRIPTION_SUCCESS: 'STOP_SUBSCRIPTION_SUCCESS',
  STOP_SUBSCRIPTION_FAILED: 'STOP_SUBSCRIPTION_FAILED',
  CLEAR_SUCCESS_STATE: 'CLEAR_SUCCESS_STATE',
  UPDATE_MODIFY_SUBSCRIPTION_DATA: 'UPDATE_MODIFY_SUBSCRIPTION_DATA',
  UPDATE_SUBSCRIPTION_DEFAULT_DATA: 'UPDATE_SUBSCRIPTION_DEFAULT_DATA'
  RESET_MODIFIED_ORDER_DETAIL: 'RESET_MODIFIED_ORDER_DETAIL',
  MODIFY_SUBSCRIPTION: 'MODIFY_SUBSCRIPTION'
  MODIFYING_SUBSCRIPTION: 'MODIFYING_SUBSCRIPTION',
  MODIFY_SUBSCRIPTION_SUCCESS: 'MODIFY_SUBSCRIPTION_SUCCESS',
  MODIFY_SUBSCRIPTION_FAILED: 'MODIFY_SUBSCRIPTION_FAILED',
  SET_SUBSCRIPTION_NEW_CREDIT_CARD: 'SET_SUBSCRIPTION_NEW_CREDIT_CARD',
  RESET_MODIFY_SUBSCRIPTION_STATE: 'RESET_MODIFY_SUBSCRIPTION_STATE',
  CLEAR_MEAL_DETAIL: 'CLEAR_MEAL_DETAIL'
  CANCEL_CUSTOMER_ORDER: 'CANCEL_CUSTOMER_ORDER'
  CANCELLING_ORDER: 'CANCELLING_ORDER',
  CANCELLING_ORDER_DONE: 'CANCELLING_ORDER_DONE',
  CANCELLING_ORDER_FAILED: 'CANCELLING_ORDER_FAILED',
  RESET_ERROR_MESSAGE: 'RESET_ERROR_MESSAGE',
}

export interface List {
  order: {
    in_progress: ListOrderDetail[]
    complete: ListOrderDetail[]
  }
  subscription: {
    in_progress: ListSubscriptionDetail[]
    complete: ListSubscriptionDetail[]
  }
}

export interface ListOrderDetail {
  id: number
  number: string
  status: string
  total: number
  payment: string
  payment_method: string
  total_item_in_cart: number
  next_delivery_date: string
  next_delivery_time: string
  order_date: string
  type: string
  order_timestamp: string
  total_free_charge: number
  timeline: string
}

export interface ListSubscriptionDetail {
  id: number
  number: string
  status: string
  payment: string
  payment_method: string
  total_item_in_cart: number
  next_delivery_date: string
  next_delivery_time: string
  order_date: string
  type: string
  timeline: string
  order_timestamp: string
  theme: string
  start_date: string
  repeat_every: string
  repeat_interval: string
}
export interface DeliveryItemDetailAvailability {
  base_price: number,
  category_id: number,
  date: string,
  id: number,
  item_id: number,
  item_name: number,
  quantity: number,
  remaining_quantity: number,
  sale_price: number
}

export interface DeliveryItemDetailCategory {
  created_at: string,
  icon_file: string,
  id: number,
  name: string,
  status: string,
  updated_at: string
}
export interface DeliveryItemDetailImage {
  amount: number,
  have_child: boolean,
  id: number,
  name: string,
  unit: string
}
export interface DeliveryItemDetailNutrition {
  id: number,
  info: string,
  is_catalog_image: boolean,
  path: string,
  sort: number,
  thumbs: {
    medium_thumb: string,
    small_thumb: string
  }
}
export interface DeliveryItemDetailTag {
  id: number,
  alias: string,
  created_at: boolean,
  created_by: number,
  description: string,
  display_on_catalog: number,
  filter_type: string,
  filterable: number,
  have_child: boolean,
  icon: string,
  name: string,
  parent_id: number,
  parent_name: string,
  updated_at: string,
  updated_by: number
}
export interface DeliveryItemDetail {
  availabilities: {
    data: DeliveryItemDetailAvailability[]
  },
  base_pice: number,
  catalog_sorting: 0,
  categories: {
    data: DeliveryItemDetailCategory[]
  },
  cogs: number,
  created_at: string,
  description: string,
  home_sorting: number,
  id: number,
  images: {
    data: DeliveryItemDetailImage[]
  },
  ingredients: string,
  kitchen_meal_name: string,
  name: string,
  nutrition: {
    data: DeliveryItemDetailNutrition[]
  },
  playlist_only: string,
  short_description: string,
  sku: string,
  tags: {
    data: DeliveryItemDetailTag[]
  },
  tax_class: string,
  updated_at: string,
  visibility: number
}

export interface DeliveryItem {
  created_at: {
    date: string,
    timezone: string,
    timezone_type: number
  },
  detail: {
    data: DeliveryItemDetail
  },
  quantity: number,
  status: string,
  subtotal: number,
  unit_price: number,
  updated_at: {
    date: string,
    timezone: string,
    timezone_type: number
  }
}
export interface Delivery {
  address: string,
  address_details: string,
  can_cancel: boolean
  can_modify: boolean,
  created_at: string,
  date: string,
  delivery_date: string,
  delivery_fee: number,
  free_charged: number,
  id: number,
  items: {
    data: DeliveryItem[]
  },
  landmark: string,
  latitude: string,
  longitude: string,
  name: string,
  note: string,
  number: string,
  order_id: string,
  order_number: string,
  payment: string,
  phone: string,
  provider: string,
  provider_note: string,
  subtotal: number,
  time: string,
  updated_at: string,
  kitchen_code: string,
}

export interface SubscriptionRawData {
  id: number
  created_at: string
  updated_at: string
  account_id: number
  number: string
  theme_id: number
  status: string
  default_recipient_name: string
  default_recipient_phone: string
  default_delivery_landmark: string
  default_delivery_address: string
  default_delivery_latitude: number
  default_delivery_longitude: number
  default_delivery_details: string
  default_delivery_note: string
  default_payment_method: string
  default_payment_info: object | string
  default_delivery_time: string
  start_date: string
  repeat_every: Array<string>
  repeat_interval: string
  previous_delivery_date: string
  next_delivery_date: string,
  newCreditCardInfo: string
}
export interface SalesOrderDetail {
  id: number
  created_at: string
  updated_at: string
  status: string
  order_number: string
  type: string
  coupon_code: string
  order_comment: string
  source: string
  base_total_charged: number
  base_total_due: number
  base_total_paid: number
  base_total_refunded: number
  base_total_cancelled: number
  tax_amount_charged: number
  tax_amount_due: number
  tax_amount_paid: number
  discount_amount: number
  discount_amount_refunded: number
  discount_amount_cancelled: number
  delivery_fee: number,
  deliveries:{
    data: Delivery[]
  }
  total_amount_charged: number
  total_amount_due: number
  total_amount_paid: number
  total_amount_refunded: number
  total_amount_cancelled: number
  account_name: string
  payment: string
  total_item_in_cart: number
  client_id: number
  meal_function_id: number
  meal_function_name: string
  channel_id: number
  total_free_charged: number,
  subscription_raw_data: SubscriptionRawData
}


export interface SalesOrderSubscriptionDetail {
  subscription: {
    id: number
    number: string
    status: string
    created_at: string
    theme: string
    theme_image: string,
    payment :{
      method: string
    }
    start_date: string
    repeat_every: string
    repeat_interval: string
  },
  schedules: Array<string>
  subscription_raw_data: SubscriptionRawData
}

export interface CustomerOrderState {
  orderList: {
    order: {
      in_progress: ListOrderDetail[]
      complete: ListOrderDetail[]
    }
    subscription: {
      in_progress: ListSubscriptionDetail[]
      complete: ListSubscriptionDetail[]
    }
  }
  fetchingOrderList: boolean
  orderListError: string
  orderDetail: SalesOrderDetail
  modifiedOrderDetail: SalesOrderDetail
  fetchingOrderDetail: boolean
  fetchingMealDetail: boolean
  orderDetailError: string
  stopping: boolean
  modifying: boolean
  stopSubscriptionSuccess: boolean
  modifySubscriptionSuccess: boolean
  error: string
  mealDetailError: string
  cancelledOrder: boolean
  cancelling: boolean
  message: string
}

const { Types, Creators } = createActions<CustomeOrderActionsTypes, CustomeOrderActionsCreators>({
  resetCustomerOrder: null,
  fetchOrderList: ['silent'],
  fetchingOrderList: null,
  fetchingOrderListDone: ['list'],
  fetchingOrderListFailed: ['error'],
  fetchOrderDetail: [
    'id',
    'orderType',
  ],
  fetchingOrderDetail: null,
  fetchingOrderDetailDone: ['detail'],
  fetchingOrderDetailFailed: ['error'],
  clearOrderDetail: null,
  setModifiedOrderDetail: null,
  resetModifiedOrderDetail: null,
  clearModifiedOrderDetail: null,
  stopSubscription: ['id'],
  stoppingSubscription: null,
  stopSubscriptionSuccess: null,
  stopSubscriptionFailed: ['error'],
  clearSuccessState: null,
  updateModifySubscriptionData: ['subscription'],
  updateSubscriptionDefaultData: [
    'key',
    'value',
  ],
  setSubscriptionNewCreditCard: ['newCreditCardInfo'],
  modifySubscription: null,
  modifyingSubscription: null,
  modifySubscriptionSuccess: null,
  modifySubscriptionFailed: ['error'],
  resetModifySubscriptionState: null,
  clearMealDetail: [null],
  cancelCustomerOrder: ['id'],
  cancellingOrder: [null],
  cancellingOrderDone: [
    'id',
    'message',
  ],
  cancellingOrderFailed: ['error'],
  resetErrorMessage: [null],
})

export const CustomerOrderTypes = Types
export default Creators

export const INITIAL_STATE: CustomerOrderState = {
  orderList: null,
  fetchingOrderList: false,
  orderListError: '',
  orderDetail: null,
  modifiedOrderDetail: null,
  fetchingOrderDetail: false,
  fetchingMealDetail: false,
  orderDetailError: '',
  stopping: false,
  modifying: false,
  stopSubscriptionSuccess: null,
  modifySubscriptionSuccess: null,
  error: null,
  mealDetailError: null,
  cancelledOrder: false,
  cancelling: false,
  message: '',
}

export const resetCustomerOrder: any = (): CustomerOrderState => INITIAL_STATE
export const fetchingOrderList: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    fetchingOrderList: true,
    cancelling: false,
    cancelledOrder: false,
    orderListError: '',
  }
}
export const fetchingOrderListDone: any = (state: CustomerOrderState, { list }: { list: List }): CustomerOrderState => {
  return {
    ...state,
    fetchingOrderList: false,
    orderList: list,
    cancelling: false,
    cancelledOrder: false,
    orderListError: '',
  }
}
export const fetchingOrderListFailed: any = (state: CustomerOrderState, { error } : { error: string }): CustomerOrderState => {
  return {
    ...state,
    fetchingOrderList: false,
    orderList: null,
    cancelling: false,
    cancelledOrder: false,
    orderListError: error,
  }
}
export const fetchingOrderDetail: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    fetchingOrderDetail: true,
    cancelling: false,
    cancelledOrder: false,
    orderDetailError: '',
  }
}
export const fetchingOrderDetailDone: any = (state: CustomerOrderState, { detail } : { detail: SalesOrderDetail }): CustomerOrderState => {
  return {
    ...state,
    fetchingOrderDetail: false,
    orderDetail: detail,
    orderDetailError: '',
  }
}
export const fetchingOrderDetailFailed: any = (state: CustomerOrderState, { error }: { error: string }): CustomerOrderState => {
  return {
    ...state,
    fetchingOrderDetail: false,
    orderDetailError: error,
  }
}
export const clearOrderDetail: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    orderDetail: null,
    cancelledOrder: false,
    cancelling: false,
    message: '',
    stopSubscriptionSuccess: null,
  }
}
export const setModifiedOrderDetail: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    modifiedOrderDetail: {
      ...state.orderDetail,
      subscription_raw_data: {
        ...state.orderDetail.subscription_raw_data,
        newCreditCardInfo: null,
      },
    },
  }
}
export const clearModifiedOrderDetail: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    modifiedOrderDetail: null,
    modifySubscriptionSuccess: null,
    error: null,
  }
}
export const stoppingSubscription: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    stopping: true,
    error: null,
  }
}
export const stopSubscriptionSuccess: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    stopping: false,
    stopSubscriptionSuccess: true,
    modifySubscriptionSuccess: null,
  }
}
export const stopSubscriptionFailed: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    stopping: false,
    stopSubscriptionSuccess: false,
    modifySubscriptionSuccess: null,
  }
}
export const clearSuccessState: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    stopSubscriptionSuccess: null,
    modifySubscriptionSuccess: null,
    error: null,
  }
}
export const updateModifySubscriptionData: any = (state: CustomerOrderState, { subscription }: { subscription: SalesOrderDetail}) => {
  return {
    ...state,
    modifiedOrderDetail: subscription,
  }
}
export const modifyingSubscription: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    modifying: true,
    error: null,
  }
}
export const modifySubscriptionSuccess: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    modifying: false,
    modifySubscriptionSuccess: true,
  }
}
export const modifySubscriptionFailed: any = (state: CustomerOrderState, { error }: { error: string }): CustomerOrderState => {
  return {
    ...state,
    modifying: false,
    modifySubscriptionSuccess: false,
    error,
  }
}
export const resetModifySubscriptionState: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    modifying: false,
    modifySubscriptionSuccess: null,
    error: null,
  }
}
export const resetModifiedOrderDetail: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    modifiedOrderDetail: state.orderDetail,
  }
}
export const setSubscriptionNewCreditCard: any = (state: CustomerOrderState, { newCreditCardInfo } : { newCreditCardInfo: string }) => {
  return {
    ...state,
    modifiedOrderDetail: {
      ...state.modifiedOrderDetail,
      subscription_raw_data: {
        ...state.modifiedOrderDetail.subscription_raw_data,
        newCreditCardInfo,
      },
    },
  }
}
export const cancellingOrder: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    cancelling: true,
    cancelledOrder: false,
    orderDetailError: '',
  }
}
export const cancellingOrderDone: any = (state: CustomerOrderState, { id, message }: { id: number, message: string }): CustomerOrderState => {
  if (typeof state.orderList.order !== 'undefined') {
    let orderIndex = state.orderList.order.in_progress.findIndex(
      obj => obj.id === id)
    if (orderIndex >= 0) {
      state.orderList.order.in_progress.splice(orderIndex, 1)
    }
  }
  return {
    ...state,
    cancelling: false,
    cancelledOrder: true,
    message: message,
    orderDetailError: '',
  }
}
export const cancellingOrderFailed: any = (state: CustomerOrderState, { error }: { error: string }) => {
  return {
    ...state,
    cancelling: false,
    cancelledOrder: false,
    orderDetailError: error,
  }
}
export const resetErrorMessage: any = (state: CustomerOrderState): CustomerOrderState => {
  return {
    ...state,
    orderDetailError: '',
    cancelling: false,
    cancelledOrder: false,
    message: '',
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_CUSTOMER_ORDER]: resetCustomerOrder,
  [Types.FETCHING_ORDER_LIST]: fetchingOrderList,
  [Types.FETCHING_ORDER_LIST_DONE]: fetchingOrderListDone,
  [Types.FETCHING_ORDER_LIST_FAILED]: fetchingOrderListFailed,
  [Types.FETCHING_ORDER_DETAIL]: fetchingOrderDetail,
  [Types.FETCHING_ORDER_DETAIL_DONE]: fetchingOrderDetailDone,
  [Types.FETCHING_ORDER_DETAIL_FAILED]: fetchingOrderDetailFailed,
  [Types.CLEAR_ORDER_DETAIL]: clearOrderDetail,
  [Types.SET_MODIFIED_ORDER_DETAIL]: setModifiedOrderDetail,
  [Types.CLEAR_MODIFIED_ORDER_DETAIL]: clearModifiedOrderDetail,
  [Types.STOPPING_SUBSCRIPTION]: stoppingSubscription,
  [Types.STOP_SUBSCRIPTION_SUCCESS]: stopSubscriptionSuccess,
  [Types.STOP_SUBSCRIPTION_FAILED]: stopSubscriptionFailed,
  [Types.CLEAR_SUCCESS_STATE]: clearSuccessState,
  [Types.UPDATE_MODIFY_SUBSCRIPTION_DATA]: updateModifySubscriptionData,
  [Types.RESET_MODIFIED_ORDER_DETAIL]: resetModifiedOrderDetail,
  [Types.MODIFYING_SUBSCRIPTION]: modifyingSubscription,
  [Types.MODIFY_SUBSCRIPTION_SUCCESS]: modifySubscriptionSuccess,
  [Types.MODIFY_SUBSCRIPTION_FAILED]: modifySubscriptionFailed,
  [Types.SET_SUBSCRIPTION_NEW_CREDIT_CARD]: setSubscriptionNewCreditCard,
  [Types.RESET_MODIFY_SUBSCRIPTION_STATE]: resetModifySubscriptionState,
  [Types.CANCELLING_ORDER]: cancellingOrder,
  [Types.CANCELLING_ORDER_DONE]: cancellingOrderDone,
  [Types.CANCELLING_ORDER_FAILED]: cancellingOrderFailed,
  [Types.RESET_ERROR_MESSAGE]: resetErrorMessage,
})
