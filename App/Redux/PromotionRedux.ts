import { createActions, createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'

export interface PromotionActionsCreators {
  reset: () => AnyAction,
  fetchBanners: () => AnyAction,
  fetchingBanners: () => AnyAction,
  fetchingBannersSuccess: (banners: Banner[]) => AnyAction,
  fetchingBannersFailed: (error: string) => AnyAction,
}

export interface PromotionActionsTypes {
  RESET: 'RESET'
  FETCH_BANNERS: 'FETCH_BANNERS'
  FETCHING_BANNERS: 'FETCHING_BANNERS'
  FETCHING_BANNERS_SUCCESS: 'FETCHING_BANNERS_SUCCESS'
  FETCHING_BANNERS_FAILED: 'FETCHING_BANNERS_FAILED'
}

const { Types, Creators } = createActions<PromotionActionsTypes, PromotionActionsCreators>({
  reset: null,
  fetchBanners: null,
  fetchingBanners: null,
  fetchingBannersSuccess: ['banners'],
  fetchingBannersFailed: ['error'],
})

export const PromotionTypes = Types
export default Creators

export interface Banner {
  id: number,
  title: string,
  slug: string,
  banner_image: string,
  content: string,
  action_path: string,
  items?: Banner[],
  description: string,
  terms_and_condition: string,
  code_text: string,
}

interface BasePromotion {
  banners: Banner[],
  error: string,
  fetching: boolean,
  fetchingSuccess: boolean,
}

export const INITIAL_STATE = <BasePromotion> {
  banners: null,
  error: null,
  fetching: false,
  fetchingSuccess: false,
}

export type Promotion = typeof INITIAL_STATE

export const reset = () => INITIAL_STATE

export const fetchingBanners: any = (state: Promotion) => {
  return { ...state, fetching: true, error: null, fetchingSuccess: false }
}
export const fetchingBannersSuccess: any = (state: Promotion, { banners }: { banners: Banner[] }) => {
  return {...state, fetching: false, error: null, fetchingSuccess: true, banners}
}
export const fetchingBannersFailed: any = (state: Promotion, {error}: { error: AnyAction}) => {
  return {...state, fetching: false, fetchingSuccess: false, error, banners: null}
}

export const reducer: (state: Promotion, action: any) => Promotion = createReducer(INITIAL_STATE, {
  [Types.RESET]: reset,
  [Types.FETCHING_BANNERS]: fetchingBanners,
  [Types.FETCHING_BANNERS_SUCCESS]: fetchingBannersSuccess,
  [Types.FETCHING_BANNERS_FAILED]: fetchingBannersFailed
})
