import {
  select,
  put,
  call,
} from 'redux-saga/effects'
import Actions from '../Redux/AddressHistoryRedux'
import Api, { getAddressHistoryApiResponse } from '../Services/ApiCustomer'
import { AppState } from '../Redux/CreateStore'

export const getToken = (state: AppState) => state.login.token

export function getAddressHistoryApi({ token }: { token: string }) {
  return Api.create()
    .getAddressHistory(token)
    .then(response => response.data)
    .catch(error => error)
}

export function* fetchAddressHistory() {
  yield put(Actions.fetchingAddressHistory())
  const token = yield select(getToken)
  const response: getAddressHistoryApiResponse = yield call(getAddressHistoryApi, { token })

  if (response !== null && !response.error) {
    yield put(Actions.fetchAddressHistorySuccess(response.data))
  } else {
    yield put(Actions.fetchAddressHistoryFailed(response.message))
  }
}
