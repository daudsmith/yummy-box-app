import moment, { Moment } from 'moment'
import { call, fork, put } from 'redux-saga/effects'
import actions from '../Redux/SettingRedux'
import ApiSetting from '../Services/ApiSetting'
import { ApiResponse } from 'apisauce'
import { getBulkSettingResponse } from '../Services/ApiSetting'
import { SettingItem } from '../Redux/SettingRedux'

export function fetchSettingApi (key: string[]) {
  return ApiSetting.create()
    .bulk(key)
    .then((response: ApiResponse<getBulkSettingResponse>) => response.data)
    .catch((error: ApiResponse<getBulkSettingResponse>) => error)
}

export function filterActiveOffDates (offDates: string[]): Moment[] {
  const filteredOffDates: string[] = offDates.filter(offDate => moment().isBefore(moment(offDate)))
  return filteredOffDates.map(offDate => moment(offDate).utcOffset(7))
}

export function * fetching (key: string[]) {
  const response: getBulkSettingResponse = yield call(fetchSettingApi, key)
  if (response !== null && !response.error) {
    const setting: SettingItem[] = response.data
    let settings: any = {}
    setting.map(item => {
      if (item.key === 'off_dates') {
        settings[item.key] = filterActiveOffDates(JSON.parse(item.value))
      } else if (item.key === 'virtual_account_banks') {
        settings[item.key] = JSON.parse(item.value)
      } else {
        settings[item.key] = item.value
      }
    })
    yield put(actions.settingFetched(settings))
    yield put(actions.updateStatus(true))
  }
}

export function * fetchSetting ({ key }: ReturnType<typeof actions.fetchSetting>) {
  yield put(actions.updateStatus(false))
  yield fork(fetching, key)
}
