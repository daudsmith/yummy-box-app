import { call, put, select } from 'redux-saga/effects'
import actions, { Card } from '../Redux/CustomerCardRedux'
import api, { saveCardApiResponse, getSavedCardsApiResponse, defaultCardApiResponse } from '../Services/ApiCustomer'
import { AppState } from '../Redux/CreateStore'
import { ApiResponse } from 'apisauce'

export const getCards = (state: AppState) => state.customerCard
export const getToken = (state: AppState) => state.login

export function saveCardApi({ card, token }: { card: Card, token: string }) {
  return api.create()
    .saveCard(card, token)
    .then((response: ApiResponse<saveCardApiResponse>) => response.data)
    .catch((error: ApiResponse<saveCardApiResponse>) => error)
}

export function fetchCardsApi(token: string) {
  return api.create()
    .getSavedCards(token)
    .then((response: ApiResponse<getSavedCardsApiResponse>) => response.data)
    .catch((error: ApiResponse<getSavedCardsApiResponse>) => error)
}

export function deleteCardApi({ cardId, token }: { cardId: number, token: string }) {
  return api.create()
    .deleteCard(cardId, token)
    .then((response: ApiResponse<defaultCardApiResponse>) => response.data)
    .catch((error: ApiResponse<defaultCardApiResponse>) => error)
}

export function setDefaultCardApi({ cardId, token }: { cardId: number, token: string }) {
  return api.create()
    .setDefaultCard(cardId, token)
    .then((response: ApiResponse<defaultCardApiResponse>) => response.data)
    .catch((error: ApiResponse<defaultCardApiResponse>) => error)
}

export function checkDuplicateCard({ card, savedCards }: { card: Card, savedCards: Card[] }): boolean {
  for (let savedCard of savedCards) {
    if (savedCard.ends_with === card.ends_with && savedCard.type === card.type) {
      return true
    }
  }
  return false
}

export function* fetchCards() {
  const { token }: { token: string } = yield select(getToken)
  yield put(actions.fetchingCards())
  const response: getSavedCardsApiResponse = yield call(fetchCardsApi, token)
  if (response !== null) {
    if (response.error) {
      yield put(actions.errorOccurred(response.message))
    } else {
      yield put(actions.fetchingCardsDone())
      yield put(actions.updateCards(response.data))
    }
  }
}

export function* addCard({ card }: { card: Card }) {
  const { token }: { token: string } = yield select(getToken)
  const { cards }: { cards: Card[] } = yield select(getCards)
  const savedCards: Card[] = cards

  yield put(actions.addingCard())
  const exist: boolean = yield call(checkDuplicateCard, { card, savedCards })

  if (!exist) {
    const saving: saveCardApiResponse = yield call(saveCardApi, { card, token })
    if (saving.error) {
      yield put(actions.errorOccurred(saving.message))
    } else {
      //Temporary. Set default if this is the first saved credit card
      if (cards.length === 0) {
        const cardId = saving.data.id
        yield call(setDefaultCardApi, { cardId, token })
      }
      const response: getSavedCardsApiResponse = yield call(fetchCardsApi, token)
      yield put(actions.updateCards(response.data))
      yield put(actions.addCardSuccess())
    }
  }
}

export function* removeCard({ cardId }: { cardId: number }) {
  const { token }: { token: string } = yield select(getToken)

  yield put(actions.removingCard())
  const deleting: defaultCardApiResponse = yield call(deleteCardApi, { cardId, token })
  if (deleting.error) {
    yield put(actions.errorOccurred(deleting.message))
  } else {
    const response: getSavedCardsApiResponse = yield call(fetchCardsApi, token)
    yield put(actions.updateCards(response.data))
    yield put(actions.removeCardSuccess())
  }
}
export function* setDefaultCard({ cardId }: { cardId: number }) {
  const { token }: { token: string } = yield select(getToken)

  yield put(actions.setDefaultCardOnProcess())
  const setDefault: defaultCardApiResponse = yield call(setDefaultCardApi, { cardId, token })
  if (setDefault.error) {
    yield put(actions.errorOccurred(setDefault.message))
  } else {
    const response: getSavedCardsApiResponse = yield call(fetchCardsApi, token)
    yield put(actions.updateCards(response.data))
    yield put(actions.setDefaultCardSuccess())
  }

}
