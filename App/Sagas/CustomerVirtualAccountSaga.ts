import { call, put, select } from 'redux-saga/effects'
import actions from '../Redux/CustomerVirtualAccountRedux'
import api, { fetchVirtualAccountApiResponse } from '../Services/ApiCustomer'
import { AppState } from '../Redux/CreateStore'
import { ApiResponse } from 'apisauce'

export const getToken = (state: AppState) => state.login.token

export function fetchingVirtualAccountApi(token: string) {
  return api.create()
    .getVirtualAccounts(token)
    .then((response: ApiResponse<fetchVirtualAccountApiResponse>) => response.data)
    .catch((error: ApiResponse<fetchVirtualAccountApiResponse>) => error)
}
export function* fetchVirtualAccount() {
  yield put(actions.fetchingVirtualAccount())
  const token: string = yield select(getToken)
  const response: fetchVirtualAccountApiResponse = yield call(fetchingVirtualAccountApi, token)
  if (!response.error) {
    yield put(actions.virtualAccountFetchingSuccess(response.data))
  } else {
    yield put(actions.virtualAccountFetchingFailed(response.message))
  }
}
