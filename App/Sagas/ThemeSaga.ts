import _ from 'lodash'
import { put, call, select } from 'redux-saga/effects'
import { ApiResponse } from 'apisauce'
import Api from '../Services/ApiTheme'
import moment from 'moment'
import Actions from '../Redux/ThemeRedux'
import { AppState } from '../Redux/CreateStore'
import { Item } from '../Redux/ThemeRedux'
import {
  GetThemeDetailResponse,
  GetThemeDetailMeals,
  GetThemePackages,
  GetThemeAvailableDate,
  GetMealByDates,
} from '../Services/ApiTheme'

export const getAvailableDates = (state: AppState): string[] => state.theme.initialAvailableDateSelectedTheme
export const getExtendedAvailableDates = (state: AppState): string[] => state.theme.availableDateSelectedTheme
export const getCurrentMeals = (state: AppState): Item[] => state.theme.meals

export function generateDistinctThemeMeals(meals: Item[]): Item[] {
  if (meals.length === 0) {
    return []
  }

  let lastMeal: Item | null = null
  let distinctMeal: Item[] = []

  meals.map((meal: Item) => {
    if (lastMeal !== null) {
      if (lastMeal.date !== meal.date) {
        distinctMeal.push(meal)
        lastMeal = meal
      }
    } else {
      distinctMeal.push(meal)
      lastMeal = meal
    }
    return meal
  })

  return distinctMeal
}

export function getThemeDetailApi(id: number, kitchenCode: string) {
  return Api.create()
    .getThemeDetail(id, kitchenCode)
    .then((response: ApiResponse<GetThemeDetailResponse>) => response.data)
    .catch((error: ApiResponse<GetThemeDetailResponse>) => error)
}

export function getThemeDetailMealsApi(id: number) {
  return Api.create()
    .getThemeDetailMeals(id)
    .then((response: ApiResponse<GetThemeDetailMeals>) => response.data)
    .catch((error: ApiResponse<GetThemeDetailMeals>) => error)
}

export function getThemePackagesApi(id: number) {
  return Api.create()
    .getThemePackages(id)
    .then((response: ApiResponse<GetThemePackages>) => response.data)
    .catch((error: ApiResponse<GetThemePackages>) => error)
}

export function getAvailableDateApi({themeId}: {themeId: number}) {
  return Api.create()
    .getAvailableDate(themeId)
    .then((response: ApiResponse<GetThemeAvailableDate>) => response.data)
    .catch((error: ApiResponse<GetThemeAvailableDate>) => error)
}

export function getAvailableDateExtendsApi(themeId: number, startDate: string) {
  return Api.create()
    .getAvailableDateExtends(themeId, startDate)
    .then((response: ApiResponse<GetThemeAvailableDate>) => response.data)
    .catch((error: ApiResponse<GetThemeAvailableDate>) => error)
}

export function getMealByDatesApi({ themeId, availableDates }: { themeId: number, availableDates: string[] }) {
  return Api.create()
    .getMealByDates(themeId, availableDates)
    .then((response: ApiResponse<GetMealByDates>) => response.data)
    .catch((error: ApiResponse<GetMealByDates>) => error)
}

export function* fetchDetail({ id, kitchenCode}: ReturnType<typeof Actions.fetchDetail>) {
  yield put(Actions.fetchingDetail())
  const response: GetThemeDetailResponse = yield call(getThemeDetailApi, id, kitchenCode)
  if (response !== null && !response.error) {
    yield put(Actions.fetchDetailSuccess(response.data))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch theme detail due to network issues'
    yield put(Actions.fetchDetailFailed(error))
  }
}

export function* fetchDetailItems({ id }: ReturnType<typeof Actions.fetchDetailItems>) {
  yield put(Actions.fetchingDetailItems())
  const response: GetThemeDetailMeals = yield call(getThemeDetailMealsApi, id)

  if (response !== null && !response.error) {
    const meals: Item[] = generateDistinctThemeMeals(response.data)
    yield put(Actions.fetchDetailItemsSuccess(meals))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch detail items due to network issues'
    yield put(Actions.fetchDetailItemsFailed(error))
  }
}

export function* fetchThemePackages({ id }: ReturnType<typeof Actions.fetchThemePackages>) {
  yield put(Actions.fetchingPackages())
  const response: GetThemePackages = yield call(getThemePackagesApi, id)

  if (response !== null && !response.error) {
    yield put(Actions.fetchPackagesSuccess(response.data))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch packages due to network issues'
    yield put(Actions.fetchPackagesFailed(error))
  }
}

export function* fetchAvailableMeals({ themeId }: ReturnType<typeof Actions.fetchAvailableMeals>) {
  yield put(Actions.fetchingAvailableMeals())
  const availableDates: string[] = yield select(getAvailableDates)
  if (availableDates === null || availableDates.length === 0) {
    return
  } else {
    const response: GetMealByDates = yield call(getMealByDatesApi, { themeId, availableDates })
    if (response !== null && !response.error) {
      yield put(Actions.fetchAvailableMealsSuccess(response.data))
    } else {
      const error = response !== null ? response.message : 'Cannot fetch available meals due to network issues'
      yield put(Actions.fetchAvailableMealsFailed(error))
    }
  }
}

export function* fetchAvailableDate({ themeId }: ReturnType<typeof Actions.fetchAvailableDate>) {
  yield put(Actions.fetchingAvailableDate())
  const response: GetThemeAvailableDate = yield call(getAvailableDateApi, { themeId })
  if (response !== null && !response.error) {
    const extendedStartDate: string = response.data[response.data.length - 1]
    const extendedDateResponse: GetThemeAvailableDate = yield call(getAvailableDateExtendsApi, themeId, extendedStartDate)
    let extendedAvailableDateSelectedTheme: string[] = _.uniqBy(response.data.concat(extendedDateResponse.data))
    yield put(Actions.fetchingAvailableDateSuccess(extendedAvailableDateSelectedTheme, response.data))
    yield call(fetchAvailableMeals, {themeId} as any)
  } else {
    const error = response !== null ? response.message : 'Cannot fetch available dates due to network issues'
    yield put(Actions.fetchingAvailableDateFailed(error))
  }
}

export function* fetchAvailableDateExtends({ themeId, startDate }: ReturnType<typeof Actions.fetchAvailableDateExtends>) {
  yield put(Actions.fetchingAvailableDateExtends())
  let availableDateSelectedTheme: string[] = yield select(getAvailableDates)

  const response: GetThemeAvailableDate = yield call(getAvailableDateExtendsApi, themeId, startDate)
  if (!response.error) {
    let newAvailableDateSelectedTheme: string[] = _.uniqBy(availableDateSelectedTheme.concat(response.data))
    yield put(Actions.fetchingAvailableDateSuccess(newAvailableDateSelectedTheme, availableDateSelectedTheme))
  } else {
    const error = response.message ? response.message : 'Cannot fetch available dates due to network issues'
    yield put(Actions.fetchingAvailableDateExtendsFailed(error))
  }
}

export function* removeFirstDayOfAvailableDates() {
  const firstDay = moment()
    .utcOffset(7)
    .add(1, 'days')
    .format('YYYY-MM-DD')

  const initialAvailableDateSelectedTheme: string[] = yield select(getAvailableDates)
  const availableDateSelectedTheme: string[] = yield select(getExtendedAvailableDates)
  const currentMeals: Item[] = yield select(getCurrentMeals)

  const newExtendedDates: string[] = availableDateSelectedTheme && availableDateSelectedTheme.filter((date: string) => date !== firstDay)
  const newInitialDates: string[] = availableDateSelectedTheme && initialAvailableDateSelectedTheme.filter((date: string) => date !== firstDay)
  const newMeals: Item[] = currentMeals.filter((meal: Item) => meal.date !== firstDay)

  yield put(Actions.fetchingAvailableDateSuccess(newExtendedDates, newInitialDates))
  yield put(Actions.fetchDetailItemsSuccess(newMeals))

}
