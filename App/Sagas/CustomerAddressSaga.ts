import { call, put, select } from 'redux-saga/effects'
import actions from '../Redux/CustomerAddressRedux'
import ApiCustomer, { saveAddressApiResponse, deleteAddressApiResponse, FormAddress, getSavedAddressesApiResponse } from '../Services/ApiCustomer'
import { AppState } from '../Redux/CreateStore'
import { ApiResponse } from 'apisauce'
import Creators, { CartState } from '../Redux/CartRedux'

export const getToken = (state: AppState) => state.login
export const getCart = (state: AppState): CartState => state.cart

//-------Customer Address API-------------//

export function fetchAddressApi(token: string) {
  return ApiCustomer
    .create()
    .getSavedAddresses(token)
}

export function saveAddressApi({ token, address }: { token: string, address: FormAddress }) {
  return ApiCustomer
    .create()
    .saveAddress(token, address)
    .then((response: ApiResponse<saveAddressApiResponse>) => response.data)
    .catch((error: ApiResponse<saveAddressApiResponse>) => error)
}

export function deleteAddressApi({ addressId, token }: { addressId: number, token: string }) {
  return ApiCustomer
    .create()
    .deleteAddress(addressId, token)
    .then((response: ApiResponse<deleteAddressApiResponse>) => response.data)
    .catch((error: ApiResponse<deleteAddressApiResponse>) => error)
}

export function editAddressApi({ addressId, address, token }: { addressId: number, address: FormAddress, token: string }) {
  return ApiCustomer
    .create()
    .editAddress(addressId, address, token)
    .then((response: ApiResponse<saveAddressApiResponse>) => response.data)
    .catch((error: ApiResponse<saveAddressApiResponse>) => error)
}

//--------------Generator functions-----------//

export function* fetchAddress() {
  const { token }: { token: string } = yield select(getToken)
  yield put(actions.fetchingAddress())
  const response: getSavedAddressesApiResponse = yield call(fetchAddressApi, token)
  if (response) {
    const {
      error,
      message,
      data,
    } = response
    if (!error && data && data.length) {
      yield put(actions.fetchingAddressDone(data))
    } else {
      yield put(actions.fetchingAddressFailed(message))
    }
  }
}

export function* saveAddress({ address }) {
  const { token }: { token: string } = yield select(getToken)
  yield put(actions.savingAddress())
  const saving: saveAddressApiResponse = yield call(saveAddressApi, { token, address })
  const {
    error,
    message,
    data,
  } = saving
  if (error) {
    yield put(actions.savingAddressFailed(message))
  } else {
    yield put(actions.savingAddressDone(data))
  }
}

export function* deleteAddress({ addressId }) {
  const { token }: { token: string } = yield select(getToken)
  const { useMultipleAddress } = yield select(getCart)

  yield put(actions.addressDeleting())
  const deleting: deleteAddressApiResponse = yield call(deleteAddressApi, { addressId, token, })
  const {
    error,
    message,
    data,
  } = deleting
  if (error) {
    yield put(actions.deleteAddressFailed(message))
  } else {
    yield put(actions.deleteAddressDone(data))
    yield put(Creators.updateDeliveryAddress(null,null,useMultipleAddress))
  }
}

export function* editAddress({ addressId, address }) {
  const { token }: { token: string } = yield select(getToken)
  yield put(actions.editingAddress())
  const edit: saveAddressApiResponse = yield call(editAddressApi, { addressId, address, token })
  const {
    error,
    message,
    data,
  } = edit

  if (error) {
    yield put(actions.editAddressFailed(message))
  } else {
    yield put(actions.editAddressDone(data))
  }
}
