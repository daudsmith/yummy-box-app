import { call, put, select } from 'redux-saga/effects'
import actions from '../Redux/AddressTypeRedux'
import ApiCater, { AddressListResponse } from '../Services/ApiCater'

export const getToken = state => state.login

//-------Customer Address API-------------//

export function fetchCompanyAddressesApi(token: string) {
  return ApiCater
    .create()
    .getCompanyAddresses(token)
    .then((response) => {
      return response.data
    })
    .catch(error => error)
}

//--------------Generator functions-----------//

export function * fetchCompanyAddresses () {
  const { token } = yield select(getToken)
  yield put(actions.companyAddressesFetching())

  const response: AddressListResponse = yield call(fetchCompanyAddressesApi, token)
  if (response !== null) {
    if (response.error) {
      yield put(actions.errorOccurred(response.error))
    } else {
      yield put(actions.updateCompanyAddresses(response.data))
    }
  }
}
