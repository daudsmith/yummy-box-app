import {
  select,
  put,
  call,
} from 'redux-saga/effects'
import api, {
  mmpResponse,
  subscriptionModifyResponse,
  mmpDetailResponse,
  subscriptionModifyData,
} from '../Services/ApiMyMealPlan'
import actions, { MyMealPlanDetail, MyMealPlanDetailSuccess, PaymentInfo } from '../Redux/MyMealPlanRedux'
import { Item } from '../Redux/CartRedux'
import { Item as ThemeItem } from '../Redux/ThemeRedux'
import { AppState } from '../Redux/CreateStore'
import { ApiResponse } from 'apisauce'
import { _dateReformatter } from '../util/_dateReformatter'
export const getToken = (state: AppState): string => state.login.token
export const getModifiedMMP = (state: AppState): MyMealPlanDetailSuccess => state.myMealPlan.modifiedMyMealPlanDetail
export const getThemeMeals = (state: AppState): ThemeItem[] => state.theme.meals
export const getMMPType = (state: AppState): string => state.myMealPlan.myMealPlanDetail.type

export function buildNewPackageSelectedDate(selectedDate: string, currentDate: string, selectedDates: string[]): string[] {
  let newSelectedDates: string[] = selectedDates.filter(date => date !== currentDate)
  newSelectedDates.push(selectedDate)
  newSelectedDates.sort()
  return newSelectedDates
}

interface nonSubsMMPBody {
  address: string
  latitude: number
  longitude: number
  landmark: string
  phone: string
  name: string
  date: string
  time: string
  note: string
  items: Item[]
}

interface subsMMPBody {
  delivery_time: string
  delivery_address: string
  delivery_note: string
  delivery_longitude: number
  delivery_latitude: number
  landmark: string
  delivery_details: string
  recipient_phone: string
  recipient_name: string
  payment_method: string
  payment_info: object
}

export function buildModifyMMPBody(data: MyMealPlanDetail | subscriptionModifyData, paymentInfo: PaymentInfo, selectedPayment: string): nonSubsMMPBody | subsMMPBody {
  const dateNonSub = _dateReformatter({
    inputDate: (data as MyMealPlanDetail).date,
    returnFormat: 'YYYY-MM-DD'
  })
  if (data.type !== 'subscription') {
    return <nonSubsMMPBody>{
      address: (data as MyMealPlanDetail).address,
      latitude: (data as MyMealPlanDetail).latitude,
      longitude: (data as MyMealPlanDetail).longitude,
      landmark: (data as MyMealPlanDetail).landmark,
      phone: (data as MyMealPlanDetail).phone,
      kitchen_code: (data as MyMealPlanDetail).kitchen_code,
      name: (data as MyMealPlanDetail).name,
      date: dateNonSub,
      time: (data as MyMealPlanDetail).time,
      note: (data as MyMealPlanDetail).note,
      items: (data as MyMealPlanDetail).items.data
    }
  }

  return <subsMMPBody>{
    delivery_time: (data as subscriptionModifyData).time,
    delivery_address: (data as subscriptionModifyData).address,
    delivery_note: (data as subscriptionModifyData).note,
    delivery_longitude: (data as subscriptionModifyData).longitude,
    delivery_latitude: (data as subscriptionModifyData).latitude,
    landmark: (data as subscriptionModifyData).landmark,
    delivery_details: '',
    recipient_phone: (data as subscriptionModifyData).phone,
    recipient_name: (data as subscriptionModifyData).name,
    payment_method: selectedPayment,
    payment_info: paymentInfo.hasOwnProperty('payment_info_json')
      ? paymentInfo.payment_info_json
      : JSON.stringify(paymentInfo),
  }
}

export function getMyMealPlansApi(token: string) {
  return api.create()
    .getMyMealPlans(token)
    .then((response: ApiResponse<mmpResponse>) => response.data)
    .catch((error: ApiResponse<mmpResponse>) => error)
}

export function getMyMealPlanDetailApi({ token, id, orderType }: { token: string, id: number, orderType: string }) {
  return api.create()
    .getMyMealPlanDetail(token, id, orderType)
    .then((response: ApiResponse<mmpDetailResponse>) => response.data)
    .catch((error: ApiResponse<mmpDetailResponse>) => error)
}

export function cancelOrderApi({ token, id }: { token: string, id: number }) {
  return api.create()
    .cancelOrder(token, id)
    .then((response: ApiResponse<mmpDetailResponse>) => response.data)
    .catch((error: ApiResponse<mmpDetailResponse>) => error)
}

export function updateDeliveryApi({ token, deliveryId, data }: { token: string, deliveryId: number, data: object }) {
  return api.create()
    .updateDelivery(token, deliveryId, data)
    .then((response: ApiResponse<mmpDetailResponse>) => response.data)
    .catch((error: ApiResponse<mmpDetailResponse>) => error)
}

export function skipDelivery({ token, id }: { token: string, id: number }) {
  return api.create()
    .skipDelivery(token, id)
    .then((response: ApiResponse<subscriptionModifyResponse>) => response.data)
    .catch((error: ApiResponse<subscriptionModifyResponse>) => error)
}

export function updateSubscriptionDeliveryApi({ token, deliveryId, data }: { token: string, deliveryId: number, data: object }) {
  return api.create()
    .updateSubscriptionDelivery(token, deliveryId, data)
    .then((response: ApiResponse<subscriptionModifyResponse>) => response.data)
    .catch((error: ApiResponse<subscriptionModifyResponse>) => error)
}

export function* getMyMealPlans() {
  yield put(actions.gettingMyMealPlans())
  const token: string = yield select(getToken)

  const response: mmpResponse = yield call(getMyMealPlansApi, token)
  if (response !== null && !response.error) {
    yield put(actions.getMyMealPlansSuccess(response.data))
  } else {
    const error = response !== null
      ? response.message
      : 'Cannot get my meal plans due to network issues'
    yield put(actions.getMyMealPlansFailed(error))
  }
}

export function* getMyMealPlanDetail({ id, orderType }: { id: number, orderType: string}) {
  try {
    yield put(actions.gettingMyMealPlanDetail())
    const token: string = yield select(getToken)
    const response: mmpDetailResponse = yield call(getMyMealPlanDetailApi, {
      token,
      id,
      orderType,
    })
    if (response != null && !response.error) {
      const myMealPlanDetail: MyMealPlanDetailSuccess = {
        data: response.data,
        payment_info: response.hasOwnProperty('payment_info')
          ? response.payment_info
          : null,
        selectedPayment: response.hasOwnProperty('payment_info')
          ? response.payment_info.method
          : null,
        type: orderType,
        actions: response.hasOwnProperty('action')
          ? response.action
          : null,
        myMealPlanPackage: response.hasOwnProperty('package')
          ? response.package
          : null,
      }
      yield put(actions.getMyMealPlanDetailSuccess(myMealPlanDetail))
    } else {
      const error = response !== null && response.message
        ? response.message
        : 'Cannot get my meal plan detail due to network issues'
      yield put(actions.getMyMealPlanDetailFailed(error))
    }
  } catch (e) {
    throw new Error(e.toString())
  }
}

export function* cancelOrder({ id }: { id: number }) {
  yield put(actions.cancelingOrder())
  const token: string = yield select(getToken)
  const type: string = yield select(getMMPType)
  const response: mmpDetailResponse = type === 'item' || type === 'package'
    ? yield call(cancelOrderApi, {
      token,
      id,
    })
    : yield call(skipDelivery, {
      token,
      id,
    })
  if (response !== null && !response.error) {
    yield put(actions.cancelOrderSuccess())
    yield call(getMyMealPlanDetail, {
      id,
      orderType: type,
    })
    yield call(getMyMealPlans)
  } else {
    const error = response && response.message
      ? response.message
      : 'Failed to cancel order'
    yield put(actions.cancelOrderFailed(error))
  }
}

export function* modifyMmpPackageDelivery({ field, value }: ReturnType<typeof actions.modifyMmpPackageDelivery>) {
  const modifiedMMP: MyMealPlanDetailSuccess = yield select(getModifiedMMP)
  let newData = null
  if (field) {
    newData = {
      ...modifiedMMP.data,
      [field]: value,
    }
  } else if (field === 'address') {
    newData = {
      ...modifiedMMP.data,
      [field]: value,
      note: '',
    }
  } else {
    newData = { ...modifiedMMP.data, ...value }
  }
  const newModifiedMMP: MyMealPlanDetailSuccess = {
    ...modifiedMMP,
    data: newData,
  }
  yield put(actions.updateModifiedMmp(newModifiedMMP))
}

export function* modifyMmpPackageDeliveryMeal({ selectedDate, currentDate }: ReturnType<typeof actions.modifyMmpPackageDeliveryMeal>) {
  const modifiedMMP: MyMealPlanDetailSuccess = yield select(getModifiedMMP)
  const meals: ThemeItem[] = yield select(getThemeMeals)
  let meal: Item = null
  for (let i = 0; i < meals.length; i++) {
    if (meals[i].date === selectedDate) {
      meal = {
        id: meals[i].id,
        name: meals[i].name,
        images: meals[i].images,
        short_description: meals[i].short_description,
        description: meals[i].description,
      }
    }
  }
  const newMealDetailData = { data: meal }
  let newMealItems: Item[] = []
  newMealItems.push({
    ...(modifiedMMP.data as MyMealPlanDetail).items.data[0],
    detail: newMealDetailData,
  })
  const newSelectedDates: string[] = buildNewPackageSelectedDate(selectedDate, currentDate, modifiedMMP.myMealPlanPackage.dates)

  const newData = {
    ...modifiedMMP.data,
    items: { data: newMealItems },
    date: selectedDate,
  }
  const newMyMealPlanPackage = {
    ...modifiedMMP.myMealPlanPackage,
    dates: newSelectedDates,
  }
  const newModifiedMMP: MyMealPlanDetailSuccess = {
    ...modifiedMMP,
    data: newData,
    myMealPlanPackage: newMyMealPlanPackage,
  }
  yield put(actions.updateModifiedMmp(newModifiedMMP))
}

export function* modifyMmpSubscriptionSelectedPayment({ selectedPayment }: ReturnType<typeof actions.modifyMmpSubscriptionSelectedPayment>) {
  const modifiedMMP: MyMealPlanDetailSuccess = yield select(getModifiedMMP)
  const newModifiedMMP: MyMealPlanDetailSuccess = {
    ...modifiedMMP,
    selectedPayment,
  }
  yield put(actions.updateModifiedMmp(newModifiedMMP))
}

export function* modifyMmpSubscriptionPaymentInfo({ paymentInfo }: ReturnType<typeof actions.modifyMmpSubscriptionPaymentInfo>) {
  const modifiedMMP: MyMealPlanDetailSuccess = yield select(getModifiedMMP)
  const newModifiedMMP: MyMealPlanDetailSuccess = {
    ...modifiedMMP,
    payment_info: paymentInfo,
  }
  yield put(actions.updateModifiedMmp(newModifiedMMP))
}

export function* modifyMmp() {
  yield put(actions.modifyingMmp())
  const modifiedMMP: MyMealPlanDetailSuccess = yield select(getModifiedMMP)
  const token: string = yield select(getToken)

  const deliveryId: number = modifiedMMP.data.id
  const data: nonSubsMMPBody | subsMMPBody = buildModifyMMPBody(modifiedMMP.data, modifiedMMP.payment_info, modifiedMMP.selectedPayment)
  
  const response: mmpDetailResponse = modifiedMMP.data.type === 'subscription'
    ? yield call(updateSubscriptionDeliveryApi, {
      token,
      deliveryId,
      data,
    })
    : yield call(updateDeliveryApi, {
      token,
      deliveryId,
      data,
    })

  if (response !== null && !response.error) {
    yield call(getMyMealPlans)

    const myMealPlanDetail: MyMealPlanDetailSuccess = {
      data: response.data,
      payment_info: response.hasOwnProperty('payment_info')
        ? response.payment_info
        : null,
      selectedPayment: response.hasOwnProperty('payment_info')
        ? response.payment_info.method
        : null,
      type: modifiedMMP.data.type === 'subscription'
        ? response.data.type
        : response.order.type,
      myMealPlanPackage: response.hasOwnProperty('package')
        ? response.package
        : null,
    }
    yield put(actions.getMyMealPlanDetailSuccess(myMealPlanDetail))
    yield put(actions.modifyMmpSuccess())
  } else {
    const error = response && response.message
      ? response.message
      : 'Cannot modify my meal plan due to network issues'
    yield put(actions.modifyMmpFailed(error))
  }
}

export function* updateMyMealPlanWithPayment() {
  yield put(actions.modifyingMmp())
  const modifiedMMP: MyMealPlanDetailSuccess = yield select(getModifiedMMP)
  const token: string = yield select(getToken)

  const deliveryId: number = modifiedMMP.data.id
  const data: nonSubsMMPBody | subsMMPBody = buildModifyMMPBody(modifiedMMP.data, modifiedMMP.payment_info, modifiedMMP.selectedPayment);
  (data as subsMMPBody).payment_info = modifiedMMP.payment_info

  const response: subscriptionModifyResponse | mmpDetailResponse = modifiedMMP.data.type === 'subscription'
    ? yield call(updateSubscriptionDeliveryApi, {
      token,
      deliveryId,
      data,
    })
    : yield call(updateDeliveryApi, {
      token,
      deliveryId,
      data,
    })

  if (response !== null && !response.error) {
    yield call(getMyMealPlans)

    const myMealPlanDetail: MyMealPlanDetailSuccess = {
      data: response.data,
      payment_info: response.hasOwnProperty('payment_info')
        ? (response as mmpDetailResponse).payment_info
        : null,
      selectedPayment: response.hasOwnProperty('payment_info')
        ? (response as mmpDetailResponse).payment_info.method
        : null,
      type: response.data.type,
      myMealPlanPackage: response.hasOwnProperty('package')
        ? (response as mmpDetailResponse).package
        : null,
    }
    yield put(actions.getMyMealPlanDetailSuccess(myMealPlanDetail))
    yield put(actions.modifyMmpSuccess())
  } else {
    const error = response
      ? (response as mmpDetailResponse).message
      : 'Cannot modify my meal plan due to network issues'
    yield put(actions.modifyMmpFailed(error))
  }
}
