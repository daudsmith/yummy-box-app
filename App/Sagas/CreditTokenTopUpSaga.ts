import {
  call,
  put,
  select,
} from 'redux-saga/effects'
import ApiCustomer, { redeemCreditTokenApiResponse } from '../Services/ApiCustomer'
import CreditTokenTopUpActions from '../Redux/CreditTokenTopUpRedux'
import { fetchWallet } from './CustomerAccountSaga'
import { AppState } from '../Redux/CreateStore'

export const getToken = (state: AppState) => state.login.token

export function submitTokenApi({ token, code }: { token: string, code: string }) {
  return ApiCustomer.create()
    .redeemCreditToken(token, code)
    .then(response => response.data)
    .catch(error => error)
}

export function* submitToken({ code }: ReturnType<typeof CreditTokenTopUpActions.submitToken>) {
  const token: string = yield select(getToken)
  yield put(CreditTokenTopUpActions.submittingToken())
  const response: redeemCreditTokenApiResponse = yield call(submitTokenApi, {
    token,
    code,
  })
  if (response !== null && !response.error) {
    yield call(fetchWallet)
    yield put(CreditTokenTopUpActions.submittingTokenDone(response.data))
  } else {
    const error = response !== null
      ? response.message
      : 'Submit token failed'
    yield put(CreditTokenTopUpActions.submittingTokenFailed(error))
  }
}
