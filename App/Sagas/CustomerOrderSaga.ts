import { call, put, select } from 'redux-saga/effects'
import actions, { SalesOrderDetail, SalesOrderSubscriptionDetail, SubscriptionRawData, ListOrderDetail } from '../Redux/CustomerOrderRedux'
import ApiCustomerOrder, { ModifySubscriptionBody, salesOrderResponse, OrderDetailResponse, SubscriptPauseResponse, subscriptionOrderResponse } from '../Services/ApiCustomerOrder'
import ApiSalesOrder, { cancelApiResponse } from '../Services/ApiSalesOrder'
import JSONService from '../Services/JSONService'
import { AppState } from '../Redux/CreateStore'
import { ApiResponse } from 'apisauce'

export const getToken = (state: AppState) => state.login.token
export const getModifiedOrderDetail = (state: AppState) => state.customerOrder.modifiedOrderDetail

export function* updateSubscriptionDefaultData({ key, value }: ReturnType<typeof actions.updateSubscriptionDefaultData>) {
  const modifiedOrderDetail: SalesOrderDetail & SalesOrderSubscriptionDetail = yield select(getModifiedOrderDetail)
  if (typeof key === 'string') {
    let newModifiedOrderDetail = modifiedOrderDetail
    if (key === 'default_delivery_address') {
      newModifiedOrderDetail = {
        ...newModifiedOrderDetail,
        subscription_raw_data: {
          ...newModifiedOrderDetail.subscription_raw_data,
          [key]: value,
          default_delivery_note: '',
        },
      }
    } else {
      newModifiedOrderDetail = {
        ...newModifiedOrderDetail,
        subscription_raw_data: {
          ...newModifiedOrderDetail.subscription_raw_data,
          [key]: value,
        },
      }
    }
    yield put(actions.updateModifySubscriptionData(newModifiedOrderDetail))
  } else {
    let newModifiedOrderDetail = modifiedOrderDetail
    key.map((item, index) => {
      const thisValue = value[index]
      if (item === 'default_delivery_address') {
        newModifiedOrderDetail = {
          ...newModifiedOrderDetail,
          subscription_raw_data: {
            ...newModifiedOrderDetail.subscription_raw_data,
            [item]: thisValue,
            default_delivery_note: '',
          },
        }
      } else {
        newModifiedOrderDetail = {
          ...newModifiedOrderDetail,
          subscription_raw_data: {
            ...newModifiedOrderDetail.subscription_raw_data,
            [item]: thisValue,
          },
        }
      }
    })
    yield put(actions.updateModifySubscriptionData(newModifiedOrderDetail))
  }
}

export function* modifySubscription() {
  yield put(actions.modifyingSubscription())
  const { subscription_raw_data, subscription } = yield select(
    getModifiedOrderDetail)
  const token: string = yield select(getToken)
  const data: ModifySubscriptionBody = buildModifySubscriptionBody(subscription_raw_data)
  if (data.default_payment_info === 'error') {
    yield put(actions.modifySubscriptionFailed(
      'There is something wrong with your payment information, please contact our customer service for more information'))
  } else {
    const response = yield call(modifySubscriptionOrderApi, {
      token,
      id: subscription.id,
      data,
    })
    if (response !== null && !response.error) {
      yield call(fetchOrderDetail, {
        id: subscription.id,
        orderType: 'subscription',
      })
      yield call(fetchOrderList, { silent: true })
      yield put(actions.modifySubscriptionSuccess())
    } else {
      const error = response !== null
        ? response.error
        : 'Cannot modify subscription due to network issues'
      yield put(actions.modifySubscriptionFailed(error))
    }
  }
}

export function* fetchOrderList({ silent }: { silent: boolean }) {
  const token: string = yield select(getToken)
  if (!silent) {
    yield put(actions.fetchingOrderList())
  }
  const response: salesOrderResponse = yield call(fetchOrderListApi, token)
  const subscriptionResponse: subscriptionOrderResponse = yield call(fetchSubscriptionOrderList, token)
  if (response !== null && !response.error && subscriptionResponse !== null && !subscriptionResponse.error) {
    const order = {
      in_progress: response.data.in_progress.filter(
        (item: ListOrderDetail) => item.type !== 'subscription'),
      complete: response.data.complete.filter(
        (item: ListOrderDetail) => item.type !== 'subscription'),
    }
    const data = {
      order,
      subscription: subscriptionResponse.data,
    }
    yield put(actions.fetchingOrderListDone(data))
  } else {
    let error = ''
    const orderError = response === null
      ? 'Cannot fetch order list'
      : response.error
        ? response.message
        : ''
    const subscriptionError = subscriptionResponse === null
      ? 'Cannot fetch subscription order list'
      : subscriptionResponse.error
        ? subscriptionResponse.message
        : ''
    error = orderError === ''
      ? subscriptionError
      : `${orderError} & ${subscriptionError}`
    yield put(actions.fetchingOrderListFailed(error))
  }
}

export function* fetchOrderDetail({ id, orderType }: { id: number, orderType: string }) {
  const token: string = yield select(getToken)
  yield put(actions.fetchingOrderDetail())
  const response: OrderDetailResponse = orderType === 'subscription'
    ? yield call(fetchSubscriptionOrderDetailApi, {
      token,
      id,
    })
    : yield call(fetchOrderDetailApi, {
      token,
      id,
      orderType,
    })
  if (response !== null && !response.error) {
    yield put(actions.fetchingOrderDetailDone(response.data))
  } else {
    yield put(actions.fetchingOrderDetailFailed(response.message))
  }
}

export function* stopSubscription({ id }: ReturnType<typeof actions.stopSubscription>) {
  const token: string = yield select(getToken)
  yield put(actions.stoppingSubscription())
  const response: SubscriptPauseResponse = yield call(stopSubscriptionApi, {
    token,
    id,
  })
  if (response !== null && !response.error) {
    yield call(fetchOrderList, { silent: true })
    yield put(actions.stopSubscriptionSuccess())
  } else {
    const error = response !== null
      ? response.message
      : 'Cannot stop subscription due to network issues'
    yield put(actions.stopSubscriptionFailed(error))
  }
}

export function fetchOrderListApi(token: string) {
  return ApiCustomerOrder.create()
    .getOrderList(token)
    .then((response: ApiResponse<salesOrderResponse>) => response.data)
    .catch((error: ApiResponse<salesOrderResponse>) => error)
}

export function fetchSubscriptionOrderList(token: string) {
  return ApiCustomerOrder.create()
    .getSubscriptionList(token)
    .then((response: ApiResponse<subscriptionOrderResponse>) => response.data)
    .catch((error: ApiResponse<subscriptionOrderResponse>) => error)
}

export function fetchOrderDetailApi({ token, id, orderType }: { token: string, id: number, orderType: string }) {
  return ApiCustomerOrder.create()
    .getOrderDetail(token, id, orderType)
    .then((response: ApiResponse<OrderDetailResponse>) => response.data)
    .catch((error: ApiResponse<OrderDetailResponse>) => error)
}

export function fetchSubscriptionOrderDetailApi({ token, id }: { token: string, id: number }) {
  return ApiCustomerOrder.create()
    .getSubscriptionOrderDetail(token, id)
    .then((response: ApiResponse<OrderDetailResponse>) => response.data)
    .catch((error: ApiResponse<OrderDetailResponse>) => error)
}

export function stopSubscriptionApi({ token, id }: { token: string, id: number }) {
  return ApiCustomerOrder.create()
    .stopSubscription(token, id)
    .then((response: ApiResponse<SubscriptPauseResponse>) => response.data)
    .catch((error: ApiResponse<SubscriptPauseResponse>) => error)
}

export function modifySubscriptionOrderApi({ token, data, id }: { token: string, data: ModifySubscriptionBody, id: number }) {
  return ApiCustomerOrder.create()
    .modifySubscriptionOrder(token, data, id)
    .then((response: ApiResponse<OrderDetailResponse>) => response.data)
    .catch((error: ApiResponse<OrderDetailResponse>) => error)
}

export function buildModifySubscriptionBody(subscription_raw_data: SubscriptionRawData): ModifySubscriptionBody {
  return {
    default_delivery_time: subscription_raw_data.default_delivery_time,
    default_delivery_address: subscription_raw_data.default_delivery_address,
    default_delivery_note: subscription_raw_data.default_delivery_note,
    default_delivery_latitude: subscription_raw_data.default_delivery_latitude,
    default_delivery_longitude: subscription_raw_data.default_delivery_longitude,
    default_delivery_details: subscription_raw_data.default_delivery_details,
    default_recipient_name: subscription_raw_data.default_recipient_name,
    default_recipient_phone: subscription_raw_data.default_recipient_phone,
    default_payment_method: subscription_raw_data.default_payment_method,
    default_payment_info: buildPaymentInfo(subscription_raw_data.default_payment_info),
    repeat_interval: subscription_raw_data.repeat_interval,
    theme_id: subscription_raw_data.theme_id,
  }
}

export function buildPaymentInfo(default_payment_info: string | object) {
  if (typeof default_payment_info === 'string') {
    if (JSONService.isJSONString(default_payment_info)) {
      return default_payment_info
    } else {
      return 'error'
    }
  }
  return JSON.stringify(default_payment_info)
}

export function* cancelCustomerOrder({ id }: { id: number }) {
  const token: string = yield select(getToken)
  yield put(actions.cancellingOrder())
  const response: cancelApiResponse = yield call(cancelOrderApi, {
    token,
    id,
  })
  if (!response.error) {
    yield put(actions.cancellingOrderDone(id, response.message))
  } else {
    yield put(actions.cancellingOrderFailed(response.message))
  }
}

export function cancelOrderApi({ token, id }: { token: string, id: number }) {
  return ApiSalesOrder.create()
    .cancel(id, token)
    .then((response: ApiResponse<cancelApiResponse>) => response.data)
    .catch((error: ApiResponse<cancelApiResponse>) => error)
}
