import {
  call,
  put,
  select,
} from 'redux-saga/effects'
import SalesOrderActions from '../Redux/SalesOrderRedux'
import ApiSalesOrder from '../Services/ApiSalesOrder'
import LogEventService from '../Services/LogEventService'
import { ApiResponse } from 'apisauce'
import { createApiResponse } from '../Services/ApiSalesOrder'
import { AppState } from '../Redux/CreateStore'
import { OrderData } from '../Redux/SalesOrderRedux'

export const getToken = (state: AppState): string => state.login.token

export function createOrderApi(order: OrderData, token: string) {
  return ApiSalesOrder.create()
    .create(order, token)
    .then((response: ApiResponse<createApiResponse>) => response.data)
    .catch((error: ApiResponse<createApiResponse>) => error)
}

export function* createOrder({order}: {order: OrderData}) {
  const token: string = yield select(getToken)
  yield put(SalesOrderActions.creatingOrder('Loading...'))

  const response: createApiResponse = yield call(createOrderApi, order, token)

  if (response !== null && !response.error) {
    yield put(SalesOrderActions.createSuccess(response.data, response.invoice_url, response.checkout_url))

    let itemInCart: number = 0
    let firstItemPrice: number = 0
    if (order.type === 'item') {
      order.deliveries.map((delivery) => {
        delivery.items.map((item) => {
          itemInCart += item.quantity
        })
      })
    } else if (order.type === 'package') {
      order.deliveries[0].packages.map((item) => {
        itemInCart = item.dates.length
      })
    } else if (order.type === 'subscription') {
      itemInCart = 1
      firstItemPrice = order.deliveries[0].first_item.price
    }

    const logEventParameters = {
      order_type: order.type,
      total_item: itemInCart,
      checkout_subtotal: order.total.hasOwnProperty('amount') ? order.total.amount : firstItemPrice,
      delivery_fee: order.total.hasOwnProperty('deliveryFee') ? order.total.deliveryFee : 0,
      promo_code: order.coupon,
      discount: order.total.hasOwnProperty('discount') ? order.total.discount : 0,
      cashback: order.total.hasOwnProperty('cashback') ? order.total.cashback : 0,
      currency: 'IDR',
      value: order.total.hasOwnProperty('total') ? order.total.total : firstItemPrice,
      checkout_option: order.payment.method
    }
    yield LogEventService.logEvent('ecommerce_purchase', logEventParameters, ['default'])
  } else {
    const error = response !== null ? response.message : 'Fail to create order'
    yield put(SalesOrderActions.createFailed(error))
  }
}
