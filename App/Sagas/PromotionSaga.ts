import {call, put} from 'redux-saga/effects'
import actions from '../Redux/PromotionRedux'
import api, { getPromotionBannersApiResponse } from '../Services/ApiPromotion'

export function fetchBannersApi () {
  return api.create()
    .getPromotionBanners()
    .then(response => response.data)
    .catch(error => error)
}

export function * fetchBanners () {
  yield put(actions.fetchingBanners())
  const response: getPromotionBannersApiResponse = yield call(fetchBannersApi)
  if (response !== null && !response.error) {
    yield put(actions.fetchingBannersSuccess(response.data))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch banners'
    yield put(actions.fetchingBannersFailed(error))
  }
}
