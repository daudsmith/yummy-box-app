import {call, put} from 'redux-saga/effects'
import actions from '../Redux/ForgotPasswordRedux'
import api, { updateAccountInfoFieldApiResponse } from '../Services/ApiCustomer'
import { ApiResponse } from 'apisauce'

export function * forgotPassword ({email}: ReturnType<typeof actions.forgotPassword>) {
  yield put(actions.submitting())
  const response: updateAccountInfoFieldApiResponse = yield call(forgotPasswordApi, email)
  if (!response.error) {
    yield put(actions.submittingDone())
  } else {
    yield put(actions.submittingFailed(response.message))
  }
}
export function forgotPasswordApi (email: string) {
  return api.create()
    .forgotPassword(email)
    .then((response: ApiResponse<updateAccountInfoFieldApiResponse>) => response.data)
    .catch((error: ApiResponse<updateAccountInfoFieldApiResponse>) => error)
}
