import { call, put, select } from 'redux-saga/effects'
import actions from '../Redux/CustomerAccountRedux'
import api, { checkClientApiResponse, getWalletApiResponse, updateAccountInfoFieldApiResponse } from '../Services/ApiCustomer'
import apiRequestOtp from '../Services/ApiRequestOtp'
import { getNewUserData } from './LoginSagas'
import { AppState } from '../Redux/CreateStore'

export const getToken = (state: AppState) => state.login.token

export function fetchWalletApi(token: string) {
  return api.create()
    .getWallet(token)
    .then((response) => response.data)
    .catch((error) => error)
}

export function changePasswordApi({ token, old_password, password, password_confirmation }: { token: string, old_password: string, password: string, password_confirmation: string }) {
  return api.create()
    .changePassword(token, old_password, password, password_confirmation)
    .then((response) => response.data)
    .catch((error) => error)
}

export function changePhoneNumberApi({ token, phone }: { token: string, phone: string }) {
  return api.create()
    .changePhoneNumber(token, phone)
    .then((response) => response.data)
    .catch((error) => error)
}

export function changeEmailApi({ token, newEmail }: { token: string, newEmail: string }) {
  return api.create()
    .changeEmail(token, newEmail)
    .then((response) => response.data)
    .catch((error) => error)
}

export function requestOtpApi({ newPhoneNumber }: { newPhoneNumber: string }) {
  return apiRequestOtp.create()
    .requestOtp(newPhoneNumber)
    .then((response) => response.data)
    .catch((error) => error)
}

export function checkClientApi(token: string) {
  return api.create()
    .checkClient(token)
    .then((response) => response.data)
    .catch((error) => error)
}

export function* fetchWallet() {
  const token: string = yield select(getToken)
  yield put(actions.fetchingWallet())
  const response: getWalletApiResponse = yield call(fetchWalletApi, token)
  const clientResponse: checkClientApiResponse = yield call(checkClientApi, token)
  if (response !== null && !response.error) {
    yield put(actions.fetchingWalletDone(response))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch wallet'
    yield put(actions.fetchingWalletFailed(error))
  }
  if (clientResponse && !clientResponse.error) {
    yield put(actions.checkClientDone(clientResponse.data))
  } else {
    yield put(actions.checkClientFailed())
  }
}

export function* changePassword({ old_password, password, password_confirmation }: ReturnType<typeof actions.changePassword>) {
  const token: string = yield select(getToken)
  yield put(actions.changingPassword())
  const response: updateAccountInfoFieldApiResponse = yield call(changePasswordApi, { token, old_password, password, password_confirmation })
  if (response !== null && !response.error) {
    yield put(actions.changePasswordSuccess())
  } else {
    const error = response !== null ? response.message : 'Failed to change password'
    yield put(actions.changePasswordFailed(error))
  }
}

export function* sendOtpNumber({ newPhoneNumber }: ReturnType<typeof actions.sendOtpNumber>) {
  yield call(requestOtpApi, { newPhoneNumber })
}

export function* changePhoneNumberValidate({ phone }: ReturnType<typeof actions.changePhoneNumberValidate>) {
  const token: string = yield select(getToken)
  yield put(actions.changingPhoneNumber())
  const response: updateAccountInfoFieldApiResponse = yield call(changePhoneNumberApi, { token, phone })
  if (response !== null && !response.error) {
    yield call(getNewUserData)
    yield put(actions.changePhoneNumberSuccess())
  } else {
    const error = response !== null ? response.message : 'Failed to change phone number'
    yield put(actions.changePhoneNumberFailed(error))
  }
}

export function* changeEmail({ newEmail }: ReturnType<typeof actions.changeEmail>) {
  const token: string = yield select(getToken)
  yield put(actions.changingEmail())
  const response: updateAccountInfoFieldApiResponse = yield call(changeEmailApi, { token, newEmail })
  if (response !== null && !response.error) {
    yield call(getNewUserData)
    yield put(actions.changeEmailSuccess())
  } else {
    const error = response !== null ? response.message : 'Email address is already registered'
    yield put(actions.changeEmailFailed(error))
  }
}
