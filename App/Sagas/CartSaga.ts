import _ from 'lodash'
import {
  call,
  put,
  select,
  all,
  take,
} from 'redux-saga/effects'
import actions, {
  CartItem,
  CartItemPackage,
  CartState,
  CartTypes,
  Item,
} from '../Redux/CartRedux'
import Api, {
  apiResponse,
  cartMeta,
} from '../Services/ApiCart'
import CheckoutService from '../Services/CheckoutService'
import AddressTypeActions, { CompanyAddress } from '../Redux/AddressTypeRedux'
import CustomerAddressTypeActions, { Address } from '../Redux/CustomerAddressRedux'
import CustomerOrderTypeActions from '../Redux/CustomerOrderRedux'
import MyMealPlanActions from '../Redux/MyMealPlanRedux'
import { fetchCompanyAddressesApi } from './AddressTypeSaga'
import { fetchAddressApi } from './CustomerAddressSaga'
import { NavigationActions } from 'react-navigation'
import LogEventService from '../Services/LogEventService'
import { AppState } from '../Redux/CreateStore'
import { InitSetting } from '../Redux/SettingRedux'
import moment, { Moment } from 'moment'
import {
  itemImageData,
  orderItemData,
} from '../Redux/SalesOrderRedux'
import { ApiResponse } from 'apisauce'
import { User } from '../Redux/LoginRedux'
import { Toast } from 'native-base'

export const getItems = (state: AppState): CartItem[] => state.cart.cartItems
export const getCart = (state: AppState): CartState => state.cart
export const getMultipleAddress = (state: AppState): boolean => state.cart.useMultipleAddress
export const getCartType = (state: AppState): string => state.cart.type
export const getCurrentAmount = (state: AppState): number => state.cart.amount
export const isDinner = (state: AppState): boolean => state.cart.dinner
export const isLunch = (state: AppState): boolean => state.cart.lunch
export const cartCutOff = (state: AppState): string => state.cart.cutoff
export const cartCutDay = (state: AppState): number => state.cart.cutDay
export const cartCategory = (state: AppState): string => state.cart.category
export const exclusiveCart = (state: AppState): boolean => state.cart.exclusive
export const partnerCart = (state: AppState): string => state.cart.partner
export const isError = (state: AppState): string => state.cart.fetchingTotalsError
export const navigationTo = (state: AppState): string => state.cart.navigateTo
export const getMeta = (state: AppState): cartMeta => state.cart.meta
export const getSetting = (state: AppState): InitSetting => state.setting
export const getToken = (state: AppState): string => state.login.token

export const buildDelivery = (
  item: any,
  date: Moment,
  type: string,
  defaultTime: string | null,
): CartItem => {
  if (type === 'package') {
    return {
      date: date.format('YYYY-MM-DD'),
      deliveryTime: defaultTime || null,
      delivery: null,
      items: [],
      packages: [
        buildThemeDeliveryItem(item),
      ],
    }
  } else if (type === 'subscription') {
    return {
      date: date.format('YYYY-MM-DD'),
      deliveryTime: defaultTime || null,
      delivery: null,
      items: [],
      packages: [],
      subscription: item,
    }
  } else {
    return {
      date: date.format('YYYY-MM-DD'),
      deliveryTime: defaultTime || null,
      delivery: null,
      items: [
        buildDeliveryItem(item),
      ],
      packages: [],
    }
  }
}

interface SingleItemData {
  id: number
  name: string
  sku: string
  base_price: number
  cogs: number
  description: string
  short_description: string
  tax_class: string
  ingredients: string
  sale_price: number
  daily_quantity: number
  is_available: boolean
  catalog_image?: itemImageData
  daily_remaining_quantity: number
  home_sorting: number
  catalog_sorting: number
  kitchen_meal_name: string
  tags?: TagsFormat
  images?: ImageItemCatalogFormat
  nutrition?: NutritionFormat
  cutoff?: string
  cutoff_day?: number
}

interface NutritionFormat {
  data: NutritionData[]
}

interface NutritionData {
  id: number
  item_id: number
  nutrient_id: number
  nutrient_parent_id: number | null
  amount: number
  name: string
  unit: string
}

interface TagsFormat {
  data: TagsData[]
}

interface TagsData {
  id: number
  name: string
  icon?: string
  filterable: number
  display_on_catalog: number
  filter_type: string
  description: string
}

interface ImageItemCatalogFormat {
  data: ImageItemCatalogData[]
}

interface ImageItemCatalogData {
  is_catalog_image: boolean
  sort: number
  thumbs: ImageItemCatalogThumbnail
}

interface ImageItemCatalogThumbnail {
  small_thumb: string
  medium_thumb: string
}

interface PackageItemData {
  theme_name: string
  theme_id: number
  package_id: number
  sell_price: number
  dates: string[]
  package_name: string
  cart_image: string
}

interface Params {
  meal_id: number,
  meal_name: string,
  meal_category_name: string
}

export const calculateCartAmount = (cartItems: CartItem[]): number => {
  let amount: number = 0
  cartItems.map((cart: CartItem) => {
    if (cart.items.length) {
      cart.items.map((item: Item) => {
        amount += item.sale_price * item.quantity
      })
    }
  })
  return amount
}

export const buildThemeDeliveryItem = (item: PackageItemData): CartItemPackage => {
  return {
    theme_id: item.theme_id,
    package_id: item.package_id,
    dates: item.dates,
    quantity: 1,
    package_name: item.package_name,
    cart_image: item.cart_image,
  }
}

export const buildDeliveryItem = (item: SingleItemData): orderItemData => {
  return {
    id: item.id,
    name: item.name,
    sale_price: item.sale_price,
    tax_class: item.tax_class,
    images: item.catalog_image,
    quantity: 1,
    remaining_daily_quantity: item.daily_remaining_quantity,
    cutoff: item.cutoff ? item.cutoff : null,
    cutoff_day: item.cutoff_day ? item.cutoff_day : 0,
  }
}

export function validateCartApi({ cart, token }: { cart: CartState, token: string }) {
  return Api.create()
    .validate(cart, token)
    .then((response: ApiResponse<apiResponse>) => response.data)
    .catch((error: ApiResponse<apiResponse>) => error)
}

export const singleAddressDeliveryValidation = (cartItems: CartItem[]): CartItem[] => {
  const delivery = cartItems[0].delivery
  const deliveryTime = cartItems[0].deliveryTime
  if (cartItems.length > 1) {
    return cartItems.map((item: CartItem) => {
      return {
        ...item,
        delivery: delivery,
        deliveryTime: deliveryTime,
      }
    })
  }
  return cartItems
}

export const sortCartItemsDate = (cartItems: CartItem[]): CartItem[] => {
  if (cartItems.length > 0) {
    cartItems.sort((a, b) => {
      let first = a.date
      let second = b.date
      first = first.split('-').join('')
      second = second.split('-').join('')
      return first > second
        ? 1
        : first < second
          ? -1
          : 0
    })
  }
  return cartItems
}

export function* validateCart() {
  yield put(actions.fetchingCartTotals())
  const cart: CartState = yield select(getCart)
  const token: string = yield select(getToken)
  if (cart.isEmpty) {
    return false
  }
  const response: apiResponse = yield call(validateCartApi, { cart, token })
  if (response && !response.error) {
    const cartItems: CartItem[] = response.data.cartItems
    const totals = response.data.totals
    const dinner = response.data.dinner
    const lunch = response.data.lunch
    const proceedToPayment = CheckoutService.checkIfCartCanProceedToPayment(cartItems)
    const useMultipleAddress = cartItems.length < 1
      ? false
      : yield select(getMultipleAddress)

    yield put(actions.userUpdateCart({
      cartItems: cartItems,
      isEmpty: cartItems.length < 1,
      amount: totals.subtotal,
      totals: totals,
      coupon: cart.coupon,
      dinner,
      lunch,
      useMultipleAddress: useMultipleAddress,
      proceedToPayment: proceedToPayment,
      meta: response.meta,
      category: cart.category,
      exclusive: cart.exclusive,
      partner: cart.partner,
      cutoff: cart.cutoff,
      cutDay: cart.cutDay,
    }))
    const isPromotionValid = cart.coupon === null
      ? null
      : response.meta.promotion.isValid
    yield put(actions.setPromotionValidity(isPromotionValid))
    yield put(actions.fetchingCartTotalsDone())
  } else {
    const error = response.message
      ? response.message
      : 'Cannot refresh cart due to network issues'
    yield put(actions.fetchingCartTotalsFailed(error))
  }
}

export function* addToCart({ item, date, category, exclusive }: ReturnType<typeof actions.addToCart>) {
  const isLoggedIn: User | null | undefined = yield select(
    (state: AppState) => state.login.user)

  if (!isLoggedIn) {
    yield put(NavigationActions.navigate({ routeName: 'Login' }))
    return
  }

  const cartItems: CartItem[] = yield select(getItems)
  const cartType: string = yield select(getCartType)
  const setting: InitSetting = yield select(getSetting)
  const dinner: boolean = yield select(isDinner)
  const lunch: boolean = yield select(isLunch)
  const meta: cartMeta = yield select(getMeta)
  let cartCutoff = yield select(cartCutOff)
  let cutDay = yield select(cartCutDay)

  let defaultTime: string = null
  if (setting !== null) {
    if (setting.data.hasOwnProperty('delivery_time_slots')) {
      const deliveryTimeSlots = JSON.parse(setting.data['delivery_time_slots'])
      if (deliveryTimeSlots.length < 2) {
        defaultTime = deliveryTimeSlots[0]
      }
    }
  }

  let newCartItems: CartItem[] = []
  if (cartItems.length < 1) {
    const cartItem: CartItem = buildDelivery(item, date, cartType, defaultTime)
    newCartItems.push(cartItem)
  } else {
    if (cartType === 'package') {
      let newPackages = []
      newPackages.push(buildThemeDeliveryItem(item))
      const newCartItem: CartItem = {
        ...cartItems[0],
        date: date.format('YYYY-MM-DD'),
        packages: newPackages,
      }
      newCartItems.push(newCartItem)
    } else if (cartType === 'subscription') {
      const newCartItem = {
        ...cartItems[0],
        date: date.format('YYYY-MM-DD'),
        subscription: item,
      }
      newCartItems.push(newCartItem)
    } else {
      for (let delivery of cartItems) {
        const items = []
        for (let deliveryItem of delivery.items) {
          items.push({ ...deliveryItem })
        }
        newCartItems.push({
          ...delivery,
          items: items,
        })
      }
      const deliveryDateIndex = _.findIndex(newCartItems, (delivery) => {
        return delivery.date === date.format('YYYY-MM-DD')
      })
      if (deliveryDateIndex < 0) {
        const cartItem = buildDelivery(item, date, cartType, defaultTime)
        newCartItems.push(cartItem)
      } else {
        const deliveryItemIndex = _.findIndex(
          newCartItems[deliveryDateIndex].items, (deliveryItem) => {
            return deliveryItem.id === item.id
          })
        if (deliveryItemIndex < 0) {
          newCartItems[deliveryDateIndex].items.push(buildDeliveryItem(item))
        } else {
          newCartItems[deliveryDateIndex].items[deliveryItemIndex].quantity++
          newCartItems[deliveryDateIndex].items[deliveryItemIndex].remaining_daily_quantity--
        }
      }
    }
  }
  const useMultipleAddress = yield select(getMultipleAddress)
  const validatedAddressCart = useMultipleAddress
    ? newCartItems
    : singleAddressDeliveryValidation(newCartItems)
  const proceedToPayment = CheckoutService.checkIfCartCanProceedToPayment(
    validatedAddressCart)
  const sortedCartItems = sortCartItemsDate(validatedAddressCart)

  // SET CUTOFF if any
  if (item.cutoff !== null) {
    const mealDate = moment().utcOffset(7).add(1, 'days')
    const cutoff: Moment = moment(mealDate.format('YYYY-MM-DD') + ' ' + item.cutoff).utcOffset(7)
    if (cartCutoff) {
      cartCutoff = moment(cartCutoff).utcOffset(7)
      if (cartCutoff.isAfter(cutoff)) {
        cartCutoff = cutoff
      }
    } else {
      cartCutoff = cutoff
    }
  }
  if (item.cutoff_day !== null) {
    if (cutDay > 0) {
      if (item.cutoff_day > cutDay) {
        cutDay = item.cutoff_day
      }
    } else {
      cutDay = item.cutoff_day
    }
  }

  yield put(actions.userUpdateCart({
    cartItems: sortedCartItems,
    isEmpty: false,
    amount: cartType === 'item'
      ? calculateCartAmount(newCartItems)
      : item.sell_price,
    proceedToPayment: proceedToPayment,
    useMultipleAddress: useMultipleAddress,
    partner: item.partner,
    dinner,
    lunch,
    meta,
    category,
    exclusive,
    cutoff: cartCutoff ? cartCutoff.format('YYYY-MM-DD HH:mm:ss') : null,
    cutDay: cutDay > 0 ? cutDay : 0,
  }))
  yield call(validateCart)
  const hasError: string = yield select(isError)

  if (!hasError) {
    if (cartType === 'subscription') {
      const screeName: string = yield select(navigationTo)
      if (screeName) {
        yield put(NavigationActions.navigate({
          routeName: screeName,
          key: screeName
        }))
      }
    } else if (cartType === 'package') {
      const screeName: string = yield select(navigationTo)
      const params = {
        order_type: 'package',
        currency: 'IDR',
        value: item.sell_price,
      }
      LogEventService.logEvent('begin_checkout', params, ['default'])
      if (screeName) {
        yield put(NavigationActions.navigate({
          routeName: screeName,
          key: screeName,
          params: {
            navigateFrom: 'ThemeCalendarScreen',
            title: item.theme_name,
            id: item.theme_id,
          },
        }))
      }
    }
  }
  else{
    Toast.show({
      text: hasError,
      buttonText: '',
      duration: 1500,
    })
    yield put(actions.resetCart())
  }
}

export function* removeFromCart({ item, date }: ReturnType<typeof actions.removeFromCart>) {
  const cartItems: CartItem[] = yield select(getItems)
  const dinner: boolean = yield select(isDinner)
  const lunch: boolean = yield select(isLunch)
  const meta: cartMeta = yield select(getMeta)
  const category: string = yield select(cartCategory)
  const exclusive: boolean = yield select(exclusiveCart)
  const partner: string = yield select(partnerCart)
  let cartCutoff: string = yield select(cartCutOff)
  let cutDay: number = yield select(cartCutDay)
  let currentCartItems: CartItem[] = []
  const params: Params = {
    meal_id: item.id,
    meal_name: item.name,
    meal_category_name: category
  }
  LogEventService.logEvent('click_decrease_qty', params, ['default'])
  if (cartItems.length > 0) {
    cartItems.map((cartItem: CartItem) => {
      let items = []
      cartItem.items.map((item: Item) => {
        items.push({ ...item })
      })
      currentCartItems.push({
        ...cartItem,
        items: items,
      })
    })

    let deletedCutoff = null
    const deliveryDateIndex: number = _.findIndex(currentCartItems,
      (delivery: CartItem) => delivery.date === date.format('YYYY-MM-DD'))
    if (deliveryDateIndex > -1) {
      const deliveryItemIndex = _.findIndex(
        currentCartItems[deliveryDateIndex].items,
        (deliveryItem) => deliveryItem.id === item.id)
      if (deliveryItemIndex > -1) {
        let quantity = currentCartItems[deliveryDateIndex].items[deliveryItemIndex].quantity
        if (quantity > 1) {
          currentCartItems[deliveryDateIndex].items[deliveryItemIndex].quantity--
          currentCartItems[deliveryDateIndex].items[deliveryItemIndex].remaining_daily_quantity++
        } else {
          deletedCutoff = currentCartItems[deliveryDateIndex].items[deliveryItemIndex].cutoff
          currentCartItems[deliveryDateIndex].items.splice(deliveryItemIndex, 1)
        }
        if (currentCartItems[deliveryDateIndex].items.length < 1) {
          currentCartItems.splice(deliveryDateIndex, 1)
        }
      }
    }
    const currentCartAmount = calculateCartAmount(currentCartItems)
    const isEmpty = currentCartAmount < 1
    const useMultipleAddress = isEmpty
      ? false
      : yield select(getMultipleAddress)
    const proceedToPayment = CheckoutService.checkIfCartCanProceedToPayment(
      currentCartItems)

    if (!isEmpty && cartCutoff && deletedCutoff) {
      cartCutoff = null
      cutDay = 0
      currentCartItems.map(cartItem => {
        cartItem.items.map((item) => {
          if (item.cutoff) {
            const itemCutoff = moment(item.cutoff, 'HH:mm:ss').utcOffset(7)
            if (cartCutoff && moment(cartCutoff).isAfter(itemCutoff)) {
              cartCutoff = itemCutoff.format('YYYY-MM-DD HH:mm:ss')
            } else {
              cartCutoff = itemCutoff.format('YYYY-MM-DD HH:mm:ss')
            }
          }
          if (item.cutoff_day) {
            if (cutDay && item.cutoff_day > cutDay) {
              cutDay = item.cutoff_day
            } else {
              cutDay = item.cutoff_day
            }
          }
        })
      })
    }

    yield put(actions.userUpdateCart({
      cartItems: currentCartItems,
      isEmpty,
      amount: currentCartAmount,
      proceedToPayment: proceedToPayment,
      useMultipleAddress: useMultipleAddress,
      dinner,
      lunch,
      meta,
      category: currentCartItems.length > 0 ? category : '',
      exclusive: currentCartItems.length > 0 ? exclusive : false,
      cutoff: cartCutoff,
      cutDay: cutDay,
      partner: partner,
    }))
    if (!isEmpty) {
      yield call(validateCart)
    } else {
      yield put(actions.resetCart())
    }
  }
}

export function* updateDeliveryTime({ date, time, useMultipleAddress }: ReturnType<typeof actions.updateDeliveryTime>) {
  const cartItems: CartItem[] = yield select(getItems)
  const cartType: string = yield select(getCartType)
  const currentAmount: number = yield select(getCurrentAmount)
  const dinner: boolean = yield select(isDinner)
  const lunch: boolean = yield select(isLunch)
  const meta: cartMeta = yield select(getMeta)
  const category: string = yield select(cartCategory)
  const exclusive: boolean = yield select(exclusiveCart)
  const partner: string = yield select(partnerCart)
  const cartCutoff: string = yield select(cartCutOff)
  const cutDay: number = yield select(cartCutDay)
  let currentCartItems: CartItem[] = []
  if (cartItems.length > 0) {
    cartItems.map((cartItem: CartItem) => {
      if (cartItem.date === date || !useMultipleAddress) {
        currentCartItems.push({
          ...cartItem,
          deliveryTime: time,
        })
      } else {
        currentCartItems.push({ ...cartItem })
      }
    })
  }
  const proceedToPayment: boolean = CheckoutService.checkIfCartCanProceedToPayment(
    currentCartItems)
  yield put(actions.userUpdateCart({
    cartItems: currentCartItems,
    isEmpty: currentCartItems.length < 1,
    amount: cartType === 'item'
      ? calculateCartAmount(currentCartItems)
      : currentAmount,
    useMultipleAddress: useMultipleAddress,
    proceedToPayment: proceedToPayment,
    dinner,
    lunch,
    meta,
    category,
    exclusive,
    partner,
    cutoff: cartCutoff,
    cutDay: cutDay,
  }))
}

export function* updateDeliveryAddress({ date, address, useMultipleAddress }: ReturnType<typeof actions.updateDeliveryAddress>) {
  const cartItems: CartItem[] = yield select(getItems)
  const cartType: string = yield select(getCartType)
  const currentAmount: number = yield select(getCurrentAmount)
  const dinner: boolean = yield select(isDinner)
  const lunch: boolean = yield select(isLunch)
  const meta: cartMeta = yield select(getMeta)
  const category: string = yield select(cartCategory)
  const exclusive: boolean = yield select(exclusiveCart)
  const partner: string = yield select(partnerCart)
  const cartCutoff: string = yield select(cartCutOff)
  const cutDay: number = yield select(cartCutDay)
  let currentCartItems: CartItem[] = []
  if (cartItems.length > 0) {
    cartItems.map((cartItem: CartItem) => {
      if (cartItem.date === date || !useMultipleAddress) {
        currentCartItems.push({
          ...cartItem,
          delivery: address,
        })
      } else {
        currentCartItems.push({ ...cartItem })
      }
    })
  }
  const proceedToPayment = CheckoutService.checkIfCartCanProceedToPayment(
    currentCartItems)
  yield put(actions.userUpdateCart({
    cartItems: currentCartItems,
    isEmpty: currentCartItems.length < 1,
    amount: cartType === 'item'
      ? calculateCartAmount(currentCartItems)
      : currentAmount,
    useMultipleAddress: useMultipleAddress,
    proceedToPayment: proceedToPayment,
    dinner,
    lunch,
    meta,
    category,
    exclusive,
    partner,
    cutoff: cartCutoff,
    cutDay: cutDay,
  }))
}

export function* cancelUsingMultipleAddress() {
  const cartItems: CartItem[] = yield select(getItems)
  const dinner: boolean = yield select(isDinner)
  const lunch: boolean = yield select(isLunch)
  const meta: cartMeta = yield select(getMeta)
  const category: string = yield select(cartCategory)
  const exclusive: boolean = yield select(exclusiveCart)
  const partner: string = yield select(partnerCart)
  const cartCutoff: string = yield select(cartCutOff)
  const cutDay: number = yield select(cartCutDay)
  let currentCartItems: CartItem[] = []
  if (cartItems.length > 0) {
    const useDeliveryTime = cartItems[0].deliveryTime
    const useDeliveryAddress = cartItems[0].delivery
    cartItems.map((cartItem: CartItem) => {
      currentCartItems.push({
        ...cartItem,
        delivery: useDeliveryAddress,
        deliveryTime: useDeliveryTime,
      })
    })
  }
  const proceedToPayment: boolean = CheckoutService.checkIfCartCanProceedToPayment(currentCartItems)
  yield put(actions.userUpdateCart({
    cartItems: currentCartItems,
    isEmpty: currentCartItems.length < 1,
    amount: calculateCartAmount(currentCartItems),
    useMultipleAddress: false,
    proceedToPayment: proceedToPayment,
    dinner,
    lunch,
    meta,
    category,
    exclusive,
    partner,
    cutoff: cartCutoff,
    cutDay: cutDay,
  }))
}

export function* applyCoupon({ coupon }: ReturnType<typeof actions.applyCoupon>) {
  const cart = yield select(getCart)
  const meta: cartMeta = yield select(getMeta)
  const category: string = yield select(cartCategory)
  const exclusive: boolean = yield select(exclusiveCart)
  const cartCutoff: Moment = yield select(cartCutOff)
  const cutDay: Moment = yield select(cartCutDay)
  const cartWithCoupon = {
    ...cart,
    coupon,
    meta,
    category,
    exclusive,
    cutoff: cartCutoff,
    cutDay: cutDay,
  }
  yield put(actions.userUpdateCart(cartWithCoupon))
  if (coupon !== null) {
    yield put(actions.applyingCoupon())
    yield call(validateCart)
    yield put(actions.applyCouponFinished())
  }
}

/**
 * This saga yields a new delivery address then navigate to the screen that requested it.
 * @param {{ screenAfterNewAddressSet: { routeName: string, key: string}, useMultipleAddress: boolean, cartType: 'checkout' | 'modify_mmp' | 'modify_subscription_order', cartItem: any  }} payload
 */
export function* getNewAddress({ screenAfterNewAddressSet, useMultipleAddress, cartType: type, cartItem }: ReturnType<typeof actions.getNewAddress>) {
  // let the screen knows that personal address and company addresses are being fetched
  yield put(CustomerAddressTypeActions.fetchingAddress())
  yield put(AddressTypeActions.companyAddressesFetching())

  // fetch the personal addresses and company addresses
  const { token }: { token: string } = yield select(
    (state: AppState) => state.login)
  const [companyAddressesResponse, personalAddressesResponse] = yield all([
    call(fetchCompanyAddressesApi, token),
    call(fetchAddressApi, token),
  ])
  const companyAddresses: CompanyAddress[] = companyAddressesResponse.data
  const personalAddresses: Address[] = personalAddressesResponse.data

  // store the addresses in redux store
  yield put(AddressTypeActions.updateCompanyAddresses(companyAddresses))
  yield put(CustomerAddressTypeActions.fetchingAddressDone(personalAddresses))

  let SearchLocationScreen = 'MMPSearchLocation'
  if (type === 'checkout') {
    SearchLocationScreen = 'CoSearchLocation'
  }
  // navigate to next screen based on the addresses
  if (companyAddresses.length > 0) {
    yield put(NavigationActions.navigate({
      routeName: 'AddressTypeScreen',
    }))
  } else if (personalAddresses.length > 0) {
    yield put(NavigationActions.navigate({
      routeName: 'AddressOptionScreen',
      params: {
        addresses: personalAddresses,
        addressType: 'personal',
      },
    }))
  } else if (personalAddresses.length === 0) {
    yield put(NavigationActions.navigate({
      routeName: SearchLocationScreen,
    }))
  }

  // wait for the new delivery address
  const { place }: { place: Address } = yield take(
    CartTypes.SET_NEW_DELIVERY_ADDRESS)

  // process the new address based on the type of the cart
  if (type === 'checkout') {
    let date: string
    let address: any
    if (_.isEmpty(cartItem.delivery)) {
      const user: User = yield select((state: AppState) => state.login.user)
      const cart: CartState = yield select((state: AppState) => state.cart)
      address = CheckoutService.buildNewDeliveryAddress(user, place)
      date = !useMultipleAddress
        ? cart.cartItems[0].date
        : cartItem.date
    } else {
      const placeToSet = {
        ...place,
        landmark: place.landmark || place.label,
      }
      address = {
        ...cartItem.delivery,
        note: placeToSet.note,
        name: placeToSet.name,
        phone: placeToSet.phone,
        place: placeToSet,
      }
      date = cartItem.date
    }

    const updateDeliveryAddressData: any = { date, address, useMultipleAddress }
    yield updateDeliveryAddress(updateDeliveryAddressData)
  } else if (type === 'modify_subscription_order') {
    const keys: string[] = [
      'default_delivery_address',
      'default_delivery_latitude',
      'default_delivery_longitude',
      'default_delivery_note',
      'default_recipient_name',
      'default_recipient_phone',
    ]
    const values: string[] = [
      place.address,
      String(place.latitude),
      String(place.longitude),
      place.note,
      place.name,
      place.phone,
    ]
    yield put(
      CustomerOrderTypeActions.updateSubscriptionDefaultData(keys, values))
  } else if (type === 'modify_mmp') {
    const currentMMPDetail = cartItem
    const placeToSet = {
      ...place,
      landmark: place.landmark || place.label,
      id: currentMMPDetail.data.id,
      status: currentMMPDetail.data.status,
    }
    yield put(MyMealPlanActions.modifyMmpPackageDelivery(null, placeToSet))
  }

  // navigate back to the screen that requested the new address
  yield put(NavigationActions.navigate(screenAfterNewAddressSet))
}
