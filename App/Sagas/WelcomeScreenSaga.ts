import { call, put } from 'redux-saga/effects'
import actions from '../Redux/WelcomeScreenRedux'
import Api, { apiResponse } from '../Services/WelcomeScreenApi'
import { ApiResponse } from 'apisauce'

export function fetchSlidesApi() {
  return Api.create()
    .getWelcomeScreenSlides()
    .then((response: ApiResponse<apiResponse>) => response.data)
    .catch(error => error)
}

export function * fetchSlides () {
  yield put(actions.fetchingSlides())
  const slides: apiResponse = yield call(fetchSlidesApi)
  if (slides.error) {
    yield put(actions.fetchSlidesFailed())
  } else {
    yield put(actions.fetchSlidesDone(slides.data))
  }
}
