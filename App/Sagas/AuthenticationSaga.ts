import {
  call,
  select,
  put,
  delay,
} from 'redux-saga/effects'
import LoginActions, { LoginState } from '../Redux/LoginRedux'
import {
  NavigationActions,
  NavigationNavigateAction,
} from 'react-navigation'
import Api from '../Services/ApiCustomer'
import AppConfig from '../Config/AppConfig'
import { customerProfileApiResponse } from '../Services/ApiCustomer'
import { ApiResponse } from 'apisauce'

export function customerProfileApi(token: string) {
  return Api.create()
    .customerProfile(token)
    .then((response: ApiResponse<customerProfileApiResponse>) => response.status)
    .catch((error: ApiResponse<customerProfileApiResponse>) => error)
}

export const checkToken = (saga: any): any => {
  return function* (action: any) {
    const { login }: { login: LoginState } = yield select()
    const statusCode: number = yield call(customerProfileApi, login.token)
    if (statusCode === 200) {
      yield call(saga, action)
    } else {
      if (AppConfig.appEnvironment === 'test') {
        yield put(LoginActions.logout())
      } else {
        const navigationAction: NavigationNavigateAction = NavigationActions.navigate({
          routeName: 'Login',
        })
        yield put(navigationAction)
        yield delay(500)
        yield put(LoginActions.logout())
      }
    }
  }
}
