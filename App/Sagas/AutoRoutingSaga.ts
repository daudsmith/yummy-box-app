import {StackActions, NavigationActions} from 'react-navigation'
import {put} from 'redux-saga/effects'
import AutoRoutingActions from '../Redux/AutoRoutingRedux'

export function * addRoute ({routeName, params, isOpeningApp}: ReturnType<typeof AutoRoutingActions.addRoute>) {
  yield put(AutoRoutingActions.setRoute(routeName, params))
  if (!isOpeningApp) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'NavigationDrawer'})
      ]
    })
    yield put(resetAction)
  }
}