import { call, put } from 'redux-saga/effects'
import actions, { CustomerForm } from '../Redux/RegisterRedux'
import loginActions from '../Redux/LoginRedux'
import api, { registerApiResponse, validateIdentityApiResponse, validateApiResponse } from '../Services/ApiCustomer'
import { ApiResponse } from 'apisauce'

export function customerRegistrationApi(customer: CustomerForm) {
  return api.create()
    .register(customer)
    .then((response: ApiResponse<registerApiResponse>) => response.data)
    .catch((error: ApiResponse<registerApiResponse>) => error)
}

export function validateIdentityApi({ identity, email, phone }: { identity: string, email: string, phone: string }) {
  return api.create()
    .validateIdentity(identity, email, phone)
    .then((response: ApiResponse<validateIdentityApiResponse>) => response.data)
    .catch((error: ApiResponse<validateIdentityApiResponse>) => error)
}

export function validateCustomerApi({ customer }: { customer: CustomerForm }) {
  return api.create()
    .validate(customer)
    .then((response: ApiResponse<validateApiResponse>) => response.data)
    .catch((error: ApiResponse<validateApiResponse>) => error)
}

export function * registerCustomer ({ customer } : ReturnType<typeof actions.registerCustomer>) {
  yield put(actions.registeringCustomer())
  const response: registerApiResponse = yield call(customerRegistrationApi, customer)
  if (!response.error) {
    let registration_type: string = ''
    if (customer.social_login_type !== null){
      registration_type = `email_from_${customer.social_login_type}`
    } else {
      registration_type = customer.registration_type === 'email' ? 'email_manually_typed' : 'mobile_phone_number' 
    }
    response.data.registration_type = registration_type
    yield put(actions.registrationSuccess(response))
    yield put(loginActions.onLoginSuccess(response.data, response.token))
  } else {
    yield put(actions.registrationFailed(response.message))
  }
}

export function* validateIdentity({ identity }: ReturnType<typeof actions.validateIdentity>) {
  yield put(actions.validatingIdentity())
  const response: validateIdentityApiResponse = yield call(validateIdentityApi, { identity, email: null, phone: null })
  if (!response.error) {
    if (response.data) {
      yield put(actions.identityExist())
    } else {
      yield put(actions.identityNotExist())
    }
  }
}

export function* validateEmailPhone({ email, phone }: ReturnType<typeof actions.validateEmailPhone>) {
  yield put(actions.validatingEmailPhone())
  const response: validateIdentityApiResponse = yield call(validateIdentityApi, { identity: null, email, phone })
  if (!response.error) {
    if (response.data) {
      yield put(actions.emailPhoneExist())
    } else {
      yield put(actions.emailPhoneNotExist())
    }
  }
}

export function* validateRegisteredWithFacebook({ customer }: ReturnType<typeof actions.validateRegisteredWithFacebook>) {
  yield put(actions.validatingRegisterWithFacebook())
  const response: validateApiResponse = yield call(validateCustomerApi, { customer })
  if (!response.error) {
    if (response.exist) {
      yield put(actions.customerIsRegisteredWithFacebook())
    } else {
      yield put(actions.customerNotRegisteredWithFacebook())
    }
  }
}
