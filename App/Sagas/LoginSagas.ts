import { put, call, select } from 'redux-saga/effects'
import LoginActions, { User } from '../Redux/LoginRedux'
import AddressHistoryActions from '../Redux/AddressHistoryRedux'
import CreditTokenActions from '../Redux/CreditTokenTopUpRedux'
import CustomerAddressActions from '../Redux/CustomerAddressRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import CustomerCardActions from '../Redux/CustomerCardRedux'
import CustomerNotificationActions from '../Redux/CustomerNotificationRedux'
import CustomerVirtualAccountActions from '../Redux/CustomerVirtualAccountRedux'
import CustomerOrderActions from '../Redux/CustomerOrderRedux'
import MyMealPlanActions from '../Redux/MyMealPlanRedux'
import ForgotPasswordActions from '../Redux/ForgotPasswordRedux'
import RegistrationActions from '../Redux/RegisterRedux'
import CartActions from '../Redux/V2/CartRedux'
import OrderActions from '../Redux/SalesOrderRedux'
import ThemeActions from '../Redux/ThemeRedux'
import MealActions from '../Redux/MealsRedux'
import HomeContentActions from '../Redux/HomeContentRedux'
import PromotionActions from '../Redux/PromotionRedux'
import LastUsedActions from '../Redux/V2/LastUsedRedux'
import ApiCustomer, {
  authApiResponse,
  customerProfileApiResponse,
  deviceInfoUpdateApiResponse,
  logoutApiResponse,
  photoUploadApiResponse,
} from '../Services/ApiCustomer'
import messaging from '@react-native-firebase/messaging'
import DeviceInfo from 'react-native-device-info'
import { AppState } from '../Redux/CreateStore'
import { LoginState } from '../Redux/LoginRedux'
import { ApiResponse } from 'apisauce'

export const getToken = (state: AppState): LoginState => state.login
export const getUid = (state: AppState): string => state.login.uid

export function updateDeviceInfoApi(deviceInfo) {
  const { uid, fcm_token, token } = deviceInfo
  return ApiCustomer.create()
    .deviceInfoUpdate(uid, fcm_token, token)
    .then((response: ApiResponse<deviceInfoUpdateApiResponse>) => response.data)
    .catch((error: ApiResponse<deviceInfoUpdateApiResponse>) => error)
}

export function getFcmToken() {
  return messaging().getToken()
    .then((token: string) => token)
    .catch((error) => error)
}

export const getDeviceUniqueId = (): string => {
  return DeviceInfo.getUniqueId()
}

export function authApi({ identity, password }: { identity: string, password: string }) {
  return ApiCustomer.create()
    .auth(identity, password)
    .then((response: ApiResponse<authApiResponse>) => response.data)
    .catch((error: ApiResponse<authApiResponse>) => error)
}

export function uploadPhotoApi({ token, data }: { token: string, data: object }) {
  return ApiCustomer.create()
    .photoUpload(token, data)
    .then((response: ApiResponse<photoUploadApiResponse>) => response.data)
    .catch((error: ApiResponse<photoUploadApiResponse>) => error)
}

export function getNewUserDataApi({ token }: { token: string }) {
  return ApiCustomer.create()
    .customerProfile(token)
    .then((response: ApiResponse<customerProfileApiResponse>) => response.data)
    .catch((error: ApiResponse<customerProfileApiResponse>) => error)
}

export function logoutApi({ token, uid }: { token: string, uid: string }) {
  return ApiCustomer.create()
    .logout(token, uid)
    .then((response: ApiResponse<logoutApiResponse>) => response.data)
    .catch((error: ApiResponse<logoutApiResponse>) => error)
}

// attempts to login
export function* loginAttempt({ identity, password }: ReturnType<typeof LoginActions.loginAttempt>) {
  yield put(LoginActions.attemptingLogin())
  const response: authApiResponse = yield call(authApi, { identity, password })

  if (typeof response.token !== 'undefined') {
    const user: User = response.data
    const token: string = response.token
    yield call(onLoginSuccess, { user, token } as any)
  } else {
    const errorMsg = response.error
      ? (response.error as string)
      : 'Error on login'
    yield put(LoginActions.loginFailure(errorMsg))
  }
}
export function* onLoginSuccess({ user, token }: ReturnType<typeof LoginActions.onLoginSuccess>) {
  yield put(LoginActions.loginSuccess(user, token))

  const fcm_token = yield call(getFcmToken)
  const uid = yield call(getDeviceUniqueId)
  const deviceInfo = { uid, fcm_token, token }

  const response: deviceInfoUpdateApiResponse = yield call(updateDeviceInfoApi, deviceInfo)
  if (!response.error) {
    yield put(LoginActions.setFcmToken(fcm_token))
    yield put(LoginActions.setUid(uid))
  }

  yield put(CustomerAddressActions.fetchAddress())
}

export function* logout() {
  const { token } = yield select(getToken)
  const uid = yield select(getUid)
  yield put(LoginActions.resetState())
  yield put(CustomerAddressActions.reset())
  yield put(CartActions.resetCart())
  yield put(OrderActions.resetOrder())
  yield put(CustomerCardActions.resetCards())
  yield put(AddressHistoryActions.reset())
  yield put(CreditTokenActions.resetTokenTopUp())
  yield put(CustomerAccountActions.reset())
  yield put(CustomerNotificationActions.resetCustomerNotification())
  yield put(CustomerVirtualAccountActions.resetVirtualAccount())
  yield put(ForgotPasswordActions.reset())
  yield put(MyMealPlanActions.reset())
  yield put(RegistrationActions.reset())
  yield put(CustomerOrderActions.resetCustomerOrder())
  yield put(ThemeActions.reset())
  yield put(MealActions.reset())
  yield put(HomeContentActions.resetAll())
  yield put(PromotionActions.reset())
  yield put(LastUsedActions.resetLastUsed())
  yield call(logoutApi, { token, uid })
}

export function* updateFcmToken({ fcmToken }: ReturnType<typeof LoginActions.updateFcmToken>) {
  const { token }: { token: string } = yield select(getToken)
  const uid: string = yield call(getDeviceUniqueId)
  const response: deviceInfoUpdateApiResponse = yield call(updateDeviceInfoApi, { uid, fcmToken, token })
  if (!response.error) {
    yield put(LoginActions.setFcmToken(fcmToken))
    yield put(LoginActions.setUid(uid))
  }

}

export function* uploadPhoto({ data }: ReturnType<typeof LoginActions.uploadPhoto>) {
  const { token }: { token: string } = yield select(getToken)
  yield put(LoginActions.uploadingPhoto())
  const response: photoUploadApiResponse = yield call(uploadPhotoApi, { token, data })
  if (!response.error) {
    yield put(LoginActions.uploadingPhotoDone(response.data))
  } else {
    yield put(LoginActions.uploadingPhotoFailed(response.message))
  }
}

export function* getNewUserData() {
  const { token }: { token: string } = yield select(getToken)
  const response: customerProfileApiResponse = yield call(getNewUserDataApi, { token })
  if (!response.error) {
    const user = response.data
    yield put(LoginActions.loginSuccess(user, token))
  }
}
