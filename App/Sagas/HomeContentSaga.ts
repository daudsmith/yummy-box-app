import moment, { Moment } from 'moment'
import { call, put, select } from 'redux-saga/effects'
import actions, { ThemeItems } from '../Redux/HomeContentRedux'
import MealActions from '../Redux/MealsRedux'
import api, { GetMealsResponse, GetThemeListResponse, GetOffDates, GetPromoteResponse } from '../Services/ApiCatalog'
import settingApi, {getSettingResponse} from '../Services/ApiSetting'
import ThemeApi, { GetThemeDetailResponse, GetThemeDetailMeals } from '../Services/ApiTheme'
import AvailabilityDateService from '../Services/AvailabilityDateService'
import { generateDistinctThemeMeals } from './ThemeSaga'
import { AppState } from '../Redux/CreateStore'
import { ApiResponse } from 'apisauce'

export interface StringConstructor {
  format: (formatString: string, ...replacement: any[]) => string;
}

export const getToken = (state: AppState): string => state.login.token
const featuredThemeId = 96

export function fetchItemsApi (date: StringConstructor) {
  return api.create()
    .getMeals(date.format('YYYY-MM-DD'), 1)
    .then((response: ApiResponse<GetMealsResponse>) => response.data)
    .catch((error: ApiResponse<GetMealsResponse>) => error)
}

export function fetchThemesApi () {
  return api.create()
    .getThemeList()
    .then((response: ApiResponse<GetThemeListResponse>) => response.data)
    .catch((error: ApiResponse<GetThemeListResponse>) => error)
}

export function getOffDatesApi () {
  return api.create()
    .getOffDates()
    .then((response: ApiResponse<GetOffDates>) => response.data)
    .catch((error: ApiResponse<GetOffDates>) => error)
}

export function fetchCorporateThemesApi(token: string) {
  return api.create()
    .getCorporateThemeList(token)
    .then((response: ApiResponse<GetThemeListResponse>) => response.data)
    .catch((error: ApiResponse<GetThemeListResponse>) => error)
}
export function getSettingApi (key: string) {
  return settingApi.create()
    .get(key)
    .then((response: ApiResponse<getSettingResponse>) => response.data)
    .catch((error: ApiResponse<getSettingResponse>) => error)
}

export function fetchPromoteApi (date: string) {
  return api.create()
    .getPromoteCategories(date)
    .then(response => response.data)
    .catch(error => error)
}

export function * fetchCorporateThemes() {
  const token: string = yield select(getToken)
  if (token !== null) {
    yield put(actions.fetchingCorporateThemes())

    try {
      const response: GetThemeListResponse = yield call(fetchCorporateThemesApi, token)
      if (!response.error) {
        yield put(actions.fetchingCorporateThemesSuccess(response.data))
      } else {
        yield put(actions.fetchingCorporateThemesFailed(response.message))
      }
    } catch (error)  {
      yield put(actions.fetchingCorporateThemesFailed('Cannot fetch corporate themes due to network issues'))
    }

  }
}

export function * fetchItems () {
  yield put(actions.fetchingItems())
  const offDates: GetOffDates = yield call(getOffDatesApi)

  if (offDates !== null && !offDates.error) {
    const parsedOffDates: string[] = JSON.parse(offDates.data.replace(/'/g, '"'))
    const filteredOffDates: string[] = parsedOffDates.filter((offDate: string) => {
      return moment(offDate).utcOffset(7).isSameOrAfter(moment().utcOffset(7))
    })
    const newOffDates: Moment[] = filteredOffDates.map((offDate: string) => {
      return moment(offDate).utcOffset(7)
    })
    const cutOffTimeSeconds: getSettingResponse = yield call(getSettingApi, 'delivery_cut_off')
    const startDate: Moment = AvailabilityDateService.getInitialDates(moment().add(1, 'days'), newOffDates, cutOffTimeSeconds.data)
    yield put(MealActions.fetchingOffDatesDone(newOffDates, startDate))

    const response: GetMealsResponse = yield call(fetchItemsApi, startDate)
    if (response !== null && !response.error) {
      yield put(actions.fetchingItemsSuccess(response.data))
    } else {
      const error = response !== null ? response.message : 'Cannot fetch items'
      yield put(actions.fetchingItemsFailed(error))
    }
  }
}

export function * fetchThemes () {
  yield put(actions.fetchingThemes())
  const response: GetThemeListResponse = yield call(fetchThemesApi)
  if (response !== null && !response.error) {
    yield put(actions.fetchingThemesSuccess(response.data))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch themes'
    yield put(actions.fetchingThemesFailed(error))
  }
}

export function getThemeDetailApi (id: number) {
  return ThemeApi.create()
    .getThemeDetail(id)
    .then((response: ApiResponse<GetThemeDetailResponse>) => {
      return response.data
    })
    .catch((error: ApiResponse<GetThemeDetailResponse>) => error)
}

export function getThemeDetailMealsApi (id: number) {
  return ThemeApi.create()
    .getThemeDetailMeals(id)
    .then((response: ApiResponse<GetThemeDetailMeals>) => response.data)
    .catch((error: ApiResponse<GetThemeDetailMeals>) => error)
}

export function * fetchFeaturedThemeDetail () {
  try {
    yield put(actions.fetchingFeaturedThemeDetail())
    const response: GetThemeDetailResponse = yield call(getThemeDetailApi, featuredThemeId)
    if (response !== null && !response.error) {
      const featuredThemeDetail = response.data
      yield put(actions.fetchingFeaturedThemeDetailSuccess(featuredThemeDetail))
    } else {
      const error = response !== null ? response.message : 'Cannot fetch theme detail due to network issues'
      throw new Error(error)
    }
  } catch (error) {}
}

export function * fetchFeaturedThemeItems () {
  try {
    yield put(actions.fetchingFeaturedThemeItems())
    const response: GetThemeDetailMeals = yield call(getThemeDetailMealsApi, featuredThemeId)

    if (response !== null && !response.error) {
      const featuredThemeItems: ThemeItems[] = generateDistinctThemeMeals(response.data)
      yield put(actions.fetchingFeaturedThemeItemsSuccess(featuredThemeItems))
    } else {
      const error: string = response !== null ? response.message : 'Cannot fetch detail items due to network issues'
      yield put(actions.fetchingFeaturedThemeItemsFailed(error))
    }
  } catch (error) {

  }
}

export function * fetchPromote ({date}: ReturnType<typeof actions.fetchPromote>) {
  yield put(actions.fetchingPromote())
  const response: GetPromoteResponse = yield call(fetchPromoteApi, date)
  if (response !== null && !response.error) {
    yield put(actions.fetchingPromoteSuccess(response.data))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch Promote'
    yield put(actions.fetchingPromoteFailed(error))
  }
}
