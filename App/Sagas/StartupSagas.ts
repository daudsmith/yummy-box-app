import { call, put, select } from 'redux-saga/effects'
import CartActions from '../Redux/CartRedux'
import CustomerAccountActions from '../Redux/CustomerAccountRedux'
import CustomerCardActions from '../Redux/CustomerCardRedux'
import CustomerNotificationActions from '../Redux/CustomerNotificationRedux'
import CustomerOrderActions from '../Redux/CustomerOrderRedux'
import CustomerVirtualAccountActions from '../Redux/CustomerVirtualAccountRedux'
import HomeContentActions from '../Redux/HomeContentRedux'
import MyMealPlanActions from '../Redux/MyMealPlanRedux'
import PromotionActions from '../Redux/PromotionRedux'
import SalesOrderActions from '../Redux/SalesOrderRedux'
import StartupActions from '../Redux/StartupRedux'
import { AppState } from '../Redux/CreateStore'

export const getToken = (state: AppState) => state.login

// process STARTUP actions
export function * startup () {
  const {token}: {token: string} = yield select(getToken)
  if (token !== null) {
    yield put(CustomerCardActions.fetchCards())
  }
  yield put(SalesOrderActions.resetOrder())
  yield put(CartActions.validateCart())
  yield put(MyMealPlanActions.resetModifyState())
}

export function * startupOnAppLaunch () {
  yield call(startup)
  yield put(StartupActions.startupFinished())
}

export function * startupOnHomeScreen () {
  yield call(startup)
  yield put(PromotionActions.fetchBanners())
  yield put(HomeContentActions.fetchItems())
  yield put(HomeContentActions.fetchThemes())
  yield put(StartupActions.startupFinished())
}

export function * startupOnMmpScreen () {
  yield call(startup)
  yield put(MyMealPlanActions.getMyMealPlans())
  yield put(StartupActions.startupFinished())
}

export function * startupOnMyOrder () {
  yield call(startup)
  yield put(CustomerOrderActions.fetchOrderList(false))
  yield put(StartupActions.startupFinished())
}

export function * startupOnNotificationList () {
  yield call(startup)
  yield put(CustomerNotificationActions.fetchNotificationList(false))
  yield put(StartupActions.startupFinished())
}

export function * startupOnTopUpScreen () {
  yield call(startup)
  yield put(CustomerAccountActions.fetchWallet())
  yield put(CustomerVirtualAccountActions.fetchVirtualAccount())
  yield put(StartupActions.startupFinished())
}

export function * startupOnCreditCardList () {
  yield call(startup)
  yield put(CustomerCardActions.fetchCards())
  yield put(StartupActions.startupFinished())
}
