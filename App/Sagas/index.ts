import { networkSaga } from 'react-native-offline'
import { all, fork, takeLatest } from 'redux-saga/effects'
import { AddressHistoryTypes } from '../Redux/AddressHistoryRedux'
import { AddressTypeTypes } from '../Redux/AddressTypeRedux'
import { AutoRoutingTypes } from '../Redux/AutoRoutingRedux'
// import { CartTypes } from '../Redux/CartRedux'
import { CreditTokenTopUpTypes } from '../Redux/CreditTokenTopUpRedux'
import { CustomerAccountTypes } from '../Redux/CustomerAccountRedux'
import { CustomerAddressTypes } from '../Redux/CustomerAddressRedux'
import { CustomerCardTypes } from '../Redux/CustomerCardRedux'
import { CustomerNotificationTypes } from '../Redux/CustomerNotificationRedux'
import { CustomerOrderTypes } from '../Redux/CustomerOrderRedux'
import { CustomerVirtualAccountTypes } from '../Redux/CustomerVirtualAccountRedux'
import { ForgotPasswordTypes } from '../Redux/ForgotPasswordRedux'
import { HomeContentTypes } from '../Redux/HomeContentRedux'
import { LoginTypes } from '../Redux/LoginRedux'
import { MealDetailPopUpTypes } from '../Redux/MealDetailPopUpRedux'
import { MealsTypes } from '../Redux/MealsRedux'
import { MyMealPlanTypes } from '../Redux/MyMealPlanRedux'
import { PromotionTypes } from '../Redux/PromotionRedux'
import { RegisterTypes } from '../Redux/RegisterRedux'
import { OrderTypes } from '../Redux/SalesOrderRedux'
import { SettingTypes } from '../Redux/SettingRedux'
/* ------------- Types ------------- */
// import { StartupTypes } from '../Redux/StartupRedux'
import { ThemeTypes } from '../Redux/ThemeRedux'
import { WelcomeScreenTypes } from '../Redux/WelcomeScreenRedux'
import { fetchAddressHistory } from './AddressHistorySaga'
import { fetchCompanyAddresses } from './AddressTypeSaga'
import { checkToken } from './AuthenticationSaga'
import { addRoute } from './AutoRoutingSaga'
// import { addToCart, applyCoupon, cancelUsingMultipleAddress, removeFromCart, updateDeliveryAddress, updateDeliveryTime, validateCart, getNewAddress } from './CartSaga'
import { submitToken } from './CreditTokenTopUpSaga'
import { changeEmail, changePassword, changePhoneNumberValidate, fetchWallet, sendOtpNumber } from './CustomerAccountSaga'
import { deleteAddress, editAddress, fetchAddress, saveAddress } from './CustomerAddressSaga'
import { addCard, fetchCards, removeCard, setDefaultCard } from './CustomerCardSaga'
import { deleteAllNotifications, deleteSingleNotification, fetchingNotificationGetReadById, fetchingNotificationGetUnreadById, fetchNotificationList, fetchTotalUnreadNotifications, fetchUnreadNotifications, markNotificationsToRead } from './CustomerNotificationSaga'
import { cancelCustomerOrder, fetchOrderDetail, fetchOrderList, modifySubscription, stopSubscription, updateSubscriptionDefaultData } from './CustomerOrderSaga'
import { fetchVirtualAccount } from './CustomerVirtualAccountSaga'
import { forgotPassword } from './ForgotPasswordSaga'
import { fetchCorporateThemes, fetchFeaturedThemeDetail, fetchFeaturedThemeItems, fetchItems, fetchThemes } from './HomeContentSaga'
import { getNewUserData, loginAttempt, logout, onLoginSuccess, updateFcmToken, uploadPhoto } from './LoginSagas'
import { fetchMealDetail } from './MealDetailPopUpSaga'
import { fetchFilteredMeals, fetchingMealDetails, fetchMeals } from './MealsSaga'
import { cancelOrder, getMyMealPlanDetail, getMyMealPlans, modifyMmp, modifyMmpPackageDelivery, modifyMmpPackageDeliveryMeal, modifyMmpSubscriptionPaymentInfo, modifyMmpSubscriptionSelectedPayment, updateMyMealPlanWithPayment } from './MyMealPlanSaga'
import { fetchBanners } from './PromotionSaga'
import { validateEmailPhone, validateIdentity, validateRegisteredWithFacebook, registerCustomer } from './RegisterSaga'
import { createOrder } from './SalesOrderSaga'
import { fetchSetting } from './SettingSaga'
import { fetchPromote } from './HomeContentSaga'
/* ------------- Sagas ------------- */
// import { startupOnAppLaunch, startupOnCreditCardList, startupOnHomeScreen, startupOnMmpScreen, startupOnMyOrder, startupOnNotificationList, startupOnTopUpScreen } from './StartupSagas'
import { fetchAvailableDate, fetchAvailableDateExtends, fetchAvailableMeals, fetchDetail, fetchDetailItems, fetchThemePackages, removeFirstDayOfAvailableDates } from './ThemeSaga'
import { fetchSlides } from './WelcomeScreenSaga'

/* ------------- Connect Types To Sagas ------------- */
export default function* root() {
  yield all([
    // some sagas only receive an action
    // takeLatest(StartupTypes.STARTUP_ON_APP_LAUNCH, checkToken(startupOnAppLaunch)),
    // takeLatest(StartupTypes.STARTUP_ON_HOME_SCREEN, checkToken(startupOnHomeScreen)),
    // takeLatest(StartupTypes.STARTUP_ON_MMP_SCREEN, checkToken(startupOnMmpScreen)),
    // takeLatest(StartupTypes.STARTUP_ON_MY_ORDER, checkToken(startupOnMyOrder)),
    // takeLatest(StartupTypes.STARTUP_ON_NOTIFICATION_LIST, checkToken(startupOnNotificationList)),
    // takeLatest(StartupTypes.STARTUP_ON_TOP_UP_SCREEN, checkToken(startupOnTopUpScreen)),
    // takeLatest(StartupTypes.STARTUP_ON_CREDIT_CARD_LIST, checkToken(startupOnCreditCardList)),
    takeLatest(LoginTypes.ON_LOGIN_SUCCESS, onLoginSuccess),
    takeLatest(LoginTypes.UPDATE_FCM_TOKEN, updateFcmToken),
    takeLatest(LoginTypes.LOGIN_ATTEMPT, loginAttempt),

    takeLatest(WelcomeScreenTypes.FETCH_SLIDES, fetchSlides),

    takeLatest(MealsTypes.FETCH_MEALS, fetchMeals),
    takeLatest(MealsTypes.FETCH_MEAL_DETAILS, fetchingMealDetails),
    takeLatest(MealsTypes.FETCH_FILTERED_MEALS, fetchFilteredMeals),


    // takeLatest(CartTypes.VALIDATE_CART, validateCart),
    // takeLatest(CartTypes.ADD_TO_CART, addToCart),
    // takeLatest(CartTypes.REMOVE_FROM_CART, removeFromCart),
    // takeLatest(CartTypes.UPDATE_DELIVERY_TIME, updateDeliveryTime),
    // takeLatest(CartTypes.UPDATE_DELIVERY_ADDRESS, updateDeliveryAddress),
    // takeLatest(CartTypes.CANCEL_USING_MULTIPLE_ADDRESS, cancelUsingMultipleAddress),

    takeLatest(AddressTypeTypes.FETCH_COMPANY_ADDRESSES, fetchCompanyAddresses),

    takeLatest(CustomerCardTypes.FETCH_CARDS, checkToken(fetchCards)),
    takeLatest(CustomerCardTypes.ADD_CARD, checkToken(addCard)),
    takeLatest(CustomerCardTypes.REMOVE_CARD, checkToken(removeCard)),
    takeLatest(CustomerCardTypes.SET_DEFAULT_CARD, checkToken(setDefaultCard)),

    takeLatest(SettingTypes.FETCH_SETTING, fetchSetting),

    takeLatest(OrderTypes.CREATE_ORDER, checkToken(createOrder)),
    takeLatest(RegisterTypes.REGISTER_CUSTOMER, registerCustomer),
    takeLatest(RegisterTypes.VALIDATE_IDENTITY, validateIdentity),
    takeLatest(RegisterTypes.VALIDATE_EMAIL_PHONE, validateEmailPhone),
    takeLatest(RegisterTypes.VALIDATE_REGISTERED_WITH_FACEBOOK, validateRegisteredWithFacebook),

    takeLatest(CustomerVirtualAccountTypes.FETCH_VIRTUAL_ACCOUNT, fetchVirtualAccount),

    // takeLatest(CartTypes.APPLY_COUPON, applyCoupon),
    // takeLatest(CartTypes.GET_NEW_ADDRESS, getNewAddress),

    takeLatest(PromotionTypes.FETCH_BANNERS, fetchBanners),
    takeLatest(HomeContentTypes.FETCH_ITEMS, fetchItems),
    takeLatest(HomeContentTypes.FETCH_PROMOTE, fetchPromote),
    takeLatest(HomeContentTypes.FETCH_THEMES, fetchThemes),
    takeLatest(HomeContentTypes.FETCH_CORPORATE_THEMES, fetchCorporateThemes),
    takeLatest(HomeContentTypes.FETCH_FEATURED_THEME_DETAIL, fetchFeaturedThemeDetail),
    takeLatest(HomeContentTypes.FETCH_FEATURED_THEME_ITEMS, fetchFeaturedThemeItems),
    takeLatest(CustomerAccountTypes.FETCH_WALLET, checkToken(fetchWallet)),
    takeLatest(CustomerAccountTypes.CHANGE_PASSWORD, changePassword),
    takeLatest(CustomerAccountTypes.CHANGE_EMAIL, changeEmail),
    takeLatest(CustomerAccountTypes.CHANGE_PHONE_NUMBER_VALIDATE, changePhoneNumberValidate),
    takeLatest(CustomerAccountTypes.SEND_OTP_NUMBER, sendOtpNumber),

    takeLatest(CreditTokenTopUpTypes.SUBMIT_TOKEN, submitToken),
    takeLatest(LoginTypes.UPLOAD_PHOTO, checkToken(uploadPhoto)),
    takeLatest(LoginTypes.GET_NEW_USER_DATA, checkToken(getNewUserData)),
    takeLatest(LoginTypes.LOGOUT, logout),
    takeLatest(CustomerOrderTypes.FETCH_ORDER_LIST, checkToken(fetchOrderList)),
    takeLatest(CustomerOrderTypes.FETCH_ORDER_DETAIL, checkToken(fetchOrderDetail)),
    takeLatest(CustomerOrderTypes.STOP_SUBSCRIPTION, checkToken(stopSubscription)),
    takeLatest(CustomerOrderTypes.UPDATE_SUBSCRIPTION_DEFAULT_DATA, updateSubscriptionDefaultData),
    takeLatest(CustomerOrderTypes.MODIFY_SUBSCRIPTION, checkToken(modifySubscription)),
    takeLatest(CustomerOrderTypes.CANCEL_CUSTOMER_ORDER, checkToken(cancelCustomerOrder)),

    takeLatest(CustomerNotificationTypes.FETCH_UNREAD_NOTIFICATIONS, checkToken(fetchUnreadNotifications)),
    takeLatest(CustomerNotificationTypes.FETCH_TOTAL_UNREAD_NOTIFICATIONS, checkToken(fetchTotalUnreadNotifications)),
    takeLatest(CustomerNotificationTypes.FETCH_NOTIFICATION_LIST, checkToken(fetchNotificationList)),
    takeLatest(CustomerNotificationTypes.MARK_NOTIFICATIONS_TO_READ, checkToken(markNotificationsToRead)),
    takeLatest(CustomerNotificationTypes.FETCHING_NOTIFICATION_GET_READ_BY_ID,
      checkToken(fetchingNotificationGetReadById)),
    takeLatest(CustomerNotificationTypes.FETCHING_NOTIFICATION_GET_UNREAD_BY_ID,
      checkToken(fetchingNotificationGetUnreadById)),
    takeLatest(CustomerNotificationTypes.DELETE_SINGLE_NOTIFICATION, checkToken(deleteSingleNotification)),
    takeLatest(CustomerNotificationTypes.DELETE_ALL_NOTIFICATIONS, checkToken(deleteAllNotifications)),

    takeLatest(MyMealPlanTypes.GET_MY_MEAL_PLANS, checkToken(getMyMealPlans)),
    takeLatest(MyMealPlanTypes.GET_MY_MEAL_PLAN_DETAIL, checkToken(getMyMealPlanDetail)),
    takeLatest(MyMealPlanTypes.CANCEL_ORDER, checkToken(cancelOrder)),
    takeLatest(MyMealPlanTypes.MODIFY_MMP_PACKAGE_DELIVERY, modifyMmpPackageDelivery),
    takeLatest(MyMealPlanTypes.MODIFY_MMP_PACKAGE_DELIVERY_MEAL, modifyMmpPackageDeliveryMeal),
    takeLatest(MyMealPlanTypes.MODIFY_MMP_SUBSCRIPTION_PAYMENT_INFO, modifyMmpSubscriptionPaymentInfo),
    takeLatest(MyMealPlanTypes.MODIFY_MMP_SUBSCRIPTION_SELECTED_PAYMENT, modifyMmpSubscriptionSelectedPayment),
    takeLatest(MyMealPlanTypes.MODIFY_MMP, checkToken(modifyMmp)),
    takeLatest(MyMealPlanTypes.UPDATE_MY_MEAL_PLAN_WITH_PAYMENT, updateMyMealPlanWithPayment),

    takeLatest(ForgotPasswordTypes.FORGOT_PASSWORD, forgotPassword),

    takeLatest(ThemeTypes.FETCH_DETAIL, fetchDetail),
    takeLatest(ThemeTypes.FETCH_DETAIL_ITEMS, fetchDetailItems),
    takeLatest(ThemeTypes.FETCH_THEME_PACKAGES, fetchThemePackages),
    takeLatest(ThemeTypes.FETCH_AVAILABLE_MEALS, fetchAvailableMeals),
    takeLatest(ThemeTypes.FETCH_AVAILABLE_DATE, fetchAvailableDate),
    takeLatest(ThemeTypes.FETCH_AVAILABLE_DATE_EXTENDS, fetchAvailableDateExtends),
    takeLatest(ThemeTypes.REMOVE_FIRST_DAY_OF_AVAILABLE_DATES, removeFirstDayOfAvailableDates),

    takeLatest(MealDetailPopUpTypes.FETCH_MEAL_DETAIL, fetchMealDetail),

    takeLatest(AutoRoutingTypes.ADD_ROUTE, addRoute),

    takeLatest(AddressHistoryTypes.FETCH_ADDRESS_HISTORY, checkToken(fetchAddressHistory)),
    takeLatest(CustomerAddressTypes.FETCH_ADDRESS, checkToken(fetchAddress)),
    takeLatest(CustomerAddressTypes.SAVE_ADDRESS, checkToken(saveAddress)),
    takeLatest(CustomerAddressTypes.EDIT_ADDRESS, checkToken(editAddress)),
    takeLatest(CustomerAddressTypes.DELETE_ADDRESS, checkToken(deleteAddress)),

    // @ts-ignore
    fork(networkSaga, __DEV__ ?  {} : {
        pingTimeout: 5000,
        pingInterval: 5000
      })
  ])
}
