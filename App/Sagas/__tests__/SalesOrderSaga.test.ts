import {
  getToken,
  createOrderApi,
  createOrder,
} from '../SalesOrderSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/SalesOrderRedux'
import fakeState from './fakeState'
import { createApiResponse } from '../../Services/ApiSalesOrder'

const fakeOrder = {
  type: 'item',
  total: {
    amount: 20000,
    tax: 0,
    deliveryFee: 0,
    discount: 0,
    total: 20000,
  },
  payment: {
    method: 'bank-transfer',
  },
  deliveries: [
    {
      time: null,
      landmark: null,
      name: null,
      phone: null,
      note: null,
      address: null,
      latitude: 1,
      longitude: 1,
      items: [
        {
          id: 1,
          name: 'menu 1',
          sale_price: 20000,
          tax_class: '',
          images: {
            original: '',
            small_thumb: '',
            medium_thumb: '',
          },
          quantity: 1,
          remaining_daily_quantity: 100,
        }
      ],
      totals: {
        subtotal: 20000,
        delivery_fee: 0,
      },
      date: '2019-11-22',
    },
  ],
}

describe('SalesOrderSaga', () => {
  test('getToken should return correct token', () => {
    const token = getToken(fakeState)
    expect(token)
      .toBe(fakeState.login.token)
  })

  test('createOrderApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(200, {
        error: false,
      })
    const response = await createOrderApi(fakeOrder, token)
    expect((response as createApiResponse).error)
      .toBe(false)
  })

  test('createOrderApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(404, {
        error: true,
      })
    const response = await createOrderApi(fakeOrder, token)
    expect((response as createApiResponse).error)
      .toBe(true)
  })

  test('createOrderApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(500, {
        error: true,
      })
    const response = await createOrderApi(fakeOrder, token)
    expect((response as createApiResponse).error)
      .toBe(true)
  })

  test('createOrder item should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: 'orderNumber',
      invoice_url: 'url://jest.test.case',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(200, response)
    await runSaga(fakeStore, createOrder, { order: fakeOrder }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.createSuccess(response.data, response.invoice_url))
  })

  test('createOrder package should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: 'orderNumber',
      invoice_url: 'url://jest.test.case',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(200, response)
    fakeOrder.deliveries[0]['packages'] = {
      dates: [
        '2019-11-22',
        '2019-11-23',
        '2019-11-24',
      ],
      theme_name: 'package 1',
      package_name: 'package 1',
    }

    await runSaga(fakeStore, createOrder, { order: fakeOrder }).toPromise()
    delete fakeOrder.deliveries[0]['packages']
    expect(dispatchedAction)
      .toContainEqual(Actions.createSuccess(response.data, response.invoice_url))
  })

  test('createOrder subscription should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: 'orderNumber',
      invoice_url: 'url://jest.test.case',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(200, response)

    fakeOrder.deliveries[0]['subscription'] = {
      start_date: '2019-11-21',
      repeat_interval: 0,
    }

    await runSaga(fakeStore, createOrder, { order: fakeOrder }).toPromise()
    delete fakeOrder.deliveries[0]['subscription']
    expect(dispatchedAction)
      .toContainEqual(Actions.createSuccess(response.data, response.invoice_url))
  })

  test('createOrder should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(404, response)
    await runSaga(fakeStore, createOrder, { order: fakeOrder }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.createFailed(response.message))
  })

  test('createOrder should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({
        login: {
          token: '12345',
          user: {
            email: 'jest@test.case',
          },
        },
      }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error message',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/order/create')
      .reply(500, response)
    await runSaga(fakeStore, createOrder, { order: fakeOrder }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.createFailed(response.message))
  })

})
