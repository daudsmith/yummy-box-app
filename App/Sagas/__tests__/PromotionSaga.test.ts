import {
    fetchBannersApi,
    fetchBanners,
} from '../PromotionSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/PromotionRedux'

describe('PromotionSaga', () => {
    test('fetchBannersApi should return correct response on success response', async () => {
        nock(AppConfig.apiBaseUrl)
            .get('/public/promotion/banners')
            .reply(200, {
                error: false,
            })
        const response = await fetchBannersApi()
        expect(response.error)
            .toBe(false)
    })
    test('fetchBannersApi should return correct response on notFound response', async () => {
        nock(AppConfig.apiBaseUrl)
            .get('/public/promotion/banners')
            .reply(404, {
                error: false,
            })
        const response = await fetchBannersApi()
        expect(response.error)
            .toBe(false)
    })
    test('fetchBannersApi should return correct response on error response', async () => {
        nock(AppConfig.apiBaseUrl)
            .get('/public/promotion/banners')
            .reply(500, {
                error: false,
            })
        const response = await fetchBannersApi()
        expect(response.error)
            .toBe(false)
    })

    test('fetchBanners should return correct response on success response', async () => {
        let dispatchedAction = []
        const fakeStore = {
            dispatch: action => dispatchedAction.push(action),
        }
        const response = {
          error: false,
          data: [
            {
              id: 1,
              banner_image: 'image.png',
              title: null,
              slug: null,
              content: null,
              action_path: null,
              description: null,
              terms_and_condition: null,
              code_text: null,
            }
          ],
        }
        nock(AppConfig.apiBaseUrl)
          .get('/public/promotion/banners')
          .reply(200, response)
        await runSaga(fakeStore, fetchBanners).toPromise()
        expect(dispatchedAction)
          .toContainEqual(Actions.fetchingBannersSuccess(response.data))
    })
    test('fetchBanners should return correct response on error response', async () => {
        let dispatchedAction = []
        const fakeStore = {
            dispatch: action => dispatchedAction.push(action),
        }
        const response = {
          error: true,
          data: [
            {id: 1, banner_image: 'image.png'}
          ],
          message: 'Cannot fetch banners'
        }
        nock(AppConfig.apiBaseUrl)
          .get('/public/promotion/banners')
          .reply(500, response)
        await runSaga(fakeStore, fetchBanners).toPromise()
        expect(dispatchedAction)
          .toContainEqual(Actions.fetchingBannersFailed(response.message))
    })
})
