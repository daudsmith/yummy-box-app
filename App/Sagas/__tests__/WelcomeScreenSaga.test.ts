import {
  fetchSlidesApi,
  fetchSlides,
} from '../WelcomeScreenSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/WelcomeScreenRedux'

describe('WelcomeScreenSaga', () => {
  test('fetchSlidesApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(200, {
        error: false,
      })
    const response = await fetchSlidesApi()
    expect(response.error)
      .toBe(false)
  })
  test('fetchSlidesApi should return correct response on page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(404, {
        error: true,
      })
    const response = await fetchSlidesApi()
    expect(response.error)
      .toBe(true)
  })
  test('fetchSlidesApi should return correct response on internal system error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(500, {
        error: true,
      })
    const response = await fetchSlidesApi()
    expect(response.error)
      .toBe(true)
  })

  test('fetchSlides should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: [
        {
          id: 1,
          address: 'Jalan maju',
        },
        {
          id: 2,
          address: 'Jalan mundur',
        },
      ],
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(200, response)
    await runSaga(fakeStore, fetchSlides).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchSlidesDone(response.data))
  })
  test('fetchSlides should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: [
        {id: 1, address: 'Jalan maju'},
        {id: 2, address: 'Jalan mundur'}
      ],
      message: 'Error Response'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(500, response)
    await runSaga(fakeStore, fetchSlides).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchSlidesFailed())
  })
  test('fetchSlides should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: [
        {id: 1, address: 'Jalan maju'},
        {id: 2, address: 'Jalan mundur'}
      ],
      message: 'Error Response'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/welcome_screen')
      .reply(404, response)
    await runSaga(fakeStore, fetchSlides).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchSlidesFailed())
  })
})
