import {
  getToken,
  getModifiedOrderDetail,
  updateSubscriptionDefaultData,
  modifySubscription,
  buildModifySubscriptionBody,
  fetchOrderList,
  fetchOrderDetail,
  stopSubscription,
  fetchOrderListApi,
  fetchSubscriptionOrderList,
  fetchOrderDetailApi,
  fetchSubscriptionOrderDetailApi,
  stopSubscriptionApi,
  modifySubscriptionOrderApi,
  buildPaymentInfo,
  cancelCustomerOrder,
  cancelOrderApi,
} from '../CustomerOrderSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/CustomerOrderRedux'
import fakeState from './fakeState'
import {
  OrderDetailResponse,
  salesOrderResponse,
  subscriptionOrderResponse,
  SubscriptPauseResponse,
} from '../../Services/ApiCustomerOrder'
import { cancelApiResponse } from '../../Services/ApiSalesOrder'

const fakeData = {
  key: 'default_delivery_address',
  value: 'lorem ipsum',
}

const fakeSubsData = {
  id: 1,
  data: {
    default_delivery_time: null,
    default_delivery_address: null,
    default_delivery_note: null,
    default_delivery_latitude: null,
    default_delivery_longitude: null,
    default_delivery_details: null,
    default_recipient_name: null,
    default_recipient_phone: null,
    default_payment_method: null,
    default_payment_info: null,
    repeat_interval: null,
    theme_id: null,
  },
}

describe('CustomerOrderSaga', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })

  test('getModifiedOrderDetail should return correct token', () => {
    const response = getModifiedOrderDetail(fakeState)
    expect(response)
      .toBe(fakeState.customerOrder.modifiedOrderDetail)
  })

  test('updateSubscriptionDefaultData should return response on success', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ customerOrder: { modifiedOrderDetail: fakeState.customerOrder.modifiedOrderDetail } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const newModifiedOrderDetail = fakeState.customerOrder.modifiedOrderDetail

    const response = {
      error: false,
      data: {
        ...newModifiedOrderDetail,
        subscription_raw_data: {
          ...newModifiedOrderDetail.subscription_raw_data,
          [fakeData.key]: fakeData.value,
          default_delivery_note: '',
        },
      },
    }
    await runSaga(fakeStore, updateSubscriptionDefaultData, {
      key: fakeData.key,
      value: fakeData.value,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateModifySubscriptionData(response.data))
  })

  test('updateSubscriptionDefaultData should return response on Key value is not equal to default_delivery_address',
    async () => {
      let dispatchedAction = []
      const fakeStore = {
        getState: () => ({ customerOrder: { modifiedOrderDetail: fakeState.customerOrder.modifiedOrderDetail } }),
        dispatch: action => dispatchedAction.push(action),
      }
      const fakeData = {
        key: 'custom_delivery_address',
        value: 'lorem ipsum',
      }

      const newModifiedOrderDetail = fakeState.customerOrder.modifiedOrderDetail

      const response = {
        error: false,
        data: {
          ...newModifiedOrderDetail,
          subscription_raw_data: {
            ...newModifiedOrderDetail.subscription_raw_data,
            [fakeData.key]: fakeData.value,
          },
        },
      }

      await runSaga(fakeStore, updateSubscriptionDefaultData, {
        key: fakeData.key,
        value: fakeData.value,
      } as any).toPromise()
      expect(dispatchedAction)
        .toContainEqual(Actions.updateModifySubscriptionData(response.data))
    })

  test('updateSubscriptionDefaultData should return response on Key type is not string', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ customerOrder: { modifiedOrderDetail: fakeState.customerOrder.modifiedOrderDetail } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const fakeData = {
      key: [
        'default_delivery_address',
        'default_delivery_note',
      ],
      value: [
        'lorem ipsum',
        '',
      ],
    }

    const newModifiedOrderDetail = fakeState.customerOrder.modifiedOrderDetail

    const response = {
      error: false,
      data: {
        ...newModifiedOrderDetail,
        subscription_raw_data: {
          ...newModifiedOrderDetail.subscription_raw_data,
          [fakeData.key[0]]: fakeData.value[0],
          [fakeData.key[1]]: fakeData.value[1],
        },
      },
    }

    await runSaga(fakeStore, updateSubscriptionDefaultData, {
      key: fakeData.key,
      value: fakeData.value,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateModifySubscriptionData(response.data))
  })

  test(
    'updateSubscriptionDefaultData should return response on Key type is not string && key is not equal to default_delivery_address',
    async () => {
      let dispatchedAction = []
      const fakeStore = {
        getState: () => ({ customerOrder: { modifiedOrderDetail: fakeState.customerOrder.modifiedOrderDetail } }),
        dispatch: action => dispatchedAction.push(action),
      }
      const fakeData = {
        key: [
          'default_delivery_address',
          'default_delivery_note',
        ],
        value: [
          'lorem ipsum',
          '',
        ],
      }

      const newModifiedOrderDetail = fakeState.customerOrder.modifiedOrderDetail

      const response = {
        error: false,
        data: {
          ...newModifiedOrderDetail,
          subscription_raw_data: {
            ...newModifiedOrderDetail.subscription_raw_data,
            [fakeData.key[0]]: fakeData.value[0],
            [fakeData.key[1]]: fakeData.value[1],
          },
        },
      }
      await runSaga(fakeStore, updateSubscriptionDefaultData, {
        key: fakeData.key,
        value: fakeData.value,
      } as any).toPromise()
      expect(dispatchedAction)
        .toContainEqual(Actions.updateModifySubscriptionData(response.data))
    })

  test('modifySubscription should return response on Success Payment Info, but response is null or error', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({
        customerOrder: {
          modifiedOrderDetail: {
            ...fakeState.customerOrder.modifiedOrderDetail,
            subscription_raw_data: {
              id: 1,
              number: 'one',
              default_delivery_note: '',
              default_delivery_address: '',
              default_delivery_time: new Date(),
              default_payment_info: {
                type: 'bca',
              },
            },
            subscription: {
              id: 1,
            },
            orderType: 'subscription',
          },
        },
        login: {
          token: '12345',
          id: '1',
        },
      }),
      dispatch: action => dispatchedAction.push(action),
    }

    const response = {
      error: 'Cannot modify subscription due to network issues',
      data: [],
    }

    nock(AppConfig.apiBaseUrl)
      .post('/public/sales/subscription/1/update')
      .reply(200, response)

    await runSaga(fakeStore, modifySubscription).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifySubscriptionFailed('Cannot modify subscription due to network issues'))
  })

  test('modifySubscription should return response on Error Payment Info', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({
        customerOrder: {
          modifiedOrderDetail: {
            ...fakeState.customerOrder.modifiedOrderDetail,
            subscription_raw_data: {
              id: 1,
              number: 'one',
              default_delivery_note: '',
              default_delivery_address: '',
              default_delivery_time: new Date(),
              default_payment_info: 'ini error',
            },
          },
        },
        login: { token: '12345' },

      }),
      dispatch: action => dispatchedAction.push(action),
    }
    await runSaga(fakeStore, modifySubscription).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifySubscriptionFailed(
        'There is something wrong with your payment information, please contact our customer service for more information'))
  })

  test('fetchOrderList should return response on Success', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const responseOrder = {
      error: false,
      data: {
        in_progress: [],
        complete: [],
      },
    }

    const responseSubscription = {
      error: false,
      data: {
        in_progress: [],
        complete: [],
      },
    }

    const fakeData = {
      order: responseOrder.data,
      subscription: responseSubscription.data,
    }

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, responseOrder)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, responseSubscription)

    await runSaga(fakeStore, fetchOrderList, { silent: true }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingOrderListDone(fakeData))
  })

  test('fetchOrderList should return response on Error fetch order list', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const responseOrder = {
      error: true,
      data: null,
      message: 'Cannot fetch order list',
    }

    const responseSubscription = {
      error: false,
      data: [],
    }

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, responseOrder)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, responseSubscription)

    await runSaga(fakeStore, fetchOrderList, { silent: true }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingOrderListFailed(`${responseOrder.message} & `))
  })

  test('fetchOrderList should return response on Error fetch subscription order list', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    const responseOrder = {
      error: false,
      data: [],
    }

    const responseSubscription = {
      error: true,
      data: null,
      message: 'Cannot fetch subscription order list',
    }

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, responseOrder)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, responseSubscription)

    await runSaga(fakeStore, fetchOrderList, { silent: true }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingOrderListFailed(responseSubscription.message))
  })

  test('fetchOrderList should return response on Error fetch order list and subscription order list', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    const responseOrder = {
      error: true,
      data: null,
      message: 'Cannot fetch order list',
    }

    const responseSubscription = {
      error: true,
      data: null,
      message: 'Cannot fetch subscription order list',
    }

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, responseOrder)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, responseSubscription)

    await runSaga(fakeStore, fetchOrderList, { silent: true }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingOrderListFailed(`${responseOrder.message} & ${responseSubscription.message}`))
  })

  test('fetchOrderDetail should return response on Success', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const response = {
      error: false,
      data: fakeState.customerOrder.modifiedOrderDetail,
    }

    const fakeData = {
      id: 1,
      orderType: 'subscription',
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/detail`)
      .reply(200, response)

    await runSaga(fakeStore, fetchOrderDetail, {
      id: fakeData.id,
      orderType: fakeData.orderType,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingOrderDetailDone(response.data))
  })

  test('fetchOrderDetail should return response on Success with orderType is not subscription', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const response = {
      error: false,
      data: fakeState.customerOrder.modifiedOrderDetail,
    }

    const fakeData = {
      id: 1,
      orderType: 'non-subscription',
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeData.id}?type=${fakeData.orderType}`)
      .reply(200, response)

    await runSaga(fakeStore, fetchOrderDetail, {
      id: fakeData.id,
      orderType: fakeData.orderType,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingOrderDetailDone(response.data))
  })

  test('fetchOrderDetail should return response on Error', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const response = {
      error: true,
      data: null,
      message: 'error message',
    }

    const fakeData = {
      id: 1,
      orderType: null,
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeData.id}?type=${fakeData.orderType}`)
      .reply(200, response)

    await runSaga(fakeStore, fetchOrderDetail, {
      id: fakeData.id,
      orderType: fakeData.orderType,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingOrderDetailFailed(response.message))
  })

  test('stopSubscription should return response on Success', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const response = {
      error: false,
      data: [],
    }

    const responseOrder = {
      error: false,
      data: {
        in_progress: [
          {
            id: 1,
            type: 'non-subscription',
          },
        ],
        complete: [
          {
            id: 2,
            type: 'non-subscription',
          },
        ],
      },
    }

    const responseSubscription = {
      error: false,
      data: [],
    }

    const fakeData = {
      id: 1,
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/pause`)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, responseOrder)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, responseSubscription)

    await runSaga(fakeStore, stopSubscription, { id: fakeData.id } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.stopSubscriptionSuccess())
  })

  test('stopSubscription should return response on Error', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }

    const response = {
      error: true,
      data: [],
      message: 'Cannot stop subscription due to network issues',
    }

    const responseOrder = {
      error: false,
      data: {
        in_progress: [
          {
            id: 1,
            type: 'non-subscription',
          },
        ],
        complete: [
          {
            id: 2,
            type: 'non-subscription',
          },
        ],
      },
    }

    const responseSubscription = {
      error: false,
      data: [],
    }

    const fakeData = {
      id: 1,
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/pause`)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, responseOrder)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, responseSubscription)

    await runSaga(fakeStore, stopSubscription, { id: fakeData.id } as  any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.stopSubscriptionFailed(response.message))
  })

  test('fetchOrderListApi should return response on Success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(200, {
        error: false,
      })
    const response = await fetchOrderListApi(token)
    expect((response as salesOrderResponse).error)
      .toBe(false)
  })
  test('fetchOrderListApi should return response on Not Found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(404, {
        error: true,
      })
    const response = await fetchOrderListApi(token)
    expect((response as salesOrderResponse).error)
      .toBe(false)
  })
  test('fetchOrderListApi should return response on Error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order')
      .reply(500, {
        error: true,
      })
    const response = await fetchOrderListApi(token)
    expect((response as salesOrderResponse).error)
      .toBe(true)
  })

  test('fetchSubscriptionOrderList should return response on Success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(200, {
        error: false,
      })
    const response = await fetchSubscriptionOrderList(token)
    expect((response as subscriptionOrderResponse).error)
      .toBe(false)
  })
  test('fetchSubscriptionOrderList should return response on Not Found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(404, {
        error: true,
      })
    const response = await fetchSubscriptionOrderList(token)
    expect((response as subscriptionOrderResponse).error)
      .toBe(false)
  })
  test('fetchSubscriptionOrderList should return response on Error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/subscription')
      .reply(500, {
        error: true,
      })
    const response = await fetchSubscriptionOrderList(token)
    expect((response as subscriptionOrderResponse).error)
      .toBe(true)
  })

  test('fetchOrderDetailApi should return response on Success', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
      orderType: 'type',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeData.id}?type=${fakeData.orderType}`)
      .reply(200, {
        error: false,
      })
    const response = await fetchOrderDetailApi({
      token: token,
      id: fakeData.id,
      orderType: fakeData.orderType,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(false)
  })
  test('fetchOrderDetailApi should return response on Not Found', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
      orderType: 'type',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeData.id}?type=${fakeData.orderType}`)
      .reply(404, {
        error: true,
      })
    const response = await fetchOrderDetailApi({
      token: token,
      id: fakeData.id,
      orderType: fakeData.orderType,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(true)
  })
  test('fetchOrderDetailApi should return response on Error', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
      orderType: 'type',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/detail/${fakeData.id}?type=${fakeData.orderType}`)
      .reply(500, {
        error: true,
      })
    const response = await fetchOrderDetailApi({
      token: token,
      id: fakeData.id,
      orderType: fakeData.orderType,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(true)
  })

  test('fetchSubscriptionOrderDetailApi should return response on Success', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/detail`)
      .reply(200, {
        error: false,
      })
    const response = await fetchSubscriptionOrderDetailApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(false)
  })
  test('fetchSubscriptionOrderDetailApi should return response on Not Found', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/detail`)
      .reply(404, {
        error: true,
      })
    const response = await fetchSubscriptionOrderDetailApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(true)
  })
  test('fetchSubscriptionOrderDetailApi should return response on Error', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/detail`)
      .reply(500, {
        error: true,
      })
    const response = await fetchSubscriptionOrderDetailApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(true)
  })

  test('stopSubscriptionApi should return response on Success', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/pause`)
      .reply(200, {
        error: false,
      })
    const response = await stopSubscriptionApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as SubscriptPauseResponse).error)
      .toBe(false)
  })
  test('stopSubscriptionApi should return response on Not Found', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/pause`)
      .reply(404, {
        error: true,
      })
    const response = await stopSubscriptionApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as SubscriptPauseResponse).error)
      .toBe(true)
  })
  test('stopSubscriptionApi should return response on Error', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/pause`)
      .reply(500, {
        error: true,
      })
    const response = await stopSubscriptionApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as SubscriptPauseResponse).error)
      .toBe(true)
  })

  test('modifySubscriptionOrderApi should return response on Success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/${fakeSubsData.id}/update`)
      .reply(200, {
        error: false,
      })
    const response = await modifySubscriptionOrderApi({
      token: token,
      id: fakeSubsData.id,
      data: fakeSubsData.data,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(false)
  })
  test('modifySubscriptionOrderApi should return response on Not Found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/${fakeSubsData.id}/update`)
      .reply(404, {
        error: true,
      })
    const response = await modifySubscriptionOrderApi({
      token: token,
      id: fakeSubsData.id,
      data: fakeSubsData.data,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(true)
  })
  test('modifySubscriptionOrderApi should return response on Error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/${fakeSubsData.id}/update`)
      .reply(500, {
        error: true,
      })
    const response = await modifySubscriptionOrderApi({
      token: token,
      id: fakeSubsData.id,
      data: fakeSubsData.data,
    })
    expect((response as OrderDetailResponse).error)
      .toBe(true)
  })

  test('buildModifySubscriptionBody should return response on Success', () => {
    const fakeData = {
      id: 1,
      created_at: null,
      updated_at: null,
      account_id: 1,
      number: null,
      theme_id: 1,
      status: null,
      default_recipient_name: null,
      default_recipient_phone: null,
      default_delivery_landmark: null,
      default_delivery_address: null,
      default_delivery_latitude: 1,
      default_delivery_longitude: 1,
      default_delivery_details: null,
      default_delivery_note: null,
      default_payment_method: null,
      default_payment_info: '',
      default_delivery_time: null,
      start_date: null,
      repeat_every: null,
      repeat_interval: null,
      previous_delivery_date: null,
      next_delivery_date: null,
      newCreditCardInfo: null,
    }
    const response = buildModifySubscriptionBody(fakeData)
    let expectation = {
      default_recipient_name: null,
      default_recipient_phone: null,
      default_delivery_address: null,
      default_delivery_latitude: 1,
      default_delivery_longitude: 1,
      default_delivery_details: null,
      default_delivery_note: null,
      default_payment_method: null,
      default_payment_info: '',
      default_delivery_time: null,
      theme_id: 1,
      repeat_interval: null,
    }
    expect(response)
      .toStrictEqual(expectation)
  })

  test('buildPaymentInfo should return response on Success', () => {
    const fakeData = {
      type: 'default_payment_info',
    }
    const response = buildPaymentInfo(fakeData)
    expect(response)
      .toBe(JSON.stringify(fakeData))
  })

  test('buildPaymentInfo should return response on Error', () => {
    const fakeData = 'default_payment_info'
    const response = buildPaymentInfo(fakeData)
    expect(response)
      .toBe('error')
  })

  test('cancelCustomerOrder should return response on Success', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    const fakeData = {
      id: 1,
    }

    const response = {
      error: false,
      data: 1,
      message: ''
    }

    nock(`${AppConfig.apiBaseUrl}/public/sales/order`)
      .get(`/cancel/${fakeData.id}`)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/pause`)
      .reply(200, response)

    await runSaga(fakeStore, cancelCustomerOrder, { id: fakeData.id }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.cancellingOrderDone(response.data, ''))
  })
  test('cancelCustomerOrder should return response on Failed', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    const fakeData = {
      id: 1,
    }

    const response = {
      error: true,
      data: 1,
      message: 'Error'
    }

    nock(`${AppConfig.apiBaseUrl}/public/sales/order`)
      .get(`/cancel/${fakeData.id}`)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/pause`)
      .reply(200, response)

    await runSaga(fakeStore, cancelCustomerOrder, { id: fakeData.id }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.cancellingOrderFailed(response.message))
  })

  test('cancelOrderApi should return response on Success', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(`${AppConfig.apiBaseUrl}/public/sales/order`)
      .get(`/cancel/${fakeData.id}`)
      .reply(200, {
        error: false,
      })
    const response = await cancelOrderApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as cancelApiResponse).error)
      .toBe(false)
  })
  test('cancelOrderApi should return response on Not Found', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(`${AppConfig.apiBaseUrl}/public/sales/order`)
      .get(`/cancel/${fakeData.id}`)
      .reply(404, {
        error: true,
      })
    const response = await cancelOrderApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as cancelApiResponse).error)
      .toBe(true)
  })
  test('cancelOrderApi should return response on Error', async () => {
    const token = getToken(fakeState)
    const fakeData = {
      id: 1,
    }
    nock(`${AppConfig.apiBaseUrl}/public/sales/order`)
      .get(`/cancel/${fakeData.id}`)
      .reply(500, {
        error: true,
      })
    const response = await cancelOrderApi({
      token: token,
      id: fakeData.id,
    })
    expect((response as cancelApiResponse).error)
      .toBe(true)
  })

  test('modifySubscription should return response on Success Payment Info', async () => {
    let dispatchedAction = []
    const newDate = new Date()
    const fakeStore = {
      getState: () => ({
        customerOrder: {
          modifiedOrderDetail: {
            ...fakeState.customerOrder.modifiedOrderDetail,
            subscription_raw_data: {
              id: 1,
              number: 'one',
              default_delivery_note: '',
              default_delivery_address: '',
              default_delivery_time: newDate.toString(),
              default_payment_info: {
                type: 'bca',
              },
              default_delivery_latitude: '',
              default_delivery_longitude: '',
              default_delivery_details: '',
              default_recipient_name: '',
              default_recipient_phone: '',
              default_payment_method: '',
              repeat_interval: '',
              theme_id: '1',
            },
            subscription: {
              id: 1,
            },
            orderType: 'subscription',
          },
        },
        login: {
          token: '12345',
        },
      }),
      dispatch: action => dispatchedAction.push(action),
    }

    const fakeData = {
      id: 1,
      detail: 'penjelasan',
    }

    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/subscription/${fakeData.id}/update`)
      .reply(200, {
        error: false,
      })

    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/${fakeData.id}/detail`)
      .reply(200, {
        error: false,
      })

    await runSaga(fakeStore, modifySubscription).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifySubscriptionSuccess())
  })

})
