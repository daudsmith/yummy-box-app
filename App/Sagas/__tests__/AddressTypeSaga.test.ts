import {
  getToken,
  fetchCompanyAddressesApi,
  fetchCompanyAddresses,
} from '../AddressTypeSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/AddressTypeRedux'
import fakeState from './fakeState'

describe('fetchCompanyAddresses', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login)
  })

  test('fetchCompanyAddressesApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/address/list')
      .reply(200, {
        error: false,
      })
    const response = await fetchCompanyAddressesApi(token)
    expect(response.error)
      .toBe(false)
  })

  test('fetchCompanyAddressesApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/address/list')
      .reply(404, {
        error: true,
      })
    const response = await fetchCompanyAddressesApi(token)
    expect(response.error)
      .toBe(true)
  })

  test('fetchCompanyAddressesApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/address/list')
      .reply(500, {
        error: true,
      })
    const response = await fetchCompanyAddressesApi(token)
    expect(response.error)
      .toBe(true)
  })

  test('fetchCompanyAddresses should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.addressType.companyAddresses,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/address/list')
      .reply(200, response)
    await runSaga(fakeStore, fetchCompanyAddresses).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateCompanyAddresses(response.data))
  })

  test('fetchCompanyAddresses should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.addressType.companyAddresses,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/address/list')
      .reply(404, response)
    await runSaga(fakeStore, fetchCompanyAddresses).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.error))
  })

  test('fetchCompanyAddresses should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.addressType.companyAddresses,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/address/list')
      .reply(500, response)
    await runSaga(fakeStore, fetchCompanyAddresses).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.error))
  })
})
