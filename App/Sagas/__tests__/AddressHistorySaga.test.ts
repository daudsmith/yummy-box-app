import {
  getToken,
  getAddressHistoryApi,
  fetchAddressHistory,
} from '../AddressHistorySaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/AddressHistoryRedux'
import fakeState from './fakeState'

describe('AddressHistorySaga', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })

  test('getAddressHistoryApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address/history')
      .reply(200, {
        error: false,
      })
    const response = await getAddressHistoryApi({token})
    expect(response.error)
      .toBe(false)
  })
  test('getAddressHistoryApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address/history')
      .reply(404, {
        error: true,
      })
    const response = await getAddressHistoryApi({token})
    expect(response.error)
      .toBe(true)
  })
  test('getAddressHistoryApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address/history')
      .reply(500, {
        error: true,
      })
    const response = await getAddressHistoryApi({token})
    expect(response.error)
      .toBe(true)
  })

  test('fetchAddressHistory should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: [
        {
          address: 'Jalan mundur',
          latitude: 1,
          longitude: 1,
          address_details: '',
          note: '',
          name: '',
          used: 1,
          landmark: '',
          id:1,
          label: '',
        },
      ]
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address/history')
      .reply(200, response)
    await runSaga(fakeStore, fetchAddressHistory).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchAddressHistorySuccess(response.data))
  })
  test('fetchAddressHistory should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: [
        {id: 1, address: 'Jalan maju'},
        {id: 2, address: 'Jalan mundur'}
      ],
      message: 'Error Response'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address/history')
      .reply(500, response)
    await runSaga(fakeStore, fetchAddressHistory).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchAddressHistoryFailed(response.message))
  })
  test('fetchAddressHistory should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: [
        {id: 1, address: 'Jalan maju'},
        {id: 2, address: 'Jalan mundur'}
      ],
      message: 'Page Not Found'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address/history')
      .reply(404, response)
    await runSaga(fakeStore, fetchAddressHistory).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchAddressHistoryFailed(response.message))
  })
})
