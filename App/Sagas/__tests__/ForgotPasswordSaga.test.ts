import {
  forgotPasswordApi,
  forgotPassword,
} from '../ForgotPasswordSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/ForgotPasswordRedux'
import { updateAccountInfoFieldApiResponse } from '../../Services/ApiCustomer'

const email = 'jest.test@saga.test'

describe('forgotPassword', () => {
  test('forgotPasswordApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/forgot_password')
      .reply(200, {
        error: false,
      })
    const response = await forgotPasswordApi(email)
    expect((response as updateAccountInfoFieldApiResponse).error)
      .toBe(false)
  })

  test('forgotPasswordApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/forgot_password')
      .reply(404, {
        error: true,
      })
    const response = await forgotPasswordApi(email)
    expect((response as updateAccountInfoFieldApiResponse).error)
      .toBe(true)
  })

  test('forgotPasswordApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/forgot_password')
      .reply(500, {
        error: true,
      })
    const response = await forgotPasswordApi(email)
    expect((response as updateAccountInfoFieldApiResponse).error)
      .toBe(true)
  })

  test('forgotPassword should execute redux action on success response', async () => {
      let dispatchedAction = []
      const fakeStore = {
        dispatch: action => dispatchedAction.push(action),
      }
      const response = {
          error: false
      }
      nock(AppConfig.apiBaseUrl)
        .post('/public/forgot_password')
        .reply(200, response)
      await runSaga(fakeStore, forgotPassword, { email: email } as any).toPromise()
      expect(dispatchedAction)
        .toContainEqual(Actions.submittingDone())
  })

  test('forgotPassword should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
        error: true,
        message : 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/forgot_password')
      .reply(404, response)
    await runSaga(fakeStore, forgotPassword, { email: email } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.submittingFailed(response.message))
  })

  test('forgotPassword should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
        error: true,
        message : 'Message error'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/forgot_password')
      .reply(500, response)
    await runSaga(fakeStore, forgotPassword, { email: email } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.submittingFailed(response.message))
  })
})
