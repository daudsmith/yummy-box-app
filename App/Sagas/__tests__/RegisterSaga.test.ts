import {
  customerRegistrationApi,
  validateIdentityApi,
  validateCustomerApi,
  validateIdentity,
  validateEmailPhone,
  validateRegisteredWithFacebook,
} from '../RegisterSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/RegisterRedux'
import fakeState from './fakeState'
import {
  registerApiResponse,
  validateApiResponse,
  validateIdentityApiResponse,
} from '../../Services/ApiCustomer'

const fakeCredential = {
  identity: '123456789',
  email: 'fake@yummycorp.com',
  phone: '0876551826279',
}

const fakeCustomer = {
  identity: '123456789',
  email: 'fake@yummycorp.com',
  phone: '0876551826279',
  countryCode: null,
  first_name: null,
  last_name: null,
  name: null,
  password: '',
  password_confirmation: '',
  salutation: 'Mr',
  social_login_id: '',
  social_login_type: null,
  facebook_credentials: null,
  registration_type: null,
}

describe('RegisterSaga', () => {
  test('customerRegistrationApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/register')
      .reply(200, { error: false })
    const response = await customerRegistrationApi(fakeCustomer)
    expect((response as registerApiResponse).error)
      .toBe(false)
  })
  test('customerRegistrationApi should return correct response on page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/register')
      .reply(404, { error: true })
    const response = await customerRegistrationApi(fakeCustomer)
    expect((response as registerApiResponse).error)
      .toBe(true)
  })
  test('customerRegistrationApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/register')
      .reply(500, { error: true })
    const response = await customerRegistrationApi(fakeCustomer)
    expect((response as registerApiResponse).error)
      .toBe(true)
  })

  test('validateIdentityApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/validate')
      .query({ ...fakeCredential })
      .reply(200, { error: false })
    const response = await validateIdentityApi({ ...fakeCredential })
    expect((response as validateIdentityApiResponse).error)
      .toBe(false)
  })
  test('validateIdentityApi should return correct response on page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/validate')
      .query({
        identity: fakeCredential.identity,
        email: fakeCredential.email,
        phone: fakeCredential.phone,
      })
      .reply(404, { error: true })
    const response = await validateIdentityApi({ ...fakeCredential })
    expect((response as validateIdentityApiResponse).error)
      .toBe(true)
  })
  test('validateIdentityApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/validate')
      .query({ ...fakeCredential })
      .reply(500, { error: true })
    const response = await validateIdentityApi({ ...fakeCredential })
    expect((response as validateIdentityApiResponse).error)
      .toBe(true)
  })

  test('validateCustomerApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/check')
      .reply(200, { error: false })
    const response = await validateCustomerApi({ customer: fakeCustomer })
    expect((response as validateApiResponse).error)
      .toBe(false)
  })
  test('validateCustomerApi should return correct response on page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/check')
      .reply(404, { error: true })
    const response = await validateCustomerApi({ customer: fakeCustomer })
    expect((response as validateApiResponse).error)
      .toBe(true)
  })
  test('validateCustomerApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/check')
      .reply(500, { error: true })
    const response = await validateCustomerApi({ customer: fakeCustomer })
    expect((response as validateApiResponse).error)
      .toBe(true)
  })

  test('validateIdentity should execute redux identity exists action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: { ...fakeCustomer },
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/validate')
      .query({
        identity: fakeCredential.identity,
      })
      .reply(200, response)
    await runSaga(fakeStore, validateIdentity, { identity: fakeCredential.identity } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.identityExist())
  })
  test('validateIdentity should execute redux identity not exists action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/validate')
      .query({
        identity: fakeCredential.identity,
      })
      .reply(200, response)
    await runSaga(fakeStore, validateIdentity, { identity: fakeCredential.identity } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.identityNotExist())
  })

  test('validateEmailPhone should execute redux email, phone exists action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: { ...fakeCustomer },
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/validate')
      .query({
        email: fakeCredential.email,
        phone: fakeCredential.phone,
      })
      .reply(200, response)
    await runSaga(fakeStore, validateEmailPhone, {
      email: fakeCredential.email,
      phone: fakeCredential.phone,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.emailPhoneExist())
  })
  test('validateEmailPhone should execute redux email, phone not exists action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/validate')
      .query({
        email: fakeCredential.email,
        phone: fakeCredential.phone,
      })
      .reply(200, response)
    await runSaga(fakeStore, validateEmailPhone, {
      email: fakeCredential.email,
      phone: fakeCredential.phone,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.emailPhoneNotExist())
  })

  test('validateRegisteredWithFacebook should execute redux email, phone exists action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      exist: true,
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/check')
      .reply(200, response)
    await runSaga(fakeStore, validateRegisteredWithFacebook, {customer: fakeCustomer} as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.customerIsRegisteredWithFacebook())
  })
  test('validateRegisteredWithFacebook should execute redux email, phone not exists action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/check')
      .reply(200, response)
    await runSaga(fakeStore, validateRegisteredWithFacebook, {customer: fakeCustomer} as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.customerNotRegisteredWithFacebook())
  })
})
