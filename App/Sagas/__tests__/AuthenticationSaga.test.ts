import {
  customerProfileApi,
  checkToken,
} from '../AuthenticationSaga'
import { fetchAddressHistory } from '../AddressHistorySaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/AddressHistoryRedux'
import LoginActions from '../../Redux/LoginRedux'
import fakeState from './fakeState'

describe('AuthenticationSaga', () => {
  test('customerProfileApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(200)
    const response = await customerProfileApi(fakeState.login.token)
    expect(response)
      .toBe(200)
  })
  test('customerProfileApi should return correct response on page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(404)
    const response = await customerProfileApi(fakeState.login.token)
    expect(response)
      .toBe(404)
  })
  test('customerProfileApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(500)
    const response = await customerProfileApi(fakeState.login.token)
    expect(response)
      .toBe(500)
  })

  test('checkToken should execute fetchAddressHistory saga on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(200)

    const response = {
      error: false,
      data: fakeState.addressHistory.addresses
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address/history')
      .reply(200, response)
    await runSaga(fakeStore, checkToken(fetchAddressHistory)).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchAddressHistorySuccess(response.data))
  })
  test('checkToken should execute logout on 401 response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(401)
    await runSaga(fakeStore, checkToken(fetchAddressHistory)).toPromise()
    expect(dispatchedAction)
      .toContainEqual(LoginActions.logout())
  })
  test('checkToken should execute logout on 403 response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(403)
    await runSaga(fakeStore, checkToken(fetchAddressHistory)).toPromise()
    expect(dispatchedAction)
      .toContainEqual(LoginActions.logout())
  })
  test('checkToken should execute logout on 404 response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(404)
    await runSaga(fakeStore, checkToken(fetchAddressHistory)).toPromise()
    expect(dispatchedAction)
      .toContainEqual(LoginActions.logout())
  })
  test('checkToken should execute logout on 500 response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(500)
    await runSaga(fakeStore, checkToken(fetchAddressHistory)).toPromise()
    expect(dispatchedAction)
      .toContainEqual(LoginActions.logout())
  })
})
