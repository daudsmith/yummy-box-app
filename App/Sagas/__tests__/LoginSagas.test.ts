
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { getToken, updateDeviceInfoApi, authApi, uploadPhotoApi, getNewUserDataApi, logoutApi, getFcmToken, getDeviceUniqueId, logout, onLoginSuccess, updateFcmToken, uploadPhoto } from '../LoginSagas'


import { runSaga } from 'redux-saga'
import LoginActions from '../../Redux/LoginRedux'
import AddressHistoryActions from '../../Redux/AddressHistoryRedux'
import CreditTokenActions from '../../Redux/CreditTokenTopUpRedux'
import CustomerAddressActions from '../../Redux/CustomerAddressRedux'
import CustomerAccountActions from '../../Redux/CustomerAccountRedux'
import CustomerCardActions from '../../Redux/CustomerCardRedux'
import CustomerNotificationActions from '../../Redux/CustomerNotificationRedux'
import CustomerVirtualAccountActions from '../../Redux/CustomerVirtualAccountRedux'
import CustomerOrderActions from '../../Redux/CustomerOrderRedux'
import MyMealPlanActions from '../../Redux/MyMealPlanRedux'
import ForgotPasswordActions from '../../Redux/ForgotPasswordRedux'
import RegistrationActions from '../../Redux/RegisterRedux'
import CartActions from '../../Redux/CartRedux'
import OrderActions from '../../Redux/SalesOrderRedux'
import ThemeActions from '../../Redux/ThemeRedux'
import MealActions from '../../Redux/MealsRedux'
import HomeContentActions from '../../Redux/HomeContentRedux'
import PromotionActions from '../../Redux/PromotionRedux'
import fakeState from './fakeState'

const fakeRes = {
  data: []
}
describe('CustomerAccountSaga updateDeviceInfoApi and getToken', () => {

  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login)
  })

  const fakeRequestBody = {
    uid: 'uid',
    fcm_token: 'fcm',
  }
  test('updateDeviceInfoApi should return correct response on success', async () => {
    const token = getToken(fakeState).token

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/device/update', fakeRequestBody)
      .reply(200, fakeRes)
    const response = await updateDeviceInfoApi({ token, ...fakeRequestBody })
    expect(response)
      .toEqual(fakeRes)
  })

  test('updateDeviceInfoApi should return correct response on error', async () => {
    const token = getToken(fakeState).token

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/device/update', fakeRequestBody)
      .reply(500, fakeRes)
    const response = await updateDeviceInfoApi({ token, ...fakeRequestBody })
    expect(response)
      .toEqual(fakeRes)
  })

  test('updateDeviceInfoApi should return correct response on not found', async () => {
    const token = getToken(fakeState).token

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/device/update', fakeRequestBody)
      .reply(404, fakeRes)
    const response = await updateDeviceInfoApi({ token, ...fakeRequestBody })
    expect(response)
      .toEqual(fakeRes)
  })

})

describe('CustomerAccountSaga authApi', () => {
  const fakeParams = {
    identity: 'email',
    password: 'password',
  }

  const fakeBody = {
    email: 'email',
    password: 'password'
  }
  test('authApi should return correct response on success', async () => {

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/login', fakeBody)
      .reply(200, fakeRes)
    const response = await authApi(fakeParams)
    expect(response.data)
      .toEqual(fakeRes.data)
  })

  test('authApi should return correct response on error', async () => {

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/login', fakeBody)
      .reply(500, fakeRes)
    const response = await authApi(fakeParams)
    expect(response.data)
      .toEqual(fakeRes.data)
  })

  test('authApi should return correct response on not found', async () => {

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/login', fakeBody)
      .reply(404, fakeRes)
    const response = await authApi(fakeParams)
    expect(response.data)
      .toEqual(fakeRes.data)
  })

  test('updateFcmToken should execute redux action on success response', async () => {
    const fakeSagaParam: any = {
      fcmToken: '123'
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/device/update')
      .reply(200, {
        error: false,
        data: true
      })

    await runSaga(fakeStore, updateFcmToken, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(LoginActions.setFcmToken(fakeSagaParam.fcmToken))

  })

})

describe('CustomerAccountSaga uploadPhotoApi', () => {

  const fakeBody = {
    someData: 123
  }
  test('uploadPhotoApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/upload', fakeBody)
      .reply(200, fakeRes)
    const response = await uploadPhotoApi({ token: '123', data: { someData: 123 } })
    expect(response)
      .toEqual(fakeRes)
  })

  test('uploadPhotoApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/upload', fakeBody)
      .reply(500, fakeRes)
    const response = await uploadPhotoApi({ token: '123', data: { someData: 123 } })
    expect(response)
      .toEqual(fakeRes)
  })

  test('uploadPhotoApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/upload', fakeBody)
      .reply(404, fakeRes)
    const response = await uploadPhotoApi({ token: '123', data: { someData: 123 } })
    expect(response)
      .toEqual(fakeRes)
  })


  test('uploadPhoto should execute redux action on success response', async () => {
    const fakeSagaParam: any = {
      data: {
        fake: 123
      }
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/upload')
      .reply(200, {
        error: false,
        data: fakeState.login.user
      })

    await runSaga(fakeStore, uploadPhoto, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(LoginActions.uploadingPhoto())

    expect(dispatchedAction)
      .toContainEqual(LoginActions.uploadingPhotoDone(fakeState.login.user))

  })

  test('uploadPhoto should execute redux action on error response', async () => {
    const fakeSagaParam: any = {
      data: {
        fake: 123
      }
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/upload')
      .reply(200, {
        error: true,
        message: 'error'
      })

    await runSaga(fakeStore, uploadPhoto, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(LoginActions.uploadingPhoto())

    expect(dispatchedAction)
      .toContainEqual(LoginActions.uploadingPhotoFailed('error'))

  })
})

describe('CustomerAccountSaga getNewUserDataApi', () => {

  test('getNewUserDataApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(200, fakeRes)
    const response = await getNewUserDataApi({...fakeState.login})
    expect(response)
      .toEqual(fakeRes)
  })

  test('getNewUserDataApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(500, fakeRes)
    const response = await getNewUserDataApi({...fakeState.login})
    expect(response)
      .toEqual(fakeRes)
  })

  test('getNewUserDataApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(404, fakeRes)
    const response = await getNewUserDataApi({...fakeState.login})
    expect(response)
      .toEqual(fakeRes)
  })

})

describe('CustomerAccountSaga logoutApi', () => {
  const fakeParams = {
    uid: 123
  }
  test('logoutApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/logout', fakeParams)
      .reply(200, fakeRes)
    const response = await logoutApi({ token: '123', ...fakeParams } as any)
    expect(response)
      .toEqual(fakeRes)
  })

  test('logoutApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/logout', fakeParams)
      .reply(500, fakeRes)
    const response = await logoutApi({ token: '123', ...fakeParams } as any)
    expect(response)
      .toEqual(fakeRes)
  })

  test('logoutApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/logout', fakeParams)
      .reply(404, fakeRes)
    const response = await logoutApi({ token: '123', ...fakeParams } as any)
    expect(response)
      .toEqual(fakeRes)
  })

  test('getFcmToken should return token', async () => {
    const response = await getFcmToken()
    expect(response)
      .toEqual('token')
  })
  test('getDeviceUniqueId should return 1', async () => {
    const response = await getDeviceUniqueId()
    expect(response)
      .toEqual('1')
  })


  test('onLoginSuccess should execute redux action on success response', async () => {
    const fakeSagaParam: any = {
      user: fakeState.login.user,
      token: 'token'
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/device/update')
      .reply(200, {
        error: false,
        data: true
      })

    await runSaga(fakeStore, onLoginSuccess, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(LoginActions.loginSuccess(fakeState.login.user, fakeSagaParam.token))
    expect(dispatchedAction)
      .toContainEqual(CustomerAddressActions.fetchAddress())

  })
  test('logout should execute redux action on success response', async () => {


    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .post('/public/account/logout')
      .reply(200, fakeRes)

    await runSaga(fakeStore, logout).toPromise()


    expect(dispatchedAction)
      .toContainEqual(LoginActions.resetState())

    expect(dispatchedAction)
      .toContainEqual(CustomerAddressActions.reset())

    expect(dispatchedAction)
      .toContainEqual(AddressHistoryActions.reset())

    expect(dispatchedAction)
      .toContainEqual(CreditTokenActions.resetTokenTopUp())

    expect(dispatchedAction)
      .toContainEqual(CustomerAccountActions.reset())

    expect(dispatchedAction)
      .toContainEqual(CustomerCardActions.resetCards())

    expect(dispatchedAction)
      .toContainEqual(CustomerNotificationActions.resetCustomerNotification())

    expect(dispatchedAction)
      .toContainEqual(CustomerVirtualAccountActions.resetVirtualAccount())

    expect(dispatchedAction)
      .toContainEqual(MyMealPlanActions.reset())

    expect(dispatchedAction)
      .toContainEqual(ForgotPasswordActions.reset())

    expect(dispatchedAction)
      .toContainEqual(OrderActions.resetOrder())

    expect(dispatchedAction)
      .toContainEqual(RegistrationActions.reset())

    expect(dispatchedAction)
      .toContainEqual(CartActions.resetCart())

    expect(dispatchedAction)
      .toContainEqual(ThemeActions.reset())

    expect(dispatchedAction)
      .toContainEqual(MealActions.reset())

    expect(dispatchedAction)
      .toContainEqual(HomeContentActions.resetAll())

    expect(dispatchedAction)
      .toContainEqual(PromotionActions.reset())

    expect(dispatchedAction)
      .toContainEqual(CustomerOrderActions.resetCustomerOrder())

  })

})


