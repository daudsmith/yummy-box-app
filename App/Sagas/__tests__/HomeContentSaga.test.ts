import {
  getToken,
  fetchItemsApi,
  fetchThemesApi,
  getOffDatesApi,
  fetchCorporateThemesApi,
  getSettingApi,
  fetchCorporateThemes,
  fetchItems,
  fetchThemes,
  getThemeDetailApi,
  getThemeDetailMealsApi,
  fetchFeaturedThemeDetail,
  fetchFeaturedThemeItems,
  fetchPromoteApi,
  fetchPromote,
} from '../HomeContentSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import moment from 'moment'
import Actions from '../../Redux/HomeContentRedux'
import { runSaga } from 'redux-saga'
import fakeState from './fakeState'
import {
  GetMealsResponse,
  GetOffDates,
  GetThemeListResponse,
} from '../../Services/ApiCatalog'
import { getSettingResponse } from '../../Services/ApiSetting'
import {
  GetThemeDetailMeals,
  GetThemeDetailResponse,
} from '../../Services/ApiTheme'

const fakeDate = moment()
const fakeKey = '1q2w3e'

describe('HomeContentSage', () => {
  test('fetchPromoteApi should return correct response on success response', async () => {
    nock(AppConfig.apiBaseUrl)
        .get('/public/catalog/category/promoted')
        .query({
            date: '2020-04-23'
          })
        .reply(200, {
            error: false,
        })
    const response = await fetchPromoteApi('2020-04-23')
    expect(response.error)
        .toBe(false)
  })
  test('fetchPromoteApi should return correct response on notFound response', async () => {
      nock(AppConfig.apiBaseUrl)
          .get('/public/catalog/category/promoted')
          .query({
              date: '2020-04-23'
          })
          .reply(404, {
              error: false,
          })
      const response = await fetchPromoteApi('2020-04-23')
      expect(response.error)
          .toBe(false)
  })
  test('fetchPromoteApi should return correct response on error response', async () => {
      nock(AppConfig.apiBaseUrl)
          .get('/public/catalog/category/promoted')
          .query({
              date: '2020-04-23'
          })
          .reply(500, {
              error: false,
          })
      const response = await fetchPromoteApi('2020-04-23')
      expect(response.error)
          .toBe(false)
  })

  test('fetchPromote should return correct response on success response', async () => {
      let dispatchedAction = []
      const fakeStore = {
          dispatch: action => dispatchedAction.push(action),
      }
      const response = {
        error: false,
        data: 
          {
            id: 1,
            name: 'EasyCook',
            selected_date: '2020-04-23',
            exclusive: false,
            items: {
                data: []
            }
          }
        ,
        message: ''
      }
      nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/promoted')
      .query({
          date: '2020-04-23'
      })
      .reply(200, response)
      await runSaga(fakeStore, fetchPromote, { date: '2020-04-23' } as any).toPromise()
      expect(dispatchedAction)
        .toContainEqual(Actions.fetchingPromoteSuccess(response.data))
  })
  test('fetchPromote should return correct response on error response', async () => {
      let dispatchedAction = []
      const fakeStore = {
          dispatch: action => dispatchedAction.push(action),
      }
      const response = {
          error: true,
          data: null,
          message: 'Cannot fetch promote'
      }
      nock(AppConfig.apiBaseUrl)
        .get('/public/catalog/category/promoted')
        .query({
          date: '2020-04-23'
        })
        .reply(500, response)
      await runSaga(fakeStore, fetchPromote, { date: '2020-04-23' } as any).toPromise()
      expect(dispatchedAction)
        .toContainEqual(Actions.fetchingPromoteFailed(response.message))
  })

  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })

  test('fetchItemsApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: fakeDate.format('YYYY-MM-DD'),
        category_id: 1,
      })
      .reply(200, {
        error: false,
      })
    const response = await fetchItemsApi(fakeDate)
    expect((response as GetMealsResponse).error)
      .toBe(false)
  })
  test('fetchItemsApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: fakeDate.format('YYYY-MM-DD'),
        category_id: 1,
      })
      .reply(404, {
        error: false,
      })
    const response = await fetchItemsApi(fakeDate)
    expect((response as GetMealsResponse).error)
      .toBe(false)
  })
  test('fetchItemsApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: fakeDate.format('YYYY-MM-DD'),
        category_id: 1,
      })
      .reply(500, {
        error: false,
      })
    const response = await fetchItemsApi(fakeDate)
    expect((response as GetMealsResponse).error)
      .toBe(false)
  })

  test('fetchThemesApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(200, {
        error: false,
      })
    const response = await fetchThemesApi()
    expect((response as GetThemeListResponse).error)
      .toBe(false)
  })
  test('fetchThemesApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(404, {
        error: false,
      })
    const response = await fetchThemesApi()
    expect((response as GetThemeListResponse).error)
      .toBe(false)
  })
  test('fetchThemesApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(500, {
        error: false,
      })
    const response = await fetchThemesApi()
    expect((response as GetThemeListResponse).error)
      .toBe(false)
  })

  test('getOffDatesApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(200, {
        error: false,
      })
    const response = await getOffDatesApi()
    expect((response as GetOffDates).error)
      .toBe(false)
  })
  test('getOffDatesApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(404, {
        error: false,
      })
    const response = await getOffDatesApi()
    expect((response as GetOffDates).error)
      .toBe(false)
  })
  test('getOffDatesApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(500, {
        error: false,
      })
    const response = await getOffDatesApi()
    expect((response as GetOffDates).error)
      .toBe(false)
  })

  test('fetchCorporateThemesApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      .reply(200, {
        error: false,
      })
    const response = await fetchCorporateThemesApi(fakeState.login.token)
    expect((response as GetThemeListResponse).error)
      .toBe(false)
  })
  test('fetchCorporateThemesApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      .reply(404, {
        error: false,
      })
    const response = await fetchCorporateThemesApi(fakeState.login.token)
    expect((response as GetThemeListResponse).error)
      .toBe(false)
  })
  test('fetchCorporateThemesApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      .reply(500, {
        error: false,
      })
    const response = await fetchCorporateThemesApi(fakeState.login.token)
    expect((response as GetThemeListResponse).error)
      .toBe(false)
  })

  test('getSettingApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/setting/get/${fakeKey}`)
      .reply(200, {
        error: false,
      })
    const response = await getSettingApi(fakeKey)
    expect((response as getSettingResponse).error)
      .toBe(false)
  })
  test('getSettingApi should return correct response on not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/setting/get/${fakeKey}`)
      .reply(404, {
        error: false,
      })
    const response = await getSettingApi(fakeKey)
    expect((response as getSettingResponse).error)
      .toBe(false)
  })
  test('getSettingApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get(`/public/setting/get/${fakeKey}`)
      .reply(500, {
        error: false,
      })
    const response = await getSettingApi(fakeKey)
    expect((response as getSettingResponse).error)
      .toBe(false)
  })

  test('fetchCorporateThemes should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.homeContent.corporateThemes,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      .reply(200, response)
    await runSaga(fakeStore, fetchCorporateThemes).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingCorporateThemesSuccess(response.data))
  })
  test('fetchCorporateThemes should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.homeContent.corporateThemes,
      message: 'Cannot fetch corporate themes due to network issues',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      .reply(500, response)
    await runSaga(fakeStore, fetchCorporateThemes).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingCorporateThemesFailed(response.message))
  })
  test('fetchCorporateThemes should execute redux action on not found response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.homeContent.corporateThemes,
      message: 'Page Not Found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/corporate/list')
      .reply(404, response)
    await runSaga(fakeStore, fetchCorporateThemes).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingCorporateThemesFailed(response.message))
  })

  test('fetchItems should execute redux action on success response', async () => {
    Date.now = jest.fn(() => new Date(2019, 8, 9).valueOf())
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: JSON.stringify([
        '2019-09-01',
        '2019-09-09',
      ]),
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(200, response)

    const responseSetting = {
      error: false,
      data: 7200,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/delivery_cut_off')
      .reply(200, responseSetting)

    const responsefetchItemsApi = {
      error: false,
      data: fakeState.homeContent.items
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: moment()
          .add(1, 'days')
          .format('YYYY-MM-DD'),
        category_id: 1,
      })
      .reply(200, responsefetchItemsApi)

    await runSaga(fakeStore, fetchItems).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingItemsSuccess(responsefetchItemsApi.data))
  })
  test('fetchItems should execute redux action on failed response', async () => {
    Date.now = jest.fn(() => new Date(2019, 8, 9).valueOf())
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: JSON.stringify([
        '2019-09-01',
        '2019-09-09',
      ]),
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(200, response)

    const responseSetting = {
      error: false,
      data: 7200,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/delivery_cut_off')
      .reply(200, responseSetting)

    const responsefetchItemsApi = {
      error: true,
      data: fakeState.homeContent.items,
      message: 'Cannot fetch items',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/list')
      .query({
        date: moment()
          .add(1, 'days')
          .format('YYYY-MM-DD'),
        category_id: 1,
      })
      .reply(200, responsefetchItemsApi)

    await runSaga(fakeStore, fetchItems).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingItemsFailed(responsefetchItemsApi.message))
  })

  test('fetchThemes should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.homeContent.themes,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(200, response)
    await runSaga(fakeStore, fetchThemes).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingThemesSuccess(response.data))
  })
  test('fetchThemes should execute redux action on failed/error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.homeContent.themes,
      message: 'Cannot fetch themes',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/theme/list')
      .reply(200, response)
    await runSaga(fakeStore, fetchThemes).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingThemesFailed(response.message))
  })

  test('getThemeDetailApi should execute redux action on success response', async () => {
    const fakeId = 1

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(200, {
        error: false,
      })
    const response = await getThemeDetailApi(fakeId)
    expect((response as GetThemeDetailResponse).error)
      .toBe(false)
  })
  test('getThemeDetailApi should execute redux action on notFound response', async () => {
    const fakeId = 1

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(404, {
        error: false,
      })
    const response = await getThemeDetailApi(fakeId)
    expect((response as GetThemeDetailResponse).error)
      .toBe(false)
  })
  test('getThemeDetailApi should execute redux action on error response', async () => {
    const fakeId = 1

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(500, {
        error: false,
      })
    const response = await getThemeDetailApi(fakeId)
    expect((response as GetThemeDetailResponse).error)
      .toBe(false)
  })

  test('getThemeDetailMealsApi should execute redux action on success response', async () => {
    const fakeId = 1

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(200, {
        error: false,
      })
    const response = await getThemeDetailMealsApi(fakeId)
    expect((response as GetThemeDetailMeals).error)
      .toBe(false)
  })
  test('getThemeDetailMealsApi should execute redux action on notFound response', async () => {
    const fakeId = 1

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(404, {
        error: false,
      })
    const response = await getThemeDetailMealsApi(fakeId)
    expect((response as GetThemeDetailMeals).error)
      .toBe(false)
  })
  test('getThemeDetailMealsApi should execute redux action on error response', async () => {
    const fakeId = 1

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(500, {
        error: false,
      })
    const response = await getThemeDetailMealsApi(fakeId)
    expect((response as GetThemeDetailMeals).error)
      .toBe(false)
  })

  test('fetchFeaturedThemeDetail should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeId = 1
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.homeContent.themes,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(200, response)
    await runSaga(fakeStore, fetchFeaturedThemeDetail).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingFeaturedThemeDetail())
  })
  test('fetchFeaturedThemeDetail should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeId = 1
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.homeContent.themes,
      message: 'Cannot fetch theme detail due to network issues',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(500, response)
    await runSaga(fakeStore, fetchFeaturedThemeDetail).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingFeaturedThemeDetail())
  })

  test('fetchFeaturedThemeItems should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeId = 1
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.homeContent.themes,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(200, response)
    await runSaga(fakeStore, fetchFeaturedThemeItems).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingFeaturedThemeItems())
  })
  test('fetchFeaturedThemeItems should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeId = 1
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.homeContent.themes,
      message: 'Cannot fetch detail items due to network issues',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(500, response)
    await runSaga(fakeStore, fetchFeaturedThemeItems).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingFeaturedThemeItemsFailed(response.message))
  })

})
