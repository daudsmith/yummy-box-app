import { fetchWalletApi, fetchWallet, getToken, changePasswordApi, changePassword, changeEmail, changePhoneNumberApi, changePhoneNumberValidate, changeEmailApi, requestOtpApi, checkClientApi } from '../CustomerAccountSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import CustomerActions from '../../Redux/CustomerAccountRedux'
import fakeState from './fakeState'

// fetchWalletApi and getToken
describe('CustomerAccountSaga fetchWalletApi and getToken', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })

  test('fetchWalletApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/wallet')
      .reply(200, {
        error: false,
      })
    const response = await fetchWalletApi(token)
    expect(response.error)
      .toBe(false)
  })
  test('fetchWalletApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/wallet')
      .reply(404, {
        error: true,
      })
    const response = await fetchWalletApi(token)
    expect(response.error)
      .toBe(true)
  })
  test('fetchWalletApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/wallet')
      .reply(500, {
        error: true,
      })
    const response = await fetchWalletApi(token)
    expect(response.error)
      .toBe(true)
  })


  test('fetchWallet should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: 10000
    }
    const responseClient = {
      error: false,
      data: true
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/wallet')
      .reply(200, response)

    nock(AppConfig.apiCaterUrl)
      .get('/is_client')
      .reply(200, responseClient)

    await runSaga(fakeStore, fetchWallet).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.fetchingWalletDone(response))

    expect(dispatchedAction)
      .toContainEqual(CustomerActions.checkClientDone(true))
  })
  test('fetchWallet should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'error'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/wallet')
      .reply(500, response)

    nock(AppConfig.apiCaterUrl)
      .get('/is_client')
      .reply(500, response)

    await runSaga(fakeStore, fetchWallet).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.fetchingWalletFailed(response.message))

    expect(dispatchedAction)
      .toContainEqual(CustomerActions.checkClientFailed())
  })
  test('fetchWallet should execute redux action on not found', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'not found'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/wallet')
      .reply(404, response)

    nock(AppConfig.apiCaterUrl)
      .get('/is_client')
      .reply(404, response)

    await runSaga(fakeStore, fetchWallet).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.fetchingWalletFailed(response.message))

    expect(dispatchedAction)
      .toContainEqual(CustomerActions.checkClientFailed())
  })
})

// CustomerAccountSaga
describe('CustomerAccountSaga changePasswordApi', () => {
  const fakeRequestBody: any = {
    old_password: 'oldpass',
    password: 'pass',
    password_confirmation: 'pass',
  }
  test('changePasswordApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/password/update', fakeRequestBody)
      .reply(200, {
        error: false,
      })
    const response = await changePasswordApi({ token, ...fakeRequestBody })
    expect(response.error)
      .toBe(false)
  })
  test('changePasswordApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/password/update', fakeRequestBody)
      .reply(404, {
        error: true,
      })
    const response = await changePasswordApi({ token, ...fakeRequestBody })
    expect(response.error)
      .toBe(true)
  })
  test('changePasswordApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/password/update', fakeRequestBody)
      .reply(500, {
        error: true,
      })
    const response = await changePasswordApi({ token, ...fakeRequestBody })
    expect(response.error)
      .toBe(true)
  })

  test('changePassword should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: true
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/password/update', fakeRequestBody)
      .reply(200, response)

    await runSaga(fakeStore, changePassword, fakeRequestBody).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changePasswordSuccess())

  })
  test('changePassword should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'error'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/password/update', fakeRequestBody)
      .reply(500, response)


    await runSaga(fakeStore, changePassword, fakeRequestBody).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changePasswordFailed(response.message))

  })
  test('changePassword should execute redux action on not found', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'not found'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/password/update', fakeRequestBody)
      .reply(404, response)

    await runSaga(fakeStore, changePassword, fakeRequestBody).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changePasswordFailed(response.message))
  })
})
// changePhoneNumberApi
describe('CustomerAccountSaga changePhoneNumberApi', () => {
  const fakeRequestBody: any = {
    phone: '087665554241',
  }
  test('changePhoneNumberApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/phone', fakeRequestBody)
      .reply(200, {
        error: false,
      })
    const response = await changePhoneNumberApi({ token, ...fakeRequestBody })
    expect(response.error)
      .toBe(false)
  })
  test('changePhoneNumberApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/phone', fakeRequestBody)
      .reply(404, {
        error: true,
      })
    const response = await changePhoneNumberApi({ token, ...fakeRequestBody })
    expect(response.error)
      .toBe(true)
  })
  test('changePhoneNumberApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/phone', fakeRequestBody)
      .reply(500, {
        error: true,
      })
    const response = await changePhoneNumberApi({ token, ...fakeRequestBody })
    expect(response.error)
      .toBe(true)
  })


  test('changePhoneNumber should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: true
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/phone', fakeRequestBody)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(200, response)

    await runSaga(fakeStore, changePhoneNumberValidate, fakeRequestBody).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changePhoneNumberSuccess())

  })
  test('changePhoneNumber should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'error'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/phone', fakeRequestBody)
      .reply(500, response)


    await runSaga(fakeStore, changePhoneNumberValidate, fakeRequestBody).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changePhoneNumberFailed(response.message))

  })
  test('changePhoneNumber should execute redux action on not found', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'not found'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/phone', fakeRequestBody)
      .reply(404, response)

    await runSaga(fakeStore, changePhoneNumberValidate, fakeRequestBody).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changePhoneNumberFailed(response.message))
  })


})

// changeEmailApi
describe('CustomerAccountSaga changeEmailApi', () => {
  const fakeRequestBody = {
    email: 'ricky@gmail.com',
  }
  const fakeParam: any = {
    newEmail: 'ricky@gmail.com'
  }
  test('changeEmailApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/email', fakeRequestBody)
      .reply(200, {
        error: false,
      })
    const response = await changeEmailApi({ token, ...fakeParam })
    expect(response.error)
      .toBe(false)
  })
  test('changeEmailApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/email', fakeRequestBody)
      .reply(404, {
        error: true,
      })
    const response = await changeEmailApi({ token, ...fakeParam })
    expect(response.error)
      .toBe(true)
  })
  test('changeEmailApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/email', fakeRequestBody)
      .reply(500, {
        error: true,
      })
    const response = await changeEmailApi({ token, ...fakeParam })
    expect(response.error)
      .toBe(true)
  })


  test('changeEmail should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: true
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/email', fakeRequestBody)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get('/public/account/me')
      .reply(200, response)

    await runSaga(fakeStore, changeEmail, fakeParam).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changeEmailSuccess())

  })
  test('changeEmail should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'error'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/email', fakeRequestBody)
      .reply(500, response)


    await runSaga(fakeStore, changeEmail, fakeParam).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changeEmailFailed(response.message))

  })
  test('changeEmail should execute redux action on not found', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'not found'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/update/email', fakeRequestBody)
      .reply(404, response)

    await runSaga(fakeStore, changeEmail, fakeParam).toPromise()
    expect(dispatchedAction)
      .toContainEqual(CustomerActions.changeEmailFailed(response.message))
  })
})

// requestOtpApi
describe('CustomerAccountSaga requestOtpApi', () => {
  const fakeParam: any = {
    newPhoneNumber: '087665554241',
  }
  const fakeNewPhoneNumber = '087665554241'
  test('requestOtpApi should return correct response on success', async () => {
    nock(AppConfig.apiRequestApi)
      .post(`/start?via=sms&country_code=+62&phone_number=${fakeNewPhoneNumber}&locale=en`)
      .reply(200, {
        error: false,
      })
    const response = await requestOtpApi(fakeParam)
    expect(response.error)
      .toBe(false)
  })
  test('requestOtpApi should return correct response on page not found', async () => {
    nock(AppConfig.apiRequestApi)
      .post(`/start?via=sms&country_code=+62&phone_number=${fakeNewPhoneNumber}&locale=en`)
      .reply(404, {
        error: true,
      })
    const response = await requestOtpApi(fakeParam)
    expect(response.error)
      .toBe(true)
  })
  test('requestOtpApi should return correct response on error', async () => {
    nock(AppConfig.apiRequestApi)
      .post(`/start?via=sms&country_code=+62&phone_number=${fakeNewPhoneNumber}&locale=en`)
      .reply(500, {
        error: true,
      })
    const response = await requestOtpApi(fakeParam)
    expect(response.error)
      .toBe(true)
  })

  test('sendOtpNumber should execute redux action on success', async () => {

    nock(AppConfig.apiRequestApi)
      .post(`/start?via=sms&country_code=+62&phone_number=${fakeNewPhoneNumber}&locale=en`)
      .reply(200, {
        error: false,
      })
    expect(fakeParam.newPhoneNumber)
      .toBe(fakeNewPhoneNumber)
  })
})

// checkClientApi
describe('CustomerAccountSaga checkClientApi', () => {

  const token = getToken(fakeState)

  test('checkClientApi should return correct response on success', async () => {

    nock(AppConfig.apiCaterUrl)
      .get('/is_client')
      .reply(200, {
        error: false,
      })
    const response = await checkClientApi(token)
    expect(response.error)
      .toBe(false)
  })
  test('checkClientApi should return correct response on page not found', async () => {

    nock(AppConfig.apiCaterUrl)
      .get('/is_client')
      .reply(404, {
        error: true,
      })
    const response = await checkClientApi(token)
    expect(response.error)
      .toBe(true)
  })
  test('checkClientApi should return correct response on error', async () => {

    nock(AppConfig.apiCaterUrl)
      .get('/is_client')
      .reply(500, {
        error: true,
      })
    const response = await checkClientApi(token)
    expect(response.error)
      .toBe(true)
  })
})
