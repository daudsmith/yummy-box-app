import {
  getToken,
  getCards,
  saveCardApi,
  fetchCardsApi,
  deleteCardApi,
  setDefaultCardApi,
  checkDuplicateCard,
  fetchCards,
  addCard,
  removeCard,
  setDefaultCard,
} from '../CustomerCardSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/CustomerCardRedux'
import fakeState from './fakeState'
import {
  defaultCardApiResponse,
  getSavedCardsApiResponse,
  saveCardApiResponse,
} from '../../Services/ApiCustomer'

// const fakeState = {
//   login: {
//     token: '12345',
//   },
//   params: {
//     card: {
//       cardId: '1',
//       type: 'New',
//       endsWith: 'New'
//     }
//   },
//   customerCard: {
//     cards: [{
//       cardId: '1',
//       type: 'Type',
//       endsWith: 'Saved'
//     },
//     {
//       cardId: '2',
//       type: 'Type',
//       endsWith: 'Saved'
//     }]
//   }
// }
let cardId = 1
let cards = {
  newCard: {
    id: 2,
    created_at: '',
    updated_at: '',
    account_id: 1,
    token: '',
    authorization_id: '',
    type: 'MASTERCARD',
    ends_with: '',
    is_default: 0
  },
}

describe('CustomerCardSaga', () => {
  test('getToken should return correct token', () => {
    const { token } = getToken(fakeState)
    expect(token)
      .toBe(fakeState.login.token)
  })

  test('getCards should return correct card', () => {
    const customerCard = getCards(fakeState)
    expect(customerCard.cards)
      .toBe(fakeState.customerCard.cards)
  })

  test('saveCardApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/card/create')
      .reply(200, {
        error: false,
      })
    const response = await saveCardApi({ card: fakeState.customerCard.cards[0], token: fakeState.login.token })
    expect((response as saveCardApiResponse).error)
      .toBe(false)
  })

  test('saveCardApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/card/create')
      .reply(404, {
        error: true,
      })
    const response = await saveCardApi({ card: fakeState.customerCard.cards[0], token: fakeState.login.token })
    expect((response as saveCardApiResponse).error)
      .toBe(true)
  })

  test('saveCardApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/card/create')
      .reply(500, {
        error: true,
      })
    const response = await saveCardApi({ card: fakeState.customerCard.cards[0], token: fakeState.login.token })
    expect((response as saveCardApiResponse).error)
      .toBe(true)
  })

  test('fetchCardsApi should return correct response on success', async () => {
    const {token} = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(200, {
        error: false,
      })
    const response = await fetchCardsApi(token)
    expect((response as getSavedCardsApiResponse).error)
      .toBe(false)
  })

  test('fetchCardsApi should return correct response on error', async () => {
    const {token} = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(404, {
        error: true,
      })
    const response = await fetchCardsApi(token)
    expect((response as getSavedCardsApiResponse).error)
      .toBe(true)
  })

  test('fetchCardsApi should return correct response on error', async () => {
    const {token} = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(500, {
        error: true,
      })
    const response = await fetchCardsApi(token)
    expect((response as getSavedCardsApiResponse).error)
      .toBe(true)
  })

  test('deleteCardApi should return correct response on success', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/card/${cardId}/delete`)
      .reply(200, {
        error: false,
      })
    const response = await deleteCardApi({ cardId: cardId, token })
    expect((response as defaultCardApiResponse).error)
      .toBe(false)
  })

  test('deleteCardApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/card/${cardId}/delete`)
      .reply(404, {
        error: true,
      })
    const response = await deleteCardApi({ cardId: cardId, token })
    expect((response as defaultCardApiResponse).error)
      .toBe(true)
  })

  test('deleteCardApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/card/${cardId}/delete`)
      .reply(500, {
        error: true,
      })
    const response = await deleteCardApi({ cardId: cardId, token })
    expect((response as defaultCardApiResponse).error)
      .toBe(true)
  })

  test('setDefaultCardApi should return correct response on success', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .put(`/public/account/card/${cardId}/default`)
      .reply(200, {
        error: false,
      })
    const response = await setDefaultCardApi({ cardId: cardId, token })
    expect((response as defaultCardApiResponse).error)
      .toBe(false)
  })

  test('setDefaultCardApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .put(`/public/account/card/${cardId}/default`)
      .reply(404, {
        error: true,
      })
    const response = await setDefaultCardApi({ cardId: cardId, token })
    expect((response as defaultCardApiResponse).error)
      .toBe(true)
  })

  test('setDefaultCardApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .put(`/public/account/card/${cardId}/default`)
      .reply(500, {
        error: true,
      })
    const response = await setDefaultCardApi({ cardId: cardId, token })
    expect((response as defaultCardApiResponse).error)
      .toBe(true)
  })

  test('checkDuplicateCard should return true if card is exists in saved cards', async () => {
    const duplicateTrue = checkDuplicateCard({ card: fakeState.customerCard.cards[0], savedCards: fakeState.customerCard.cards })
    expect(duplicateTrue)
      .toBe(true)
  })

  test('checkDuplicateCard should return false if card is not exists in saved cards', async () => {
    const duplicateFalse = checkDuplicateCard({ card: cards.newCard, savedCards: fakeState.customerCard.cards })
    expect(duplicateFalse)
      .toBe(false)
  })

  test('fetchCards should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerCard.cards,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(200, response)
    await runSaga(fakeStore, fetchCards).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateCards(response.data))
  })

  test('fetchCards should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(404, response)
    await runSaga(fakeStore, fetchCards).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

  test('fetchCards should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error message',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(500, response)
    await runSaga(fakeStore, fetchCards).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

  test('addCard should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerCard.cards
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/card/create')
      .reply(200, {
        error: false,
      })
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(200, response)

    await runSaga(fakeStore, addCard, { card: cards.newCard }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateCards(response.data))
    expect(dispatchedAction)
      .toContainEqual(Actions.addCardSuccess())
  })

  test('addCard should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/card/create')
      .reply(404, response)

    await runSaga(fakeStore, addCard, { card: cards.newCard }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

  test('addCard should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error message'
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/card/create')
      .reply(500, response)

    await runSaga(fakeStore, addCard, { card: cards.newCard }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

  test('removeCard should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerCard.cards
    }
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/card/${cardId}/delete`)
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(200, response)

    await runSaga(fakeStore, removeCard, { cardId: cardId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateCards(response.data))
    expect(dispatchedAction)
      .toContainEqual(Actions.removeCardSuccess())
  })

  test('removeCard should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/card/${cardId}/delete`)
      .reply(404, response)
    await runSaga(fakeStore, removeCard, { cardId: cardId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

  test('removeCard should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error message'
    }
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/card/${cardId}/delete`)
      .reply(500, response)
    await runSaga(fakeStore, removeCard, { cardId: cardId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

  test('setDefaultCard should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerCard.cards
    }
    nock(AppConfig.apiBaseUrl)
      .put(`/public/account/card/${cardId}/default`)
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/card')
      .reply(200, response)

    await runSaga(fakeStore, setDefaultCard, { cardId: cardId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateCards(response.data))
    expect(dispatchedAction)
      .toContainEqual(Actions.setDefaultCardSuccess())
  })

  test('setDefaultCard should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .put(`/public/account/card/${cardId}/default`)
      .reply(404, response)

    await runSaga(fakeStore, setDefaultCard, { cardId: cardId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

  test('setDefaultCard should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error message'
    }
    nock(AppConfig.apiBaseUrl)
      .put(`/public/account/card/${cardId}/default`)
      .reply(500, response)

    await runSaga(fakeStore, setDefaultCard, { cardId: cardId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.errorOccurred(response.message))
  })

})
