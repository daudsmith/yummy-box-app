import {
  getToken,
  fetchUnreadNotificationsApi,
  fetchUnreadNotifications,
  fetchTotalUnreadNotificationsApi,
  fetchTotalUnreadNotifications,
  fetchNotificationListApi,
  fetchNotificationList,
  markNotificationsToReadApi,
  markNotificationsToRead,
  notificationGetReadByidApi,
  fetchingNotificationGetReadById,
  notificationGetUnreadByidApi,
  fetchingNotificationGetUnreadById,
  deleteSingleNotificationAPI,
  deleteSingleNotification,
  deleteAllNotificationsAPI,
  deleteAllNotifications,
} from '../CustomerNotificationSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/CustomerNotificationRedux'
import fakeState from './fakeState'
import {
  DeleteAllNotifications,
  GetNotifications,
  GetTotalUnread,
} from '../../Services/ApiCustomerNotification'

const fakeId = 1
describe('CustomerNotificationSaga', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })

  test('fetchUnreadNotificationsApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread')
      .reply(200, {
        error: false,
      })
    const response = await fetchUnreadNotificationsApi(token)
    expect((response as GetNotifications).error)
      .toBe(false)
  })
  test('fetchUnreadNotificationsApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread')
      .reply(404, {
        error: true,
      })
    const response = await fetchUnreadNotificationsApi(token)
    expect((response as GetNotifications).error)
      .toBe(true)
  })
  test('fetchUnreadNotificationsApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread')
      .reply(500, {
        error: true,
      })
    const response = await fetchUnreadNotificationsApi(token)
    expect((response as GetNotifications).error)
      .toBe(true)
  })

  test('fetchUnreadNotifications should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread')
      .reply(200, response)
    await runSaga(fakeStore, fetchUnreadNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingUnreadNotificationsDone(response.data.length))
  })
  test('fetchUnreadNotifications should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerNotification.notificationList,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread')
      .reply(500, response)
    await runSaga(fakeStore, fetchUnreadNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.resetCustomerNotification())
  })
  test('fetchUnreadNotifications should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerNotification.notificationList,
      message: 'Page Not Found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread')
      .reply(404, response)
    await runSaga(fakeStore, fetchUnreadNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.resetCustomerNotification())
  })

  test('fetchTotalUnreadNotificationsApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread/total')
      .reply(200, {
        error: false,
      })
    const response = await fetchTotalUnreadNotificationsApi(token)
    expect((response as GetTotalUnread).error)
      .toBe(false)
  })
  test('fetchTotalUnreadNotificationsApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread/total')
      .reply(404, {
        error: true,
      })
    const response = await fetchTotalUnreadNotificationsApi(token)
    expect((response as GetTotalUnread).error)
      .toBe(true)
  })
  test('fetchTotalUnreadNotificationsApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread/total')
      .reply(500, {
        error: true,
      })
    const response = await fetchTotalUnreadNotificationsApi(token)
    expect((response as GetTotalUnread).error)
      .toBe(true)
  })

  test('fetchTotalUnreadNotifications should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: 30,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread/total')
      .reply(200, response)
    await runSaga(fakeStore, fetchTotalUnreadNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingUnreadNotificationsDone(response.data))
  })
  test('fetchTotalUnreadNotifications should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: 5,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread/total')
      .reply(500, response)
    await runSaga(fakeStore, fetchTotalUnreadNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.resetCustomerNotification())
  })
  test('fetchTotalUnreadNotifications should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: 5,
      message: 'Page Not Found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread/total')
      .reply(404, response)
    await runSaga(fakeStore, fetchTotalUnreadNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.resetCustomerNotification())
  })

  test('fetchNotificationListApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/list')
      .reply(200, {
        error: false,
      })
    const response = await fetchNotificationListApi(token)
    expect((response as GetNotifications).error)
      .toBe(false)
  })
  test('fetchNotificationListApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/list')
      .reply(404, {
        error: true,
      })
    const response = await fetchNotificationListApi(token)
    expect((response as GetNotifications).error)
      .toBe(true)
  })
  test('fetchNotificationListApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/list')
      .reply(500, {
        error: true,
      })
    const response = await fetchNotificationListApi(token)
    expect((response as GetNotifications).error)
      .toBe(true)
  })

  test('fetchNotificationList should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/list')
      .reply(200, response)
    await runSaga(fakeStore, fetchNotificationList, { silent: true }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingNotificationListDone(response.data))
  })
  test('fetchNotificationList should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerNotification.notificationList,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/list')
      .reply(500, response)
    await runSaga(fakeStore, fetchNotificationList, { silent: false }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingNotificationListFailed(response.message))
  })
  test('fetchTotalUnreadNotifications should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerNotification.notificationList,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/list')
      .reply(404, response)
    await runSaga(fakeStore, fetchNotificationList, { silent: false }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingNotificationListFailed(response.message))
  })

  test('markNotificationsToReadApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/mark_read')
      .reply(200, {
        error: false,
      })
    const response = await markNotificationsToReadApi(token)
    expect((response as GetNotifications).error)
      .toBe(false)
  })
  test('markNotificationsToReadApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/mark_read')
      .reply(404, {
        error: true,
      })
    const response = await markNotificationsToReadApi(token)
    expect((response as GetNotifications).error)
      .toBe(true)
  })
  test('markNotificationsToReadApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/mark_read')
      .reply(500, {
        error: true,
      })
    const response = await markNotificationsToReadApi(token)
    expect((response as GetNotifications).error)
      .toBe(true)
  })

  test('markNotificationsToRead should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/mark_read')
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/list')
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/unread')
      .reply(200, response)
    await runSaga(fakeStore, markNotificationsToRead).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingUnreadNotificationsDone(response.data.length))
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingNotificationListDone(response.data))
  })
  test('markNotificationsToRead should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerNotification.notificationList,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/mark_read')
      .reply(500, response)
    await runSaga(fakeStore, markNotificationsToRead).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.markNotificationsToReadFailed(response.message))
  })
  test('markNotificationsToRead should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerNotification.notificationList,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/mark_read')
      .reply(404, response)
    await runSaga(fakeStore, markNotificationsToRead).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.markNotificationsToReadFailed(response.message))
  })

  test('notificationGetReadByidApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/read`)
      .reply(200, {
        error: false,
      })
    const response = await notificationGetReadByidApi(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(false)
  })
  test('notificationGetReadByidApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/read`)
      .reply(404, {
        error: true,
      })
    const response = await notificationGetReadByidApi(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(true)
  })
  test('notificationGetReadByidApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/read`)
      .reply(500, {
        error: true,
      })
    const response = await notificationGetReadByidApi(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(true)
  })

  test('fetchingNotificationGetReadById should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/read`)
      .reply(200, response)
    await runSaga(fakeStore, fetchingNotificationGetReadById, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updatingReadStatus(fakeId))
  })
  test('fetchingNotificationGetReadById should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/read`)
      .reply(500, response)
    await runSaga(fakeStore, fetchingNotificationGetReadById, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updatingReadStatus(fakeId))
  })
  test('fetchingNotificationGetReadById should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/read`)
      .reply(404, response)
    await runSaga(fakeStore, fetchingNotificationGetReadById, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updatingReadStatus(fakeId))
  })

  test('notificationGetUnreadByidApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/unread`)
      .reply(200, {
        error: false,
      })
    const response = await notificationGetUnreadByidApi(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(false)
  })
  test('notificationGetUnreadByidApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/unread`)
      .reply(404, {
        error: true,
      })
    const response = await notificationGetUnreadByidApi(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(true)
  })
  test('notificationGetUnreadByidApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/unread`)
      .reply(500, {
        error: true,
      })
    const response = await notificationGetUnreadByidApi(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(true)
  })

  test('fetchingNotificationGetUnreadById should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/unread`)
      .reply(200, response)
    await runSaga(fakeStore, fetchingNotificationGetUnreadById, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updatingUnreadStatus(fakeId))
  })
  test('fetchingNotificationGetUnreadById should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/unread`)
      .reply(500, response)
    await runSaga(fakeStore, fetchingNotificationGetUnreadById, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updatingUnreadStatus(fakeId))
  })
  test('fetchingNotificationGetUnreadById should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/unread`)
      .reply(404, response)
    await runSaga(fakeStore, fetchingNotificationGetUnreadById, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updatingUnreadStatus(fakeId))
  })

  test('deleteSingleNotificationAPI should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/delete`)
      .reply(200, {
        error: false,
      })
    const response = await deleteSingleNotificationAPI(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(false)
  })
  test('deleteSingleNotificationAPI should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/delete`)
      .reply(404, {
        error: true,
      })
    const response = await deleteSingleNotificationAPI(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(true)
  })
  test('deleteSingleNotificationAPI should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/delete`)
      .reply(500, {
        error: true,
      })
    const response = await deleteSingleNotificationAPI(token, fakeId)
    expect((response as GetNotifications).error)
      .toBe(true)
  })

  test('deleteSingleNotification should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/delete`)
      .reply(200, response)
    await runSaga(fakeStore, deleteSingleNotification, { notificationId: fakeId }).toPromise()
    const unread = response.data.filter((notification) => !notification.read)
    expect(dispatchedAction)
      .toContainEqual(Actions.singleNotificationDeleted(response.data, unread.length))
  })
  test('deleteSingleNotification should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Response Error',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/delete`)
      .reply(500, response)
    await runSaga(fakeStore, deleteSingleNotification, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.deleteNotificationFailed(response.message))
  })
  test('deleteSingleNotification should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/account/notification/${fakeId}/delete`)
      .reply(404, response)
    await runSaga(fakeStore, deleteSingleNotification, { notificationId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.deleteNotificationFailed(response.message))
  })

  test('deleteAllNotificationsAPI should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/delete/all')
      .reply(200, { error: false })
    const response = await deleteAllNotificationsAPI(token)
    expect((response as DeleteAllNotifications).error)
      .toBe(false)
  })
  test('deleteAllNotificationsAPI should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/delete/all')
      .reply(404, { error: true })
    const response = await deleteAllNotificationsAPI(token)
    expect((response as DeleteAllNotifications).error)
      .toBe(true)
  })
  test('deleteAllNotificationsAPI should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/delete/all')
      .reply(500, { error: true })
    const response = await deleteAllNotificationsAPI(token)
    expect((response as DeleteAllNotifications).error)
      .toBe(true)
  })

  test('deleteAllNotifications should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerNotification.notificationList,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/delete/all')
      .reply(200, response)
    await runSaga(fakeStore, deleteAllNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.allNotificationsDeleted())
  })
  test('deleteAllNotifications should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Response Error',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/delete/all')
      .reply(500, response)
    await runSaga(fakeStore, deleteAllNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.deleteNotificationFailed(response.message))
  })
  test('deleteAllNotifications should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/notification/delete/all')
      .reply(404, response)
    await runSaga(fakeStore, deleteAllNotifications).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.deleteNotificationFailed(response.message))
  })
})
