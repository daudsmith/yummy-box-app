import {
  fetchingMealsApi,
  fetchMealDetailsApi,
  fetchTagListApi,
  fetchFilteredMealsApi,
  getOffDatesApi,
  fetchMeals,
  fetchingMealDetails,
  fetchFilteredMeals,
} from '../MealsSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/MealsRedux'
import {
  GetCatalogCategoriesResponse,
  GetMealResponse,
  GetOffDates,
  GetTagListResponse,
} from '../../Services/ApiCatalog'
import fakeState from './fakeState'

const fakeParams = {
  id: 1,
  date: '2019-11-19',
  includeTags: 1,
  excludeTags: 2,
}

describe('MealsSaga', () => {
  test('fetchingMealsApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date
      })
      .reply(200, {
        error: false
      })
    const response = await fetchingMealsApi(fakeParams.date)
    expect((response as GetCatalogCategoriesResponse).error)
      .toBe(false)
  })

  test('fetchingMealsApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date
      })
      .reply(404, {
        error: true
      })
    const response = await fetchingMealsApi(fakeParams.date)
    expect((response as GetCatalogCategoriesResponse).error)
      .toBe(true)
  })

  test('fetchingMealsApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date
      })
      .reply(500, {
        error: true
      })
    const response = await fetchingMealsApi(fakeParams.date)
    expect((response as GetCatalogCategoriesResponse).error)
      .toBe(true)
  })

  test('fetchMealDetailsApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .reply(200, {
        error: false
      })
    const response = await fetchMealDetailsApi({ id: null, date: null })
    expect((response as GetMealResponse).error)
      .toBe(false)
  })

  test('fetchMealDetailsApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .reply(404, {
        error: true
      })
    const response = await fetchMealDetailsApi({ id: null, date: null })
    expect((response as GetMealResponse).error)
      .toBe(true)
  })

  test('fetchMealDetailsApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .reply(500, {
        error: true
      })
    const response = await fetchMealDetailsApi({ id: null, date: null })
    expect((response as GetMealResponse).error)
      .toBe(true)
  })

  test('fetchTagListApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(200, {
        error: false
      })
    const response = await fetchTagListApi()
    expect((response as GetTagListResponse).error)
      .toBe(false)
  })

  test('fetchTagListApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(404, {
        error: true
      })
    const response = await fetchTagListApi()
    expect((response as GetTagListResponse).error)
      .toBe(true)
  })

  test('fetchTagListApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(500, {
        error: true
      })
    const response = await fetchTagListApi()
    expect((response as GetTagListResponse).error)
      .toBe(true)
  })

  test('fetchFilteredMealsApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date,
        include_tags: fakeParams.includeTags,
        exclude_tags: fakeParams.excludeTags
      })
      .reply(200, {
        error: false
      })
    const response = await fetchFilteredMealsApi({
      date: fakeParams.date,
      includeTags: fakeParams.includeTags,
      excludeTags: fakeParams.excludeTags
    } as any)
    expect((response as GetCatalogCategoriesResponse).error)
      .toBe(false)
  })

  test('fetchFilteredMealsApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date,
        include_tags: fakeParams.includeTags,
        exclude_tags: fakeParams.excludeTags
      })
      .reply(404, {
        error: true
      })
    const response = await fetchFilteredMealsApi({
      date: fakeParams.date,
      includeTags: fakeParams.includeTags,
      excludeTags: fakeParams.excludeTags
    } as any)
    expect((response as GetCatalogCategoriesResponse).error)
      .toBe(true)
  })

  test('fetchFilteredMealsApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date,
        include_tags: fakeParams.includeTags,
        exclude_tags: fakeParams.excludeTags
      })
      .reply(500, {
        error: true
      })
    const response = await fetchFilteredMealsApi({
      date: fakeParams.date,
      includeTags: fakeParams.includeTags,
      excludeTags: fakeParams.excludeTags
    } as any)
    expect((response as GetCatalogCategoriesResponse).error)
      .toBe(true)
  })

  test('getOffDatesApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(200, {
        error: false
      })
    const response = await getOffDatesApi()
    expect((response as GetOffDates).error)
      .toBe(false)
  })

  test('getOffDatesApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(404, {
        error: true
      })
    const response = await getOffDatesApi()
    expect((response as GetOffDates).error)
      .toBe(true)
  })

  test('getOffDatesApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/off_dates')
      .reply(500, {
        error: true
      })
    const response = await getOffDatesApi()
    expect((response as GetOffDates).error)
      .toBe(true)
  })

  test('fetchMeals should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.meals.meals,
      tags: []
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date
      })
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(200, response)
      await runSaga(fakeStore, fetchMeals, { date: fakeParams.date, isBypassChecking:true } as any).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchMealsDone(response.data))
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingTagsDone(response.tags))
  })

  test('fetchMeals should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date
      })
      .reply(404, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(404, response)
    await runSaga(fakeStore, fetchMeals, { date: fakeParams.date, isBypassChecking:true } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchMealsFailed(response.message))
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingTagsFailed(response.message))
  })

  test('fetchMeals should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date
      })
      .reply(500, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/tag/list')
      .reply(500, response)
    await runSaga(fakeStore, fetchMeals, { date: fakeParams.date, isBypassChecking:true } as any).toPromise()
    // this part found error
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchMealsFailed(response.message))
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingTagsFailed(response.message))
  })

  test('fetchingMealDetails should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.meals.meals,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeParams.id,
        date: fakeParams.date,
      })
      .reply(200, response)
    await runSaga(fakeStore, fetchingMealDetails, { id: fakeParams.id, date: fakeParams.date } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingMealDetailsDone(response.data))
  })

  test('fetchingMealDetails should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeParams.id,
        date: fakeParams.date,
      })
      .reply(404, response)
    await runSaga(fakeStore, fetchingMealDetails, { id: fakeParams.id, date: fakeParams.date } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingMealDetailsFailed(response.message))
  })

  test('fetchingMealDetails should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error message'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeParams.id,
        date: fakeParams.date,
      })
      .reply(500, response)
    await runSaga(fakeStore, fetchingMealDetails, { id: fakeParams.id, date: fakeParams.date } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingMealDetailsFailed(response.message))
  })

  test('fetchFilteredMeals should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.meals.meals
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date,
        include_tags: fakeParams.includeTags,
        exclude_tags: fakeParams.excludeTags
      })
      .reply(200, response)
    await runSaga(fakeStore, fetchFilteredMeals, {
      date: fakeParams.date,
      includeTags: fakeParams.includeTags,
      excludeTags: fakeParams.excludeTags
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingFilteredMealsDone(response.data))
  })

  test('fetchFilteredMeals should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date,
        include_tags: fakeParams.includeTags,
        exclude_tags: fakeParams.excludeTags
      })
      .reply(404, response)
    await runSaga(fakeStore, fetchFilteredMeals, {
      date: fakeParams.date,
      includeTags: fakeParams.includeTags,
      excludeTags: fakeParams.excludeTags
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingFilteredMealsFailed(response.message))
  })

  test('fetchFilteredMeals should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error message'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/category/list')
      .query({
        date: fakeParams.date,
        include_tags: fakeParams.includeTags,
        exclude_tags: fakeParams.excludeTags
      })
      .reply(500, response)
    await runSaga(fakeStore, fetchFilteredMeals, {
      date: fakeParams.date,
      includeTags: fakeParams.includeTags,
      excludeTags: fakeParams.excludeTags
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingFilteredMealsFailed(response.message))
  })

})
