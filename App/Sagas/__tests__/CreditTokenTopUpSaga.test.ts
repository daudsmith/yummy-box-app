import {
  getToken,
  submitTokenApi,
  submitToken,
} from '../CreditTokenTopUpSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/CreditTokenTopUpRedux'
import fakeState from './fakeState'

const fakeCode = 'abcdef'

describe('CreditTokenTopUpSaga', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })

  test('submitTokenApi should return correct response on success', async (done) => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/wallet/redeem', { token: fakeCode })
      .reply(200, {
        error: false,
      })
    const response = await submitTokenApi({
      token,
      code: fakeCode,
    })
    expect(response.error)
      .toBe(false)
    done()
  })
  test('submitTokenApi should return correct response on page not found', async (done) => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/wallet/redeem', { token: fakeCode })
      .reply(404, {
        error: true,
      })
    const response = await submitTokenApi({
      token,
      code: fakeCode,
    })
    expect(response.error)
      .toBe(true)
    done()
  })
  test('submitTokenApi should return correct response on error', async (done) => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/wallet/redeem', { token: fakeCode })
      .reply(500, {
        error: true,
      })
    const response = await submitTokenApi({
      token,
      code: fakeCode,
    })
    expect(response.error)
      .toBe(true)
    done()
  })

  test('submitToken should execute redux action on success response', async (done) => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: 130000,
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/wallet/redeem', { token: fakeCode })
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/wallet')
      .reply(200, response)

    await runSaga(fakeStore, submitToken, { code: fakeCode } as any).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.submittingTokenDone(response.data))
    done()
  })
  test('submitToken should execute redux action on error 500 response', async (done) => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: 0,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/wallet/redeem', { token: fakeCode })
      .reply(500, response)
    await runSaga(fakeStore, submitToken, { code: fakeCode } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.submittingTokenFailed(response.message))
    done()
  })
  test('submitToken should execute redux action on error 404 response', async (done) => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: 0,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/wallet/redeem', { token: fakeCode })
      .reply(404, response)
    await runSaga(fakeStore, submitToken, { code: fakeCode } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.submittingTokenFailed(response.message))
    done()
  })
})
