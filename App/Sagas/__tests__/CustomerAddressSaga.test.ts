import {
  getToken,
  fetchAddressApi,
  deleteAddressApi,
  saveAddressApi,
  editAddressApi,
  fetchAddress,
  deleteAddress,
  saveAddress,
  editAddress,
} from '../CustomerAddressSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/CustomerAddressRedux'
import fakeState from './fakeState'
import { deleteAddressApiResponse } from '../../Services/ApiCustomer'

const fakeId = 2000
const fakeData: any = {
  latitude: 1,
  longitude: 1,
  addressLabel: 'Kantor',
  addressPinpoint: 'YummyCorp HQ',
  addressDetail: 'Jalan BSD',
  recipientName: 'Will Smith',
  recipientNumber: '628765786598',
  deliveryInstructions: '',
}
const fakePostData = {
  landmark: fakeData.addressPinpoint,
  address: fakeData.addressDetail,
  latitude: fakeData.latitude,
  longitude: fakeData.longitude,
  name: fakeData.recipientName,
  phone: fakeData.recipientNumber,
  label: fakeData.addressLabel,
  note: fakeData.deliveryInstructions,
}
describe('CustomerAddressSaga', () => {
  test('getToken should return correct token', () => {
    const { token } = getToken(fakeState)
    expect(token)
      .toBe(fakeState.login.token)
  })

  test('fetchAddressApi should return correct response on success', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address')
      .reply(200, {
        error: false,
      })
    const response = await fetchAddressApi(token)
    expect(response.error)
      .toBe(false)
  })
  test('fetchAddressApi should return correct response on page not found', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address')
      .reply(404, {
        error: true,
      })
    const response = await fetchAddressApi(token)
    expect(response.error)
      .toBe(true)
  })
  test('fetchAddressApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address')
      .reply(500, {
        error: true,
      })
    const response = await fetchAddressApi(token)
    expect(response.error)
      .toBe(true)
  })

  test('deleteAddressApi should return correct response on success', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/address/${fakeId}/destroy`)
      .reply(200, {
        error: false,
      })
    const response = await deleteAddressApi({
      addressId: fakeId,
      token,
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(false)
  })
  test('deleteAddressApi should return correct response on page not found', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/address/${fakeId}/destroy`)
      .reply(404, {
        error: true,
      })
    const response = await deleteAddressApi({
      addressId: fakeId,
      token,
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(true)
  })
  test('deleteAddressApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/address/${fakeId}/destroy`)
      .reply(500, {
        error: true,
      })
    const response = await deleteAddressApi({
      addressId: fakeId,
      token,
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(true)
  })

  test('saveAddressApi should return correct response on success', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/address/create', { ...fakePostData })
      .reply(200, {
        error: false,
      })
    const response = await saveAddressApi({
      token,
      address: { ...fakeData },
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(false)
  })
  test('saveAddressApi should return correct response on page not found', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/address/create', { ...fakePostData })
      .reply(404, {
        error: true,
      })
    const response = await saveAddressApi({
      token,
      address: { ...fakeData },
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(true)
  })
  test('saveAddressApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/address/create', { ...fakePostData })
      .reply(500, {
        error: true,
      })
    const response = await saveAddressApi({
      token,
      address: { ...fakeData },
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(true)
  })

  test('editAddressApi should return correct response on success', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post(`/public/account/address/${fakeId}/update`, { ...fakePostData })
      .reply(200, {
        error: false,
      })
    const response = await editAddressApi({
      token,
      addressId: fakeId,
      address: { ...fakeData },
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(false)
  })
  test('editAddressApi should return correct response on page not found', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post(`/public/account/address/${fakeId}/update`, { ...fakePostData })
      .reply(404, {
        error: true,
      })
    const response = await editAddressApi({
      token,
      addressId: fakeId,
      address: { ...fakeData },
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(true)
  })
  test('editAddressApi should return correct response on error', async () => {
    const { token } = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .post(`/public/account/address/${fakeId}/update`, { ...fakePostData })
      .reply(500, {
        error: true,
      })
    const response = await editAddressApi({
      token,
      addressId: fakeId,
      address: { ...fakeData },
    })
    expect((response as deleteAddressApiResponse).error)
      .toBe(true)
  })

  test('fetchAddress should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerAddress.addresses,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address')
      .reply(200, response)
    await runSaga(fakeStore, fetchAddress).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingAddressDone(response.data))
  })
  test('fetchAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address')
      .reply(500, response)
    await runSaga(fakeStore, fetchAddress).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingAddressFailed(response.message))
  })
  test('fetchAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/account/address')
      .reply(404, response)
    await runSaga(fakeStore, fetchAddress).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingAddressFailed(response.message))
  })

  test('deleteAddress should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerAddress.addresses,
    }
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/address/${fakeId}/destroy`)
      .reply(200, response)
    await runSaga(fakeStore, deleteAddress, { addressId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.deleteAddressDone(response.data))
  })
  test('deleteAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/address/${fakeId}/destroy`)
      .reply(500, response)
    await runSaga(fakeStore, deleteAddress, { addressId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.deleteAddressFailed(response.message))
  })
  test('deleteAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .delete(`/public/account/address/${fakeId}/destroy`)
      .reply(404, response)
    await runSaga(fakeStore, deleteAddress, { addressId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.deleteAddressFailed(response.message))
  })

  test('saveAddress should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerAddress.addresses,
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/address/create', { ...fakePostData })
      .reply(200, response)
    await runSaga(fakeStore, saveAddress, { address: fakeData }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.savingAddressDone(response.data))
  })
  test('saveAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/address/create', { ...fakePostData })
      .reply(500, response)
    await runSaga(fakeStore, saveAddress, { address: fakeData }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.savingAddressFailed(response.message))
  })
  test('saveAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post('/public/account/address/create', { ...fakePostData })
      .reply(404, response)
    await runSaga(fakeStore, saveAddress, { address: fakeData }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.savingAddressFailed(response.message))
  })

  test('editAddress should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerAddress.addresses,
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/account/address/${fakeId}/update`, { ...fakePostData })
      .reply(200, response)
    await runSaga(fakeStore, editAddress, { address: fakeData, addressId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.editAddressDone(response.data))
  })
  test('editAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/account/address/${fakeId}/update`, { ...fakePostData })
      .reply(500, response)
    await runSaga(fakeStore, editAddress, { address: fakeData, addressId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.editAddressFailed(response.message))
  })
  test('editAddress should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerAddress.addresses,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/account/address/${fakeId}/update`, { ...fakePostData })
      .reply(404, response)
    await runSaga(fakeStore, editAddress, { address: fakeData, addressId: fakeId }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.editAddressFailed(response.message))
  })
})
