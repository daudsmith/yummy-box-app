import {
  getMealDetailApi,
  fetchMealDetail,
} from '../MealDetailPopUpSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/MealDetailPopUpRedux'
import fakeState from './fakeState'
import { GetMealResponse } from '../../Services/ApiCatalog'

const fakeId = 1000
const fakeDate = '2019-09-09'
describe('MealDetailPopUpSaga', () => {
  test('getMealDetailApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeId,
        date: fakeDate,
      })
      .reply(200, {
        error: false,
      })
    const response = await getMealDetailApi({
      mealId: fakeId,
      date: fakeDate,
    })
    expect((response as GetMealResponse).error)
      .toBe(false)
  })
  test('getMealDetailApi should return correct response on page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeId,
        date: fakeDate,
      })
      .reply(404, {
        error: true,
      })
    const response = await getMealDetailApi({
      mealId: fakeId,
      date: fakeDate,
    })
    expect((response as GetMealResponse).error)
      .toBe(true)
  })
  test('getMealDetailApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeId,
        date: fakeDate,
      })
      .reply(500, {
        error: true,
      })
    const response = await getMealDetailApi({
      mealId: fakeId,
      date: fakeDate,
    })
    expect((response as GetMealResponse).error)
      .toBe(true)
  })

  test('fetchMealDetail should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: [
        {
          id: 1,
          address: 'Jalan maju',
        },
        {
          id: 2,
          address: 'Jalan mundur',
        },
      ],
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeId,
        date: fakeDate,
      })
      .reply(200, response)
    await runSaga(fakeStore, fetchMealDetail, {
      mealId: fakeId,
      date: fakeDate,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchMealDetailSuccess({
        ...response.data,
        date: fakeDate,
      } as any))
  })
  test('fetchMealDetail should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: [
        {
          id: 1,
          address: 'Jalan maju',
        },
        {
          id: 2,
          address: 'Jalan mundur',
        },
      ],
      message: 'Response Error',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeId,
        date: fakeDate,
      })
      .reply(500, response)
    await runSaga(fakeStore, fetchMealDetail, {
      mealId: fakeId,
      date: fakeDate,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchMealDetailFailed(response.message))
  })
  test('fetchMealDetail should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: [
        {
          id: 1,
          address: 'Jalan maju',
        },
        {
          id: 2,
          address: 'Jalan mundur',
        },
      ],
      message: 'Page Not Found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/catalog/product/detail')
      .query({
        product_id: fakeId,
        date: fakeDate,
      })
      .reply(404, response)
    await runSaga(fakeStore, fetchMealDetail, {
      mealId: fakeId,
      date: fakeDate,
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.fetchMealDetailFailed(response.message))
  })
})
