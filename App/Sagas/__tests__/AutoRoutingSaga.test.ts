import {
  addRoute,
} from '../AutoRoutingSaga'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/AutoRoutingRedux'

describe('AutoRoutingSaga', () => {
  test('addRoute should execute redux action on error response', async () => {
    let params: any = {
      routeName: 'TestScreen',
      params: { id: 1, text: 'Text' },
      isOpeningApp: true,
    }
    let dispatchedAction = []
    const fakeStore = {
      dispatch: action => dispatchedAction.push(action),
    }
    await runSaga(fakeStore, addRoute, params).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.setRoute(params.routeName, params.params))
  })
})
