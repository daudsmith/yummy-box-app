import {
  fetchSettingApi,
  fetching,
} from '../SettingSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/SettingRedux'
import { getBulkSettingResponse } from '../../Services/ApiSetting'

const fakeKey = [
  'key1',
  'key2',
  'key3',
]
describe('SettingSaga', () => {
  test('fetchSettingApi should return correct response on success', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/bulk')
      .query({
        keys: JSON.stringify(fakeKey),
      })
      .reply(200, { error: false })
    const response = await fetchSettingApi(fakeKey)
    expect((response as getBulkSettingResponse).error)
      .toBe(false)
  })
  test('fetchSettingApi should return correct response on page not found', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/bulk')
      .query({
        keys: JSON.stringify(fakeKey),
      })
      .reply(404, { error: true })
    const response = await fetchSettingApi(fakeKey)
    expect((response as getBulkSettingResponse).error)
      .toBe(true)
  })
  test('fetchSettingApi should return correct response on error', async () => {
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/bulk')
      .query({
        keys: JSON.stringify(fakeKey),
      })
      .reply(500, { error: true })
    const response = await fetchSettingApi(fakeKey)
    expect((response as getBulkSettingResponse).error)
      .toBe(true)
  })

  test('fetching should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: [
        { setting1: 'setting1' },
        { setting2: 'setting2' },
      ],
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/setting/get/bulk')
      .query({
        keys: JSON.stringify(fakeKey),
      })
      .reply(200, response)
    await runSaga(fakeStore, fetching, fakeKey).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateStatus(true))
  })
})
