import {
  getToken,
  getModifiedMMP,
  getThemeMeals,
  getMMPType,
  buildNewPackageSelectedDate,
  buildModifyMMPBody,
  getMyMealPlansApi,
  getMyMealPlans,
  getMyMealPlanDetailApi,
  getMyMealPlanDetail,
  cancelOrderApi,
  skipDelivery,
  cancelOrder,
  modifyMmpPackageDelivery,
  modifyMmpSubscriptionSelectedPayment,
  modifyMmpSubscriptionPaymentInfo,
  modifyMmpPackageDeliveryMeal,
  modifyMmp,
  updateMyMealPlanWithPayment,
} from '../MyMealPlanSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/MyMealPlanRedux'
import fakeState from './fakeState'
import {
  mmpDetailResponse,
  mmpResponse,
  subscriptionModifyResponse,
} from '../../Services/ApiMyMealPlan'

const fakeId = 1
const fakeOrderType = 'item'

interface getMyMealPlanDetailResponse {
  error: boolean
  data?: any
  payment_info?: any
  action?: any
  package?: any
}

describe('MyMealPlanSaga', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })
  test('getModifiedMMP should return correct modified data', () => {
    const response = getModifiedMMP(fakeState)
    expect(response)
      .toBe(fakeState.myMealPlan.modifiedMyMealPlanDetail)
  })
  test('getThemeMeals should return correct meals data', () => {
    const response = getThemeMeals(fakeState)
    expect(response)
      .toBe(fakeState.theme.meals)
  })
  test('getMMPType should return correct meal plan type', () => {
    const response = getMMPType(fakeState)
    expect(response)
      .toBe(fakeState.myMealPlan.myMealPlanDetail.type)
  })
  test('buildNewPackageSelectedDate should return correct new ordered dates', () => {
    const response = buildNewPackageSelectedDate('2019-09-09', '2019-09-10', [
      '2019-09-08',
      '2019-09-10',
      '2019-09-07',
    ])
    expect(response)
      .toEqual([
        '2019-09-07',
        '2019-09-08',
        '2019-09-09',
      ])
  })
  test('buildModifyMMPBody should return correct non subscription formatted data', () => {
    const response = buildModifyMMPBody(fakeState.myMealPlan.myMealPlanDetail, null, 'wallet')
    const expectedData = {
      address: fakeState.myMealPlan.myMealPlanDetail.address,
      latitude: fakeState.myMealPlan.myMealPlanDetail.latitude,
      longitude: fakeState.myMealPlan.myMealPlanDetail.longitude,
      landmark: fakeState.myMealPlan.myMealPlanDetail.landmark,
      phone: fakeState.myMealPlan.myMealPlanDetail.phone,
      name: fakeState.myMealPlan.myMealPlanDetail.name,
      date: '2019-09-19',
      time: fakeState.myMealPlan.myMealPlanDetail.time,
      note: fakeState.myMealPlan.myMealPlanDetail.note,
      items: fakeState.myMealPlan.myMealPlanDetail.items.data
    }
    expect(response)
      .toEqual(expectedData)
  })
  test('buildModifyMMPBody should return correct subscription formatted data', () => {
    fakeState.myMealPlan.myMealPlanDetail.type = 'subscription'
    const paymentInfo = { payment_info_json: { wallet: 120000 } }
    const response = buildModifyMMPBody(fakeState.myMealPlan.myMealPlanDetail, paymentInfo, 'wallet')
    const expectedData = {
      delivery_time: fakeState.myMealPlan.myMealPlanDetail.time,
      delivery_address: fakeState.myMealPlan.myMealPlanDetail.address,
      delivery_note: fakeState.myMealPlan.myMealPlanDetail.note,
      delivery_longitude: fakeState.myMealPlan.myMealPlanDetail.longitude,
      delivery_latitude: fakeState.myMealPlan.myMealPlanDetail.latitude,
      landmark: fakeState.myMealPlan.myMealPlanDetail.landmark,
      delivery_details: '',
      recipient_phone: fakeState.myMealPlan.myMealPlanDetail.phone,
      recipient_name: fakeState.myMealPlan.myMealPlanDetail.name,
      payment_method: 'wallet',
      payment_info: paymentInfo.payment_info_json,
    }
    fakeState.myMealPlan.myMealPlanDetail.type = 'item'
    expect(response)
      .toEqual(expectedData)
  })

  test('getMyMealPlansApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(200, { error: false })
    const response = await getMyMealPlansApi(token)
    expect((response as mmpResponse).error)
      .toBe(false)
  })
  test('getMyMealPlansApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(404, { error: true })
    const response = await getMyMealPlansApi(token)
    expect((response as mmpResponse).error)
      .toBe(true)
  })
  test('getMyMealPlansApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(500, { error: true })
    const response = await getMyMealPlansApi(token)
    expect((response as mmpResponse).error)
      .toBe(true)
  })

  test('getMyMealPlans should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.myMealPlan.myMealPlans,
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(200, response)
    await runSaga(fakeStore, getMyMealPlans).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.getMyMealPlansSuccess(response.data))
  })
  test('getMyMealPlans should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(500, response)
    await runSaga(fakeStore, getMyMealPlans).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.getMyMealPlansFailed(response.message))
  })
  test('getMyMealPlans should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page Not Found',
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(404, response)
    await runSaga(fakeStore, getMyMealPlans).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.getMyMealPlansFailed(response.message))
  })

  test('getMyMealPlanDetailApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({
        type: fakeOrderType,
      })
      .reply(200, { error: false })
    const response = await getMyMealPlanDetailApi({
      token,
      id: fakeId,
      orderType: fakeOrderType,
    })
    expect((response as mmpDetailResponse).error)
      .toBe(false)
  })
  test('getMyMealPlanDetailApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({
        type: fakeOrderType,
      })
      .reply(404, { error: true })
    const response = await getMyMealPlanDetailApi({
      token,
      id: fakeId,
      orderType: fakeOrderType,
    })
    expect((response as mmpDetailResponse).error)
      .toBe(true)
  })
  test('getMyMealPlanDetailApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({
        type: fakeOrderType,
      })
      .reply(500, { error: true })
    const response = await getMyMealPlanDetailApi({
      token,
      id: fakeId,
      orderType: fakeOrderType,
    })
    expect((response as mmpDetailResponse).error)
      .toBe(true)
  })

  test('getMyMealPlanDetail should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    const response: getMyMealPlanDetailResponse = {
      error: false,
      data: [
        {
          id: 1,
          menu: 'Menu 1',
        },
        {
          id: 2,
          menu: 'Menu 2',
        },
      ],
      payment_info: {
        info: {
          wallet: 120000,
        },
        method: 'wallet',
      },
      action: '',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({
        type: fakeOrderType,
      })
      .reply(200, response)
    await runSaga(fakeStore, getMyMealPlanDetail, {
      id: fakeId,
      orderType: fakeOrderType,
    }).toPromise()
    const myMealPlanDetail = {
      data: response.data,
      payment_info: response.hasOwnProperty('payment_info')
        ? response.payment_info
        : null,
      selectedPayment: response.hasOwnProperty('payment_info')
        ? response.payment_info.method
        : null,
      type: fakeOrderType,
      actions: response.hasOwnProperty('action')
        ? response.action
        : null,
      myMealPlanPackage: response.hasOwnProperty('package')
        ? response.package
        : null,
    }
    expect(dispatchedAction)
      .toContainEqual(Actions.getMyMealPlanDetailSuccess(myMealPlanDetail))
  })
  test('getMyMealPlanDetail should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Response Error',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({
        type: fakeOrderType,
      })
      .reply(500, response)
    await runSaga(fakeStore, getMyMealPlanDetail, {
      id: fakeId,
      orderType: fakeOrderType,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.getMyMealPlanDetailFailed(response.message))
  })
  test('getMyMealPlanDetail should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ login: { token: '12345' } }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({
        type: fakeOrderType,
      })
      .reply(404, response)
    await runSaga(fakeStore, getMyMealPlanDetail, {
      id: fakeId,
      orderType: fakeOrderType,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.getMyMealPlanDetailFailed(response.message))
  })

  test('cancelOrderApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(200, { error: false })
    const response = await cancelOrderApi({
      token,
      id: fakeId,
    })
    expect((response as mmpDetailResponse).error)
      .toBe(false)
  })
  test('cancelOrderApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(404, { error: true })
    const response = await cancelOrderApi({
      token,
      id: fakeId,
    })
    expect((response as mmpDetailResponse).error)
      .toBe(true)
  })
  test('cancelOrderApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(500, { error: true })
    const response = await cancelOrderApi({
      token,
      id: fakeId,
    })
    expect((response as mmpDetailResponse).error)
      .toBe(true)
  })
  test('skipDelivery should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/schedule/${fakeId}/skip`)
      .reply(200, { error: false })
    const response = await skipDelivery({
      token,
      id: fakeId,
    })
    expect((response as subscriptionModifyResponse).error)
      .toBe(false)
  })
  test('skipDelivery should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/schedule/${fakeId}/skip`)
      .reply(404, { error: true })
    const response = await skipDelivery({
      token,
      id: fakeId,
    })
    expect((response as subscriptionModifyResponse).error)
      .toBe(true)
  })
  test('skipDelivery should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/subscription/schedule/${fakeId}/skip`)
      .reply(500, { error: true })
    const response = await skipDelivery({
      token,
      id: fakeId,
    })
    expect((response as subscriptionModifyResponse).error)
      .toBe(true)
  })

  test('cancelOrder should execute cancel order redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({ type: fakeOrderType })
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(200, response)
    await runSaga(fakeStore, cancelOrder, {
      id: fakeId,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.cancelOrderSuccess())
  })

  test('cancelOrder should execute skip order redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
    }
    const responseDetail = {
      error: false,
      data: [
        {
          id: 1,
          menu: 'Menu 1',
        },
        {
          id: 2,
          menu: 'Menu 2',
        },
      ],
      payment_info: {
        info: {
          wallet: 120000,
        },
        method: 'wallet',
      },
      action: '',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(200, response)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({type: fakeOrderType})
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(200, responseDetail)

    await runSaga(fakeStore, cancelOrder, {
      id: fakeId,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.cancelOrderSuccess())
  })
  test('cancelOrder should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Response Error',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(500, response)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({type: fakeOrderType})
      .reply(500, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(500, response)

    await runSaga(fakeStore, cancelOrder, {
      id: fakeId,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.cancelOrderFailed(response.message))
  })
  test('cancelOrder should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/cancel/${fakeId}`)
      .reply(404, response)
    nock(AppConfig.apiBaseUrl)
      .get(`/public/sales/order/delivery/detail/${fakeId}`)
      .query({type: fakeOrderType})
      .reply(404, response)
    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(404, response)
    await runSaga(fakeStore, cancelOrder, {
      id: fakeId,
    }).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.cancelOrderFailed(response.message))
  })
  test('modifyMmpPackageDelivery should return correct mmp detail delivery data', () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    runSaga(fakeStore, modifyMmpPackageDelivery, {
      field: 'address',
      value: 'Jalan baru',
    } as any).toPromise()
    expect(dispatchedAction[0])
      .toMatchObject({
        modifiedMyMealPlanDetail: {
          data: {
            address: 'Jalan baru',
          },
        },
      })
  })
  test('modifyMmpPackageDelivery should return correct mmp detail delivery data on only value param set', () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    runSaga(fakeStore, modifyMmpPackageDelivery, {
      value: {
        name: 'tarjo',
      },
      field: true,
    } as any).toPromise()
    expect(dispatchedAction[0])
      .toMatchObject({
        modifiedMyMealPlanDetail: {
          data: {
            name: 'tarjo',
          },
        },
      })
  })
  test('modifyMmpSubscriptionSelectedPayment should return correct mmp detail on update payment', () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    runSaga(fakeStore, modifyMmpSubscriptionSelectedPayment, {
      selectedPayment: 'wallet',
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateModifiedMmp({
        ...fakeState.myMealPlan.modifiedMyMealPlanDetail,
        selectedPayment: 'wallet',
      }))
  })
  test('modifyMmpSubscriptionPaymentInfo should return correct mmp detail on update paymentInfo', () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    runSaga(fakeStore, modifyMmpSubscriptionPaymentInfo, {
      paymentInfo: {
        info: {
          method: 'wallet',
          balance: 1200000,
        },
      },
    } as any).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.updateModifiedMmp({
        ...fakeState.myMealPlan.modifiedMyMealPlanDetail,
        payment_info: {
          info: {
            method: 'wallet',
            balance: 1200000,
          },
        },
      } as any))
  })
  test('modifyMmpPackageDeliveryMeal should return correct mmp detail delivery data on update selected date', () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    runSaga(fakeStore, modifyMmpPackageDeliveryMeal, {
      selectedDate: '2019-09-10',
      currentDate: '2019-09-09',
    } as any).toPromise()
    expect(dispatchedAction[0])
      .toMatchObject({
        modifiedMyMealPlanDetail: {
          data: {
            date: '2019-09-10',
          },
        },
      })
  })
  test('modifyMmp should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      ...fakeState.myMealPlan.modifiedMyMealPlanDetail,
      error: false,
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(200, response)
    await runSaga(fakeStore, modifyMmp).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifyMmpSuccess())
  })
  test('modifyMmp should execute redux action on page not found response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`)
      .reply(404, response)
    await runSaga(fakeStore, modifyMmp).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifyMmpFailed(response.message))
  })
  test('modifyMmp should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`)
      .reply(500, response)
    await runSaga(fakeStore, modifyMmp).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifyMmpFailed(response.message))
  })
  test('updateMyMealPlanWithPayment should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      ...fakeState.myMealPlan.modifiedMyMealPlanDetail,
      error: false,
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`)
      .reply(200, response)

    nock(AppConfig.apiBaseUrl)
      .get('/public/sales/order/delivery/list')
      .reply(200, response)
    await runSaga(fakeStore, updateMyMealPlanWithPayment).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifyMmpSuccess())
  })
  test('updateMyMealPlanWithPayment should execute redux action on page not found response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Page not found',
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`)
      .reply(404, response)
    await runSaga(fakeStore, updateMyMealPlanWithPayment).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifyMmpFailed(response.message))
  })
  test('updateMyMealPlanWithPayment should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => ({ ...fakeState }),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      message: 'Error Response',
    }
    nock(AppConfig.apiBaseUrl)
      .post(`/public/sales/order/delivery/update/${fakeId}`)
      .reply(500, response)
    await runSaga(fakeStore, updateMyMealPlanWithPayment).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.modifyMmpFailed(response.message))
  })
})
