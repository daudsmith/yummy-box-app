import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { getAvailableDates, getExtendedAvailableDates, getCurrentMeals, getThemeDetailApi, getThemeDetailMealsApi, getThemePackagesApi, getAvailableDateApi, getAvailableDateExtendsApi, getMealByDatesApi, fetchDetail, fetchDetailItems, fetchThemePackages, fetchAvailableDate, fetchAvailableDateExtends } from '../ThemeSaga'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/ThemeRedux'
import fakeState from './fakeState'

const fakeDate = '2014-10-10'
describe('getState function', () => {
  test('getAvailableDates should return initialAvailableDateSelectedTheme ', () => {
    expect(getAvailableDates(fakeState)).toBe(fakeState.theme.initialAvailableDateSelectedTheme)
  })

  test('getCurrentMeals should return initialAvailableDateSelectedTheme ', () => {
    expect(getCurrentMeals(fakeState)).toBe(fakeState.theme.meals)
  })
  test('getExtendedAvailableDates should return initialAvailableDateSelectedTheme ', () => {
    expect(getExtendedAvailableDates(fakeState)).toBe(fakeState.theme.availableDateSelectedTheme)
  })

})

describe('api function', () => {
  const fakeId = 1
  let fakeRes = {
    data: true,
  }
  // getThemeDetail
  test('getThemeDetailApi should return correct response on success', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(200, fakeRes)

    const response = await getThemeDetailApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })

  test('getThemeDetailApi should return correct response on error', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(500, fakeRes)

    const response = await getThemeDetailApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })
  test('getThemeDetailApi should return correct response on not found', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(404, fakeRes)

    const response = await getThemeDetailApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })

  // getDetailMeals
  test('getThemeDetailMealsApi should return correct response on success', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(200, fakeRes)

    const response = await getThemeDetailMealsApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })

  test('getThemeDetailMealsApi should return correct response on error', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(500, fakeRes)

    const response = await getThemeDetailMealsApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })
  test('getThemeDetailMealsApi should return correct response on not found', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(404, fakeRes)

    const response = await getThemeDetailMealsApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })

  // getThemePackagesApi
  test('getThemePackagesApi should return correct response on success', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/packages`)
      .reply(200, fakeRes)

    const response = await getThemePackagesApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })

  test('getThemePackagesApi should return correct response on error', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/packages`)
      .reply(500, fakeRes)

    const response = await getThemePackagesApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })
  test('getThemePackagesApi should return correct response on not found', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/packages`)
      .reply(404, fakeRes)

    const response = await getThemePackagesApi(fakeId)

    expect(response.data)
      .toEqual(true)
  })



  // getAvailableDatesApi
  test('getAvailableDatesApi should return correct response on success', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available`)
      .reply(200, fakeRes)

    const response = await getAvailableDateApi({themeId: fakeId} as any)

    expect(response.data)
      .toEqual(true)
  })

  test('getAvailableDatesApi should return correct response on error', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available`)
      .reply(500, fakeRes)

    const response = await getAvailableDateApi({themeId: fakeId} as any)

    expect(response.data)
      .toEqual(true)
  })
  test('getAvailableDatesApi should return correct response on not found', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available`)
      .reply(404, fakeRes)

    const response = await getAvailableDateApi({themeId: fakeId} as any)

    expect(response.data)
      .toEqual(true)
  })


  // getAvailableDateExtendsApi
  test('getAvailableDateExtendsApi should return correct response on success', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available?start_from=${fakeDate}`)
      .reply(200, fakeRes)

    const response = await getAvailableDateExtendsApi(fakeId, fakeDate)

    expect(response.data)
      .toEqual(true)
  })

  test('getAvailableDateExtendsApi should return correct response on error', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available?start_from=${fakeDate}`)
      .reply(500, fakeRes)

    const response = await getAvailableDateExtendsApi(fakeId, fakeDate)

    expect(response.data)
      .toEqual(true)
  })
  test('getAvailableDateExtendsApi should return correct response on not found', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available?start_from=${fakeDate}`)
      .reply(404, fakeRes)

    const response = await getAvailableDateExtendsApi(fakeId, fakeDate)

    expect(response.data)
      .toEqual(true)
  })


  // getMealByDatesApi
  test('getMealByDatesApi should return correct response on success', async () => {

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meals_by_dates?dates[]=${fakeDate}`)
      .reply(200, fakeRes)

    const response = await getMealByDatesApi({ themeId: fakeId, availableDates: [fakeDate] })

    expect(response.data)
      .toEqual(true)
  })

})

describe('generator function', () => {
  const fakeId = 1
  const fakeRes: any = {
    data: fakeState.theme.themeDetail,
  }

  test('fetchDetail should execute redux action on success', async () => {
    const fakeSagaParam: any = {
      id: fakeId
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(200, fakeRes)

    await runSaga(fakeStore, fetchDetail, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingDetail())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchDetailSuccess(fakeState.theme.themeDetail))

  })

  test('fetchDetail should execute redux action on error', async () => {
    const fakeSagaParam: any = {
      id: fakeId
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/detail`)
      .reply(200, {
        error: true,
        message: 'error'
      })

    await runSaga(fakeStore, fetchDetail, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingDetail())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchDetailFailed('error'))

  })



  test('fetchDetailItems should execute redux action on success', async () => {
    const fakeSagaParam: any = {
      id: fakeId
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(200, {
        data: []
      })

    await runSaga(fakeStore, fetchDetailItems, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingDetailItems())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchDetailItemsSuccess([]))

  })

  test('fetchDetailItems should execute redux action on error', async () => {
    const fakeSagaParam: any = {
      id: fakeId
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/meal/list`)
      .reply(200, {
        error: true,
        message: 'error'
      })

    await runSaga(fakeStore, fetchDetailItems, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingDetailItems())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchDetailItemsFailed('error'))

  })


  test('fetchThemePackages should execute redux action on success', async () => {
    const fakeSagaParam: any = {
      id: fakeId
    }
    fakeRes.data = fakeState.theme.themePackages

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/packages`)
      .reply(200, fakeRes)

    await runSaga(fakeStore, fetchThemePackages, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingPackages())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchPackagesSuccess(fakeState.theme.themePackages))

  })

  test('fetchThemePackages should execute redux action on error', async () => {
    const fakeSagaParam: any = {
      id: fakeId
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/packages`)
      .reply(200, {
        error: true,
        message: 'error'
      })

    await runSaga(fakeStore, fetchThemePackages, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingPackages())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchPackagesFailed('error'))

  })

  test('fetchAvailableDate should execute redux action on error', async () => {
    const fakeSagaParam: any = {
      themeId: fakeId
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available`)
      .reply(200, {
        error: true,
        message: 'error'
      })

    await runSaga(fakeStore, fetchAvailableDate, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingAvailableDate())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingAvailableDateFailed('error'))

  })

  test('fetchAvailableDateExtends should execute redux action on error', async () => {
    const fakeSagaParam: any = {
      themeId: fakeId,
      startDate: fakeDate
    }

    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }

    nock(AppConfig.apiBaseUrl)
      .get(`/public/catalog/theme/${fakeId}/available?start_from=${fakeDate}`)
      .reply(200, {
        error: true,
        message: 'error'
      })

    await runSaga(fakeStore, fetchAvailableDateExtends, fakeSagaParam).toPromise()

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingAvailableDateExtends())

    expect(dispatchedAction)
      .toContainEqual(Actions.fetchingAvailableDateExtendsFailed('error'))

  })
})
