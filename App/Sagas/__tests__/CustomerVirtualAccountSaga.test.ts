import {
  getToken,
  fetchVirtualAccount,
  fetchingVirtualAccountApi,
} from '../CustomerVirtualAccountSaga'
import nock from 'nock'
import AppConfig from '../../Config/AppConfig'
import { runSaga } from 'redux-saga'
import Actions from '../../Redux/CustomerVirtualAccountRedux'
import fakeState from './fakeState'
import { fetchVirtualAccountApiResponse } from '../../Services/ApiCustomer'

describe('CustomerVirtualAccountSaga', () => {
  test('getToken should return correct token', () => {
    const response = getToken(fakeState)
    expect(response)
      .toBe(fakeState.login.token)
  })

  test('fetchingVirtualAccountApi should return correct response on success', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/payment/virtual_account')
      .reply(200, {
        error: false,
      })
    const response = await fetchingVirtualAccountApi(token)
    expect((response as fetchVirtualAccountApiResponse).error)
      .toBe(false)
  })
  test('fetchingVirtualAccountApi should return correct response on page not found', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/payment/virtual_account')
      .reply(404, {
        error: true,
      })
    const response = await fetchingVirtualAccountApi(token)
    expect((response as fetchVirtualAccountApiResponse).error)
      .toBe(true)
  })
  test('fetchingVirtualAccountApi should return correct response on error', async () => {
    const token = getToken(fakeState)
    nock(AppConfig.apiBaseUrl)
      .get('/public/payment/virtual_account')
      .reply(500, {
        error: true,
      })
    const response = await fetchingVirtualAccountApi(token)
    expect((response as fetchVirtualAccountApiResponse).error)
      .toBe(true)
  })

  test('fetchVirtualAccount should execute redux action on success response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: false,
      data: fakeState.customerVirtualAccount.list
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/payment/virtual_account')
      .reply(200, response)
    await runSaga(fakeStore, fetchVirtualAccount).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.virtualAccountFetchingSuccess(response.data))
  })
  test('fetchVirtualAccount should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerVirtualAccount.list,
      message: 'Error Response'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/payment/virtual_account')
      .reply(500, response)
    await runSaga(fakeStore, fetchVirtualAccount).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.virtualAccountFetchingFailed(response.message))
  })
  test('fetchVirtualAccount should execute redux action on error response', async () => {
    let dispatchedAction = []
    const fakeStore = {
      getState: () => (fakeState),
      dispatch: action => dispatchedAction.push(action),
    }
    const response = {
      error: true,
      data: fakeState.customerVirtualAccount.list,
      message: 'Page Not Found'
    }
    nock(AppConfig.apiBaseUrl)
      .get('/public/payment/virtual_account')
      .reply(404, response)
    await runSaga(fakeStore, fetchVirtualAccount).toPromise()
    expect(dispatchedAction)
      .toContainEqual(Actions.virtualAccountFetchingFailed(response.message))
  })
})
