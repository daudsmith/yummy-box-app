import { Alert } from 'react-native'
import { call, put, delay, select } from 'redux-saga/effects'
import actions, { Children, CachingData } from '../Redux/MealsRedux'
import Api, {
  GetCatalogCategoriesResponse,
  GetMealResponse,
  GetOffDates,
  GetTagListResponse,
} from '../Services/ApiCatalog'
import { ApiResponse } from 'apisauce'

interface FetchParams {
  id: number
  date: string
}

export function fetchingMealsApi(date: string, kitchenCode: string) {
  return Api.create()
    .getCatalogCategories(date, kitchenCode)
    .then((response: ApiResponse<GetCatalogCategoriesResponse>) => response.data)
    .catch((error: ApiResponse<GetCatalogCategoriesResponse>) => error)
}

export function fetchMealDetailsApi(params: FetchParams) {
  const { id, date } = params
  return Api.create()
    .getMealDetails(id, date)
    .then((response: ApiResponse<GetMealResponse>) => response.data)
    .catch((error: ApiResponse<GetMealResponse>) => error)
}

export function fetchTagListApi() {
  return Api.create()
    .getTagList()
    .then((response: ApiResponse<GetTagListResponse>) => response.data)
    .catch((error: ApiResponse<GetTagListResponse>) => error)
}

export function fetchFilteredMealsApi({ date, includeTags, excludeTags }: { date: string, includeTags: boolean, excludeTags: boolean }) {
  return Api.create()
    .getFilteredMeals(date, includeTags, excludeTags)
    .then((response: ApiResponse<GetCatalogCategoriesResponse>) => response.data)
    .catch((error: ApiResponse<GetCatalogCategoriesResponse>) => error)
}

export function getOffDatesApi() {
  return Api.create()
    .getOffDates()
    .then((response: ApiResponse<GetOffDates>) => response.data)
    .catch((error: ApiResponse<GetOffDates>) => error)
}

export function* fetchMeals({ date, kitchenCode, isBypassChecking }: ReturnType<typeof actions.fetchMeals>) {
  let cachingData: CachingData[] = []
 
  if (!isBypassChecking) {
    cachingData = yield select((state) => state?.meals?.cachingData ?? [])
  }
  
  yield put(actions.fetchingMeals())

  if (
    cachingData &&
    cachingData.length > 0 &&
    cachingData.findIndex((value: CachingData) => value.date === date) != -1 &&
    kitchenCode
  ) {
    const dataCaching: CachingData = cachingData.find((value: CachingData) => value.date === date)

    let stock = dataCaching.meals.map((meal) => {
      return meal.items.data.map((item) => {
        return {
          item_id: item.id,
          quantity: item.daily_remaining_quantity
        }
      })
    })

    yield put(actions.setItemStock(stock.reduce((a, b) => a.concat(b), [])))
    yield put(actions.fetchMealsDone(dataCaching.meals))
    yield put(actions.fetchingTagsDone(dataCaching.tags))

  } else {
    const meals: GetCatalogCategoriesResponse = yield call(fetchingMealsApi, date, kitchenCode)
    if (meals !== null && !meals.error) {

      let stock = meals.data.map((meal) => {
        return meal.items.data.map((item) => {
          return {
            item_id: item.id,
            quantity: item.daily_remaining_quantity
          }
        })
      })

      yield put(actions.setItemStock(stock.reduce((a, b) => a.concat(b), [])))
      yield put(actions.fetchMealsDone(meals.data))

    } else {
      const error = meals !== null ? meals.message : 'Cannot fetch meals'
      yield put(actions.fetchMealsFailed(error))
      Alert.alert(
        'Error',
        error
      )
    }

    yield put(actions.fetchingTags())
    const tagsResponse: GetTagListResponse = yield call(fetchTagListApi)

    if (tagsResponse !== null && !tagsResponse.error) {
      const tags = tagsResponse.data
        .filter((tag) => tag.filterable === 1)
        .map((tag) => {
          if (tag.hasOwnProperty('children')) {
            const children = tag.children.data
              .filter((children: Children) => children.filterable === 1)
              .map((children: Children) => {
                return { ...children, isSelected: false }
              })
            if (children.length > 0) {
              return { ...tag, children: children, isSelected: false }
            } else {
              delete tag.children
              return { ...tag, isSelected: false }
            }
          } else {
            return { ...tag, isSelected: false }
          }
        })
      if (meals !== null && !meals.error) {
        const cachingDataResponse: CachingData = {
          date: date,
          meals: meals.data,
          tags: tags,
        }

        const arrCachingData: CachingData[] = [
          cachingDataResponse,
          ...cachingData,
        ]

        yield put(actions.cachingDataDone(arrCachingData))
      }
      yield put(actions.fetchingTagsDone(tags))
    } else {
      const error = tagsResponse !== null ? tagsResponse.message : 'Cannot fetching tags'
      yield put(actions.fetchingTagsFailed(error))
    }
  }
}

export function* fetchingMealDetails({ id, date }: ReturnType<typeof actions.fetchingMealDetails>) {
  yield put(actions.fetchingMealDetails())
  const meal: GetMealResponse = yield call(fetchMealDetailsApi, { id: id, date: date })
  if (meal !== null && !meal.error) {
    yield put(actions.fetchingMealDetailsDone(meal.data))
  } else {
    const error = meal !== null ? meal.message : 'Cannot fetch meal details'
    yield put(actions.fetchingMealDetailsFailed(error))
    Alert.alert(
      'Error',
      error
    )
  }
}

export function* fetchFilteredMeals({ date, includeTags, excludeTags }: ReturnType<typeof actions.fetchFilteredMeals>) {
  yield put(actions.fetchingFilteredMeals())
  const meals: GetCatalogCategoriesResponse = yield call(fetchFilteredMealsApi, { date, includeTags, excludeTags })
  yield delay(200)
  if (meals !== null && !meals.error) {
    yield put(actions.fetchingFilteredMealsDone(meals.data))
  } else {
    const error = meals !== null ? meals.message : 'Cannot fetch filtered meals'
    yield put(actions.fetchingFilteredMealsFailed(error))
  }
}
