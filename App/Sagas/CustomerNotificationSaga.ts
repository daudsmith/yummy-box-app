import {
  call,
  put,
  select,
} from 'redux-saga/effects'
import ApiCustomerNotification, { GetNotifications, GetTotalUnread, DeleteAllNotifications } from '../Services/ApiCustomerNotification'
import actions from '../Redux/CustomerNotificationRedux'
import { AppState } from '../Redux/CreateStore'
import { ApiResponse } from 'apisauce'

export const getToken = (state: AppState) => state.login.token

export function fetchUnreadNotificationsApi(token: string) {
  return ApiCustomerNotification.create()
    .getUnread(token)
    .then((response: ApiResponse<GetNotifications>) => response.data)
    .catch((error: ApiResponse<GetNotifications>) => error)
}

export function fetchTotalUnreadNotificationsApi(token: string) {
  return ApiCustomerNotification.create()
    .getTotalUnread(token)
    .then((response: ApiResponse<GetTotalUnread>) => response.data)
    .catch((error: ApiResponse<GetTotalUnread>) => error)
}

export function fetchNotificationListApi(token: string) {
  return ApiCustomerNotification.create()
    .getNotifications(token)
    .then((response: ApiResponse<GetNotifications>) => response.data)
    .catch((error: ApiResponse<GetNotifications>) => error)
}

export function markNotificationsToReadApi(token: string) {
  return ApiCustomerNotification.create()
    .markNotificationRead(token)
    .then((response: ApiResponse<GetNotifications>) => response.data)
    .catch((error: ApiResponse<GetNotifications>) => error)
}

export function notificationGetReadByidApi(token, notificationId) {
  return ApiCustomerNotification.create()
    .getReadByid(token, notificationId)
    .then((response: ApiResponse<GetNotifications>) => response.data)
    .catch((error: ApiResponse<GetNotifications>) => error)
}

export function notificationGetUnreadByidApi(token, notificationId) {
  return ApiCustomerNotification.create()
    .getUnreadByid(token, notificationId)
    .then((response: ApiResponse<GetNotifications>) => response.data)
    .catch((error: ApiResponse<GetNotifications>) => error)
}

export function deleteSingleNotificationAPI(token: string, notificationId: number) {
  return ApiCustomerNotification.create()
    .deleteSingleNotification(token, notificationId)
    .then((response: ApiResponse<GetNotifications>) => response.data)
    .catch((error: ApiResponse<GetNotifications>) => error)
}

export function deleteAllNotificationsAPI(token: string) {
  return ApiCustomerNotification.create()
    .deleteAllNotifications(token)
    .then((response: ApiResponse<DeleteAllNotifications>) => response.data)
    .catch((error: ApiResponse<DeleteAllNotifications>) => error)
}

export function* fetchUnreadNotifications() {
  const token: string = yield select(getToken)
  yield put(actions.fetchingUnreadNotifications())
  const response: GetNotifications = yield call(fetchUnreadNotificationsApi, token)
  if (response !== null && !response.error) {
    yield put(actions.fetchingUnreadNotificationsDone(response.data.length))
  } else {
    yield put(actions.resetCustomerNotification())
  }
}

export function* fetchTotalUnreadNotifications() {
  const token: string = yield select(getToken)
  yield put(actions.fetchingUnreadNotifications())
  const response: GetTotalUnread = yield call(fetchTotalUnreadNotificationsApi, token)
  if (response !== null && !response.error) {
    yield put(actions.fetchingUnreadNotificationsDone(response.data))
  } else {
    yield put(actions.resetCustomerNotification())
  }
}

export function* fetchNotificationList({ silent }: { silent: boolean }) {
  if (!silent) {
    yield put(actions.fetchingNotificationList())
  }
  const token: string = yield select(getToken)
  const response: GetNotifications = yield call(fetchNotificationListApi, token)
  if (response !== null && !response.error) {
    yield put(actions.fetchingNotificationListDone(response.data))
  } else {
    const error = response !== null
      ? response.message
      : 'Cannot fetch notification list'
    yield put(actions.fetchingNotificationListFailed(error))
  }
}

export function* markNotificationsToRead() {
  const token: string = yield select(getToken)
  const response: GetNotifications = yield call(markNotificationsToReadApi, token)
  if (response !== null && !response.error) {
    yield call(fetchUnreadNotifications)
    yield call(fetchNotificationList, { silent: true })
  } else {
    const error = response !== null
      ? response.message
      : 'Failed to mark notification as read'
    yield put(actions.markNotificationsToReadFailed(error))
  }
}

export function* fetchingNotificationGetReadById({ notificationId }: { notificationId: number }) {
  // redux update unread and notification read status
  yield put(actions.updatingReadStatus(notificationId))
  const token: string = yield select(getToken)
  yield call(notificationGetReadByidApi, token, notificationId)
}

export function* fetchingNotificationGetUnreadById({ notificationId }: { notificationId: number }) {
  // redux update unread and notification read status
  yield put(actions.updatingUnreadStatus(notificationId))
  const token: string = yield select(getToken)
  yield call(notificationGetUnreadByidApi, token, notificationId)
}

export function* deleteSingleNotification({ notificationId }: { notificationId: number }) {
  yield put(actions.deletingNotification())
  const token: string = yield select(getToken)
  const response: GetNotifications = yield call(deleteSingleNotificationAPI, token, notificationId)
  if (response !== null && !response.error) {
    const unread = response.data.filter((notification) => !notification.read)
    yield put(actions.singleNotificationDeleted(response.data, unread.length))
  } else {
    const error = response !== null
      ? response.message
      : 'Cannot delete notification'
    yield put(actions.deleteNotificationFailed(error))
  }
}

export function* deleteAllNotifications() {
  yield put(actions.deletingNotification())
  const token: string = yield select(getToken)
  const response: DeleteAllNotifications = yield call(deleteAllNotificationsAPI, token)
  if (response !== null && !response.error) {
    yield put(actions.allNotificationsDeleted())
  } else {
    const error = response !== null
      ? response.message
      : 'Cannot delete all notifications'
    yield put(actions.deleteNotificationFailed(error))
  }
}
