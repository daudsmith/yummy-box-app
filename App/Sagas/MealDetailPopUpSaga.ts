import { call, put } from 'redux-saga/effects'
import Actions from '../Redux/MealDetailPopUpRedux'
import Api, { GetMealResponse } from '../Services/ApiCatalog'
import { ApiResponse } from 'apisauce'

export function getMealDetailApi ({mealId, date}: {mealId: number, date: string}) {
  return Api.create()
    .getMealDetails(mealId, date)
    .then((response: ApiResponse<GetMealResponse>) => response.data)
    .catch((error: ApiResponse<GetMealResponse>) => error)
}

export function * fetchMealDetail ({mealId, date}: ReturnType<typeof Actions.fetchMealDetail>) {
  yield put(Actions.fetchingMealDetail())
  const response: GetMealResponse = yield call(getMealDetailApi, {mealId, date})
  if (response !== null && !response.error) {
    const mealDetail = {...response.data, date}
    yield put(Actions.fetchMealDetailSuccess(mealDetail))
  } else {
    const error = response !== null ? response.message : 'Cannot fetch meal detail due to network issues'
    yield put(Actions.fetchMealDetailFailed(error))
  }
}
