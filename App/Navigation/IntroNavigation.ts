import {
  createStackNavigator,
  TransitionSpecs,
} from 'react-navigation-stack'

import IntroOneScreen from '../Containers/IntroOneScreen'
import IntroTwoScreen from '../Containers/IntroTwoScreen'
import LoginScreen from '../Containers/LoginScreen'
import SearchLocation from '../Containers/MyAddresses/SearchLocation'
import RegistrationScreen from '../Containers/RegistrationScreen'
import EmailPhoneVerificationScreen from '../Containers/EmailPhoneVerificationScreen'
import ForgotPasswordScreen from '../Containers/ForgotPasswordScreen'
import PhoneVerificationScreen from '../Containers/PhoneVerificationScreen'
import AddressTypeScreen from '../Containers/AddressTypeScreen'
import AddressOptionScreen from '../Containers/AddressOptionScreen'


const IntroNavigation = createStackNavigator({
  IntroOneScreen: { screen: IntroOneScreen },
  IntroTwoScreen: { screen: IntroTwoScreen },
  Login: { screen: LoginScreen },
  AddressTypeScreen: { screen: AddressTypeScreen },
  AddressOptionScreen: { screen: AddressOptionScreen },
  SearchLocation: { screen: SearchLocation },
  RegistrationScreen: { screen: RegistrationScreen },
  EmailPhoneVerificationScreen: { screen: EmailPhoneVerificationScreen },
  ForgotPasswordScreen: { screen: ForgotPasswordScreen },
  PhoneVerificationScreen: { screen: PhoneVerificationScreen },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.TransitionIOSSpec,
      close: TransitionSpecs.TransitionIOSSpec,
    },
  },
})

export default IntroNavigation
