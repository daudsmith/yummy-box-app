import { createDrawerNavigator } from 'react-navigation-drawer'
import { scale } from 'react-native-size-matters'

import DrawerContent from '../Containers/DrawerContent'
import ContactUs from '../Containers/ContactUs'

import NotificationListScreen from '../Containers/NotificationListScreen'
import MyMealPlanNavigation from './MyMealPlanNavigation'
import CustomerOrderNavigation from './CustomerOrderNavigation'
import ScreenNavigation from './ScreenNavigation'
import MainNavigation from './MainNavigation'
import PromoDetailScreen from '../Containers/PromoDetailScreen'
import MealListScreen from '../Containers/MealListScreen'
import TopUpScreen from '../Containers/TopUpScreen'
import { TransitionSpecs } from 'react-navigation-stack'

const NavigationDrawer = createDrawerNavigator({
  HomeScreen: {
    screen: MainNavigation,
    navigationOptions: {
      drawerLabel: 'Home',
    },
  },
  MyMealPlanScreen: {
    screen: MyMealPlanNavigation,
    navigationOptions: {
      drawerLabel: 'My Meal Plan',
    },
  },
  CustomerMyOrderListScreen: {
    screen: CustomerOrderNavigation,
    navigationOptions: {
      drawerLabel: 'My Orders',
    },
  },
  NotificationListScreen: {
    screen: NotificationListScreen,
    navigationOptions: {
      drawerLabel: 'Notifications',
    },
  },
  ContactUs: {
    screen: ContactUs,
    navigationOptions: {
      drawerLabel: 'Help & Contact Us',
    },
  },
  TopUpScreen: { screen: TopUpScreen },
  MealListScreen: { screen: MealListScreen },
  ScreenNavigation: { screen: ScreenNavigation },
  PromoDetailScreen: { screen: PromoDetailScreen },
}, {
  initialRouteName: 'HomeScreen',
  contentComponent: DrawerContent,
  drawerWidth: scale(275),
  backBehavior: 'history',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.ScaleFromCenterAndroidSpec,
      close: TransitionSpecs.ScaleFromCenterAndroidSpec,
    },
  },
})

export default NavigationDrawer
