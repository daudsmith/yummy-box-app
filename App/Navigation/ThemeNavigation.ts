import {
  createStackNavigator,
  TransitionSpecs,
} from 'react-navigation-stack'

import ThemeDetailScreen from '../Containers/ThemeDetailScreen'
import ThemeCalendarScreen from '../Containers/ThemeCalendarScreen'
import SubscriptionScheduleScreen
  from '../Containers/SubscriptionScheduleScreen'
import SubscriptionDetailScreen from '../Containers/SubscriptionDetailScreen'
import SingleOrderCheckoutNavigation from './SingleOrderCheckoutNavigation'
import SubscriptionCheckoutNavigation from './SubscriptionCheckoutNavigation'

const ThemeNavigation = createStackNavigator({
  ThemeDetailScreen: { screen: ThemeDetailScreen },
  ThemeCalendarScreen: { screen: ThemeCalendarScreen },
  PackageSingleOrderCheckoutScreen: { screen: SingleOrderCheckoutNavigation },
  SubscriptionScheduleScreen: { screen: SubscriptionScheduleScreen },
  SubscriptionDetailScreen: { screen: SubscriptionDetailScreen },
  SubscriptionConfirmationScreen: { screen: SubscriptionCheckoutNavigation },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.ScaleFromCenterAndroidSpec,
      close: TransitionSpecs.ScaleFromCenterAndroidSpec,
    },
  },
})

export default ThemeNavigation
