import {
  createStackNavigator,
  TransitionSpecs,
} from 'react-navigation-stack'

import CustomerMyOrderListScreen from '../Containers/CustomerMyOrderListScreen'
import CustomerMyOrderDetailScreen
  from '../Containers/CustomerMyOrderDetailScreen'
import MyMealPlanDetailScreen from '../Containers/MyMealPlanDetailScreen'
import MyMealPlanModifyScreen from '../Containers/MyMealPlanModifyScreen'
import MyMealPlanModifyCatalogScreen
  from '../Containers/MyMealPlanModifyCatalogScreen'
import MyMealPlanModifyPaymentScreen
  from '../Containers/MyMealPlanModifyPaymentScreen'
import MySubscriptionDetailScreen
  from '../Containers/MySubscriptionDetailScreen'
import SubscriptionOrderModifyScreen
  from '../Containers/SubscriptionOrderModifyScreen'
import MyMealPlanModifyCalendarScreen
  from '../Containers/MyMealPlanModifyCalendarScreen'
import AddressTypeScreen from '../Containers/AddressTypeScreen'
import AddressOptionScreen from '../Containers/AddressOptionScreen'
import SearchLocation from '../Containers/MyAddresses/SearchLocation'
import CreateAddress from '../Containers/MyAddresses/CreateAddress'

const ScreenNavigation = createStackNavigator({
  CustomerMyOrderListScreen: { screen: CustomerMyOrderListScreen },
  CustomerMyOrderDetailScreen: { screen: CustomerMyOrderDetailScreen },
  MyMealPlanDetailScreen: { screen: MyMealPlanDetailScreen },
  MyMealPlanModifyScreen: { screen: MyMealPlanModifyScreen },
  MyMealPlanModifyCatalogScreen: { screen: MyMealPlanModifyCatalogScreen },
  MyMealPlanModifyPaymentScreen: { screen: MyMealPlanModifyPaymentScreen },
  MyMealPlanModifyCalendarScreen: { screen: MyMealPlanModifyCalendarScreen },
  MySubscriptionDetailScreen: { screen: MySubscriptionDetailScreen },
  SubscriptionOrderModifyScreen: { screen: SubscriptionOrderModifyScreen },
  AddressTypeScreen: { screen: AddressTypeScreen },
  AddressOptionScreen: { screen: AddressOptionScreen },
  CoSearchLocation: { screen: SearchLocation },
  CoCreateAddress: { screen: CreateAddress },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.TransitionIOSSpec,
      close: TransitionSpecs.TransitionIOSSpec,
    },
  },
})

export default ScreenNavigation
