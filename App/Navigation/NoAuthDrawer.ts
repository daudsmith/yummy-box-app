import { createDrawerNavigator } from 'react-navigation-drawer'
import HomeScreen from '../Containers/HomeScreen'
import DrawerContent from '../Containers/DrawerContent'
import ContactUs from '../Containers/ContactUs'
import MealListScreen from '../Containers/MealListScreen'
import MealDetailScreen from '../Containers/MealDetailScreen'
import PromoDetailScreen from '../Containers/PromoDetailScreen'
import ThemeDetailScreen from '../Containers/ThemeDetailScreen'

import AuthNavigation from './AuthNavigation'
import { scale } from 'react-native-size-matters'
import {
  CardStyleInterpolators,
  TransitionSpecs,
  createStackNavigator,
} from 'react-navigation-stack'

const NoAuthStack = createStackNavigator({
  HomeScreen: { screen: HomeScreen },
  MealDetailScreen: { screen: MealDetailScreen },
  PromoDetailScreen: { screen: PromoDetailScreen },
  MealListScreen: { screen: MealListScreen },
  ThemeDetailScreen: { screen: ThemeDetailScreen },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.TransitionIOSSpec,
      close: TransitionSpecs.TransitionIOSSpec,
    },
  },
})

const NoAuthDrawer = createDrawerNavigator({
    HomeScreen: {
      screen: NoAuthStack,
      navigationOptions: {
        drawerLabel: 'Home',
      },
    },
    ContactUs: {
      screen: ContactUs,
      navigationOptions: {
        drawerLabel: 'Help & Contact Us',
      },
    },
    AuthStep: { screen: AuthNavigation },
  },
  {
    initialRouteName: 'HomeScreen',
    contentComponent: DrawerContent,
    drawerWidth: scale(275),
    backBehavior: 'history',
    navigationOptions: {
      transitionSpec: {
        open: TransitionSpecs.ScaleFromCenterAndroidSpec,
        close: TransitionSpecs.ScaleFromCenterAndroidSpec,
      },
      cardStyleInterpolator: CardStyleInterpolators.forScaleFromCenterAndroid
    },
  },
)

export default NoAuthDrawer
