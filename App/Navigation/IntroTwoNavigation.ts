import {
	createStackNavigator,
	TransitionSpecs,
} from 'react-navigation-stack'

import IntroTwoScreen from '../Containers/IntroTwoScreen'
import SearchLocation from '../Containers/MyAddresses/SearchLocation'


const IntroTwoNavigation = createStackNavigator({
	IntroTwoScreen: { screen: IntroTwoScreen },
	SearchLocation: { screen: SearchLocation },
}, {
	headerMode: 'none',
	navigationOptions: {
		transitionSpec: {
			open: TransitionSpecs.TransitionIOSSpec,
			close: TransitionSpecs.TransitionIOSSpec,
		},
	},
})

export default IntroTwoNavigation
