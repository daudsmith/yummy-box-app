import { createSwitchNavigator } from 'react-navigation'

// screens identified by the router
import LaunchScreen from '../Containers/LaunchScreen'
import NavigationDrawer from './NavigationDrawer'
import IntroNavigation from './IntroNavigation'
import IntroTwoNavigation from './IntroTwoNavigation'

const PrimaryNav = createSwitchNavigator({
  LaunchScreen: { screen: LaunchScreen },
  NavigationDrawer: { screen: NavigationDrawer },
  IntroNavigation: { screen: IntroNavigation },
  IntroTwoNavigation: { screen: IntroTwoNavigation },
}, {
  initialRouteName: 'LaunchScreen',
})

export default PrimaryNav
