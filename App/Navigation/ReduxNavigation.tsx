import React, {Component} from 'react'
import {BackHandler, View} from 'react-native'
import { NavigationSwitchProp, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import {createReduxContainer} from 'react-navigation-redux-helpers'
import AppNavigation from './AppNavigation'
import { Root } from 'native-base'
import AlertBox from '../Components/AlertBox'
import { InitStateNavigation } from '../Redux/NavigationRedux'
import { AppState } from '../Redux/CreateStore'

const mapNavigationProps = (state: AppState) => ({
  state: state.nav,
})

const App = createReduxContainer(AppNavigation)
const AppWithNavigationState = connect(mapNavigationProps)(App)

interface ReduxNavigationPropsInterface {
  navigation?: NavigationSwitchProp
  dispatch: any
  nav: InitStateNavigation
}

interface ReduxNavigationStateInterface {
  showExitModal: boolean
}

class ReduxNavigation extends Component<ReduxNavigationPropsInterface, ReduxNavigationStateInterface> {
  state = {
    showExitModal: false
  }

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
  }

  componentWillUnmount () {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
  }
  onBackPress = () => {
    const { nav, dispatch } = this.props
    const routes = nav.routes
    const index = nav.index
        
    if (index !== 0) {
      const routeIndex = routes[index].index
      if (routes[index].index > 0) {
        dispatch(NavigationActions.back())
        return true
      } else if (
        routes[index].routes[routeIndex].index > 0
      ) {
        dispatch(NavigationActions.back())
        return true
      } else {
        this.setState({showExitModal: true})
        return true
      }
    } else {
      dispatch(NavigationActions.back())
      return true
    }
  }

  render () {
    return (
      <View style={{flex: 1}}>
        <Root>
          <AppWithNavigationState />
        </Root>
        <AlertBox
          primaryAction={() => this.setState({showExitModal: false})}
          primaryActionText='No'
          secondaryAction={() => BackHandler.exitApp()}
          secondaryActionText='Yes'
          dismiss={() => this.setState({showExitModal: false})}
          text='Are you sure you want to exit?'
          visible={this.state.showExitModal}
          animationIn='fadeIn'
          animationOut='fadeOut'
        />
      </View>
    )
  }
}
const mapStateToProps = state => ({ nav: state.nav })
export default connect(mapStateToProps)(ReduxNavigation)
