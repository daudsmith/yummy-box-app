import {
  createStackNavigator,
  TransitionSpecs,
} from 'react-navigation-stack'

import SingleOrderCheckoutScreen from '../Containers/SingleOrderCheckoutScreen'
import SingleOrderCheckoutPaymentScreen from '../Containers/SingleOrderCheckoutPaymentScreen'
import SearchLocation from '../Containers/MyAddresses/SearchLocation'
import CreateAddress from '../Containers/MyAddresses/CreateAddress'
import AddressTypeScreen from '../Containers/AddressTypeScreen'
import AddressOptionScreen from '../Containers/AddressOptionScreen'
import MapScreen from '../Containers/MapScreen'

const SingleOrderCheckoutNavigation = createStackNavigator({
  SingleOrderCheckoutScreen: { screen: SingleOrderCheckoutScreen },
  SingleOrderCheckoutPaymentScreen: { screen: SingleOrderCheckoutPaymentScreen },
  AddressTypeScreen: { screen: AddressTypeScreen },
  AddressOptionScreen: { screen: AddressOptionScreen },
  CoSearchLocation: { screen: SearchLocation },
  CoCreateAddress: { screen: CreateAddress },
  CoMapScreen: { screen: MapScreen },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.ScaleFromCenterAndroidSpec,
      close: TransitionSpecs.ScaleFromCenterAndroidSpec,
    },
  },
})

export default SingleOrderCheckoutNavigation
