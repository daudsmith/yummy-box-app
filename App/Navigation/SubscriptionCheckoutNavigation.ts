import {
  createStackNavigator,
  TransitionSpecs,
} from 'react-navigation-stack'

import SearchLocation from '../Containers/MyAddresses/SearchLocation'
import CreateAddress from '../Containers/MyAddresses/CreateAddress'
import AddressTypeScreen from '../Containers/AddressTypeScreen'
import AddressOptionScreen from '../Containers/AddressOptionScreen'
import SubscriptionConfirmationScreen from '../Containers/SubscriptionConfirmationScreen'

const SubscriptionCheckoutNavigation = createStackNavigator({
  SubscriptionConfirmationScreen: { screen: SubscriptionConfirmationScreen },
  SubAddressTypeScreen: { screen: AddressTypeScreen },
  SubAddressOptionScreen: { screen: AddressOptionScreen },
  SubCoSearchLocation: { screen: SearchLocation },
  SubCoCreateAddress: { screen: CreateAddress },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.ScaleFromCenterAndroidSpec,
      close: TransitionSpecs.ScaleFromCenterAndroidSpec,
    },
  },
})

export default SubscriptionCheckoutNavigation
