import {
  createStackNavigator,
  TransitionSpecs,
} from 'react-navigation-stack'

import HomeScreen from '../Containers/HomeScreen'
import ThemeNavigation from './ThemeNavigation'
import PromoDetailScreen from '../Containers/PromoDetailScreen'
import MealDetailScreen from '../Containers/MealDetailScreen'
import SubscriptionScheduleScreen
  from '../Containers/SubscriptionScheduleScreen'
import SubscriptionDetailScreen from '../Containers/SubscriptionDetailScreen'
import SubscriptionConfirmationScreen
  from '../Containers/SubscriptionConfirmationScreen'
import SingleOrderCheckoutNavigation from './SingleOrderCheckoutNavigation'
import MealListScreen from '../Containers/MealListScreen'
import SearchLocation from '../Containers/MyAddresses/SearchLocation'
import AddressTypeScreen from '../Containers/AddressTypeScreen'
import AddressOptionScreen from '../Containers/AddressOptionScreen'
import IntroTwoScreen from '../Containers/IntroTwoScreen'

const MainNavigation = createStackNavigator({
  HomeScreen: { screen: HomeScreen },
  MealDetailScreen: { screen: MealDetailScreen },
  PromoDetailScreen: { screen: PromoDetailScreen },
  ThemeNavigation: { screen: ThemeNavigation },
  SingleOrderCheckoutScreen: { screen: SingleOrderCheckoutNavigation },
  HomeSubscriptionSchedule: { screen: SubscriptionScheduleScreen },
  HomeSubscriptionDetail: { screen: SubscriptionDetailScreen },
  HomeSubscriptionConfirmation: { screen: SubscriptionConfirmationScreen },
  MealListScreen: { screen: MealListScreen },
  SearchLocation: { screen: SearchLocation },
  AddressTypeScreen: { screen: AddressTypeScreen },
  AddressOptionScreen: { screen: AddressOptionScreen },
  IntroTwoScreen: { screen: IntroTwoScreen },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.TransitionIOSSpec,
      close: TransitionSpecs.TransitionIOSSpec,
    },
  },
})

export default MainNavigation
