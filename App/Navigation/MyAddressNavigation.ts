import { createStackNavigator, TransitionSpecs } from 'react-navigation-stack'

import MyAddressList from '../Containers/MyAddresses/List'
import CreateAddress from '../Containers/MyAddresses/CreateAddress'
import SearchLocation from '../Containers/MyAddresses/SearchLocation'
import MapScreen from '../Containers/MapScreen'

const MainNavigation = createStackNavigator({
  MyAddressList: { screen: MyAddressList },
  SearchLocation: { screen: SearchLocation },
  CreateAddress: { screen: CreateAddress },
  MapScreen: { screen: MapScreen },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.TransitionIOSSpec,
      close: TransitionSpecs.TransitionIOSSpec,
    },
  },
})

export default MainNavigation
