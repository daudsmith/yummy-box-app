import {
  createStackNavigator,
} from 'react-navigation-stack'
import Login from '../Containers/LoginScreen'
import LoginLandingScreen from '../Containers/LoginLandingScreen'
import RegistrationScreen from '../Containers/RegistrationScreen'
import EmailPhoneVerificationScreen from '../Containers/EmailPhoneVerificationScreen'
import ForgotPasswordScreen from '../Containers/ForgotPasswordScreen'
import PhoneVerificationScreen from '../Containers/PhoneVerificationScreen'

const AuthNavigation = createStackNavigator({
    LoginLandingScreen: { screen: LoginLandingScreen },
    Login: { screen: Login },
    RegistrationScreen: { screen: RegistrationScreen },
    EmailPhoneVerificationScreen: { screen: EmailPhoneVerificationScreen },
    ForgotPasswordScreen: { screen: ForgotPasswordScreen },
    PhoneVerificationScreen: { screen: PhoneVerificationScreen },
  },
  {
    initialRouteName: 'LoginLandingScreen',
    headerMode: 'none',
  },
)

export default AuthNavigation
