import {
  createStackNavigator,
  TransitionSpecs,
} from 'react-navigation-stack'

import AccountScreen from '../Containers/AccountScreen'
import CreditCardAddScreen from '../Containers/CreditCardAddScreen'
import CustomerMyOrderDetailScreen
  from '../Containers/CustomerMyOrderDetailScreen'
import CreditCardListScreen from '../Containers/CreditCardListScreen'
import ChangePasswordScreen from '../Containers/ChangePasswordScreen'
import ChangePhoneNumberScreen from '../Containers/ChangePhoneNumberScreen'
import ChangeEmail from '../Containers/ChangeEmail'
import MyAddressNavigation from './MyAddressNavigation'

const ScreenNavigation = createStackNavigator({
  AccountScreen: { screen: AccountScreen },
  CreditCardAddScreen: { screen: CreditCardAddScreen },
  CustomerMyOrderDetailScreen: { screen: CustomerMyOrderDetailScreen },
  CreditCardListScreen: { screen: CreditCardListScreen },
  ChangePasswordScreen: { screen: ChangePasswordScreen },
  ChangePhoneNumberScreen: { screen: ChangePhoneNumberScreen },
  ChangeEmail: { screen: ChangeEmail },
  MyAddressList: { screen: MyAddressNavigation },
}, {
  headerMode: 'none',
  navigationOptions: {
    transitionSpec: {
      open: TransitionSpecs.ScaleFromCenterAndroidSpec,
      close: TransitionSpecs.ScaleFromCenterAndroidSpec,
    },
  },
})

export default ScreenNavigation
