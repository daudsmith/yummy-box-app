import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform'
import AsyncStorage from '@react-native-community/async-storage'
import Config from 'react-native-config'

interface ReduxPersistInterface {
  active: boolean
  reducerVersion: string
  storeConfig: any
}

const REDUX_PERSIST: ReduxPersistInterface = {
  active: Config.IS_TEST === undefined ? true : parseInt(Config.IS_TEST) !== 1,
  reducerVersion: '1.1',
  storeConfig: {
    key: 'yummybox-persistor',
    storage: AsyncStorage,
    // blacklist: ['search', 'nav', 'startup', 'autoRouting'], // reducer keys that you do NOT want stored to persistence here
    whitelist: ['login', 'setting', 'cart', 'network', 'initialScreen', 'customerAccount', 'welcomeScreen', 'lastUsed'], // Optionally, just specify the keys you DO want stored to
    // persistence. An empty array means 'don't store any reducers' -> infinitered/ignite#409
    transforms: [immutablePersistenceTransform]
  }
}

export default REDUX_PERSIST
