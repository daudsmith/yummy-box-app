// Simple React Native specific changes
import Config from 'react-native-config'
import DeviceInfo from 'react-native-device-info'
const {
  API_BASE_URL,
  API_CATER_URL,
  XENDIT_CLIENT,
  IS_UAT,
  IOS_APP_VERSION,
  IOS_APP_BUILD,
  ANDROID_APP_VERSION,
  ANDROID_APP_BUILD,
  RELEASE_VERSION,
  APP_ENV,
  APP_VERSION,
  APP_ID,
  SENTRY_DSN,
  BUILD_REV,
  CODEPUSH_KEY,
  IOS_CODEPUSH_KEY,
  GOOGLE_MAPS_API_KEY,
} = Config

export interface AppConfigInterface {
  allowTextFontScaling: boolean
  apiBaseUrl: string
  apiCaterUrl: string
  dateFormat: string
  xenditKey: string
  defaultCurrencySymbol: string
  is_uat: string
  FIREBASE_APP_DOMAIN_HTTPS: string
  FIREBASE_LONG_DOMAIN: string
  apiRequestApi: string
  iOSAppVersion: string
  iOSAppBuild: string
  AndroidAppVersion: string
  AndroidAppBuild: string
  ReleaseVersion: string
  patchVersion: string
  appEnvironment: string
  SentryDSN: string
  buildRev: string
  appId: string,
  appVersion: string,
  codePushKey: string,
  iosCodePushKey: string,
  googleMapsKey: string,
}

export default <AppConfigInterface>{
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  apiBaseUrl: API_BASE_URL,
  apiCaterUrl: API_CATER_URL,
  dateFormat: 'ddd, DD MMM',
  xenditKey: XENDIT_CLIENT,
  defaultCurrencySymbol: 'Rp. ',
  is_uat: IS_UAT,
  FIREBASE_APP_DOMAIN_HTTPS: 'https://yummybox.page.link',
  FIREBASE_LONG_DOMAIN: 'https://www.yummybox.id',
  apiRequestApi: 'https://api.authy.com/protected/json/phones/verification/',
  iOSAppVersion: IOS_APP_VERSION,
  iOSAppBuild: IOS_APP_BUILD,
  AndroidAppVersion: ANDROID_APP_VERSION,
  AndroidAppBuild: ANDROID_APP_BUILD,
  ReleaseVersion: RELEASE_VERSION,
  patchVersion: DeviceInfo.getBuildNumber(),
  appEnvironment: APP_ENV,
  appId: APP_ID,
  appVersion: APP_VERSION,
  SentryDSN: SENTRY_DSN ? SENTRY_DSN : '',
  buildRev: BUILD_REV,
  codePushKey: CODEPUSH_KEY,
  iosCodePushKey: IOS_CODEPUSH_KEY,
  googleMapsKey: GOOGLE_MAPS_API_KEY
}
