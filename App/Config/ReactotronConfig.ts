import config from 'react-native-config'
import Immutable from 'seamless-immutable'
import Reactotron, { asyncStorage } from 'reactotron-react-native'
import { reactotronRedux as reduxPlugin } from 'reactotron-redux'
import sagaPlugin from 'reactotron-redux-saga'

const reactotron = Reactotron
  .configure({ name: config.APP_NAME })
  .use(asyncStorage({}))
  .useReactNative()
  .use(reduxPlugin({ onRestore: Immutable }))
  .use(sagaPlugin({}))
  .connect()

export default reactotron
