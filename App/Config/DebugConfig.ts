interface debugConfigInterface {
  useFixtures: boolean
  yellowBox: boolean
  includeExamples: boolean
  useReactotron: boolean
}

export default <debugConfigInterface>{
  useFixtures: false,
  yellowBox: __DEV__,
  includeExamples: __DEV__,
  useReactotron: __DEV__
}
