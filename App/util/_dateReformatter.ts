import moment from 'moment'
import { Alert } from 'react-native'

export type ReformattedDateType = 'ddd, DD MMM YYYY' | 'YYYY-MM-DD';

interface DateReformatterInterface {
  inputDate: string;
  returnFormat: ReformattedDateType;
}

export const _dateReformatter = (props: DateReformatterInterface) => {
  const { inputDate, returnFormat } = props
  const inputDateChecker = parseInt(inputDate)
    ? 'YYYY-MM-DD'
    : 'ddd, DD MMM YYYY'

  if (inputDate) {
    return moment(inputDate, inputDateChecker).format(returnFormat)
  } else {
    if (__DEV__) {
      Alert.alert('DateReformatter got an Error')
    }
  }

  return null
}
