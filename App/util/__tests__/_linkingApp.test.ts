import { Linking } from 'react-native'
import { _linkingToOtherApp } from '../_linkingApp'

describe('_linkingToOtherApp', () => {
  test('_linkingToOtherApp checkout_url is Object App Installed', async () => {
    const spy = jest.spyOn(Linking, 'canOpenURL')
    await _linkingToOtherApp({
      checkout_url: {
        redirect_url_app: 'yummybox://',
        redirect_url_http: 'https://yummycorp.com'
      }
    })
    expect(spy).toBeCalledWith('yummybox://')
    spy.mockReset()
    spy.mockRestore()
  })
  test('_linkingToOtherApp checkout_url is Object Not Installed', async () => {
    const spy = jest.spyOn(Linking, 'openURL')
    await _linkingToOtherApp({
      checkout_url: {
        redirect_url_app: 'yummybox://',
        redirect_url_http: 'https://yummycorp.com'
      }
    })
    expect(spy).toBeCalledWith('https://yummycorp.com')
    spy.mockReset()
    spy.mockRestore()
  })

  test('_linkingToOtherApp checkout_url is String', async () => {
    expect(
      await _linkingToOtherApp({
        checkout_url: 'https://yummycorp.com'
      })
    ).toBeNull()
  })
})
