import { Linking } from 'react-native'

export type CheckoutUrlType =
  | {
      redirect_url_app: string;
      redirect_url_http: string;
    }
  | string;

export interface UrlLinkingInterface {
  checkout_url: CheckoutUrlType;
}

interface LinkingToOtherAppProps extends UrlLinkingInterface {}

export const _linkingToOtherApp = async ({
  checkout_url
}: LinkingToOtherAppProps) => {
  if (typeof checkout_url === 'object') {
    const resUrl: {
      redirect_url_app: string;
      redirect_url_http: string;
    } =
      typeof checkout_url !== 'object'
        ? JSON.parse(checkout_url)
        : checkout_url

    const isAppInstalled = await Linking.canOpenURL(resUrl.redirect_url_app)
    Linking.openURL(
      isAppInstalled ? resUrl.redirect_url_app : resUrl.redirect_url_http
    )
  }
  return null
}

export interface LinkingFromOtherAppProps {
  onSuccess: () => void;
  onFailure: () => void;
}

export const _linkingFromOtherApp = ({
  onSuccess,
  onFailure
}: LinkingFromOtherAppProps) => {
  const _handleLinkingState = ({ url }: { url: string }) => {
    // Shopee doesn't have callback payment status on deep-link url to our app so return always be a success
    // for payment status only be handled from with fetch order
    const isShopeePay = url.includes('payment_type=shopee')
    if (isShopeePay) {
      onSuccess()
    } else {
      const isPaymentSuccess = url.includes('success')
      isPaymentSuccess ? onSuccess() : onFailure()
    }
  }

  const mountData = () => {
    Linking.addEventListener('url', _handleLinkingState)
  }
  const unmountData = () => {
    Linking.removeEventListener('url', _handleLinkingState)
  }

  return { mountData, unmountData }
}
