import valid from 'card-validator'
import {
  removeLeadingSpaces,
  removeNonNumber,
} from './Utilities'
import pick from 'lodash/pick'

const limitLength = (string: string = "", maxLength: number) => string.substr(0, maxLength);
const addGaps = (string: string = "", gaps: number): string => {
  const offsets: number[] = [0].concat(gaps).concat([string.length]);

  return offsets.map((end, index) => {
    if (index === 0) return "";
    const start = offsets[index - 1];
    return string.substr(start, end - start);
  }).filter(part => part !== "").join(" ");
};

const FALLBACK_CARD = { gaps: [4, 8, 12], lengths: [16], code: { size: 3 } };
export default class CCFieldFormatter {
  _displayedFields

  constructor(displayedFields) {
    this._displayedFields = [...displayedFields, "type"];
  }

  formatValues = (values): any => {
    const card = valid.number(values.number).card || FALLBACK_CARD;

    return pick({
      type: card.type,
      number: this._formatNumber(values.number, card),
      expiry: this._formatExpiry(values.expiry),
      cvc: this._formatCVC(values.cvc, card),
      name: removeLeadingSpaces(values.name),
      postalCode: removeNonNumber(values.postalCode),
    }, this._displayedFields);
  };

  _formatNumber = (number: string, card) => {
    const numberSanitized = removeNonNumber(number);
    const maxLength = card.lengths[card.lengths.length - 1];
    const lengthSanitized = limitLength(numberSanitized, maxLength);
    return addGaps(lengthSanitized, card.gaps);
  };

  _formatExpiry = (expiry: string): string => {
    const sanitized = limitLength(removeNonNumber(expiry), 4);
    if (sanitized.match(/^[2-9]$/)) { return `0${sanitized}`; }
    if (sanitized.length > 2) { return `${sanitized.substr(0, 2)}/${sanitized.substr(2, sanitized.length)}`; }
    return sanitized;
  };

  _formatCVC = (cvc: string, card): string => {
    const maxCVCLength = card.code.size;
    return limitLength(removeNonNumber(cvc), maxCVCLength);
  };
}
