import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ViewPropTypes,
  KeyboardType,
  ViewStyle,
  TextStyle,
} from 'react-native'
import Styles from './Styles'
import Colors from '../../../Themes/Colors'
import { scale, verticalScale } from 'react-native-size-matters'

const s = StyleSheet.create({
  baseInputStyle: {
    color: 'black',
    flex: 1
  },
})

interface CCInputPropsInterface {
  field: string
  label?: string
  value?: string
  placeholder?: string
  keyboardType?: KeyboardType
  status?: 'valid' | 'invalid' | 'incomplete'
  containerStyle?: any
  inputStyle?: TextStyle
  labelStyle?: TextStyle
  validColor?: string
  invalidColor?: string
  placeholderColor?: string
  onFocus?: (field: string) => void
  onChange?: (field: string, value: string) => void
  onBecomeEmpty?: (field: string) => void
  onBecomeValid?: (field: string) => void
  handleFocusInput?: (field: string) => void
  setRef?: (ref: any) => void
  isLiteInput?: boolean
  testID?: string
  accessibilityLabel?: string
}

export default class CCInput extends Component<CCInputPropsInterface> {
  inputRefs
  static propTypes = {
    field: PropTypes.string.isRequired,
    label: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    keyboardType: PropTypes.string,
    status: PropTypes.oneOf(['valid', 'invalid', 'incomplete']),
    containerStyle: ViewPropTypes.style,
    inputStyle: PropTypes.object,
    labelStyle: PropTypes.object,
    validColor: PropTypes.string,
    invalidColor: PropTypes.string,
    placeholderColor: PropTypes.string,
    onFocus: PropTypes.func,
    onChange: PropTypes.func,
    onBecomeEmpty: PropTypes.func,
    onBecomeValid: PropTypes.func,
    setRef: PropTypes.func,
  }

  static defaultProps = {
    label: '',
    value: '',
    status: 'incomplete',
    keyboardType: 'numeric',
    containerStyle: {},
    inputStyle: {},
    labelStyle: {},
    onFocus: () => {
    },
    onChange: () => {
    },
    onBecomeEmpty: () => {
    },
    onBecomeValid: () => {
    },
  }

  UNSAFE_componentWillReceiveProps = newProps => {
    const {status, value, onBecomeEmpty, onBecomeValid, field} = this.props
    const {status: newStatus, value: newValue} = newProps

    if (value !== '' && newValue === '') onBecomeEmpty(field)
    if (status !== 'valid' && newStatus === 'valid') onBecomeValid(field)
  }

  focus = () => this.inputRefs.focus()

  _onFocus = () => this.props.onFocus(this.props.field)
  _onChange = value => {
    this.props.onChange(this.props.field, value)
  }

  render () {
    const {
      label,
      value,
      placeholder,
      status,
      keyboardType,
      containerStyle,
      inputStyle,
      labelStyle,
      placeholderColor,
      isLiteInput,
      setRef,
      handleFocusInput,
      field,
      testID,
      accessibilityLabel,
    } = this.props

    const borderBottomColor = value === '' ? Colors.pinkishGrey : status === 'valid' ? Colors.greyishBrown : 'red'
    const borderBottomWidth = isLiteInput ? 0 : 1

    return (
      <TouchableOpacity onPress={()=>handleFocusInput(field)} style={{flex: 1, borderBottomWidth: borderBottomWidth, borderBottomColor: borderBottomColor}}>
        <Text
          style={{
            fontFamily: 'Rubik-Regular',
            fontSize: scale(14),
            ...Styles.inputLabel,
            ...labelStyle,
          }}
        >
          {label}
        </Text>
        <View
          style={{
            ...containerStyle,
            height: verticalScale(40),
            marginTop: verticalScale(10)
          }}
        >
          <TextInput
            testID={testID}
            accessibilityLabel={accessibilityLabel}
            ref={setRef}
            keyboardType={keyboardType}
            autoCapitalize='words'
            autoCorrect={false}
            style={{
              ...s.baseInputStyle,
              ...inputStyle,
              fontFamily: 'Rubik-Regular',
              color: Colors.greyishBrownTwo,
              top: verticalScale(7)
            }}
            underlineColorAndroid={'transparent'}
            placeholderTextColor={placeholderColor}
            placeholder={placeholder}
            value={value}
            onFocus={this._onFocus}
            onChangeText={this._onChange}
            />
        </View>
      </TouchableOpacity>
    )
  }
}
