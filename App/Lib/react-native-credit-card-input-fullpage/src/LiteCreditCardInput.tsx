import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  StyleSheet,
  Image,
  LayoutAnimation,
  TouchableOpacity,
} from 'react-native'
import { scale, verticalScale } from 'react-native-size-matters'
import {createIconSetFromIcoMoon} from 'react-native-vector-icons'

import Icons from './Icons'
import CCInput from './CCInput'
import { InjectedProps, InjectedInterface } from './connectToState'
import Colors from '../../../Themes/Colors'
import icoMoonConfig from '../../../Images/SvgIcon/selection.json'

const INFINITE_WIDTH: number = 1000
const YumboxIcon = createIconSetFromIcoMoon(icoMoonConfig)

const s = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    overflow: 'hidden'
  },
  icon: {
    width: scale(32),
    height: verticalScale(23),
    resizeMode: 'contain',
  },
  expanded: {
    flex: 1,
  },
  hidden: {
    width: 0,
  },
  leftPart: {               // styling textinput for input user credit card number
    overflow: 'hidden',
    width: '100%'
  },
  rightPart: {              // styling textinput for input user CVV & expired date
    overflow: 'hidden',
    flexDirection: 'row'
  },
  last4: {                  // styling for 4 digit number
    flex: 1,
    justifyContent: 'center'
  },
  numberInput: {
    width: INFINITE_WIDTH
  },
  expiryInput: {
    width: scale(91),
    marginLeft: scale(20),
    borderBottomWidth: 1
  },
  cvcInput: {
    width: scale(53),
    marginLeft: scale(10),
    borderBottomWidth: 1
  },
  last4Input: {
    marginLeft: scale(10),
    width: scale(91),
    borderBottomWidth: 1
  },
  input: {
    height: verticalScale(40),
    color: Colors.brownishGrey,
  },
})

interface LiteCreditCardInputInterface {
  placeholders?: any
  inputStyle?: any
  validColor?: string
  invalidColor?: string
  placeholderColor?: string
}

interface LiteCreditCardInputStateInterface {
  borderWidthContainer: number
}

/* eslint react/prop-types: 0 */ // https://github.com/yannickcr/eslint-plugin-react/issues/106
export default class LiteCreditCardInput extends Component<InjectedInterface & LiteCreditCardInputInterface, LiteCreditCardInputStateInterface> {
  inputRef = []
  static propTypes = {
    ...InjectedProps,

    placeholders: PropTypes.object,

    inputStyle: PropTypes.object,

    validColor: PropTypes.string,
    invalidColor: PropTypes.string,
    placeholderColor: PropTypes.string,
  }

  static defaultProps = {
    placeholders: {
      number: 'Credit Card Number',
      expiry: 'Expiry Date',
      cvc: 'CVC',
    },
    validColor: '',
    invalidColor: 'red',
    placeholderColor: 'gray',
  }

  state = {
    borderWidthContainer: 1
  }

  componentDidMount = () => this._focus(this.props.focused)

  UNSAFE_componentWillReceiveProps = newProps => {
    if (this.props.focused !== newProps.focused) this._focus(newProps.focused)
  }

  _focusNumber = () => this._focus('number')
  _focusExpiry = () => this._focus('expiry')

  _focus = field => {
    if (!field) return
    LayoutAnimation.easeInEaseOut()
    this.inputRef[field].focus()
    // toogle show border
    if (field === 'expiry') {
      this.setState({borderWidthContainer: 0})
    } else {
      this.setState({borderWidthContainer: 1})
    }
  }

  setRef = (ref,field) => {
    this.inputRef[field] = ref
  }

  _inputProps = field => {
    const {
      inputStyle, validColor, invalidColor, placeholderColor,
      placeholders, values, status,
      onFocus, onChange, onBecomeEmpty, onBecomeValid,
    } = this.props

    return {
      field,
      inputStyle: [s.input, inputStyle],
      validColor, invalidColor, placeholderColor,
      setRef: (ref) => this.setRef(ref,field),

      placeholder: placeholders[field],
      value: values[field],
      status: status[field],

      onFocus, onChange, onBecomeEmpty, onBecomeValid,
      isLiteInput: true
    }
  }

  _iconToShow = () => {
    const { focused, values: { type } } = this.props
    if (focused === 'cvc' && type === 'american-express') return 'cvc_amex'
    if (focused === 'cvc') return 'cvc'
    if (type) return type
    return 'placeholder'
  }

  render () {
    const { focused, values: { number }, inputStyle, status: { number: numberStatus } } = this.props
    const showRightPart = focused && focused !== 'number'

    const numberLineColor = this._inputProps('number').value === '' ? Colors.pinkishGrey : Colors.brownishGrey
    const expiredLineColor = this._inputProps('expiry').value === '' ? Colors.pinkishGrey : Colors.brownishGrey
    const cvcLineColor = this._inputProps('cvc').value === '' ? Colors.pinkishGrey : Colors.brownishGrey

    const last4Value = numberStatus === 'valid' ? number.substr(number.length - 4, 4) : ''
    const last4LineColor = last4Value === '' ? Colors.pinkishGrey : Colors.brownishGrey
    const iconToShow = this._iconToShow()

    return (
      <View style={s.container}>
        <View style={{borderBottomWidth: 0.5, flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end', borderBottomColor: numberLineColor}}>
          <View style={[
            s.leftPart,
            showRightPart ? s.hidden : s.expanded,
          ]}>
            <CCInput
              {...this._inputProps('number')}
              inputStyle={{fontSize: scale(14)}}
              containerStyle={s.numberInput}
              testID='ccNumber'
              accessibilityLabel='ccNumber'
            />
          </View>
          <TouchableOpacity testID='imgCC' accessibilityLabel='imgCC' onPress={showRightPart ? this._focusNumber : this._focusExpiry }>
            {iconToShow === 'placeholder' ?
              <YumboxIcon name='cc-normal' size={scale(26)} color={Colors.greyishBrownTwo} style={{marginBottom: verticalScale(5)}} />
            :
              <Image style={{...s.icon, marginLeft: 0, marginBottom: verticalScale(5)}}
                source={{ uri: Icons[this._iconToShow()] }} />
            }

          </TouchableOpacity>
        </View>
        <View style={[
          s.rightPart,
          showRightPart ? s.expanded : s.hidden,
        ]}>
          <TouchableOpacity onPress={this._focusNumber}
            style={s.last4}>
            <View pointerEvents={'none'}>
              <CCInput field='last4'
                value={last4Value}
                inputStyle={{
                  ...s.input,
                  ...inputStyle,
                  fontFamily: 'Rubik-Regular',
                  fontSize: scale(14),
                }}
                containerStyle={{
                  ...s.last4Input,
                  borderBottomColor: last4LineColor,
                }}
                isLiteInput={true}
              />
            </View>
          </TouchableOpacity>
          <View style={{right: scale(20)}}>
            <CCInput
              {...this._inputProps('expiry')}
              inputStyle={{fontSize: scale(14)}}
              containerStyle={{
                ...s.expiryInput,
                borderBottomColor: expiredLineColor,
              }}
              testID='expDate'
              accessibilityLabel='expDate'
            />
          </View>
          <View style={{right: scale(15)}}>
            <CCInput
              {...this._inputProps('cvc')}
              inputStyle={{fontSize: scale(14)}}
              containerStyle={{
                ...s.cvcInput,
                borderBottomColor: cvcLineColor,
              }}
              testID='cvc'
              accessibilityLabel='cvc'
            />
          </View>
        </View>
      </View>
    )
  }
}
