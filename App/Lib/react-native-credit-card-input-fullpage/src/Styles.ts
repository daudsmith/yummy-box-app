import { Dimensions, StyleSheet } from 'react-native'

const scale = Dimensions.get('window').width / 375; // normalize on iPhone 6 width

function normalize(dim) {
  return Math.round(dim * scale);
}

const Style = StyleSheet.create({
  normalText: {
    color: '#000',
    fontSize: normalize(16),
    fontFamily: 'Rubik-Regular'
  },
  inputLabel: {
    color: '#000',
    fontSize: normalize(16),
    paddingRight: normalize(15),
    fontFamily: 'Rubik-Regular'
  },
  inputContainer: {
    height: normalize(43),
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  separator: {
    borderColor: '#dededf',
    borderBottomWidth: 1,
    height: normalize(1),
    marginLeft: normalize(15)
  },
  input: {
    fontSize: normalize(16),
    height: normalize(45),
    fontFamily: 'Rubik-Regular'
  }
})
export default Style
