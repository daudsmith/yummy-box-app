export const removeNonNumber = (string: string = ""): string => string.replace(/[^\d]/g, "");
export const removeLeadingSpaces = (string: string = ""):string => string.replace(/^\s+/g, "");
