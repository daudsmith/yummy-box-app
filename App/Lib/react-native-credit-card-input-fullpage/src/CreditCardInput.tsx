import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactNative, {
  NativeModules,
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  ViewPropTypes,
  ViewStyle,
} from 'react-native'

import CreditCard from './CardView'
import CCInput from './CCInput'
import { InjectedProps, InjectedInterface } from './connectToState'
import Styles from './Styles'
import Colors from '../../../Themes/Colors'

const scale = Dimensions.get('window').width / 375

function normalize(dim) {
  return Math.round(dim * scale)
}

const s = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  form: {
    marginTop: 30
  },
  inputContainer: {

  },
  inputLabel: {
    padding: 10,
    fontSize: 18,
    fontWeight: '800'
  },
  input: {
    paddingLeft: 10,
    paddingRight: 10
  }
})

const CARD_NUMBER_INPUT_WIDTH = Dimensions.get('window').width
const PREVIOUS_FIELD_OFFSET = 40

interface CreditCardInputPropsInterface {
  labels?: object
  placeholders?: object
  inputContainerStyle?: ViewStyle
  validColor?: string
  invalidColor?: string
  placeholderColor?: string
  cardImageFront?: number
  cardImageBack?: number
  cardScale?: number
  cardFontFamily?: string,
}

export default class CreditCardInput extends Component<InjectedInterface & CreditCardInputPropsInterface> {
  inputRef = []
  formRef
  static propTypes = {
    ...InjectedProps,
    labels: PropTypes.object,
    placeholders: PropTypes.object,

    inputContainerStyle: ViewPropTypes.style,

    validColor: PropTypes.string,
    invalidColor: PropTypes.string,
    placeholderColor: PropTypes.string,

    cardImageFront: PropTypes.number,
    cardImageBack: PropTypes.number,
    cardScale: PropTypes.number,
    cardFontFamily: PropTypes.string,
  }

  static defaultProps = {
    cardViewSize: {},
    labels: {
      name: 'Cardholder\'s name',
      number: 'Credit Card Number',
      expiry: 'Valid until',
      cvc: '3 Digit CVV',
      postalCode: 'Postal Code'
    },
    placeholders: {
      name: 'Full Name',
      number: '',
      expiry: '',
      cvc: '',
      postalCode: '34567'
    },
    inputContainerStyle: {
    },
    validColor: Colors.greyishBrownTwo,
    invalidColor: 'red',
    placeholderColor: Colors.pinkishGrey
  }

  constructor(props){
    super(props)
  }

  componentDidMount = () => this._focus(this.props.focused)

  UNSAFE_componentWillReceiveProps = newProps => {
    if (this.props.focused !== newProps.focused) this._focus(newProps.focused)
  }

  _focus = field => {
    if (!field) return

    const nodeHandle = ReactNative.findNodeHandle(this.inputRef[field])

    NativeModules.UIManager.measureLayoutRelativeToParent(nodeHandle,
      e => { throw e },
      x => {
        if (this.formRef) {
          this.formRef.scrollTo(
            { x: Math.max(x - PREVIOUS_FIELD_OFFSET, 0), animated: true })
        }
        this.inputRef[field].focus()
      })
  }
  setRef = (ref,field) => {
    this.inputRef[field] = ref
  }
  handleFocusInput = (field) => {
    this.inputRef[field].focus()
  }

  _inputProps = field => {
    const {
      inputStyle, labelStyle, validColor, invalidColor, placeholderColor,
      placeholders, labels, values, status,
      onFocus, onChange, onBecomeEmpty, onBecomeValid,
    } = this.props

    return {
      field,
      inputStyle: {...Styles.input, ...inputStyle},
      labelStyle: {...Styles.inputLabel, ...labelStyle},
      validColor, invalidColor, placeholderColor,
      setRef: (ref)=>this.setRef(ref,field),
      handleFocusInput: ()=>this.handleFocusInput(field),

      label: labels[field],
      placeholder: placeholders[field],
      value: values[field],
      status: status[field],

      onFocus, onChange, onBecomeEmpty, onBecomeValid,
    }
  }

  render () {
    const {
      cardImageFront,
      cardImageBack,
      inputContainerStyle,
      values: { number, expiry, cvc, name, type },
      focused,
      requiresName,
      requiresCVC,
      requiresPostalCode,
      cardScale,
      cardFontFamily
    } = this.props

    return (
      <View style={s.container}>
        <CreditCard
          focused={focused}
          brand={type}
          scale={cardScale}
          fontFamily={cardFontFamily}
          imageFront={cardImageFront}
          imageBack={cardImageBack}
          name={requiresName ? name : ' '}
          number={number}
          expiry={expiry}
          cvc={cvc}
        />
        <ScrollView ref={ref => this.formRef = ref}
          horizontal={false}
          keyboardShouldPersistTaps={'always'}
          scrollEnabled={false}
          showsHorizontalScrollIndicator={false}
          style={s.form}>
          <View>
            <CCInput
              {...this._inputProps('number')}
              containerStyle={{
                ...Styles.inputContainer,
                ...inputContainerStyle,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: CARD_NUMBER_INPUT_WIDTH,
              }}
            />
          </View>
          <View style={{flex:1, flexDirection: 'row', marginTop: normalize(22)}}>
            <View style={{flex: 1, marginRight: normalize(31)}}>
              <CCInput
                {...this._inputProps('expiry')}
                containerStyle={{
                  ...Styles.inputContainer,
                  ...inputContainerStyle,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: CARD_NUMBER_INPUT_WIDTH,
                }}
              />
            </View>
            <View style={{flex: 1, marginLeft: normalize(31)}}>
              { requiresCVC &&
                  <CCInput
                    {...this._inputProps('cvc')}
                    containerStyle={{
                      ...Styles.inputContainer,
                      ...inputContainerStyle,
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      width: CARD_NUMBER_INPUT_WIDTH,
                    }}
                  />
                }
            </View>
          </View>

          { requiresName &&
            <CCInput
              {...this._inputProps('name')}
              keyboardType='default'
              containerStyle={{
                ...Styles.inputContainer,
                ...inputContainerStyle,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: CARD_NUMBER_INPUT_WIDTH,
              }}
            />
          }
          { requiresName && <View style={Styles.separator} />}
          { requiresPostalCode &&
            <CCInput
              {...this._inputProps('postalCode')}
              containerStyle={{
                ...Styles.inputContainer,
                ...inputContainerStyle,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: CARD_NUMBER_INPUT_WIDTH,
              }}
            />
          }
          { requiresPostalCode && <View style={Styles.separator} />}
        </ScrollView>
      </View>
    )
  }
}
