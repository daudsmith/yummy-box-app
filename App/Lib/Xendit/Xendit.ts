import Base64 from '../Base64'
import { string } from 'prop-types'
import { xenditCreditTokenResponse } from '../../Services/XenditService'

interface RequestUtilInterface {
  request?: (requestData: RequestDataInterface, responseHandler: (err?: any, tokenizedCreditCard?: string) => void) => void
}

interface HeaderInterface {
  Authorization: string
}

interface BodyInterface {
  is_single_use?: boolean
  should_authenticate?: boolean
  amount: string | number
  card_data?: object
  transaction_metadata?: any
}

interface RequestDataInterface {
  method: string
  url: string
  headers: HeaderInterface
  body: BodyInterface
}

const RequestUtil: RequestUtilInterface = {}
RequestUtil.request = function (requestData, responseHandler) {
  const request = new XMLHttpRequest();
  const body = requestData.body ? JSON.stringify(requestData.body) : null;
  const headers = requestData.headers ? requestData.headers : {};

  request.open(requestData.method, requestData.url, true);
  request.onreadystatechange = function () {
      if(request.readyState === XMLHttpRequest.DONE) {
          if (request.status === 200) {
              responseHandler(null, JSON.parse(request.responseText));
          } else {
              try {
                  responseHandler(JSON.parse(request.responseText));
              } catch (e) {
                  responseHandler({
                      error_code: 'SERVER_ERROR',
                      message: 'Sorry, we had a problem processing your request.'
                  });
              }
          }
      }
  };

  request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

  for (let key in headers) {
    request.setRequestHeader(key, headers[key]);
  }

  request.send(body);
}

interface CardTypes {
  VISA: string
  MASTERCARD: string
  AMEX: string
  DISCOVER: string
  JCB: string
  VISA_ELECTRON: string
  DANKORT: string
}

interface CreditCardUtilInterface {
  CYBCardTypes?: CardTypes
  NUMBER_REGEX?: RegExp
  isCreditCardNumberValid?: (creditCardNumber: string) => boolean
  isValidLuhnNumber?: (creditCardNumber: string) => boolean
  isCreditCardExpirationDateValid?: (expMon: string, expYear: string) => boolean
  isCreditCardCVNValid?: (cvn: string) => boolean
  isCreditCardCVNValidForCardType?: (cvn: string, creditCardNumber: string) => boolean
  getCardType?: (creditCardNumber: string) => string | null
  _isCardAmex?: (creditCardNumber: string) => boolean
  _isCardMastercard?: (creditCardNumber: string) => boolean
  _isCardDiscover?: (creditCardNumber: string) => boolean
  _isCardJCB?: (creditCardNumber: string) => boolean
  _isCardMaestro?: (creditCardNumber: string) => boolean
  _isCardDankort?: (creditCardNumber: string) => boolean
  _isCardVisaElectron?: (creditCardNumber: string) => boolean
}

const CreditCardUtil: CreditCardUtilInterface = {}
CreditCardUtil.CYBCardTypes = {
  VISA: '001',
  MASTERCARD: '002',
  AMEX: '003',
  DISCOVER: '004',
  JCB: '007',
  VISA_ELECTRON: '033',
  DANKORT: '034'
}
CreditCardUtil.NUMBER_REGEX = /^\d+$/
CreditCardUtil.isCreditCardNumberValid = function (creditCardNumber) {
  return this.NUMBER_REGEX.test(creditCardNumber) &&
    creditCardNumber.length >= 12 &&
    creditCardNumber.length <= 19 &&
    CreditCardUtil.getCardType(creditCardNumber) !== null &&
    CreditCardUtil.isValidLuhnNumber(creditCardNumber)
}
CreditCardUtil.isCreditCardExpirationDateValid = function (cardExpirationMonth, cardExpirationYear) {
  return CreditCardUtil.NUMBER_REGEX.test(cardExpirationMonth) &&
    CreditCardUtil.NUMBER_REGEX.test(cardExpirationYear) &&
    Number(cardExpirationMonth) >= 1 &&
    Number(cardExpirationMonth) <= 12 &&
    Number(cardExpirationYear) >= 2016 &&
    Number(cardExpirationYear) <= 2100;
}
CreditCardUtil.isCreditCardCVNValid = function (creditCardCVN) {
  if (creditCardCVN) {
    return CreditCardUtil.NUMBER_REGEX.test(creditCardCVN) &&
      Number(creditCardCVN) >= 0 && String(creditCardCVN).length <= 4;
  } else {
    return true;
  }
}
CreditCardUtil.isCreditCardCVNValidForCardType = function (creditCardCVN, cardNumber) {
  if (creditCardCVN) {
    if (CreditCardUtil.NUMBER_REGEX.test(creditCardCVN) && Number(creditCardCVN)) {
      return CreditCardUtil._isCardAmex(cardNumber) ? String(creditCardCVN).length === 4 : String(creditCardCVN).length === 3;
    }

    return false;
  } else {
    return true;
  }
}
CreditCardUtil.isValidLuhnNumber = function (cardNumber) {
  let sum = 0, bEven = false;
  cardNumber = cardNumber.replace(/\D/g, '');

  for (let n = cardNumber.length - 1; n >= 0; n--) {
    const cDigit = cardNumber.charAt(n);
    let nDigit = parseInt(cDigit, 10);

    if (bEven) {
      if ((nDigit *= 2) > 9) {
        nDigit -= 9
      }
    }

    sum += nDigit;
    bEven = !bEven;
  }

  return (sum % 10) === 0;
}
CreditCardUtil.getCardType = function (cardNumber) {
  if (cardNumber.indexOf('4') === 0)  {
    if (CreditCardUtil._isCardVisaElectron(cardNumber)) {
      return CreditCardUtil.CYBCardTypes.VISA_ELECTRON;
    } else {
      return CreditCardUtil.CYBCardTypes.VISA;
    }
  } else if (CreditCardUtil._isCardAmex(cardNumber)) {
    return CreditCardUtil.CYBCardTypes.AMEX;
  } else if (CreditCardUtil._isCardMastercard(cardNumber)) {
    return CreditCardUtil.CYBCardTypes.MASTERCARD;
  } else if (CreditCardUtil._isCardDiscover(cardNumber)) {
    return CreditCardUtil.CYBCardTypes.DISCOVER;
  } else if (CreditCardUtil._isCardJCB(cardNumber)) {
    return CreditCardUtil.CYBCardTypes.JCB;
  } else if (CreditCardUtil._isCardDankort(cardNumber)) {
    return CreditCardUtil.CYBCardTypes.DANKORT;
  } else {
    return null;
  }
}
CreditCardUtil._isCardAmex = function (cardNumber) {
  return cardNumber.indexOf('34') === 0 || cardNumber.indexOf('37') === 0;
}
CreditCardUtil._isCardMastercard = function (cardNumber) {
  const startingNumber = Number(cardNumber.substring(0, 2));
  return startingNumber >= 50 && startingNumber <=55;
}
CreditCardUtil._isCardDiscover = function (cardNumber) {
  const firstStartingNumber = Number(cardNumber.substring(0, 3));
  const secondStartingNumber = Number(cardNumber.substring(0, 6));

  return (firstStartingNumber >= 644 && firstStartingNumber <= 649) ||
      (secondStartingNumber >= 622126 && secondStartingNumber <= 622925) ||
      cardNumber.indexOf('65') === 0 ||
      cardNumber.indexOf('6011') === 0;
}
CreditCardUtil._isCardJCB = function (cardNumber) {
  const startingNumber = cardNumber.substring(0, 4);
  return Number(startingNumber) >= 3528 && Number(startingNumber) <= 3589;
}
CreditCardUtil._isCardMaestro = function (cardNumber) {
  return cardNumber.indexOf('5018') === 0 ||
  cardNumber.indexOf('5020') === 0 ||
  cardNumber.indexOf('5038') === 0 ||
  cardNumber.indexOf('5612') === 0 ||
  cardNumber.indexOf('5893') === 0 ||
  cardNumber.indexOf('6304') === 0 ||
  cardNumber.indexOf('6759') === 0 ||
  cardNumber.indexOf('6761') === 0 ||
  cardNumber.indexOf('6762') === 0 ||
  cardNumber.indexOf('6763') === 0 ||
  cardNumber.indexOf('0604') === 0 ||
  cardNumber.indexOf('6390') === 0;
}
CreditCardUtil._isCardDankort = function (cardNumber) {
  return cardNumber.indexOf('5019') === 0;
}
CreditCardUtil._isCardVisaElectron = function (cardNumber) {
  return cardNumber.indexOf('4026') === 0 ||
      cardNumber.indexOf('417500') === 0 ||
      cardNumber.indexOf('4405') === 0 ||
      cardNumber.indexOf('4508') === 0 ||
      cardNumber.indexOf('4844') === 0 ||
      cardNumber.indexOf('4913') === 0 ||
      cardNumber.indexOf('4917') === 0;
}

export interface CardInterface {
  createToken?: (transactionData: transactionDataInterface, transactionMetadata: (err?: any, tokenizedCreditCard?: string) => void, callback?: (err?: any, tokenizedCreditCard?: string) => void) => void
  _getTokenizedCreditCard?: (err: any, callback: (err?: any, tokenizedCreditCard?: string) => void) => void
  createAuthentication?: (authData: authDataInterface, transactionMetadata: (err?: any, tokenizedCreditCard?: string) => void, callback?: (err?: any, tokenizedCreditCard?: string) => void) => void
  _createAuthentication?: (authData: authDataInterface, transactionMetadata: (err?: any, tokenizedCreditCard?: string) => void, callback?: (err?: any, tokenizedCreditCard?: string) => void) => void
  validateCardNumber?: (carNumber: string) => boolean
  validateExpiry?: (expMonth: string, expYear: string) => boolean
  validateCvn?: (cvn: string) => boolean
  validateCvnForCardType?: (cvn: string, carNumber: string) => boolean
}

export interface transactionDataInterface {
  is_multiple_use?: boolean
  should_authenticate?: boolean
  amount: number
  card_number: string
  card_exp_month: string | number
  card_exp_year: string | number
  card_cvn: string | number
}

export interface authDataInterface {
  amount: number
  token_id: string
}

const Card: CardInterface = {}
Card.createToken = function (transactionData, transactionMetadata, callback) {
  transactionData.is_multiple_use = transactionData.is_multiple_use !== undefined ? transactionData.is_multiple_use : false;
  transactionData.should_authenticate = transactionData.should_authenticate !== undefined ? transactionData.should_authenticate : true;

  if (arguments.length === 2) {
      callback = transactionMetadata;
      transactionMetadata = null;
  }

  if (!transactionData.is_multiple_use && (isNaN(transactionData.amount) || Number(transactionData.amount) < 0)) {
      return callback({ error_code: 'VALIDATION_ERROR', message: 'Amount must be a number equal or greater than 0' });
  }

  if (!CreditCardUtil.isCreditCardNumberValid(transactionData.card_number)) {
      return callback({ error_code: 'VALIDATION_ERROR', message: 'Card number is invalid' });
  }

  if (
    !CreditCardUtil.isCreditCardExpirationDateValid((
      transactionData.card_exp_month as string),
      (transactionData.card_exp_year as string),
    )
  ) {
      return callback({ error_code: 'VALIDATION_ERROR', message: 'Card expiration date is invalid' });
  }

  if (!CreditCardUtil.isCreditCardCVNValid((transactionData.card_cvn as string))) {
      return callback({ error_code: 'VALIDATION_ERROR', message: 'Card CVN is invalid' });
  }

  if (!CreditCardUtil.isCreditCardCVNValidForCardType((transactionData.card_cvn as string), transactionData.card_number)) {
      return callback({ error_code: 'VALIDATION_ERROR', message: 'Card CVN is invalid for this card type' });
  }

  Card._getTokenizedCreditCard(transactionData, function (err, tokenizedCreditCard) {
      if (err) {
          return callback(err);
      }

      callback(null, tokenizedCreditCard);
  });
}
Card.createAuthentication = function (authenticationData, transactionMetadata, callback) {
  if (arguments.length === 2) {
    callback = transactionMetadata;
    transactionMetadata = null;
  }

  if (isNaN(authenticationData.amount) || Number(authenticationData.amount) < 0) {
    return callback({ error_code: 'VALIDATION_ERROR', message: 'Amount must be a number equal or greater than 0' });
  }

  if ((typeof authenticationData.token_id) !== 'string') {
    return callback({ error_code: 'VALIDATION_ERROR', message: 'Token id must be a string' });
  }

  Card._createAuthentication(authenticationData, transactionMetadata, function (err, authentication) {
    if (err) {
      return callback(err);
    }

    callback(null, authentication);
  });
}
Card.validateCardNumber = function (cardNumber): boolean {
  return CreditCardUtil.isCreditCardNumberValid(cardNumber);
}
Card.validateExpiry = function (expMonth, expYear): boolean {
  return CreditCardUtil.isCreditCardExpirationDateValid(expMonth, expYear);
}
Card.validateCvn = function (cvn): boolean {
  return CreditCardUtil.isCreditCardCVNValid(cvn);
}
Card.validateCvnForCardType = function (cvn, cardNumber): boolean {
  return CreditCardUtil.isCreditCardCVNValidForCardType(cvn, cardNumber);
}
Card._getTokenizedCreditCard = function (transactionData, callback) {
  const publicApiKey = Xendit._getPublishableKey();
  const basicAuthCredentials = 'Basic ' + Base64.btoa(publicApiKey + ':');
  const xenditBaseURL = Xendit._getXenditURL();

  const data = {
    account_number: transactionData.card_number,
    exp_year: transactionData.card_exp_year,
    exp_month: transactionData.card_exp_month,
    cvn: transactionData.card_cvn,
  }


  RequestUtil.request({
      method: 'POST',
      url: xenditBaseURL + '/v2/credit_card_tokens',
      headers: { Authorization: basicAuthCredentials },
      body: {
          is_single_use: !transactionData.is_multiple_use,
          should_authenticate: transactionData.should_authenticate,
          amount: transactionData.amount,
          card_data: data
      }

  }, callback);
}
Card._createAuthentication = function (authenticationData, transactionMetadata, callback) {
  const publicApiKey = Xendit._getPublishableKey();
  const basicAuthCredentials = 'Basic ' + Base64.btoa(publicApiKey + ':');
  const xenditBaseURL = Xendit._getXenditURL();

  const body: BodyInterface = {
      amount: authenticationData.amount
  };

  if (transactionMetadata !== null) {
      body.transaction_metadata = transactionMetadata;
  }

  RequestUtil.request({
      method: 'POST',
      url: xenditBaseURL + '/credit_card_tokens/' + authenticationData.token_id + '/authentications',
      headers: { Authorization: basicAuthCredentials },
      body: body
  }, callback);
}

interface XenditInterface {
  STAGING_XENDIT_BASE_URL?: string
  PRODUCTION_XENDIT_BASE_URL?: string
  card?: CardInterface
  settings?: {
    url?: string
    publishable_key?: string
  },
  setPublishableKey?: (publishableKey: string) => void
  _useStagingURL?: (toggle: boolean) => void
  _getXenditURL?: () => string
  _getPublishableKey?: () => string
  _getEnvironment?: () => 'PRODUCTION' | 'DEVELOPMENT'
}

const Xendit: XenditInterface = {}
Xendit.STAGING_XENDIT_BASE_URL = 'https://api-staging.xendit.co'
Xendit.PRODUCTION_XENDIT_BASE_URL = 'https://api.xendit.co'
Xendit.card = Card
Xendit.settings = {
  url: 'https://api.xendit.co',
}
Xendit.setPublishableKey = function (publishableKey): void {
  Xendit.settings.publishable_key = publishableKey
}
Xendit._useStagingURL = function (toggle): void {
  Xendit.settings.url = toggle === false ? Xendit.PRODUCTION_XENDIT_BASE_URL : Xendit.STAGING_XENDIT_BASE_URL
}
Xendit._getXenditURL = function (): string {
  return this.settings.url
}
Xendit._getPublishableKey = function (): string {
  return this.settings.publishable_key
}
Xendit._getEnvironment = function (): 'PRODUCTION' | 'DEVELOPMENT' {
  const normalizedKey: string = this._getPublishableKey().toUpperCase()
  const isKeyProduction: boolean = normalizedKey.indexOf('PRODUCTION') > -1

  return isKeyProduction ? 'PRODUCTION' : 'DEVELOPMENT'
}

export default Xendit
