import {
  ImageSourcePropType,
  ImageURISource,
} from 'react-native'

export type imageList =
  'loginScreenFooter'
  | 'Sorry'
  | 'ThankYou'
  | 'YumboxAvatarDefault'
  | 'showingSoon'
  | 'topupLogoMANDIRI'
  | 'topupLogoBRI'
  | 'topupLogoBNI'
  | 'topupLogoBCA'
  | 'topupLogoTOKEN'
  | 'YummyboxBeta'
  | 'YummyboxUAT'
  | 'favourite'
  | 'arrowLeft'
  | 'iconFaq'
  | 'iconPrivacy'
  | 'iconTnc'
  | 'bookmark'
  | 'noSavedAddress'
  | 'currentLocation'
  | 'trash'
  | 'unknownCard'
  | 'cardFront'
  | 'cardBack'
  | 'waIcon'
  | 'whatsappIcon'
  | 'danaWhite'
  | 'danaGrey'
  | 'shopeeWhite'
  | 'shopeeGrey'
  | 'gopayWhite'
  | 'gopayGrey'
  | 'emptyCart'
  | 'arrowDown'
  | 'IntroOneBackground'
  | 'IntroTwoBackground'
  | 'Logo'
  | 'Gps'
  | 'Map'
  | 'mapPin'

export type imagesInterface = { [name in imageList]: ImageSourcePropType & ImageURISource }

const images: imagesInterface = {
  loginScreenFooter: require('../assets/images/LoginFooter.png'),
  Sorry: require('../assets/images/Sorry.png'),
  ThankYou: require('../assets/images/ThankYou.png'),
  YumboxAvatarDefault: require('../assets/images/profileImage.png'),
  showingSoon: require('../assets/images/showingSoon.png'),
  topupLogoMANDIRI: require('../assets/images/topup-logo/topup-logo-mandiri.png'),
  topupLogoBRI: require('../assets/images/topup-logo/topup-logo-bri.png'),
  topupLogoBNI: require('../assets/images/topup-logo/topup-logo-bni.png'),
  topupLogoBCA: require('../assets/images/topup-logo/topup-logo-bca.png'),
  topupLogoTOKEN: require('../assets/images/yummycredits.png'),
  YummyboxBeta: require('../assets/images/YummyboxBeta.png'),
  YummyboxUAT: require('../assets/images/YummyboxUAT.png'),
  favourite: require('../assets/images/favourite.png'),
  arrowLeft: require('../assets/images/arrowLeft.png'),
  iconFaq: require('../assets/images/faq-icon.png'),
  iconPrivacy: require('../assets/images/privacy-icon.png'),
  iconTnc: require('../assets/images/tnc-icon.png'),
  bookmark: require('../assets/images/bookmark.png'),
  noSavedAddress: require('../assets/images/NoSavedAddress.png'),
  currentLocation: require('../assets/images/current-location.png'),
  trash: require('../assets/images/trash.png'),
  unknownCard: require('../assets/images/UnknownCard.png'),
  cardFront: require('../Lib/react-native-credit-card-input-fullpage/images/card-front.png'),
  cardBack: require('../Lib/react-native-credit-card-input-fullpage/images/card-back.png'),
  waIcon: require('../assets/images/wa.png'),
  whatsappIcon: require('../assets/images/whatsapp.png'),
  danaWhite: require('../assets/images/payment/dana-white.png'),
  danaGrey: require('../assets/images/payment/dana-grey.png'),
  shopeeWhite: require('../assets/images/payment/shopee-white.png'),
  shopeeGrey: require('../assets/images/payment/shopee-grey.png'),
  gopayWhite: require('../assets/images/payment/gopay-white.png'),
  gopayGrey: require('../assets/images/payment/gopay-grey.png'),
  emptyCart: require('../assets/images/EmptyCart.png'),
  arrowDown: require('../assets/images/Down.png'),
  IntroOneBackground: require('../assets/images/IntroOneBackground.png'),
  IntroTwoBackground: require('../assets/images/IntroTwoBackground.png'),
  Logo: require('../assets/images/Logo.png'),
  Gps: require('../assets/images/gps.png'),
  Map: require('../assets/images/map.png'),
  mapPin: require('../assets/images/pin.png'),
}

export default images
