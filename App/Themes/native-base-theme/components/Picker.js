import variable from "./../variables/platform"
import { Platform } from 'react-native'
let picker
if (Platform.OS === 'android'){
  picker =  (variables = variable) => {
    const pickerTheme = {
      ".note": {
        color: "#8F8E95"
      },
      width: 90,
      marginRight: -4
    }

    return pickerTheme
  }
}else{
  picker = (variables = variable) => {
    const pickerTheme = {}
  
    return pickerTheme
  }
}

export default picker
