import Colors from './Colors'
import Metrics from './Metrics'
import { ViewStyle } from 'react-native'

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

export interface ScreenStylesInterface {
  mainContainer: ViewStyle,
  container: ViewStyle,
}

export interface ApplicationStylesInterface {
  screen: ScreenStylesInterface
}

const ApplicationStyles: ApplicationStylesInterface = {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.lightGrey4,
      flexDirection: 'column'
    },
    container: {
      flex: 1,
      paddingHorizontal: Metrics.mainSpaceHorizontal,
      paddingVertical: Metrics.mainSpaceVertical,
      backgroundColor: Colors.transparent,
    },

  },
}

export default ApplicationStyles
