import AvailabilityDateService from '../../App/Services/AvailabilityDateService'
import Moment from 'moment'


test('should return true if cutoff time has passed', () => {
  const afterCutoffTime = Moment('23:00', 'HH:mm').valueOf()
  jest.spyOn(Date, 'now').mockImplementation(() => afterCutoffTime)
  const cutOffTime = '22:00'
  expect(AvailabilityDateService.checkIfNowIsExceedingCutOffTime(cutOffTime)).toBe(true)
})

test('should return false if cutoff time is still in the future', () => {
  const afterCutoffTime = Moment('05:00', 'HH:mm').valueOf()
  jest.spyOn(Date, 'now').mockImplementation(() => afterCutoffTime)
  const cutOffTime = '22:00'
  expect(AvailabilityDateService.checkIfNowIsExceedingCutOffTime(cutOffTime)).toBe(false)
})