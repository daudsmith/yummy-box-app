jest.mock('react-native-config', () => ({
  API_BASE_URL: 'http://localhost',
  API_CATER_URL: 'http://localhost',
  XENDIT_CLIENT: 'http://localhost',
  IS_UAT: 'false',
  FIREBASE_APP_DOMAIN: 'firebase',
  IOS_APP_VERSION: '1',
  IOS_APP_BUILD: '1',
  ANDROID_APP_VERSION: '1',
  ANDROID_APP_BUILD: '1',
  RELEASE_VERSION: '1',
  APP_ENV: 'test',
  SENTRY_DSN: 'sentry_dsn',
  BUILD_REV: '1234'
}))

jest.mock('react-native-device-info', () => {
  return {
    isTablet: jest.fn(() => false),
    getUniqueId: jest.fn(() => '1'),
    getBuildNumber: jest.fn(() => '1')
  }
})

jest.mock('@react-native-firebase/analytics', () => {
  return () => ({
    logEvent: jest.fn(),
    setUserProperties: jest.fn(),
    setUserId: jest.fn(),
    setCurrentScreen: jest.fn(),
  })
});

jest.mock('@react-native-firebase/messaging', () => {
  return () => ({
    hasPermission: jest.fn(() => Promise.resolve(true)),
    subscribeToTopic: jest.fn(),
    unsubscribeFromTopic: jest.fn(),
    requestPermission: jest.fn(() => Promise.resolve(true)),
    getToken: jest.fn(() => Promise.resolve('token')),
  })
});

jest.mock('react-navigation', () => ({ withNavigation: component => component}));
