const fs = require('fs')
const gradlePath = './android/app/build.gradle'

const gradleContent = fs.readFileSync(gradlePath).toString()

if (gradleContent.indexOf('versionCode 1 //') < 0) {
    console.error('ERROR: PLEASE CHANGE versionCode TO 1 IN android/app/build.gradle') // eslint-disable-line no-console
    console.error('versionCode WILL BE SET AUTOMATICALLY IN APPCENTER') // eslint-disable-line no-console
    process.exit(1)
}
