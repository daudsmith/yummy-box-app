# YumboxApp

## Requirements

- Xcode. Install from the Mac App Store. Make sure you have opened Xcode at least once and install the command line tools.
- [nodejs](https://nodejs.org/en/) 10
- [yarn](https://yarnpkg.com/en/): `brew install yarn`
- [cocoapods](https://cocoapods.org/): `gem install cocoapods`
- `java 8`. You can install java sdk on Mac using brew:

```bash
brew tap adoptopenjdk/openjdk
brew cask install adoptopenjdk8
```

- Add the path to `~/.bashrc` or `~/.zshrc`:

```bash
export ANDROID_HOME=~/Library/Android/sdk/
export PATH=$PATH:~/android-sdks/platform-tools/
export PATH=$PATH:~/android-sdks/tools/
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
```

- [Android Studio](https://developer.android.com/studio).
- Install SDKs and create Android virtual device by following the instructions [here](https://facebook.github.io/react-native/docs/getting-started).

## Getting Started

1. Clone this repo
2. Enter the cloned repo directory
3. Install dependencies: `yarn`
4. Enter `ios` directory: `cd ios`
5. Install pod dependencies: `pod install`

## Development

### iOS

1. `yarn run:ios --scheme "UAT YumboxApp" --simulator "iPhone 11 Pro Max"` to run the iOS app on iOS simulator. Replace the simulator name with the simulator you want. This command will automatically open iOS simulator if it's not running yet.
2. `yarn run:ios --scheme "UAT YumboxApp" --device "<YOUR_DEVICE_NAME_OR_ID>"` to run the iOS app on your device. You might need to open `ios/YumboxApp.xcworkspace` once, then add your device to the development provisioning profile.
3. Or, open `YumboxApp.xcworkspace` in `ios` directory, choose the scheme ("UAT YumboxApp" for example), then choose Run with your iPhone selected in the Xcode.

### Android

1. Open Android Virtual Device Manager via Android Studio.
2. Launch an AVD in the emulator.
3. `ENVFILE=.env.uat yarn run:android` to run the android app.
4. To run the android app on your development device: plug the phone then run `adb devices`. Make sure it shows a device in the list.

## Debugging

### Android
1. download flipper on `https://fbflipper.com/`
2. add path flipper on your `.bashrc` or `.zshrc`

### iOS [TODO flipper implementation]
1. download Reactotron on `https://github.com/infinitered/reactotron/releases`
2. add path reactotron on your `.bashrc` or `.zshrc`


### Test

Run `yarn test` to run jest tests, linter, and typecheck.

## Continuous Deployment

This project uses Appcenter to distribute the application automatically to beta/UAT testers via email and to Android Play Store and iTunes Connect for iOS. There are 6 applications in the Yummycorp's [appcenter](https://appcenter.ms/users/yummycorp/applications):

1. [Yummybox iOS App Store.](https://appcenter.ms/users/yummycorp/apps/Yumbox-iOS) Build the app for iOS using _production_ environment then upload the binary to [App Store Connect TestFlight](https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1371320051/testflight?section=iosbuilds).
2. [Yummybox iOS UAT.](https://appcenter.ms/users/yummycorp/apps/Yummybox-iOS-Prod) Build the app for iOS using _UAT_ environment then distribute to testers by email.
3. [Yummybox iOS Staging.](https://appcenter.ms/users/yummycorp/apps/Yummybox-iOS-Staging) Build the app for iOS using _staging_ environment then distribute to testers by email.
4. [Yummybox Android Play Store.](https://appcenter.ms/users/yummycorp/apps/Yummybox-Android-Prod) Build the app for Android using _production_ environment then upload to Beta Play Store.
5. [Yummybox Android UAT.](https://appcenter.ms/users/yummycorp/apps/Yumbox-Android) Build the app for Android using _UAT_ environment then distribute to testers by email.
6. [Yummybox Staging.](https://appcenter.ms/users/yummycorp/apps/Yumbox-Android-Staging) Build the app for Android using _staging_ environment then distribute to testers by email.

All of the applications above have been configured to automatically build the Yummybox app for its respective platform when there is a new commit to **master** branch.

To distribute the app from another git branch:

1. First decide which of the 6 apps above you want to distribute.
2. Go to that app in the appcenter.
3. Select the branch of your work.
4. Click the down arrow on the right side of blue `Configure` button.
5. Click `Clone from existing configuration`
6. Choose `master` then click `Clone`
7. Click `Save & Build`.
8. Repeat step 2 to 7 if you want to distribute the other apps from that branch.

To add a tester, you need to add that person's e-mail address to **all of the 6 apps** above in the appcenter:

1. Select the app in the appcenter.
2. Click `Distribute` on the sidebar.
3. Click `Groups` under the `Distribute` menu.
4. Then you can either add the tester to Collaborators or create a new distribution group. After adding the tester's email address, the tester will receive an email from Appcenter to accept the invitation. They have to accept the invitation first before receiving new app update notification.
5. For the iOS app, you might need to register the tester's device first in the appcenter by going to _Distribute > Groups > Collaborators > Devices_.

### Releasing the Apps

- To release the iOS app to the App Store, you need to go to App Store Connect and select the build that has been uploaded by appcenter.
- To release the Android app to the Play Store, you need to go to Play Store console and release the beta build to Production.

## App Version

- App build version will be incremented automatically in appcenter.
- For android, the `versionCode` in `android/app/build.gradle` will be updated before building.
- For iOS, the `CFBundleVersion` string in `ios/YumboxApp/Info.plist`.
- You can find the script to update these values in `appcenter-pre-build.sh` file.

There are 2 reasons why we use the script in `appcenter-pre-build.sh` to increment the build version number instead of appcenter's auto increment feature:

1. Since the app has been released on the App Store and Play Store since before appcenter is used, the build version number of the app is already more than 1. But unfortunately, there is no way to set initial version number for the auto increment. This has been [an open issue](https://github.com/microsoft/appcenter/issues/21).
2. There is a bug in appcenter where the [auto increment is not working for Android](https://github.com/Microsoft/appcenter/issues/45#issuecomment-465269485) when the gradle plugin is `3.3.0` which is used in this project.

To make `appcenter-pre-build.sh` work properly:

- Add an environment variable called `VERSION_CODE_SHIFT` to the build configuration in appcenter for both the android and iOS.
- Set the value of `VERSION_CODE_SHIFT` to `52`. This number is the current `versionCode` of the app in the Play Store.
- As of this writing, these values have been added to the build configuration for master branch on both iOS and Android apps. So you just need to clone the configuration in the appcenter.

## Helpers

- test deep link app with adb shell `adb shell am start -W -a android.intent.action.VIEW -d "yummybox://" com.yummycorp.yummybox.staging`
